﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSQRCODESERVICE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new QRProcess()
            };
            ServiceBase.Run(ServicesToRun);

            #region For debug:
            QRProcess service = new QRProcess();
            service.StartService();
            System.Threading.Thread.Sleep(5000);
            #endregion
        }
    }
}
