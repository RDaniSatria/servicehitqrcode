﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSQRCODESERVICE.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MPMWMSQRCODESERVICE
{
    public partial class QRProcess : ServiceBase
    {
        QRUnitController qrUnitController = new QRUnitController();
        Thread thread;
        AutoResetEvent StopRequest = new AutoResetEvent(false);

        public QRProcess()
        {
            InitializeComponent();
            this.ServiceName = "QR Code Unit Service";
        }

        public void StartService()
        {
            thread = new Thread(new ThreadStart(QRUnitProcess));
            thread.Start();
        }

        protected override void OnStart(string[] args)
        {
            // Start the worker thread
            thread = new Thread(QRUnitProcess);
            thread.Start();
            //StartService();
        }

        protected override void OnStop()
        {
        }

        private void QRUnitProcess()
        {
            while (true)
            {
                try
                {
                    qrUnitController.processDataToSend();
                 
                    //Thread.Sleep(1000 * 10);
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}
