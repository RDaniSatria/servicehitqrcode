﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Mvc;
using MPMWMSQRCODESERVICE.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSQRCODESERVICE.Controllers
{
    public class QRUnitController
    {
        QRUnitEx qrUnit = new QRUnitEx();

        public void processDataToSend()
        {
            String engino = String.Empty;
            int idqr = 0;
            try
            {
                //bool un = qrUnit.unmatch("db78d1427b69641396bfa13368290987", "JBK1E1658072", "GD1", "Update Data");
                var listUnit = qrUnit.getDataByStatusSend(0).Where(x => x.RESPONSEMSG == null || x.RESPONSEMSG == "");
                var listCustomer = qrUnit.getCustomerQR(0).Where(x => x.RESPONSEMSG == null || x.RESPONSEMSG == "");

                string key = qrUnit.GetTokenVendore();
                string ret = String.Empty;

                foreach (var data in listUnit)
                {
                    engino = data.ENGINENO;
                    idqr = data.IDQR;
                    qrUnit.sendQRCodeVendor(data, key);
                }

                foreach (var data in listCustomer)
                {

                    engino = data.CUSTOMER_AHM_NO;
                    qrUnit.sendCustomer(data, key);
                }
            }
            catch (MPMException ex)
            {
                qrUnit.updateQRUnit(engino, idqr,"400", ex.Message, "error");
                qrUnit.updateCustomer(engino, "400", ex.Message, "error");

                //throw new MPMException(e.Message);
            }
        }
    }
}
