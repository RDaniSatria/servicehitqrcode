﻿using MPMLibrary.NET.Lib.Print;
using MPMWMSMODEL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesterModel
{
    class Program
    {

        private void PrintZebra()
        {
            try
            {
                String data =
                "^XA^PR3^XZ\r\n" +
                "^XA\r\n" +
                "^FO405,0^A0N,60,60^FD" + "STORING" + "^FS\r\n" +
                "^FO405,60^A0N,30,32^FD" + "GD1" + "^FS\r\n" +
                "^FO180,0^A0N,30,32^FD" + "KF11E2053172" + "^FS\r\n" +
                "^FO180,30^A0N,30,32^FD" + "WH" + "(" + "2017" + ")" + "^FS\r\n" +
                "^FO180,60^A0N,30,32^FD" + "VARIO" + "^FS\r\n" +
                "^FO180,90^A0N,30,32^FD" + "06-04-2017" + "^FS\r\n" +
                "^FO405,90^A0N,20,22^FD" + "06-04-2017" + "^FS\r\n" +
                "^FO220,120^BY2,1.0^BCN,50,Y,N,N^FD" + "KF11E2053172" +
                "^FS\r\n" +
                "^PQ1\r\n" +
                "^XZ";

                MPMNetworkPrint print = new MPMNetworkPrint("10.10.206.111", 6101, data);
                print = null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        static void Main(string[] args)
        {
            /*BaseModel model = new BaseModel();
            List<MPMWMSMODEL.View.Customer.CustomerRecord> data = model.LovDealerObject.List("mml");

            foreach(var a in data)
            {
                Console.WriteLine(a.ACCOUNTNUM);
            }*/

            Program p = new Program();
            p.PrintZebra();
            Console.WriteLine("finish.. press any key.");
            Console.ReadKey();

        }
    }
}
