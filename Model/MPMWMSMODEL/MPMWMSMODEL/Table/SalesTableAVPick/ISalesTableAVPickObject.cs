﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SalesTableAVPick
{
    public interface ISalesTableAVPickObject
    {
        MPMWMS_DO_AVPICK Create();
        List<MPMWMS_DO_AVPICK> List();
        void Refresh();
        MPMWMS_DO_AVPICK Item(String sales_id, String dataarea_id, String maindealer_id, String site_id);
        void Insert(MPMWMS_DO_AVPICK item);
        void Update(MPMWMS_DO_AVPICK item);
    }
}
