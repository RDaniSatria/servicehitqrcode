﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.SalesTableAVPick
{
    public class SalesTableAVPickObject : MPMDbObject<WmsUnitDataContext>, ISalesTableAVPickObject
    {
        public SalesTableAVPickObject()
            : base()
        {
        }

        public MPMWMS_DO_AVPICK Create()
        {
            MPMWMS_DO_AVPICK item = new MPMWMS_DO_AVPICK();
            return item;
        }

        public List<MPMWMS_DO_AVPICK> List()
        {
            return Context.MPMWMS_DO_AVPICKs.ToList();
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_DO_AVPICKs);
        }

        public MPMWMS_DO_AVPICK Item(String do_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from do_av in Context.MPMWMS_DO_AVPICKs
                                where
                                (do_av.DO_ID == do_id) &&
                                (do_av.MAINDEALER_ID == maindealer_id) &&
                                (do_av.SITE_ID == site_id) &&
                                (do_av.DATAAREA_ID == dataarea_id)
                                select do_av;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_DO_AVPICK item)
        {
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_DO_AVPICKs.InsertOnSubmit(item);
        }

        public void Update(MPMWMS_DO_AVPICK item)
        {
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = DateTime.Now;
        }
    }
}
