﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Collie
{
    public class CollieRecord
    {
        [Display(Name = "Code")]
        [Required(ErrorMessage = "CODE is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String CODE { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "NAME is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String NAME { get; set; }

        [Display(Name = "Dimension (cm)")]
        [Required(ErrorMessage = "DIMENSION is required")]
        public long DIMENSION { get; set; }

        [Display(Name = "Cubic (m3)")]
        [Required(ErrorMessage = "CUBIC is required")]
        public decimal CUBIC { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "TYPE is required")]
        [StringLength(1, ErrorMessage = "Must be 1 characters")]
        public String TYPE { get; set; }

        [Display(Name = "Coefficient Cost")]
        [Required(ErrorMessage = "COEFFICIENT is required")]
        public decimal COEFFICIENT { get; set; }

        [Display(Name = "Value")]
        [Required(ErrorMessage = "VALUE is required")]
        public int VALUE { get; set; }

        [Display(Name = "Freight Value")]
        [Required(ErrorMessage = "FREIGH TVALUE is required")]
        public decimal FREIGHTVALUE { get; set; }

        public CollieRecord(string _code, string _name, long _dimension, decimal _cubic, string _type, decimal _coeficient, int _value, decimal _freight)
        {
            CODE = _code;
            NAME = _name;
            DIMENSION = _dimension;
            CUBIC = _cubic;
            TYPE = _type;
            COEFFICIENT = _coeficient;
            VALUE = _value;
            FREIGHTVALUE = _freight;
        }

        public CollieRecord()
        {
        }
    }
}
