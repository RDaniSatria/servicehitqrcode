﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Collie
{
    public class CollieObject : MPMDbObject<WmsUnitDataContext>
    {
        public enum CollieType
        {
            BAN = 1,
            OLI,
            PART
        };

        public CollieObject()
            : base()
        {
        }

        public MPMWMS_COLLIE Item(MPMWMS_COLLIE _item)
        {
            try
            {
                var data = from a in Context.MPMWMS_COLLIEs
                           where a.MAINDEALERID == _item.MAINDEALERID
                               && a.SITEID == _item.SITEID
                               && a.DATAAREAID == _item.DATAAREAID
                               && a.COLLIECODE == _item.COLLIECODE
                           select a;

                return data.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<CollieRecord> ViewAll()
        {
            try
            {
                var data = from a in Context.MPMWMS_COLLIEs
                           where a.SITEID == UserSession.SITE_ID_MAPPING
                               && a.MAINDEALERID == UserSession.MAINDEALER_ID
                               && a.DATAAREAID == UserSession.DATAAREA_ID
                           select new CollieRecord
                               (
                                    a.COLLIECODE,
                                    a.COLLIENAME,
                                    Convert.ToInt64((long)a.WIDTH * (long)a.HEIGHT * (long)a.DEPTH),
                                    Convert.ToDecimal((decimal)a.WIDTH * (decimal)a.HEIGHT * (decimal)a.DEPTH) / 1000000,
                                    convertCollTypeEnum(Convert.ToInt16(a.COLIIETYPE)),
                                    Convert.ToDecimal(a.COEFFICIENTCOST),
                                    Convert.ToInt32(a.VALUE),
                                    Convert.ToDecimal(a.FREIGHTVALUE)
                               );

                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_COLLIE> ItemByCollieCode(string _collieCode, string _mainDealer, string _site, string _dataArea)
        {
            try
            {
                var data = from a in Context.MPMWMS_COLLIEs
                           where a.MAINDEALERID == _mainDealer
                               && a.SITEID == _site
                               && a.DATAAREAID == _dataArea
                               && a.COLLIECODE == _collieCode
                           select a;

                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_COLLIE item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_COLLIE item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMS_COLLIEs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_COLLIE item)
        {
            try
            {
                Context.MPMWMS_COLLIEs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        string convertCollTypeEnum(int _value)
        {
            CollieType collieTypeEnum = (CollieType)_value;

            return collieTypeEnum.ToString();
        }
    }
}
