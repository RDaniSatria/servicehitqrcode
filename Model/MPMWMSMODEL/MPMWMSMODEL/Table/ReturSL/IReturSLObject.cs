﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ReturSL
{
    public interface IReturSLObject
    {
        MPMWMS_RETUR_SL Create();
        void Insert(MPMWMS_RETUR_SL item);
        void Update(MPMWMS_RETUR_SL item);
        MPMWMS_RETUR_SL Item(String retur_sl_id, String dataarea_id, String maindealer_id, String site_id);
    }
}
