﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.ReturSLLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.ReturSL
{
    public class ReturSLObject: MPMDbObject<WmsUnitDataContext>, IReturSLObject
    {
        private ReturSLLineObject _ReturSLLineObject = new ReturSLLineObject();

        public ReturSLObject()
            : base()
        {
        }

        public ReturSLLineObject ReturSLLineObject
        {
            get { return _ReturSLLineObject; }
        }

        public MPMWMS_RETUR_SL Create()
        {
            MPMWMS_RETUR_SL row = new MPMWMS_RETUR_SL();
            row.RETUR_SL_DATE = MPMDateUtil.DateTime;
            row.STATUS = "O";
            row.IS_CANCEL = "N";
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public List<MPMWMS_RETUR_SL> List()
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_RETUR_SLs
                    where
                        a.DATAAREA_ID == UserSession.DATAAREA_ID
                        && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                        && a.SITE_ID == UserSession.SITE_ID_MAPPING
                        && a.IS_CANCEL == "N"
                    select a;

                return search.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public void Insert(MPMWMS_RETUR_SL item)
        {
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_RETUR_SLs.InsertOnSubmit(item);
        }

        public void Update(MPMWMS_RETUR_SL item)
        {
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public MPMWMS_RETUR_SL Item(String retur_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_RETUR_SLs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.RETUR_SL_ID == retur_sl_id
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
