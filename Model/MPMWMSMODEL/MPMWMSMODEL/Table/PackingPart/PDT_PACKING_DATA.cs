﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PackingPart
{
    [Serializable]
    public class PDT_PACKING_DATA
    {
        public String PACKING_ID;
        public String PICKING_ID;
        public String DATAAREA_ID;
        public String DEALER_CODE;
        public String DEALER_NAME;
        public int COLLIE;

        public PDT_PACKING_DATA(String packing_id, String picking_id, String dataarea_id, String dealer_code, String dealer_name)
        {
            PACKING_ID = packing_id;
            PICKING_ID = picking_id;
            DATAAREA_ID = dataarea_id;
            DEALER_CODE = dealer_code;
            DEALER_NAME = dealer_name;
            COLLIE = 0;
        }
    }
}
