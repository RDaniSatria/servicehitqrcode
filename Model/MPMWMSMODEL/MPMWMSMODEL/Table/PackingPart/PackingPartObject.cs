﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.PackingPart;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PackingPart
{
    public class PackingPartObject : MPMDbObject<WmsUnitDataContext>
    {
        public PackingPartObject() 
            : base()
        {
        }

        public List<MPMWMS_MAP_SO_PICAREA> dataPacking(string _trollyNbr, string _npk, string _dataArea)
        {
            String QueryitemsObj =
                "SELECT A.* " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) P " +
                "LEFT JOIN CUSTINVOICEJOUR (NOLOCK) I ON I.SALESID = P.SALESID AND I.DATAAREAID = P.DATAAREAID AND I.PARTITION = P.PARTITION " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON P.SALESID = A.SOID AND P.DATAAREAID = A.DATAAREAID AND P.INVOICEACCOUNT = A.DEALERID " +
                "WHERE P.XTSTROLLEYNBR = '" + _trollyNbr + "' " +
                "AND P.DATAAREAID = '" + _dataArea + "' " +
                "AND P.XTSLOCKEDUSERID = '" + _npk + "' "+
                "AND I.INVOICEID IS NULL";

            List<MPMWMS_MAP_SO_PICAREA> List = Context.ExecuteQuery<MPMWMS_MAP_SO_PICAREA>(QueryitemsObj).ToList();

            return List;

            //var dataJob =   from pack in Context.CUSTPACKINGSLIPJOURs
            //                where pack.XTSTROLLEYNBR == _trollyNbr
            //                    && pack.XTSLOCKEDUSERID != ""
            //                    && pack.XTSLOCKEDUSERID == _npk
            //                    && pack.DATAAREAID == _dataArea
            //                join mapSO in Context.MPMWMS_MAP_SO_PICAREAs on new { A = pack.SALESID, B = pack.DATAAREAID, C = pack.INVOICEACCOUNT }
            //                                                        equals new { A = mapSO.SOID, B = mapSO.DATAAREAID, C = mapSO.DEALERID }
            //                select mapSO;

            //dataJob = from data in dataJob
            //          group data by data.JOBID into g
            //          select g.FirstOrDefault();

            //return dataJob.ToList();
        }

        public List<int> jobIDReadyPack(string _trollyNbr, string _npk, string _dataArea)
        {
            // ambil data Job yang sudah jadi Packing 
            var dataJobPacking = from pack in Context.CUSTPACKINGSLIPJOURs
                                 where pack.XTSTROLLEYNBR == _trollyNbr
                                     && pack.XTSLOCKEDUSERID != ""
                                     && pack.XTSLOCKEDUSERID == _npk
                                     && pack.DATAAREAID == _dataArea
                                 join mapSO in Context.MPMWMS_MAP_SO_PICAREAs on new { A = pack.SALESID, B = pack.DATAAREAID, C = pack.INVOICEACCOUNT }
                                                                         equals new { A = mapSO.SOID, B = mapSO.DATAAREAID, C = mapSO.DEALERID }
                                 select mapSO.JOBID;

            //// ambil data Job yang akan dijadikan Packing (proses picking sudah selesai)
            //var dataJobReadyPacking = from order in Context.WMSORDERTRANs
            //                          where order.MPMSTATUSPICKING == 2
            //                          join mapSO in Context.MPMWMS_MAP_SO_PICAREAs on new { A = order.INVENTTRANSREFID, B = order.DATAAREAID }
            //                                                                   equals new { A = mapSO.SOID, B = mapSO.DATAAREAID }
            //                          select mapSO.JOBID;

            //var dataJob = dataJobPacking.Union(dataJobReadyPacking);

            var dataJob = dataJobPacking;

            dataJob = from data in dataJob
                      group data by data into g
                      select g.FirstOrDefault();

            return dataJob.ToList();
        }

        public Boolean checkAlreadyPacked(String _packingId, String _itemId, String _boxId)
        {
            var data = from pack in Context.MPMWMS_PACKINGBOXes
                       where pack.PACKINGID == _packingId
                           && pack.PARTNO == _itemId
                           && pack.BOXID == _boxId
                       select pack.PARTNO;

            return data.Count() > 0 ? true : false;
        }

        public List<PACKING_INFO_Record> dataPackingInfoByJob(String _dataArea, String _npk, int _jobId)
        {
            String QueryitemsObj =
                "SELECT JOBID = A.JOBID " +
                     ", DEALERID = P.INVOICEACCOUNT " +
                     ", DEALERNAME = P.DELIVERYNAME " +
                     ", TOTALSO = 0 " +
                     ", TOTALPART = 0 " +
                     ", SALESID = P.SALESID " +
                     ", ITEMID = L.ITEMID " +
                     ", ITEMNAME = L.NAME " +
                     ", PACKINGSLIPID = P.PACKINGSLIPID " +
                     ", RUTEID = CASE WHEN R.DESCRIPTION IS NULL THEN '' ELSE P.MPMROUTECODE + ' - ' + R.DESCRIPTION END " +
                     ", KOTA = CASE WHEN V.CITY IS NULL THEN '' ELSE V.CITY END " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) P " +
                "JOIN CUSTPACKINGSLIPTRANS (NOLOCK) L ON P.PACKINGSLIPID = L.PACKINGSLIPID AND P.DATAAREAID = L.DATAAREAID " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON P.SALESID = A.SOID AND P.DATAAREAID = A.DATAAREAID AND P.INVOICEACCOUNT = A.DEALERID " +
                "LEFT JOIN MPMSERVVIEWCHANNELH3 (NOLOCK) V ON V.ACCOUNTNUM = P.INVOICEACCOUNT " +
                "LEFT JOIN MPMWMS_ROUTE(NOLOCK) R ON P.MPMROUTECODE = R.ROUTEID " +
                "WHERE P.XTSTROLLEYNBR <> '' " +
                "AND P.XTSLOCKEDUSERID = '" + _npk + "' " +
                "AND P.DATAAREAID = '" + _dataArea + "' " +
                "AND A.JOBID = " + _jobId + "";

            List<PACKING_INFO_Record> List = Context.ExecuteQuery<PACKING_INFO_Record>(QueryitemsObj).ToList();
            return List;
        }

        public int qtySisaPacking(String _itemId, int _jobId, String _dataArea, String _siteId)
        {
            int qtySisa = 0;

            String QueryitemsObj =
                "SELECT SUM(X.QTY) " +
                "FROM " +
                "( " +
                    "SELECT QTY = ISNULL(CONVERT(INT, SUM(L.QTY) - " +
                    "( " +
                    "    SELECT SUM(B.QUANTITY) " +
                    "    FROM MPMWMS_PACKINGBOX (NOLOCK) B " +
                    "    WHERE B.PACKINGID = L.PACKINGSLIPID AND L.ITEMID = B.PARTNO " +
                    ")), SUM(L.QTY)) " +
                    "FROM CUSTPACKINGSLIPTRANS (NOLOCK) L " +
                    "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON L.SALESID = A.SOID AND L.DATAAREAID = A.DATAAREAID " +
                    "WHERE L.ITEMID = '" + _itemId + "' " +
                    "AND A.JOBID = " + _jobId + "" +
                    "AND A.DATAAREAID = '" + _dataArea + "' " +
                    "AND A.SITEID = '" + _siteId + "' " +
                    "GROUP BY L.ITEMID, L.PACKINGSLIPID " +
                ") X ";

            foreach (var x in Context.ExecuteQuery<int>(QueryitemsObj).ToList())
            {
                qtySisa = x;
            }

            return qtySisa;
        }

        public void updateFlagIsPacked(String _itemId, int _jobId, String _dataArea, String _siteId, int _isPacked)
        {
            String QueryitemsObj =
                "UPDATE L SET XTSISPACKED = " + _isPacked + " " +
                "FROM CUSTPACKINGSLIPTRANS (NOLOCK) L " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON L.SALESID = A.SOID AND L.DATAAREAID = A.DATAAREAID  " +
                "LEFT JOIN MPMWMS_PACKINGBOX (NOLOCK) B ON B.PACKINGID = L.PACKINGSLIPID AND L.ITEMID = B.PARTNO " +
                "WHERE L.ITEMID = '" + _itemId + "' " +
                "AND A.JOBID = " + _jobId + "" +
                "AND A.DATAAREAID = '" + _dataArea + "' " +
                "AND A.SITEID = '" + _siteId + "' ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public void resetFlagCustPackingSlipJour(String _itemId, int _jobId, String _dataArea, String _siteId)
        {
            String QueryitemsObj =
                "UPDATE J SET XTSTROLLEYNBR = '', XTSLOCKEDUSERID = '', XTSISTRANSFERED = 2 " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) J " +
                "JOIN CUSTPACKINGSLIPTRANS (NOLOCK) L ON L.PACKINGSLIPID = J.PACKINGSLIPID AND L.SALESID = J.SALESID AND L.DATAAREAID = J.DATAAREAID  " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON L.SALESID = A.SOID AND L.DATAAREAID = A.DATAAREAID  " +
                "LEFT JOIN MPMWMS_PACKINGBOX (NOLOCK) B ON B.PACKINGID = L.PACKINGSLIPID AND L.ITEMID = B.PARTNO " +
                "WHERE L.XTSISPACKED <> 0 " +
                "AND L.ITEMID = '" + _itemId + "' " +
                "AND A.JOBID = " + _jobId + "" +
                "AND A.DATAAREAID = '" + _dataArea + "' " +
                "AND A.SITEID = '" + _siteId + "' ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public Boolean cekIsPackedFinish(String _trolley, String _dataArea, String _siteId)
        {
            int countData = 0;

            String QueryitemsObj =
                "SELECT COUNT(*)                                                                                                                 " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) P                                                                                             " +
                "JOIN CUSTPACKINGSLIPTRANS (NOLOCK) L ON P.PACKINGSLIPID = L.PACKINGSLIPID AND P.DATAAREAID = L.DATAAREAID                       " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON P.SALESID = A.SOID AND P.DATAAREAID = A.DATAAREAID AND P.INVOICEACCOUNT = A.DEALERID   " +
                "LEFT JOIN CUSTINVOICEJOUR (NOLOCK)I ON I.SALESID = P.SALESID AND I.DATAAREAID = P.DATAAREAID AND I.PARTITION = P.PARTITION      " +
                "WHERE P.XTSTROLLEYNBR = '" + _trolley + "'                                                                                      " +
                "AND I.INVOICEID IS NULL                                                                                                         " +
                "AND L.XTSISPACKED = 0                                                                                                           " +
                "AND A.SITEID = '" + _siteId + "'                                                                                                " +
                "AND P.DATAAREAID = '" + _dataArea + "'                                                                                          ";

            Context.ExecuteQuery<int>(QueryitemsObj).ToList();

            foreach (var x in Context.ExecuteQuery<int>(QueryitemsObj).ToList())
            {
                countData = x;
            }

            return (countData == 0) ? true : false;
        }
         
        public List<LIST_PART_Record> dataListPart(string _trolley, String _dataArea, String _siteId)
        {
            String QueryitemsObj =
                " IF OBJECT_ID('tempdb..#PACKING') IS NOT NULL                                                             " +
                " BEGIN                                                                                                    " +
                " DROP TABLE #PACKING                                                                                      " +
                " END;                                                                                                     " +
                "                                                                                                          " +
                " SELECT L.ITEMID, L.NAME, L.SALESID, L.PACKINGSLIPID, L.DATAAREAID                                        " +
                " INTO #PACKING                                                                                            " +
                " FROM CUSTPACKINGSLIPJOUR(NOLOCK)P                                                                        " +
                " JOIN CUSTPACKINGSLIPTRANS(NOLOCK) L ON P.PACKINGSLIPID = L.PACKINGSLIPID AND P.DATAAREAID = L.DATAAREAID " +
                " WHERE P.XTSTROLLEYNBR = '" + _trolley + "'                                                               " +
                " AND P.DATAAREAID = '" + _dataArea + "'                                                                   " +
                "                                                                                                          " +
                " SELECT ITEMID = L.ITEMID                                                                                 " +
                " , ITEMNAME = L.NAME                                                                                      " +
                " , BOXID = B.BOXID                                                                                        " +
                " , QTY = B.QUANTITY                                                                                       " +
                " , SOID = L.SALESID                                                                                       " +
                " , JOBID = A.JOBID                                                                                        " +
                " FROM #PACKING L                                                                                          " +
                " JOIN MPMWMS_MAP_SO_PICAREA(NOLOCK) A ON L.SALESID = A.SOID AND L.DATAAREAID = A.DATAAREAID               " +
                " JOIN MPMWMS_PACKINGBOX(NOLOCK) B ON B.PACKINGID = L.PACKINGSLIPID AND L.ITEMID = B.PARTNO                " +
                " WHERE A.DATAAREAID = '" + _dataArea + "'                                                                 " +
                " AND A.SITEID = '" + _siteId + "'                                                                         " +
                " GROUP BY L.ITEMID, L.NAME, B.BOXID, B.QUANTITY, L.SALESID, A.JOBID                                       " +
                "                                                                                                          " +
                " IF OBJECT_ID('tempdb..#PACKING') IS NOT NULL                                                             " +
                " BEGIN                                                                                                    " +
                " DROP TABLE #PACKING                                                                                      " +
                " END;                                                                                                     "; 


            //"SELECT ITEMID = L.ITEMID " +
            //    ", ITEMNAME = L.NAME " +
            //    ", BOXID = B.BOXID " +
            //    ", QTY = B.QUANTITY " +
            //    ", SOID = L.SALESID " +
            //    ", JOBID = A.JOBID " +
            //"FROM CUSTPACKINGSLIPJOUR(NOLOCK) P " +
            //"JOIN CUSTPACKINGSLIPTRANS (NOLOCK) L ON P.PACKINGSLIPID = L.PACKINGSLIPID AND P.DATAAREAID = L.DATAAREAID " +
            //"JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) A ON L.SALESID = A.SOID AND L.DATAAREAID = A.DATAAREAID " +
            //"JOIN MPMWMS_PACKINGBOX (NOLOCK) B ON B.PACKINGID = L.PACKINGSLIPID AND L.ITEMID = B.PARTNO " +
            //"WHERE P.XTSTROLLEYNBR = '" + _trolley + "' " +
            //"AND A.DATAAREAID = '" + _dataArea + "' " +
            //"AND A.SITEID = '" + _siteId + "' " +
            //"GROUP BY L.ITEMID, L.NAME, B.BOXID, B.QUANTITY, L.SALESID, A.JOBID";

            List<LIST_PART_Record> List = Context.ExecuteQuery<LIST_PART_Record>(QueryitemsObj).ToList();

            return List;
        }

        public int getWeightSticker(String _tag1, String _tag2, int _year, int _month)
        {
            String QueryitemsObj =
                "DECLARE @GETNUMBER AS INT " +
                "EXEC MPMGETRUNNINGNUMBER '" + _tag1 + "', '" + _tag2 + "', " + _year + ", " + _month + ", @GETNUMBER OUTPUT " + 
                "SELECT @GETNUMBER";

            foreach (var data in Context.ExecuteQuery<int>(QueryitemsObj).ToList())
            {
                return data;
            }

            return 0;
        }

        public MPMWMS_COLLIE getCollieData(String _collieCode, String _maindealer, String _siteid)
        {
            var data = from collie in Context.MPMWMS_COLLIEs
                        where SqlMethods.Like(collie.COLLIECODE, _collieCode + "%")
                            && collie.SITEID == _siteid
                            && collie.MAINDEALERID == _maindealer
                        select collie;

            return data.FirstOrDefault();
        }

        public int getDimensionBox(String _collieCode, String _maindealer, String _siteid)
        {
            try
            {
                String collieCode = "";

                var validasi = this.getCollieData(_collieCode, _maindealer, _siteid);

                if (validasi != null)
                {
                    collieCode = validasi.COLLIECODE;

                    var data = from collie in Context.MPMWMS_COLLIEs
                                    where collie.COLLIECODE == collieCode
                               join box in Context.MPMWMS_BOX_BARCODEs on new { A = collie.COLLIECODE, B = collie.MAINDEALERID, C = collie.SITEID }
                                                                   equals new { A = box.COLLIECODE, B = box.MAINDEALERID, C = box.SITEID }
                               select collie;

                    var item = data.FirstOrDefault();

                    if(item != null)
                    {
                        return (item.HEIGHT.Value * item.WIDTH.Value * item.DEPTH.Value) * item.VALUE.Value;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    throw new MPMException("Kode collie tidak vaild !!!");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void updateRegCollie(String _dataArea, String _siteId, String _regCollie, String _soId)
        {
            String QueryitemsObj =
                "UPDATE L SET MPMREGCOLLIE = '" + _regCollie + "' " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) L " +
                "WHERE L.SALESID = '" + _soId + "' " +
                "AND L.DATAAREAID = '" + _dataArea + "' " +
                "AND L.PRINTMGMTSITEID = '" + _siteId + "' ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public void updateInvoiceProses(String _dataArea, String _siteId, String _trolley)
        {
            String QueryitemsObj =
                "UPDATE I SET XTSLOCKEDUSERID = 'INVOICED'   " +
                "FROM CUSTPACKINGSLIPJOUR I                  " +
                "WHERE I.XTSTROLLEYNBR = '" + _trolley + "'  " +
                "AND I.PRINTMGMTSITEID = '" + _siteId + "'   " +
                "AND I.DATAAREAID = '" + _dataArea + "'      ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public void updateBoxId(String _dataArea, String _siteId, String _mainDealer, String _boxIdOld, String _boxIdNew)
        {
            String QueryitemsObj =
                " UPDATE B SET BOXID = '" + _boxIdNew + "'   " +
                " FROM MPMWMS_PACKINGBOX (NOLOCK)B           " +
                " WHERE B.BOXID = '" + _boxIdOld + "'        " +
                " AND B.DATAAREAID = '" + _dataArea + "'     " +
                " AND B.SITEID = '" + _siteId + "'           " +
                " AND B.MAINDEALERID = '" + _mainDealer + "' ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public List<PICKING_LIST_Record> getPickingListByJob(String _dataArea, String _siteId, String _jobId, String _trolley)
        {
            String QueryitemsObj =
                "SELECT CAST(M.JOBID AS varchar(2)) JOBID, L.SALESID, T.ITEMID, L.NAME, D.INVENTLOCATIONID, CAST(SUM(T.QTY) AS INT) QTY, P.XTSTROLLEYNBR                                                     " +
                "FROM  WMSPICKINGROUTE P (NOLOCK)                                                                                                                     " +
                "JOIN WMSORDERTRANS T(NOLOCK) ON P.PICKINGROUTEID = T.ROUTEID AND P.DATAAREAID = T.DATAAREAID                                                         " +
                "JOIN SALESLINE L(NOLOCK) ON L.SALESID = P.TRANSREFID AND L.ITEMID = T.ITEMID AND L.INVENTTRANSID = T.INVENTTRANSID AND L.INVENTDIMID = T.INVENTDIMID " +
                "JOIN INVENTDIM D(NOLOCK) ON D.INVENTDIMID = T.INVENTDIMID                                                                                            " +
                "JOIN MPMWMS_MAP_SO_PICAREA(NOLOCK) M ON M.SOID = L.SALESID AND M.SITEID = D.INVENTSITEID                                                             " +
                "WHERE M.JOBID = '" + _jobId + "'                                                                                                                     " +
                "AND M.SITEID = '" + _siteId + "'                                                                                                                     " +
                "AND M.DATAAREAID = '" + _dataArea + "'                                                                                                               " +
                "AND P.XTSTROLLEYNBR = '" + _trolley + "'                                                                                                             " +
                "GROUP BY L.SALESID, T.ITEMID, L.NAME, D.INVENTLOCATIONID, M.JOBID, P.XTSTROLLEYNBR                                                                   ";

            List<PICKING_LIST_Record> List = Context.ExecuteQuery<PICKING_LIST_Record>(QueryitemsObj).ToList();

            return List;
        }
    }
}
