﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.MutationLocation
{
    public class MutationLocationObject : MPMDbObject<WmsUnitDataContext>, IMutationLocationObject
    {
        public MutationLocationObject()
            : base()
        {            
        }

        public MPMWMS_MUTATION_LOCATION Create()
        {
            MPMWMS_MUTATION_LOCATION item = new MPMWMS_MUTATION_LOCATION();
            return item;
        }

        public void Insert(MPMWMS_MUTATION_LOCATION row)
        {
            row.CREATE_BY = "";
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_MUTATION_LOCATIONs.InsertOnSubmit(row);
        }
    }
}
