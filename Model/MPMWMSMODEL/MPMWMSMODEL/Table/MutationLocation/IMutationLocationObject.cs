﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.MutationLocation
{
    public interface IMutationLocationObject
    {
        MPMWMS_MUTATION_LOCATION Create();
        void Insert(MPMWMS_MUTATION_LOCATION row);
    }
}
