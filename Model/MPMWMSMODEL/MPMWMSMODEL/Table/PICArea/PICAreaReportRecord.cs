﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PICArea
{
    public class PICAreaReportRecord
    {
        public String DEALER { get; set; }
        public String SALESID { get; set; }
        public DateTime SODATE { get; set; }
        public DateTime PICKINGDATE { get; set; }
        public DateTime PACKINGDATE { get; set; }
        public String JOB { get; set; }
        public String PICPICKER { get; set; }
        public String NAMEPICKER { get; set; }
        public String PICTRANSPORTER { get; set; }
        public String NAMETRANSPORTER { get; set; }
        public String PICCHECKER { get; set; }
        public String NAMECHECKER { get; set; }

        public PICAreaReportRecord(
              string _dealer
            , string _salesid
            , DateTime _soDate
            , DateTime _pickingDate
            , DateTime _packingDate
            , string _job
            , string _picpicker
            , string _namePicker
            , string _pictranspoter
            , string _nameTranspoter
            , string _picchecker
            , string _nameChecker)
        {
            DEALER = _dealer;
            SALESID = _salesid;
            SODATE = _soDate;
            PICKINGDATE = _pickingDate;
            PACKINGDATE = _packingDate;
            JOB = _job;
            PICPICKER = _picpicker;
            NAMEPICKER = _namePicker;
            PICTRANSPORTER = _pictranspoter;
            NAMETRANSPORTER = _nameTranspoter;
            PICCHECKER = _picchecker;
            NAMECHECKER = _nameChecker;
        }


        public PICAreaReportRecord()
        {
        }
    }
}
