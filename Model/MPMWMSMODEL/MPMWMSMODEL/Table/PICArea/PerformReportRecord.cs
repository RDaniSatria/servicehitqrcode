﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PICArea
{
    public class PerformReportRecord
    {
        [Display(Name = "TANGGAL")]
        public DateTime TRANSDATE { get; set; }

        [Display(Name = "NPK")]
        public String PIC { get; set; }

        [Display(Name = "NAMA")]
        public String USER_NAME { get; set; }

        [Display(Name = "START JOB")]
        public DateTime STARTSCAN { get; set; }

        [Display(Name = "END JOB")]
        public DateTime ENDSCAN { get; set; }

        [Display(Name = "LEADTIME (hh mm)")]
        public String LEADTIME { get; set; }

        [Display(Name = "TARGET")]
        public String DATATARGET { get; set; }

        [Display(Name = "COUNT SO")]
        public int SO_COUNT { get; set; }

        [Display(Name = "SUM QTY")]
        public int SUM_QTY { get; set; }

        [Display(Name = "JUMLAH LINE")]
        public int SUM_SO_LINE { get; set; }

        [Display(Name = "%")]
        public decimal PERSEN { get; set; }

        [Display(Name = "JAM LEMBUR")]
        public String LEMBUR { get; set; }

        [Display(Name = "OVER LINE")]
        public int OVERLINE { get; set; }

        public PerformReportRecord()
        {
        }

        public PerformReportRecord(
              DateTime _tanggal
            , String _NPK
            , String _nama
            , DateTime _startJob
            , DateTime _endJob
            , String _leadTime
            , String _target
            , int _jumlahLine
            , int _countSO
            , int _sumQty
            , decimal _persen
            , String _lembur
            , int _overLine
            )
        {
            TRANSDATE = _tanggal;
            PIC = _NPK;
            USER_NAME = _nama;
            STARTSCAN = _startJob;
            ENDSCAN = _endJob;
            LEADTIME = _leadTime;
            DATATARGET = _target;
            SUM_SO_LINE = _jumlahLine;
            SO_COUNT = _countSO;
            SUM_QTY = _sumQty;
            PERSEN = _persen;
            LEMBUR = _lembur;
            OVERLINE = _overLine;
        }
    }
}
