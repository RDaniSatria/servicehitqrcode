﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PICArea
{
    public class PICAreaRecord
    {
        public String PIC { get; set; }
        public String NPK { get; set; }
        public String SITEID { get; set; }
        public String INITIALWAREHOUSEID { get; set; }
        public String NAMAUSER { get; set; }

        public PICAreaRecord(string _PIC, string _NPK, string _SiteID, string _InitialWH, string _NamaUser)
        {
            PIC = _PIC;
            NPK = _NPK;
            SITEID = _SiteID;
            INITIALWAREHOUSEID = _InitialWH;
            NAMAUSER = _NamaUser;
        }

        public PICAreaRecord()
        {
        }
    }
}
