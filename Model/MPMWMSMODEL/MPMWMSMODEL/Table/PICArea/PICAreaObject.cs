﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.PICArea;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PICArea
{
    public class PICAreaObject : MPMDbObject<WmsUnitDataContext>
    {
        public PICAreaObject()
            : base()
        {
        }

        public MPMWMS_MAP_PIC_WAREHOUSE Item(String _pic, String _NPK, String _siteId, String _initWarehouse)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_PIC_WAREHOUSEs
                               where (a.PIC == _pic)
                                && (a.NPK == _NPK)
                                && (a.SITEID == _siteId)
                                && (a.INITIALWAREHOUSEID == _initWarehouse)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_MAP_PIC_WAREHOUSE ItemByInitial(String _pic, String _NPK, String _initialWarehouse, String _siteId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_PIC_WAREHOUSEs
                               where (a.PIC == _pic)
                                && (a.NPK == _NPK)
                                && (a.INITIALWAREHOUSEID == _initialWarehouse)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<PICAreaRecord> ItemAll(String _siteId)
        {
            try
            {
                var itemsObj =  from a in Context.MPMWMS_MAP_PIC_WAREHOUSEs
                                    where (a.SITEID == _siteId)
                                join b in Context.MPM_USERs on new { A = a.NPK }
                                                       equals new { A = b.NPK }
                                select new PICAreaRecord(
                                            a.PIC,
                                            a.NPK,
                                            a.SITEID,
                                            a.INITIALWAREHOUSEID,
                                            b.USER_NAME
                                        );

                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE= MPMDateUtil.DateTime;
                Context.MPMWMS_MAP_PIC_WAREHOUSEs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            try
            {
                Context.MPMWMS_MAP_PIC_WAREHOUSEs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public bool validasiDataUser(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            bool userValid = false;

            try
            {
                // validasi data NPK sudah ada atau belum
                var itemsUser = from a in Context.MPM_USERs
                                    where (a.NPK == item.NPK)
                                    select a.NPK;

                userValid = (itemsUser.Count() == 0) ? false : true;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }

            return userValid;
        }

        public bool validasiDataLocation(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            bool locationValid = false;

            try
            {
                // validasi data Location / Warehouse sudah sesuai atau tidak
                var itemsLocation = from a in Context.INVENTLOCATIONs
                                    where (a.INVENTSITEID == item.SITEID)
                                        && (a.INVENTSITEID == UserSession.SITE_ID_MAPPING)
                                        && (SqlMethods.Like(a.INVENTLOCATIONID, "%" + item.INITIALWAREHOUSEID + "%"))
                                    select a.RECID;

                locationValid = (itemsLocation.Count() == 0) ? false : true;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }

            return locationValid;
        }

        public List<PerformReportRecord> Report_PerfomancePIC(string _reportType, DateTime _beginDate, DateTime _endDate)
        {
		    var data = Context.MPMWMS_RPT_PIC_PERFORMANCE(_reportType, _beginDate, _endDate);
            
            List<PerformReportRecord> dataList = new List<PerformReportRecord>();

            foreach (var x in data.ToList())
            {
                dataList.Add(new PerformReportRecord
                               {
                                   TRANSDATE = x.TRANSDATE.Value
                                ,
                                   PIC = x.PIC
                                ,
                                   USER_NAME = x.USER_NAME
                                ,
                                   STARTSCAN = x.STARTSCAN.Value
                                ,
                                   ENDSCAN = x.ENDSCAN.Value
                                ,
                                   LEADTIME = x.LEADTIME
                                ,
                                   DATATARGET = x.DATATARGET
                                ,
                                   SO_COUNT = x.SO_COUNT
                                ,
                                   SUM_QTY = x.SUM_QTY
                                ,
                                   SUM_SO_LINE = x.SUM_SO_LINE
                                ,
                                   PERSEN = x.PERSEN.Value
                                ,
                                   LEMBUR = x.LEMBUR
                                ,
                                   OVERLINE = x.OVERLINE
                               });
            }

            return dataList.ToList();
        }

        public List<PICAreaReportRecord> dataHistoryPengerjaan
            (   
                  string _dataarea
                , string _site
                , string _maindealer
                , DateTime _beginDate
                , DateTime _endDate
            )
        {
            DateTime nullDate = new DateTime(1900,1,1);

            try
            {
                var itemsObj = from picHistory in Context.MPMWMS_MAP_PIC_HISTORies
                                   where (picHistory.DATAAREAID == _dataarea)
                                   && (picHistory.SITEID == _site)
                                   && (picHistory.MAINDEALERID == _maindealer)
                               join salesTable in Context.SALESTABLEs on new { A = picHistory.SOID, B = picHistory.DEALERID, C = picHistory.MAINDEALERID, D = picHistory.SITEID, E = picHistory.DATAAREAID }
                                                                  equals new { A = salesTable.SALESID, B = salesTable.CUSTACCOUNT, C = salesTable.XTSMAINDEALERCODE, D = salesTable.INVENTSITEID, E = salesTable.DATAAREAID }
                                                                  into temp1
                               from PicSales in temp1.DefaultIfEmpty()
                                    where (PicSales.XTSTRANSDATE >= _beginDate)
                                    && (PicSales.XTSTRANSDATE <= _endDate)
                               join pickingRoute in Context.WMSPICKINGROUTEs on new { A = PicSales.SALESID, B = PicSales.CUSTACCOUNT, C = PicSales.DATAAREAID }
                                                                  equals new { A = pickingRoute.TRANSREFID, B = pickingRoute.CUSTOMER, C = pickingRoute.DATAAREAID }
                                                                  into temp2
                               from PicPick in temp2.DefaultIfEmpty()
                               join custPack in Context.CUSTPACKINGSLIPJOURs on new { A = PicPick.TRANSREFID, B = PicPick.DATAAREAID }
                                                                  equals new { A = custPack.SALESID, B = custPack.DATAAREAID }
                                                                  into temp3
                               from Packing in temp3.DefaultIfEmpty()
                               select new PICAreaReportRecord(
                                         picHistory.DEALERID
                                       , picHistory.SOID
                                       , PicSales.XTSTRANSDATE.Date
                                       , (PicPick.ACTIVATIONDATETIME.Date != null) ? PicPick.ACTIVATIONDATETIME.Date : nullDate
                                       , (Packing.DELIVERYDATE.Date != null) ? Packing.DELIVERYDATE.Date : nullDate
                                       , picHistory.JOBID.ToString()
                                       , picHistory.PIC
                                       , getUserName(picHistory.PIC, _dataarea, _site, _maindealer)
                                       , picHistory.PICTRANSPORTER
                                       , getUserName(picHistory.PICTRANSPORTER, _dataarea, _site, _maindealer)
                                       , picHistory.PICCHECKER
                                       , getUserName(picHistory.PICCHECKER, _dataarea, _site, _maindealer)
                                  );

                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //public List<PerformReportRecord> dataPerformPicker
        //    (
        //          string _dataarea
        //        , string _site
        //        , string _maindealer
        //        , DateTime _beginDate
        //        , DateTime _endDate
        //    )
        //{
        //    try
        //    {
        //        var data = from picHistory in Context.MPMWMS_MAP_PIC_HISTORies
        //                       where (picHistory.DATAAREAID == _dataarea)
        //                       && (picHistory.SITEID == _site)
        //                       && (picHistory.MAINDEALERID == _maindealer)
        //                   join user in Context.MPM_USERs on new { A = picHistory.PIC, B = picHistory.SITEID }
        //                                              equals new { A = user.NPK, B = user.SITE_ID_MAPPING }
        //                   join target in Context.MPM_SETTING_DETAILs on new { A = user.SITE_ID_MAPPING }
        //                                                          equals new { A = target.KEY_TAG_1 }
        //                        where target.KEY_TAG_2 == "PICKER"
        //                   select new PerformReportRecord
        //                   (
        //                         picHistory.STARTSCAN.Value.Date
        //                       , picHistory.PIC
        //                       , user.USER_NAME
        //                       , picHistory.STARTSCAN.Value.Date
        //                       , picHistory.ENDSCAN.Value.Date
        //                       , (picHistory.ENDSCAN.Value.Date - picHistory.STARTSCAN.Value.Date).ToString()
        //                       , target.TAG_1
        //                       , 0 // sum so line
        //                       , 0 // count so
        //                       , 0 // sum qty
        //                       , 0 // target / leadtime
        //                       , 0 // jam lembur
        //                   );

        //        return data.ToList();
        //    }           
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        private string getUserName(string _NPK, string _dataarea, string _site, string _maindealer)
        {
            try
            {
                string namaUser = "";

                var data = (from user in Context.MPM_USERs
                            where (user.NPK == _NPK)
                               && (user.MAIN_DEALER_ID == _maindealer)
                               && (user.DATAAREA_ID == _dataarea)
                               && (user.SITE_ID_MAPPING == _site)
                            select user).Take(1);
                
                foreach (var x in data)
                {
                    namaUser = x.USER_NAME;
                }

                return namaUser;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MonitoringSO_Record> dataTableMonitoringSO(DateTime _soDate)
        {
            String QueryitemsObj =
                "SELECT SO_NOTDONE = COALESCE(SUM(CASE WHEN SALESSTATUS < 3 THEN 1 ELSE 0 END), 0) " +
                ", SO_DONE = COALESCE(SUM(CASE WHEN SALESSTATUS = 3 THEN 1 ELSE 0 END), 0) " +
                ", SO_TOTAL = COUNT(T.SALESID) " +
                "FROM SALESTABLE (NOLOCK) T " +
                "WHERE T.XTSTRANSDATE = CONVERT(DATE, '" + _soDate + "') " +
                "AND T.XTSCATEGORYCODE = 'P' " +
                "AND T.SALESTYPE = 3 " +
                "AND T.SALESSTATUS < 4 ";

            return Context.ExecuteQuery<MonitoringSO_Record>(QueryitemsObj).ToList();
        }

        public List<CancelTrolley_Record> getTransporterTrolleyCanCanceled(String _siteId)
        {
            String QueryitemsObj =
                " SELECT DISTINCT TROLLEYNBR = R.XTSTROLLEYNBR, P.JOBID                                                  " +
                " , STATUSTRANSPORTER = CASE O.MPMSTATUSTRANSPORTER WHEN 1 THEN 'SUDAH DIAMBIL' ELSE 'BELUM DIAMBIL' END " +
                " FROM MPMWMS_MAP_SO_PICAREA P                                                                           " +
                " JOIN WMSORDERTRANS O ON P.SOID = O.INVENTTRANSREFID                                                    " +
                " JOIN WMSPICKINGROUTE R ON O.ROUTEID = R.PICKINGROUTEID AND O.INVENTTRANSREFID = R.TRANSREFID           " +
                " WHERE P.ISDONEPICKING = 'Y'                                                                            " +
                " AND O.MPMSTATUSPICKING < 2                                                                             " +
                " AND P.SITEID = '" + _siteId + "'                                                                       " +
                " ORDER BY R.XTSTROLLEYNBR, P.JOBID ASC                                                                  ";

            return Context.ExecuteQuery<CancelTrolley_Record>(QueryitemsObj).ToList();
        }

        public void prosesCancelTrolley(String _siteId)
        {
            String QueryitemsObj = "";
        }

        public List<GrafikData_Record> dataGrafikMonitoringSO(DateTime _soDate, String _siteId)
        {
            var data = Context.MPMWMS_RPT_MONITORING_SO(_siteId, _soDate);

            List<GrafikData_Record> dataList = new List<GrafikData_Record>();

            foreach (var x in data.ToList())
            {
                dataList.Add(new GrafikData_Record
                            {
                                KATEGORI = x.KATEGORI,
                                VALUE = x.VALUE,
                                VALUEMTD = x.VALUEMTD
                            });
            }

            return dataList.ToList();
        }

        public List<MonitoringTargetActual_Record> dataMonitoringTargetActualPIC(DateTime _soDate)
        {
            //String QueryitemsObj =
            //    "EXEC [MPMWMS_MONITORING_TARGET_ACTUAL_PIC] '" + _soDate + "' ";

            //return Context.ExecuteQuery<MonitoringTargetActual_Record>(QueryitemsObj).ToList();

            var data = Context.MPMWMS_MONITORING_TARGET_ACTUAL_PIC(_soDate);

            List<MonitoringTargetActual_Record> dataList = new List<MonitoringTargetActual_Record>();

            foreach (var x in data.ToList())
            {
                dataList.Add(new MonitoringTargetActual_Record
                                {
                                    NPK = x.NPK
                                    ,
                                    USER_NAME = x.USER_NAME
                                    ,
                                    POSITION = x.POSITION
                                    ,
                                    TARGETSO = x.TARGETSO
                                    ,
                                    ACTUALSO = x.ACTUALSO
                                    ,
                                    TARGETLINESO = x.TARGETLINESO
                                    ,
                                    ACTUALLINESO = x.ACTUALLINESO
                                    ,
                                    TARGETQTYSO = x.TARGETQTYSO
                                    ,
                                    ACTUALQTYSO = x.ACTUALQTYSO
                                });
            }

            return dataList.ToList();
        }
    }
}
