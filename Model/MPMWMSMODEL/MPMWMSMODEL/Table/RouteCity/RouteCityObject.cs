﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Lov.City;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteCity
{
    public class LIST_ROUTE_CITY
    {
        public int INDEX { get; set; }
        public String MAINDEALERID { get; set; }
        public String ROUTEID { get; set; }
        public String SITEID { get; set; }
        public String CITY { get; set; }
        public Int64 CITYRECID { get; set; }

        public LIST_ROUTE_CITY
        (
              int       _INDEX
            , String    _MAINDEALERID
            , String    _ROUTEID
            , String    _SITEID
            , String    _CITY
            , Int64     _CITYRECID
        )
        {
            INDEX = _INDEX;
            MAINDEALERID = _MAINDEALERID;
            ROUTEID = _ROUTEID;
            SITEID = _SITEID;
            CITY = _CITY;
            CITYRECID = _CITYRECID;
        }
    }

    public class RouteCityObject : MPMDbObject<WmsUnitDataContext>
    {
        public RouteCityObject()
            : base()
        {
        }
        
        public void Update(MPMWMS_ROUTE_CITY item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_ROUTE_CITY item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMS_ROUTE_CITies.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_ROUTE_CITY item)
        {
            try
            {
                Context.MPMWMS_ROUTE_CITies.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<LIST_ROUTE_CITY> ListAll(String _dataArea, String _mainDealer, String _siteId, String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTE_CITies
                               join b in Context.LOGISTICSADDRESSCOUNTies on a.CITYRECID equals b.RECID
                               where (a.DATAAREAID == _dataArea)
                               && (a.MAINDEALERID == _mainDealer)
                               && (a.SITEID == _siteId)
                               && (a.ROUTEID == _routeId)
                               select new LIST_ROUTE_CITY
                               ( 
                                     a.INDEX
                                   , a.MAINDEALERID
                                   , a.ROUTEID
                                   , a.SITEID
                                   , b.NAME
                                   , b.RECID
                               );
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public LIST_ROUTE_CITY ListByIndex(String _dataArea, String _mainDealer, String _siteId, String _routeId, int _index)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTE_CITies
                               join b in Context.LOGISTICSADDRESSCOUNTies on a.CITYRECID equals b.RECID
                               where (a.DATAAREAID == _dataArea)
                               && (a.MAINDEALERID == _mainDealer)
                               && (a.SITEID == _siteId)
                               && (a.ROUTEID == _routeId)
                               && (a.INDEX == _index)
                               select new LIST_ROUTE_CITY
                               (
                                     a.INDEX
                                   , a.MAINDEALERID
                                   , a.ROUTEID
                                   , a.SITEID
                                   , b.NAME
                                   , b.RECID
                               );
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_ROUTE_CITY Item(String _dataArea, String _mainDealer, String _siteId, String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTE_CITies
                               where (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                                && (a.ROUTEID == _routeId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_ROUTE_CITY ItemByIndex(String _dataArea, String _mainDealer, String _siteId, String _routeId, int _index)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTE_CITies
                               where (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                                && (a.ROUTEID == _routeId)
                                && (a.INDEX == _index)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<LovCity> LovCity(String _dataArea)
        {
            try
            {
                LovCityObject lovCityObj = new LovCityObject();

                return lovCityObj.List(_dataArea);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

    }
}
