﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.QCUnitNRFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InfoKerusakanUnit
{
    public class InfoKerusakanUnit_Obj : MPMDbObject<WmsUnitDataContext>
    {
        

        public InfoKerusakanUnit_Obj() : base()
        {
            Context = new WmsUnitDataContext();
        }

        public List<Unit_Rec> ListItem(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.MACHINE_ID == NoMesin && a.STATUS == "C"
                             select new Unit_Rec
                             {
                                 NoUnit = a.MACHINE_ID
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<Unit_Rec> ListValidLKU(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             where a.SRMESIN + a.NOMESIN == NoMesin /*a.NOMERMESIN.Replace(" ", "").Contains(NoMesin)*/
                             select new Unit_Rec
                             {
                                 NoUnit = a.NOMERMESIN
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public List<Unit_Rec> ListValidNONLKU(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             where a.NOMESIN.Contains(NoMesin)
                             select new Unit_Rec
                             {
                                 NoUnit = a.NOMESIN
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public List<DetilUnit_Rec> ListDataNRFS(string NoMesin)
        {
            try
            {
                Context.CommandTimeout = 5000000;
                var data = (from a in ((WmsUnitDataContext)Context).MPMWMS_HISTORY_UNITs
                            where a.WAREHOUSE_ID.Contains("NRFS") && !a.WAREHOUSE_ID_OLD.Contains("NRFS")
                            && a.MACHINE_ID == NoMesin
                            orderby a.CREATE_DATE descending
                            select new QCHistory
                            {
                                No = a.MACHINE_ID,
                                tanggalnrfs = a.CHANGES_DATE.Date.ToString()
                            }).Take(1).FirstOrDefault();
                if (data == null)
                {
                    QCHistory history = new QCHistory();
                    history.No = NoMesin;
                    history.tanggalnrfs = "";
                    List<QCHistory> qc = new List<QCHistory>();
                    qc.Add(history);
                    data = qc.Take(1).FirstOrDefault();
                }

                var result = from c in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             where c.NOMESIN.Contains(NoMesin)
                             group c by new { c.NOMESIN, c.USERID, c.STATUSKERJA, c.TGLJAMMULAI, c.TGLJAMSELESAI, c.TYPEPERBAIKAN } into g
                             select new DetilUnit_Rec
                             {
                                 TglLku = data.tanggalnrfs,
                                 Status = g.Key.STATUSKERJA == 0 ? "Open" : g.Key.STATUSKERJA == 1 ? "On Progress" : g.Key.STATUSKERJA == 2 ? "Pending" : "Done",
                                 Teknisi = g.Key.USERID,
                                 Unit = g.Key.NOMESIN,
                                 TypeRepair = g.Key.TYPEPERBAIKAN
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<ListDetil_Rec> ListDetaiDataNonLKU(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMDETAILNONLKUs
                             join b in ((WmsUnitDataContext)Context).MPMSERVVIEWDAFTARPARTs
                             on a.ITEM equals b.ITEMID into gj
                             from sub in gj.DefaultIfEmpty()
                             where a.NOMESIN.Contains(NoMesin)
                             select new ListDetil_Rec
                             {
                                 PartName = a.ITEM,
                                 ItemPair = a.TYPEREPAIR == "Bongkar Part" ? sub.ITEMNAME : "-",
                                 KodePart = a.TYPEREPAIR
                             };
                return result.AsEnumerable() // Client-side from here on
                    .Select((player, index) => new ListDetil_Rec()
                    {
                        PartName = player.PartName,
                        ItemPair = player.ItemPair,
                        KodePart = player.KodePart,
                        No = index + 1
                    })
                    .ToList(); ;
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<ListDetil_Rec> ListDetaiData(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             where a.NOMERMESIN.Replace(" ", "").Contains(NoMesin)
                             select new ListDetil_Rec
                             {
                                 PartName = a.PARTNAME,
                                 ItemPair = a.ITEMREPAIR,
                                 KodePart = a.KODEPART
                             };
                return result.AsEnumerable() // Client-side from here on
                    .Select((player, index) => new ListDetil_Rec()
                    {
                        PartName = player.PartName,
                        ItemPair = player.ItemPair,
                        KodePart = player.KodePart,
                        No = index + 1
                    })
                    .ToList(); ;
            }
            catch (MPMException)
            {
                return null;
            }
        }


        public List<DetilUnit_Rec> ListData(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKUHEADERs
                             join b in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             on new { MPMSALLKUHEADER = a.RECID, a.DATAAREAID, a.PARTITION } equals new { b.MPMSALLKUHEADER, b.DATAAREAID, b.PARTITION }
                             join c in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             on b.NOMERMESIN.Replace(" ", "") equals c.NOMESIN
                             where b.NOMERMESIN.Replace(" ", "").Contains(NoMesin)
                             group new { a, c } by new { a.TGLLKU, c.NOMESIN, c.USERID, c.STATUSKERJA , c.TYPEPERBAIKAN} into g
                             select new DetilUnit_Rec
                             {
                                 TglLku = g.Key.TGLLKU.ToShortDateString(),
                                 Status = g.Key.STATUSKERJA == 0 ? "Open" : g.Key.STATUSKERJA == 1 ? "On Progress" : g.Key.STATUSKERJA == 2 ? "Pending" : "Done",
                                 Teknisi = g.Key.USERID,
                                 Unit = g.Key.NOMESIN,
                                 TypeRepair = g.Key.TYPEPERBAIKAN
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

    }
}
