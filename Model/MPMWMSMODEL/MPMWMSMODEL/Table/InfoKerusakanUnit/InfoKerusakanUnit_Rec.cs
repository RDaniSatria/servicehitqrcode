﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InfoKerusakanUnit
{
    public class InfoKerusakanUnit_Rec
    {

    }
    public class Unit_Rec
    {
        public string NoUnit { set; get; }
    }

    public class DetilUnit_Rec
    {
        public string Unit { set; get; }
        public string TglLku { set; get; }
        public string Teknisi { set; get; }
        public string Status { set; get; }
        public string TypeRepair { set; get; }
    }

    public class ListDetil_Rec
    {
        public int No { set; get; }
        public string KodePart { set; get; }
        public string PartName { set; get; }
        public string ItemPair { set; get; }
    }
}
