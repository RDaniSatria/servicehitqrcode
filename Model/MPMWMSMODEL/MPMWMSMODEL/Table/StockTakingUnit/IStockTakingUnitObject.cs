﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StockTakingUnit
{
    public interface IStockTakingUnitObject
    {
        List<MPMWMS_STOCK_TAKING_UNIT> List();
        MPMWMS_STOCK_TAKING_UNIT Create();
        void Insert(MPMWMS_STOCK_TAKING_UNIT row);
    }
}
