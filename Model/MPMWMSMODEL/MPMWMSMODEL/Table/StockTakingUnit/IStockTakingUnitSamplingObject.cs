﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.StockTakingUnit
{
    public interface IStockTakingUnitSamplingObject
    {
        List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> List();
        List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> List(String stock_taking_id);
        MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA Create();
        void Insert(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row);
        void Update(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row);
        void Delete(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row);
    }
}
