﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.StockTakingUnit
{
    public class StockTakingUnitReplicateObject : MPMDbObject<WmsUnitDataContext>
    {
        public MPMWMS_STOCK_TAKING_UNIT_REPLICATE Item(String dataarea_id, String machine_or_frame_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_STOCK_TAKING_UNIT_REPLICATEs
                               where
                               (
                                   "MH1" + unit.FRAME_ID == machine_or_frame_id ||
                                   unit.MACHINE_ID == machine_or_frame_id ||
                                   "MLH" + unit.FRAME_ID == machine_or_frame_id ||
                                   unit.FRAME_ID == machine_or_frame_id
                               ) &&
                               (unit.DATAAREA_ID == dataarea_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
