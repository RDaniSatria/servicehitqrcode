﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StockTakingUnit
{
    public class StockTakingUnitObject : MPMDbObject<WmsUnitDataContext>, IStockTakingUnitObject
    {
        public class MPMWMS_STOCK_TAKING_GROUP_UNIT_RECORD
        {
            public String WAREHOUSE_ID { get; set; }
            public String UNIT_TYPE { get; set; }
            public String COLOR_TYPE { get; set; }
            public int COUNT { get; set; }

            public MPMWMS_STOCK_TAKING_GROUP_UNIT_RECORD(String warehouse_id, String unit_type, String color_type,
                int count)
            {
                WAREHOUSE_ID = warehouse_id;
                UNIT_TYPE = unit_type;
                COLOR_TYPE = color_type;
                COUNT = count;
            }
        }

        public StockTakingUnitObject()
            : base()
        {
        }

        public List<MPMWMS_STOCK_TAKING_UNIT> List()
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNITs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               select stock;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_STOCK_TAKING_UNIT> List(String stock_taking_id)
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNITs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (stock.STOCK_TAKING_ID == stock_taking_id)
                               select stock;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_STOCK_TAKING_GROUP_UNIT_RECORD> ListGroupUnitType(String stock_taking_id)
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNITs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (stock.STOCK_TAKING_ID == stock_taking_id)
                               group stock by new { stock.WAREHOUSE_ID, stock.UNIT_TYPE, stock.COLOR_TYPE } into g_d
                               select new MPMWMS_STOCK_TAKING_GROUP_UNIT_RECORD
                               (
                                   g_d.Key.WAREHOUSE_ID,
                                   g_d.Key.UNIT_TYPE,
                                   g_d.Key.COLOR_TYPE,
                                   g_d.Count()
                               );
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_STOCK_TAKING_UNIT Create()
        {
            MPMWMS_STOCK_TAKING_UNIT item = new MPMWMS_STOCK_TAKING_UNIT();
            return item;
        }

        public void Insert(MPMWMS_STOCK_TAKING_UNIT row)
        { 
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_STOCK_TAKING_UNITs.InsertOnSubmit(row);
        }

        public MPMWMS_STOCK_TAKING_UNIT Item(String stock_taking_id, String machine_id, String frame_id, String location_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_STOCK_TAKING_UNITs
                               where (unit.FRAME_ID == frame_id) &&
                               (unit.MACHINE_ID == machine_id) &&
                               (unit.DATAAREA_ID == UserSession.DATAAREA_ID) &&
                               (unit.SITE_ID == UserSession.SITE_ID_MAPPING) &&
                               (unit.LOCATION_ID == location_id) &&
                               (unit.STOCK_TAKING_ID == stock_taking_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_STOCK_TAKING_UNIT row)
        {
            try
            {
                // delete
                Context.MPMWMS_STOCK_TAKING_UNITs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
