﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.StockTakingUnit
{
    public class StockTakingUnitSamplingObject : MPMDbObject<WmsUnitDataContext>, IStockTakingUnitSamplingObject
    {
        public StockTakingUnitSamplingObject()
            : base()
        {
        }

        public List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> List()
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               select stock;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> List(String stock_taking_id)
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (stock.STOCK_TAKING_ID == stock_taking_id)
                               select stock;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA Item(
            String stock_taking_id, String dataarea_id, String site_id)
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                               where
                               (stock.DATAAREA_ID == dataarea_id)
                               && (stock.SITE_ID == site_id)
                               && (stock.STOCK_TAKING_ID == stock_taking_id)
                               select stock;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA Item(
            String stock_taking_id, String warehouse_id, String unit_type, String color_type)
        {
            try
            {
                var itemsObj = from stock in Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                               where
                               (stock.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (stock.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (stock.STOCK_TAKING_ID == stock_taking_id)
                               && (stock.UNIT_TYPE == unit_type)
                               && (stock.COLOR_TYPE == color_type)
                               select stock;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA FilterByCriteria(
            String stock_taking_id, String dataarea_id, String site_id, String warehouse_id,
            String unit_type, String color_type)
        {
            try
            {
                var itemsObj = 
                    from stock in Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                    where
                        (stock.DATAAREA_ID == dataarea_id)
                        && (stock.SITE_ID == site_id)
                        && (stock.STOCK_TAKING_ID == stock_taking_id)
                        && (stock.WAREHOUSE_ID == warehouse_id || stock.WAREHOUSE_ID == "ALL")
                        && (stock.UNIT_TYPE == unit_type || stock.UNIT_TYPE == "ALL")
                        && (stock.COLOR_TYPE == color_type || stock.COLOR_TYPE == "ALL")
                    select stock;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA Create()
        {
            MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA item = new MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA();
            return item;
        }

        public void Insert(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row)
        { 
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void Delete(MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA row)
        {
            Context.MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs.DeleteOnSubmit(row);
        }
    }
}
