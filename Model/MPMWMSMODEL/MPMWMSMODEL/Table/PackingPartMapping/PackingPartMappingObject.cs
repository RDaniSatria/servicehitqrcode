﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.PackingPartMapping
{
    public class PackingPartMappingObject : MPMDbObject<WmsUnitDataContext>, IPackingPartMappingObject
    {
        public PackingPartMappingObject()
            : base()
        {
        }

        public List<MPMWMS_PACKING_PART_MAPPING> List(String trolley_id)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_PACKING_PART_MAPPINGs
                               where
                               (a.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (a.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (a.MAINDEALER_ID == UserSession.MAINDEALER_ID)
                               && (a.TROLLEY_ID == trolley_id)
                               && (a.USER_ID == UserSession.NPK)
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_PACKING_PART_MAPPING Create()
        {
            MPMWMS_PACKING_PART_MAPPING item = new MPMWMS_PACKING_PART_MAPPING();
            item.USER_ID = UserSession.NPK;
            return item;
        }

        public void Insert(MPMWMS_PACKING_PART_MAPPING row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_PACKING_PART_MAPPINGs.InsertOnSubmit(row);
        }

        public void Delete(MPMWMS_PACKING_PART_MAPPING row)
        {
            try
            {
                Context.MPMWMS_PACKING_PART_MAPPINGs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
