﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.PackingPartMapping
{
    public interface IPackingPartMappingObject
    {
        List<MPMWMS_PACKING_PART_MAPPING> List(String trolley_id);
        MPMWMS_PACKING_PART_MAPPING Create();
        void Insert(MPMWMS_PACKING_PART_MAPPING row);
        void Delete(MPMWMS_PACKING_PART_MAPPING row);
    }
}
