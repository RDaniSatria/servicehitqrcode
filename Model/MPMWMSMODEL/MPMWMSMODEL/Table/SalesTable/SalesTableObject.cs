﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.SalesLine;
using MPMWMSMODEL.View.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.SalesTable
{
    public class SalesTableObject : MPMDbObject<WmsUnitDataContext>, ISalesTableObject
    {

        private SalesLineObject _SalesLineObject = new SalesLineObject();

        public SalesTableObject()
            : base()
        {
        }

        public SalesLineObject SalesLineObject
        {
            get { return _SalesLineObject; }
        }

        public List<SALESTABLE> List()
        {
            return Context.SALESTABLEs.ToList();
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).SALESTABLEs);
        }

        public SALESTABLE Item(string sales_id, string dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from salestable in Context.SALESTABLEs
                                where (salestable.SALESID == sales_id)
                                && (salestable.DATAAREAID == dataarea_id)
                                && (salestable.XTSMAINDEALERCODE == maindealer_id)
                                && (salestable.INVENTSITEID == site_id)
                                select salestable;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public SALESTABLE Item(string sales_id)
        {
            try
            {
                var itemsObj = from salestable in Context.SALESTABLEs
                               where (salestable.SALESID == sales_id)
                               select salestable;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
