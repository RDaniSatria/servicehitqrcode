﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SalesTable
{
    public interface ISalesTableObject
    {
        List<SALESTABLE> List();
        SALESTABLE Item(string sales_id, string dataarea_id, String maindealer_id, String site_id);
        SALESTABLE Item(string sales_id);
        void Refresh();
    }
}
