﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SalesTable
{
    public class SALESTABLE_AV_PICK
    {
        public String ISINTERNAL
        {
            get;
            set;
        }

        public String DOID
        {
            get;
            set;
        }

        public String DATAAREAID
        {
            get;
            set;
        }

        public String MAINDEALERID
        {
            get;
            set;
        }

        public String SITEID
        {
            get;
            set;
        }

        public String SALESNAME
        {
            get;
            set;
        }

        public String CUSTACCOUNT
        {
            get;
            set;
        }

        public String INVOICEACCOUNT
        {
            get;
            set;
        }

        public String DEALER_NAME
        {
            get;
            set;
        }

        public String DEALER_ADDRESS
        {
            get;
            set;
        }

        public String DEALER_CITY
        {
            get;
            set;
        }

        public String DELIVERYNAME
        {
            get;
            set;
        }

        public DateTime TRANSDATE
        {
            get;
            set;
        }

        public int QUANTITY_DO
        {
            get;
            set;
        }

        public int QUANTITY_PICK
        {
            get;
            set;
        }

        public int QUANTITY_SL
        {
            get;
            set;
        }

        public SALESTABLE_AV_PICK(String IsInternal, String DOId,
            String DataAreaId,
            String MainDealerId,
            String SiteId,
            String SalesName,
            String CustAccount,
            String DealerName,
            String DealerAddress,
            String DealerCity,
            String DeliveryName,
            DateTime TransDate,
            int QuantityDO,
            int QuantityPick,
            int QuantitySL)
        {
            ISINTERNAL = IsInternal;
            DOID = DOId;
            DATAAREAID = DataAreaId;
            MAINDEALERID = MainDealerId;
            SITEID = SiteId;
            CUSTACCOUNT = CustAccount;
            INVOICEACCOUNT = CustAccount;
            DEALER_NAME = DealerName;
            DEALER_ADDRESS = DealerAddress;
            DEALER_CITY = DealerCity;
            DELIVERYNAME = DeliveryName;
            TRANSDATE = TransDate;
            QUANTITY_DO = QuantityDO;
            QUANTITY_PICK = QuantityPick;
            QUANTITY_SL = QuantitySL;
        }
    }
}
