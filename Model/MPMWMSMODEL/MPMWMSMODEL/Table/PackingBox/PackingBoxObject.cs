﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.BoxProperty;

namespace MPMWMSMODEL.Table.PackingBox
{
    public class TransMonitoringBox
    {
        public String BOXID { get; set; }
        public String DEALERCODE { get; set; }
        public String PACKINGID { get; set; }
        public String PICKINGID { get; set; }
        public String PARTNO { get; set; }
        public int QUANTITY { get; set; }
        public String PIC { get; set; }
        public String SALESID { get; set; }


        public TransMonitoringBox()
        {
            
        }
    }

    public class HEADER_MONITORING_BOX_PDT
    {
        public String BOXID { get; set; }
        public String DEALERCODE { get; set; }
        public String PACKINGID { get; set; }
        public String PICKINGID { get; set; }
        public String SALESID { get; set; }
        public String BERAT { get; set; }
        
        public List<DETAIL_PART_BOX_PDT> DETAIL_PART { get; set; }
    }

    public class DETAIL_PART_BOX_PDT
    {
        public String PARTNO { get; set; }
        public int QUANTITY { get; set; }
        public String PIC { get; set; }
    }

    public class TransMonitoringBoxPDT
    {
        public String BOXID { get; set; }
        public List<HEADER_MONITORING_BOX_PDT> LIST_PER_BOX { get; set; }
    }

    public class TRANS_PRINTLABELBOX
    {
        public String SALESID {get;set;}
        public String WEIGHT {get;set;}
        public String TOTALBOX {get;set;}
        public String PIC {get;set;}
        public String TGLPACKING {get;set;}
        public String KOTA { get; set; }

    }

    public class PackingBoxObject : MPMDbObject<WmsUnitDataContext>
    {
        public List<TRANS_PRINTLABELBOX> Label(String boxId)
        {
            List<TRANS_PRINTLABELBOX> ListPrintLabel = new List<TRANS_PRINTLABELBOX>();

            //TRANS_PRINTLABELBOX temp = new TRANS_PRINTLABELBOX();

            var search_header =
              from result in
                  (
                      from a in Context.MPMWMS_PACKINGBOXes
                      join b in Context.CUSTPACKINGSLIPJOURs
                      on new { a.PACKINGID } equals new { PACKINGID = b.PACKINGSLIPID }
                      join c in Context.CUSTINVOICEJOURs
                      on new { b.DATAAREAID, b.SALESID } equals new { c.DATAAREAID, c.SALESID }
                      join d in Context.DIRPARTYPOSTALADDRESSVIEWs
                      on new { DELIVERYPOSTALADDRESS = c.DELIVERYPOSTALADDRESS } equals new { DELIVERYPOSTALADDRESS = (long)d.POSTALADDRESS }
                      where
                      a.BOXID == boxId
                      select new
                      {
                          c.SALESID,
                          d.CITY,
                          b.DELIVERYDATE,
                          a.CREATE_BY
                      }
                  )
              group result by new { result.SALESID, result.CITY, result.DELIVERYDATE, result.CREATE_BY } into group_result
              select new
              {
                  SALESID = group_result.Key.SALESID,
                  CITY = group_result.Key.CITY,
                  DELIVERYDATE = group_result.Key.DELIVERYDATE,
                  CREATE_BY = group_result.Key.CREATE_BY
              };

            
            
            foreach (var item_header in search_header.OrderBy(a=>a.SALESID).ThenBy(a=>a.DELIVERYDATE).ThenBy(a=>a.CREATE_BY))
            {
                TRANS_PRINTLABELBOX temp = new TRANS_PRINTLABELBOX();
                temp.SALESID = item_header.SALESID.Substring(17,item_header.SALESID.Length - 17) ;

                if (item_header.CITY != "")
                {
                    if (item_header.CITY.Length > 13)
                    {
                        temp.KOTA = item_header.CITY.Substring(0, 13);
                    }
                    else
                        temp.KOTA = item_header.CITY;
                }
                else
                    temp.KOTA = item_header.CITY;
                
                temp.TGLPACKING = item_header.DELIVERYDATE.ToString("dd-MMM-yyyy");
                temp.PIC = item_header.CREATE_BY;

                temp.TOTALBOX = TotalBox(boxId).ToString();
                temp.WEIGHT = "0";

                ListPrintLabel.Add(temp);
            }

            return ListPrintLabel;
        }

        public int TotalBox(String boxId)
        {
            var search = 
                from result in
                (
                         from a in Context.MPMWMS_PACKINGBOXes
                         where
                         a.BOXID == boxId
                         select new
                         {
                            BOXID = a.BOXID,
                            PACKINGID = a.PACKINGID
                         }
                 )
                group result by new { result.BOXID, result.PACKINGID } into group_result
                select new
                {
                    JUMLAH = group_result.Count()
                };

            int jumlah_box = 0;
            jumlah_box = search.Count();

            return jumlah_box;
        }

        public TransMonitoringBoxPDT ListMonitoringBoxPDT(String boxId)
        {
            TransMonitoringBoxPDT _TransMonitoringBoxPDT = new TransMonitoringBoxPDT();
            _TransMonitoringBoxPDT.BOXID = boxId;

            var search_header =
              from result in
              (
                  from a in Context.MPMWMS_PACKINGBOXes
                  join b in Context.CUSTPACKINGSLIPJOURs
                  on new { a.PACKINGID } equals new { PACKINGID = b.PACKINGSLIPID }
                  join c in Context.SALESTABLEs
                  on new {  b.SALESID } equals new { c.SALESID}
                  where
                  a.BOXID == boxId &&
                  c.INVENTSITEID == UserSession.SITE_ID_MAPPING
                  select new
                  {
                      b.INVOICEACCOUNT,
                      b.PACKINGSLIPID,
                      b.XTSPICKINGROUTEID,
                      c.SALESID
                  }
              ) 
              group result by new {result.INVOICEACCOUNT, result.PACKINGSLIPID, result.XTSPICKINGROUTEID, result.SALESID} into group_result
              select new
              {
                  INVOICEACCOUNT = group_result.Key.INVOICEACCOUNT,
                  PACKINGSLIPID = group_result.Key.PACKINGSLIPID,
                  XTSPICKINGROUTEID = group_result.Key.XTSPICKINGROUTEID,
                  SALESID = group_result.Key.SALESID
                  
              };

            //mencari berat
            BoxPropertyObject _BoxPropertyObject = new BoxPropertyObject();
            _BoxPropertyObject.Context = this.Context;

            MPMWMS_BOXPROPERTy item_box_property = _BoxPropertyObject.item(boxId);
            String berat = "0";
            if (item_box_property != null)
                berat = item_box_property.WEIGHT.ToString();


            List<HEADER_MONITORING_BOX_PDT> _HEADER_MONITORING_BOX_PDT_LIST = new List<HEADER_MONITORING_BOX_PDT>();
            foreach (var item_header in search_header)
            {

                HEADER_MONITORING_BOX_PDT _HEADER_MONITORING_BOX_PDT = new HEADER_MONITORING_BOX_PDT();
                _HEADER_MONITORING_BOX_PDT.BOXID = boxId;
                _HEADER_MONITORING_BOX_PDT.DEALERCODE = item_header.INVOICEACCOUNT;
                _HEADER_MONITORING_BOX_PDT.PACKINGID = item_header.PACKINGSLIPID;
                _HEADER_MONITORING_BOX_PDT.PICKINGID = item_header.XTSPICKINGROUTEID;
                _HEADER_MONITORING_BOX_PDT.SALESID = item_header.SALESID;
                _HEADER_MONITORING_BOX_PDT.BERAT = berat;

                //cari detail part
                var search_detail = from a in Context.MPMWMS_PACKINGBOXes
                                    where
                                    a.DEALERCODE == item_header.INVOICEACCOUNT &&
                                    a.PACKINGID == item_header.PACKINGSLIPID &&
                                    a.BOXID == boxId
                                    select new
                                    {
                                        a.PARTNO,
                                        a.QUANTITY,
                                        a.CREATE_BY
                                    };

                List<DETAIL_PART_BOX_PDT> _DETAIL_PART_BOX_PDT_LIST = new List<DETAIL_PART_BOX_PDT>();
                foreach (var item_detail in search_detail)
                {
                    DETAIL_PART_BOX_PDT _DETAIL_PART_BOX_PDT = new DETAIL_PART_BOX_PDT();
                    _DETAIL_PART_BOX_PDT.PARTNO = item_detail.PARTNO;
                    _DETAIL_PART_BOX_PDT.QUANTITY = item_detail.QUANTITY;
                    _DETAIL_PART_BOX_PDT.PIC = item_detail.CREATE_BY;

                    _DETAIL_PART_BOX_PDT_LIST.Add(_DETAIL_PART_BOX_PDT);
                    
                }

                _HEADER_MONITORING_BOX_PDT.DETAIL_PART = _DETAIL_PART_BOX_PDT_LIST;


                _HEADER_MONITORING_BOX_PDT_LIST.Add(_HEADER_MONITORING_BOX_PDT);
                
            }

            _TransMonitoringBoxPDT.LIST_PER_BOX = _HEADER_MONITORING_BOX_PDT_LIST;

            return _TransMonitoringBoxPDT;
        }

        public bool IsAvailableIsiBoxPDT(String boxId)
        {
            var search =
                from line in Context.MPMWMS_PACKINGBOXes
                group line by new { line.BOXID } into group_box
                where
                    group_box.Key.BOXID == boxId
                select new
                {
                    BOXID = group_box.Key.BOXID
                };

            if (search.Any())
                return true;
            else
                return false;
        }

        public bool isInvoicedBox(String boxId)
        {
            var search =
                        from line in Context.MPMWMS_PACKINGBOXes
                        join custjour in Context.CUSTPACKINGSLIPJOURs
                        on new { PACKINGID = line.PACKINGID } equals new { PACKINGID = custjour.PACKINGSLIPID }
                        join saltable in Context.SALESTABLEs
                        on new { SALESID = custjour.SALESID } equals new { SALESID = saltable.SALESID }
                        where
                            line.BOXID == boxId &&
                            saltable.SALESSTATUS == 3
                        select new
                        {
                            CREATE_DATE = line.CREATE_DATE,
                            DEALERCODE = line.DEALERCODE
                        };

            if (search.Any())
            {
                return true;
            }
            else
                return false;

        }

        public bool isPostingBox(String boxId)
        {
            var search =
                        from line in Context.MPMWMS_PACKINGBOXes
                        join custtrans in Context.CUSTPACKINGSLIPTRANs
                        on new { PACKINGID = line.PACKINGID, PARTNO = line.PARTNO } equals new { PACKINGID = custtrans.PACKINGSLIPID, PARTNO = custtrans.ITEMID }
                        where
                            line.BOXID == boxId &&
                            custtrans.XTSISPACKED == 2
                        select new
                        {
                            CREATE_DATE = line.CREATE_DATE,
                            DEALERCODE = line.DEALERCODE
                        };

            if (search.Any())
            {
                return true;
            }
            else
                return false;

        }


        public bool isValidBox(String boxId, String dealerCode)
        {
            var search =
                           from line in Context.MPMWMS_PACKINGBOXes
                           where
                               line.BOXID == boxId &&
                               line.DEALERCODE != dealerCode
                           select new
                           {
                               CREATE_DATE = line.CREATE_DATE,
                               DEALERCODE = line.DEALERCODE
                           };


            if (search.Any())
            {
                return false;
            }
            else
                return true;

            /*
            if (search.Any())
            {
                //String strdate = "";
                string strdealercode = "";
                foreach (var item in search)
                {
                    //if (strdate == "")
                        //strdate = item.CREATE_DATE.Value.Date.ToString("d-M-yyyy");

                    if (strdealercode == "")
                    {
                        strdealercode = item.DEALERCODE;
                    }
                    else
                    {
                        if (strdealercode != item.DEALERCODE)
                        {
                            valid = false;
                        }
                    }
                }

                return valid;
            }
            else
            {
                return false;
            }
            */ 
        }
        
        public IQueryable<TransMonitoringBox> ListMonitoringBox()
        {
            IQueryable<TransMonitoringBox> search =
              from a in Context.MPMWMS_PACKINGBOXes
              join b in Context.CUSTPACKINGSLIPJOURs
              on new { a.PACKINGID } equals new { PACKINGID = b.PACKINGSLIPID }
              //join c in Context.SALESTABLEs
              //on new { b.SALESID } equals new {c.SALESID}
              where
              //c.INVENTSITEID == UserSession.SITE_ID_MAPPING
              b.PRINTMGMTSITEID == UserSession.SITE_ID_MAPPING
              orderby a.BOXID
              select new TransMonitoringBox
              {
                  BOXID = a.BOXID,
                  DEALERCODE = a.DEALERCODE,
                  PACKINGID = a.PACKINGID,
                  PICKINGID = b.XTSPICKINGROUTEID,
                  PARTNO = a.PARTNO,
                  QUANTITY = a.QUANTITY,
                  PIC = a.CREATE_BY,
                  SALESID = b.SALESID
              };

            return search;
        }

        public int Number_distinctbox(String packingId)
        {
            var search =
                from result in
                    (
                        from line in Context.MPMWMS_PACKINGBOXes
                        where
                            line.PACKINGID == packingId
                        group line by new { line.BOXID } into group_line
                        select new
                        {
                            BOXID = group_line.Key.BOXID
                        }
                    )
                group result by 1 into group_result
                select new
                {
                    JUMLAH = group_result.Count()
                };

            int jumlah = 0;
            foreach (var item in search)
            {
                jumlah = item.JUMLAH;
            }

            return jumlah;
        }

        public int Number_distinctbox(List<string> list_packing)
        {
            var search =
                from result in
                    (
                        from line in Context.MPMWMS_PACKINGBOXes
                        where
                        list_packing.Contains(line.PACKINGID)
                        group line by new { line.BOXID } into group_line
                        select new
                        {
                            BOXID = group_line.Key.BOXID
                        }
                    )
                group result by 1 into group_result
                select new
                {
                    JUMLAH = group_result.Count()
                };

            int jumlah = 0;
            foreach (var item in search)
            {
                jumlah = item.JUMLAH;
            }

            return jumlah;
        }

        public List<MPMWMS_PACKINGBOX> List(String packingId)
        {
            IQueryable<MPMWMS_PACKINGBOX> search =
                from line in Context.MPMWMS_PACKINGBOXes
                where
                    line.PACKINGID == packingId
                select line;
            return search.ToList();
        }

        public List<MPMWMS_PACKINGBOX> List(String packingId, String partNo)
        {
            IQueryable<MPMWMS_PACKINGBOX> search =
                from line in Context.MPMWMS_PACKINGBOXes
                where
                    line.PACKINGID == packingId
                    && line.PARTNO == partNo
                select line;
            return search.ToList();
        }


        public void Insert(MPMWMS_PACKINGBOX row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_PACKINGBOXes.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_PACKINGBOX row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_PACKINGBOX row)
        {
            Context.MPMWMS_PACKINGBOXes.DeleteOnSubmit(row);
        }

        public MPMWMS_PACKINGBOX item(String packingId, String boxId, String partNo)
        {
            IQueryable<MPMWMS_PACKINGBOX> search =
                from line in Context.MPMWMS_PACKINGBOXes
                where
                    line.PACKINGID == packingId
                    && line.BOXID == boxId
                    && line.PARTNO == partNo
                    && line.MAINDEALERID == UserSession.MAINDEALER_ID
                    && line.DATAAREAID == UserSession.DATAAREA_ID
                    && line.SITEID == UserSession.SITE_ID_MAPPING
                select line;

            return search.FirstOrDefault();
        }


    }
}
