﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteEAD
{
    public class RouteListEADObject : MPMDbObject<WmsUnitDataContext>
    {
        public RouteListEADObject()
            : base()
        {
        }

        public List<MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST> ItemAll()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST Item(String _routeId, String _delaerCode)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs
                               where a.ROUTE_ID == _routeId
                                && a.DEALER_CODE == _delaerCode
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST item)
        {
            try
            {
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                item.MAINDEALER_ID = (UserSession == null ? "" : UserSession.MAINDEALER_ID);
                item.SITE_ID = (UserSession == null ? "" : UserSession.SITE_ID_MAPPING);
                item.DATAAREA_ID = (UserSession == null ? "" : UserSession.DATAAREA_ID);
                Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST item)
        {
            try
            {
                Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        
        public List<ViewRouteListRecord> ViewRouteList(String _routeId)
        {
            try
            {
                //var itemsObj = from unitList in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs
                //                    where unitList.ROUTE_ID == _routeId
                //               join cust in Context.CUSTTABLEs on new { CUSTID = unitList.DEALER_CODE }
                //                                            equals new { CUSTID = cust.ACCOUNTNUM }
                //               join party in Context.DIRPARTYTABLEs on new { PARTYID = cust.PARTY }
                //                                            equals new { PARTYID = party.RECID }
                //               select new ViewRouteListRecord(unitList.ROUTE_ID, unitList.DEALER_CODE, party.NAME, cust.MPMLAT, cust.MPMLONG, cust.MPMDISTANCE);

                //return itemsObj.ToList();

                var itemsObj = from unitList in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_LISTs
                               where unitList.ROUTE_ID == _routeId
                               join cust in Context.MPMSERVVIEWCHANNELH1s on new { CUSTID = unitList.DEALER_CODE }
                                                            equals new { CUSTID = cust.ACCOUNTNUM }
                               //join party in Context.DIRPARTYTABLEs on new { PARTYID = cust.PARTY }
                               //                             equals new { PARTYID = party.RECID }
                                                            //select new ViewRouteListRecord(unitList.ROUTE_ID, unitList.DEALER_CODE, party.NAME, cust.MPMLAT, cust.MPMLONG, cust.MPMDISTANCE);
                               select new ViewRouteListRecord(unitList.ROUTE_ID, unitList.DEALER_CODE, cust.NAME, cust.MPMLAT, cust.MPMLONG, cust.MPMDISTANCE);

                return itemsObj.ToList();

            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
