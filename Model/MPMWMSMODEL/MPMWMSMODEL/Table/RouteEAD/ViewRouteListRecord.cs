﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteEAD
{
    public class ViewRouteListRecord
    {
        public String ROUTEID { get; set; }
        public String DEALERCODE { get; set; }
        public String DEALERNAME { get; set; }
        public Decimal LATITUDE { get; set; }
        public Decimal LONGITUDE { get; set; }
        public Decimal RANGE { get; set; }

        public ViewRouteListRecord() { }


        public ViewRouteListRecord
            (
                String _routeid,
                String _dealercode,
                String _dealername,
                Decimal _latitude,
                Decimal _longitude,
                Decimal _range
            )
        {
            ROUTEID = _routeid;
            DEALERCODE = _dealercode;
            DEALERNAME = _dealername;
            LATITUDE = _latitude;
            LONGITUDE = _longitude;
            RANGE = _range;
        }
    }
}
