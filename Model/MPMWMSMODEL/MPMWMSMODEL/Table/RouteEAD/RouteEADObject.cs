﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.EADUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteEAD
{
    public class RouteEADObject : MPMDbObject<WmsUnitDataContext>
    {
        public RouteEADObject()
            : base()
        {
        }

        public List<MPMWMS_EAD_ROUTE_DEALER_UNIT> ItemAll()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNITs
                               where a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<RouteMulti_REC> ItemAllMulti( string routeid)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs
                               join b in Context.MPMWMS_EAD_ROUTE_DEALER_UNITs on a.MULTI_ROUTE_ID equals b.ROUTE_ID
                               where a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                                && a.ROUTE_ID == routeid
                               select new RouteMulti_REC
                               {
                                   ID = a.ID,
                                   ROUTEID = a.ROUTE_ID,
                                   ROUTENAME = b.ROUTE_NAME,
                                   NUMBER = a.NUMBER,
                                   MULTIROUTE = a.MULTI_ROUTE_ID


                               };
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI ItemDeleteMulti(String _routeId, string multiroute)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs
                               where a.ROUTE_ID == _routeId && a.MULTI_ROUTE_ID == multiroute
                                && a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI CekUrutanMulti(String _routeId, int urutan)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs
                               where a.ROUTE_ID == _routeId && a.NUMBER == urutan
                                && a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI ItemMultibyId(int id)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs
                               where a.ID == id
                                && a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_ROUTE_DEALER_UNIT Item(String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNITs
                               where a.ROUTE_ID == _routeId
                                && a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


        public RouteMulti_REC ItemWithREC(String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_ROUTE_DEALER_UNITs
                               where a.ROUTE_ID == _routeId
                                && a.DATAAREA_ID == UserSession.DATAAREA_ID
                                && a.MAINDEALER_ID == UserSession.MAINDEALER_ID
                                && a.SITE_ID == UserSession.SITE_ID_MAPPING
                               select new RouteMulti_REC
                               {
                                   
                                   ROUTEID = a.ROUTE_ID,
                                   ROUTENAME = a.ROUTE_NAME,
                                   

                               };
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_ROUTE_DEALER_UNIT item)
        {
            try
            {
                item.MAINDEALER_ID = (UserSession == null ? "" : UserSession.MAINDEALER_ID);
                item.SITE_ID = (UserSession == null ? "" : UserSession.SITE_ID_MAPPING);
                item.DATAAREA_ID = (UserSession == null ? "" : UserSession.DATAAREA_ID);
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public String Insert(MPMWMS_EAD_ROUTE_DEALER_UNIT item)
        {
            int? output = 0;
            int tahun = DateTime.Now.Year;

            try
            {
                Context.MPMGETRUNNINGNUMBER("ROUTE_EAD", "", tahun, 0, ref output);

                item.ROUTE_ID = "R" + tahun + "00000".Substring(0, 5 - output.ToString().Length) + output.ToString();
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                item.MAINDEALER_ID = (UserSession == null ? "" : UserSession.MAINDEALER_ID);
                item.SITE_ID = (UserSession == null ? "" : UserSession.SITE_ID_MAPPING);
                item.DATAAREA_ID = (UserSession == null ? "" : UserSession.DATAAREA_ID);
                Context.MPMWMS_EAD_ROUTE_DEALER_UNITs.InsertOnSubmit(item);

                return item.ROUTE_ID;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public String InsertMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI item)
        {
            int? output = 0;
            int tahun = DateTime.Now.Year;

            try
            {
              
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                item.MAINDEALER_ID = (UserSession == null ? "" : UserSession.MAINDEALER_ID);
                item.SITE_ID = (UserSession == null ? "" : UserSession.SITE_ID_MAPPING);
                item.DATAAREA_ID = (UserSession == null ? "" : UserSession.DATAAREA_ID);
                Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs.InsertOnSubmit(item);

                return item.ROUTE_ID;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_ROUTE_DEALER_UNIT item)
        {
            try
            {
                Context.MPMWMS_EAD_ROUTE_DEALER_UNITs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DeleteMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI item)
        {
            try
            {
                Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

    }
}
