﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.AutoPickingEAD
{
    public class AutomaticPickingRecord
    {
        [Display(Name = "Main Dealer")]
        public String MAINDEALERID { get; set; }
        
        [Display(Name = "Data Area")]
        public String DATAAREA { get; set; }

        [Display(Name = "Site")]
        public String SITEID { get; set; }

        [Display(Name = "Picking ID")]
        public String PICKINGID { get; set; }

        [Display(Name = "Picking Date")]
        public DateTime PICKINGDATE { get; set; }

        [Display(Name = "DO ID")]
        public String INVOICEID { get; set; }

        [Display(Name = "DO Date")]
        public DateTime DODATE { get; set; }

        [Display(Name = "Dealer Code")]
        public String DEALERCODE { get; set; }

        [Display(Name = "Dealer Name")]
        public String DEALERNAME { get; set; }

        [Display(Name = "Dealer Address")]
        public String DEALERADDRESS { get; set; }

        [Display(Name = "Dealer City")]
        public String DEALERCITY { get; set; }

        [Display(Name = "Expedition")]
        public String EXPEDITION { get; set; }

        [Display(Name = "Police Number")]
        public String POLICE_NUMBER { get; set; }

        [Display(Name = "Driver")]
        public String DRIVER { get; set; }

        [Display(Name = "Descripsi")]
        public String DESCRIPSI { get; set; }

        [Display(Name = "NPK")]
        public String NPK { get; set; }

        [Display(Name = "Warehouse ID")]
        public String WAREHOUSEID { get; set; }

        [Display(Name = "Locationid ID")]
        public String LOCATIONID { get; set; }

        [Display(Name = "Machine ID")]
        public String MACHINEID { get; set; }

        [Display(Name = "Frame ID")]
        public String FRAMEID { get; set; }

        [Display(Name = "Color")]
        public String COLOR { get; set; }

        [Display(Name = "Item ID")]
        public String ITEMID { get; set; }
    }
}
