﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.AutoPickingEAD
{
    public class AutomaticPickingObject : MPMDbObject<WmsUnitDataContext>
    {
        public AutomaticPickingObject()
            : base()
        {
        }

        public List<AutomaticPickingRecord> TOResult(String _prioritas1, String _prioritas2, String _prioritas3)
        {
            try
            {
                Context.CommandTimeout = 180;

                var data = Context.MPMWMS_SP_PICKING_TRUCK_OPTIMATION_UNIT(UserSession.SITE_ID_MAPPING, UserSession.MAINDEALER_ID, UserSession.DATAAREA_ID, UserSession.NPK, _prioritas1, _prioritas2, _prioritas3);

                List<AutomaticPickingRecord> dataList = new List<AutomaticPickingRecord>();

                foreach (var x in data.ToList())
                {
                    dataList.Add(new AutomaticPickingRecord
                    {
                        MAINDEALERID = x.MAINDEALER_ID
                        ,
                        DATAAREA = x.DATAAREA_ID
                        ,
                        SITEID = x.SITE_ID
                        ,
                        PICKINGID = x.PICKINGID
                        ,
                        INVOICEID = x.INVOICEID
                        ,
                        DODATE = x.DO_DATE
                        ,
                        DEALERCODE = x.DEALERCODE
                        ,
                        DEALERNAME = x.DEALERNAME
                        ,
                        DEALERADDRESS = x.DEALERADDRESS
                        ,
                        DEALERCITY = x.DEALERCITY
                        ,
                        EXPEDITION = x.EXPEDITION
                        ,
                        POLICE_NUMBER = x.POLICE_NUMBER
                        ,
                        DRIVER = x.DRIVER
                        ,
                        DESCRIPSI = x.DESCRIPSI
                        ,
                        NPK = x.NPK
                        ,
                        WAREHOUSEID = x.WAREHOUSEID
                        ,
                        LOCATIONID = x.LOCATIONID
                        ,
                        MACHINEID = x.MACHINEID
                        ,
                        FRAMEID = x.FRAMEID
                        ,
                        ITEMID = x.ITEMID
                        ,
                        COLOR = x.COLOR
                    });
                }

                return dataList.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
