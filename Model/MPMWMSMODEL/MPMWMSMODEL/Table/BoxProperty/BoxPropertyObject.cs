﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.BoxProperty
{
    public class BoxPropertyObject : MPMDbObject<WmsUnitDataContext>
    {
        public void Insert(MPMWMS_BOXPROPERTy row)
        {
            row.CREATEBY = UserSession.NPK;
            row.CREATEDATE = MPMDateUtil.DateTime;
            Context.MPMWMS_BOXPROPERTies.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_BOXPROPERTy row)
        {
            row.MODIFBY = UserSession.NPK;
            row.MODIFDATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_BOXPROPERTy row)
        {
            Context.MPMWMS_BOXPROPERTies.DeleteOnSubmit(row);
        }

        public MPMWMS_BOXPROPERTy item(String boxId)
        {
            IQueryable<MPMWMS_BOXPROPERTy> search =
                from line in Context.MPMWMS_BOXPROPERTies
                where
                    line.BOXID == boxId
                select line;

            return search.FirstOrDefault();
        }


    }
}
