﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventLocation
{
    public class InventLocationObject: MPMDbObject<WmsUnitDataContext>, IInventLocationObject
    {
        public InventLocationObject()
        {
        }

        public List<INVENTLOCATION> List()
        {
            return Context.INVENTLOCATIONs.ToList();
        }

        public INVENTLOCATION Item(String dataarea_id, String site_id)
        {
            IQueryable<INVENTLOCATION> itemObj =
                from location in Context.INVENTLOCATIONs
                where (location.DATAAREAID == dataarea_id)
                && (location.INVENTSITEID == site_id)
                select location;

            return itemObj.Take(1).FirstOrDefault();
        }

        public INVENTLOCATION Item(String dataarea_id, String site_id, String location_id)
        {
            IQueryable<INVENTLOCATION> itemObj =
                from location in Context.INVENTLOCATIONs
                where (location.DATAAREAID == dataarea_id)
                && (location.INVENTSITEID == site_id)
                && (location.INVENTLOCATIONID == location_id)
                select location;

            return itemObj.Take(1).FirstOrDefault();
        }

        public INVENTLOCATION ItemIntransit(String dataarea_id, String site_id)
        {
            IQueryable<INVENTLOCATION> itemObj =
                from location in Context.INVENTLOCATIONs
                where (location.DATAAREAID == dataarea_id)
                && (location.INVENTSITEID == site_id)
                && (location.INVENTLOCATIONTYPE == 2)
                select location;

            return itemObj.Take(1).FirstOrDefault();
        }
    }
}
