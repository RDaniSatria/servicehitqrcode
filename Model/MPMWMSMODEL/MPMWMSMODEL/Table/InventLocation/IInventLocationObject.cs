﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventLocation
{
    public interface IInventLocationObject
    {
        List<INVENTLOCATION> List();
        INVENTLOCATION Item(String dataarea_id, String site_id);
        INVENTLOCATION Item(String dataarea_id, String site_id, String location_id);
    }
}
