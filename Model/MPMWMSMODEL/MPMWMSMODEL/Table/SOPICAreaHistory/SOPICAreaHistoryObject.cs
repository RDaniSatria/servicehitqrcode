﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.PICAreaHistory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SOPICAreaHistory
{
    public class SOPICAreaHistoryObject : MPMDbObject<WmsUnitDataContext>
    {
        public SOPICAreaHistoryObject()
            : base()
        {
        }

        public MPMWMS_MAP_PIC_HISTORY Item(
                  String _soId
                , String _dealerId
                , String _pickingId
                , String _pic
                , String _dataArea
                , String _mainDealer
                , String _siteId
            )
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_PIC_HISTORies
                               where (a.SOID == _soId)
                                && (a.DEALERID == _dealerId)
                                && (a.PICKINGID == _pickingId)
                                && (a.PIC == _pic)
                                && (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_MAP_PIC_HISTORY ItemBySO(
                  String _soId
                , String _dataArea
                , String _mainDealer
                , String _siteId
            )
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_PIC_HISTORies
                               where (a.SOID == _soId)
                                && (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_MAP_PIC_HISTORY ItemByPicking(
              String _pickingId
            , String _dataArea
            , String _mainDealer
            , String _siteId
        )
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_PIC_HISTORies
                               where (a.PICKINGID == _pickingId)
                                && (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MAP_PIC_HISTORY item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_MAP_PIC_HISTORY item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMS_MAP_PIC_HISTORies.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_MAP_PIC_HISTORY item)
        {
            try
            {
                Context.MPMWMS_MAP_PIC_HISTORies.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DashboardMonitoring_Record> dataDashboardMonitoring()
        {
            var data = Context.MPMWMSDATADASHBOARDMONITORING();

            List<DashboardMonitoring_Record> dataList = new List<DashboardMonitoring_Record>();

            foreach (var z in data.ToList())
            {
                dataList.Add(new DashboardMonitoring_Record
                {
                      DEALERID = z.DEALERID
                    , SOID = z.SOID
                    , PIC = z.PIC
                    , PICTIME = z.PICTIME
                    , PICSTATUS = z.PICSTATUS
                    , PICTRANSPORTER = z.PICTRANSPORTER
                    , TRANSPORTERTIME = z.TRANSPORTERTIME
                    , TRANSPORTERSTATUS = z.TRANSPORTERSTATUS
                    , PICCHECKER = z.PICCHECKER
                    , CHECKERTIME = z.CHECKERTIME
                    , CHECKERSTATUS = z.CHECKERSTATUS
                });
            }

            return dataList.ToList();
        }
    }
}
