﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MPMWMSMODEL.Table.DealerExcept
{
    public class DEALER_EXCEPT
    {
        [Required(ErrorMessage = "Tanggal is required")]
        public DateTime Tanggal { get; set; }
        [Required(ErrorMessage = "Dealer is required")]
        public string Dealer { get; set; }
        public string Status { get; set; }
        [Required(ErrorMessage = "DO ID is required")]
        public string DoID { get; set; }
        [Required(ErrorMessage = "Tipe is required")]
        public string Tipe { get; set; }
        [Required(ErrorMessage = "Warna is required")]
        public string Warna { get; set; }
        [Required(ErrorMessage = "Warna is required")]
        public int Quantity { get; set; }
        public string DataAreaID { get; set; }
        public string MainDealerID { get; set; }
        public string SiteID { get; set; }
        public int ID { get; set; }
        public string DealerName { get; set; }

    }
}
