﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DealerExcept
{
    public class DealerExceptObject : MPMDbObject<WmsUnitDataContext>
    {
        public DealerExceptObject()
            : base()
        {
        }

        public void InsertDealerExcept(MPMWMS_EAD_DEALER_EXCEPT row)
        {

            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EAD_DEALER_EXCEPTs.InsertOnSubmit(row);
        }

        public void UpdateDealerExcept(MPMWMS_EAD_DEALER_EXCEPT row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void DeleteDealerExcept(MPMWMS_EAD_DEALER_EXCEPT row)
        {
            try
            {
                Context.MPMWMS_EAD_DEALER_EXCEPTs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_DEALER_EXCEPT> ListDealerExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_EXCEPTs
                               select datas;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DEALER_EXCEPT> ListDataDealerExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_EXCEPTs
                               join dealer in Context.MPMSERVVIEWCHANNELH1s on datas.DEALER_CODE equals dealer.ACCOUNTNUM
                               select new DEALER_EXCEPT
                               {
                                   Dealer = datas.DEALER_CODE,
                                   Status = (datas.STATUS == "1") ? "Aktif" : "Done",
                                   Tanggal = datas.DATE,
                                   DealerName = dealer.NAME
                                };
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_DEALER_EXCEPT ItemDealerExcept(DateTime date, string dealercode)
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_EXCEPTs
                               where datas.DATE.Equals(date) &&
                               datas.DEALER_CODE.Equals(dealercode)
                               select datas;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertDealerDOExcept(MPMWMS_EAD_DEALER_DO_EXCEPT row)
        {

            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EAD_DEALER_DO_EXCEPTs.InsertOnSubmit(row);
        }

        public void UpdateDealerDOExcept(MPMWMS_EAD_DEALER_DO_EXCEPT row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void DeleteDealerDOExcept(MPMWMS_EAD_DEALER_DO_EXCEPT row)
        {
            try
            {
                Context.MPMWMS_EAD_DEALER_DO_EXCEPTs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_DEALER_DO_EXCEPT> ListDealerDOExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_DO_EXCEPTs
                               select datas;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DEALER_EXCEPT> ListDataDealerDOExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_DO_EXCEPTs
                               select new DEALER_EXCEPT
                               {
                                   Dealer = datas.DEALER_CODE,
                                   Status = (datas.STATUS == "1") ? "Aktif" : "Done",
                                   Tanggal = datas.DATE,
                                   DoID = datas.DO_ID


                               };
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_DEALER_DO_EXCEPT ItemDealerDOExcept(DateTime date, string dealercode, string doid)
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_DO_EXCEPTs
                               where datas.DATE.Equals(date) &&
                               datas.DEALER_CODE.Equals(dealercode) &&
                               datas.DO_ID.Equals(doid) 

                               select datas;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertDealerTypeColorExcept(MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT row)
        {

            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPTs.InsertOnSubmit(row);
        }

        public void UpdateDealerTypeColorExcept(MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void DeleteDealerTypeColorExcept(MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT row)
        {
            try
            {
                Context.MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPTs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT> ListDealerTypeColorExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPTs
                               select datas;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DEALER_EXCEPT> ListDataDealerTypeColorExcept()
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPTs
                               select new DEALER_EXCEPT
                               {
                                   Dealer = datas.DEALER_CODE,
                                   Tanggal = datas.DATE,
                                   Tipe = datas.TYPE,
                                   Warna = datas.COLOR,
                                   Quantity = datas.QUANTITY,

                               };
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


        public MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT ItemDealerTypeColorExcept(DateTime date, string dealercode, string tipe, string color)
        {
            try
            {
                var itemsObj = from datas in Context.MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPTs
                               where datas.DATE.Equals(date) &&
                                datas.DEALER_CODE.Equals(dealercode)
                                && datas.COLOR.Equals(color)
                                && datas.TYPE.Equals(tipe)

                               select datas;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
