﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ReturSLLine
{
    public interface IReturSLLineObject
    {
        List<MPMWMS_RETUR_SL_LINE> List(MPMWMS_RETUR_SL header);
        MPMWMS_RETUR_SL_LINE Create(MPMWMS_RETUR_SL header);
        void Insert(MPMWMS_RETUR_SL_LINE item);
        void Delete(MPMWMS_RETUR_SL_LINE row);
        int SumQuantityRetur(String dataarea_id, String maindealer_id, String site_id, String sales_id, String item_id, String invent_dim_id, String unit_year);
    }
}
