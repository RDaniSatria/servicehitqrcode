﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ReturSLLine
{
    public class ReturSLLineObject: MPMDbObject<WmsUnitDataContext>, IReturSLLineObject
    {
        public ReturSLLineObject()
            : base()
        {
        }

        public List<MPMWMS_RETUR_SL_LINE> List(MPMWMS_RETUR_SL header)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_RETUR_SL_LINEs
                    where
                        a.DATAAREA_ID == header.DATAAREA_ID
                        && a.MAINDEALER_ID == header.MAINDEALER_ID
                        && a.SITE_ID == header.SITE_ID
                        && a.SHIPPING_LIST_ID == header.SHIPPING_LIST_ID
                        && a.PICKING_ID == header.PICKING_ID
                        && a.RETUR_SL_ID == header.RETUR_SL_ID
                    select a;

                return search.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_RETUR_SL_LINE Item(String retur_sl_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_RETUR_SL_LINEs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.RETUR_SL_ID == retur_sl_id
                        && a.MACHINE_ID == machine_id
                        && a.FRAME_ID == frame_id
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_RETUR_SL_LINE Create(MPMWMS_RETUR_SL header)
        {
            MPMWMS_RETUR_SL_LINE row = new MPMWMS_RETUR_SL_LINE();
            row.DATAAREA_ID = header.DATAAREA_ID;
            row.MAINDEALER_ID = header.MAINDEALER_ID;
            row.SITE_ID = header.SITE_ID;
            row.RETUR_SL_ID = header.RETUR_SL_ID;
            row.RETUR_SL_DATE = header.RETUR_SL_DATE;
            row.DATAAREA_ID = header.DATAAREA_ID;
            row.SHIPPING_LIST_ID = header.SHIPPING_LIST_ID;
            row.SHIPPING_LIST_DATE = header.SHIPPING_LIST_DATE;
            row.PICKING_ID = header.PICKING_ID;
            row.PICKING_DATE = header.PICKING_DATE;
            row.DO_ID = header.DO_ID;
            row.DO_DATE = header.DO_DATE;
            return row;
        }

        public void Insert(MPMWMS_RETUR_SL_LINE item)
        {
            item.STATUS = "O";
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_RETUR_SL_LINEs.InsertOnSubmit(item);
        }

        public void Delete(MPMWMS_RETUR_SL_LINE row)
        {
            try
            {
                Context.MPMWMS_RETUR_SL_LINEs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public int SumQuantityRetur(String dataarea_id, String maindealer_id, String site_id, String sales_id, String item_id, String invent_dim_id, String unit_year)
        {
            IQueryable<int> list = from retur_line in Context.MPMWMS_RETUR_SL_LINEs
                                   join unit in Context.MPMWMS_UNITs on
                                        new { retur_line.MACHINE_ID, retur_line.FRAME_ID, retur_line.SITE_ID, retur_line.DATAAREA_ID } equals
                                        new { unit.MACHINE_ID, unit.FRAME_ID, unit.SITE_ID, unit.DATAAREA_ID } into join_unit_pick
                                   from result in join_unit_pick.DefaultIfEmpty()
                                   where (retur_line.DO_ID == sales_id) &&
                                   (retur_line.DATAAREA_ID == dataarea_id) &&
                                   (retur_line.MAINDEALER_ID == maindealer_id) &&
                                   (retur_line.SITE_ID == site_id) &&
                                   (retur_line.COLOR_TYPE == invent_dim_id) &&
                                   (retur_line.UNIT_TYPE == item_id) &&
                                   (retur_line.STATUS == "O") &&
                                   (result.UNIT_YEAR == unit_year)
                                   group retur_line by new
                                   {
                                       retur_line.DO_ID,
                                       retur_line.DATAAREA_ID,
                                       retur_line.COLOR_TYPE,
                                       retur_line.UNIT_TYPE
                                   } into g
                                   select g.Count();

            return list.Take(1).FirstOrDefault();
        }
    }
}
