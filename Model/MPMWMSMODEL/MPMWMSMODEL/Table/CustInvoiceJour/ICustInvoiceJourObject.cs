﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.SalesTable;

namespace MPMWMSMODEL.Table.CustInvoiceJour
{
    public interface ICustInvoiceJourObject
    {
        List<CUSTINVOICEJOUR> List();
        IQueryable<SALESTABLE_AV_PICK> SalesAVPicks();
        IQueryable<SALESTABLE_AV_PICK> JournalAVPicks(String category_product);
        List<SALESTABLE_AV_PICK> ListAVPick(String category_product);
        List<SALESTABLE_AV_PICK> ListAVPick(String category_product, int year, String status, String filterSO);
    }
}
