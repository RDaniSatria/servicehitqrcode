﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.CustInvoiceTrans;
using MPMWMSMODEL.Table.SalesTable;

namespace MPMWMSMODEL.Table.CustInvoiceJour
{
    public class CustInvoiceJourObject : MPMDbObject<WmsUnitDataContext>, ICustInvoiceJourObject
    {
        public CustInvoiceTransObject _CustInvoiceTransObject = new CustInvoiceTransObject(); 

        public CustInvoiceJourObject()
            : base()
        {
        }

        public CustInvoiceTransObject CustInvoiceTransObject
        {
            get { return _CustInvoiceTransObject; }
        }

        public List<CUSTINVOICEJOUR> List()
        {
            return Context.CUSTINVOICEJOURs.ToList();
        }

        public IQueryable<SALESTABLE_AV_PICK> SalesAVPicksSync()
        {
            return 
                from cj in Context.CUSTINVOICEJOURs
                join avpick in Context.MPMWMS_DO_AVPICKs on
                    new { 
                        salesId = cj.INVOICEID, 
                        dataId = cj.DATAAREAID, 
                    } equals
                    new { 
                        salesId = avpick.DO_ID, 
                        dataId = avpick.DATAAREA_ID, 
                    } into salestable_av
                from result in salestable_av
                join customer in Context.CUSTTABLEs on
                    new { accountnum = cj.INVOICEACCOUNT, dataareaid = cj.DATAAREAID } equals
                    new { accountnum = customer.ACCOUNTNUM, dataareaid = customer.DATAAREAID } into customer_join
                from result_customer in customer_join.DefaultIfEmpty()
                join addr in Context.MPMVIEW_DEALER_ADDRESSes on
                    new
                    {
                        cust = cj.INVOICEACCOUNT
                    } equals
                    new
                    {
                        cust = addr.ACCOUNTNUM
                    } into address_join
                from result_address in address_join.DefaultIfEmpty()
                where
                    cj.DATAAREAID == UserSession.DATAAREA_ID &&
                    result.MAINDEALER_ID == UserSession.MAINDEALER_ID &&
                    result.SITE_ID == UserSession.SITE_ID_MAPPING &&
                    result.QUANTITY_DO - result.QUANTITY_PICK > 0
                select new SALESTABLE_AV_PICK(
                    "N",
                    cj.INVOICEID,
                    cj.DATAAREAID,
                    result.MAINDEALER_ID,
                    result.SITE_ID,
                    cj.INVOICINGNAME,
                    cj.INVOICEACCOUNT,
                    result_address.NAME,
                    result_address.STREET,
                    result_address.CITY,
                    cj.DELIVERYNAME,
                    cj.INVOICEDATE,
                    result.QUANTITY_DO,
                    result.QUANTITY_PICK,
                    result.QUANTITY_SL
                );
        }

        private static Func<WmsUnitDataContext, String, String, String, int, String, IQueryable<SALESTABLE_AV_PICK>>
           
            FLPSalesAVPickSyncQuery =
                CompiledQuery.Compile((WmsUnitDataContext db, String dataareaid, String maindealerid, String siteid, int year, String yearstr)
                    =>
                from a in db.CUSTINVOICEJOURs
                join b in db.MPMWMS_DO_AVPICKs on
	                new { do_id = a.INVOICEID, dataareaid = a.DATAAREAID } equals
	                new { do_id = b.DO_ID, dataareaid = b.DATAAREA_ID} into join_ab
                from result_ab in join_ab
                join c in db.CUSTTABLEs on
	                new { accountnum = a.INVOICEACCOUNT, dataareaid = a.DATAAREAID } equals
	                new { accountnum = c.ACCOUNTNUM, dataareaid = c.DATAAREAID } into join_ac
                from result_ac in join_ac.DefaultIfEmpty()
                    //join d in db.MPMVIEW_DEALER_ADDRESSes on 
                    // new { accountnum = a.INVOICEACCOUNT } equals
                    // new { accountnum = d.ACCOUNTNUM } into join_ad
                join d in db.MPMSERVVIEWCHANNELH1s on
                 new { accountnum = a.INVOICEACCOUNT } equals
                 new { accountnum = d.ACCOUNTNUM } into join_ad
                from result_ad in join_ad.DefaultIfEmpty()
                where
	                //a.INVOICEDATE.Year == year
                    result_ab.DO_ID.Contains(yearstr) //String.Format("/{0}/",year)
                    && a.DATAAREAID == dataareaid
	                && result_ab.MAINDEALER_ID == maindealerid
	                && result_ab.SITE_ID == siteid
                    && result_ab.QUANTITY_DO > 0
                    && result_ab.QUANTITY_DO > result_ab.QUANTITY_SL
                    && result_ab.QUANTITY_PICK < result_ab.QUANTITY_DO
                select new SALESTABLE_AV_PICK(
                    "N",
                    a.INVOICEID,
                    a.DATAAREAID,
                    result_ab.MAINDEALER_ID,
                    result_ab.SITE_ID,
                    a.INVOICINGNAME,
                    a.INVOICEACCOUNT,
                    result_ad.NAME,
	                result_ad.STREET,
	                result_ad.CITY,
                    a.DELIVERYNAME,
	                a.INVOICEDATE,
                    result_ab.QUANTITY_DO,
                    result_ab.QUANTITY_PICK,
                    result_ab.QUANTITY_SL
                ));
        /*
                from a in db.MPMVIEW_PICKING_LIST_INVOICEs
                where
                    a.dataareaid == dataareaid &&
                    a.maindealer_id == maindealerid &&
                    a.site_id == siteid &&
                    a.year == year
                select new SALESTABLE_AV_PICK(
                    "N",
                    a.invoiceid,
                    a.dataareaid,
                    a.maindealer_id,
                    a.site_id,
                    a.invoicingname,
                    a.invoiceaccount,
                    a.name,
                    a.street,
                    a.city,
                    a.deliveryname,
                    a.invoicedate,
                    a.quantity_do == null ? 0 : a.quantity_do,
                    a.quantity_pick == null ? 0 : a.quantity_pick,
                    a.quantity_sl == null ? 0 : a.quantity_sl
                ));*/

        //private static Func<WmsUnitDataContext, String, String, String, String, IQueryable<SALESTABLE_AV_PICK>>

        //    FLPSalesAVPickSyncQuery2 =
        //        CompiledQuery.Compile((WmsUnitDataContext db, String dataareaid, String maindealerid, String siteid, String yearstr)
        //            =>
        //        from a in db.CUSTINVOICEJOURs//MPMWMS_SP_LISTAVPICK(dataareaid, maindealerid, siteid, yearstr)
        //        select new SALESTABLE_AV_PICK(
        //            a.IsInternal,
        //            a.INVOICEID,
        //            a.DATAAREAID,
        //            a.MAINDEALER_ID,
        //            a.SITE_ID,
        //            a.INVOICINGNAME,
        //            a.INVOICEACCOUNT,
        //            a.NAME,
        //            a.STREET,
        //            a.CITY,
        //            a.DELIVERYNAME,
        //            a.INVOICEDATE,
        //            a.QUANTITY_DO,
        //            a.QUANTITY_PICK,
        //            a.QUANTITY_SL
        //        ));

        //public IQueryable<SALESTABLE_AV_PICK> SalesAVPicksSync(int year)
        public List<SALESTABLE_AV_PICK> SalesAVPicksSync(int year)
        {
            String yearstr = String.Format("/{0}/", year);
            var data = ((WmsUnitDataContext)Context).MPMWMS_SP_LISTAVPICK(UserSession.DATAAREA_ID,
                UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING, yearstr).
                Select(a=> new SALESTABLE_AV_PICK(
                    a.IsInternal,
                    a.INVOICEID,
                    a.DATAAREAID,
                    a.MAINDEALER_ID,
                    a.SITE_ID,
                    a.INVOICINGNAME,
                    a.INVOICEACCOUNT,
                    a.NAME,
                    a.STREET,
                    a.CITY,
                    a.DELIVERYNAME,
                    a.INVOICEDATE,
                    a.QUANTITY_DO,
                    a.QUANTITY_PICK,
                    a.QUANTITY_SL
                    ));
            return data.ToList();
            ////comment 2021/09/09
            //return FLPSalesAVPickSyncQuery(Context, UserSession.DATAAREA_ID,
            //    UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING, year, yearstr);


            /*return
                from cj in Context.CUSTINVOICEJOURs
                join avpick in Context.MPMWMS_DO_AVPICKs on
                    new
                    {
                        salesId = cj.INVOICEID,
                        dataId = cj.DATAAREAID,
                    } equals
                    new
                    {
                        salesId = avpick.DO_ID,
                        dataId = avpick.DATAAREA_ID,
                    } into salestable_av
                from result in salestable_av
                join customer in Context.CUSTTABLEs on
                    new { accountnum = cj.INVOICEACCOUNT, dataareaid = cj.DATAAREAID } equals
                    new { accountnum = customer.ACCOUNTNUM, dataareaid = customer.DATAAREAID } into customer_join
                from result_customer in customer_join.DefaultIfEmpty()
                join addr in Context.MPMVIEW_DEALER_ADDRESSes on
                    new
                    {
                        cust = cj.INVOICEACCOUNT
                    } equals
                    new
                    {
                        cust = addr.ACCOUNTNUM
                    } into address_join
                from result_address in address_join.DefaultIfEmpty()
                where
                    cj.DATAAREAID == UserSession.DATAAREA_ID &&
                    result.MAINDEALER_ID == UserSession.MAINDEALER_ID &&
                    result.SITE_ID == UserSession.SITE_ID_MAPPING &&
                    cj.INVOICEDATE.Year == year
                select new SALESTABLE_AV_PICK(
                    "N",
                    cj.INVOICEID,
                    cj.DATAAREAID,
                    result.MAINDEALER_ID,
                    result.SITE_ID,
                    cj.INVOICINGNAME,
                    cj.INVOICEACCOUNT,
                    result_address.NAME,
                    result_address.STREET,
                    result_address.CITY,
                    cj.DELIVERYNAME,
                    cj.INVOICEDATE,
                    result.QUANTITY_DO == null ? 0 : result.QUANTITY_DO,
                    result.QUANTITY_PICK == null ? 0 : result.QUANTITY_PICK,
                    result.QUANTITY_SL == null ? 0 : result.QUANTITY_SL
                );*/
        }

        public IQueryable<SALESTABLE_AV_PICK> SalesAVPicksSync(String invoice_id)
        {
            return
                from cj in Context.CUSTINVOICEJOURs
                join avpick in Context.MPMWMS_DO_AVPICKs on
                    new
                    {
                        salesId = cj.INVOICEID,
                        dataId = cj.DATAAREAID,
                    } equals
                    new
                    {
                        salesId = avpick.DO_ID,
                        dataId = avpick.DATAAREA_ID,
                    } into salestable_av
                from result in salestable_av
                join customer in Context.CUSTTABLEs on
                    new { accountnum = cj.INVOICEACCOUNT, dataareaid = cj.DATAAREAID } equals
                    new { accountnum = customer.ACCOUNTNUM, dataareaid = customer.DATAAREAID } into customer_join
                from result_customer in customer_join.DefaultIfEmpty()
                join addr in Context.MPMVIEW_DEALER_ADDRESSes on
                    new
                    {
                        cust = cj.INVOICEACCOUNT
                    } equals
                    new
                    {
                        cust = addr.ACCOUNTNUM
                    } into address_join
                from result_address in address_join.DefaultIfEmpty()
                where
                    cj.DATAAREAID == UserSession.DATAAREA_ID &&
                    result.MAINDEALER_ID == UserSession.MAINDEALER_ID &&
                    result.SITE_ID == UserSession.SITE_ID_MAPPING &&
                    cj.INVOICEID == invoice_id
                select new SALESTABLE_AV_PICK(
                    "N",
                    cj.INVOICEID,
                    cj.DATAAREAID,
                    result.MAINDEALER_ID,
                    result.SITE_ID,
                    cj.INVOICINGNAME,
                    cj.INVOICEACCOUNT,
                    result_address.NAME,
                    result_address.STREET,
                    result_address.CITY,
                    cj.DELIVERYNAME,
                    cj.INVOICEDATE,
                    result.QUANTITY_DO,
                    result.QUANTITY_PICK,
                    result.QUANTITY_SL
                );
        }

        public IQueryable<SALESTABLE_AV_PICK> SalesAVPicks()
        {
            return 
                from cj in Context.CUSTINVOICEJOURs
                join avpick in Context.MPMWMS_DO_AVPICKs on
                    new { 
                        salesId = cj.INVOICEID, 
                        dataId = cj.DATAAREAID, 
                    } equals
                    new { 
                        salesId = avpick.DO_ID, 
                        dataId = avpick.DATAAREA_ID, 
                    } into salestable_av
                from result in salestable_av
                join customer in Context.CUSTTABLEs on
                    new { accountnum = cj.INVOICEACCOUNT, dataareaid = cj.DATAAREAID } equals
                    new { accountnum = customer.ACCOUNTNUM, dataareaid = customer.DATAAREAID } into customer_join
                from result_customer in customer_join.DefaultIfEmpty()
                join addr in Context.MPMVIEW_DEALER_ADDRESSes on
                    new
                    {
                        cust = cj.INVOICEACCOUNT
                    } equals
                    new
                    {
                        cust = addr.ACCOUNTNUM
                    } into address_join
                from result_address in address_join.DefaultIfEmpty()
                where
                    cj.DATAAREAID == UserSession.DATAAREA_ID &&
                    result.MAINDEALER_ID == UserSession.MAINDEALER_ID &&
                    result.SITE_ID == UserSession.SITE_ID_MAPPING &&
                    ((result.QUANTITY_DO) - (result.QUANTITY_PICK) != 0) 
                select new SALESTABLE_AV_PICK(
                    "N",
                    cj.INVOICEID,
                    cj.DATAAREAID,
                    result.MAINDEALER_ID,
                    result.SITE_ID,
                    cj.INVOICINGNAME,
                    cj.INVOICEACCOUNT,
                    result_address.NAME,
                    result_address.STREET,
                    result_address.CITY,
                    cj.DELIVERYNAME,
                    cj.INVOICEDATE,
                    result.QUANTITY_DO,
                    result.QUANTITY_PICK,
                    result.QUANTITY_SL
                );
        }

        public IQueryable<SALESTABLE_AV_PICK> SalesAVPicks(String invoice_id)
        {
            return
                from cj in Context.CUSTINVOICEJOURs
                join avpick in Context.MPMWMS_DO_AVPICKs on
                    new
                    {
                        salesId = cj.INVOICEID,
                        dataId = cj.DATAAREAID,
                    } equals
                    new
                    {
                        salesId = avpick.DO_ID,
                        dataId = avpick.DATAAREA_ID,
                    } into salestable_av
                from result in salestable_av
                join customer in Context.CUSTTABLEs on
                    new { accountnum = cj.INVOICEACCOUNT, dataareaid = cj.DATAAREAID } equals
                    new { accountnum = customer.ACCOUNTNUM, dataareaid = customer.DATAAREAID } into customer_join
                from result_customer in customer_join.DefaultIfEmpty()
                join addr in Context.MPMVIEW_DEALER_ADDRESSes on
                    new
                    {
                        cust = cj.INVOICEACCOUNT
                    } equals
                    new
                    {
                        cust = addr.ACCOUNTNUM
                    } into address_join
                from result_address in address_join.DefaultIfEmpty()
                where
                    cj.DATAAREAID == UserSession.DATAAREA_ID &&
                    result.MAINDEALER_ID == UserSession.MAINDEALER_ID &&
                    result.SITE_ID == UserSession.SITE_ID_MAPPING &&
                    cj.INVOICEID == invoice_id &&
                    ((result.QUANTITY_DO) - (result.QUANTITY_PICK) != 0)
                select new SALESTABLE_AV_PICK(
                    "N",
                    cj.INVOICEID,
                    cj.DATAAREAID,
                    result.MAINDEALER_ID,
                    result.SITE_ID,
                    cj.INVOICINGNAME,
                    cj.INVOICEACCOUNT,
                    result_address.NAME,
                    result_address.STREET,
                    result_address.CITY,
                    cj.DELIVERYNAME,
                    cj.INVOICEDATE,
                    result.QUANTITY_DO,
                    result.QUANTITY_PICK,
                    result.QUANTITY_SL
                );
        }

        //add by vania

        public IQueryable<SALESTABLE_AV_PICK> JournalAVPicksSync(String category_product)
        {
            return from inventname in Context.INVENTJOURNALNAMEs
                   from journal in Context.INVENTJOURNALTABLEs
                   where inventname.JOURNALNAMEID == journal.JOURNALNAMEID
                       && journal.XTSCATEGORYCODE == category_product
                       && inventname.XTSJOURNALNAMETRANSACTION == 5
                       && inventname.DATAAREAID == journal.DATAAREAID
                       && journal.DATAAREAID == UserSession.DATAAREA_ID
                       && journal.XTSMAINDEALERCODE == UserSession.MAINDEALER_ID
                       && journal.INVENTSITEID == UserSession.SITE_ID_MAPPING
                   join avpick in Context.MPMWMS_DO_AVPICKs on
                   new { journalid = journal.JOURNALID, dataarea = journal.DATAAREAID } equals
                   new { journalid = avpick.DO_ID, dataarea = avpick.DATAAREA_ID } into journaltable_av
                   from result in journaltable_av.DefaultIfEmpty()
                   select new SALESTABLE_AV_PICK(
                                   "Y",
                                       journal.JOURNALID,
                                       journal.DATAAREAID,
                                       journal.XTSMAINDEALERCODE,
                                       journal.INVENTSITEID,
                                       journal.JOURNALNAMEID,
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       journal.POSTEDDATETIME,
                                       Convert.ToInt32(result.QUANTITY_DO),
                                       Convert.ToInt32(result.QUANTITY_PICK),
                                       result.QUANTITY_SL);
        }

        public IQueryable<SALESTABLE_AV_PICK> JournalAVPicksSync(String category_product, int year)
        {
            return from inventname in Context.INVENTJOURNALNAMEs
                   from journal in Context.INVENTJOURNALTABLEs
                   where inventname.JOURNALNAMEID == journal.JOURNALNAMEID
                       && journal.XTSCATEGORYCODE == category_product
                       && inventname.XTSJOURNALNAMETRANSACTION == 5
                       && inventname.DATAAREAID == journal.DATAAREAID
                       && journal.DATAAREAID == UserSession.DATAAREA_ID
                       && journal.XTSMAINDEALERCODE == UserSession.MAINDEALER_ID
                       && journal.INVENTSITEID == UserSession.SITE_ID_MAPPING
                       && journal.POSTEDDATETIME.Year == year
                   join avpick in Context.MPMWMS_DO_AVPICKs on
                        new { journalid = journal.JOURNALID, dataarea = journal.DATAAREAID } equals
                        new { journalid = avpick.DO_ID, dataarea = avpick.DATAAREA_ID } into journaltable_av
                   from result in journaltable_av
                   select new SALESTABLE_AV_PICK(
                                   "Y",
                                       journal.JOURNALID,
                                       journal.DATAAREAID,
                                       journal.XTSMAINDEALERCODE,
                                       journal.INVENTSITEID,
                                       journal.JOURNALNAMEID,
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       journal.POSTEDDATETIME,
                                       result.QUANTITY_DO,
                                       result.QUANTITY_PICK,
                                       result.QUANTITY_SL);
        }

        public IQueryable<SALESTABLE_AV_PICK> JournalAVPicksSync(String category_product, String sales_id)
        {
            return from inventname in Context.INVENTJOURNALNAMEs
                   from journal in Context.INVENTJOURNALTABLEs
                   where inventname.JOURNALNAMEID == journal.JOURNALNAMEID
                       && journal.XTSCATEGORYCODE == category_product
                       && inventname.XTSJOURNALNAMETRANSACTION == 5
                       && inventname.DATAAREAID == journal.DATAAREAID
                       && journal.DATAAREAID == UserSession.DATAAREA_ID
                       && journal.XTSMAINDEALERCODE == UserSession.MAINDEALER_ID
                       && journal.JOURNALID == sales_id
                   join avpick in Context.MPMWMS_DO_AVPICKs on
                   new { journalid = journal.JOURNALID, dataarea = journal.DATAAREAID } equals
                   new { journalid = avpick.DO_ID, dataarea = avpick.DATAAREA_ID } into journaltable_av
                   from result in journaltable_av.DefaultIfEmpty()
                   select new SALESTABLE_AV_PICK(
                                   "Y",
                                       journal.JOURNALID,
                                       journal.DATAAREAID,
                                       journal.XTSMAINDEALERCODE,
                                       journal.INVENTSITEID,
                                       journal.JOURNALNAMEID,
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       journal.POSTEDDATETIME,
                                       Convert.ToInt32(result.QUANTITY_DO),
                                       Convert.ToInt32(result.QUANTITY_PICK),
                                       result.QUANTITY_SL);
        }

        public IQueryable<SALESTABLE_AV_PICK> JournalAVPicks(String category_product)
        {
            return from inventname in Context.INVENTJOURNALNAMEs
                   from journal in Context.INVENTJOURNALTABLEs
                   where inventname.JOURNALNAMEID == journal.JOURNALNAMEID
                       && journal.XTSCATEGORYCODE == category_product
                       && inventname.XTSJOURNALNAMETRANSACTION == 5
                       && inventname.DATAAREAID == journal.DATAAREAID
                       && journal.DATAAREAID == UserSession.DATAAREA_ID
                       && journal.XTSMAINDEALERCODE == UserSession.MAINDEALER_ID
                   join avpick in Context.MPMWMS_DO_AVPICKs on
                   new { journalid = journal.JOURNALID, dataarea = journal.DATAAREAID } equals
                   new { journalid = avpick.DO_ID, dataarea = avpick.DATAAREA_ID } into journaltable_av
                   from result in journaltable_av.DefaultIfEmpty()
                   where
                       ((Convert.ToInt32(result.QUANTITY_DO)) - (Convert.ToInt32(result.QUANTITY_PICK)) != 0)
                   select new SALESTABLE_AV_PICK(
                                   "Y",
                                       journal.JOURNALID,
                                       journal.DATAAREAID,
                                       journal.XTSMAINDEALERCODE,
                                       journal.INVENTSITEID,
                                       journal.JOURNALNAMEID,
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       journal.POSTEDDATETIME,
                                       Convert.ToInt32(result.QUANTITY_DO),
                                       Convert.ToInt32(result.QUANTITY_PICK),
                                       result.QUANTITY_SL);
        }

        public IQueryable<SALESTABLE_AV_PICK> JournalAVPicks(String category_product, String sales_id)
        {
            return from inventname in Context.INVENTJOURNALNAMEs
                   from journal in Context.INVENTJOURNALTABLEs
                   where inventname.JOURNALNAMEID == journal.JOURNALNAMEID
                       && journal.XTSCATEGORYCODE == category_product
                       && inventname.XTSJOURNALNAMETRANSACTION == 5
                       && inventname.DATAAREAID == journal.DATAAREAID
                       && journal.DATAAREAID == UserSession.DATAAREA_ID
                       && journal.XTSMAINDEALERCODE == UserSession.MAINDEALER_ID
                       && journal.JOURNALID == sales_id
                   join avpick in Context.MPMWMS_DO_AVPICKs on
                   new { journalid = journal.JOURNALID, dataarea = journal.DATAAREAID } equals
                   new { journalid = avpick.DO_ID, dataarea = avpick.DATAAREA_ID } into journaltable_av
                   from result in journaltable_av.DefaultIfEmpty()
                   where
                       ((Convert.ToInt32(result.QUANTITY_DO)) - (Convert.ToInt32(result.QUANTITY_PICK)) != 0)
                   select new SALESTABLE_AV_PICK(
                                   "Y",
                                       journal.JOURNALID,
                                       journal.DATAAREAID,
                                       journal.XTSMAINDEALERCODE,
                                       journal.INVENTSITEID,
                                       journal.JOURNALNAMEID,
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       journal.POSTEDDATETIME,
                                       Convert.ToInt32(result.QUANTITY_DO),
                                       Convert.ToInt32(result.QUANTITY_PICK),
                                       result.QUANTITY_SL);
        }

        public List<SALESTABLE_AV_PICK> ListAVPick(String category_product)
        {
            IQueryable<SALESTABLE_AV_PICK> search = SalesAVPicksSync();
            IQueryable<SALESTABLE_AV_PICK> search_in = JournalAVPicksSync(category_product);

            var mergedlist = search.Union(search_in).ToList();
            return mergedlist;
        }

        public List<SALESTABLE_AV_PICK> ListAVPick(String category_product, int year, String status, String filterSO = "")
        {
            Context.CommandTimeout = 6000;
            if (filterSO == null) filterSO = "";

            if (status == "U")
            {
                List<SALESTABLE_AV_PICK> search = SalesAVPicksSync(year).ToList()//.Where(
                     //   x => (x.QUANTITY_DO > 0 && x.QUANTITY_DO > x.QUANTITY_SL && x.QUANTITY_PICK < x.QUANTITY_DO)
                    //)
                        .OrderBy(y => y.DOID).ToList();
                List<SALESTABLE_AV_PICK> search_in = JournalAVPicksSync(category_product, year).ToList().Where(
                        x => (x.QUANTITY_DO > 0 && x.QUANTITY_DO > x.QUANTITY_SL && x.QUANTITY_PICK < x.QUANTITY_DO)
                    ).OrderBy(y => y.DOID).ToList();

                var mergedlist = search.Union(search_in).ToList();
                return mergedlist;
            }
            else if (status == "A")
            {
                List<SALESTABLE_AV_PICK> search = SalesAVPicksSync(year).ToList().Where(
                        x => (x.QUANTITY_DO > 0 && x.QUANTITY_DO > x.QUANTITY_SL)
                    ).OrderBy(y => y.DOID).ToList();
                List<SALESTABLE_AV_PICK> search_in = JournalAVPicksSync(category_product, year).ToList().Where(
                        x => (x.QUANTITY_DO > 0 && x.QUANTITY_DO > x.QUANTITY_SL)
                    ).OrderBy(y => y.DOID).ToList();

                var mergedlist = search.Union(search_in).ToList();
                return mergedlist;
            }

            return new List<SALESTABLE_AV_PICK>();
        }

        public CUSTINVOICEJOUR Item(string do_id, string dataarea_id)
        {
            Context.CommandTimeout = 6000;
            try
            {
                var itemsObj = from salestable in Context.CUSTINVOICEJOURs
                               where (salestable.INVOICEID == do_id)
                               && (salestable.DATAAREAID == dataarea_id)
                               select salestable;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
