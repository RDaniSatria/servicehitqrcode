﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.TroliPart
{
    public interface ITroliPartObject
    {
        List<MPMWMS_TROLI_PART> List();
        List<MPMWMS_TROLI_PART> List(String dataarea_id);
        List<MPMWMS_TROLI_PART> ListByEmptyTrolley(String dataarea_id, String pic);
    }
}
