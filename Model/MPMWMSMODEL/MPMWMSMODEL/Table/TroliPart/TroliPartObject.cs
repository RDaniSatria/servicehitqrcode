﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.TroliPart
{
    public class TroliPartObject: MPMDbObject<WmsUnitDataContext>, ITroliPartObject
    {
        public TroliPartObject()
            : base()
        {
        }

        public List<MPMWMS_TROLI_PART> List()
        {
            return Context.MPMWMS_TROLI_PARTs.ToList();
        }

        public List<MPMWMS_TROLI_PART> List(String dataarea_id)
        {
            try
            {
                /*var itemsObj =
                    from troli in Context.MPMWMS_TROLI_PARTs
                    from picking in Context.WMSPICKINGROUTEs
                    where
                        (troli.DATAAREA_ID == "mml")
                        && (troli.TROLI_ID == picking.XTSTROLLEYNBR)
                        && (picking.DATAAREAID == troli.DATAAREA_ID)
                        && (picking.XTSLOCKEDUSERID != "")
                    orderby troli.ORDER ascending
                    select troli;*/

                var a =
                    from picking in Context.WMSPICKINGROUTEs
                    where
                        picking.DATAAREAID == dataarea_id
                        && picking.XTSLOCKEDUSERID != ""
                    select new { TROLLEY_ID = picking.XTSTROLLEYNBR };

                var b = a.Union(
                        from packing in Context.CUSTPACKINGSLIPJOURs
                        where
                            packing.DATAAREAID == dataarea_id
                            && packing.XTSLOCKEDUSERID == UserSession.NPK
                        select new { TROLLEY_ID = packing.XTSTROLLEYNBR }
                    );
                    
                var c =
                    from troli in Context.MPMWMS_TROLI_PARTs
                    from cc in b
                    where
                        troli.TROLI_ID == cc.TROLLEY_ID
                    orderby troli.ORDER ascending
                    select troli;
                return c.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_TROLI_PART> ListByEmptyTrolley(String dataarea_id, String pic)
        {
            try
            {
                /*
                String QueryitemsObj = 
                     "   select TROLI_ID, DATAAREA_ID, STATUS,  [ORDER], CREATE_BY, CREATE_DATE, MODIF_BY, MODIF_DATE " +
                     "   from MPMWMS_TROLI_PART(nolock) a "+
                     "   where "+
                     "   DATAAREA_ID = '"+ dataarea_id +"' and "+
                     "   TROLI_ID not in " +
                     "   ( "+
                     "     select XTSTROLLEYNBR "+
	                 "       from WMSPICKINGROUTE(nolock) "+
	                 "       where "+
	                 "       DATAAREAID = '"+ dataarea_id +"' and "+
                     "     XTSTROLLEYNBR !='' and " +
                     "     EXPEDITIONSTATUS = 3 and "+
                     "     XTSLOCKEDUSERID != '" + pic +"' and "+
                     "     XTSLOCKEDUSERID != '' "+
                     "   ) ORDER BY a.[order] asc ";
                     */
                String QueryitemsObj =
                    "SELECT TROLI_ID = " +
                    "CASE WHEN TROLI_ID = " +
                    "    ( " +
                    "       SELECT DISTINCT XTSTROLLEYNBR " +
                    "       FROM MPMWMS_MAP_SO_PICAREA(NOLOCK) P " +
                    "       JOIN WMSORDERTRANS(NOLOCK) O ON P.SOID = O.INVENTTRANSREFID AND P.DATAAREAID = O.DATAAREAID " +
                    "       JOIN WMSPICKINGROUTE(NOLOCK)R ON R.PICKINGROUTEID = O.ROUTEID AND O.INVENTTRANSREFID = R.TRANSREFID AND R.DATAAREAID = O.DATAAREAID " +
                    "       WHERE P.DATAAREAID = '" + dataarea_id + "' AND " +
                    "       R.XTSTROLLEYNBR != '' AND " +
                    "       (R.EXPEDITIONSTATUS = 3 OR R.EXPEDITIONSTATUS = 10) AND " +
                    "       R.XTSLOCKEDUSERID = '" + pic + "' AND " +
                    "       O.MPMSTATUSPICKING < 2 " +
                    "    ) " +
                    "THEN TROLI_ID + ' (Current)' ELSE TROLI_ID END, " +
                    "DATAAREA_ID, STATUS, [ORDER], CREATE_BY, CREATE_DATE, MODIF_BY, MODIF_DATE " +
                    "FROM " +
                    "    MPMWMS_TROLI_PART A " +
                    "WHERE" +
                    "    DATAAREA_ID = '" + dataarea_id + "' AND " +
                    "    TROLI_ID NOT IN " +
                    "    ( " +
                    "           SELECT XTSTROLLEYNBR " +
                    "           FROM MPMWMS_MAP_SO_PICAREA(NOLOCK) P " +
                    "           JOIN WMSPICKINGROUTE(NOLOCK)R ON P.SOID = R.TRANSREFID AND R.DATAAREAID = P.DATAAREAID " +
                    "            WHERE P.DATAAREAID = '" + dataarea_id + "' AND " +
                    "                R.XTSTROLLEYNBR != '' AND " +
                    "                 (R.EXPEDITIONSTATUS = 3 OR R.EXPEDITIONSTATUS = 10) AND " +
                    "                R.XTSLOCKEDUSERID != '" + pic + "' " +
                    "            UNION " +
                    "            SELECT XTSTROLLEYNBR " +
                    "            FROM MPMWMS_MAP_SO_PICAREA(NOLOCK) P " +
                    "            JOIN WMSORDERTRANS(NOLOCK) O ON P.SOID = O.INVENTTRANSREFID AND P.DATAAREAID = O.DATAAREAID " +
                    "            JOIN WMSPICKINGROUTE (NOLOCK)R ON R.PICKINGROUTEID = O.ROUTEID AND O.INVENTTRANSREFID = R.TRANSREFID AND R.DATAAREAID = O.DATAAREAID " +
                    "            WHERE " +
                    "                P.DATAAREAID = '" + dataarea_id + "' AND " +
                    "                R.XTSTROLLEYNBR != '' AND " +
                    "                (R.EXPEDITIONSTATUS = 3 OR R.EXPEDITIONSTATUS = 10) AND " +
                    "                O.MPMSTATUSPICKING = 2 " +
                    "    ) ";
                    //"ORDER BY A.[ORDER] ASC ";

                List<MPMWMS_TROLI_PART> List = Context.ExecuteQuery<MPMWMS_TROLI_PART>(QueryitemsObj).ToList();
                return List;
                

                /*
                var itemsObj = from troli in Context.MPMWMS_TROLI_PARTs
                               where 
                               troli.DATAAREA_ID == dataarea_id &&
                               !Context.WMSPICKINGROUTEs.Any
                               (
                                 route=>
                                 route.DATAAREAID == dataarea_id &&
                                 route.XTSTROLLEYNBR == troli.TROLI_ID &&
                                 route.EXPEDITIONSTATUS == 3 &&
                                 route.XTSLOCKEDUSERID != pic
                               )
                               orderby troli.ORDER ascending
                               select troli;

                return itemsObj.ToList(); 
                */
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        #region UNTUK APP PACKING DI WINDOWS FORM

        public List<MPMWMS_TROLI_PART> TrolleyCurrent(String _dataArea, String _npk)
        {
            String QueryitemsObj =
                "SELECT" +
                "    DISTINCT TROLI_ID = A.XTSTROLLEYNBR  + ' - (Current)', DATAAREA_ID = A.DATAAREAID, STATUS = 'A', [ORDER] = 0" +
                "FROM " +
                "    CUSTPACKINGSLIPJOUR (NOLOCK) A " +
                "LEFT JOIN " +
                "    CUSTINVOICEJOUR I ON A.SALESID = I.SALESID AND A.DATAAREAID = I.DATAAREAID " +
                "JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) C ON A.SALESID = C.SOID AND A.DATAAREAID = C.DATAAREAID AND A.INVOICEACCOUNT = C.DEALERID " +
                "WHERE" +
                "    I.SALESID IS NULL AND " +
                "    A.DATAAREAID = '" + _dataArea + "' AND " +
                "    XTSTROLLEYNBR <> '' AND " +
                "    XTSLOCKEDUSERID = '" + _npk + "' ";

            List<MPMWMS_TROLI_PART> List = Context.ExecuteQuery<MPMWMS_TROLI_PART>(QueryitemsObj).ToList();
            return List;
        }

        public List<String> ListTrolleyActive(String _dataArea, String _npk)
        {
            /* RD ~ COMMENT 8 DES 2017
            var dataPacking = from packing in TrolleyCurrent(_dataArea, _npk)
                              select new { TROLLEY_ID = packing.TROLI_ID };

            var dataGabungan = dataPacking.Union(
                            from order in Context.WMSORDERTRANs
                                where order.MPMSTATUSPICKING == 2
                                    && order.XTSPACKINGSLIPID == ""
                            join picking in Context.WMSPICKINGROUTEs on new { A = order.ROUTEID, B = order.PARTITION, C = order.DATAAREAID, D = order.INVENTTRANSREFID }
                                                                equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                                //where picking.XTSLOCKEDUSERID == ""
                            join salestable in Context.SALESTABLEs on new { TRANSREFID = picking.TRANSREFID }
                                                               equals new { TRANSREFID = salestable.SALESID }
                                where salestable.SALESSTATUS < 3 
                            join troli in Context.MPMWMS_TROLI_PARTs on picking.XTSTROLLEYNBR equals troli.TROLI_ID
                            orderby troli.ORDER ascending
                            select new { TROLLEY_ID = troli.TROLI_ID });

            var dataTroli = from uni in dataGabungan.GroupBy(x => x.TROLLEY_ID)
                            .Select(data => new { TROLLEY_ID = data.Key })
                            select uni.TROLLEY_ID;

            return dataTroli.ToList();
            */

            String QueryitemsObj =
                " SELECT DISTINCT TROLI_ID = A.XTSTROLLEYNBR + ' - (Current)'                                                                    " +
                " FROM CUSTPACKINGSLIPJOUR (NOLOCK) A                                                                                            " +
                " LEFT JOIN CUSTINVOICEJOUR (NOLOCK) I ON A.SALESID = I.SALESID AND A.DATAAREAID = I.DATAAREAID                                  " +
                " JOIN MPMWMS_MAP_SO_PICAREA (NOLOCK) C ON A.SALESID = C.SOID AND A.DATAAREAID = C.DATAAREAID AND A.INVOICEACCOUNT = C.DEALERID  " +
                " WHERE I.SALESID IS NULL AND                                                                                                    " +
                " A.DATAAREAID =  '" + _dataArea + "' AND                                                                                        " +
                " XTSTROLLEYNBR <> '' AND                                                                                                        " +
                " XTSLOCKEDUSERID = '" + _npk + "'                                                                                               " +
                "                                                                                                                                " +
                " UNION                                                                                                                          " +
                "                                                                                                                                " +
                " SELECT T.TROLI_ID                                                                                                              " +
                " FROM WMSORDERTRANS (NOLOCK) O                                                                                                  " +
                " JOIN WMSPICKINGROUTE (NOLOCK) R ON O.ROUTEID = R.PICKINGROUTEID AND O.PARTITION = R.PARTITION                                  " +
                " 				AND O.DATAAREAID = R.DATAAREAID AND O.INVENTTRANSREFID = R.TRANSREFID                                            " +
                " JOIN CUSTPACKINGSLIPJOUR (NOLOCK) S ON R.TRANSREFID = S.SALESID AND R.PARTITION = S.PARTITION AND R.DATAAREAID = S.DATAAREAID  " +
                " JOIN MPMWMS_TROLI_PART (NOLOCK) T ON T.TROLI_ID = R.XTSTROLLEYNBR                                                              " +
                " LEFT JOIN CUSTINVOICEJOUR (NOLOCK)I ON I.SALESID = R.TRANSREFID                                                                " + 
                " WHERE O.MPMSTATUSPICKING = '2'                                                                                                 " +
                " AND I.SALESID IS NULL                                                                                                          " +
                " AND S.XTSLOCKEDUSERID = ''                                                                                                     " +  
                " AND O.XTSPACKINGSLIPID = ''                                                                                                    ";

            List <String> List = Context.ExecuteQuery<String>(QueryitemsObj).ToList();
            return List;
        }

        #endregion
    }
}
