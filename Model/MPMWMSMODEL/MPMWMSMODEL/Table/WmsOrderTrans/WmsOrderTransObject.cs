﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InventDim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.WmsOrderTrans
{
    public class WMSORDERTRAN_CUSTOM
    {
        public String SALES_ID { get; set; }
        public String ROUTE_ID { get; set; }
        public String DEALER { get; set; }
        public String DATAAREA_ID { get; set; }
        public String INVENTDIM_ID { get; set; }
        public String SITE_ID { get; set; }
        public String RACK_ID { get; set; }
        public String ITEM_ID { get; set; }
        public int EXPEDITION_STATUS { get; set; }
        public int MPMSTATUSPICKING { get; set; }
        public int QTY { get; set; }
        public int UNPICK_QTY { get; set; }
        public long REC_ID { get; set; }
        public int LEN_RACK_ID { get; set; }

        public WMSORDERTRAN_CUSTOM()
        {
        }
    }

    public class WmsOrderTransObject: MPMDbObject<WmsUnitDataContext>, IWmsOrderTransObject
    {
        private InventDimObject _InventDimObject = new InventDimObject();

        public WmsOrderTransObject()
            : base()
        {
        }

        public InventDimObject InventDimObject
        {
            get { return _InventDimObject; }
        }

        public List<WMSORDERTRAN> List(WMSPICKINGROUTE route)
        {
            IQueryable<WMSORDERTRAN> search =
                from tran in Context.WMSORDERTRANs
                where
                    tran.ROUTEID == route.PICKINGROUTEID
                    && tran.DATAAREAID == route.DATAAREAID
                select tran;
            return search.ToList();
        }

        public List<WMSORDERTRAN_CUSTOM> List(String dataarea_id, String pic, String trolley_id)
        {
            IQueryable<WMSORDERTRAN> search =
                from tran in Context.WMSORDERTRANs
                join route in Context.WMSPICKINGROUTEs on
                    new { route_id = tran.ROUTEID, dataarea = tran.DATAAREAID } equals
                    new { route_id = route.PICKINGROUTEID, dataarea = route.DATAAREAID } into route_join
                from result in route_join.DefaultIfEmpty()
                where
                    tran.DATAAREAID == dataarea_id
                    && result.XTSLOCKEDUSERID == pic
                    && result.XTSTROLLEYNBR ==  trolley_id
                    && result.EXPEDITIONSTATUS == 3
                select tran;

            List<WMSORDERTRAN_CUSTOM> custom = new List<WMSORDERTRAN_CUSTOM>();
            foreach (var detail in search.ToList())
            {
                WMSORDERTRAN_CUSTOM item = new WMSORDERTRAN_CUSTOM();
                item.SALES_ID = detail.INVENTTRANSREFID;
                item.REC_ID = detail.RECID;
                item.ROUTE_ID = detail.ROUTEID;
                item.DEALER = detail.CUSTOMER;
                item.DATAAREA_ID = detail.DATAAREAID;
                item.INVENTDIM_ID = detail.INVENTDIMID;
                item.SITE_ID = InventDimObject.Item(item.DATAAREA_ID, item.INVENTDIM_ID).INVENTSITEID;
                item.RACK_ID = InventDimObject.Item(item.DATAAREA_ID, item.INVENTDIM_ID).INVENTLOCATIONID;
                item.LEN_RACK_ID = item.RACK_ID.Length;
                item.ITEM_ID = detail.ITEMID;
                item.EXPEDITION_STATUS = detail.EXPEDITIONSTATUS;
                item.MPMSTATUSPICKING = detail.MPMSTATUSPICKING;
                item.QTY = Convert.ToInt32(detail.QTY) + Convert.ToInt32(detail.XTSUNPICKQTY);
                item.UNPICK_QTY = item.QTY - Convert.ToInt32(detail.XTSUNPICKQTY);
                custom.Add(item);
            }
            return custom.OrderBy(x => x.LEN_RACK_ID).ThenBy(x => x.RACK_ID).ToList();
        }

        public WMSORDERTRAN Item(String dataarea_id, String route_id, String item_id)
        {
            IQueryable<WMSORDERTRAN> search =
                from tran in Context.WMSORDERTRANs
                where
                    tran.ROUTEID == route_id
                    && tran.DATAAREAID == dataarea_id
                    && tran.ITEMID == item_id
                select tran;
            return search.Take(1).FirstOrDefault();
        }

        public WMSORDERTRAN Item(string dataarea_id, string route_id, string item_id, string inventdim_id)
        {
            IQueryable<WMSORDERTRAN> search =
                from tran in Context.WMSORDERTRANs
                where
                    tran.ROUTEID == route_id
                    && tran.DATAAREAID == dataarea_id
                    && tran.ITEMID == item_id
                    && tran.INVENTDIMID == inventdim_id
                select tran;
            return search.Take(1).FirstOrDefault();
        }

        public WMSORDERTRAN ItemByRecId(string dataarea_id, string route_id, string item_id, string inventdim_id, long rec_id)
        {
            IQueryable<WMSORDERTRAN> search =
                from tran in Context.WMSORDERTRANs
                where
                    tran.ROUTEID == route_id
                    && tran.DATAAREAID == dataarea_id
                    && tran.ITEMID == item_id
                    && tran.INVENTDIMID == inventdim_id
                    && tran.RECID == rec_id
                select tran;
            return search.Take(1).FirstOrDefault();
        }

        #region FOR PACKING DEKSTOP APP

        public List<WMSORDERTRAN> ItemsPacking(String _pickingRouteId, String _dataArea)
        {
            IQueryable<WMSORDERTRAN> search =
                    from tran in Context.WMSORDERTRANs
                    where tran.ROUTEID == _pickingRouteId
                        && tran.DATAAREAID == _dataArea
                        && tran.MPMSTATUSPICKING == 2
                        && tran.EXPEDITIONSTATUS != 20 // NOT CANCELED
                    select tran;
            
            return search.ToList();
        }

        #endregion
    }
}
