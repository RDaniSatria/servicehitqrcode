﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.WmsOrderTrans
{
    public interface IWmsOrderTransObject
    {
        List<WMSORDERTRAN> List(WMSPICKINGROUTE route);
    }
}
