﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.Stock;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DashboardStock
{
    public class DashboardStockByKelBarangObject: MPMDbObject<WmsUnitDataContext>
    {
        public DashboardStockByKelBarangObject()
            : base()
        {
        }

        private static Func<WmsUnitDataContext, string, string, string, IQueryable<DASHBOARD_STOCK_BY_KEL_BARANG_REC>>
          StockQuery =
               CompiledQuery.Compile((WmsUnitDataContext db, string dataareaid, string mdcode, string siteid)
                   =>
               from a in db.MPM_WMS_DASHBOARD_STOCK_KELBRG_SP(siteid, dataareaid, mdcode)
               select new DASHBOARD_STOCK_BY_KEL_BARANG_REC(
                   a.KELBARANG,
                   a.DETAIL,
                   a.GROUPPARTNUMBER,
                   a.AVERAGE_POD,
                   a.POD_MTD,
                   a.AVERAGE_SO,
                   a.SO_MTD,
                   a.STOCK,
                   a.MIN_,
                   a.MAX_,
                   a.INTRANSIT,
                   a.PO_FIX_AHM,
                   a.PO_REG_AHM,
                   a.SUPPLY_AHM
                   )
           );

        public List<DASHBOARD_STOCK_BY_KEL_BARANG_REC> LaporanList()
        {
            try
            {
                string dataareaid = UserSession.DATAAREA_ID;
                string mdcode = UserSession.MAINDEALER_ID;
                string siteid = UserSession.SITE_ID_MAPPING;
                Context.CommandTimeout = 240;  // set timeout to 4 minutes
                var result = StockQuery((WmsUnitDataContext)Context, dataareaid, mdcode, siteid);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<DASHBOARD_STOCK_BY_KEL_BARANG_REC> List()
        {
            try
            {
                string dataareaid = UserSession.DATAAREA_ID;
                string mdcode = UserSession.MAINDEALER_ID;
                string siteid = UserSession.SITE_ID_MAPPING;
                Context.CommandTimeout = 240;  // set timeout to 4 minutes
                var result = StockQuery((WmsUnitDataContext)Context, dataareaid, mdcode, siteid).OrderByDescending(x => x.AVERAGE_SO).Take(50);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
