﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.DataArea;
using MPMWMSMODEL.Table.MutationSiteLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.MutationSite
{
    public class MutationSiteObject : MPMDbObject<WmsUnitDataContext>, IMutationSiteObject
    {
        private MutationSiteLineObject _MutationSiteLineObject = new MutationSiteLineObject();

        public MutationSiteObject()
            : base()
        {
        }

        public MutationSiteLineObject MutationSiteLineObject
        {
            get { return _MutationSiteLineObject; }
        }

        public MPMWMS_MUTATION_SITE LastItem(String machine_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
                IQueryable<MPMWMS_MUTATION_SITE> search =
                     from line in Context.MPMWMS_MUTATION_SITE_LINEs
                     where
                          line.MACHINE_ID == machine_id
                     join mutation in Context.MPMWMS_MUTATION_SITEs on line.MUTATION_ID equals mutation.MUTATION_ID
                     orderby line.CREATE_DATE descending
                     select mutation;
                return search.Take(1).FirstOrDefault();
            }
        }

        public MPMWMS_MUTATION_SITE Item(String dataarea_id, String mutation_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
                IQueryable<MPMWMS_MUTATION_SITE> search =
                    from mutation in Context.MPMWMS_MUTATION_SITEs
                    where
                        mutation.DATAAREA_ID == dataarea_id
                        && mutation.MUTATION_ID == mutation_id
                    select mutation;

                return search.Take(1).FirstOrDefault();
            }
        }

        public List<MPMWMS_MUTATION_SITE> List()
        {
            try
            {
                using (
                    var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    var itemsObj = from mutation in Context.MPMWMS_MUTATION_SITEs
                                   where
                                   (mutation.DATAAREA_ID == UserSession.DATAAREA_ID)
                                   && (
                                   (
                                        mutation.SITE_ID == UserSession.SITE_ID_MAPPING)
                                        ||
                                        (mutation.SITE_ID_NEW == UserSession.SITE_ID_MAPPING && mutation.STATUS != "O")
                                    )
                                   select mutation;
                    return itemsObj.ToList();
                }
            }
            catch (MPMException)
            {
                return null;
            }
            //return Context.MPMWMS_MUTATION_SITEs.ToList();
        }

        public MPMWMS_MUTATION_SITE Create()
        {
            MPMWMS_MUTATION_SITE item = new MPMWMS_MUTATION_SITE();
            item.STATUS = "O";
            return item;
        }


        public void Insert(MPMWMS_MUTATION_SITE row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_MUTATION_SITEs.InsertOnSubmit(row);
        }

        public void Delete(MPMWMS_MUTATION_SITE row)
        {
            try
            {
                //TO DO DELETE MUTATION
                if (row.STATUS.Equals("P"))
                {
                    throw new MPMException("Can't delete because already Posted");
                }
                else if (row.STATUS.Equals("HR"))
                {
                    throw new MPMException("Can't delete because already Received");
                }
                else if (row.STATUS.Equals("FR"))
                {
                    throw new MPMException("Can't delete because already Received");
                }
                else if (row.STATUS.Equals("O"))
                {
                    Context.MPMWMS_MUTATION_SITEs.DeleteOnSubmit(row);
                }
                else
                {
                    throw new MPMException("Can't be deleted");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MUTATION_SITE row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public MPMWMS_UNIT ItemUnit(String dataarea_id, String machine)
        {
            try
            {
                using (
                    var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    var itemsObj = from unit in Context.MPMWMS_UNITs
                                   where
                                   (unit.FRAME_ID == machine || unit.MACHINE_ID == machine) &&
                                   (unit.DATAAREA_ID == dataarea_id)
                                   select unit;
                    var currentItem = itemsObj.Take(1).FirstOrDefault();
                    return currentItem;
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_MUTATION_SITE> ListPosted(String dataarea_id, String site_id_destination)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
                IQueryable<MPMWMS_MUTATION_SITE> search =
                    from mutation in Context.MPMWMS_MUTATION_SITEs
                    where
                        mutation.DATAAREA_ID == dataarea_id
                        && (mutation.STATUS == "P" || mutation.STATUS == "HR")
                        && mutation.SITE_ID_NEW == site_id_destination
                    select mutation;

                return search.ToList();
            }
        }
    }
}
