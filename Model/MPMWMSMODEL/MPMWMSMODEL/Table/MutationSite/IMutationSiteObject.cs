﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.MutationSite
{
    public interface IMutationSiteObject
    {
        MPMWMS_MUTATION_SITE Item(String dataarea_id, String mutation_id);
        List<MPMWMS_MUTATION_SITE> List();
        List<MPMWMS_MUTATION_SITE> ListPosted(String dataarea_id, String site_id);
        MPMWMS_MUTATION_SITE Create();
        void Insert(MPMWMS_MUTATION_SITE row);
        void Update(MPMWMS_MUTATION_SITE row);
        void Delete(MPMWMS_MUTATION_SITE row);
    }
}
