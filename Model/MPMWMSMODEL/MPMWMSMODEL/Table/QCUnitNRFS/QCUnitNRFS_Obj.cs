﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.QCUnitNRFS
{
    public class QCUnitNRFS_Obj : MPMDbObject<WmsUnitDataContext>
    {
        public QCUnitNRFS_Obj() : base()
        {
            Context = new WmsUnitDataContext();
        }


        public MPMWMSHISTCLOCKING ItemGridHeader(string Nosin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             where a.NOMESIN == Nosin
                             select a;
                return result.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }

        public MPMWMS_UNIT ItemUnit(string Nosin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.MACHINE_ID == Nosin
                             select a;
                return result.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }
        public void updateUnit(MPMWMS_UNIT item)
        {
        }

        public void UpdateHeader(MPMWMSHISTCLOCKING item)
        {
        }

        public List<QCUnit_Rec> ListItem(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.MACHINE_ID == NoMesin && a.STATUS == "C" && a.SITE_ID == UserSession.SITE_ID_MAPPING
                             select new QCUnit_Rec
                             {
                                 NoUnit = a.MACHINE_ID,
                                 storingdate = a.STORING_DATE == null ? "" : a.STORING_DATE.ToString() 
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCClocking_Rec> ListClocking(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             where a.NOMESIN == NoMesin
                             select new QCClocking_Rec
                             {
                                 NoUnit = a.NOMESIN,
                                 StatusKerja = a.STATUSKERJA,
                                 StatusQc = a.STATUSQC

                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCListDetil_Rec> ListDetaiData(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             where a.NOMERMESIN.Replace(" ", "").Contains(NoMesin)
                             select new QCListDetil_Rec
                             {
                                 PartName = a.PARTNAME,
                                 ItemPair = a.ITEMREPAIR,
                                 KodePart = a.KODEPART
                             };
                return result.AsEnumerable() // Client-side from here on
                    .Select((player, index) => new QCListDetil_Rec()
                    {
                        PartName = player.PartName,
                        ItemPair = player.ItemPair,
                        KodePart = player.KodePart,
                        No = index + 1
                    })
                    .ToList(); ;
            }
            catch (MPMException)
            {
                return null;
            }
        }


        public List<QCListDetil_Rec> ListDetaiDataNRFS(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMDETAILNONLKUs
                             join b in ((WmsUnitDataContext)Context).MPMSERVVIEWDAFTARPARTs
                             on a.ITEM equals b.ITEMID into gj
                             from sub in gj.DefaultIfEmpty()
                             where a.NOMESIN.Contains(NoMesin)
                             select new QCListDetil_Rec
                             {
                                 PartName = a.ITEM,
                                 ItemPair = a.TYPEREPAIR == "Bongkar Part" ? sub.ITEMNAME : "-",
                                 KodePart = a.TYPEREPAIR
                             };
                return result.AsEnumerable() // Client-side from here on
                    .Select((player, index) => new QCListDetil_Rec()
                    {
                        PartName = player.PartName,
                        ItemPair = player.ItemPair,
                        KodePart = player.KodePart,
                        No = index + 1
                    })
                    .ToList(); ;
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCDetilUnit_Rec> ListData(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKUHEADERs
                             join b in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             on new { MPMSALLKUHEADER = a.RECID, a.DATAAREAID, a.PARTITION } equals new { b.MPMSALLKUHEADER, b.DATAAREAID, b.PARTITION }
                             join c in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             on b.NOMERMESIN.Replace(" ", "") equals c.NOMESIN
                             where b.NOMERMESIN.Replace(" ", "").Contains(NoMesin)
                             group new { a, c } by new { a.TGLLKU, c.NOMESIN, c.USERID, c.STATUSKERJA, c.TGLJAMMULAI, c.TGLJAMSELESAI } into g
                             select new QCDetilUnit_Rec
                             {
                                 TglLku = g.Key.TGLLKU.ToShortDateString(),
                                 Status = g.Key.STATUSKERJA == 0 ? "Open" : g.Key.STATUSKERJA == 1 ? "On Progress" : g.Key.STATUSKERJA == 2 ? "Pending" : "Done",
                                 Teknisi = g.Key.USERID,
                                 Unit = g.Key.NOMESIN,
                                 TglMulai = g.Key.TGLJAMMULAI.Value.ToShortDateString(),
                                 TglSelesai = g.Key.TGLJAMSELESAI.Value.ToShortDateString()
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCDetilUnit_Rec> ListDataNRFS(string NoMesin)
        {
            try
            {
                Context.CommandTimeout = 5000000;
                var data = (from a in ((WmsUnitDataContext)Context).MPMWMS_HISTORY_UNITs
                            where a.WAREHOUSE_ID.Contains("NRFS") && !a.WAREHOUSE_ID_OLD.Contains("NRFS")
                            && a.MACHINE_ID == NoMesin
                            orderby a.CREATE_DATE descending
                            select new QCHistory
                            {
                                No = a.MACHINE_ID,
                                tanggalnrfs = a.CHANGES_DATE.Date.ToString()
                            }).Take(1).FirstOrDefault();
                if(data == null)
                {
                    QCHistory history = new QCHistory();
                    history.No = NoMesin;
                    history.tanggalnrfs = "";
                    List<QCHistory> qc = new List<QCHistory>();
                    qc.Add(history);
                    data = qc.Take(1).FirstOrDefault();
                }

                var result = from c in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             where c.NOMESIN.Contains(NoMesin)
                             group c by new { c.NOMESIN, c.USERID, c.STATUSKERJA, c.TGLJAMMULAI, c.TGLJAMSELESAI } into g
                             select new QCDetilUnit_Rec
                             {
                                 TglLku = data.tanggalnrfs ,
                                 Status = g.Key.STATUSKERJA == 0 ? "Open" : g.Key.STATUSKERJA == 1 ? "On Progress" : g.Key.STATUSKERJA == 2 ? "Pending" : "Done",
                                 Teknisi = g.Key.USERID,
                                 Unit = g.Key.NOMESIN,
                                 TglMulai = g.Key.TGLJAMMULAI.Value.ToShortDateString(),
                                 TglSelesai = g.Key.TGLJAMSELESAI.Value.ToShortDateString()
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCUnit_Rec> ListValidLKU(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMSALLKULINEs
                             where a.NOMERMESIN.Replace(" ", "").Contains(NoMesin)
                             select new QCUnit_Rec
                             {
                                 NoUnit = a.NOMERMESIN
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<QCLocationUnit_Rec> ListLocation(string NoMesin)
        {
            try
            {
                var result = from a in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                             join b in ((WmsUnitDataContext)Context).MPMWMSHISTCLOCKINGs
                             on a.MACHINE_ID equals b.NOMESIN
                             where a.MACHINE_ID.Contains(NoMesin)
                             select new QCLocationUnit_Rec
                             {
                                 Unit = a.MACHINE_ID,
                                 LokasiAwal = a.WAREHOUSE_ID + " - " + a.LOCATION_ID,
                                 Status = b.STATUSKERJA == 0 ? "Open" : b.STATUSKERJA == 1 ? "On Progress" : b.STATUSKERJA == 2 ? "Pending" : "Done",
                                 Teknisi = b.USERID,
                                 TglMulai = b.TGLJAMMULAI.Value.ToShortDateString(),
                                 TglSelesai = b.TGLJAMSELESAI.Value.ToShortDateString()
                             };
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
