﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.QCUnitNRFS
{
    public class QCUnitNRFS_Rec
    {
    }

    public class QCUnit_Rec
    {
        public string NoUnit { set; get; }
        public string storingdate { set; get; }
    }

    public class QCClocking_Rec
    {
        public string NoUnit { set; get; }
        public int? StatusKerja { set; get; }
        public int? StatusQc { set; get; }
    }

    public class QCDetilUnit_Rec
    {
        public string Unit { set; get; }
        public string TglLku { set; get; }
        public string Teknisi { set; get; }
        public string Status { set; get; }
        public string TglMulai { set; get; }
        public string TglSelesai { set; get; }
    }
    public class QCLocationUnit_Rec
    {
        public string Unit { set; get; }
        public string LokasiAwal { set; get; }
        public string Teknisi { set; get; }
        public string Status { set; get; }
        public string TglMulai { set; get; }
        public string TglSelesai { set; get; }
    }

    public class QCListDetil_Rec
    {
        public int No { set; get; }
        public string KodePart { set; get; }
        public string PartName { set; get; }
        public string ItemPair { set; get; }
    }

    public class QCHistory
    {
        public string No { set; get; }
        public string tanggalnrfs { set; get; }
    }
}
