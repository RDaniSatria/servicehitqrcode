﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventDim
{
    public interface IInventDimObject
    {
        INVENTDIM Item(String dataarea_id, String invent_dim_id);
    }
}
