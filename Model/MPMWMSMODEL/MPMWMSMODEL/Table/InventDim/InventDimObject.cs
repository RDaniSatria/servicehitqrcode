﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventDim
{
    public class InventDimObject: MPMDbObject<WmsUnitDataContext>, IInventDimObject
    {
        public InventDimObject(): base()
        {
        }

        public INVENTDIM Item(String dataarea_id, String invent_dim_id)
        {
            IQueryable<INVENTDIM> search =
                from invent in ((WmsUnitDataContext)Context).INVENTDIMs
                where
                    invent.INVENTDIMID == invent_dim_id
                    && invent.DATAAREAID == dataarea_id
                select invent;

            return search.Take(1).FirstOrDefault();
        }
    }
}
