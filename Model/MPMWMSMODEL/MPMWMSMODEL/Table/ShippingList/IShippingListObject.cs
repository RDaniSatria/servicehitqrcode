﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingList
{
    public interface IShippingListObject
    {
        List<MPMWMS_SHIPPING_LIST> List();
        List<MPMWMS_SHIPPING_LIST> ListPrint(String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_SHIPPING_LIST Create();
        void Insert(MPMWMS_SHIPPING_LIST row);
        MPMWMS_SHIPPING_LIST Item(String shipping_list_id, String dataarea_id, String maindealer_id, String site_id);
    }
}
