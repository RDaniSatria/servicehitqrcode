﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingList
{
    public class ShippingListRec
    {
        public string IS_PRINT{ get; set; }
        public string SHIPPING_LIST_ID { get; set; }
        public DateTime SHIPPING_DATE{ get; set; }
        public string INVOICE_ID_AX{ get; set; }
        public string DATAAREA_ID { get; set; }
        public string PICKING_ID{ get; set; }
        public DateTime PICKING_DATE { get; set; }
        public string DESCRIPTION{ get; set; }
        public string DO_ID{ get; set; }
        public string DEALER_CODE{ get; set; }
        public string DEALER_NAME{ get; set; }
        public string DEALER_ADDRESS{ get; set; }
        public string DEALER_CITY{ get; set; }
        public string EXPEDITION_ID{ get; set; }
        public string POLICE_NUMBER{ get; set; }
        public string DRIVER{ get; set; }
        public string IS_CANCEL{ get; set; }
        public string REKAPSL_ID { get; set; }     
        public DateTime? PRINT_DATE { get; set; }
        public int? IS_VISTUAL { get; set; }
        public string NOTE { get; set; }
        public DateTime? TANGGAL_KELUAR_TITIPAN { get; set; }
    }


    public class DetailUMSL
    {
        public string invoiceId { get; set; }
        public string itemId { get; set; }
        public string colorId { get; set; }
        public string noRangka { get; set; }
        public string noMesin { get; set; }
        public int tahunProduksi { get; set; }
        public int unitDisc { get; set; }
    }
    public class HeaderUMSL
    {
        public string principalCode { get; set; }
        public string mdCode { get; set; }
        public string mainDealer { get; set; }
        public string noSL { get; set; }
        public string tglSL { get; set; }
        public string rekapSLID { get; set; }
        public string Ekspedisi { get; set; }
        public string NoPolisi { get; set; }
        public string Driver { get; set; }

        public List<DetailUMSL> detail { get; set; }
    }
    public class ResDetailUMSL
    {
        public string nosl { get; set; }
    }
    public class ResHeaderUMSL
    {
        public int status { get; set; }
        public string message { get; set; }
        public ResDetailUMSL data { get; set; }
    }
    public class DocumentUMSL
    {
        public string PRINCIPALCODE { get; set; }
        public string MDCODE { get; set; }
        public string SHIPPING_LIST_ID { get; set; }
        public string SLD { get; set; }
        public string KDDLR1 { get; set; }
        public string KDDLR2 { get; set; }
        public string UNIT_TYPE { get; set; }
        public string COLOR_TYPE { get; set; }
        public string FRAME_ID { get; set; }
        public string MESIN { get; set; }
        public string UNIT_YEAR { get; set; }
        public string DO_ID { get; set; }
        public string NAME { get; set; }
        public string POLICE_NUMBER { get; set; }
        public string REKAPSL_ID { get; set; }
        public string DRIVER { get; set; }
        public DateTime SLDATE { get; set; }

    }
}
