﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingList
{
    public class MPMWMS_FAKTUR_SL_MAPPING_Record
    {
        public String SHIPPING_LIST_ID { get; set; }
        public DateTime SHIPPING_DATE { get; set; }
        public String DO_ID { get; set; }
        public DateTime DO_DATE { get; set; }
        public String DATAAREA_ID { get; set; }
        public String MAINDEALER_ID { get; set; }
        public String SITE_ID { get; set; }
        public String DEALER_CODE { get; set; }
        public String DEALER_CODE_AX { get; set; }
        public String MACHINE_ID { get; set; }
        public String FRAME_ID { get; set; }
        public String IS_UPDATE_FAKTUR { get; set; }

        public MPMWMS_FAKTUR_SL_MAPPING_Record() { }

        public MPMWMS_FAKTUR_SL_MAPPING_Record
            (
                String shipping_list_id,
                DateTime shipping_date,
                String do_id,
                DateTime do_date,
                String dataarea_id,
                String maindealer_id,
                String site_id,
                String dealer_code,
                String dealer_code_ax,
                String machine_id,
                String frame_id,
                String is_update_faktur
            )
        {
            SHIPPING_LIST_ID = shipping_list_id;
            SHIPPING_DATE = shipping_date;
            DO_ID = do_id;
            DO_DATE = do_date;
            DATAAREA_ID = dataarea_id;
            MAINDEALER_ID = maindealer_id;
            SITE_ID = site_id;
            DEALER_CODE = dealer_code;
            DEALER_CODE_AX = dealer_code_ax;
            MACHINE_ID = machine_id;
            FRAME_ID = frame_id;
            IS_UPDATE_FAKTUR = is_update_faktur;
        }
    }
}
