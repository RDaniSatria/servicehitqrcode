﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.ShippingListLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Util;

namespace MPMWMSMODEL.Table.ShippingList
{
    public class ShippingListObject : MPMDbObject<WmsUnitDataContext>, IShippingListObject
    {
        private ShippingListLineObject _ShippingListLineObject = new ShippingListLineObject();

        public ShippingListObject()
            : base()
        {
        }

        public ShippingListLineObject ShippingListLineObject
        {
            get
            {
                return _ShippingListLineObject;
            }
        }

        public List<MPMWMS_SHIPPING_LIST> List()
        {
            return Context.MPMWMS_SHIPPING_LISTs.ToList();
        }

        public List<MPMWMS_SHIPPING_LIST> List(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj =
                    from shipping in Context.MPMWMS_SHIPPING_LISTs
                    where
                        (shipping.DATAAREA_ID == dataarea_id)
                        && (shipping.MAINDEALER_ID == maindealer_id)
                        && (shipping.SITE_ID == site_id)
                        && (shipping.CREATE_DATE.Value.Date >= MPMDateUtil.DateTime.Date.AddDays(-365))
                    select shipping;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_SHIPPING_LIST> ListCreateToday(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj =
                    from shipping in Context.MPMWMS_SHIPPING_LISTs
                    where
                        (shipping.DATAAREA_ID == dataarea_id)
                        && (shipping.MAINDEALER_ID == maindealer_id)
                        && (shipping.SITE_ID == site_id)
                        && (shipping.CREATE_DATE.Value.Date == MPMDateUtil.DateTime.Date)
                    select shipping;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        //Tambahan Dev.Magang 
        public List<ShippingListRec> ListRec(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                Context.CommandTimeout = 5000000;
                DateTime date = Convert.ToDateTime("2016-04-01");
                var itemsObj = (
                    from shipping in Context.MPMWMS_SHIPPING_LISTs

                    join titipan in Context.MPMWMS_LOKASI_TITIPANs
                    on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }
                    into ps
                    from p in ps.DefaultIfEmpty()
                    where
                        //(shipping.DATAAREA_ID == dataarea_id)
                        //&& (shipping.MAINDEALER_ID == maindealer_id)
                        //&& 
                        (shipping.SITE_ID == site_id)
                        && (shipping.SHIPPING_DATE.Date >= MPMDateUtil.DateTime.Date.AddDays(-365))

                    select new ShippingListRec
                    {
                        DATAAREA_ID = shipping.DATAAREA_ID,
                        DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                        DEALER_CITY = shipping.DEALER_CITY,
                        DEALER_CODE = shipping.DEALER_CODE,
                        DEALER_NAME = shipping.DEALER_NAME,
                        DESCRIPTION = shipping.DESCRIPTION,
                        DO_ID = shipping.DO_ID,
                        DRIVER = shipping.DRIVER,
                        EXPEDITION_ID = shipping.EXPEDITION_ID,
                        INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                        IS_CANCEL = shipping.IS_CANCEL,
                        IS_PRINT = shipping.IS_PRINT,
                        //IS_VISTUAL = picking.IS_VIRTUAL,
                        PICKING_DATE = shipping.PICKING_DATE,
                        PICKING_ID = shipping.PICKING_ID,
                        POLICE_NUMBER = shipping.POLICE_NUMBER,
                        PRINT_DATE = shipping.PRINT_DATE,
                        REKAPSL_ID = shipping.REKAPSL_ID,
                        SHIPPING_DATE = shipping.SHIPPING_DATE,
                        SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                        NOTE = p.NOTE,
                        TANGGAL_KELUAR_TITIPAN = p.TGLKELUAR

                    }).Union(
                    from shipping in Context.MPMWMS_SHIPPING_LISTs
                    join titipan in Context.MPMWMS_LOKASI_TITIPANs
                    on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }

                    where
                        //(shipping.DATAAREA_ID == dataarea_id)
                        //&& (shipping.MAINDEALER_ID == maindealer_id)
                        //&& 
                        (titipan.SITE_ID == site_id)
                        && (titipan.TGLKELUAR.Value.Date >= MPMDateUtil.DateTime.Date.AddDays(-365))

                    select new ShippingListRec
                    {
                        DATAAREA_ID = shipping.DATAAREA_ID,
                        DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                        DEALER_CITY = shipping.DEALER_CITY,
                        DEALER_CODE = shipping.DEALER_CODE,
                        DEALER_NAME = shipping.DEALER_NAME,
                        DESCRIPTION = shipping.DESCRIPTION,
                        DO_ID = shipping.DO_ID,
                        DRIVER = shipping.DRIVER,
                        EXPEDITION_ID = shipping.EXPEDITION_ID,
                        INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                        IS_CANCEL = shipping.IS_CANCEL,
                        IS_PRINT = shipping.IS_PRINT,
                        //IS_VISTUAL = picking.IS_VIRTUAL,
                        PICKING_DATE = shipping.PICKING_DATE,
                        PICKING_ID = shipping.PICKING_ID,
                        POLICE_NUMBER = shipping.POLICE_NUMBER,
                        PRINT_DATE = shipping.PRINT_DATE,
                        REKAPSL_ID = shipping.REKAPSL_ID,
                        SHIPPING_DATE = shipping.SHIPPING_DATE,
                        SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                        NOTE = titipan.NOTE,
                        TANGGAL_KELUAR_TITIPAN = titipan.TGLKELUAR
                    }
                    );
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        //Tambahan Dev.Magang 
        public List<ShippingListRec> ListCreateTodayRec(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {


                Context.CommandTimeout = 5000000;
                var itemsObj = (
                   from shipping in Context.MPMWMS_SHIPPING_LISTs

                   join titipan in Context.MPMWMS_LOKASI_TITIPANs
                   on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }
                   into ps
                   from p in ps.DefaultIfEmpty()
                   where
                       //(shipping.DATAAREA_ID == dataarea_id)
                       //&& (shipping.MAINDEALER_ID == maindealer_id)
                       //&& 
                       (shipping.SITE_ID == site_id)
                       && (shipping.SHIPPING_DATE.Date == MPMDateUtil.DateTime.Date)

                   select new ShippingListRec
                   {
                       DATAAREA_ID = shipping.DATAAREA_ID,
                       DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                       DEALER_CITY = shipping.DEALER_CITY,
                       DEALER_CODE = shipping.DEALER_CODE,
                       DEALER_NAME = shipping.DEALER_NAME,
                       DESCRIPTION = shipping.DESCRIPTION,
                       DO_ID = shipping.DO_ID,
                       DRIVER = shipping.DRIVER,
                       EXPEDITION_ID = shipping.EXPEDITION_ID,
                       INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                       IS_CANCEL = shipping.IS_CANCEL,
                       IS_PRINT = shipping.IS_PRINT,
                       //IS_VISTUAL = picking.IS_VIRTUAL,
                       PICKING_DATE = shipping.PICKING_DATE,
                       PICKING_ID = shipping.PICKING_ID,
                       POLICE_NUMBER = shipping.POLICE_NUMBER,
                       PRINT_DATE = shipping.PRINT_DATE,
                       REKAPSL_ID = shipping.REKAPSL_ID,
                       SHIPPING_DATE = shipping.SHIPPING_DATE,
                       SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                       NOTE = p.NOTE,
                       TANGGAL_KELUAR_TITIPAN = p.TGLKELUAR

                   }).Union(
                   from shipping in Context.MPMWMS_SHIPPING_LISTs
                   join titipan in Context.MPMWMS_LOKASI_TITIPANs
                   on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }

                   where
                       //(shipping.DATAAREA_ID == dataarea_id)
                       //&& (shipping.MAINDEALER_ID == maindealer_id)
                       //&& 
                       (titipan.SITE_ID == site_id)
                       && (titipan.TGLKELUAR.Value.Date == MPMDateUtil.DateTime.Date)

                   select new ShippingListRec
                   {
                       DATAAREA_ID = shipping.DATAAREA_ID,
                       DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                       DEALER_CITY = shipping.DEALER_CITY,
                       DEALER_CODE = shipping.DEALER_CODE,
                       DEALER_NAME = shipping.DEALER_NAME,
                       DESCRIPTION = shipping.DESCRIPTION,
                       DO_ID = shipping.DO_ID,
                       DRIVER = shipping.DRIVER,
                       EXPEDITION_ID = shipping.EXPEDITION_ID,
                       INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                       IS_CANCEL = shipping.IS_CANCEL,
                       IS_PRINT = shipping.IS_PRINT,
                       //IS_VISTUAL = picking.IS_VIRTUAL,
                       PICKING_DATE = shipping.PICKING_DATE,
                       PICKING_ID = shipping.PICKING_ID,
                       POLICE_NUMBER = shipping.POLICE_NUMBER,
                       PRINT_DATE = shipping.PRINT_DATE,
                       REKAPSL_ID = shipping.REKAPSL_ID,
                       SHIPPING_DATE = shipping.SHIPPING_DATE,
                       SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                       NOTE = titipan.NOTE,
                       TANGGAL_KELUAR_TITIPAN = titipan.TGLKELUAR
                   }
                   );
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        //Tambahan Dev.Magang 
        public List<ShippingListRec> ListCreateSubstractDateRec(String dataarea_id, String maindealer_id, String site_id, int date)
        {
            try
            {
                Context.CommandTimeout = 5000000;


                var itemsObj = (
                    from shipping in Context.MPMWMS_SHIPPING_LISTs

                    join titipan in Context.MPMWMS_LOKASI_TITIPANs
                    on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }
                    into ps
                    from p in ps.DefaultIfEmpty()
                    where
                        //(shipping.DATAAREA_ID == dataarea_id)
                        //&& (shipping.MAINDEALER_ID == maindealer_id)
                        //&& 
                        (shipping.SITE_ID == site_id)
                        && (shipping.SHIPPING_DATE.Date >= MPMDateUtil.DateTime.Date.AddDays(date))

                    select new ShippingListRec
                    {
                        DATAAREA_ID = shipping.DATAAREA_ID,
                        DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                        DEALER_CITY = shipping.DEALER_CITY,
                        DEALER_CODE = shipping.DEALER_CODE,
                        DEALER_NAME = shipping.DEALER_NAME,
                        DESCRIPTION = shipping.DESCRIPTION,
                        DO_ID = shipping.DO_ID,
                        DRIVER = shipping.DRIVER,
                        EXPEDITION_ID = shipping.EXPEDITION_ID,
                        INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                        IS_CANCEL = shipping.IS_CANCEL,
                        IS_PRINT = shipping.IS_PRINT,
                        //IS_VISTUAL = picking.IS_VIRTUAL,
                        PICKING_DATE = shipping.PICKING_DATE,
                        PICKING_ID = shipping.PICKING_ID,
                        POLICE_NUMBER = shipping.POLICE_NUMBER,
                        PRINT_DATE = shipping.PRINT_DATE,
                        REKAPSL_ID = shipping.REKAPSL_ID,
                        SHIPPING_DATE = shipping.SHIPPING_DATE,
                        SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                        NOTE = p.NOTE,
                        TANGGAL_KELUAR_TITIPAN = p.TGLKELUAR

                    }).Union(
                    from shipping in Context.MPMWMS_SHIPPING_LISTs
                    join titipan in Context.MPMWMS_LOKASI_TITIPANs
                    on new { SHIPPINGLIST_ID = shipping.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }

                    where
                        //(shipping.DATAAREA_ID == dataarea_id)
                        //&& (shipping.MAINDEALER_ID == maindealer_id)
                        //&& 
                        (titipan.SITE_ID == site_id)
                        && (titipan.TGLKELUAR.Value.Date >= MPMDateUtil.DateTime.Date.AddDays(date))

                    select new ShippingListRec
                    {
                        DATAAREA_ID = shipping.DATAAREA_ID,
                        DEALER_ADDRESS = shipping.DEALER_ADDRESS,
                        DEALER_CITY = shipping.DEALER_CITY,
                        DEALER_CODE = shipping.DEALER_CODE,
                        DEALER_NAME = shipping.DEALER_NAME,
                        DESCRIPTION = shipping.DESCRIPTION,
                        DO_ID = shipping.DO_ID,
                        DRIVER = shipping.DRIVER,
                        EXPEDITION_ID = shipping.EXPEDITION_ID,
                        INVOICE_ID_AX = shipping.INVOICE_ID_AX,
                        IS_CANCEL = shipping.IS_CANCEL,
                        IS_PRINT = shipping.IS_PRINT,
                        //IS_VISTUAL = picking.IS_VIRTUAL,
                        PICKING_DATE = shipping.PICKING_DATE,
                        PICKING_ID = shipping.PICKING_ID,
                        POLICE_NUMBER = shipping.POLICE_NUMBER,
                        PRINT_DATE = shipping.PRINT_DATE,
                        REKAPSL_ID = shipping.REKAPSL_ID,
                        SHIPPING_DATE = shipping.SHIPPING_DATE,
                        SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID,
                        NOTE = titipan.NOTE,
                        TANGGAL_KELUAR_TITIPAN = titipan.TGLKELUAR
                    }
                    );
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }


        public List<MPMWMS_SHIPPING_LIST> ListCreateSubstractDate(String dataarea_id, String maindealer_id, String site_id, double date)
        {
            try
            {
                var itemsObj =
                    from shipping in Context.MPMWMS_SHIPPING_LISTs
                    where
                        (shipping.DATAAREA_ID == dataarea_id)
                        && (shipping.MAINDEALER_ID == maindealer_id)
                        && (shipping.SITE_ID == site_id)
                        && (shipping.CREATE_DATE.Value.Date >= MPMDateUtil.DateTime.Date.AddDays(date))
                        && (shipping.CREATE_DATE.Value.Date <= MPMDateUtil.DateTime.Date)
                    select shipping;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_SHIPPING_LIST> ListInvoiceAXEmpty(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from shipping in Context.MPMWMS_SHIPPING_LISTs
                               where
                               (shipping.DATAAREA_ID == dataarea_id) &&
                               (shipping.MAINDEALER_ID == maindealer_id) &&
                               (shipping.SITE_ID == site_id) &&
                               (shipping.INVOICE_ID_AX == null || shipping.INVOICE_ID_AX == "") &&
                               (shipping.SHIPPING_DATE.Date.Year == DateTime.Today.Year)
                               select shipping;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anong 7/18/2019
        public MPMWMS_SHIPPING_LIST StoringSPG(String shipping_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from shipping in Context.MPMWMS_SHIPPING_LISTs
                               where
                               (shipping.SHIPPING_LIST_ID == shipping_id) &&
                               (shipping.DATAAREA_ID == dataarea_id) &&
                               (shipping.MAINDEALER_ID == maindealer_id) &&
                               (shipping.SITE_ID == site_id)
                               select shipping;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_SHIPPING_LIST> ListPrint(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                //var result = ListPrintReguler(dataarea_id, maindealer_id, site_id);
                var result = ListPrintRegulerAndTitipan(dataarea_id, maindealer_id, site_id);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<MPMWMS_SHIPPING_LIST> ListPrintRegulerAndTitipan(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = (
                                from shipping in Context.MPMWMS_SHIPPING_LISTs
                                where
                                (shipping.DATAAREA_ID == dataarea_id) &&
                                (shipping.MAINDEALER_ID == maindealer_id) &&
                                (shipping.SITE_ID == site_id) &&
                                (shipping.IS_PRINT == "1")
                                && !Context.MPMWMS_LOKASI_TITIPANs.Any(t => t.SHIPPINGLIST_ID == shipping.SHIPPING_LIST_ID)
                                select shipping
                                )
                                .Union
                                (
                                from t in Context.MPMWMS_LOKASI_TITIPANs
                                join s in Context.MPMWMS_SHIPPING_LISTs on t.SHIPPINGLIST_ID equals s.SHIPPING_LIST_ID
                                where t.STATUS == 1 && t.SITE_ID == site_id && s.DATAAREA_ID == dataarea_id && s.IS_PRINT == "1" && s.MAINDEALER_ID == maindealer_id 
                                select s
                                );
                return itemsObj.ToList();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<MPMWMS_SHIPPING_LIST> ListPrintReguler(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from shipping in Context.MPMWMS_SHIPPING_LISTs
                               where
                               (shipping.DATAAREA_ID == dataarea_id) &&
                               (shipping.MAINDEALER_ID == maindealer_id) &&
                               (shipping.SITE_ID == site_id) &&
                               (shipping.IS_PRINT == "1")
                               select shipping;
                return itemsObj.ToList();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public List<MPMWMS_SHIPPING_LIST> ListPrintLama(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from shipping in Context.MPMWMS_SHIPPING_LISTs
                               where
                               (shipping.DATAAREA_ID == dataarea_id) &&
                               (shipping.MAINDEALER_ID == maindealer_id) &&
                               (shipping.SITE_ID == site_id) &&
                               (shipping.IS_PRINT == "2")
                               select shipping;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SHIPPING_LIST Create()
        {
            MPMWMS_SHIPPING_LIST item = new MPMWMS_SHIPPING_LIST();
            item.STATUS = "O";
            item.IS_PRINT = "1";
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.NUMBER_PRINTED = 0;
            return item;
        }

        public void Insert(MPMWMS_SHIPPING_LIST row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.NUMBER_PRINTED = 0;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_SHIPPING_LISTs.InsertOnSubmit(row);
        }

        public void InsertLog(MPM_MDMS_LOGSENDSDM row)
        {
            row.CREATEDATE = DateTime.Now;
            Context.MPM_MDMS_LOGSENDSDMs.InsertOnSubmit(row);
        }

        public List<vMPM_UMSL> UMSL(String shipping_list_id, String dataarea_id, String maindealer_id, String site_id, String do_id)
        {
            IQueryable<vMPM_UMSL> search =
                from umsl in Context.vMPM_UMSLs
                where
                    umsl.DATAAREA_ID == dataarea_id
                    && umsl.MAINDEALER_ID == maindealer_id
                    && umsl.SITE_ID == site_id
                    && umsl.SHIPPING_LIST_ID == shipping_list_id
                    && umsl.DO_ID == do_id
                select umsl;
            return search.ToList(); ;
        }

        public MPMWMS_SHIPPING_LIST Item(String shipping_list_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<MPMWMS_SHIPPING_LIST> search =
                    (from a in Context.MPMWMS_SHIPPING_LISTs
                     join titipan in Context.MPMWMS_LOKASI_TITIPANs
                    on new { SHIPPINGLIST_ID = a.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }
                    into ps
                     from p in ps.DefaultIfEmpty()
                     where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.SHIPPING_LIST_ID == shipping_list_id
                     select a).Union(
                        from a in Context.MPMWMS_SHIPPING_LISTs
                        join titipan in Context.MPMWMS_LOKASI_TITIPANs
                        on new { SHIPPINGLIST_ID = a.SHIPPING_LIST_ID } equals new { titipan.SHIPPINGLIST_ID }
                        where
                             titipan.SITE_ID == site_id
                            && titipan.SHIPPINGLIST_ID == shipping_list_id
                        select a
                        );

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
