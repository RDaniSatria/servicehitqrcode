﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MPMWMSMODEL.Table.TruckOperation
{
    public class TRUCK_OPERATION
    {
        public DateTime Tanggal { get; set; }
        public string No { get; set; }
        [Required(ErrorMessage = "ID Expedition is required")]
        public string IDExpedition { get; set; }
        [Required(ErrorMessage = "No Polisi is required")]
        public string NoPolisi { get; set; }
        public decimal Kapasitas { get; set; }
        public decimal AktualKapasitas { get; set; }
        public string Status { get; set; }
        public string DATAAREA_ID { get; set; }
        public string MAINDEALER_ID { get; set; }
        public string SITE_ID { get; set; }


    }
}
