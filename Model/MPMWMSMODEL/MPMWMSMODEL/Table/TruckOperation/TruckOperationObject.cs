﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.TruckOperation
{
    public class TruckOperationObject : MPMDbObject<WmsUnitDataContext>
    {
        public TruckOperationObject()
            : base()
        {
        }

        public MPMWMS_EAD_TRUCK_OPERATION Create()
        {
            MPMWMS_EAD_TRUCK_OPERATION item = new MPMWMS_EAD_TRUCK_OPERATION();
            return item;
        }

        public List<TRUCK_OPERATION> List(String _status)
        {
            try
            {
                var itemsObj = from eadto in Context.MPMWMS_EAD_TRUCK_OPERATIONs
                               where (eadto.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (eadto.MAINDEALER_ID == UserSession.MAINDEALER_ID)
                               && (eadto.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (eadto.STATUS == ((_status == "Aktif") ? "1" : "2"))
                               select new TRUCK_OPERATION
                                {
                                    No = eadto.NUMBER.ToString(),
                                    Tanggal = eadto.DATE,
                                    IDExpedition = eadto.EXPEDITION_ID,
                                    NoPolisi = eadto.POLICE_NUMBER,
                                    Kapasitas = eadto.CAPACITY,
                                    AktualKapasitas = eadto.ACTUALCAPACITY,
                                    Status = (eadto.STATUS == "1") ? "Aktif" : "Done"
                                };
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_TRUCK_OPERATION> List(String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from eadto in Context.MPMWMS_EAD_TRUCK_OPERATIONs
                               where (eadto.EXPEDITION_ID == expedition_id)
                               && (eadto.DATAAREA_ID == dataarea_id)
                               && (eadto.MAINDEALER_ID == maindealer_id)
                               && (eadto.SITE_ID == site_id)
                               && (eadto.POLICE_NUMBER == police_number)
                               select eadto;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_EAD_TRUCK_OPERATIONs);
        }

        public MPMWMS_EAD_TRUCK_OPERATION Item(DateTime date, int number, String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from eadto in Context.MPMWMS_EAD_TRUCK_OPERATIONs
                               where (eadto.EXPEDITION_ID == expedition_id)
                               && (eadto.DATAAREA_ID == dataarea_id)
                               && (eadto.MAINDEALER_ID == maindealer_id)
                               && (eadto.SITE_ID == site_id)
                               && (eadto.POLICE_NUMBER == police_number)
                               && (eadto.DATE == date)
                               && (eadto.NUMBER == number)
                               select eadto;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //public MPMWMS_EAD_TRUCK_OPERATION ItemNumber(String no)
        //{
        //    try
        //    {
        //        var itemsObj = from eadto in Context.MPMWMS_EAD_TRUCK_OPERATIONs
        //                       //where (eadto.NUMBER == no)
                             
        //                       select eadto;
        //        var currentItem = itemsObj.Take(1).FirstOrDefault();
        //        return currentItem;
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        public int RunningNumber(DateTime tanggal, string policeno)
        {
            int runningno = 1;
            try
            {
                var itemsObj = from eadto in Context.MPMWMS_EAD_TRUCK_OPERATIONs
                               where 
                               (eadto.POLICE_NUMBER == policeno)
                               && (eadto.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (eadto.MAINDEALER_ID == UserSession.MAINDEALER_ID)
                               && (eadto.SITE_ID == UserSession.SITE_ID_MAPPING)
                               && (eadto.DATE == tanggal)
                               select eadto;

                List< MPMWMS_EAD_TRUCK_OPERATION> datas = itemsObj.ToList();
                if (datas.Count > 0)
                {
                    var dataPols = datas.Where(x => x.POLICE_NUMBER.Equals(policeno));
                    runningno = dataPols.Max(y => y.NUMBER)  + 1;
                }
                
                return runningno;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EAD_TRUCK_OPERATION row)
        {
            //int? runningnumber = 0;
            //Context.MPMGETRUNNINGNUMBER("TO", "TROPR", 0, 0, ref runningnumber);
            //row.NUMBER = runningnumber.ToString().PadLeft(10, '0');

            row.NUMBER = this.RunningNumber(row.DATE, row.POLICE_NUMBER);
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EAD_TRUCK_OPERATIONs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_EAD_TRUCK_OPERATION row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void Delete(MPMWMS_EAD_TRUCK_OPERATION row)
        {
            try
            {
                Context.MPMWMS_EAD_TRUCK_OPERATIONs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
