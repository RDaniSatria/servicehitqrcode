﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CorrectionSL
{
    public interface ICorrectionSLObject
    {
        List<MPMWMS_CORRECTION_SL> List(String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_CORRECTION_SL Item(String correction_sl_id, String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_CORRECTION_SL Create();
        void Delete(MPMWMS_CORRECTION_SL item);
    }
}
