﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.CorrectionSLLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CorrectionSL
{
    public class CorrectionSLObject: MPMDbObject<WmsUnitDataContext>, ICorrectionSLObject
    {
        private CorrectionSLLineObject _CorrectionSLLineObject = new CorrectionSLLineObject();

        public CorrectionSLObject()
            : base()
        {
        }

        public CorrectionSLLineObject CorrectionSLLineObject
        {
            get { return _CorrectionSLLineObject; }
        }

        public List<MPMWMS_CORRECTION_SL> List(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_CORRECTION_SLs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.IS_CANCEL == "N"
                    select a;

                return search.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_CORRECTION_SL Item(String correction_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_CORRECTION_SLs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.CORRECTION_SL_ID == correction_sl_id
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_CORRECTION_SL Create()
        {
            MPMWMS_CORRECTION_SL row = new MPMWMS_CORRECTION_SL();
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public void Insert(MPMWMS_CORRECTION_SL item)
        {
            item.STATUS = "O";
            item.IS_CANCEL = "N";
            item.CORRECTION_SL_DATE = MPMDateUtil.DateTime;
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_CORRECTION_SLs.InsertOnSubmit(item);
        }

        public void Delete(MPMWMS_CORRECTION_SL item)
        {
            item.IS_CANCEL = "Y";
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = MPMDateUtil.DateTime;
        }
    }
}
