﻿using MPMLibrary.NET.Lib.Db.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;
using MPMLibrary.NET.Lib.Exception;

namespace MPMWMSMODEL.Table.VendorStoringUnit
{
    public class VendorStoringUnitObject : MPMDbObject<WmsMpmLiveDataContext>, IVendorStoringUnitObject
    {
        public VendorStoringUnitObject() : base() { }

        public List<MPM_VENDOR> ListUnitVendor()
        {
            try
            {
                var itemsObj = from a in Context.MPM_VENDORs
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public MPM_VENDOR ItemUnitVendor(string IdVendor)
        {
            try
            {
                var itemsObj = from a in Context.MPM_VENDORs
                               where a.IDVENDOR == IdVendor
                               select a;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public void Insert(MPM_VENDOR item)
        {
            Context.MPM_VENDORs.InsertOnSubmit(item);
        }
        public void Insert(MPMQRCODE_VENDOR item)
        {
            Context.MPMQRCODE_VENDORs.InsertOnSubmit(item);
        }
        public void Insert(MPM_VENDOR_TRACK item)
        {
            Context.MPM_VENDOR_TRACKs.InsertOnSubmit(item);
        }
        public void Insertlog(MPMHITAPIQTRUSTLOG item)
        {
            Context.MPMHITAPIQTRUSTLOGs.InsertOnSubmit(item);
        }
        //public void InsertlogsApi(MPMQTRUSTSynchLogAPI item)
        //{
        //    Context.MPMQTRUSTSynchLogAPIs.InsertOnSubmit(item);
        //}
    }
}
