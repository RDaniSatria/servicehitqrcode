﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.VendorStoringUnit
{
    public interface IVendorStoringUnitObject
    {
        List<MPM_VENDOR> ListUnitVendor();
        MPM_VENDOR ItemUnitVendor(string IdVendor);
        void Insert(MPM_VENDOR item);
        void Insert(MPMQRCODE_VENDOR item);
        void Insert(MPM_VENDOR_TRACK item);
    }
}
