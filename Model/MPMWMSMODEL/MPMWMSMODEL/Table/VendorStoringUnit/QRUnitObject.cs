﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Customer;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.VendorStoringUnit
{
    public class QRUnitObject : MPMDbObject<WmsMpmLiveDataContext>
    {
        public QRUnitObject() 
            : base()
        {
        }

        public MPMWMSQRUNIT ItemUpdate(string machineid, string qrcode)
        {
            try
            {
                var result = from a in Context.MPMWMSQRUNITs
                             where a.ENGINENO== machineid && a.QRCODE == qrcode
                             select a;
                return result.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        public List<MPMWMSQRUNIT> getDataByStatusSend(int _sendStatus)
        {
           
            try
            {
                /* ISSEND = 0 --> belum terkirim ke WS vendor */
                /* ISSEND = 1 --> sudah terkirim ke WS vendor */
                //tambahan untuk cek kirim data ke vendor (menambahkan where (parameter) ke AGENTTO, AGENTFROM, ACTIVITYDESCRIPTION)
                var itemsObj = from a in Context.MPMWMSQRUNITs
                               where a.ISSEND == _sendStatus
                               //&& a.AGENTTO == "Z0180"
                               ////&& a.ENGINENO == "JM71E1048580"
                               //&& a.AGENTFROM == "GD1"
                               //&& a.ACTIVITYDESCRIPTION == "MD TO DEALER"
                               //where a.IDQR == 1177737
                               orderby a.IDQR ascending
                               select a;
                return itemsObj.ToList();

            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMSQRUNIT getDataByEngine(String _engineno, int _IDQR)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMSQRUNITs
                               where a.ENGINENO == _engineno &&
                               a.IDQR == _IDQR
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMHITAPIQTRUSTLOG getDataLog(String IdApiLog)
        {
            try
            {
                var itemObj = from a in Context.MPMHITAPIQTRUSTLOGs
                              where a.IDAPILOG == IdApiLog
                              select a;
                return itemObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateLog(MPMHITAPIQTRUSTLOG item)
        {
            try
            {
                item.WAKTU = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMSQRUNIT item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
                
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMSQRUNIT item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMSQRUNITs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public void Insert1(MPMWMSQRUNIT item)
        {
            try
            {
                Context.MPMWMSQRUNITs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMSQRUNIT item)
        {
            try
            {
                Context.MPMWMSQRUNITs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


        #region CUSTOMER QR

        //public List<MPM_VENDOR_CUSTOMER> getCustomerQR(int _sendStatus)
        //{
        //    try
        //    {
        //        /* ISSEND = 0 --> belum terkirim ke WS vendor */
        //        /* ISSEND = 1 --> sudah terkirim ke WS vendor */

        //        var itemsObj = from a in Context.MPM_VENDOR_CUSTOMERs
        //                       where a.ISSEND == _sendStatus &&
        //                       (a.RESPONSEMSG == null || a.RESPONSEMSG == "")
        //                       orderby a.CREATEDATE ascending, a.CUSTOMER_AHM_NO ascending
        //                       select a;
        //        return itemsObj.ToList();
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        //perubahan query "itemsObj" anas 15-2-2021
        public List<customerPenjualan> getCustomerQR(int _sendStatus)
        {
            try
            {
                WmsUnitDataContext context1 = new WmsUnitDataContext();
                /* ISSEND = 0 --> belum terkirim ke WS vendor */
                /* ISSEND = 1 --> sudah terkirim ke WS vendor */

                var itemsObj = (from c in Context.V_MPMWMS_CUSTOMER_PENJUALANs
                                where c.ISSEND == _sendStatus &&
                                        (c.RESPONSEMSG == null || c.RESPONSEMSG == "")

                                //&& a.CUSTOMER_AHM_NO == "JM31E3025629"
                                orderby c.CREATEDATE ascending, c.CUSTOMER_AHM_NO ascending
                                select new customerPenjualan
                                {
                                    CUSTOMER_AHM_NO = c.CUSTOMER_AHM_NO,
                                    NAME = c.NAME,
                                    EMAIL = c.EMAIL,
                                    PHONENUMBER = c.PHONENUMBER,
                                    IDENTITYNUMBER = c.IDENTITYNUMBER,
                                    GENDER = c.GENDER,
                                    BIRTHDATE = c.BIRTHDATE,
                                    POSTCODE = c.POSTCODE,
                                    ADDRESS = c.ADDRESS,
                                    PROVINCE = c.PROVINCE,
                                    CITY = c.CITY,
                                    DISTRICT = c.DISTRICT,
                                    USERID = c.USERID,
                                    USERTYPE = c.USERTYPE,
                                    RESPONSESTATUS = c.RESPONSESTATUS,
                                    RESPONSECODE = c.RESPONSECODE,
                                    RESPONSEMSG = c.RESPONSEMSG,
                                    ISSEND = c.ISSEND,
                                    SENDDATE = c.SENDDATE,
                                    CREATEBY = c.CREATEBY,
                                    CREATEDATE = c.CREATEDATE,
                                    MODIFBY = c.MODIFBY,
                                    MODIFDATE = c.MODIFDATE,
                                    KODEDEALER = c.KODEDEALER,
                                    NOFAKTUR = c.NOFAKTUR,
                                    QRCODEUNCLAIM = c.QRCODEUNCLAIM,
                                    IDENTITYNUMBERUNCLAIM = c.IDENTITYNUMBERUNCLAIM,
                                    NUMBERFLAG = c.NUMBERFLAG,
                                    ActivityDescription = c.ActDesc,

                                    UNIT_TYPE = c.UNIT_TYPE,
                                    COLOR_TYPE = c.COLOR_TYPE,
                                    UNIT_YEAR = c.UNIT_YEAR,
                                    FRAME_ID = c.FRAME_ID,
                                    QR_CODE = c.QR_CODE

                                }).ToList();

                //var itemsObj1 = (from a in context1.MPMSALMASTERTABLEs
                //                 where a.JENISTAB == "QRCODE" && a.KODETAB == "1"
                //                 select new mastertable
                //                 {
                //                     ActivityDescription = a.NAMATAB

                //                 }).ToList();

                //foreach (var DataItemsObj in itemsObj) {
                //    var itemsObj1 = (from a in context1.MPMSALMASTERTABLEs
                //                     where a.JENISTAB == "QRCODE" && a.KODETAB == "1"
                //                     select new mastertable
                //                     {
                //                         ActivityDescription = a.NAMATAB

                //                     }).ToList();
                //}

                return itemsObj.Take(1000).ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_VENDOR_CUSTOMER getCustomerByEngine(String _engineno)
        {
            try
            {
                var itemsObj = from a in Context.MPM_VENDOR_CUSTOMERs
                               where a.CUSTOMER_AHM_NO == _engineno
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateCustomer(MPM_VENDOR_CUSTOMER item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        #endregion

    }
}
