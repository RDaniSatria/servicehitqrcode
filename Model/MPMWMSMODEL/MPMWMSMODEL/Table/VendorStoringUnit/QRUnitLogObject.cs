﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.VendorStoringUnit
{
    public class QRUnitLogObject : MPMDbObject<WmsMpmLiveDataContext>
    {
        public QRUnitLogObject() 
            : base()
        {
        }

        public void Update(MPMWMSQRUNITLOG item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMSQRUNITLOG item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMSQRUNITLOGs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMSQRUNITLOG item)
        {
            try
            {
                Context.MPMWMSQRUNITLOGs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
