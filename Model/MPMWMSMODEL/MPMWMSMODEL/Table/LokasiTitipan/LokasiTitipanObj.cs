﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.LokasiTitipan
{
    public class LokasiTitipanObj : MPMDbObject<WmsUnitDataContext>
    {
        public LokasiTitipanObj() : base()
        {
            Context = new WmsUnitDataContext();
        }

        public List<LokasiTitipanRec> DataTitipanSiteSama(string machineid, String siteid)
        {
            var data = (from a in Context.MPMWMS_LOKASI_TITIPANs
                        where a.MACHINE_ID.Equals(machineid) && a.SITE_ID.Equals(siteid)
                        select new LokasiTitipanRec
                        {
                            machineid = a.MACHINE_ID,
                            siteid = a.SITE_ID,
                            status = a.STATUS,
                            tglkeluar = a.TGLKELUAR
                        });
            return data.ToList();


        }
        public List<LokasiTitipanRec> DataTitipan(string machineid)
        {
            var data = (from a in Context.MPMWMS_LOKASI_TITIPANs
                        where a.MACHINE_ID.Equals(machineid) 
                        select new LokasiTitipanRec
                        {
                            machineid = a.MACHINE_ID,
                            siteid = a.SITE_ID,
                            status = a.STATUS,
                            tglkeluar = a.TGLKELUAR
                        });
            return data.ToList();


        }


        public List<LokasiTitipanRec> Cekdatatitipan(string machineid,string siteid)
        {
            var data = (from a in Context.MPMWMS_LOKASI_TITIPANs
                       where a.MACHINE_ID.Equals(machineid) && !a.SITE_ID.Equals(siteid)
                       select new LokasiTitipanRec
                       {
                           machineid = a.MACHINE_ID,
                           siteid = a.SITE_ID,
                           status = a.STATUS,
                           tglkeluar = a.TGLKELUAR
                       });
            return data.ToList();


        }

        public MPMWMS_LOKASI_TITIPAN Item( String machine_or_frame_id, string siteid)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_LOKASI_TITIPANs
                               where a.MACHINE_ID.Equals(machine_or_frame_id) && a.SITE_ID.Equals(siteid)
                               select a;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertLokasiTitipan(MPMWMS_LOKASI_TITIPAN Item)
        {
            try
            {
                Item.CREATEDBY = UserSession.USER_ID;
                Item.CREATEDDATETIME = DateTime.Now;
                Context.MPMWMS_LOKASI_TITIPANs.InsertOnSubmit(Item);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
    }
}
