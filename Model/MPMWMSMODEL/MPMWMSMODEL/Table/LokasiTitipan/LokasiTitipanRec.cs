﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.LokasiTitipan
{
    public class LokasiTitipanRec
    {
        public string machineid { get; set; }
        public string siteid { get; set; }
        public DateTime? tglkeluar { get; set; }
        public int? status { get; set; }
    }
}
