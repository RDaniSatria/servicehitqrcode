﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.SPKAhassLIne
{
    public interface ISPKRepairAhassLineObject
    {
        List<MPMWMS_SPK_REPAIR_AHASS_LINE> List(String spk_id, String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_SPK_REPAIR_AHASS_LINE Item(
            String spk_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id
        );
        MPMWMS_SPK_REPAIR_AHASS_LINE Create();
        void Insert(MPMWMS_SPK_REPAIR_AHASS_LINE row);
        void Update(MPMWMS_SPK_REPAIR_AHASS_LINE item);
        void Delete(MPMWMS_SPK_REPAIR_AHASS_LINE item);
    }
}
