﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.SPKAhassLIne
{
    public class SPKRepairAhassLineObject: MPMDbObject<WmsUnitDataContext>, ISPKRepairAhassLineObject
    {
        public SPKRepairAhassLineObject()
            : base()
        {
        }

        public List<MPMWMS_SPK_REPAIR_AHASS_LINE> List(String spk_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<MPMWMS_SPK_REPAIR_AHASS_LINE> query =
                    from a in Context.MPMWMS_SPK_REPAIR_AHASS_LINEs
                    where
                        a.SPK_ID == spk_id
                        && a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                    select a;

                return query.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SPK_REPAIR_AHASS_LINE Item(String spk_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id)
        {
            try
            {
                IQueryable<MPMWMS_SPK_REPAIR_AHASS_LINE> query =
                    from a in Context.MPMWMS_SPK_REPAIR_AHASS_LINEs
                    where
                        a.SPK_ID == spk_id
                        && a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.MACHINE_ID == machine_id
                        && a.FRAME_ID == frame_id
                    select a;

                return query.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        //public MPMSERVVIEWDAFTARUNITWARNA Item(String item_id)
        //{
        //    IQueryable<MPMSERVVIEWDAFTARUNITWARNA> search =
        //        from eco in ((WmsUnitDataContext)Context).MPMSERVVIEWDAFTARUNITWARNAs
        //        where
        //            eco.DISPLAYPRODUCTNUMBER == item_id
        //        select eco;

        //    return search.Take(1).FirstOrDefault();
        //}

        public MPMWMS_SPK_REPAIR_AHASS_LINE Create()
        {
            MPMWMS_SPK_REPAIR_AHASS_LINE item = new MPMWMS_SPK_REPAIR_AHASS_LINE();
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            return item;
        }

        public void Insert(MPMWMS_SPK_REPAIR_AHASS_LINE row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_SPK_REPAIR_AHASS_LINEs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_SPK_REPAIR_AHASS_LINE item)
        {
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_SPK_REPAIR_AHASS_LINE item)
        {
            Context.MPMWMS_SPK_REPAIR_AHASS_LINEs.DeleteOnSubmit(item);
        }
    }
}
