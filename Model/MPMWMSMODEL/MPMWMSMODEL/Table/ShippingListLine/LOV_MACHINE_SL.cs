﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingListLine
{
    public class LOV_MACHINE_SL
    {
        public String MACHINE_ID { get; set; }
        public String FRAME_ID { get; set; }
        public String UNIT_TYPE { get; set; }
        public String COLOR_TYPE { get; set; }

        public LOV_MACHINE_SL(String machine_id, String frame_id, String unit_type, String color_type)
        {
            MACHINE_ID = machine_id;
            FRAME_ID = frame_id;
            UNIT_TYPE = unit_type;
            COLOR_TYPE = color_type;
        }
    }
}
