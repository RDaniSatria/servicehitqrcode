﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingListLine
{
    public class ShippingListLineObject : MPMDbObject<WmsUnitDataContext>, IShippingListLineObject
    {
        public ShippingListLineObject()
            : base()
        {
        }

        public MPMWMS_SHIPPING_LIST_LINE Create()
        {
            MPMWMS_SHIPPING_LIST_LINE item = new MPMWMS_SHIPPING_LIST_LINE();
            item.STATUS = "O";
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            return item;
        }

        public void Insert(MPMWMS_SHIPPING_LIST_LINE row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_SHIPPING_LIST_LINEs.InsertOnSubmit(row);
        }

        public IQueryable<MPMWMS_SHIPPING_LIST_LINE> runQuery(String shippinglist_id, String dataarea_id, String maindealer_id, String site_id, 
            String picking_id, String do_id)
        {
            return from detail in Context.MPMWMS_SHIPPING_LIST_LINEs
                   where
                   detail.SHIPPING_LIST_ID == shippinglist_id &&
                   detail.DATAAREA_ID == dataarea_id &&
                   detail.MAINDEALER_ID == maindealer_id &&
                   detail.SITE_ID == site_id &&
                   detail.PICKING_ID == picking_id &&
                   detail.DO_ID == do_id
                   select detail;
        }

        public List<MPMWMS_SHIPPING_LIST_LINE> List(String shippinglist_id, String dataarea_id, String maindealer_id, String site_id, 
            String picking_id, String do_id)
        {
            try
            {
                var itemsObj = runQuery(shippinglist_id, dataarea_id, maindealer_id, site_id, picking_id, do_id);
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SHIPPING_LIST_LINE Item(String shipping_list_id, String dataarea_id, String maindealer_id, String site_id, 
            String machine_id, String frame_id)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_SHIPPING_LIST_LINEs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.SHIPPING_LIST_ID == shipping_list_id
                        && a.MACHINE_ID == machine_id
                        && a.FRAME_ID == frame_id
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public void Update(MPMWMS_SHIPPING_LIST_LINE item)
        {
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = MPMDateUtil.DateTime;
        }
    }
}
