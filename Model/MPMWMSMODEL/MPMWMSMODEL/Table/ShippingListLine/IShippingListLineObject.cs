﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ShippingListLine
{
    public interface IShippingListLineObject
    {
        MPMWMS_SHIPPING_LIST_LINE Create();
        void Insert(MPMWMS_SHIPPING_LIST_LINE row);
        void Update(MPMWMS_SHIPPING_LIST_LINE item);
        List<MPMWMS_SHIPPING_LIST_LINE> List(String shippinglist_id, String dataarea_id, String maindealer_id, String site_id, 
            String picking_id, String do_id);
        MPMWMS_SHIPPING_LIST_LINE Item(String shipping_list_id, String dataarea_id, String maindealer_id, String site_id, 
            String machine_id, String frame_id);
    }
}
