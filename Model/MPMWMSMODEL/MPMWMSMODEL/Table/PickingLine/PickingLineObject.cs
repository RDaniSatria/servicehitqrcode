﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.PickingLine
{
    public class PickingLineObject: MPMDbObject<WmsUnitDataContext>, IPickingLineObject
    {
        public PickingLineObject()
            : base()
        {
        }

        public MPMWMS_PICKING_LINE Create()
        {
            MPMWMS_PICKING_LINE row = new MPMWMS_PICKING_LINE();
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public void Insert(MPMWMS_PICKING_LINE row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_PICKING_LINEs.InsertOnSubmit(row);
        }

        private IQueryable<MPMWMS_PICKING_LINE> RunQuery(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id)
        {
            return from detail in Context.MPMWMS_PICKING_LINEs
                    where (detail.PICKING_ID == picking_id) &&
                    (detail.DATAAREA_ID == dataarea_id) &&
                    (detail.MAINDEALER_ID == maindealer_id) &&
                    (detail.SITE_ID == site_id) &&
                    (detail.DO_ID == do_id)
                    select detail;
        }

        private IQueryable<MPMWMS_PICKING_LINE> RunQuery(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            return from detail in Context.MPMWMS_PICKING_LINEs
                    where (detail.PICKING_ID == picking_id) &&
                    (detail.DATAAREA_ID == dataarea_id) &&
                    (detail.MAINDEALER_ID == maindealer_id) &&
                    (detail.SITE_ID == site_id)
                    select detail;
        }

        public MPMWMS_PICKING_LINE Item(String picking_id, String dataarea_id, String maindealer_id, String site_id, 
            String do_id, String machine_id, String frame_id)
        {
            try
            {
                var itemsObj = from detail in Context.MPMWMS_PICKING_LINEs
                                where (detail.PICKING_ID == picking_id) &&
                                (detail.DATAAREA_ID == dataarea_id) &&
                                (detail.MAINDEALER_ID == maindealer_id) &&
                                (detail.SITE_ID == site_id) &&
                                (detail.DO_ID == do_id) &&
                                (detail.MACHINE_ID == machine_id) &&
                                (detail.FRAME_ID == frame_id)
                                select detail;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_PICKING_LINE LastInsertItem(String picking_id, String dataarea_id, String maindealer_id, String site_id,
            String do_id)
        {
            try
            {
                var itemsObj = from detail in Context.MPMWMS_PICKING_LINEs
                                where (detail.PICKING_ID == picking_id) &&
                                (detail.DATAAREA_ID == dataarea_id) &&
                                (detail.MAINDEALER_ID == maindealer_id) &&
                                (detail.SITE_ID == site_id) &&
                                (detail.DO_ID == do_id)
                                orderby detail.CREATE_DATE descending
                                select detail;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING_LINE> List()
        {
            return Context.MPMWMS_PICKING_LINEs.ToList();
        }

        public List<MPMWMS_PICKING_LINE> List(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id)
        {
            try
            {
                var itemsObj = RunQuery(picking_id, dataarea_id, maindealer_id, site_id, do_id);
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING_LINE> List(MPMWMS_PICKING header)
        {
            try
            {
                var itemsObj = RunQuery(header.PICKING_ID, header.DATAAREA_ID, header.MAINDEALER_ID, header.SITE_ID, header.DO_ID);
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING_LINE> List(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = RunQuery(picking_id, dataarea_id, maindealer_id, site_id);
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public int Count(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            List<MPMWMS_PICKING_LINE> take = List(picking_id, dataarea_id, maindealer_id, site_id);
            if (take != null)
            {
                return take.ToList().Count();
            }

            return 0;
        }

        public int QuantityDO(String dataarea_id, String maindealer_id, String site_id, String do_id, String item_id, String color_id)
        {
            IQueryable<MPMVIEW_INVOICE> itemsObj =
                    from invoice in Context.MPMVIEW_INVOICEs
                    where
                        invoice.DATAAREA_ID == dataarea_id
                        && invoice.DO_ID == do_id
                        && invoice.MAINDEALER_ID == maindealer_id
                        && invoice.SITE_ID == site_id
                        && invoice.UNIT_TYPE == item_id
                        && invoice.COLOR_TYPE == color_id
                    select invoice;

            int qty = 0;
            foreach (var a in itemsObj)
            {
                qty = qty + Convert.ToInt32(a.QTY_DO + a.QTY_RETUR);
            }

            return qty;
        }

        public int SumQuantityPicking(String dataarea_id, String maindealer_id, String site_id, String sales_id, String item_id, String color_id, String unit_year)
        {
            IQueryable<int> list = from pick_line in Context.MPMWMS_PICKING_LINEs
                                   join unit in Context.MPMWMS_UNITs on
                                        new { pick_line.MACHINE_ID, pick_line.FRAME_ID, pick_line.SITE_ID, pick_line.DATAAREA_ID } equals
                                        new { unit.MACHINE_ID, unit.FRAME_ID, unit.SITE_ID, unit.DATAAREA_ID } into join_unit_pick
                                   from result in join_unit_pick.DefaultIfEmpty()
                                   where (pick_line.DO_ID == sales_id) &&
                                   (pick_line.DATAAREA_ID == dataarea_id) &&
                                   (pick_line.MAINDEALER_ID == maindealer_id) &&
                                   (pick_line.SITE_ID == site_id) &&
                                   (pick_line.COLOR_TYPE == color_id) &&
                                   (pick_line.UNIT_TYPE == item_id) &&
                                   (result.UNIT_YEAR == unit_year)
                                   group pick_line by new
                                   {
                                       pick_line.DO_ID,
                                       pick_line.DATAAREA_ID,
                                       pick_line.COLOR_TYPE,
                                       pick_line.UNIT_TYPE
                                   } into g
                                   select g.Count();

            return list.Take(1).FirstOrDefault();
        }

        public void Delete(MPMWMS_PICKING_LINE row)
        {
            try
            {
                Context.MPMWMS_PICKING_LINEs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
