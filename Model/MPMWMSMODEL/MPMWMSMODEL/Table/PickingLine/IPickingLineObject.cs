﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PickingLine
{
    public interface IPickingLineObject
    {
        MPMWMS_PICKING_LINE Create();
        void Insert(MPMWMS_PICKING_LINE row);
        List<MPMWMS_PICKING_LINE> List();
        List<MPMWMS_PICKING_LINE> List(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id);
        List<MPMWMS_PICKING_LINE> List(MPMWMS_PICKING header);
    }
}
