﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.SPKAhass
{
    public interface ISPKRepairAhassObject
    {
        List<MPMWMS_SPK_REPAIR_AHASS> List(String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_SPK_REPAIR_AHASS Item(String spk_id, String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_SPK_REPAIR_AHASS Create();
        void Insert(MPMWMS_SPK_REPAIR_AHASS row);
        void Update(MPMWMS_SPK_REPAIR_AHASS item);
        void Delete(MPMWMS_SPK_REPAIR_AHASS item);
    }
}
