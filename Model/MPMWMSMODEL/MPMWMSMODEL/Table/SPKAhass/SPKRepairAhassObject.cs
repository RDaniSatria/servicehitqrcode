﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.SPKAhassLIne;

namespace MPMWMSMODEL.Table.SPKAhass
{
    public class SPKRepairAhassObject : MPMDbObject<WmsUnitDataContext>, ISPKRepairAhassObject
    {
        private SPKRepairAhassLineObject _SPKRepairAhassLineObject = new SPKRepairAhassLineObject();

        public SPKRepairAhassObject()
            : base()
        {
        }

        public SPKRepairAhassLineObject SPKRepairAhassLineObject
        {
            get
            {
                return _SPKRepairAhassLineObject;
            }
        }

        public List<MPMWMS_SPK_REPAIR_AHASS> List(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<MPMWMS_SPK_REPAIR_AHASS> query =
                    from a in Context.MPMWMS_SPK_REPAIR_AHASSes
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                    select a;

                return query.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SPK_REPAIR_AHASS Item(String spk_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<MPMWMS_SPK_REPAIR_AHASS> query =
                    from a in Context.MPMWMS_SPK_REPAIR_AHASSes
                    where
                        a.SPK_ID == spk_id
                        && a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                    select a;

                return query.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SPK_REPAIR_AHASS Create()
        {
            MPMWMS_SPK_REPAIR_AHASS item = new MPMWMS_SPK_REPAIR_AHASS();
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            return item;
        }

        public void Insert(MPMWMS_SPK_REPAIR_AHASS row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_SPK_REPAIR_AHASSes.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_SPK_REPAIR_AHASS item)
        {
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.MODIF_BY = UserSession.NPK;
            item.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_SPK_REPAIR_AHASS item)
        {
            Context.MPMWMS_SPK_REPAIR_AHASSes.DeleteOnSubmit(item);
        }
    }
}
