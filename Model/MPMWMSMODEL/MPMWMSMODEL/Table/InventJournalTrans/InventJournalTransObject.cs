﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.InventJournalTrans
{
    public class InventJournalTransObject : MPMDbObject<WmsUnitDataContext>, IInventJournalTransObject
    {
        public InventJournalTransObject()
            : base()
        {
        }

        public int SumQuantityJournal(String journal_id, String dataarea_id)
        {
            try
            {
                IQueryable<int> itemsObj = from journaltrans in Context.INVENTJOURNALTRANs
                                            where
                                            (journaltrans.JOURNALID == journal_id)
                                            && (journaltrans.DATAAREAID == dataarea_id)
                                            && (journaltrans.XTSFLAGLOADITEMACCESSORIES == 1)
                                            let k = new { item = journaltrans.JOURNALID, dataarea = journaltrans.DATAAREAID }
                                            group journaltrans by k into tgroup
                                            select Convert.ToInt32(tgroup.Sum(journaltrans => journaltrans.QTY * -1));
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public List<INVENTJOURNALTRAN> List(string dataarea_id, string journal_id)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                where
                    (a.DATAAREAID == dataarea_id) &&
                    (a.JOURNALID == journal_id) &&
                    (a.XTSFLAGLOADITEMACCESSORIES == 1)
                select a;
            return search.ToList();
        }

        public List<INVENTJOURNALTRAN> ListAll(string dataarea_id, string journal_id)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                where
                    (a.DATAAREAID == dataarea_id) &&
                    (a.JOURNALID == journal_id)
                select a;
            return search.ToList();
        }

        public INVENTJOURNALTRAN Item(String journal_id, String dataarea_id, String item_id, String color)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.JOURNALID == journal_id &&
                a.ITEMID == item_id &&
                result.INVENTCOLORID == color
                select a;

            return search.Take(1).FirstOrDefault();
        }

        public INVENTJOURNALTRAN ItemByYear(String journal_id, String dataarea_id, String item_id, String color, String unit_year)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.JOURNALID == journal_id &&
                a.ITEMID == item_id &&
                a.XTSUNITYEAR == unit_year &&
                result.INVENTCOLORID == color
                select a;

            return search.Take(1).FirstOrDefault();
        }

        public INVENTJOURNALTRAN Item(String journal_id, String dataarea_id, String item_id)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                where
                a.DATAAREAID == dataarea_id &&
                a.JOURNALID == journal_id &&
                a.ITEMID == item_id
                select a;

            return search.Take(1).FirstOrDefault();

        }

        public INVENTJOURNALTRAN Item(String journal_id, String dataarea_id, 
            String item_id, String site_id, String current_rack)
        {
            try
            {
                IQueryable<INVENTDIM> dim =
                    from a in Context.INVENTDIMs
                    where
                    a.DATAAREAID == dataarea_id
                    && a.INVENTSITEID == site_id
                    && a.INVENTLOCATIONID == current_rack
                    select a;

                String inventdimid = "";
                if (dim != null) inventdimid = dim.Take(1).FirstOrDefault().INVENTDIMID;

                IQueryable<INVENTJOURNALTRAN> search =
                    from a in Context.INVENTJOURNALTRANs
                    where
                    a.DATAAREAID == dataarea_id &&
                    a.JOURNALID == journal_id &&
                    a.ITEMID == item_id &&
                    a.INVENTDIMID == inventdimid
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public INVENTJOURNALTRAN ItemByTransferIdFather(
            String journal_id, String dataarea_id, 
            String item_id, String color,
            String transfer_id_father)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.JOURNALID == journal_id &&
                a.ITEMID == item_id &&
                result.INVENTCOLORID == color &&
                (a.INVENTTRANSIDFATHER == transfer_id_father || a.XTSINVENTTRANSIDFATHER == transfer_id_father)
                select a;

            return search.Take(1).FirstOrDefault();
        }

        public INVENTJOURNALTRAN ItemByTransferId(
            String journal_id, String dataarea_id,
            String item_id, String color,
            String transfer_id)
        {
            IQueryable<INVENTJOURNALTRAN> search =
                from a in Context.INVENTJOURNALTRANs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.JOURNALID == journal_id &&
                a.ITEMID == item_id &&
                result.INVENTCOLORID == color &&
                a.INVENTTRANSID == transfer_id
                select a;

            return search.Take(1).FirstOrDefault();
        }

        public List<INVENT_JOURNAL_TRANS> ListDisplay(String dataarea_id, String journal_id)
        {
            IQueryable<INVENT_JOURNAL_TRANS> search =
                from trans in Context.INVENTJOURNALTRANs
                join a in Context.INVENTDIMs on
                    new { dataarea = trans.DATAAREAID, dim = trans.INVENTDIMID } equals
                    new { dataarea = a.DATAAREAID, dim = a.INVENTDIMID } into trans_a
                from result in trans_a.DefaultIfEmpty()
                where
                    trans.DATAAREAID == dataarea_id
                    && trans.JOURNALID == journal_id
                orderby
                    trans.LINENUM ascending
                select new INVENT_JOURNAL_TRANS(
                    trans.JOURNALID,
                    trans.DATAAREAID,
                    trans.LINENUM,
                    trans.ITEMID,
                    result.INVENTSITEID,
                    result.INVENTLOCATIONID,
                    trans.INVENTONHAND,
                    trans.COUNTED,
                        (
                            from b in Context.MPMWMS_STOCK_TAKING_PARTs
                            where
                                b.DATAAREA_ID == trans.DATAAREAID
                                && b.STOCK_TAKING_ID == trans.JOURNALID
                                && b.COUNTING_ID == trans.LINENUM
                                && b.ITEM_ID == trans.ITEMID
                                && b.SITE_ID == result.INVENTSITEID
                                && b.WAREHOUSE_ID == result.INVENTLOCATIONID
                            select b
                        ).Max(x => (int?)x.COUNTING_TAG) ?? 0
                    );
            return search.ToList();
        }

        public INVENTJOURNALTRAN ItemByTag(String journal_id, String dataarea_id, String tag)
        {
            try
            {
                IQueryable<INVENTJOURNALTRAN> search =
                    from a in Context.INVENTJOURNALTRANs
                    where
                    a.DATAAREAID == dataarea_id &&
                    a.JOURNALID == journal_id &&
                    a.LINENUM == Convert.ToInt32(tag)
                    select a;

                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public INVENT_JOURNAL_TRANS ItemByTagWarehouse(String journal_id, String dataarea_id, String tag_id)
        {
            try
            {
                IQueryable<INVENT_JOURNAL_TRANS> search =
                    from trans in Context.INVENTJOURNALTRANs
                    join a in Context.INVENTDIMs on
                        new { dataarea = trans.DATAAREAID, dim = trans.INVENTDIMID } equals
                        new { dataarea = a.DATAAREAID, dim = a.INVENTDIMID } into a_b
                    from result in a_b.DefaultIfEmpty()
                    where
                        trans.DATAAREAID == dataarea_id
                        && trans.JOURNALID == journal_id
                        && trans.LINENUM == Convert.ToInt32(tag_id)
                    orderby
                        trans.LINENUM ascending
                    select new INVENT_JOURNAL_TRANS(
                        trans.JOURNALID,
                        trans.DATAAREAID,
                        trans.LINENUM,
                        trans.ITEMID,
                        result.INVENTSITEID,
                        result.INVENTLOCATIONID,
                        trans.INVENTONHAND,
                        trans.COUNTED,
                        (
                            from b in Context.MPMWMS_STOCK_TAKING_PARTs
                            where
                                b.DATAAREA_ID == trans.DATAAREAID
                                && b.STOCK_TAKING_ID == trans.JOURNALID
                                && b.COUNTING_ID == trans.LINENUM
                                && b.ITEM_ID == trans.ITEMID
                                && b.SITE_ID == result.INVENTSITEID
                                && b.WAREHOUSE_ID == result.INVENTLOCATIONID
                            select b).Count()
                        );
                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }            
        }
    }
}
