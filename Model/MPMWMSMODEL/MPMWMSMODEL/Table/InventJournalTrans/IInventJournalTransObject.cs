﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventJournalTrans
{
    public interface IInventJournalTransObject
    {
        int SumQuantityJournal(String journal_id, String dataarea_id);
        List<INVENTJOURNALTRAN> List(string dataarea_id, string journal_id);
        INVENTJOURNALTRAN Item(String journal_id, String dataarea_id, String item_id, String color);
        INVENTJOURNALTRAN Item(String journal_id, String dataarea_id, String item_id);
    }
}
