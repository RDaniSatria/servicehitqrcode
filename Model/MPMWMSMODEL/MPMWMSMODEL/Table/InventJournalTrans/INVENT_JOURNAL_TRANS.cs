﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventJournalTrans
{
    public class INVENT_JOURNAL_TRANS
    {
        public String JOURNAL_ID { get; set; }
        public String DATAAREA_ID { get; set; }
        public int LINE_NUM { get; set; }
        public String ITEM_ID { get; set; }
        public String NAME { get; set; }
        public String SITE_ID { get; set; }
        public String WAREHOUSE_ID { get; set; }
        public decimal INVENTONHAND { get; set; }
        public int COUNTED { get; set; }
        public int CHANGES_COUNT { get; set; }
        public String IS_MATCH { get; set; }

        public INVENT_JOURNAL_TRANS()
        {
        }

        public INVENT_JOURNAL_TRANS(String journal_id, String dataarea_id, 
            decimal line_num, String item_id, 
            String site_id, String warehouse_id, 
            decimal inventonhand, decimal counted, int changes_count)
        {
            JOURNAL_ID = journal_id;
            DATAAREA_ID = dataarea_id;
            LINE_NUM = Convert.ToInt32(line_num);
            ITEM_ID = item_id;
            SITE_ID = site_id;
            WAREHOUSE_ID = warehouse_id;
            INVENTONHAND = Convert.ToInt32((inventonhand < 0 ? 0 : inventonhand));
            COUNTED = Convert.ToInt32(counted);
            CHANGES_COUNT = changes_count;

            if (COUNTED == INVENTONHAND)
            {
                IS_MATCH = "Y";
            }
            else
            {
                IS_MATCH = "N";
            }
        }
    }
}
