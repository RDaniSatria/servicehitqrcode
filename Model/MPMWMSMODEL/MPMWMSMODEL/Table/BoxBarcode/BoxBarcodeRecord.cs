﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.BoxBarcode
{
    public class BoxBarcodeRecord
    {
        [Display(Name = "Yearly")]
        [Required(ErrorMessage = "YEARLY is required")]
        public int YEARLY { get; set; }

        [Display(Name = "Monthly")]
        [Required(ErrorMessage = "MONTHLY is required")]
        public int MONTHLY { get; set; }

        [Display(Name = "Collie Code")]
        [Required(ErrorMessage = "COLLIE CODE is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String COLLIECODE { get; set; }

        [Display(Name = "Site ID")]
        [Required(ErrorMessage = "SITE ID is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String SITEID { get; set; }

        [Display(Name = "Main Dealer")]
        [Required(ErrorMessage = "MAIN DEALER is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String MAINDEALERID { get; set; }

        [Display(Name = "Data Area")]
        [Required(ErrorMessage = "DATA AREA is required")]
        [StringLength(5, ErrorMessage = "Must be under 5 characters")]
        public String DATAAREAID { get; set; }

        [Display(Name = "Value")]
        [Required(ErrorMessage = "Value is required")]
        public int CODE { get; set; }
    }
}
