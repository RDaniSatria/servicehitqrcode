﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.BoxBarcode
{
    public class BoxBarcodeObject : MPMDbObject<WmsUnitDataContext>
    {
        public BoxBarcodeObject()
            : base()
        {
        }

        public MPMWMS_BOX_BARCODE Item(MPMWMS_BOX_BARCODE _item)
        {
            try
            {
                var data = from a in Context.MPMWMS_BOX_BARCODEs
                           where a.MAINDEALERID == _item.MAINDEALERID
                               && a.SITEID == _item.SITEID
                               && a.DATAAREAID == _item.DATAAREAID
                               && a.COLLIECODE == _item.COLLIECODE
                               && a.YEARLY == _item.YEARLY
                               && a.MONTHLY == _item.MONTHLY
                           select a;

                return data.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_COLLIE> ListCollieCode(string _mainDealer, string _site, string _dataArea)
        {
            try
            {
                var data = from a in Context.MPMWMS_COLLIEs
                           where a.MAINDEALERID == _mainDealer
                               && a.SITEID == _site
                               && a.DATAAREAID == _dataArea
                           select a;

                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_BOX_BARCODE item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;

                Context.MPMWMS_BOX_BARCODEs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_BOX_BARCODE item)
        {
            try
            {
                Context.MPMWMS_BOX_BARCODEs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<String> listBarcodeNumber
            (
                  int _numberBarcode
                , int _year
                , int _month
                , String _collieCode
                , String _site
                , String _mainDealer
                , String _dataArea
            )
        {
            try
            {
                var data = Context.MPMWMS_SP_GENERATE_BARCODE_BOX(_numberBarcode, _year, _month, _collieCode, _site, _mainDealer, _dataArea);

                var dataReturn = from x in data
                                 select x.BARCODEID;

                return dataReturn.ToList();
            }
            catch(MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
