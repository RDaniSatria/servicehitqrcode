﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Location
{
    public interface ILocationObject
    {
        MPMWMS_WAREHOUSE_LOCATION Create();
        void Insert(MPMWMS_WAREHOUSE_LOCATION row);
        MPMWMS_WAREHOUSE_LOCATION Item(String dataarea_id, String site_id, String location_id);
        MPMWMS_WAREHOUSE_LOCATION Item(String dataarea_id, String site_id, String location_id, String warehouse_id);
        List<MPMWMS_WAREHOUSE_LOCATION> List();
        List<MPMWMS_WAREHOUSE_LOCATION> List(String dataarea_id, String site_id);
        int CountLocation(String dataarea_id, String site_id, String head_location_id);
        int CountAvailableLocation(String dataarea_id, String site_id, String head_location_id);
        String GetWarehouse(String dataarea_id, String site_id, String location_id);
        String GetWarehouseByType(String dataarea_id, String site_id, String location_type);
    }
}
