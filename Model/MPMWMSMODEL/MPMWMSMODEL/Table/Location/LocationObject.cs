﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Location
{
    public class LocationObject : MPMDbObject<WmsUnitDataContext>, ILocationObject
    {
        public LocationObject()
            : base()
        {
        }

        public MPMWMS_WAREHOUSE_LOCATION Create()
        {
            MPMWMS_WAREHOUSE_LOCATION row = new MPMWMS_WAREHOUSE_LOCATION();
            row.LOCATION_TYPE = "R";
            row.UNIT_GROUP = "U";
            row.STOCK_MAX = 0;
            row.STOCK_ACTUAL = 0;
            row.STOCK_BOOKING = 0;
            return row;
        }

        public void Insert(MPMWMS_WAREHOUSE_LOCATION row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_WAREHOUSE_LOCATIONs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_WAREHOUSE_LOCATION row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void Delete(MPMWMS_WAREHOUSE_LOCATION row)
        {
            try
            {
                Context.MPMWMS_WAREHOUSE_LOCATIONs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_WAREHOUSE_LOCATION Item(String dataarea_id, String site_id, String location_id, String warehouse_id)
        {
            IQueryable<MPMWMS_WAREHOUSE_LOCATION> search = from location in Context.MPMWMS_WAREHOUSE_LOCATIONs
                                                           where (location.DATAAREA_ID == dataarea_id) &&
                                                           (location.SITE_ID == site_id) &&
                                                           (location.LOCATION_ID == location_id) &&
                                                           (location.WAREHOUSE_ID.ToUpper() == warehouse_id.ToUpper())
                                                           select location;
            return search.FirstOrDefault();
        }

        public List<MPMWMS_WAREHOUSE_LOCATION> List()
        {
            try
            {
                var itemsObj = from warehouse in Context.MPMWMS_WAREHOUSE_LOCATIONs
                               where
                               (warehouse.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (warehouse.SITE_ID == UserSession.SITE_ID_MAPPING)
                               select warehouse;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
            //return Context.MPMWMS_WAREHOUSE_LOCATIONs.ToList();
        }

        public List<MPMWMS_WAREHOUSE_LOCATION> List(String dataarea_id, String site_id)
        {
            try
            {
                var itemsObj = from warehouse in Context.MPMWMS_WAREHOUSE_LOCATIONs
                               where
                               (warehouse.DATAAREA_ID == dataarea_id)
                               && (warehouse.SITE_ID == site_id)
                               select warehouse;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
            //return Context.MPMWMS_WAREHOUSE_LOCATIONs.ToList();
        }

        public int CountLocation(String dataarea_id, String site_id, String head_location_id)
        {
            IQueryable<String> searchCount = from location in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                                             where (location.DATAAREA_ID == dataarea_id) &&
                                             (location.SITE_ID == site_id) &&
                                             (SqlMethods.Like(location.LOCATION_ID, head_location_id + "%"))
                                             select location.LOCATION_ID;

            return searchCount.ToList().Count();
        }

        public int CountAvailableLocation(String dataarea_id, String site_id, String head_location_id)
        {
            IQueryable<int> searchAvailable = 
                from location in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                where (location.DATAAREA_ID == dataarea_id) &&
                (location.SITE_ID == site_id) &&
                (SqlMethods.Like(location.LOCATION_ID, head_location_id + "%")) &&
                (location.STOCK_MAX - (location.STOCK_ACTUAL + location.STOCK_BOOKING) > 0)
                select (location.STOCK_MAX - (location.STOCK_ACTUAL + location.STOCK_BOOKING));

            if (searchAvailable.ToList().Count() == 0) return 0;
            return searchAvailable.Sum();
        }

        public MPMWMS_WAREHOUSE_LOCATION ItemByType(String dataarea_id, String site_id, String type)
        {
            IQueryable<MPMWMS_WAREHOUSE_LOCATION> search = from location in Context.MPMWMS_WAREHOUSE_LOCATIONs
                                                           where (location.DATAAREA_ID == dataarea_id) &&
                                                           (location.SITE_ID == site_id) &&
                                                           (location.LOCATION_TYPE == type) &&
                                                           (location.STOCK_ACTUAL + location.STOCK_BOOKING < location.STOCK_MAX)
                                                           select location;
            return search.Take(1).FirstOrDefault();
        }

        public MPMWMS_WAREHOUSE_LOCATION ItemHead(String dataarea_id, string site_id, string head_location_id)
        {
            IQueryable<MPMWMS_WAREHOUSE_LOCATION> searchAvailable =
                from location in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                where (location.DATAAREA_ID == dataarea_id) &&
                (location.SITE_ID == site_id) &&
                (SqlMethods.Like(location.LOCATION_ID, head_location_id + "%"))
                select location;

            return searchAvailable.Take(1).FirstOrDefault();
        }

        public MPMWMS_WAREHOUSE_LOCATION Item(String dataarea_id, String site_id, String location_id)
        {
            IQueryable<MPMWMS_WAREHOUSE_LOCATION> search = from location in Context.MPMWMS_WAREHOUSE_LOCATIONs
                                                           where (location.DATAAREA_ID == dataarea_id) &&
                                                           (location.SITE_ID == site_id) &&
                                                           (location.LOCATION_ID == location_id)
                                                           select location;
            return search.FirstOrDefault();
        }

        public String GetWarehouse(String dataarea_id, String site_id, String location_id)
        {
            MPMWMS_WAREHOUSE_LOCATION location = Item(dataarea_id, site_id, location_id);
            if (location != null)
            {
                return location.WAREHOUSE_ID;
            }

            return "";
        }

        public String GetWarehouseByType(String dataarea_id, String site_id, String location_type)
        {
            MPMWMS_WAREHOUSE_LOCATION location = ItemByType(dataarea_id, site_id, location_type);
            if (location != null)
            {
                return location.WAREHOUSE_ID;
            }

            return "";
        }
    }
}
