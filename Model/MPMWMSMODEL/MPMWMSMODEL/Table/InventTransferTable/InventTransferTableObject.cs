﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventTransferTable
{
    public class InventTransferTableObject: MPMDbObject<WmsUnitDataContext>, IInventTransferTableObject
    {
        public InventTransferTableObject()
            : base()
        {
        }

        public INVENTTRANSFERTABLE Item(String dataarea_id, String transfer_id)
        {
            try
            {
                var itemsObj = from transfer in Context.INVENTTRANSFERTABLEs
                               where (transfer.TRANSFERID == transfer_id) &&
                               (transfer.DATAAREAID == dataarea_id)
                               select transfer;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
