﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventTransferTable
{
    public interface IInventTransferTableObject
    {
        INVENTTRANSFERTABLE Item(String dataarea_id, String transfer_id);
    }
}
