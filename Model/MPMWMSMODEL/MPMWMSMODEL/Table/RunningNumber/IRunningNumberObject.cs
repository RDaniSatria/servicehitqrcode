﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RunningNumber
{
    public interface IRunningNumberObject
    {
        MPM_RUNNING_NUMBER Item(String code_tag_1);
        MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2);
        MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2, int year);
        MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2, int year, int month);
        MPM_RUNNING_NUMBER Item(String code_tag_1, int year);

        MPM_RUNNING_NUMBER Create();
        void Insert(MPM_RUNNING_NUMBER row);
    }
}
