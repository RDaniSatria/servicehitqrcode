﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.RunningNumber
{
    public class RunningNumberObject : MPMDbObject<WmsUnitDataContext>, IRunningNumberObject
    {
        public RunningNumberObject()
            : base()
        {
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1)
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1) &&
                                (run.CODE_TAG_2 == code_tag_2)
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2, int year)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1) &&
                                (run.CODE_TAG_2 == code_tag_2) &&
                                (run.YEAR == year)
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1, String code_tag_2, int year, int month)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1) &&
                                (run.CODE_TAG_2 == code_tag_2) &&
                                (run.YEAR == year) &&
                                (run.MONTH == month)
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1, int year)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1) &&
                                (run.YEAR == year)
                                orderby run.CREATE_DATE descending
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Item(String code_tag_1, int year, int month)
        {
            try
            {
                var itemsObj = from run in Context.MPM_RUNNING_NUMBERs
                                where
                                (run.CODE_TAG_1 == code_tag_1) &&
                                (run.YEAR == year) &&
                                (run.MONTH == month)
                                select run;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPM_RUNNING_NUMBER Create()
        {
            MPM_RUNNING_NUMBER row = new MPM_RUNNING_NUMBER();
            row.NUMBER = 1;
            row.YEAR = 0;
            row.MONTH = 0;
            return row;
        }

        public void Insert(MPM_RUNNING_NUMBER row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPM_RUNNING_NUMBERs.InsertOnSubmit(row);
        }
    }
}
