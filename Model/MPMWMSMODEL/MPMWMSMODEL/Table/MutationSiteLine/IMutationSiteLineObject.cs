﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.MutationSiteLine
{
    public interface IMutationSiteLineObject
    {
        MPMWMS_MUTATION_SITE_LINE Item(String dataarea_id, String mutation_id, String machine_id, String frame_id);
        List<MPMWMS_MUTATION_SITE_LINE> List(String dataarea_id, String mutation_id);
        MPMWMS_MUTATION_SITE_LINE Create();
        void Insert(MPMWMS_MUTATION_SITE_LINE row);
        void Update(MPMWMS_MUTATION_SITE_LINE row);
        void Delete(MPMWMS_MUTATION_SITE_LINE row);
    }
}
