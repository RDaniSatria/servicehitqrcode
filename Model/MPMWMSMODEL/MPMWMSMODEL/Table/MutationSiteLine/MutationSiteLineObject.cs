﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.MutationSiteLine
{
    public class MPMWMS_MUTATION_SITE_LINE_RECORD
    {
        public String _STATUS { get; set; }
        public String _MACHINE_ID { get; set; }
        public String _FRAME_ID { get; set; }
        public String _MUTATION_ID { get; set; }
        public String _SITE_ID_INTR { get; set; }
        public String _WAREHOUSE_ID_INTR { get; set; }
        public String _LOCATION_ID_INTR { get; set; }
        public String _SITE_ID_NEW { get; set; }
        public String _WAREHOUSE_ID_NEW { get; set; }
        public String _LOCATION_ID_NEW { get; set; }
        public String _TRANSFER_ID { get; set; }

        public String _QR_CODE { get; set; }
    }

    public class MutationSiteLineObject: MPMDbObject<WmsUnitDataContext>, IMutationSiteLineObject
    {
        public MutationSiteLineObject()
            : base()
        {
        }

        public MPMWMS_MUTATION_SITE_LINE Item(String dataarea_id, String mutation_id, String machine_id, String frame_id)
        {
            IQueryable<MPMWMS_MUTATION_SITE_LINE> search =
                from line in Context.MPMWMS_MUTATION_SITE_LINEs
                where
                    line.DATAAREA_ID == dataarea_id
                    && line.MUTATION_ID == mutation_id
                    && line.MACHINE_ID == machine_id
                    && line.FRAME_ID == frame_id
                select line;
            return search.Take(1).FirstOrDefault();
        }

        public MPMWMS_MUTATION_SITE_LINE Item1(String dataarea_id, String mutation_id, String machine_id, String frame_id)
        {
            IQueryable<MPMWMS_MUTATION_SITE_LINE> search =
                from line in Context.MPMWMS_MUTATION_SITE_LINEs
                where
                    line.DATAAREA_ID == dataarea_id
                    && line.MUTATION_ID == mutation_id
                    && line.MACHINE_ID == machine_id
                    && line.FRAME_ID == frame_id
                    && line.STATUS == "R"
                select line;
            return search.Take(1).FirstOrDefault();
        }

        public List<MPMWMS_MUTATION_SITE_LINE> List(String dataarea_id, String mutation_id)
        {
            IQueryable<MPMWMS_MUTATION_SITE_LINE> search =
                from line in Context.MPMWMS_MUTATION_SITE_LINEs
                where
                    line.DATAAREA_ID == dataarea_id
                    && line.MUTATION_ID == mutation_id
                orderby line.STATUS ascending   
                select line;
            return search.ToList();
        }

        //tambahan anas 3-12-2019
        public List<MPMWMS_MUTATION_SITE_LINE_RECORD> List1(String dataarea_id, String mutation_id)
        {
            IQueryable<MPMWMS_MUTATION_SITE_LINE_RECORD> search =
                from line1 in Context.MPMWMS_MUTATION_SITE_LINEs
                join unit1 in Context.MPMWMS_UNITs on line1.MACHINE_ID equals unit1.MACHINE_ID
                where
                    line1.DATAAREA_ID == dataarea_id
                    && line1.MUTATION_ID == mutation_id 
                    //&& unit1.QR_CODE !=null
                orderby line1.STATUS ascending
                select new MPMWMS_MUTATION_SITE_LINE_RECORD {
                    _STATUS = line1.STATUS,
                    _MACHINE_ID = line1.MACHINE_ID,
                    _FRAME_ID = line1.FRAME_ID,
                    _MUTATION_ID = line1.MUTATION_ID,
                    _SITE_ID_INTR = line1.SITE_ID_INTR,
                    _WAREHOUSE_ID_INTR = line1.WAREHOUSE_ID,
                    _LOCATION_ID_INTR = line1.LOCATION_ID_INTR,
                    _SITE_ID_NEW = line1.SITE_ID_NEW,
                    _WAREHOUSE_ID_NEW = line1.WAREHOUSE_ID_NEW,
                    _LOCATION_ID_NEW = line1.LOCATION_ID_NEW,
                    _TRANSFER_ID = line1.TRANSFER_ID,
                    _QR_CODE = unit1.QR_CODE
                };
            return search.ToList();
        }

        public int ValidasiQtyReceived(String machine_id, String transferid)
        {
            try
            {
                var itemsObj = from invtab in Context.INVENTTRANSFERTABLEs
                               join invtrans in Context.INVENTTRANSFERLINEs
                               on new { invtab.TRANSFERID, invtab.PARTITION } equals new { invtrans.TRANSFERID, invtrans.PARTITION }
                               where invtab.DATAAREAID == UserSession.DATAAREA_ID
                               && invtab.TRANSFERID == transferid
                               && invtrans.MPMMACHINENO == machine_id
                               select invtrans;
                var currentItem = itemsObj.Take(1).FirstOrDefault().QTYRECEIVED;
                return Convert.ToInt32(currentItem);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public MPMWMS_MUTATION_SITE_LINE Create()
        {
            MPMWMS_MUTATION_SITE_LINE item = new MPMWMS_MUTATION_SITE_LINE();
            return item;
        }

        public void Insert(MPMWMS_MUTATION_SITE_LINE row)
        {
            row.STATUS = "O";
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_MUTATION_SITE_LINEs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_MUTATION_SITE_LINE row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_MUTATION_SITE_LINE row)
        {
            Context.MPMWMS_MUTATION_SITE_LINEs.DeleteOnSubmit(row);
        }
    }
}
