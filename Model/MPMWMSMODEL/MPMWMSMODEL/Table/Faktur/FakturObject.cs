﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Exception;

namespace MPMWMSMODEL.Table.Faktur
{
    public class FakturObject : MPMDbObject<WmsUnitDataContext>, IFakturObject
    {
        public FakturObject()
            : base()
        {
        }

        public MPMINVFAKTUR Item(String dataarea_id, String machine_id)
        {
            IQueryable<MPMINVFAKTUR> search =   from faktur in Context.MPMINVFAKTURs
                                                where (faktur.DATAAREAID == dataarea_id) &&
                                                (faktur.ENGINENO == machine_id) &&
                                                //(faktur.SRMESIN  + faktur.NOMESIN== machine_id) &&
                                                //(faktur.NORANGKA == frame_id) &&
                                                (faktur.PARTITION == 5637144576)
                                                select faktur;
            return search.FirstOrDefault();
        }

        public long Partition(String dataarea_id, String machine_id)
        {
            return Item(dataarea_id, machine_id).PARTITION;
        }

        public List<MPMINVFAKTUR> List()
        {
            return Context.MPMINVFAKTURs.ToList();
        }

        /*
        public List<MPMINVFAKTUR> List(String dataarea_id, String no_mesin, String sr_mesin)
        {
            try
            {
                IQueryable<MPMINVFAKTUR> search = Item(dataarea_id, no_mesin, sr_mesin);
                var currentItem = search.FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        */
    }
}
