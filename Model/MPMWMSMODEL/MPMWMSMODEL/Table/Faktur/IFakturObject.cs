﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Faktur
{
    public interface IFakturObject
    {
        MPMINVFAKTUR Item(String dataarea_id, String machine_id);
        List<MPMINVFAKTUR> List();
        long Partition(String dataarea_id, String machine_id);
    }
}
