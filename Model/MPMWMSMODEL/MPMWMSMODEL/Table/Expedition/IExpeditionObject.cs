﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Expedition
{
    public interface IExpeditionObject
    {
        MPMWMS_EXPEDITION Create();
        List<MPMWMS_EXPEDITION> List();
        void Refresh();
        MPMWMS_EXPEDITION Item(String expedition_id, String dataarea_id, String maindealer_id, String site_id);
        void Insert(MPMWMS_EXPEDITION row);
        void Delete(MPMWMS_EXPEDITION row);
    }
}
