﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.DataArea;
using MPMWMSMODEL.Table.ExpeditionLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Expedition
{
    public class ExpeditionObject : MPMDbObject<WmsUnitDataContext>, IExpeditionObject
    {
        ExpeditionLineObject _ExpeditionLineObject = new ExpeditionLineObject();

        public ExpeditionObject()
            : base()
        {
        }

        public ExpeditionLineObject ExpeditionLineObject
        {
            get
            {
                return _ExpeditionLineObject;
            }
        }

        public MPMWMS_EXPEDITION Create()
        {
            MPMWMS_EXPEDITION item = new MPMWMS_EXPEDITION();
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.IS_ACTIVE = "Y";
            return item;
        }

        public List<MPMWMS_EXPEDITION> List()
        {
            try
            {
                var itemsObj = from expedition in Context.MPMWMS_EXPEDITIONs
                               where
                               (expedition.DATAAREA_ID == UserSession.DATAAREA_ID)
                               && (expedition.MAINDEALER_ID == UserSession.MAINDEALER_ID)
                               && (expedition.SITE_ID == UserSession.SITE_ID_MAPPING)
                               select expedition;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
            //return Context.MPMWMS_EXPEDITIONs.ToList();
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_EXPEDITIONs);
        }

        public MPMWMS_EXPEDITION Item(String expedition_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from expedition in Context.MPMWMS_EXPEDITIONs
                               where (expedition.EXPEDITION_ID == expedition_id) 
                               && (expedition.DATAAREA_ID == dataarea_id)
                               && (expedition.MAINDEALER_ID == maindealer_id)
                               && (expedition.SITE_ID == site_id)
                               select expedition;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EXPEDITION row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EXPEDITIONs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_EXPEDITION row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void Delete(MPMWMS_EXPEDITION row)
        {
            try
            {
                Context.MPMWMS_EXPEDITIONs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
