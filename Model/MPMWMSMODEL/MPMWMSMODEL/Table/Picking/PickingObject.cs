﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.PickingLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Picking
{
    public class POLICE_NUMBER_PICKING
    {
        public String POLICE_NUMBER { get; set; }

        public POLICE_NUMBER_PICKING(String police_number)
        {
            POLICE_NUMBER = police_number;
        }
    }


    public class PickingObject : MPMDbObject<WmsUnitDataContext>, IPickingObject 
    {
        PickingLineObject _PickingLineObject = new PickingLineObject();
    
        public PickingObject()
            : base()
        {
        }

        public PickingLineObject PickingLineObject
        {
            get
            {
                return _PickingLineObject;
            }
        }

        public List<MPMWMS_PICKING> List(String sales_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from picking in Context.MPMWMS_PICKINGs
                               where (picking.DO_ID == sales_id) &&
                               (picking.DATAAREA_ID == dataarea_id) &&
                               (picking.MAINDEALER_ID == maindealer_id) &&
                               (picking.SITE_ID == site_id)
                               select picking;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING> ListAllPicking(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from picking in Context.MPMWMS_PICKINGs
                               where
                               (picking.DATAAREA_ID == dataarea_id) &&
                               (picking.MAINDEALER_ID == maindealer_id) &&
                               (picking.SITE_ID == site_id) &&
                               (picking.STATUS == "O")
                               select picking;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<String> ListPickingByPoliceNumber(String dataarea_id, String maindealer_id, String site_id, String police_number)
        {
            try
            {
                IQueryable<String> list =
                    from p in Context.MPMWMS_POLICE_NUMBER_PICKINGs
                    where
                        (p.DATAAREA_ID == dataarea_id)
                        && (p.MAINDEALER_ID == maindealer_id)
                        && (p.SITE_ID == site_id)
                        && (p.POLICE_NUMBER == police_number)
                    select p.PICKING_ID;

                return list.ToList();

                /*
                IQueryable<MPMWMS_PICKING> itemsObj =
                    from picking in Context.MPMWMS_PICKINGs
                    where
                    (picking.DATAAREA_ID == dataarea_id) &&
                    (picking.MAINDEALER_ID == maindealer_id) &&
                    (picking.SITE_ID == site_id) &&
                    (picking.POLICE_NUMBER == police_number)
                    orderby picking.PICKING_DATE ascending
                    select picking;

                List<MPMWMS_PICKING> list = new List<MPMWMS_PICKING>();

                foreach (var item in itemsObj)
                {
                    int a_count = item.MPMWMS_PICKING_LINEs.ToList().Count();

                    IQueryable<MPMWMS_PICKING_TAKE_MAPPING> b =
                        from a in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                        where a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.PICKING_ID == item.PICKING_ID
                        && a.DO_ID == item.DO_ID
                        select a;
                    int b_count = b.ToList().Count();

                    if (a_count != b_count)
                    {
                        if (a_count > 0)
                        {
                            list.Add(item);
                            //if (list.Count() == 8) break;
                        }
                    }
                }

                return list.ToList();*/
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<POLICE_NUMBER_PICKING> ListCurrentPoliceNumberOnTheDay(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<POLICE_NUMBER_PICKING> list =
                    from p in Context.MPMWMS_POLICE_NUMBER_PICKINGs
                    where
                        (p.DATAAREA_ID == dataarea_id)
                        && (p.MAINDEALER_ID == maindealer_id)
                        && (p.SITE_ID == site_id)
                    group p by p.POLICE_NUMBER into g
                    select new POLICE_NUMBER_PICKING(g.Key);

                return list.ToList();
                    

                /*IQueryable<MPMWMS_PICKING> itemsObj = 
                    from picking in Context.MPMWMS_PICKINGs
                    where (picking.DATAAREA_ID == dataarea_id) 
                    && (picking.EXPEDITION_ID != null)
                    && (picking.MAINDEALER_ID == maindealer_id)
                    && (picking.SITE_ID == site_id)
                    && (picking.STATUS == "O")
                    //(picking.PICKING_DATE.ToShortDateString() == MPMDateUtil.DateTime.ToShortDateString())
                    //group picking by picking.POLICE_NUMBER into picking_group
                    orderby picking.PICKING_DATE ascending
                    select picking;

                List<POLICE_NUMBER_PICKING> list = new List<POLICE_NUMBER_PICKING>();
                foreach (var item in itemsObj)
                {
                    IQueryable<MPMWMS_PICKING_LINE> a_count =
                        from a in Context.MPMWMS_PICKING_LINEs
                        where a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.PICKING_ID == item.PICKING_ID
                        && a.DO_ID == item.DO_ID
                        select a;

                    IQueryable<MPMWMS_PICKING_TAKE_MAPPING> b_count =
                        from a in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                        where a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.PICKING_ID == item.PICKING_ID
                        && a.DO_ID == item.DO_ID
                        select a;

                    if (a_count.ToList().Count() > 0)
                    {
                        if (a_count.ToList().Count() != b_count.ToList().Count())
                        {
                            if ((list.Find(r => r.POLICE_NUMBER == item.POLICE_NUMBER) == null) 
                                && (a_count.Take(1).FirstOrDefault().SITE_ID == UserSession.SITE_ID_MAPPING)) {
                                list.Add(new POLICE_NUMBER_PICKING(item.POLICE_NUMBER));
                            }

                            // TODO open to break the police number
                            //if (list.Count() == 8) break;
                        }
                    }
                }*/

                //return list.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<POLICE_NUMBER_PICKING> ListCurrentPoliceNumberOnTheDay(String dataarea_id, String maindealer_id, String site_id, String policeno)
        {
            try
            {
                IQueryable<POLICE_NUMBER_PICKING> list =
                    from p in Context.MPMWMS_POLICE_NUMBER_PICKINGs
                    where
                        (p.DATAAREA_ID == dataarea_id)
                        && (p.MAINDEALER_ID == maindealer_id)
                        && (p.SITE_ID == site_id)
                        && (p.POLICE_NUMBER.Contains(policeno))
                    group p by p.POLICE_NUMBER into g
                    select new POLICE_NUMBER_PICKING(g.Key);

                return list.ToList();

            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_PICKING Create()
        {
            MPMWMS_PICKING row = new MPMWMS_PICKING();
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.STATUS = "O";
            row.IS_CANCEL = "N";
            row.IS_PRINT = "1";
            return row;
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_PICKINGs);
        }

        public void Insert(MPMWMS_PICKING row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_PICKINGs.InsertOnSubmit(row);
        }

        private IQueryable<MPMWMS_PICKING> RunQuery(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            return from picking in Context.MPMWMS_PICKINGs
                   where (picking.PICKING_ID == picking_id) &&
                   (picking.DATAAREA_ID == dataarea_id) &&
                   (picking.MAINDEALER_ID == maindealer_id) &&
                   (picking.SITE_ID == site_id)
                   select picking;
        }

        private IQueryable<MPMWMS_PICKING> RunQuery(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id)
        {
            return from picking in Context.MPMWMS_PICKINGs
                   where (picking.PICKING_ID == picking_id) &&
                   (picking.DO_ID == do_id) &&
                   (picking.DATAAREA_ID == dataarea_id) &&
                   (picking.MAINDEALER_ID == maindealer_id) &&
                   (picking.SITE_ID == site_id)
                   select picking;
        }

        public MPMWMS_PICKING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<MPMWMS_PICKING> search = RunQuery(picking_id, dataarea_id, maindealer_id, site_id);
                var currentItem = search.FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_PICKING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id)
        {
            try
            {
                IQueryable<MPMWMS_PICKING> search = RunQuery(picking_id, dataarea_id, maindealer_id, site_id, do_id);
                var currentItem = search.FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_PICKING row)
        {
            try
            {
                Context.MPMWMS_PICKINGs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING> ListPrint(String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from picking in Context.MPMWMS_PICKINGs
                               where 
                               (picking.DATAAREA_ID == dataarea_id) &&
                               (picking.MAINDEALER_ID == maindealer_id) &&
                               (picking.SITE_ID == site_id) &&
                               (picking.IS_PRINT == "1")
                               select picking;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_PICKING row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = MPMDateUtil.DateTime;
        }
    }
}
