﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Picking
{
    public interface IPickingObject
    {
        void Refresh();
        List<MPMWMS_PICKING> List(String sales_id, String dataarea_id, String maindealer_id, String site_id);
        List<MPMWMS_PICKING> ListPrint(String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_PICKING Create();
        void Insert(MPMWMS_PICKING row);
        void Update(MPMWMS_PICKING row);
        MPMWMS_PICKING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id);
        void Delete(MPMWMS_PICKING row);
    }
}
