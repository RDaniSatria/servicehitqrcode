﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Picking
{
    public class PICKING_SLIP
    {
        public String PICKING_ID { get; set; }
        public DateTime PICKING_DATE { get; set; }
        public String DO_ID { get; set; }
        public String DATAAREA_ID { get; set; }
        public String DEALER_CODE { get; set; }
        public String DEALER_NAME { get; set; }
        public String DEALER_ADDRESS { get; set; }
        public String DEALER_CITY { get; set; }
        public String MACHINE_ID { get; set; }
        public String FRAME_ID { get; set; }
        public String UNIT_TYPE { get; set; }
        public String COLOR_TYPE { get; set; }
        public String SITE_ID { get; set; }
        public String LOCATION_ID { get; set; }

        public PICKING_SLIP(
            String picking_id,
            DateTime picking_date,
            String do_id,
            String dataarea_id,
            String dealer_code,
            String dealer_name,
            String dealer_address,
            String dealer_city,
            String machine_id,
            String frame_id,
            String unit_type,
            String color_type,
            String site_id,
            String location_id)
        {
            PICKING_ID = picking_id;
            PICKING_DATE = picking_date;
            DO_ID = do_id;
            DATAAREA_ID = dataarea_id;
            DEALER_CODE = dealer_code;
            DEALER_NAME = dealer_name;
            DEALER_ADDRESS = dealer_address;
            DEALER_CITY = dealer_city;
            MACHINE_ID = machine_id;
            FRAME_ID = frame_id;
            UNIT_TYPE = unit_type;
            COLOR_TYPE = color_type;
            SITE_ID = site_id;
            LOCATION_ID = location_id;
        }
    }
}
