﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ClockingUnit
{
    public class ClockingUnitObject : MPMDbObject<WmsUnitDataContext>, IClockingUnitObject
    {
        public List<ClockingUnitLkujasarepairRec> ListJasaRepair(string nosin, string userid)
        {
            try
            {
                var data = (from b in Context.MPMDETAILNONLKUs
                           where b.TYPEREPAIR.Equals("Repair") && b.NOMESIN.Equals(nosin) 
                           group b by b.ITEM
                           into g
                           select g.Key).ToList();

                var List = from a in Context.MPMLKUJASAREPAIRs
                           where !data.Contains(a.REPAIR)
                           select new ClockingUnitLkujasarepairRec {
                               repair = a.REPAIR,
                               harga = string.Format("{0:#,###}", a.HARGA)
                           };
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLkujasabongkarRec> ListJasaBongkar(string nosin, string userid)
        {
            try
            {
                var data = (from b in Context.MPMDETAILNONLKUs
                            where b.TYPEREPAIR.Equals("Bongkar Part") && b.NOMESIN.Equals(nosin) 
                            group b by b.ITEM
                            into g
                            select g.Key).ToList();

                var List = from a in Context.MPMLKUJASABONGKARPARTs
                           join b in Context.MPMSERVVIEWDAFTARPARTs
                           on new { a.ITEMID, a.PARTITION, a.DATAAREAID } equals new { b.ITEMID, b.PARTITION, b.DATAAREAID }
                           where !data.Contains(a.ITEMID)
                           select new ClockingUnitLkujasabongkarRec
                           {
                               itemid = a.ITEMID,
                               name = b.ITEMNAME,
                               harga = string.Format("{0:#,###}", a.HARGA)
                           };
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitNonLkujasa> ListNonLku(string nomesin)
        {
            try
            {
                var List = from a in Context.MPMDETAILNONLKUs
                           join b in Context.MPMLKUJASABONGKARPARTs
                           on a.ITEM equals b.ITEMID into l1
                           from d in l1.DefaultIfEmpty()
                           join c in Context.MPMLKUJASAREPAIRs
                           on a.ITEM equals c.REPAIR into l2
                           from e in l2.DefaultIfEmpty()
                           where a.NOMESIN == nomesin
                           select new ClockingUnitNonLkujasa(1, a.TYPEREPAIR,a.ITEM, e.HARGA, d.HARGA);
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        public List<ClockingUnit> ListUnit(string nosin)
        {
            try
            {
                var data = from a in Context.MPMWMS_UNITs
                           where a.MACHINE_ID.Equals(nosin) && a.SITE_ID == UserSession.SITE_ID_MAPPING && a.STATUS != "RFS"
                           select new ClockingUnit
                           {
                               unit = a.MACHINE_ID
                           };
                return data.ToList();


            }
            catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
            
        }
        public List<ClockingUnit> ListHistoryUnit(string nosin)
        {
            try
            {
                var data = from a in Context.MPMWMSHISTCLOCKINGs
                           where a.NOMESIN.Equals(nosin) && a.USERID.Equals(UserSession.USER_ID)
                           select new ClockingUnit
                           {
                               unit = a.NOMESIN,
                               statuslku = a.TYPEPERBAIKAN
                           };
                return data.ToList();


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }
        public List<ClockingTujuanUnit> ListTujuanUnit(string nosin)
        {
            try
            {
                var data = from a in Context.MPMLKUROBBINGPARTs
                           join b in Context.MPMSERVVIEWDAFTARPARTs
                            on a.KODEPART equals b.ITEMID
                            into left
                           from c in left.DefaultIfEmpty()
                           where a.NOMESINTUJUAN.Equals(nosin) && a.USERID.Equals(UserSession.USER_ID)
                           select new ClockingTujuanUnit
                           {
                              nosinawal = a.NOMESINAWAL,
                              kodepart = a.KODEPART,
                              namapart = c.ITEMNAME
                              
                           };
                return data.ToList();


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }
        public List<ClockingTujuanUnit> ValidasiDetailNonLKU(string nosin, string kodepart)
        {
            try
            {

                var data = from a in Context.MPMDETAILNONLKUs
                           where a.NOMESIN.Equals(nosin) && a.ITEM.Equals(kodepart)
                           select new ClockingTujuanUnit
                           {
                               nosinawal = a.NOMESIN,
                               kodepart = a.ITEM

                           };
                return data.ToList();


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }

        public List<ClockingTujuanUnit> ValidasiSalLKU(string nosin, string kodepart)
        {
            try
            {

                var data = from a in Context.MPMSALLKULINEs
                           where a.SRMESIN+a.NOMESIN == nosin && a.KODEPART.Equals(kodepart)
                           select new ClockingTujuanUnit
                           {
                               nosinawal = a.NOMESIN,
                               kodepart = a.KODEPART

                           };
                return data.ToList();


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }

        public List<ClockingTujuanUnit> ListCekDataMoving(string nosin, string kodepart)
        {
            try
            {

                var data = from a in Context.MPMLKUROBBINGPARTs
                           where a.NOMESINAWAL.Equals(nosin) && a.KODEPART.Equals(kodepart)
                           select new ClockingTujuanUnit
                           {
                               nosinawal = a.NOMESINAWAL,
                               kodepart = a.KODEPART,
                               namapart = a.NOMESINTUJUAN

                           };
                return data.ToList();


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }
        public List<ClockingAsalUnit> ListAsalUnitClocking(string nosin, string type)
        {
            try
            {
                var tujuan = (from a in Context.MPMLKUROBBINGPARTs
                             where a.NOMESINTUJUAN.Equals(nosin)
                             group a by a.KODEPART into g
                             select g.Key).ToList();

                List < ClockingAsalUnit > data = new List<ClockingAsalUnit>();
                if(type == "NON LKU")
                {
                    data = (from a in Context.MPMDETAILNONLKUs
                            join b in Context.MPMSERVVIEWDAFTARPARTs
                            on a.ITEM equals b.ITEMID
                            where a.NOMESIN.Equals(nosin) 
                            && !tujuan.Contains(a.ITEM)
                            group new { a, b } by new { a.ITEM, b.ITEMNAME } into g
                            select new ClockingAsalUnit
                            {
                                kodepart = g.Key.ITEM,
                                namapart = g.Key.ITEMNAME
                            }).ToList();
                }
                if (type == "LKU")
                {
                    data = (from a in Context.MPMSALLKULINEs
                            join b in Context.MPMSERVVIEWDAFTARPARTs
                            on a.KODEPART equals b.ITEMID into left
                            from c in left.DefaultIfEmpty()
                            where a.NOMERMESIN.Replace(" ","").Equals(nosin) && !tujuan.Contains(a.KODEPART)
                            group new {a,c } by new { a.KODEPART, c.ITEMNAME } into g
                            select new ClockingAsalUnit
                            {
                                kodepart = g.Key.KODEPART,
                                namapart = g.Key.ITEMNAME
                            }).ToList();
                }


                return data;


            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }

        }

        public List<ClockingUnitLisKerusakan> ListKerusakanLku(String Nomesin)
        {
            try
            {
                var List = from a in Context.MPMSALLKULINEs
                           where a.SRMESIN + a.NOMESIN == Nomesin
                           select new ClockingUnitLisKerusakan(a.KODEPART, a.ITEMREPAIR, a.PARTNAME);
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLisKerusakan> ListKerusakan(String Nomesin)
        {
            try
            {
                var List = from a in Context.MPMDETAILNONLKUs
                           join b in Context.MPMSERVVIEWDAFTARPARTs
                           on a.ITEM equals b.ITEMID into l1
                           from d in l1.DefaultIfEmpty()
                           where a.NOMESIN == Nomesin
                           select new ClockingUnitLisKerusakan(a.TYPEREPAIR, a.ITEM, d.ITEMNAME);
                return List.ToList();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitAsal> ListAsal(string NoMesin)
        {
            try
            {
                var itemin = from a in Context.MPMLKUROBBINGPARTs
                             select a.KODEPART;
                var List = from a in Context.MPMSALLKULINEs
                           join b in Context.MPMSERVVIEWDAFTARPARTs
                           on a.KODEPART equals b.ITEMID
                           where a.SRMESIN + a.NOMESIN == NoMesin && itemin.Contains(a.KODEPART)
                           select new ClockingUnitAsal
                           {
                               kodetype = a.KODEPART,
                               namepart = b.ITEMNAME
                           };
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitListPending> ListPending(string userid, string siteid, string statuskerja)
        {
            try
            {
                Context.CommandTimeout = 50000;
                Context.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
                IEnumerable<ClockingUnitListPending> pending = null;
                pending = Context.ExecuteQuery<ClockingUnitListPending>("EXEC MPMSPWMSSELECTCLOCKING '" + userid + "'"+",'"+siteid+"','"+statuskerja+"'");

                return pending.ToList();

            }
            catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<MPMDETAILNONLKU> ListDetailNonLku(string Nomesin)
        {
            try
            {
                var List = from a in Context.MPMDETAILNONLKUs
                           where a.NOMESIN == Nomesin
                           select a;
                return List.ToList();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<MPMWMSHISTPENDINGMEKANIK> ListPendingClocking(string Nomesin)
        {
            try
            {
                var List = from a in Context.MPMWMSHISTPENDINGMEKANIKs
                           where a.NOMESIN == Nomesin && a.TGLJAMSELESAI == null && a.STATUSKERJA == 2
                           select a;
                return List.ToList();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<MPMSALLKULINE> ListSalLkuLine(string Nomesin)
        {
            try
            {
                var List = from a in Context.MPMSALLKULINEs
                           where a.SRMESIN + a.NOMESIN == Nomesin
                           select a;
                return List.ToList();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public MPMSALLKULINE ItemSalLKU(string Nomesin)
        {
            var item = from a in Context.MPMSALLKULINEs
                       where a.SRMESIN + a.NOMESIN == Nomesin
                       select a;
            return item.Take(1).FirstOrDefault();
        }
        public MPMSALLKUHEADER ItemSalLKUH(string Nomesin)
        {
            var item = from b in Context.MPMSALLKUHEADERs
                       join a in Context.MPMSALLKULINEs
                       on b.RECID equals a.MPMSALLKUHEADER
                       where a.SRMESIN + a.NOMESIN == Nomesin
                       select b;
            return item.Take(1).FirstOrDefault();
        }
        public void UpdateSalLKU(MPMSALLKULINE item)
        {
            
        }
        public MPMDETAILNONLKU ItemDetailNonLku(string Nomesin,string Item)
        {
            try
            {
                var List = from a in Context.MPMDETAILNONLKUs
                           where a.NOMESIN == Nomesin && a.ITEM == Item
                           select a;
                return List.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public MPMLKUROBBINGPART ItemDetailTujaun(string NosinTujan, string NosinAwal, string Item)
        {
            try
            {
                var List = from a in Context.MPMLKUROBBINGPARTs
                           where a.NOMESINAWAL == NosinAwal && a.NOMESINTUJUAN == NosinTujan && a.KODEPART == Item
                           select a;
                return List.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void DeleteDetailNonLku(string Nomesin,string Item)
        {
            try
            {
                var item = ItemDetailNonLku(Nomesin, Item);
                Context.MPMDETAILNONLKUs.DeleteOnSubmit(item);
            }
            catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void DeleteDetailTujuan(string Nosinawal, string NosinTujuan, string Item)
        {
            try
            {
                var item = ItemDetailTujaun(NosinTujuan, Nosinawal,Item);
                Context.MPMLKUROBBINGPARTs.DeleteOnSubmit(item);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void InsertDetailNonLku(MPMDETAILNONLKU Item)
        {
            try
            {
                Item.CREATEBY = UserSession.USER_ID;
                Item.CREATEDATE = DateTime.Now;
                Context.MPMDETAILNONLKUs.InsertOnSubmit(Item);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public MPMWMSHISTCLOCKING CekItemCloking(string Nomesin)
        {
            try
            {
                var item = from a in Context.MPMWMSHISTCLOCKINGs
                           where a.NOMESIN == Nomesin
                           orderby a.TGLJAMMULAI descending
                           select a;
                return item.Take(1).FirstOrDefault();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public MPMWMSHISTPENDINGMEKANIK Itempending(string Nomesin)
        {
            try
            {
                var item = from a in Context.MPMWMSHISTPENDINGMEKANIKs
                           where a.NOMESIN == Nomesin
                           && a.STATUSKERJA == 2
                           orderby a.TGLJAMMULAI descending
                           select a;
                return item.Take(1).FirstOrDefault();
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void UpdatePending(MPMWMSHISTPENDINGMEKANIK item)
        {
            try
            {
                item.MODIFBY = UserSession.USER_ID;
                item.MODIFDATE = DateTime.Now;

            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void InsertMovingPart(MPMLKUROBBINGPART Item)
        {
            try
            {
                Item.CREATEBY = UserSession.USER_ID;
                Item.CREATEDATE = DateTime.Now;
                Context.MPMLKUROBBINGPARTs.InsertOnSubmit(Item);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void InsertPending(MPMWMSHISTPENDINGMEKANIK Item)
        {
            try
            {
                Item.CREATEBY = UserSession.USER_ID;
                Item.CREATEDATE = DateTime.Now;
                Context.MPMWMSHISTPENDINGMEKANIKs.InsertOnSubmit(Item);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public MPMLKUROBBINGPART ItemRobbing(string nomesinawal,string nomesintujuan,string kodepart)
        {
            try
            {
                var item = Context.MPMLKUROBBINGPARTs.Where(x => x.NOMESINAWAL == nomesinawal && x.NOMESINTUJUAN == nomesintujuan && x.KODEPART == kodepart).Take(1).FirstOrDefault();
                return item;
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void InsertRobbing(MPMLKUROBBINGPART item)
        {
            try
            {
                item.CREATEBY = UserSession.USER_ID;
                item.CREATEDATE = DateTime.Now;
                Context.MPMLKUROBBINGPARTs.InsertOnSubmit(item);
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void UpdateRobbing(MPMLKUROBBINGPART item)
        {
            try
            {
                item.MODIFBY = UserSession.USER_ID;
                item.MODIFDATE = DateTime.Now;
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void DeleteRobbing(MPMLKUROBBINGPART item)
        {
            try
            {
                var items = ItemRobbing(item.NOMESINAWAL, item.NOMESINTUJUAN, item.KODEPART);
                Context.MPMLKUROBBINGPARTs.DeleteOnSubmit(items);
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public Time[] ListTime(int statuskerja, string unit, string userid)
        {
            try
            {

                Context.CommandTimeout = 50000;
                Context.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
                IEnumerable<Time> vo_titipan = null;
                vo_titipan = Context.ExecuteQuery<Time>("EXEC MPMWMSTIMERCLOCKING '" + userid + "'," + statuskerja + ",'" + unit + "'");

                return vo_titipan.ToArray();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
    }
}
