﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ClockingUnit
{
    public class ClockingUnitRec
    {
    }
    public class ClockingUnitLkujasarepairRec
    {
        public string repair { get; set; }
        public string harga { get; set; }
    }
    public class ClockingUnitLkujasabongkarRec
    {
        public string itemid { get; set; }
        public string name { get; set; }
        public string harga { get; set; }
    }
    public class ClockingUnitNonLkujasa
    {
        public int no { get; set; }
        public string type { get; set; }
        public string item { get; set; }
        public string harga { get; set; }
        public ClockingUnitNonLkujasa(int _no,string _type,string _item, decimal? _hargajasa, decimal? _hargabongkar)
        {
            no = _no;
            type = _type;
            item = _item;
            if(_type == "Bongkar Part")
            {
                harga = String.Format("{0:#,###}", _hargabongkar);
            }
            else
            {
                harga = String.Format("{0:#,###}", _hargajasa);
            }
        }
    }
    public class ClockingUnitLisKerusakan
    {
        public string typepart { get; set; }
        public string itempart { get; set; }
        public string namepart { get; set; }
        public ClockingUnitLisKerusakan(string _typepart,string _itempart,string _namepart = "-")
        {
            typepart = _typepart;
            itempart = _itempart;
            namepart = _namepart;
        }
    }
    public class ClockingUnitAsal
    {
        public string kodetype { get; set; }
        public string namepart { get; set; }
    }
    public class ClockingUnitListPending
    {
        public string type { get; set; }
        public string unit { get; set; }
        public string userid { get; set; }
        public string lokasi { get; set; }
        public string statusqc { get; set; }
        public string status { get; set; }
        public string waktu { get; set; }
    }
    public class ClockingUnitNonLku
    {
        public string nomesin { get; set; }
        public DateTime? tgllku { get; set; }
        public string tgl { get; set; }
        public string tglqc { get; set; }
        public string userid { get; set; }
        public string status { get; set; }
        public int? statuskerja { get; set; }
        public int? statusqc { get; set; }
    }
    public class Time
    {
        public int hari { get; set; }
        public int jam { get; set; }
        public int menit { get; set; }
        public int detik { get; set; }
    }
    public class ClockingUnit
    {
        public string unit { get; set; }
        public string statuslku { get; set; }
    }
    public class ClockingAsalUnit
    {
        public string kodepart { get; set; }
        public string namapart { get; set; }
    }
    public class ClockingTujuanUnit
    {
        public string nosinawal { get; set; }
        public string kodepart { get; set; }
        public string namapart { get; set; }
    }

}
