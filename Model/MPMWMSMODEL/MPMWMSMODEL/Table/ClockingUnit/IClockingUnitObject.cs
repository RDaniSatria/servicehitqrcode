﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ClockingUnit
{
    public interface IClockingUnitObject
    {
        List<ClockingUnitListPending> ListPending(string userid, string siteid, string statuskerja);
        MPMWMSHISTPENDINGMEKANIK Itempending(string Nomesin);
    }
}
