﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.EcoResProduct
{
    public class EcoResProductObject: MPMDbObject<WmsUnitDataContext>, IEcoResProductObject
    {
        public EcoResProductObject()
            : base()
        {
        }

        public ECORESPRODUCT Item(String item_id)
        {
            IQueryable<ECORESPRODUCT> search =
                from eco in ((WmsUnitDataContext)Context).ECORESPRODUCTs
                where
                    eco.DISPLAYPRODUCTNUMBER == item_id
                select eco;

            return search.Take(1).FirstOrDefault();
        }
    }
}
