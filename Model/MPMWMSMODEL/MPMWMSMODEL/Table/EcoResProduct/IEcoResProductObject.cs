﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.EcoResProduct
{
    public interface IEcoResProductObject
    {
        ECORESPRODUCT Item(String item_id);
    }
}
