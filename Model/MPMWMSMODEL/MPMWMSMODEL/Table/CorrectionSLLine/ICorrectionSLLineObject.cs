﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CorrectionSLLine
{
    public interface ICorrectionSLLineObject
    {
        List<MPMWMS_CORRECTION_SL_LINE> List(MPMWMS_CORRECTION_SL header);
        MPMWMS_CORRECTION_SL_LINE Create(MPMWMS_CORRECTION_SL header);
        void Insert(MPMWMS_CORRECTION_SL_LINE item);
    }
}
