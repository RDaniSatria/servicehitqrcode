﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.ShippingList;
using MPMWMSMODEL.Table.ShippingListLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CorrectionSLLine
{
    public class CorrectionSLLineObject: MPMDbObject<WmsUnitDataContext>, ICorrectionSLLineObject
    {
        public CorrectionSLLineObject()
            : base()
        {

        }

        public List<MPMWMS_CORRECTION_SL_LINE> List(MPMWMS_CORRECTION_SL header)
        {
            try
            {
                var search =
                    from a in Context.MPMWMS_CORRECTION_SL_LINEs
                    where
                        a.DATAAREA_ID == header.DATAAREA_ID
                        && a.MAINDEALER_ID == header.MAINDEALER_ID
                        && a.SITE_ID == header.SITE_ID
                        && a.CORRECTION_SL_ID == header.CORRECTION_SL_ID
                    select a;

                return search.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_CORRECTION_SL_LINE Create(MPMWMS_CORRECTION_SL header)
        {
            MPMWMS_CORRECTION_SL_LINE row = new MPMWMS_CORRECTION_SL_LINE();
            row.DATAAREA_ID = header.DATAAREA_ID;
            row.MAINDEALER_ID = header.MAINDEALER_ID;
            row.SITE_ID = header.SITE_ID;
            row.CORRECTION_SL_ID = header.CORRECTION_SL_ID;
            row.CORRECTION_SL_DATE = header.CORRECTION_SL_DATE;
            row.RETUR_SL_ID_1 = header.RETUR_SL_ID_1;
            row.RETUR_SL_ID_2 = header.RETUR_SL_ID_2;
            row.SHIPPING_LIST_ID_1 = header.SHIPPING_LIST_ID_1;
            row.SHIPPING_LIST_ID_2 = header.SHIPPING_LIST_ID_2;
            return row;
        }

        public void Insert(MPMWMS_CORRECTION_SL_LINE item)
        {
            item.STATUS = "O";
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_CORRECTION_SL_LINEs.InsertOnSubmit(item);
        }
    }
}
