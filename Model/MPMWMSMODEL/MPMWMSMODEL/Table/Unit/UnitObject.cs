﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record;
using MPMWMSMODEL.Table.EcoResProduct;
using MPMWMSMODEL.Table.HistoryUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.Unit
{
    public class UnitObject : MPMDbObject<WmsUnitDataContext>, IUnitObject
    {
        private HistoryUnitObject _HistoryUnitObject = new HistoryUnitObject();

        public UnitObject()
            : base()
        {

        }

        public HistoryUnitObject HistoryUnitObject
        {
            get
            {
                return _HistoryUnitObject;
            }
        }

        public MPMWMS_UNIT Create()
        {
            MPMWMS_UNIT item = new MPMWMS_UNIT();
            item.UNIT_GROUP = "C";
            item.STATUS = "U";
            item.IS_RFS = "N";
            item.IS_READYTOPICK = "N";
            item.IS_READYTOPICK_AX = "N";
            return item;
        }

        public List<MPMWMS_UNIT> List()
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (unit.DATAAREA_ID == UserSession.DATAAREA_ID)
                                && (unit.SITE_ID == UserSession.SITE_ID_MAPPING)
                                select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_UNIT> List(String dataarea_id, String site_id, String year, String status)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (unit.DATAAREA_ID == dataarea_id)
                                && (unit.SITE_ID == site_id)
                                && (unit.UNIT_YEAR == year)
                                && (unit.STATUS == status)
                                select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_UNIT> List(String dataarea_id, String site_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (unit.DATAAREA_ID == dataarea_id)
                                && (unit.SITE_ID == site_id)
                                select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_UNIT> ListBySLSPG(String dataarea_id, String sl_id, String spg_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (unit.DATAAREA_ID == dataarea_id)
                                && (unit.SL_ID == sl_id)
                                && (unit.SPG_ID == spg_id)
                                select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_UNIT> ListBySPG(String dataarea_id, String site_id, String spg_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.DATAAREA_ID == dataarea_id)
                               && (unit.SITE_ID == site_id)
                               && (unit.SPG_ID == spg_id)
                               select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPMWMS_UNIT> ListByITO(String dataarea_id, String spg_id)
        {
            try
            {
//                select u.MACHINE_ID,u.*
//from MPMWMS_MUTATION_SITE l
//join MPMWMS_MUTATION_SITE_LINE l2 on l.MUTATION_ID = l2.MUTATION_ID
//join mpmwms_unit u on u.MACHINE_ID = l2.MACHINE_ID
//where l.TRANSFER_ID = 'M2Z/ITO/U/10/2020/00000018'
                var itemsObj = from header in Context.MPMWMS_MUTATION_SITEs
                               join line in Context.MPMWMS_MUTATION_SITE_LINEs 
                               on header.MUTATION_ID equals line.MUTATION_ID
                               join unit in Context.MPMWMS_UNITs
                               on line.MACHINE_ID equals unit.MACHINE_ID
                               where
                               (header.DATAAREA_ID == dataarea_id)
                               //&& (unit.SITE_ID == site_id)
                               && (header.TRANSFER_ID == spg_id)
                               select unit;
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_UNITs);
        }

        public MPMWMS_UNIT Item(String machine_id, String frame_id, String dataarea_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where (unit.FRAME_ID == frame_id) &&
                                (unit.MACHINE_ID == machine_id) &&
                                (unit.DATAAREA_ID == dataarea_id)
                                select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public MPMWMS_UNIT ItemByMachineOrFrameId(String machine_id, String dataarea_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where ((unit.FRAME_ID == machine_id) ||
                               (unit.MACHINE_ID == machine_id)) &&
                               (unit.DATAAREA_ID == dataarea_id)
                               select unit;
                //var itemsObj = from unit in Context.MPMWMS_UNITs
                //               where ((unit.QR_CODE == machine_id) ||
                //               (unit.FRAME_ID == machine_id) ||
                //               (unit.MACHINE_ID == machine_id)) &&
                //               (unit.DATAAREA_ID == dataarea_id)
                //               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anong 25-07-2019
        public MPMWMS_UNIT ItemByQR(String dataarea_id, String qrcode, String siteId)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.QR_CODE == qrcode) && //|| unit.MACHINE_ID == qrcode
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteId)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 27-11-2019
        public MPMWMS_UNIT ItemByNosin(String dataarea_id, String nosin, String siteId)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.MACHINE_ID == nosin) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteId)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        public MPMWMS_PICKING ItemByPicking(String picking)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_PICKINGs
                               where
                                (unit.PICKING_ID == picking)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        public CUSTTABLE ItemByKdDealer(String dealer_code)
        {
            try
            {
                var itemsObj = from unit in Context.CUSTTABLEs
                               where
                               (unit.ACCOUNTNUM == dealer_code)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //asri 2020-01-13
        public MPMSERVVIEWCHANNELH1 ItemByKdDealerH1(String dealer_code)
        {
            try
            {
                var itemsObj = from unit in Context.MPMSERVVIEWCHANNELH1s
                               where
                               (unit.ACCOUNTNUM == dealer_code)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        public MPMWMS_MUTATION_LOCATION ItemByjurnal(String machineid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_MUTATION_LOCATIONs
                               where(unit.MACHINE_ID == machineid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
                //var currentItem = itemsObj.FirstOrDefault().MACHINE_ID.Take(1).FirstOrDefault();
                //return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        public MPMWMS_MUTATION_LOCATION ItemByjurnal1(String machineid, String Mutation_ID)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_MUTATION_LOCATIONs
                               where (unit.MACHINE_ID == machineid) &&
                               (unit.MUTATION_LOCATION_ID == Mutation_ID)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public XVS_BOM_VIEW ItemByjurnalBundleUnitBom1(String type, String color, String site_id, String dataareaid, String maindealerid)
        {
            try
            {
                var itemsObj = from unit in Context.XVS_BOM_VIEWs
                               where (unit.ITEMID == type) &&
                               (unit.INVENTCOLORID == color) &&
                               (unit.INVENTSITEID == site_id) &&
                               (unit.XTSBOMFLAG == 0) &&
                               (unit.DATAAREAID == dataareaid) &&
                               (unit.XTSMAINDEALERCODE == maindealerid) &&
                               (unit.BOMLINE == 1)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public XVS_BOM_VIEW ItemByjurnalBundleUnitBom2(String journalid)
        {
            try
            {
                var itemsObj = from unit in Context.XVS_BOM_VIEWs
                               where (unit.JOURNALID == journalid) &&
                               (unit.BOMLINE == 0)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public MPMWMS_BOM ItemByjurnalBundleUnitBom(String type, String color)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_BOMs
                               where (unit.UNIT_TYPE == type) &&
                               (unit.COLOR_TYPE == color) 
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
                //var currentItem = itemsObj.FirstOrDefault().MACHINE_ID.Take(1).FirstOrDefault();
                //return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public XVS_BOR_VIEW ItemByjurnalBundleUnitBor1(String type, String color, String site_id, String dataareaid, String maindealerid)
        {
            try
            {
                var itemsObj = from unit in Context.XVS_BOR_VIEWs
                               where (unit.ITEMID == type) &&
                               (unit.INVENTCOLORID == color) &&
                               (unit.INVENTSITEID == site_id) &&
                               (unit.XTSBOMFLAG == 0) &&
                               (unit.DATAAREAID == dataareaid) &&
                               (unit.XTSMAINDEALERCODE == maindealerid) &&
                               (unit.BOMLINE == "1")
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public XVS_BOR_VIEW ItemByjurnalBundleUnitBor2(String journalid)
        {
            try
            {
                var itemsObj = from unit in Context.XVS_BOR_VIEWs
                               where (unit.JOURNALID == journalid) &&
                               (unit.BOMLINE == "0")
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public MPMWMS_BOR ItemByjurnalBundleUnitBor(String type, String color)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_BORs
                               where (unit.UNIT_TYPE == type) &&
                               (unit.COLOR_TYPE == color)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
                //var currentItem = itemsObj.FirstOrDefault().MACHINE_ID.Take(1).FirstOrDefault();
                //return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas 
        public MPMWMS_PICKING ItemByjurnalPicking(String pickingid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_PICKINGs
                               where (unit.PICKING_ID == pickingid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
                //var currentItem = itemsObj.FirstOrDefault().MACHINE_ID.Take(1).FirstOrDefault();
                //return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anong 06-09-2019
        public MPMWMS_UNIT ItemByQRWithLocation(String dataarea_id, String qrcode, String siteId, String warehouse_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.QR_CODE == qrcode) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteId) &&
                               (unit.WAREHOUSE_ID == warehouse_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
       
        
        //update anong 9/9/2019
        public MPMWMS_UNIT Item(String dataarea_id, String machine_or_frame_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (
                                    unit.MACHINE_ID == machine_or_frame_id ||
                                    unit.FRAME_ID == machine_or_frame_id
                                ) &&
                                (unit.DATAAREA_ID == dataarea_id)
                                select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas
        public MPMWMS_UNIT GetNosinFromQr(String dataarea_id, String qrcode)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.QR_CODE == qrcode) &&
                               (unit.DATAAREA_ID == dataarea_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //update ardian 6/11/2019
        public MPMWMS_UNIT ItemQR(String dataarea_id, String machine_or_frame_qr)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (
                                   unit.QR_CODE == machine_or_frame_qr
                               ) &&
                               (unit.DATAAREA_ID == dataarea_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemBySite(String dataarea_id, String site_id, String machine_or_frame_id)
        {
            try
            {
                Context.CommandTimeout = 500;

                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (
                                   //"MH1" + unit.FRAME_ID == machine_or_frame_id ||
                                   //unit.MACHINE_ID == machine_or_frame_id ||
                                   //"MLH" + unit.FRAME_ID == machine_or_frame_id ||
                                   //unit.FRAME_ID == machine_or_frame_id
                                   unit.MACHINE_ID == machine_or_frame_id
                               ) &&
                               (unit.DATAAREA_ID == dataarea_id)
                               && (unit.SITE_ID == site_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByLocation(String dataarea_id, String machine_or_frame_id, String site_id, String warehouse_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                                where
                                (
                                    "MH1" + unit.FRAME_ID == machine_or_frame_id ||
                                    "MLH" + unit.FRAME_ID == machine_or_frame_id ||
                                    unit.MACHINE_ID == machine_or_frame_id ||
                                    unit.FRAME_ID == machine_or_frame_id                              
                                ) &&
                                (unit.DATAAREA_ID == dataarea_id) &&
                                (unit.SITE_ID == site_id) &&
                                (unit.WAREHOUSE_ID == warehouse_id)
                                select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemBySPG(String dataarea_id, String site_id, String spg_id)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == site_id) &&
                               (unit.SPG_ID == spg_id)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        private MPMWMS_HISTORY_UNIT InsertHistory(MPMWMS_UNIT row,
                                                  String changes_type, String site_id_old = "", String warehouse_id_old = "", String location_id_old = "")
        {
            // insert history
            MPMWMS_HISTORY_UNIT history = HistoryUnitObject.Create(row);
            history.CHANGES_TYPE = changes_type;
            history.SITE_ID_OLD = site_id_old;
            history.WAREHOUSE_ID_OLD = warehouse_id_old;
            history.LOCATION_ID_OLD = location_id_old;
            HistoryUnitObject.Insert(history);

            return history;
        }

        public void Insert(MPMWMS_UNIT row, String changes_type = "U")
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_UNITs.InsertOnSubmit(row);

            // insert history
            InsertHistory(row, changes_type);
        }

        public void Update(MPMWMS_UNIT row, 
                           String changes_type = "U", String site_id_old = "", String warehouse_id_old = "", String location_id_old = "")
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
            Context.SubmitChanges();
            // insert history
            InsertHistory(row, changes_type, site_id_old, warehouse_id_old, location_id_old);
        }

        public void Delete(MPMWMS_UNIT row)
        {
            try
            {
                // if in history is only 1 field then can be delete
                if (HistoryUnitObject.Count(row.MACHINE_ID, row.FRAME_ID, row.DATAAREA_ID) > 1)
                {
                    throw new MPMException("Can't delete unit because it's already transaction.");
                }

                // delete history
                MPMWMS_HISTORY_UNIT historyItem = HistoryUnitObject.Item(row.MACHINE_ID, row.FRAME_ID, row.DATAAREA_ID);
                HistoryUnitObject.Delete(historyItem);

                // delete unit
                Context.MPMWMS_UNITs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }

            //Context.MPMWMS_UNITs.DeleteOnSubmit(row);
        }

        public MPMWMS_UNIT SuggestedPicking(String dataarea_id, String site_id, String color, String type, String year)
        {
            try
            {
                var itemsObj =
                    from unit in Context.MPMWMS_UNITs
                    join location in Context.MPMWMS_WAREHOUSE_LOCATIONs on
                        new { unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID } equals
                        new { location.SITE_ID, location.WAREHOUSE_ID, location.LOCATION_ID } into join_unit_location
                    from result_join_unit_location in join_unit_location
                    where
                        (unit.DATAAREA_ID == dataarea_id) &&
                        (unit.SITE_ID == site_id) &&
                        (unit.COLOR_TYPE == color) &&
                        (unit.UNIT_TYPE == type) &&
                        (unit.IS_RFS == "Y") &&
                        (unit.IS_READYTOPICK == "N") &&
                        (unit.STATUS == "RFS") &&
                        (unit.UNIT_YEAR != "") &&
                        (Convert.ToInt32(unit.UNIT_YEAR) <= Convert.ToInt32(year)) &&
                        (
                            (result_join_unit_location.LOCATION_TYPE == "R") ||
                            (result_join_unit_location.LOCATION_TYPE == "S")
                        )
                    orderby
                        unit.UNIT_YEAR ascending, unit.SPG_DATE ascending
                    select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public List<MPMWMS_UNIT> ListClockingUnit(string MACHINE_ID)
        {
            var list = from a in Context.MPMWMS_UNITs
                       where a.STATUS != "RFS"  && a.MACHINE_ID == MACHINE_ID && a.SITE_ID ==  UserSession.SITE_ID_MAPPING
                       select a;
            //var result = list.Where(x => x.STATUS != "S");

            return list.ToList();
        }


        public MPMWMS_UNIT ItemByMachine(String machine_id, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.MACHINE_ID == machine_id) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid) 
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByMachineUnitError(String qrcode, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.QR_CODE == qrcode) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByNosinUnmatch1(String nosin, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.MACHINE_ID == nosin) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByQrUnmatch2(String qr, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.QR_CODE == qr) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByFrame(String frame_id, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.FRAME_ID == frame_id) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT  ItemByCekNosin(String nsoin, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.MACHINE_ID == nsoin) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_UNIT ItemByCekNoka(String noka, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.FRAME_ID == noka) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //tambahan anas
        public MPMWMS_UNIT ItemByCekQr(String qr,/* String nosin,*/ String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from unit in Context.MPMWMS_UNITs
                               where (unit.QR_CODE == qr) &&
                               //(unit.MACHINE_ID == nosin) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        #region cek shipping by nosin
        public MPMWMS_SHIPPING_LIST_LINE ItemCekShipListByNosin(String nosin, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from ssl in Context.MPMWMS_SHIPPING_LIST_LINEs
                               join unit in Context.MPMWMS_UNITs on ssl.MACHINE_ID equals unit.MACHINE_ID
                               where (unit.MACHINE_ID == nosin) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid) &&
                               (unit.QR_CODE != null)
                               select ssl;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_SHIPPING_LIST_LINE ItemCekShipListByNoka(String noka, String dataarea_id, String siteid)
        {
            try
            {
                var itemsObj = from ssl in Context.MPMWMS_SHIPPING_LIST_LINEs
                               join unit in Context.MPMWMS_UNITs on ssl.MACHINE_ID equals unit.MACHINE_ID
                               where (unit.FRAME_ID == noka) &&
                               (unit.DATAAREA_ID == dataarea_id) &&
                               (unit.SITE_ID == siteid) &&
                               (unit.QR_CODE != null)
                               select ssl;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        #endregion
    }
}
