﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Unit
{
    public interface IUnitObject
    {
        MPMWMS_UNIT Create();
        List<MPMWMS_UNIT> List();
        List<MPMWMS_UNIT> List(String dataarea_id, String site_id);
        List<MPMWMS_UNIT> List(String dataarea_id, String site_id, String year, String status);
        List<MPMWMS_UNIT> ListBySLSPG(String dataarea_id, String sl_id, String spg_id);
        void Refresh();
        MPMWMS_UNIT Item(String machine_id, String frame_id, String dataarea_id);
        MPMWMS_UNIT Item(String dataarea_id, String machine_or_frame_id);
        void Insert(MPMWMS_UNIT row, String changes_type = "U");
        void Delete(MPMWMS_UNIT row);
    }
}
