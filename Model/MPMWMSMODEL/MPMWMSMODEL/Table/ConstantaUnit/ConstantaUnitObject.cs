﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.EADUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ConstantaUnit
{
    public class ConstantaUnitObject : MPMDbObject<WmsUnitDataContext>
    {
        public ConstantaUnitObject()
            : base()
        {
        }


        public List<MPMWMS_EAD_CONSTANTA_UNIT_TYPE> ItemAll()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_CONSTANTA_UNIT_TYPE Item(String _type)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs
                               where a.TYPE == _type
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_CONSTANTA_UNIT_TYPE item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EAD_CONSTANTA_UNIT_TYPE item)
        {
            try
            {
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_CONSTANTA_UNIT_TYPE item)
        {
            try
            {
                Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //public List<ConstantaTypeRecord> ViewAll()
        //{
        //    try
        //    {
        //        var itemsObj = from a in Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs
        //                       join invent in Context.INVENTTABLEs on new { ITEMID = a.TYPE }
        //                                                        equals new { ITEMID = invent.ITEMID }
        //                       join product in Context.ECORESPRODUCTs on new { PRODUCT = invent.PRODUCT }
        //                                                        equals new { PRODUCT = product.RECID }
        //                        select new ConstantaTypeRecord
        //                            (   a.TYPE,
        //                                product.SEARCHNAME,
        //                                product.XTSINVTYPEDESCR,
        //                                a.VALUE
        //                            );


        //        return itemsObj.ToList();
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}
        public List<ConstantaTypeRecord> ViewAll()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_CONSTANTA_UNIT_TYPEs
                               join b in Context.MPMSERVVIEWDAFTARUNITWARNAs on a.TYPE equals b.ITEMID
                               select new ConstantaTypeRecord
                                   (a.TYPE,
                                       b.SEARCHNAME,
                                       b.ITEMNAME,
                                       a.VALUE
                                   );


                return itemsObj.Distinct().ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

    }
}
