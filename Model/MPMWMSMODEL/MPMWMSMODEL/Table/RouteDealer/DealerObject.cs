﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.RouteDealer;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteDealer
{
    public class DealerObject : MPMDbObject<WmsUnitDataContext>
    {
        public DealerObject()
            : base()
        {
        }

        public Boolean Update(String _accountnum, String _zone, Decimal _ongkos, Decimal _lat, Decimal _long, Decimal _distance)
        {
            Boolean flagSave = false;
            System.Data.Common.DbTransaction trans = null;

            if (Context.Connection.State != System.Data.ConnectionState.Closed)
            {
                Context.Connection.Close();
            }

            Context.Connection.Open();

            trans = Context.Connection.BeginTransaction();
            Context.Transaction = trans;

            try
            {

                var itemsObj = from a in Context.CUSTTABLEs
                               where a.ACCOUNTNUM == _accountnum
                               select a;

                foreach (CUSTTABLE x in itemsObj)
                {
                    if (x.ACCOUNTNUM == _accountnum)
                    {
                        x.MPMZONE = _zone;
                        x.MPMONGKOSANGKUT = _ongkos;
                        x.MPMDISTANCE = _distance;
                        x.MPMLAT = _lat;
                        x.MPMLONG = _long;
                    }
                }

                Context.SubmitChanges();

                trans.Commit();

                flagSave = true;

            }
            catch (MPMException)
            {
                trans.Rollback();
                flagSave = false;
            }

            Context.Connection.Close();
            return flagSave;
        }

        public EDIT_DEALER getDealerInfo(String dealercode)
        {

            return (from a in Context.CUSTTABLEs
                    join b in Context.DIRPARTYPOSTALADDRESSVIEWs
                        on a.PARTY equals b.PARTY
                    join c in Context.MPMWMS_TO_ZONEs
                        on a.MPMZONE equals c.ZONECODE into x_ac
                    from a_c in x_ac.DefaultIfEmpty()
                    where a.ACCOUNTNUM == dealercode
                        && b.ISPRIMARY == 1
                    select new EDIT_DEALER
                    {
                        DEALERID = a.ACCOUNTNUM,
                        DEALERNAME = b.LOCATIONNAME,
                        ZONE = a.MPMZONE,
                        ONGKOSANGKUT = a.MPMONGKOSANGKUT,
                        LAT = a.MPMLAT,
                        LONG = a.MPMLONG
                    }).FirstOrDefault();

        }

        public class EDIT_DEALER
        {
            public String DEALERID { get; set; }
            public String DEALERNAME { get; set; }
            public String ZONE { get; set; }
            public Decimal ONGKOSANGKUT { get; set; }
            public Decimal LONG { get; set; }
            public Decimal LAT { get; set; }
        }

    }
}
