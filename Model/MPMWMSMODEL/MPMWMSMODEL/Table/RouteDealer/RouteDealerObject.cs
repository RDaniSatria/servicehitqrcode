﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.RouteDealer;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.RouteDealer
{
    public class RouteDealerObject : MPMDbObject<WmsUnitDataContext>
    {
        public RouteDealerObject() 
            : base() 
        { 
        }

        public MPMWMS_MAP_ROUTE_DEALER Item(String _dataArea, String _mainDealer, String _siteId, String _accountNum, String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_ROUTE_DEALERs
                               where (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                                && (a.ACCOUNTNUM == _accountNum)
                                && (a.ROUTEID == _routeId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_MAP_ROUTE_DEALER> ListAll(String _dataArea, String _mainDealer, String _siteId, String _accountNum)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_ROUTE_DEALERs
                               where (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                                && (a.ACCOUNTNUM == _accountNum)
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<RouteZoneRecord> ListRouteZone(String _dataArea, String _mainDealer, String _siteId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTEs
                               where (a.DATAAREAID == _dataArea)
                                    && (a.MAINDEALERID == _mainDealer)
                                    && (a.SITEID == _siteId)
                               //join b in Context.MPMWMS_TO_ZONEs on new { ZONE = a.ZONECODE }
                               //                                       equals new { ZONE = b.ZONECODE }
                               select new RouteZoneRecord
                               (
                                    a.ROUTEID,
                                    a.DESCRIPTION
                               );

                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DealerZoneRecord> ListDealerZone()
        {
            try
            {
                var itemsObj = from a in Context.CUSTTABLEs 
                                    where a.XTSCATEGORYCODE == "P"
                                        && SqlMethods.Like(a.ACCOUNTNUM, "%P")
                               join c in Context.DIRPARTYPOSTALADDRESSVIEWs on new { PARTY = a.PARTY }
                                                       equals new { PARTY = c.PARTY }
                                    where c.ISPRIMARY == 1
                                        && c.VALIDTO >= DateTime.Today.Date
                               join d in Context.MPMWMS_TO_ZONEs on a.MPMZONE equals d.ZONECODE into cd
                               from x_cd in cd.DefaultIfEmpty()
                               select new DealerZoneRecord
                               (
                                    a.ACCOUNTNUM,
                                    c.LOCATIONNAME,
                                    c.CITY,
                                    a.MPMZONE,
                                    x_cd.ZONEDESCRIPTION,
                                    a.MPMONGKOSANGKUT,
                                    a.MPMDISTANCE
                               );

                return itemsObj.Distinct().ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MAP_ROUTE_DEALER item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_MAP_ROUTE_DEALER item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;

                Context.MPMWMS_MAP_ROUTE_DEALERs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_MAP_ROUTE_DEALER item)
        {
            try
            {
                Context.MPMWMS_MAP_ROUTE_DEALERs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_TO_ZONE> getListZone()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_TO_ZONEs
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<DealerRouteRecord> getListDealerRoute(String _dataArea, String _mainDealer, String _siteId, String _accountNum)
        {
            try
            {
                var itemsObj =  from a in Context.MPMWMS_ROUTEs
                                where a.DATAAREAID == _dataArea
                                   && a.MAINDEALERID == _mainDealer
                                   && a.SITEID == _siteId
                                join b in Context.MPMWMS_MAP_ROUTE_DEALERs on new { ROUTEID = a.ROUTEID, SITE = a.SITEID, MAIN = a.MAINDEALERID, DATAAREA = a.DATAAREAID }
                                                                   equals new { ROUTEID = b.ROUTEID, SITE = b.SITEID, MAIN = b.MAINDEALERID, DATAAREA = b.DATAAREAID }
                                where b.ACCOUNTNUM == _accountNum
                                select new DealerRouteRecord
                                (
                                    b.ACCOUNTNUM,
                                    a.ROUTEID,
                                    a.DESCRIPTION
                                );

                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_ROUTE> getListRoute(String _dataArea, String _mainDealer, String _siteId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTEs
                               where a.DATAAREAID == _dataArea
                               && a.MAINDEALERID == _mainDealer
                               && a.SITEID == _siteId
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
