﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.HistClocking
{
    public interface IHistClockingObject
    {
        List<MPMWMSHISTCLOCKING> List();
        MPMWMSHISTCLOCKING Item(string NoMesin);
        void Insert(MPMWMSHISTCLOCKING item);
        MPMWMSHISTCLOCKING Item(string NoMesin, string userid, int?[] status);
    }
}
