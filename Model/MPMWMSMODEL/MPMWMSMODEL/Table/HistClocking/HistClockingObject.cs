﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.HistClocking
{
    public class HistClockingObject : MPMDbObject<WmsUnitDataContext>, IHistClockingObject
    {
        public HistClockingObject() : base() { }

        public List<MPMWMSHISTCLOCKING> List()
        {
            var list = from a in Context.MPMWMSHISTCLOCKINGs
                       select a;
            return list.ToList();
        }
        public MPMWMSHISTCLOCKING Item(string NoMesin)
        {
            var item = from a in Context.MPMWMSHISTCLOCKINGs
                       where a.NOMESIN == NoMesin
                       select a;
            return item.Take(1).FirstOrDefault();
        }
        public MPMWMSHISTCLOCKING Item(string NoMesin,int? status)
        {
            var item = from a in Context.MPMWMSHISTCLOCKINGs
                       where a.NOMESIN == NoMesin
                       && a.STATUSKERJA == status
                       && a.STATUSQC == 0
                       && a.TGLSTATUSQC == null
                       && a.USERID == UserSession.USER_ID
                       select a;
            return item.Take(1).FirstOrDefault();
        }
        public MPMWMSHISTCLOCKING ItemNotePassed(string NoMesin, int? status)
        {
            var item = from a in Context.MPMWMSHISTCLOCKINGs
                       where a.NOMESIN == NoMesin
                       && a.STATUSKERJA == status
                       && a.STATUSQC == 0
                       && a.TGLSTATUSQC != null
                       && a.USERID == UserSession.USER_ID
                       select a;
            return item.Take(1).FirstOrDefault();
        }
        public MPMWMSHISTCLOCKING Item(string NoMesin,string userid, int?[] status)
        {
            var item = from a in Context.MPMWMSHISTCLOCKINGs
                       where a.NOMESIN == NoMesin
                       && a.USERID != userid
                       && status.Contains(a.STATUSKERJA)
                       select a;
            return item.Take(1).FirstOrDefault();
        }
        public void Insert(MPMWMSHISTCLOCKING item)
        {
            try
            {
                item.CREATEBY = UserSession.USER_ID;
                item.CREATEDATE = DateTime.Now;
                Context.MPMWMSHISTCLOCKINGs.InsertOnSubmit(item);
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void Update(MPMWMSHISTCLOCKING item)
        {
            try
            {
                item.MODIFBY = UserSession.USER_ID;
                item.MODIFDATE = DateTime.Now;
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
    }
}
