﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.CustInvoiceTrans
{
    public class CustInvoiceTransObject : MPMDbObject<WmsUnitDataContext>, ICustInvoiceTransObject
    {
        public CustInvoiceTransObject()
            : base()
        {
        }

        public List<CUSTINVOICETRAN> List()
        {
            return Context.CUSTINVOICETRANs.ToList();
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).CUSTINVOICETRANs);
        }

        public int SumQuantityDO(String do_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<int> itemsObj =
                    from invoice in Context.MPMVIEW_INVOICEs
                    where
                        invoice.DATAAREA_ID == dataarea_id
                        && invoice.DO_ID == do_id
                        && invoice.MAINDEALER_ID == maindealer_id
                        && invoice.SITE_ID == site_id
                    let k = new { invoice.SALES_ID, invoice.DATAAREA_ID, invoice.MAINDEALER_ID, invoice.SITE_ID }
                    group invoice by k into t_group
                    select Convert.ToInt32(t_group.Sum(invoice => invoice.QTY_DO + invoice.QTY_RETUR));
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public int SumQuantityReturn(String inventtransidreturn, String dataarea_id)
        {
            try
            {
                IQueryable<int> itemsObj = from salesline in Context.SALESLINEs
                                            where
                                                (salesline.INVENTTRANSIDRETURN == inventtransidreturn) &&
                                                (salesline.RETURNSTATUS == 4 || salesline.RETURNSTATUS == 5) &&
                                                (salesline.DATAAREAID == dataarea_id)
                                            let k = new { item = salesline.SALESID, dataarea = salesline.DATAAREAID }
                                            group salesline by k into t_group
                                            select Convert.ToInt32(t_group.Sum(salesline => salesline.SALESQTY));
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public CUSTINVOICETRAN SearchSalesLine(String dataarea_id, String do_id, String item_id)
        {
            IQueryable<CUSTINVOICETRAN> search = from a in Context.CUSTINVOICETRANs
                                           where
                                           a.DATAAREAID == dataarea_id &&
                                           a.INVOICEID == do_id &&
                                           a.ITEMID == item_id
                                           select a;

            return search.Take(1).FirstOrDefault();

        }

        public CUSTINVOICETRAN Item(String do_id, String dataarea_id, String item_id, String color)
        {
            IQueryable<CUSTINVOICETRAN> search =
                from a in Context.CUSTINVOICETRANs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.INVOICEID == do_id &&
                a.ITEMID == item_id &&
                result.INVENTCOLORID == color
                select a;

            return search.Take(1).FirstOrDefault();

        }

        public List<CUSTINVOICETRAN> List(String dataarea_id, String do_id)
        {
            IQueryable<CUSTINVOICETRAN> search = from a in Context.CUSTINVOICETRANs
                                           where
                                           (a.DATAAREAID == dataarea_id) &&
                                           (a.INVOICEID == do_id)
                                           select a;
            return search.ToList();
        }
    }
}
