﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.CustInvoiceTrans
{
    public interface ICustInvoiceTransObject
    {
        List<CUSTINVOICETRAN> List();
        void Refresh();
        int SumQuantityDO(String do_id, String dataarea_id, String maindealer_id, String site_id);
        int SumQuantityReturn(String inventtransidreturn, String dataarea_id);
        CUSTINVOICETRAN SearchSalesLine(String dataarea_id, String do_id, String item_id);
        CUSTINVOICETRAN Item(String do_id, String dataarea_id, String item_id, String color);
        List<CUSTINVOICETRAN> List(String dataarea_id, String do_id);
    }
}
