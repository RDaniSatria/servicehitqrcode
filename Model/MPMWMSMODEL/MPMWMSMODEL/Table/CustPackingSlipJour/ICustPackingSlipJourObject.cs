﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CustPackingSlipJour
{
    public interface ICustPackingSlipJourObject
    {
        CUSTPACKINGSLIPJOUR Item(String packing_id, String dataarea_id);
        CUSTPACKINGSLIPJOUR ItemBySalesIdAndParam(String sales_id, String param_id);
    }
}
