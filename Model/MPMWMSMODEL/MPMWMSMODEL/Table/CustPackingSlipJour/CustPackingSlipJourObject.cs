﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.CustPackingSlipTrans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CustPackingSlipJour
{
    public class CustPackingSlipJourObject: MPMDbObject<WmsUnitDataContext>, ICustPackingSlipJourObject
    {
        private CustPackingSlipTransObject _CustPackingSlipTransObject = new CustPackingSlipTransObject();

        public CustPackingSlipJourObject()
            : base()
        {
        }

        public CustPackingSlipTransObject CustPackingSlipTransObject
        {
            get { return _CustPackingSlipTransObject; }
        }

        public List<CUSTPACKINGSLIPJOUR> ListPicked(String trolley_id)
        {
            IQueryable<CUSTPACKINGSLIPJOUR> search =
                from tran in Context.CUSTPACKINGSLIPJOURs
                where
                    tran.DATAAREAID == UserSession.DATAAREA_ID
                    && tran.XTSTROLLEYNBR == trolley_id
                    && tran.XTSISTRANSFERED == 1
                select tran;

            return search.ToList();
        }

        public CUSTPACKINGSLIPJOUR Item(String packing_id, String dataarea_id)
        {
            IQueryable<CUSTPACKINGSLIPJOUR> search =
                from packing in Context.CUSTPACKINGSLIPJOURs
                where (packing.DATAAREAID == dataarea_id)
                && (packing.PACKINGSLIPID == packing_id)
                select packing;

            return search.Take(1).FirstOrDefault();
        }

        public CUSTPACKINGSLIPJOUR ItemBySID(String sid, String dataarea_id)
        {
            IQueryable<CUSTPACKINGSLIPJOUR> search =
                from packing in Context.CUSTPACKINGSLIPJOURs
                where (packing.DATAAREAID == dataarea_id)
                && (packing.PARMID == sid)
                orderby packing.RECID descending
                select packing;

            return search.Take(1).FirstOrDefault();
        }

        public CUSTPACKINGSLIPJOUR ItemBySalesIdAndParam(String sales_id, String param_id)
        {
            IQueryable<CUSTPACKINGSLIPJOUR> search =
                from packing in Context.CUSTPACKINGSLIPJOURs
                where (packing.SALESID == sales_id)
                && (packing.PARMID == param_id)
                select packing;

            return search.Take(1).FirstOrDefault();
        }

        public void updateRuteId(String _packingSlipId)
        {
            String QueryitemsObj =
                "UPDATE P SET MPMROUTECODE = ISNULL(R.ROUTEID, '0')                      " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) P                                     " +
                "JOIN MPMWMS_MAP_ROUTE_DEALER(NOLOCK) R ON P.ORDERACCOUNT = R.ACCOUNTNUM " +
                "WHERE P.PACKINGSLIPID = '" +_packingSlipId + "'                         ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public void updateLockedUser(String _soId, String _pic, String _dataArea)
        {
            String QueryitemsObj =
                "UPDATE P SET XTSLOCKEDUSERID = '" + _pic + "'  " +
                "FROM CUSTPACKINGSLIPJOUR (NOLOCK) P            " +
                "WHERE P.SALESID = '" + _soId + "'              " +
                "AND P.XTSLOCKEDUSERID = ''                     " +
                "AND P.DATAAREAID = '" + _dataArea + "'         ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public void updateLockedUserChecker(String _pic, String _trolley, String _siteId, String _dataArea, String _workHour, String _restHour)
        {
            String QueryitemsObj =
                " UPDATE P SET XTSLOCKEDUSERID = '" + _pic + "'                                                                                " +
                " FROM CUSTPACKINGSLIPJOUR (NOLOCK) P                                                                                          " +
                " LEFT JOIN CUSTINVOICEJOUR(NOLOCK) I ON I.SALESID = P.SALESID AND I.DATAAREAID = P.DATAAREAID AND I.PARTITION = P.PARTITION   " +
                " JOIN MPMWMS_MAP_SO_PICAREA(NOLOCK) A ON P.SALESID = A.SOID AND P.DATAAREAID = A.DATAAREAID AND P.INVOICEACCOUNT = A.DEALERID " +
                " WHERE P.XTSTROLLEYNBR = '" + _trolley + "'                                                                                   " +
                " AND P.DATAAREAID = '" + _dataArea + "'                                                                                       " +
                " AND I.INVOICEID IS NULL                                                                                                      " +
                "                                                                                                                              " +
                " UPDATE H SET PICCHECKER = '" + _pic + "', STARTPACKING = GETDATE(),                                                          " +
                " WORKHOURPACKING = '" + _workHour + "', RESTHOURPACKING = '" + _restHour + "'                                                 " +
                " FROM MPMWMS_MAP_PIC_HISTORY(NOLOCK) H                                                                                        " +
                " JOIN CUSTPACKINGSLIPJOUR(NOLOCK) P ON H.SOID = P.SALESID                                                                     " +
                " LEFT JOIN CUSTINVOICEJOUR(NOLOCK) I ON P.SALESID = I.SALESID AND P.DATAAREAID = I.DATAAREAID AND P.PARTITION = I.PARTITION   " +
                " WHERE H.SITEID = '" + _siteId + "'                                                                                           " +
                " AND H.DATAAREAID = '" + _dataArea + "'                                                                                       " +
                " AND P.XTSTROLLEYNBR = '" + _trolley + "'                                                                                     " +
                " AND I.INVOICEID IS NULL                                                                                                      ";

            Context.ExecuteQuery<String>(QueryitemsObj).ToList();
        }

        public List<CUSTPACKINGSLIPJOUR> ListByTrolley(String trolley_id, String dataArea)
        {
            IQueryable<CUSTPACKINGSLIPJOUR> search =
                from tran in Context.CUSTPACKINGSLIPJOURs
                where
                    tran.DATAAREAID == dataArea
                    && tran.XTSTROLLEYNBR == trolley_id
                select tran;

            return search.ToList();
        }
    }
}
