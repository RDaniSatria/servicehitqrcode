﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DataArea
{
    public interface IDataAreaObject
    {
        DATAAREA Item(String dataarea_id);
        long Partition(String dataarea_id);
    }
}
