﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DataArea
{
    public class DataAreaObject : MPMDbObject<WmsMPMITModuleDataContext>, IDataAreaObject
    {
        public DataAreaObject()
            : base()
        {
        }

        public DATAAREA Item(String dataarea_id)
        {
            IQueryable<DATAAREA> search = from dataarea in Context.DATAAREAs
                                          where (dataarea.ID == dataarea_id)
                                          select dataarea;
            return search.FirstOrDefault();
        }

        public long Partition(String dataarea_id)
        {
            return Item(dataarea_id).PARTITION;
        }
    }
}
