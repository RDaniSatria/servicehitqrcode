﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.WmsOrderTrans;

namespace MPMWMSMODEL.Table.MapPicking
{
    public class MapPickingObject: MPMDbObject<WmsUnitDataContext>
    {
        public MapPickingObject(): base()
        {
        }

        public void Update(ref MPMWMS_MAP_PICKING row)
        {
            row.MODIFBY = UserSession.NPK;
            row.MODIFDATE = DateTime.Now;
        }

        private static Func<WmsUnitDataContext, string, string, IQueryable<MPMWMS_MAP_PICKING>>
           MapPickingQuery =
               CompiledQuery.Compile((WmsUnitDataContext db, string maindealer_id, string site_id)
                   =>
               from a in db.MPMWMS_MAP_PICKINGs
               where 
                    a.MAINDEALER_ID == maindealer_id
                    && a.SITE_ID == site_id
               select a
           );

        public List<MPMWMS_MAP_PICKING> List()
        {
            try
            {
                var result = MapPickingQuery(Context, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_MAP_PICKING Item(string pickingid)
        {
            try
            {
                return List().Where(x => x.JOURNALID == pickingid).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public MPMWMS_MAP_PICKING ItemEmpty()
        {
            try
            {
                return List().Where(x => x.JOURNALID == null || x.JOURNALID == "").Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        private static Func<WmsUnitDataContext, string, string, IQueryable<V_MPM_LIST_FOR_TRANSPORTER_PART>>
           ListPartPickingDoneQuery =
               CompiledQuery.Compile((WmsUnitDataContext db, string pic, string trolley_id)
                   =>
               from a in db.V_MPM_LIST_FOR_TRANSPORTER_PARTs
               where
                    a.PICTRANSPORTER == pic
                    && a.XTSTROLLEYNBR == trolley_id
               select a
           );

        public List<V_MPM_LIST_FOR_TRANSPORTER_PART> ListTransporterPart(String pic, String trolley_id)
        {
            IQueryable<V_MPM_LIST_FOR_TRANSPORTER_PART> search =
                ListPartPickingDoneQuery(Context, pic, trolley_id);
            return search.ToList();
        }
    }
}
