﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.MapPicking
{
    public class MapPicHistoryObject: MPMDbObject<WmsUnitDataContext>
    {
        public MapPicHistoryObject()
            : base()
        {
        }

        public MPMWMS_MAP_PIC_HISTORY Create()
        {
            return new MPMWMS_MAP_PIC_HISTORY();
        }

        public void Insert(ref MPMWMS_MAP_PIC_HISTORY row)
        {
            row.CREATEBY = UserSession.NPK;
            row.CREATEDATE = DateTime.Now;
        }
    }
}
