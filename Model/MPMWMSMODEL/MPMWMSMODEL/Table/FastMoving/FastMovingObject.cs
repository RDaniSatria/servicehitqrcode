﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.FastMoving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.FastMoving
{
    public class FastMovingObject : MPMDbObject<WmsUnitDataContext>
    {
        public FastMovingObject()
            : base()
        {
        }

        public List<FastMovingRecord> ViewAll()
        {
            try
            {
                //var itemsObj = from a in Context.MPMWMS_EAD_FASTMOVINGs
                //               join invent in Context.INVENTTABLEs on new { ITEMID = a.TYPE }
                //                                                equals new { ITEMID = invent.ITEMID }
                //               join product in Context.ECORESPRODUCTs on new { PRODUCT = invent.PRODUCT }
                //                                                equals new { PRODUCT = product.RECID }
                //               join color in Context.ECORESCOLORs on new { COLOR = a.COLOR }
                //                                            equals new { COLOR = color.NAME }
                //               join dimension in Context.ECORESPRODUCTMASTERDIMENSIONVALUEs on new { NAME = (long)color.RECID }
                //                                            equals new { NAME = (long)dimension.COLOR }
                //               select new FastMovingRecord
                //                    (   a.TYPE, 
                //                        product.SEARCHNAME, 
                //                        a.COLOR,
                //                        dimension.DESCRIPTION
                //                    );
                var itemsObj = from a in Context.MPMWMS_EAD_FASTMOVINGs
                               join b in Context.MPMSERVVIEWDAFTARUNITWARNAs on new { a.TYPE, a.COLOR }
                               equals new { TYPE = b.ITEMID, COLOR = b.ITEMCOLORCODE }
                               select new FastMovingRecord
                                    (a.TYPE,
                                        b.SEARCHNAME,
                                        a.COLOR,
                                        b.ITEMCOLOR
                                    );

                return itemsObj.Distinct().ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_FASTMOVING> ItemAll()
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_FASTMOVINGs
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_EAD_FASTMOVING Item(String _type, String _color)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_EAD_FASTMOVINGs
                               where a.TYPE == _type
                                    && a.COLOR == _color
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_FASTMOVING item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EAD_FASTMOVING item)
        {
            try
            {
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;

                Context.MPMWMS_EAD_FASTMOVINGs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_FASTMOVING item)
        {
            try
            {
                Context.MPMWMS_EAD_FASTMOVINGs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

    }
}
