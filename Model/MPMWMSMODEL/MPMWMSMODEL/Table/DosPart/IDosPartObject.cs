﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DosPart
{
    public interface IDosPartObject
    {
        MPMWMS_DOS_PART Create();
        void Insert(MPMWMS_DOS_PART row);
    }
}
