﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Exception;

namespace MPMWMSMODEL.Table.DosPart
{
    public class DosPartObject : MPMDbObject<WmsUnitDataContext>, IDosPartObject
    {
        public DosPartObject()
            : base()
        {
        }

        public MPMWMS_DOS_PART Create()
        {
            MPMWMS_DOS_PART item = new MPMWMS_DOS_PART();
            item.STATUS = "O";
            return item;
        }

        public void Insert(MPMWMS_DOS_PART row)
        {
            row.CREATE_BY = "";
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_DOS_PARTs.InsertOnSubmit(row);
        }

        private IQueryable<MPMWMS_DOS_PART> RunQuery(String id, String dataarea_id)
        {
            return from a in Context.MPMWMS_DOS_PARTs
                   where (a.DATAAREA_ID == dataarea_id) &&
                   (a.DOS_ID == id)
                   select a;
        }

        public MPMWMS_DOS_PART Item(String id, String dataarea_id)
        {
            try
            {
                IQueryable<MPMWMS_DOS_PART> search = RunQuery(id, dataarea_id);
                var currentItem = search.FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
