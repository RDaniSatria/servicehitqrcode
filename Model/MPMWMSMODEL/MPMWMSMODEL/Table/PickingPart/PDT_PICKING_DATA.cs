﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PickingPart
{
    [Serializable]
    public class PDT_PICKING_DATA
    {
        public String PICKING_ID;
        public String DATAAREA_ID;

        public PDT_PICKING_DATA()
        {
        }

        public PDT_PICKING_DATA(String picking_id, String dataarea_id)
        {
            PICKING_ID = picking_id;
            DATAAREA_ID = dataarea_id;
        }
    }
}
