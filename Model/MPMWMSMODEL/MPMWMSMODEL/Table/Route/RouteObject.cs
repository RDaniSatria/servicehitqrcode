﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Route
{
    public class RouteObject : MPMDbObject<WmsUnitDataContext>
    {
        public RouteObject()
            : base()
        {
        }

        public void Update(MPMWMS_ROUTE item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_ROUTE item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMS_ROUTEs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_ROUTE item)
        {
            try
            {
                Context.MPMWMS_ROUTEs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_ROUTE> ItemAll(String _dataArea, String _mainDealer, String _siteId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTEs
                               where (a.DATAAREAID == _dataArea)
                                 && (a.MAINDEALERID == _mainDealer)
                                 && (a.SITEID == _siteId)
                               select a;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_ROUTE Item(String _dataArea, String _mainDealer, String _siteId, String _routeId)
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_ROUTEs
                               where (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                                && (a.ROUTEID == _routeId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

    }
}
