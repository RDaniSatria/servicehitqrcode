﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.Bom
{
    public interface IBomObject
    {
        MPMWMS_BOM Create();
        void Insert(MPMWMS_BOM item);
    }
}
