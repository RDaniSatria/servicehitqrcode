﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.Bom
{
    public class BomObject: MPMDbObject<WmsUnitDataContext>, IBomObject
    {
        public BomObject()
            : base()
        {
        }

        public MPMWMS_BOM Create()
        {
            MPMWMS_BOM row = new MPMWMS_BOM();
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public void Insert(MPMWMS_BOM item)
        {
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_BOMs.InsertOnSubmit(item);
        }
    }
}
