﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.SpgIncludeBomDetail
{
    public class MPMWMS_SPG_INCLUDE_BOM_TRANS
    {
        public DateTime BOMDATE { get; set; }
        public String BOMID { get; set; }
        public String UNITTYPE1 { get; set; }
        public String UNITTYPE2 { get; set; }
        public String ITEMPART { get; set; }
        public int STATUS { get; set; }
        public DateTime SPGDATE { get; set; }
        public String SPGID { get; set; }
        public int QTY { get; set; }
        public String UNITCOLOR { get; set; }
        public String INVENTDIMID_UNIT { get; set; }
        public String SITEID_UNIT { get; set; }
        public String LOCATIONID_UNIT { get; set; }
        public String INVENTDIMID_PART { get; set; }
        public String SITEID_PART { get; set; }
        public String LOCATIONID_PART { get; set; }
        public long RECID { get; set; }
       
        public MPMWMS_SPG_INCLUDE_BOM_TRANS()
        {
        }
    }


    public class SpgIncludeBomDetailObject : MPMDbObject<WmsUnitDataContext>, ISpgIncludeBomDetailObject
    {


        public IQueryable<MPMWMS_SPG_INCLUDE_BOM_TRANS> List(String site_id)
        {
            IQueryable<MPMWMS_SPG_INCLUDE_BOM_TRANS> result =
                from a in Context.MPMWMS_SPG_INCLUDE_BOMs
                join b in Context.MPMWMS_SPG_INCLUDE_BOM_DETAILs
                on new { SPGID = a.SPGID } equals new { SPGID = b.SPGID }
                where //a.STATUS == 1 &&
                      b.STATUS == 1 &&
                      b.SITEID_UNIT ==  site_id
                orderby a.SPGID ascending
                select new MPMWMS_SPG_INCLUDE_BOM_TRANS
                {
                    BOMDATE = b.BOMDATE,
                    BOMID = b.BOMID,
                    UNITTYPE1 = b.UNITTYPE1,
                    UNITTYPE2 = b.UNITTYPE2,
                    //ITEMPART = b.ITEMPART, //comment steven
                    STATUS = b.STATUS,
                    SPGDATE = a.SPGDATE,
                    SPGID = a.SPGID,
                    QTY = (int)b.QTY,
                    UNITCOLOR = b.UNITCOLOR,
                    INVENTDIMID_UNIT = b.INVENTDIMID_UNIT,
                    SITEID_UNIT = b.SITEID_UNIT,
                    LOCATIONID_UNIT  = b.LOCATIONID_UNIT,
                    //INVENTDIMID_PART = b.INVENTDIMID_PART, //comment steven
                    //SITEID_PART = b.SITEID_PART, //comment steven
                    //LOCATIONID_PART = b.LOCATIONID_PART, //comment steven
                    RECID = b.RECID
                };

            return result;
                
        }


        public MPMWMS_SPG_INCLUDE_BOM_DETAIL item(long recid)
        {
            IQueryable<MPMWMS_SPG_INCLUDE_BOM_DETAIL> search =
                from line in Context.MPMWMS_SPG_INCLUDE_BOM_DETAILs
                where
                    line.RECID == recid
                select line;

            return search.FirstOrDefault();
        }




        public void Insert(MPMWMS_SPG_INCLUDE_BOM_DETAIL row)
        {
            //row.CREATEBY = UserSession.NPK;
            //row.CREATEDATE = MPMDateUtil.DateTime;
            //Context.MPMWMS_BOXPROPERTies.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_SPG_INCLUDE_BOM_DETAIL row)
        {
            //row.MODIFBY = UserSession.NPK;
            //row.MODIFDATE = MPMDateUtil.DateTime;
        }

        public void Delete(MPMWMS_SPG_INCLUDE_BOM_DETAIL row)
        {
            //Context.MPMWMS_BOXPROPERTies.DeleteOnSubmit(row);
        }

        /*
        public MPMWMS_SPG_INCLUDE_BOM_DETAIL item(String boxId)
        {
            IQueryable<MPMWMS_SPG_INCLUDE_BOM_DETAIL> search =
                from line in Context.MPMWMS_BOXPROPERTies
                where
                    line.BOXID == boxId
                select line;

            return search.FirstOrDefault();
        }
        */

    }
}
