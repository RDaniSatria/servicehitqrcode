﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.SpgIncludeBomDetail
{
    public interface ISpgIncludeBomDetailObject
    {
        IQueryable<MPMWMS_SPG_INCLUDE_BOM_TRANS> List(String site_id);
        MPMWMS_SPG_INCLUDE_BOM_DETAIL item(long recid);
    }
}
