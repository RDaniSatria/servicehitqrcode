﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PackingPartLine
{
    [Serializable]
    public class PDT_PACKING_DATA_LINE
    {
        public String PACKING_ID { get; set; }
        public String PICKING_ID { get; set; }
        public String DATAAREA_ID { get; set; }
        public String PART_ID { get; set; }
        public String DEALER_CODE { get; set; }
        public String DEALER_NAME { get; set; }
        public int IS_PACKED { get; set; }
        public int QTY { get; set; }


        public PDT_PACKING_DATA_LINE(String packing_id, 
            String picking_id, 
            String dataarea_id, 
            String part_id, 
            String dealer_code,
            String dealer_name,
            int is_packed)
        {
            PACKING_ID = packing_id;
            PICKING_ID = picking_id;
            DATAAREA_ID = dataarea_id;
            PART_ID = part_id;
            DEALER_CODE = dealer_code;
            DEALER_NAME = dealer_name;
            IS_PACKED = is_packed;
        }

        public PDT_PACKING_DATA_LINE(String packing_id,
            String picking_id,
            String dataarea_id,
            String part_id,
            String dealer_code,
            String dealer_name,
            int is_packed,
            int qty)
        {
            PACKING_ID = packing_id;
            PICKING_ID = picking_id;
            DATAAREA_ID = dataarea_id;
            PART_ID = part_id;
            DEALER_CODE = dealer_code;
            DEALER_NAME = dealer_name;
            IS_PACKED = is_packed;
            QTY = qty;
        }


    }
}
