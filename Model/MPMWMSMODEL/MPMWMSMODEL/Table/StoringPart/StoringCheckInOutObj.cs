﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StroringPart
{
    public class StoringCheckInOutObj : MPMDbObject<WmsUnitDataContext>
    {
        public StoringCheckInOutObj()
            : base()
        { 
        
        }

        public Func<WmsUnitDataContext, IQueryable<StoringCheckInOutRec>>
            MessageQuery =
             CompiledQuery.Compile((WmsUnitDataContext db)
            =>
            from a in db.MPMSTORINGPARTCHECKINOUTs
            select new StoringCheckInOutRec
            {
                IDCheckInOut = a.IDCHECKIN,
                IDUSer = a.IDUSER,
                Date = a.DATE.ToString(),
                Checkintime = a.CHECKINTIME,
                Checkouttime = a.CHECKOUTTIME,
                Checkintimetruk = a.CHECKINTIMETRUCK,
                Checkouttimetruk = a.CHECKOUTTIMETRUCK,
                Nopol = a.XTSPLATENO,
                totalworknopol = a.TOTALWORKHOURPERNOPOL,
                totalworkuser = a.TOTALWORKHOURPERUSER
            }
            );

        public List<StoringCheckInOutRec> ListEkspedisi()
        {
            try
            {

                var result = MessageQuery((WmsUnitDataContext)Context).ToList();
               
                var query = result.ToList();


                return query;



            }


            catch (MPMException)
            {
                return null;
            }
        }
        public MPMSTORINGPARTCHECKINOUT ItemCheck(string nopol, string keyDate, string user_id)
        {
            try
            {
                //return ListEkspedisi().Where(x => x.Nopol.Equals(nopol) && x.Date == keyDate && x.IDUSer.Equals(user_id)).Take(1).FirstOrDefault();
                //return ListEkspedisi();
                return MessageQueryA((WmsUnitDataContext)Context).Where(x => x.XTSPLATENO.Equals(nopol) && x.DATE.Date == Convert.ToDateTime( keyDate) && x.IDUSER.Equals(user_id)).Take(1).FirstOrDefault();
                
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public static Func<WmsUnitDataContext, IQueryable<MPMSTORINGPARTCHECKINOUT>>
            MessageQueryA =
             CompiledQuery.Compile((WmsUnitDataContext db)
                    =>
                from a in db.MPMSTORINGPARTCHECKINOUTs
                select a
            );
        public List<MPMSTORINGPARTCHECKINOUT> ListUpdate()
        {
            try
            {
                var result = MessageQueryA(Context);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public MPMSTORINGPARTCHECKINOUT ItemUpdate(int idchekin , string userid)
        {
            try
            {
                return ListUpdate().Where(x => x.IDCHECKIN.Equals(idchekin) && x.IDUSER.Equals(userid)).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public MPMSTORINGPARTCHECKINOUT ItemUpdateTruk(string nopol, DateTime date)
        {
            try
            {
                return ListUpdate().Where(x => x.XTSPLATENO.Equals(nopol) && x.DATE == date).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public string Insert(MPMSTORINGPARTCHECKINOUT row)
        {
            try { 
            row.CHECKINTIME = DateTime.Now;
            row.IDUSER = UserSession.USER_ID;
            
            Context.MPMSTORINGPARTCHECKINOUTs.InsertOnSubmit(row);
            Context.SubmitChanges();
            return row.IDCHECKIN.ToString();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public void Update(MPMSTORINGPARTCHECKINOUT item)
        {
            try
            {

                item.CHECKOUTTIME = DateTime.Now;
                double TotalUser = (DateTime.Now - item.CHECKINTIME.Value).TotalHours;
                item.TOTALWORKHOURPERUSER = (int)TotalUser;
                Context.SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


    }
}
