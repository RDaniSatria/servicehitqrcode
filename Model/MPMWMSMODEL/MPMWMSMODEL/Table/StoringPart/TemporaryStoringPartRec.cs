﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StoringPart
{
    public class TemporaryStoringPartRec
    {
        public int NOMOR_ID { get; set; }
        public String Nopol { get; set; }
        public String Part { get; set; }
        public int? Qty { get; set; }
        public String User_ID { get; set; }
        public DateTime? Createddate { get; set; }
        public String Createdby { get; set; }
    }
}
