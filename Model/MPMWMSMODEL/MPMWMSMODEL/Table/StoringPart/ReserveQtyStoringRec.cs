﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StroringPart
{
    public class ReserveQtyStoringRec
    {
        public int Number
        {
            get;
            set;
        }
        public string Part
        {
            get;
            set;

        }
        public string LokasiBaru
        {
            get;
            set;
        }
        public int? QtyIn
       
        {
            get;
            set;
        }
        public string Dokumen
        {
            get;
            set;
        }
        public int? QtyInDokumen
        {
            get;
            set;
        }
        public int? Sisa
        {
            get;
            set;
        }
        public int? Claim
        {
            get;
            set;
        }
        public int? QtyPembanding
        {
            get;
            set;
        }
        public string JournalIdStoring
        {
            get;
            set;
        }
        public int? StatusPosting
        {
            get;
            set;
        }
        public DateTime? CreateDate
        {
            get;
            set;
        }

    }
}
