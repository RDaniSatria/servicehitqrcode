﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StroringPart
{
    public class StoringCheckInOutRec
    {
        public int IDCheckInOut
        {
            get;
            set;
        }
        public string IDUSer
        {
            get;
            set;
        }
        public string Date
        {
            get;
            set;
        }
        public DateTime? Checkintime 
        {
            get;
            set;
        }
        public DateTime? Checkouttime
        {
            get;
            set;
        }
        public DateTime? Checkintimetruk
        {
            get;
            set;
        }
        public DateTime? Checkouttimetruk
        {
            get;
            set;
        }
        public string Nopol
        {
            get;
            set;
        }
        public int? totalworkuser
        {
            get;
            set;
        }
        public int? totalworknopol
        {
            get;
            set;
        }
    }
}