﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StroringPart
{
    public class ReserveQtyStoringObj : MPMDbObject<WmsUnitDataContext>
    {
        public ReserveQtyStoringObj()
            : base()
        { 
        }
        public Func<WmsUnitDataContext, IQueryable<ReserveQtyStoringRec>>
            MessageQuery =
             CompiledQuery.Compile((WmsUnitDataContext db)
            =>
            from a in db.MPMRESERVEQTYSTORINGPARTs
            select new ReserveQtyStoringRec
            {
               Number = a.NUMBER,
               JournalIdStoring = a.JOURNALIDSTORING,
               Dokumen = a.DOKUMEN,
               LokasiBaru = a.LOKASIBARU,
               Part = a.PART,
               QtyIn = a.QTYIN,
               QtyInDokumen = a.QTYINDOKUMEN,
               Claim = a.CLAIM,
               Sisa = a.SISA,
               QtyPembanding = a.QTYPEMBANDING,
               StatusPosting = a.STATUSPOSTING,
               CreateDate = a.CREATEDDATE
               
            }
            );
        public List<ReserveQtyStoringRec> ListReserveRec()
        {
            try
            {
                var result = MessageQuery((WmsUnitDataContext)Context).ToList();
                var query = result.ToList();
                return query;                
            }
            catch (MPMException)
            {
                return null;
            }
        }
        //public List<ReserveQtyStoringRec> ItemUpdateStatus(string jurnalid, int statusposting)
        //{
        //    try
        //    {

        //        var result = MessageQuery((WmsUnitDataContext)Context).Where(x => x.Dokumen.Equals(jurnalid) && x.StatusPosting.Equals(statusposting)).ToList();
        //        return result.ToList();
        //    }
        //    catch (MPMException)
        //    {
        //        return null;
        //    }
        //}
        public MPMRESERVEQTYSTORINGPART ItemUpdatestatus(string jurnal, int status, int sisa, int claim)
        {
            try
            {
                string date = DateTime.Now.ToString("MM/dd/yyyy");
                return ListUpdate().Where(x => x.DOKUMEN.Equals(jurnal) && x.STATUSPOSTING.Equals(status) && x.SISA.Equals(sisa) && x.CLAIM.Equals(claim) && x.CREATEDDATE.Value.Date == Convert.ToDateTime(date)).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public string Insert(MPMRESERVEQTYSTORINGPART row)
        {
            try
            {

                row.CREATEDDATE = DateTime.Now;
                row.MODIFDATE = DateTime.Now;
                Context.MPMRESERVEQTYSTORINGPARTs.InsertOnSubmit(row);
                Context.SubmitChanges();
                return row.NUMBER.ToString();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public static Func<WmsUnitDataContext, IQueryable<MPMRESERVEQTYSTORINGPART>>
            MessageQueryA =
             CompiledQuery.Compile((WmsUnitDataContext db)
                    =>
                from a in db.MPMRESERVEQTYSTORINGPARTs
                select a
            );
        public List<MPMRESERVEQTYSTORINGPART> ListUpdate()
        {
            try
            {
                var result = MessageQueryA(Context);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public void Update(MPMRESERVEQTYSTORINGPART item)
        {
            try
            {

                
                Context.SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
       
    }
}
