﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MPMWMSMODEL.Table.StoringPart
{
    public class TemporaryStoringPartObj : MPMDbObject<WmsUnitDataContext>
    {
        public TemporaryStoringPartObj() 
            : base()
        {

        }

        public Func<WmsUnitDataContext, IQueryable<TemporaryStoringPartRec>>
            MessageQuery = CompiledQuery.Compile((WmsUnitDataContext db)
            =>
                from a in db.MPMRESEVETEMPs
                select new TemporaryStoringPartRec
                {
                    NOMOR_ID = a.NOMOR,
                    Nopol   = a.NOPOL,
                    Part    = a.PART,
                    Qty     = a.QTY,
                    User_ID = a.USERID,
                    Createddate  = a.CREATRDATETIME,
                    Createdby    = a.CREATEBY
                }
            );

        public List<TemporaryStoringPartRec> ListTempStoring()
        {
            try
            {

                var result = MessageQuery((WmsUnitDataContext)Context).ToList();

                var query = result.ToList();

                return query;

            }
            catch (MPMException)
            {
                return null;
            }
        }

        public static Func<WmsUnitDataContext,string, string, IQueryable<MPMRESEVETEMP>>
            MessageQueryTemp =
             CompiledQuery.Compile((WmsUnitDataContext db, string part, string nopol)
                    =>
                from a in db.MPMRESEVETEMPs
                where a.NOPOL.Equals(nopol) && a.PART.Equals(part)
                select a
            );

        public List<MPMRESEVETEMP> List( string part, string nopol)
        {
            try
            {
                var result = MessageQueryTemp(Context, part, nopol);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public static Func<WmsUnitDataContext, IQueryable<MPMRESEVETEMP>>
            MessageQueryDelete =
             CompiledQuery.Compile((WmsUnitDataContext db)
                    =>
                from a in db.MPMRESEVETEMPs
                select a
            );

        public List<MPMRESEVETEMP> Listdelete()
        {
            try
            {
                var result = MessageQueryDelete(Context);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public MPMRESEVETEMP Item(int idMessage)
        {
            try
            {
                return Listdelete().Where(x => x.NOMOR == idMessage).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        public void Delete(MPMRESEVETEMP item)
        {
            try
            {
                Context.MPMRESEVETEMPs.DeleteOnSubmit(item);
                
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }

        }
        public string Insert(MPMRESEVETEMP row)
        {
            try
            {
                row.CREATRDATETIME = DateTime.Now;
                row.CREATEBY = UserSession.USER_ID;
                row.USERID = UserSession.USER_ID;

                Context.MPMRESEVETEMPs.InsertOnSubmit(row);
                Context.SubmitChanges();
                return row.NOMOR.ToString();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


      
            
    }
}
