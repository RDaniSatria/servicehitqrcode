﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.Bor
{
    public class BorObject : MPMDbObject<WmsUnitDataContext>, IBorObject
    {
        public BorObject()
            : base()
        {
        }

        public MPMWMS_BOR Create()
        {
            MPMWMS_BOR row = new MPMWMS_BOR();
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public void Insert(MPMWMS_BOR item)
        {            
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.CREATE_BY = UserSession.NPK;
            item.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_BORs.InsertOnSubmit(item);
            //Context.SubmitChanges();
        }

        public List<MPMWMS_SP_AUTOBOR_LISTResult> ListAutoBor(String SITEID)
        {
            try
            {
                var data = from a in Context.MPMWMS_SP_AUTOBOR_LIST(SITEID)
                           select a;
                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_SP_AUTOBOR_DETAILUNITResult> ListAutoBorDetailUnit(String JOURNALCOLLECTIVEID)
        {
            try
            {
                var data = ((WmsUnitDataContext)Context).MPMWMS_SP_AUTOBOR_DETAILUNIT(JOURNALCOLLECTIVEID); //from a in Context.MPMWMS_SP_AUTOBOR_DETAILUNIT(JOURNALCOLLECTIVEID)
                                                                                                            //select a;
                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_SP_AUTOBOR_DETAILPARTResult> ListAutoBorDetailPart(String JOURNALCOLLECTIVEID)
        {
            try
            {
                var data = from a in Context.MPMWMS_SP_AUTOBOR_DETAILPART(JOURNALCOLLECTIVEID)
                           select a;
                return data.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_UNIT> ListProcessBOM(String JOURNALBOMID, String UNITTYPE, String UNITCOLOR)
        {
            try
            {
                //var data = from c in Context.MPMWMS_BOMs
                //           join d in Context.MPMWMS_UNITs
                //           on new { MACHINE_ID = c.MACHINE_ID } equals new { MACHINE_ID = d.MACHINE_ID }
                //           where d.STATUS == "RFS" && c.JOURNAL_ID == JOURNALBOMID
                //           && d.UNIT_TYPE == UNITTYPE && d.COLOR_TYPE == UNITCOLOR
                //           orderby d.MACHINE_ID ascending
                //           select d;
                //return data.ToList();


                var res = Context.ExecuteQuery<MPMWMS_UNIT>("EXEC MPMWMS_SP_AUTOBOR_LISTPROCESSBOM '" + JOURNALBOMID + "', '" + UNITTYPE+ "', '" + UNITCOLOR + "'").ToList();
                //var hasil = res.ToList();

                return res.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertHistory(MPMWMS_AUTOBORHISTORY item)
        {
            item.CREATEBY = UserSession.NPK;
            item.CREATEDATE = MPMDateUtil.DateTime;
            Context.MPMWMS_AUTOBORHISTORies.InsertOnSubmit(item);
        }

        public MPMWMS_SPG_INCLUDE_BOM_DETAIL FindJournalBomId(string JOURNALBOMID)
        {
            var data = from a in Context.MPMWMS_SPG_INCLUDE_BOM_DETAILs where a.BOMID == JOURNALBOMID
                       select a;
            return data.FirstOrDefault();
        }



    }

}
