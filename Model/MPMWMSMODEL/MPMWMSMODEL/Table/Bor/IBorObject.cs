﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Table.Bor
{
    public interface IBorObject
    {
        MPMWMS_BOR Create();
        void Insert(MPMWMS_BOR item);
    }
}
