﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Setting
{
    public class SettingDetailObject: MPMDbObject<WmsUnitDataContext>
    {
        public SettingDetailObject()
            : base()
        {
        }

        public String getEnumDay(DayOfWeek _today)
        {
            int hari = 0;

            switch (_today)
            {
                case DayOfWeek.Monday:
                    hari = 0;
                    break;

                case DayOfWeek.Tuesday:
                    hari = 1;
                    break;

                case DayOfWeek.Wednesday:
                    hari = 2;
                    break;

                case DayOfWeek.Thursday:
                    hari = 3;
                    break;

                case DayOfWeek.Friday:
                    hari = 4;
                    break;

                case DayOfWeek.Saturday:
                    hari = 5;
                    break;

                case DayOfWeek.Sunday:
                    hari = 6;
                    break;
            }

            return hari.ToString();
        }

        public MPM_SETTING_DETAIL Item(string settingId, string keyTag1, string keyTag2)
        {
            try
            {
                var itemsObj =  from a in Context.MPM_SETTING_DETAILs
                                    where (a.SETTING_ID == settingId)
                                    && (a.KEY_TAG_1 == keyTag1)
                                    && (a.KEY_TAG_2 == keyTag2)
                                join h in Context.MPM_SETTINGs on a.SETTING_ID equals h.SETTING_ID
                                    where (h.IS_ACTIVE == "Y")
                                select a;

                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPM_SETTING_DETAIL item)
        {
            try
            {
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                Context.MPM_SETTING_DETAILs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPM_SETTING_DETAIL item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
