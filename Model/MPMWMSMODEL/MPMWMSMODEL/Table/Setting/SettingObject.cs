﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Setting
{
    public class SettingObject : MPMDbObject<WmsUnitDataContext>
    {
        public SettingObject()
            : base()
        {
        }

        public MPM_SETTING Item(string settingId)
        {
            try
            {
                var itemsObj =  from h in Context.MPM_SETTINGs
                                    where (h.IS_ACTIVE == "Y")
                                    && (h.SETTING_ID == settingId)
                                select h;

                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPM_SETTING item)
        {
            try
            {
                item.CREATE_BY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATE_DATE = MPMDateUtil.DateTime;
                Context.MPM_SETTINGs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPM_SETTING item)
        {
            try
            {
                item.MODIF_BY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIF_DATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
