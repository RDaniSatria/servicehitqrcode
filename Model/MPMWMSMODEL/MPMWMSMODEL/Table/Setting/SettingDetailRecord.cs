﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Setting
{
    public class SettingDetailRecord
    {
        public String SETTING_ID { get; set; }
        public String SETTING_NAME { get; set; }
        public String KEY_TAG_1 { get; set; }
        public String KEY_TAG_2 { get; set; }
        public String TAG_1 { get; set; }
        public String TAG_2 { get; set; }

        public SettingDetailRecord()
        {
        }

        public SettingDetailRecord(String _settingId, String _settingName, String _keyTag1, String _keyTag2, String _tag1, String _tag2)
        {
            SETTING_ID = _settingId;
            SETTING_NAME = _settingName;
            KEY_TAG_1 = _keyTag1;
            KEY_TAG_2 = _keyTag2;
            TAG_1 = _tag1;
            TAG_2 = _tag2;
        }
    }
}
