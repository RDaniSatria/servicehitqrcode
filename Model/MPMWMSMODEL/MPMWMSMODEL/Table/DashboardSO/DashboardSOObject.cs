﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.SO;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.DashboardSO
{
    public class DashboardSOObject : MPMDbObject<WmsUnitDataContext>
    {
        public DashboardSOObject()
            : base()
        {
        }

        private static Func<WmsUnitDataContext, int, string, string, string, IQueryable<DASHBOARD_SO_MONITORING_REC>>
          SoQuery =
               CompiledQuery.Compile((WmsUnitDataContext db, int konstanta, string dataareaid, string mdcode, string siteid)
                   =>
               from a in db.MPM_WMS_DASHBOARDMONITORINGSO(konstanta, siteid, dataareaid, mdcode)
               select new DASHBOARD_SO_MONITORING_REC(
                   a.NAMA_PO.Trim(),
                   a.SO,
                   a.LINES == null? 0 : a.LINES,
                   a.PICK,
                   a.PACK,
                   a.INV,
                   a.SJ,
                   a.MUAT,
                   a.SISA_SO,
                   a.SISA_LINES,
                   a.SISA_INV,
                   a.SISA_SJ,
                   a.SISA_MUAT,
                   a.SISA_SJ_H_K
                   )
           );

        public List<DASHBOARD_SO_MONITORING_REC> LaporanList(int konstanta)
        {
            string dataareaid = UserSession.DATAAREA_ID;
            string mdcode = UserSession.MAINDEALER_ID;
            string siteid = UserSession.SITE_ID_MAPPING;
            try
            {
                Context.CommandTimeout = 240;
             //   WmsUnitDataContext db = new WmsUnitDataContext();
             //   db.CommandTimeout = 240;
                var query1 = SoQuery((WmsUnitDataContext)Context, konstanta, dataareaid, mdcode, siteid);
                  /*  from a in db.MPM_WMS_DASHBOARDMONITORINGSO(konstanta)
                    select new DASHBOARD_SO_MONITORING_REC(
                        a.NAMA_PO,
                        a.SO,
                        a.LINES,
                        a.PICK,
                        a.PACK,
                        a.INV,
                        a.SJ,
                        a.MUAT,
                        a.SISA_SO,
                        a.SISA_LINES,
                        a.SISA_INV,
                        a.SISA_SJ,
                        a.SISA_MUAT,
                        a.SISA_SJ_H_K
                        );*/
                List<DASHBOARD_SO_MONITORING_REC> list = new List<DASHBOARD_SO_MONITORING_REC>();
                list = query1.ToList();
                List<DASHBOARD_SO_MONITORING_REC> list2 = new List<DASHBOARD_SO_MONITORING_REC>();
                list2 = list.Where(x=>x.NAMA_PO!="OIL" && x.NAMA_PO!="BAN").ToList();
                DASHBOARD_SO_MONITORING_REC item = new DASHBOARD_SO_MONITORING_REC();
                item.NAMA_PO = "TOTAL";
                item.NUMBER = 6;
                item.SO = list2.Sum(x => x.SO);
                item.LINES = list2.Sum(x => x.LINES);
                item.PICK = list2.Sum(x => x.PICK);
                item.PACK = list2.Sum(x => x.PACK);
                item.INV = list2.Sum(x => x.INV);
                item.SJ = list2.Sum(x => x.SJ);
                item.MUAT = list2.Sum(x => x.MUAT);
                item.SISA_SO = list2.Sum(x => x.SISA_SO);
                item.SISA_LINES = list2.Sum(x => x.SISA_LINES);
                item.SISA_INV = list2.Sum(x => x.SISA_INV);
                item.SISA_SJ = list2.Sum(x => x.SISA_SJ);
                item.SISA_MUAT = list2.Sum(x => x.SISA_MUAT);
                item.SISA_SJ_H_MIN = list2.Sum(x => x.SISA_SJ_H_MIN);
                List<DASHBOARD_SO_MONITORING_REC> result = new List<DASHBOARD_SO_MONITORING_REC>();
                result.AddRange(list);
                result.Add(item);
                return result.OrderBy(x => x.NUMBER).ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
