﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.Customer
{
    public class customerPenjualan
    {
        public string CUSTOMER_AHM_NO { get; set; }

        public string NAME { get; set; }

        public string EMAIL { get; set; }

        public string PHONENUMBER { get; set; }

        public string IDENTITYNUMBER { get; set; }

        public string GENDER { get; set; }

        public DateTime? BIRTHDATE { get; set; }

        public string POSTCODE { get; set; }

        public string ADDRESS { get; set; }

        public string PROVINCE { get; set; }

        public string CITY { get; set; }

        public string DISTRICT { get; set; }

        public string USERID { get; set; }

        public string USERTYPE { get; set; }

        public string RESPONSESTATUS { get; set; }

        public string RESPONSECODE { get; set; }

        public string RESPONSEMSG { get; set; }

        public Byte? ISSEND { get; set; }

        public DateTime? SENDDATE { get; set; }

        public string CREATEBY { get; set; }

        public DateTime? CREATEDATE { get; set; }

        public string MODIFBY { get; set; }

        public DateTime? MODIFDATE { get; set; }

        public string KODEDEALER { get; set; }

        public string NOFAKTUR { get; set; }

        public string QRCODEUNCLAIM { get; set; }

        public string IDENTITYNUMBERUNCLAIM { get; set; }

        public string NUMBERFLAG { get; set; }

        public string ActivityDescription { get; set; }

        public string UNIT_TYPE { get; set; }

        public string COLOR_TYPE { get; set; }

        public string UNIT_YEAR { get; set; }

        public string FRAME_ID { get; set; }

        public string QR_CODE { get; set; }
    }

    public class mastertable
    {
        public string ActivityDescription { get; set; }
    }
}
