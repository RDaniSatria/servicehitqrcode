﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InventJournalTrans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventJournalTable
{
    public class InventJournalTableObject : MPMDbObject<WmsUnitDataContext>, IInventJournalTableObject
    {
        private InventJournalTransObject _InventJournalTransObject = new InventJournalTransObject();

        public InventJournalTableObject()
            : base()
        {
        }

        public InventJournalTransObject InventJournalTransObject
        {
            get { return _InventJournalTransObject; }
        }

        public INVENTJOURNALTABLE Item(String dataarea_id, String journal_id)
        {
            try
            {
                var itemsObj = from journaltable in Context.INVENTJOURNALTABLEs
                               where (journaltable.JOURNALID == journal_id) &&
                               (journaltable.DATAAREAID == dataarea_id)
                               select journaltable;

                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<INVENTJOURNALTABLE> ListOpname(String dataarea_id, String site_id, String category_code, int posted = 0)
        {
            IQueryable<INVENTJOURNALTABLE> search =
                from journaltable in Context.INVENTJOURNALTABLEs
                where (journaltable.DATAAREAID == dataarea_id)
                && (journaltable.INVENTSITEID == site_id)
                && (journaltable.JOURNALTYPE == 4)
                && (journaltable.POSTED == posted)
                && (journaltable.XTSCATEGORYCODE == category_code)
                select journaltable;
            return search.ToList();
        }

        public List<INVENTJOURNALTABLE> ListOpnameAll(String dataarea_id, String site_id, String category_code)
        {
            IQueryable<INVENTJOURNALTABLE> search =
                from journaltable in Context.INVENTJOURNALTABLEs
                where (journaltable.DATAAREAID == dataarea_id)
                && (journaltable.INVENTSITEID == site_id)
                && (journaltable.JOURNALTYPE == 4)
                && (journaltable.XTSCATEGORYCODE == category_code)
                select journaltable;
            return search.ToList();
        }
    }
}
