﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.InventJournalTable
{
    public interface IInventJournalTableObject
    {
        INVENTJOURNALTABLE Item(String dataarea_id, String journal_id);
    }
}
