﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SOPICArea
{
    public class SOPICAreaObject : MPMDbObject<WmsUnitDataContext>
    {
        public SOPICAreaObject() 
            : base()
        {
        }

        public MPMWMS_MAP_SO_PICAREA Item(
                  int _jobId
                , String _dataArea
                , String _mainDealer
                , String _siteId
                , String _dealerId
                , String _soId
            )
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_SO_PICAREAs
                               where (a.SOID == _soId)
                                && (a.DEALERID == _dealerId)
                                && (a.JOBID == _jobId)
                                && (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_MAP_SO_PICAREA ItemByJob(
                  int _jobId
                , String _dataArea
                , String _mainDealer
                , String _siteId
            )
        {
            try
            {
                var itemsObj = from a in Context.MPMWMS_MAP_SO_PICAREAs
                               where (a.JOBID == _jobId)
                                && (a.DATAAREAID == _dataArea)
                                && (a.MAINDEALERID == _mainDealer)
                                && (a.SITEID == _siteId)
                               select a;
                return itemsObj.FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateDonePicking(int jobId)
        {
            try
            {
                var itemObj = from area in Context.MPMWMS_MAP_SO_PICAREAs
                              orderby area.JOBID
                              where (area.DATAAREAID == UserSession.DATAAREA_ID)
                                  && (area.MAINDEALERID == UserSession.MAINDEALER_ID)
                                  && (area.SITEID == UserSession.SITE_ID_MAPPING)
                                  && (area.JOBID == jobId)
                                  //&& (area.SOID == item.SOID)
                              join orderTrans in Context.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                              where (orderTrans.DATAAREAID == UserSession.DATAAREA_ID)
                                  && (orderTrans.MPMSTATUSPICKING == 0) //flag utk yang belum diproses picking PDT
                              select area;

                // kalau hasil select BELUM DIPROSES PICKING PDT SUDAH TIDAK ADA, maka proses untuk JOB tersebut SELESAI
                if (itemObj.Count() == 0)
                {
                    //item.ISDONEPICKING = "Y";
                    //item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                    //item.MODIFDATE = MPMDateUtil.DateTime;

                    String QueryitemsObj =
                        "UPDATE S SET ISDONEPICKING = 'Y' " +
                        ", MODIFBY = '" + (UserSession == null ? "" : UserSession.NPK) + "' " +
                        ", MODIFDATE = '" + MPMDateUtil.DateTime + "' " +
                        "FROM MPMWMS_MAP_SO_PICAREA S " +
                        "WHERE S.JOBID = '" + jobId + "' " +
                        "AND S.DATAAREAID = '" + UserSession.DATAAREA_ID + "' " +
                        "AND S.SITEID = '" + UserSession.SITE_ID_MAPPING + "' " +
                        "AND S.MAINDEALERID = '" + UserSession.MAINDEALER_ID + "' ";

                    Context.ExecuteQuery<String>(QueryitemsObj);
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MAP_SO_PICAREA item)
        {
            try
            {
                item.MODIFBY = (UserSession == null ? "" : UserSession.NPK);
                item.MODIFDATE = MPMDateUtil.DateTime;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_MAP_SO_PICAREA item)
        {
            try
            {
                item.CREATEBY = (UserSession == null ? "" : UserSession.NPK);
                item.CREATEDATE = MPMDateUtil.DateTime;
                Context.MPMWMS_MAP_SO_PICAREAs.InsertOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_MAP_SO_PICAREA item)
        {
            try
            {
                Context.MPMWMS_MAP_SO_PICAREAs.DeleteOnSubmit(item);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
