﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.SalesLine
{
    public class SalesLineObject : MPMDbObject<WmsUnitDataContext>, ISalesLineObject
    {
        public SalesLineObject()
            : base()
        {
        }

        public SALESLINE Create()
        {
            SALESLINE item = new SALESLINE();
            return item;
        }

        public List<SALESLINE> List()
        {
            return Context.SALESLINEs.ToList();
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).SALESLINEs);
        }

        public int SumQuantitySales(String sales_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                IQueryable<int> itemsObj =
                    from invoice in Context.MPMVIEW_INVOICE_LINEs
                    where
                        invoice.DATAAREA_ID == dataarea_id
                        && invoice.SALES_ID == sales_id
                        && invoice.MAINDEALER_ID == maindealer_id
                        && invoice.SITE_ID == site_id
                    let k = new { invoice.SALES_ID, invoice.DATAAREA_ID, invoice.MAINDEALER_ID, invoice.SITE_ID }
                    group invoice by k into t_group
                    select Convert.ToInt32(t_group.Sum(invoice => invoice.SALES_DO + invoice.SALES_RETURN));
                /*
                IQueryable<int> itemsObj = from salesline in Context.SALESLINEs
                                            where
                                                (salesline.SALESID == sales_id) &&
                                                (salesline.DATAAREAID == dataarea_id)
                                            let k = new { item = salesline.SALESID, dataarea = salesline.DATAAREAID }
                                            group salesline by k into t_group
                                            select Convert.ToInt32(t_group.Sum(salesline => salesline.SALESQTY));*/
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public int SumQuantityReturn(String inventtransidreturn, String dataarea_id)
        {
            try
            {
                IQueryable<int> itemsObj = from salesline in Context.SALESLINEs
                                            where
                                                (salesline.INVENTTRANSIDRETURN == inventtransidreturn) &&
                                                (salesline.RETURNSTATUS == 4 || salesline.RETURNSTATUS == 5) &&
                                                (salesline.DATAAREAID == dataarea_id)
                                            let k = new { item = salesline.SALESID, dataarea = salesline.DATAAREAID }
                                            group salesline by k into t_group
                                            select Convert.ToInt32(t_group.Sum(salesline => salesline.SALESQTY));
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public SALESLINE SearchSalesLine(String dataarea_id, String sales_id, String item_id)
        {
            IQueryable<SALESLINE> search = from a in Context.SALESLINEs
                                           where
                                           a.DATAAREAID == dataarea_id &&
                                           a.SALESID == sales_id &&
                                           a.ITEMID == item_id &&
                                           a.SALESSTATUS == 3
                                           select a;

            return search.Take(1).FirstOrDefault();

        }

        public SALESLINE Item(String sales_id, String dataarea_id, String item_id, String color)
        {
            IQueryable<SALESLINE> search =
                from a in Context.SALESLINEs
                join b in Context.INVENTDIMs on
                    new { dataarea = a.DATAAREAID, invent = a.INVENTDIMID } equals
                    new { dataarea = b.DATAAREAID, invent = b.INVENTDIMID } into a_b
                from result in a_b
                where
                a.DATAAREAID == dataarea_id &&
                a.SALESID == sales_id &&
                a.ITEMID == item_id &&
                result.INVENTCOLORID == color &&
                a.SALESSTATUS == 3
                select a;

            return search.Take(1).FirstOrDefault();

        }

        public List<SALESLINE> List(String dataarea_id, String sales_id)
        {
            IQueryable<SALESLINE> search = from a in Context.SALESLINEs
                                           where
                                           (a.DATAAREAID == dataarea_id) &&
                                           (a.SALESID == sales_id) &&
                                           (a.SALESSTATUS == 3)
                                           select a;
            return search.ToList();
        }
    }
}
