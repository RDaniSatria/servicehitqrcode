﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.SalesLine
{
    public interface ISalesLineObject
    {
        SALESLINE Create();
        List<SALESLINE> List();
        void Refresh();
        int SumQuantitySales(String sales_id, String dataarea_id, String maindealer_id, String site_id);
        SALESLINE SearchSalesLine(String dataarea_id, String sales_id, String item_id);
    }
}
