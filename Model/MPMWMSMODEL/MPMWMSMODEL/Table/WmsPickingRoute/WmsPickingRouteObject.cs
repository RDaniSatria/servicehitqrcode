﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.WmsOrderTrans;
using MPMWMSMODEL.Table.SalesTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.WmsPickingRoute
{
    public class WmsPickingRouteObject: MPMDbObject<WmsUnitDataContext>, IWmsPickingRouteObject
    {
        private WmsOrderTransObject _WmsOrderTransObject = new WmsOrderTransObject();

        public WmsPickingRouteObject()
            : base()
        {
        }

        public WmsOrderTransObject WmsOrderTransObject
        {
            get { return _WmsOrderTransObject; }
        }

        public List<WMSPICKINGROUTE> ListByPIC(String dataarea_id, String pic = "")
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.XTSLOCKEDUSERID == pic &&
                    route.EXPEDITIONSTATUS == 3
                select route;
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> ListMonitoringTroley()
        {
            IQueryable<WMSPICKINGROUTE> search =
                from a in Context.WMSPICKINGROUTEs
                where a.XTSTROLLEYNBR != "" && a.XTSLOCKEDUSERID != ""
                select a;
             return search.ToList();
        }

        public List<MonitoringTrolley_REC> SummaryMonitoringTroley()
        {
            IQueryable<MonitoringTrolley_REC> search =
                from a in Context.WMSPICKINGROUTEs
                where a.XTSTROLLEYNBR != "" && a.XTSLOCKEDUSERID != ""
                group a by a.XTSTROLLEYNBR into g
                select new MonitoringTrolley_REC(
                g.Key,
                g.First().XTSLOCKEDUSERID,
                g.Count());
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> ListByExpeditionStatus(String dataarea_id, int expedition_status)
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                join sales in Context.SALESTABLEs
                on new { SALESID =  route.TRANSREFID} equals new { SALESID = sales.SALESID}
                where
                    route.DATAAREAID == dataarea_id &&
                    route.EXPEDITIONSTATUS == expedition_status &&
                    sales.INVENTSITEID == UserSession.SITE_ID_MAPPING
                select route;
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> List(String dataarea_id, int expedition_status, String pic = "")
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.XTSLOCKEDUSERID == pic &&
                    route.EXPEDITIONSTATUS == expedition_status
                select route;
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> List(String dataarea_id, int expedition_status, String pic = "", String troli_id = "")
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.XTSLOCKEDUSERID == pic &&
                    route.EXPEDITIONSTATUS == expedition_status &&
                    route.XTSTROLLEYNBR == troli_id
                select route;
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> ListForPacking(String dataarea_id, int expedition_status, String troli_id = "")
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                join salestable in Context.SALESTABLEs
                on new { TRANSREFID = route.TRANSREFID } equals new { TRANSREFID = salestable.SALESID }
                where
                    salestable.SALESSTATUS!= 3 &&
                    salestable.SALESSTATUS != 4 &&
                    route.DATAAREAID == dataarea_id &&
                    route.EXPEDITIONSTATUS == expedition_status &&
                    route.XTSTROLLEYNBR == troli_id &&
                    route.XTSLOCKEDUSERID != ""
                select route;
            return search.ToList();
        }

        public List<WMSPICKINGROUTE> List(String dataarea_id, String pic = "", String troli_id = "")
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.XTSLOCKEDUSERID == pic &&
                    route.XTSTROLLEYNBR == troli_id &&
                    route.EXPEDITIONSTATUS == 3
                select route;
            return search.ToList();
        }

        public WMSPICKINGROUTE Item(String dataarea_id, String route_id)
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.PICKINGROUTEID == route_id
                select route;
            return search.Take(1).FirstOrDefault();
        }

        public WMSPICKINGROUTE Item(String dataarea_id, String route_id, String troli_id)
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                where
                    route.DATAAREAID == dataarea_id &&
                    route.PICKINGROUTEID == route_id &&
                    route.XTSTROLLEYNBR == troli_id
                select route;
            return search.Take(1).FirstOrDefault();
        }

        #region FOR PACKING DEKSTOP APP

        public List<WMSPICKINGROUTE> ListReadyToPack(String dataarea_id, int expedition_status, String troli_id)
        {
            IQueryable<WMSPICKINGROUTE> search =
                from route in Context.WMSPICKINGROUTEs
                    where route.DATAAREAID == dataarea_id &&
                        route.EXPEDITIONSTATUS == expedition_status &&
                        route.XTSTROLLEYNBR == troli_id
                join salestable in Context.SALESTABLEs on new { TRANSREFID = route.TRANSREFID, DATAAREA = route.DATAAREAID, PARTITION = route.PARTITION } 
                                                   equals new { TRANSREFID = salestable.SALESID, DATAAREA = salestable.DATAAREAID, PARTITION = salestable.PARTITION }
                    where
                        salestable.SALESSTATUS != 3 &&
                        salestable.SALESSTATUS != 4
                join troli in Context.MPMWMS_TROLI_PARTs on route.XTSTROLLEYNBR equals troli.TROLI_ID
                select route;

            return search.ToList();
        }

        #endregion
    }
}
