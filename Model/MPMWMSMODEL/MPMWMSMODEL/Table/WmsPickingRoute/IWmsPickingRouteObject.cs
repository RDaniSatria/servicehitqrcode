﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.WmsPickingRoute
{
    public interface IWmsPickingRouteObject
    {
        List<WMSPICKINGROUTE> List(String dataarea_id, int expedition_status, String pic = "", String troli_id = "");
        List<WMSPICKINGROUTE> ListByPIC(String dataarea_id, String pic = "");
        List<WMSPICKINGROUTE> List(String dataarea_id, String troli_id, String pic = "");
        List<WMSPICKINGROUTE> ListByExpeditionStatus(String dataarea_id, int expedition_status);
        WMSPICKINGROUTE Item(String dataarea_id, String route_id);
        WMSPICKINGROUTE Item(String dataarea_id, String route_id, String troli_id);
    }
}
