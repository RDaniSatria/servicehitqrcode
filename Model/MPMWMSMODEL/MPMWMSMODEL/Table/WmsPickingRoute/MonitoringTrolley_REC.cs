﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.WmsPickingRoute
{
    public class MonitoringTrolley_REC
    {
        public String XTSTROLLEYNBR { get; set; }
        public String XTSLOCKEDUSERID { get; set; }
        public int SUM { get; set; }

        public MonitoringTrolley_REC()
        { }

        public MonitoringTrolley_REC(
            String xtsrolleynbr,
            String xtslockeduserid,
            int sum)
         {
             XTSTROLLEYNBR = xtsrolleynbr;
             XTSLOCKEDUSERID = xtslockeduserid;
             SUM = sum;
         }
    }
}
