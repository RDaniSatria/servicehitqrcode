﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ExpeditionLine
{
    public class ExpeditionLineObject: MPMDbObject<WmsUnitDataContext>, IExpeditionLineObject
    {
        public ExpeditionLineObject()
            : base()
        {
        }

        public MPMWMS_EXPEDITION_LINE Create()
        {
            MPMWMS_EXPEDITION_LINE item = new MPMWMS_EXPEDITION_LINE();
            item.DATAAREA_ID = UserSession.DATAAREA_ID;
            item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            item.SITE_ID = UserSession.SITE_ID_MAPPING;
            item.IS_BLACKLIST = "N";
            return item;
        }

        public List<MPMWMS_EXPEDITION_LINE> List(String expedition_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from expedition_line in Context.MPMWMS_EXPEDITION_LINEs
                               where (expedition_line.EXPEDITION_ID == expedition_id) 
                               && (expedition_line.DATAAREA_ID == dataarea_id)
                               && (expedition_line.MAINDEALER_ID == maindealer_id)
                               && (expedition_line.SITE_ID == site_id)
                               select expedition_line;
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Refresh()
        {
            Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues,
                    (Context).MPMWMS_EXPEDITION_LINEs);
        }

        public MPMWMS_EXPEDITION_LINE Item(String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = from expedition_line in Context.MPMWMS_EXPEDITION_LINEs
                               where (expedition_line.EXPEDITION_ID == expedition_id) 
                               && (expedition_line.POLICE_NUMBER == police_number) 
                               && (expedition_line.DATAAREA_ID == dataarea_id)
                               && (expedition_line.MAINDEALER_ID == maindealer_id)
                               && (expedition_line.SITE_ID == site_id)
                               select expedition_line;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPMWMS_EXPEDITION_LINE row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_EXPEDITION_LINEs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_EXPEDITION_LINE row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = DateTime.Now;
        }

        public void Delete(MPMWMS_EXPEDITION_LINE row)
        {
            try
            {
                Context.MPMWMS_EXPEDITION_LINEs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
