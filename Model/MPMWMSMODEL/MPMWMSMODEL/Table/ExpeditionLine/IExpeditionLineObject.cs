﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.ExpeditionLine
{
    public interface IExpeditionLineObject
    {
        MPMWMS_EXPEDITION_LINE Create();
        List<MPMWMS_EXPEDITION_LINE> List(String expedition_id, String dataarea_id, String maindealer_id, String site_id);
        void Refresh();
        MPMWMS_EXPEDITION_LINE Item(String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id);
        void Insert(MPMWMS_EXPEDITION_LINE row);
        void Delete(MPMWMS_EXPEDITION_LINE row);
    }
}
