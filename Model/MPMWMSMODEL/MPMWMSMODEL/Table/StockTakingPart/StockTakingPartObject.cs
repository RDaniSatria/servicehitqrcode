﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StockTakingPart
{
    public class StockTakingPartObject: MPMDbObject<WmsUnitDataContext>, IStockTakingPartObject
    {
        public StockTakingPartObject()
            : base()
        {
        }

        public List<MPMWMS_STOCK_TAKING_PART> List(String dataarea_id, String journal_id)
        {
            IQueryable<MPMWMS_STOCK_TAKING_PART> search =
                from opname in Context.MPMWMS_STOCK_TAKING_PARTs
                where
                    opname.DATAAREA_ID == dataarea_id
                    && opname.STOCK_TAKING_ID == journal_id
                orderby
                    opname.STOCK_TAKING_ID, opname.COUNTING_ID, opname.CHANGES_DATE ascending
                select opname;

            return search.ToList();
        }

        public MPMWMS_STOCK_TAKING_PART Create()
        {
            return new MPMWMS_STOCK_TAKING_PART();
        }

        public void Insert(MPMWMS_STOCK_TAKING_PART row)
        {
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = MPMDateUtil.DateTime;
            Context.MPMWMS_STOCK_TAKING_PARTs.InsertOnSubmit(row);
        }

        public void Update(MPMWMS_STOCK_TAKING_PART row)
        {
            row.MODIF_BY = UserSession.NPK;
            row.MODIF_DATE = MPMDateUtil.DateTime;
        }
    }
}
