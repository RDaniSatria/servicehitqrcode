﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.StockTakingPart
{
    public interface IStockTakingPartObject
    {
        List<MPMWMS_STOCK_TAKING_PART> List(String dataarea_id, String journal_id);
        MPMWMS_STOCK_TAKING_PART Create();
        void Insert(MPMWMS_STOCK_TAKING_PART row);
        void Update(MPMWMS_STOCK_TAKING_PART row);
    }
}
