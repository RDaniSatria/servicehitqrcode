﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.HistoryUnit
{
    public class HistoryUnitObject : MPMDbObject<WmsUnitDataContext>, IHistoryUnitObject
    {
        public HistoryUnitObject()
            : base()
        {            
        }

        public MPMWMS_HISTORY_UNIT Create(MPMWMS_UNIT unit)
        {
            MPMWMS_HISTORY_UNIT history = new MPMWMS_HISTORY_UNIT();
            history.MACHINE_ID = unit.MACHINE_ID;
            history.FRAME_ID = unit.FRAME_ID;
            history.DATAAREA_ID = unit.DATAAREA_ID;
            history.SITE_ID = unit.SITE_ID;
            history.WAREHOUSE_ID = unit.WAREHOUSE_ID;
            history.LOCATION_ID = unit.LOCATION_ID;
            history.CHANGES_DATE = DateTime.Now;
            history.UNIT_YEAR = unit.UNIT_YEAR;
            history.SL_ID = unit.SL_ID;
            history.SL_DATE = unit.SL_DATE;
            history.SPG_ID = unit.SPG_ID;
            history.SPG_DATE = unit.SPG_DATE;
            history.UNLOAD_START_TIME = unit.UNLOAD_START_TIME;
            history.UNLOAD_STOP_TIME = unit.UNLOAD_STOP_TIME;
            history.STORING_DATE = unit.STORING_DATE;
            history.DO_ID = unit.DO_ID;
            history.DO_DATE = unit.DO_DATE;
            history.PICKING_ID = unit.PICKING_ID;
            history.PICKING_DATE = unit.PICKING_DATE;
            history.PICKING_SCAN_TIME = unit.PICKING_SCAN_TIME;
            history.SHIPPING_ID = unit.SHIPPING_ID;
            history.SHIPPING_DATE = unit.SHIPPING_DATE;
            history.STATUS = unit.STATUS;
            history.IS_RFS = unit.IS_RFS;
            history.IS_READYTOPICK = unit.IS_READYTOPICK;
            history.IS_READYTOPICK_AX = unit.IS_READYTOPICK_AX;
            history.UNIT_GROUP = unit.UNIT_GROUP;
            history.COLOR_TYPE = unit.COLOR_TYPE;
            history.UNIT_TYPE = unit.UNIT_TYPE;
            history.SITE_ID_OLD = null;
            history.LOCATION_ID_OLD = null;
            history.CHANGES_TYPE = "U";
            history.CREATE_BY = UserSession.NPK;
            history.CREATE_DATE = DateTime.Now;

            return history;
        }

        public void Insert(MPMWMS_HISTORY_UNIT row)
        {
            Context.MPMWMS_HISTORY_UNITs.InsertOnSubmit(row);
        }

        public void Delete(MPMWMS_HISTORY_UNIT row)
        {
            Context.MPMWMS_HISTORY_UNITs.DeleteOnSubmit(row);
        }

        public int Count(String machine_id, String frame_id, String dataarea_id)
        {
            List<MPMWMS_HISTORY_UNIT> search = List(machine_id, frame_id, dataarea_id);
            return search.ToList().Count();
        }

        private IQueryable<MPMWMS_HISTORY_UNIT> RunQuery(String machine_id, String frame_id, String dataarea_id)
        {
            return from history in Context.MPMWMS_HISTORY_UNITs
                   where (history.FRAME_ID == frame_id) &&
                   (history.MACHINE_ID == machine_id) &&
                   (history.DATAAREA_ID == dataarea_id)
                   orderby history.CHANGES_DATE ascending
                   select history;
        }

        public MPMWMS_HISTORY_UNIT Item(String machine_id, String frame_id, String dataarea_id)
        {
            try
            {
                IQueryable<MPMWMS_HISTORY_UNIT> search = RunQuery(machine_id, frame_id, dataarea_id);
                var currentItem = search.FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_HISTORY_UNIT> List(String machine_id, String frame_id, String dataarea_id)
        {
            try
            {
                IQueryable<MPMWMS_HISTORY_UNIT> search = RunQuery(machine_id, frame_id, dataarea_id);
                return search.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public MPMWMS_HISTORY_UNIT Item(String machine_frame_id = "")
        {
            try
            {
                Context.CommandTimeout = 500000;
                IQueryable<MPMWMS_HISTORY_UNIT> search = from a in Context.MPMWMS_HISTORY_UNITs
                                                         where  a.MACHINE_ID == machine_frame_id
                                                         && !a.WAREHOUSE_ID_OLD.EndsWith("nrfs")
                                                         && a.WAREHOUSE_ID.EndsWith("nrfs")
                                                         orderby a.CREATE_DATE descending
                                                         select a;
                return search.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
