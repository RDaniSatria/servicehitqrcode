﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.HistoryUnit
{
    public interface IHistoryUnitObject
    {
        MPMWMS_HISTORY_UNIT Create(MPMWMS_UNIT unit);
        void Insert(MPMWMS_HISTORY_UNIT row);
        void Delete(MPMWMS_HISTORY_UNIT row);

        int Count(String machine_id, String frame_id, String dataarea_id);
        MPMWMS_HISTORY_UNIT Item(String machine_id, String frame_id, String dataarea_id);
        List<MPMWMS_HISTORY_UNIT> List(String machine_id, String frame_id, String dataarea_id);
        MPMWMS_HISTORY_UNIT Item(String machine_frame_id = "");
    }
}
