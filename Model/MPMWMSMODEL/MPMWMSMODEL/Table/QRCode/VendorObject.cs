﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.QRCode
{
    public class VendorObject
    {
        public string NumEngine { get; set; }
        public string IdentityNumber { get; set; }
        public string PoliceNo { get; set; }
        public string Invoice { get; set; }
    }
}
