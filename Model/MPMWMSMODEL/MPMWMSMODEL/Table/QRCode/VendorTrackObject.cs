﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.QRCode
{
    public class VendorTrackObject
    {
        public string Code { get; set; }
        public string UserId { get; set; }
        public string UserType { get; set; }
        public string FunLocCode { get; set; }
    }
}
