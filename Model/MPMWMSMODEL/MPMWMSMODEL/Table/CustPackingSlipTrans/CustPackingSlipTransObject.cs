﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CustPackingSlipTrans
{
    public class CustPackingSlipTransObject: MPMDbObject<WmsUnitDataContext>, ICustPackingSlipTransObject
    {
        public CustPackingSlipTransObject()
            : base()
        {
        }

        public CUSTPACKINGSLIPTRAN Item(string dataarea_id, string packing_id, string item_id)
        {
            IQueryable<CUSTPACKINGSLIPTRAN> search =
                from tran in Context.CUSTPACKINGSLIPTRANs
                where
                    tran.DATAAREAID == dataarea_id
                    && tran.PACKINGSLIPID == packing_id
                    && tran.ITEMID == item_id
                select tran;

            return search.Take(1).FirstOrDefault();
        }

        public List<CUSTPACKINGSLIPTRAN> List(string dataarea_id, string packing_id, string item_id)
        {
            IQueryable<CUSTPACKINGSLIPTRAN> search =
                from tran in Context.CUSTPACKINGSLIPTRANs
                where
                    tran.DATAAREAID == dataarea_id
                    && tran.PACKINGSLIPID == packing_id
                    && tran.ITEMID == item_id
                select tran;

            return search.ToList();
        }

        public List<CUSTPACKINGSLIPTRAN> ListByPackingId(string dataarea_id, string packing_id)
        {
            IQueryable<CUSTPACKINGSLIPTRAN> search =
                from tran in Context.CUSTPACKINGSLIPTRANs
                where
                    tran.DATAAREAID == dataarea_id
                    && tran.PACKINGSLIPID == packing_id
                select tran;

            return search.ToList();
        }
    }
}
