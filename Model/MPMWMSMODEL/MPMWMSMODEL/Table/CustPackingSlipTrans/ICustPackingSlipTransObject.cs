﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.CustPackingSlipTrans
{
    public interface ICustPackingSlipTransObject
    {
        CUSTPACKINGSLIPTRAN Item(string dataarea_id, string packing_id, string item_id);
        List<CUSTPACKINGSLIPTRAN> List(string dataarea_id, string packing_id, string item_id);
    }
}
