﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.QrUnit
{
   public class QrUnitObject : MPMDbObject<WmsMpmLiveDataContext>
    {
        WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
        public QrUnitObject() 
            : base()
        {
            
        }

        public MPMWMSQRUNIT ItemByMachineQrUnit(String nosin, String siteid)
        {
            try
            {
               
                var itemsObj = from unit in db.MPMWMSQRUNITs
                               where (unit.ENGINENO == nosin) &&
                               (unit.SITEID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMSQRUNIT ItemByMachineQr1(String qr, String siteid)
        {
            try
            {

                var itemsObj = from unit in db.MPMWMSQRUNITs
                               where (unit.QRCODE == qr) &&
                               (unit.SITEID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMSQRUNITLOG ItemByMachineQrUnitLog(String nosin, String siteid)
        {
            try
            {
                var itemsObj = from unit in db.MPMWMSQRUNITLOGs
                               where (unit.ENGINENO == nosin) &&
                               (unit.SITEID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMSQRUNIT ItemByQrQrUnit(String qr, /*String nosin,*/ String siteid)
        {
            try
            {

                var itemsObj = from unit in db.MPMWMSQRUNITs
                               where (unit.QRCODE == qr)&&
                               //(unit.ENGINENO == nosin) &&
                               (unit.SITEID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMSQRUNITLOG ItemByQrQrUnitLog(String qr, /*String nosin,*/ String siteid)
        {
            try
            {
                var itemsObj = from unit in db.MPMWMSQRUNITLOGs
                               where (unit.QRCODE == qr) &&
                               //(unit.ENGINENO == nosin) &&
                               (unit.SITEID == siteid)
                               select unit;
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void updateUnmatch(MPMWMSQRUNIT item)
        {

        }

        public MPMWMSQRUNIT RowUnit() {
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();
            return item;
        }


    }
}
