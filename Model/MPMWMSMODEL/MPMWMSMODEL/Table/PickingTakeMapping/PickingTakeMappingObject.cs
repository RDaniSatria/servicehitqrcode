﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Table.PickingTakeMapping
{
    public class PickingTakeMappingObject : MPMDbObject<WmsUnitDataContext>, IPickingTakeMappingObject
    {
        public PickingTakeMappingObject()
            : base()
        {
        }

        public MPMWMS_PICKING_TAKE_MAPPING Create()
        {
            MPMWMS_PICKING_TAKE_MAPPING row = new MPMWMS_PICKING_TAKE_MAPPING();
            row.STATUS = "O";
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            return row;
        }

        public void Insert(MPMWMS_PICKING_TAKE_MAPPING row)
        {
            row.DATAAREA_ID = UserSession.DATAAREA_ID;
            row.MAINDEALER_ID = UserSession.MAINDEALER_ID;
            row.SITE_ID = UserSession.SITE_ID_MAPPING;
            row.CREATE_BY = UserSession.NPK;
            row.CREATE_DATE = DateTime.Now;
            Context.MPMWMS_PICKING_TAKE_MAPPINGs.InsertOnSubmit(row);
        }

        private IQueryable<MPMWMS_PICKING_TAKE_MAPPING> RunQuery(String picking_id,
            String dataarea_id, String maindealer_id, String site_id)
        {
            return from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                    where (taking.PICKING_ID == picking_id) &&
                    (taking.DATAAREA_ID == dataarea_id) &&
                    (taking.MAINDEALER_ID == maindealer_id) &&
                    (taking.SITE_ID == site_id)
                    select taking;
        }

        private IQueryable<MPMWMS_PICKING_TAKE_MAPPING> RunQuery(String picking_id, 
            String dataarea_id, String maindealer_id, String site_id, 
            String frame_id_take, String machine_id_take)
        {
            return from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                    where (taking.PICKING_ID == picking_id) &&
                    (taking.DATAAREA_ID == dataarea_id) &&
                    (taking.MAINDEALER_ID == maindealer_id) &&
                    (taking.SITE_ID == site_id) &&
                    (taking.MACHINE_ID_TAKE == machine_id_take) &&
                    (taking.FRAME_ID_TAKE == frame_id_take)
                    select taking;
        }


        private IQueryable<MPMWMS_PICKING_TAKE_MAPPING> RunQueryNoPicking(
            String dataarea_id, String maindealer_id, String site_id,
            String frame_id_take, String machine_id_take)
        {
            return from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                    where
                    (taking.DATAAREA_ID == dataarea_id) &&
                    (taking.MAINDEALER_ID == maindealer_id) &&
                    (taking.SITE_ID == site_id) &&
                    (taking.MACHINE_ID_TAKE == machine_id_take) &&
                    (taking.FRAME_ID_TAKE == frame_id_take)
                    select taking;
        }

        public MPMWMS_PICKING_TAKE_MAPPING Item(String picking_id,
            String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = RunQuery(picking_id, dataarea_id, maindealer_id, site_id);
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_PICKING_TAKE_MAPPING ItemTakeNoPicking(
            String dataarea_id, String maindealer_id, String site_id,
            String frame_id_take, String machine_id_take)
        {
            try
            {
                var itemsObj = RunQueryNoPicking(dataarea_id, maindealer_id, site_id, frame_id_take, machine_id_take);
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_PICKING_TAKE_MAPPING ItemTake(String picking_id, 
            String dataarea_id, String maindealer_id, String site_id,
            String frame_id_take, String machine_id_take)
        {
            try
            {
                var itemsObj = RunQuery(picking_id, dataarea_id, maindealer_id, site_id, frame_id_take, machine_id_take);
                var currentItem = itemsObj.Take(1).FirstOrDefault();
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_PICKING_TAKE_MAPPING> List(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                var itemsObj = RunQuery(picking_id, dataarea_id, maindealer_id, site_id);
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public int Count(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            List<MPMWMS_PICKING_TAKE_MAPPING> take = List(picking_id, dataarea_id, maindealer_id, site_id);
            if (take != null)
            {
                return take.ToList().Count();
            }

            return 0;
        }

        public System.Data.Linq.Table<Module.MPMWMS_PICKING_TAKE_MAPPING> Context_MPMWMS_PICKING_TAKE_MAPPING()
        {
            return Context.MPMWMS_PICKING_TAKE_MAPPINGs;
        }

        public List<MPMWMS_PICKING_TAKE_MAPPING> ListOpen(String dataarea_id, String maindealer_id, String site_id)
        {
            IQueryable<MPMWMS_PICKING_TAKE_MAPPING> search =
                from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                where
                (taking.DATAAREA_ID == dataarea_id)
                && (taking.MAINDEALER_ID == maindealer_id)
                && (taking.SITE_ID == site_id)
                && (taking.STATUS == "O")
                select taking;
            return search.ToList();
        }

        public MPMWMS_PICKING_TAKE_MAPPING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id,
            String do_id, 
            String machine_id, String machine_id_take)
        {
            IQueryable<MPMWMS_PICKING_TAKE_MAPPING> search =
                from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                where
                (taking.DATAAREA_ID == dataarea_id)
                && (taking.MAINDEALER_ID == maindealer_id)
                && (taking.SITE_ID == site_id)
                && (taking.PICKING_ID == picking_id)
                && (taking.DO_ID == do_id)
                && (taking.MACHINE_ID == machine_id)
                && (taking.MACHINE_ID_TAKE == machine_id_take)
                select taking;

            return search.Take(1).FirstOrDefault();
        }

        public MPMWMS_PICKING_TAKE_MAPPING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id,
            String do_id, 
            String machine_id)
        {
            IQueryable<MPMWMS_PICKING_TAKE_MAPPING> search =
                from taking in Context.MPMWMS_PICKING_TAKE_MAPPINGs
                where
                (taking.DATAAREA_ID == dataarea_id)
                && (taking.MAINDEALER_ID == maindealer_id)
                && (taking.SITE_ID == site_id)
                && (taking.PICKING_ID == picking_id)
                && (taking.DO_ID == do_id)
                && (taking.MACHINE_ID == machine_id)
                select taking;

            return search.Take(1).FirstOrDefault();
        }

        public void Delete(MPMWMS_PICKING_TAKE_MAPPING row)
        {
            try
            {
                Context.MPMWMS_PICKING_TAKE_MAPPINGs.DeleteOnSubmit(row);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
