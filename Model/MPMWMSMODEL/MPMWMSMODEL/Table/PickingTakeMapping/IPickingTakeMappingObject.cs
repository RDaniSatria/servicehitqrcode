﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Table.PickingTakeMapping
{
    public interface IPickingTakeMappingObject
    {
        MPMWMS_PICKING_TAKE_MAPPING Create();
        void Insert(MPMWMS_PICKING_TAKE_MAPPING row);
        List<MPMWMS_PICKING_TAKE_MAPPING> List(String picking_id, String dataarea_id, String maindealer_id, String site_id);
        List<MPMWMS_PICKING_TAKE_MAPPING> ListOpen(String dataarea_id, String maindealer_id, String site_id);
        MPMWMS_PICKING_TAKE_MAPPING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id, 
            String machine_id, String machine_id_take);
        MPMWMS_PICKING_TAKE_MAPPING Item(String picking_id, String dataarea_id, String maindealer_id, String site_id, String do_id, 
            String machine_id);
        System.Data.Linq.Table<Module.MPMWMS_PICKING_TAKE_MAPPING> Context_MPMWMS_PICKING_TAKE_MAPPING();
        void Delete(MPMWMS_PICKING_TAKE_MAPPING row);
    }
}
