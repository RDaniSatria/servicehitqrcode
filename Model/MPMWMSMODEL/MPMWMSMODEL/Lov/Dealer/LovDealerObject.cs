﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.View.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Dealer
{
    public class LovDealerObject: IDisposable
    {
        MPMWMSMODEL.Module.WmsUnitDataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<CustomerRecord> ListDealerPart(String dataarea_id)
        {
            try
            {
                using (
                    var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    var itemsObj = 
                        from dealer in Context.MPM_VIEW_DEALERs
                        where dealer.dataareaid == dataarea_id
                        select new CustomerRecord(
                            dealer.dataareaid,
                            dealer.mdcode,
                            dealer.dealer,
                            null,
                            null,
                            null
                            );
                    return itemsObj.ToList();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<CustomerRecord> List(String dataarea_id)
        {
            try
            {
                using (
                    var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    //var itemsObj = from custtable in Context.CUSTTABLEs
                    //               join dirpartytable in Context.DIRPARTYTABLEs on custtable.PARTY equals dirpartytable.RECID
                    //               join logisticpostaladdress in Context.LOGISTICSPOSTALADDRESSes on dirpartytable.PRIMARYADDRESSLOCATION equals logisticpostaladdress.LOCATION
                    //               where
                    //               (custtable.DATAAREAID == dataarea_id) &&
                    //               (logisticpostaladdress.VALIDFROM.Date <= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date) &&
                    //               (logisticpostaladdress.VALIDTO.Date >= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date)
                    //               select new CustomerRecord(
                    //                    custtable.DATAAREAID,
                    //                    custtable.ACCOUNTNUM,
                    //                    dirpartytable.NAME,
                    //                    logisticpostaladdress.ADDRESS,
                    //                    logisticpostaladdress.CITY,
                    //                    logisticpostaladdress.STATE
                    //                   );
                    //return itemsObj.ToList();

                    var itemsObj = from MPMSERVVIEWCHANNELH1 in Context.MPMSERVVIEWCHANNELH1s
                                   //join dirpartytable in Context.DIRPARTYTABLEs on MPMSERVVIEWCHANNELH1.PARTY equals dirpartytable.RECID
                                   //join logisticpostaladdress in Context.LOGISTICSPOSTALADDRESSes on dirpartytable.PRIMARYADDRESSLOCATION equals logisticpostaladdress.LOCATION
                                   //where
                                   //(MPMSERVVIEWCHANNELH1.DATAAREAID == dataarea_id) &&
                                   //(logisticpostaladdress.VALIDFROM.Date <= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date) &&
                                   //(logisticpostaladdress.VALIDTO.Date >= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date)
                                   select new CustomerRecord(
                                        MPMSERVVIEWCHANNELH1.DATAAREAID,
                                        MPMSERVVIEWCHANNELH1.ACCOUNTNUM,
                                        MPMSERVVIEWCHANNELH1.NAME,
                                        MPMSERVVIEWCHANNELH1.ADDRESS,
                                        MPMSERVVIEWCHANNELH1.CITY,
                                        MPMSERVVIEWCHANNELH1.STATE
                                       );
                    return itemsObj.ToList();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_ROUTE_DEALER_UNIT> ListRouteUnit(string routeid)
        {
            try
            {
                var multi = (from mul in Context.MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTIs
                             where mul.ROUTE_ID == routeid
                            select mul.MULTI_ROUTE_ID).ToList();

                var itemsObj =
                    from unit in Context.MPMWMS_EAD_ROUTE_DEALER_UNITs
                    where !multi.Contains(unit.ROUTE_ID) && !unit.ROUTE_ID.Contains(routeid)
                    select unit;
                    return itemsObj.ToList();
                
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
