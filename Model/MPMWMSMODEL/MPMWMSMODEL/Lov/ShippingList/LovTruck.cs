﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.ShippingList
{
    public class LovTruck
    {
        public String POLICE_NUMBER { get; set; }
        public String DRIVER { get; set; }

        public LovTruck(String police_number, String driver)
        {
            POLICE_NUMBER = police_number;
            DRIVER = driver;
        }
    }
}
