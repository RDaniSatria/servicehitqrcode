﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Lov.ShippingList
{
    public class LovTruckShippingListObject : IDisposable
    {
        WmsUnitDataContext Context = new WmsUnitDataContext();

        public List<LovTruck> List(String dataarea_id, String maindealer_id, String site_id)
        {
            Context.CommandTimeout = 5000000;
            IQueryable<LovTruck> lov =
                from a in Context.MPMWMS_PICKINGs
                join b in (
                    from c in Context.MPMWMS_PICKING_TAKE_MAPPINGs
	                group c by new { 
		                c.PICKING_ID,
		                c.DATAAREA_ID,
		                c.MAINDEALER_ID,
		                c.SITE_ID } into group_c
	                select new { 
		                group_c.Key.PICKING_ID,
		                group_c.Key.DATAAREA_ID,
		                group_c.Key.MAINDEALER_ID,
		                group_c.Key.SITE_ID,
		                total_take = group_c.Count()
	                }
                ) on
	                new { a.PICKING_ID, a.DATAAREA_ID, a.MAINDEALER_ID, a.SITE_ID } equals 
	                new { b.PICKING_ID, b.DATAAREA_ID, b.MAINDEALER_ID, b.SITE_ID } into join_b_a
                from c in join_b_a
                join d in (
                    from cc in Context.MPMWMS_PICKING_LINEs
	                group cc by new { 
		                cc.PICKING_ID,
		                cc.DATAAREA_ID,
		                cc.MAINDEALER_ID,
		                cc.SITE_ID } into group_cc
	                select new { 
		                group_cc.Key.PICKING_ID,
		                group_cc.Key.DATAAREA_ID,
		                group_cc.Key.MAINDEALER_ID,
		                group_cc.Key.SITE_ID,
		                total_line = group_cc.Count()
	                }
                ) on
	                new { a.PICKING_ID, a.DATAAREA_ID, a.MAINDEALER_ID, a.SITE_ID } equals 
	                new { d.PICKING_ID, d.DATAAREA_ID, d.MAINDEALER_ID, d.SITE_ID } into join_d_a
                from e in join_d_a                
                where
	                a.DATAAREA_ID == dataarea_id
	                && a.MAINDEALER_ID == maindealer_id
	                && a.SITE_ID == site_id
	                && (a.STATUS == "P" || a.STATUS == "F")
                    && c.total_take == e.total_line
                join s in Context.MPMWMS_SHIPPING_LISTs
                on new { e.PICKING_ID } equals new { s.PICKING_ID }
                into join_e_s
                from pd in join_e_s.DefaultIfEmpty()
                where pd.SHIPPING_LIST_ID == null
                group a by new { a.POLICE_NUMBER, a.DRIVER } into group_police_number
                select new LovTruck ( group_police_number.Key.POLICE_NUMBER, group_police_number.Key.DRIVER );

            return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
