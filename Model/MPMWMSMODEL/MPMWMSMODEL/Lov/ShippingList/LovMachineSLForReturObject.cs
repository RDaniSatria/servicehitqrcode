﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Table.ShippingListLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MPMWMSMODEL.Module;
using System.Data.Linq;

namespace MPMWMSMODEL.Lov.ShippingList
{
    public class LovMachineSLForReturObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LOV_MACHINE_SL> List(String shipping_list_id, String dataarea_id)
        {
            try
            {
                var search =
                    from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_SHIPPING_LIST_LINEs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.SHIPPING_LIST_ID == shipping_list_id
                        && (a.STATUS == "O" || a.STATUS == "C")
                    select new LOV_MACHINE_SL(a.MACHINE_ID, a.FRAME_ID, a.UNIT_TYPE, a.COLOR_TYPE);

                return search.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
