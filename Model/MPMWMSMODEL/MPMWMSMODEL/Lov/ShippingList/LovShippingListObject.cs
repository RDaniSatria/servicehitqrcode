﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.ShippingList
{
    public class LovShippingListObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovShippingList> List(String dataarea_id, String maindealer_id, String site_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovShippingList> lov =
                    from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_SHIPPING_LISTs
                    where
                        a.DATAAREA_ID == dataarea_id
                        && a.MAINDEALER_ID == maindealer_id
                        && a.SITE_ID == site_id
                        && a.STATUS == "O"
                    group a by new { a.SHIPPING_LIST_ID, a.SHIPPING_DATE, a.DATAAREA_ID } into group_sl
                    select new LovShippingList(group_sl.Key.SHIPPING_LIST_ID, group_sl.Key.SHIPPING_DATE, group_sl.Key.DATAAREA_ID);

                return lov.ToList();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
