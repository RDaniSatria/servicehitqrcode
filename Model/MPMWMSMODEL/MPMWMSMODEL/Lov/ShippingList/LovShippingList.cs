﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.ShippingList
{
    public class LovShippingList
    {
        public String SHIPPING_LIST_ID { get; set; }
        public DateTime SHIPPING_LIST_DATE { get; set; }
        public String DATAAREA_ID { get; set; }

        public LovShippingList(String shipping_list_id, DateTime shipping_list_date, String dataarea_id)
        {
            SHIPPING_LIST_ID = shipping_list_id;
            SHIPPING_LIST_DATE = shipping_list_date;
            DATAAREA_ID = dataarea_id;
        }
    }
}
