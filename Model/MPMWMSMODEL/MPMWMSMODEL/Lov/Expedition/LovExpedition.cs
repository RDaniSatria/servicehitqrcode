﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Expedition
{
    public class LovExpedition
    {
        public String EXPEDITION_ID
        {
            get;
            set;
        }

        public String EXPEDITION_NAME
        {
            get;
            set;
        }

        public String DRIVER_NAME
        {
            get;
            set;
        }

        public String POLICE_NUMBER
        {
            get;
            set;
        }

        public Decimal CAPACITY
        {
            get;
            set;
        }

        public LovExpedition(String id, String name)
        {
            EXPEDITION_ID = id;
            EXPEDITION_NAME = name;
        }

        public LovExpedition(String id, String name, String driver, String policeNumber, decimal capacity)
        {
            EXPEDITION_ID = id;
            EXPEDITION_NAME = name;
            DRIVER_NAME = driver;
            CAPACITY = capacity;
            POLICE_NUMBER = policeNumber;
        }
    }
}
