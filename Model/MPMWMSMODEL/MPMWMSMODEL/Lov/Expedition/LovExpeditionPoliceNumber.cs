﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Expedition
{
    public class LovExpeditionPoliceNumber
    {
        public String POLICE_NUMBER
        {
            get;
            set;
        }

        public String DRIVER
        {
            get;
            set;
        }

        public LovExpeditionPoliceNumber(String police_number, String driver)
        {
            POLICE_NUMBER = police_number;
            DRIVER = driver;
        }
    }
}
