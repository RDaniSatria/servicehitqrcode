﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Expedition
{
    public class LovExpeditionObject: IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovExpedition> List(String dataarea_id, String maindealer_id, String site_id)
        {
            IQueryable<LovExpedition> lov = from expedition in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_EXPEDITIONs
                                            where (expedition.DATAAREA_ID == dataarea_id) &&
                                            (expedition.MAINDEALER_ID == maindealer_id) &&
                                            (expedition.SITE_ID == site_id) &&
                                            (expedition.IS_ACTIVE == "Y")
                                            select new LovExpedition(expedition.EXPEDITION_ID, expedition.NAME);
            return lov.ToList();
        }

        public List<string> ListExpeditionLine(String dataarea_id, String maindealer_id, String site_id)
        {
            List<string> lov = (from expedition in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_EXPEDITION_LINEs
                                            where (expedition.DATAAREA_ID == dataarea_id) &&
                                            (expedition.MAINDEALER_ID == maindealer_id) &&
                                            (expedition.SITE_ID == site_id) &&
                                            (expedition.IS_BLACKLIST == "N")
                                            select expedition.EXPEDITION_ID).Distinct().ToList();
            return lov.ToList();
        }

        public List<LovExpedition> ListNoPolisiLine(String expeditionid, String dataarea_id, String maindealer_id, String site_id)
        {
            List<LovExpedition> lov = (from expedition in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_EXPEDITION_LINEs
                                where (expedition.DATAAREA_ID == dataarea_id) &&
                                (expedition.MAINDEALER_ID == maindealer_id) &&
                                (expedition.SITE_ID == site_id) &&
                                (expedition.IS_BLACKLIST == "N") &&
                                (expedition.EXPEDITION_ID == expeditionid)
                                select new LovExpedition(expedition.EXPEDITION_ID, "", expedition.DRIVER, expedition.POLICE_NUMBER, expedition.CAPACITY.Value)).ToList();
            return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
