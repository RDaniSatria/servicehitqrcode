﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Expedition
{
    public class LovExpeditionPoliceNumberObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovExpeditionPoliceNumber> List(String expedition_id, String dataarea_id, String maindealer_id, String site_id)
        {
            IQueryable<LovExpeditionPoliceNumber> lov = from expedition in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_EXPEDITION_LINEs
                                                        where (expedition.DATAAREA_ID == dataarea_id) &&
                                                        (expedition.EXPEDITION_ID == expedition_id) &&
                                                        (expedition.MAINDEALER_ID == maindealer_id) &&
                                                        (expedition.SITE_ID == site_id) &&
                                                        (expedition.IS_BLACKLIST == "N")
                                                        select new LovExpeditionPoliceNumber(expedition.POLICE_NUMBER, expedition.DRIVER);
            return lov.ToList();
        }

        public LovExpeditionPoliceNumber Item(String expedition_id, String dataarea_id, String maindealer_id, String site_id, String police_number)
        {
            IQueryable<LovExpeditionPoliceNumber> lov = from expedition in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_EXPEDITION_LINEs
                                                        where (expedition.DATAAREA_ID == dataarea_id) &&
                                                        (expedition.EXPEDITION_ID == expedition_id) &&
                                                        (expedition.POLICE_NUMBER == police_number) &&
                                                        (expedition.MAINDEALER_ID == maindealer_id) &&
                                            (expedition.SITE_ID == site_id) &&
                                            (expedition.IS_BLACKLIST == "N")
                                                        select new LovExpeditionPoliceNumber(expedition.POLICE_NUMBER, expedition.DRIVER);
            return lov.ToList().FirstOrDefault();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
