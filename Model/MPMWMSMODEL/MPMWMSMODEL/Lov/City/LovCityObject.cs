﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.City
{
    public class LovCityObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }

        public List<LovCity> List(String dataarea_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
                IQueryable<LovCity> lov = from city in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).LOGISTICSADDRESSCOUNTies
                                          select new LovCity(city.NAME, city.RECID);

                return lov.ToList();
            }
        }

    }
}
