﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.City
{
    public class LovCity
    {
        public String CITY_NAME 
        { 
            get;
            set;
        }

        public Int64 CITY_RECID
        {
            get;
            set;
        }

        public LovCity(String name, Int64 recId)
        {
            CITY_NAME = name;
            CITY_RECID = recId;
        }
    }
}
