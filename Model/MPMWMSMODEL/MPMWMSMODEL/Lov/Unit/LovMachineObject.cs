﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovMachineObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();
        
        public List<LovMachine> List(String dataarea_id, String site_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovMachine> lov = from unit in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                                             where (unit.DATAAREA_ID == dataarea_id) &&
                                                (unit.SITE_ID == site_id)
                                             select new LovMachine(unit.MACHINE_ID);
                return lov.Skip(0).Take(99).ToList();
            }
        }

        public List<LovMachine> ListAvailable(String dataarea_id, String site_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovMachine> lov = from unit in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                                             where (unit.DATAAREA_ID == dataarea_id) &&
                                                (unit.SITE_ID == site_id)
                                                && (unit.STATUS != "S")
                                             select new LovMachine(unit.MACHINE_ID);
                return lov.Skip(0).Take(99).ToList();
            }
        }

        public List<LovMachine> ListAvailable(String dataarea_id, String site_id, String warehouse_id, String mask = "")
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovMachine> lov =
                    from unit in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                    where (unit.DATAAREA_ID == dataarea_id)
                    && (unit.WAREHOUSE_ID == warehouse_id)
                    && (unit.SITE_ID == site_id)
                    && (unit.STATUS != "S")
                    && (unit.MACHINE_ID.Contains(mask) || unit.FRAME_ID.Contains(mask))
                    select new LovMachine(unit.MACHINE_ID);

                return lov.ToList(); // lov.Skip(0).Take(99).ToList();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
