﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovColorTypeObject
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovColorType> List(String dataarea_id, String site_id)
        {
            IQueryable<LovColorType> lov =
                from unit in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                where
                    (unit.DATAAREA_ID == dataarea_id)
                    && (unit.SITE_ID == site_id)
                group unit by new { unit.COLOR_TYPE } into g
                orderby g.Key.COLOR_TYPE ascending
                select new LovColorType(g.Key.COLOR_TYPE);
            return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
