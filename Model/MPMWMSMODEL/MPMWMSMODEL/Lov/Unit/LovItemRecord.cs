﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class ItemLovRecord
    {
        [Display(Name = "Item ID")]
        public String ITEMID { get; set; }

        [Display(Name = "Item Name")]
        public String NAME { get; set; }

        public ItemLovRecord()
        {
        }

        public ItemLovRecord(String _itemid, String _name)
        {
            ITEMID = _itemid;
            NAME = _name;
        }
    }
}
