﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovUnitTypeObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovUnitType> List(String dataarea_id, String site_id)
        {
            IQueryable<LovUnitType> lov =
                from unit in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                where
                    (unit.DATAAREA_ID == dataarea_id)
                    && (unit.SITE_ID == site_id)
                group unit by new { unit.UNIT_TYPE } into g
                orderby g.Key.UNIT_TYPE ascending
                select new LovUnitType(g.Key.UNIT_TYPE);
            return lov.ToList(); // lov.Skip(0).Take(99).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
