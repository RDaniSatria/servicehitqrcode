﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovMachine
    {
        public String MACHINE_ID
        {
            get;
            set;
        }
        
        public LovMachine(String machine)
        {
            MACHINE_ID = machine;
        }
    }
}
