﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class ColorLovRecord
    {
        [Display(Name = "Color ID")]
        public String COLOR { get; set; }

        [Display(Name = "Color Name")]
        public String NAME { get; set; }

        public ColorLovRecord()
        {
        }

        public ColorLovRecord(String _color, String _name)
        {
            COLOR = _color;
            NAME = _name;
        }
    }
}
