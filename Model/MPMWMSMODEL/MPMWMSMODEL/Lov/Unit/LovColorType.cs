﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovColorType
    {
        public String COLOR_TYPE { get; set; }

        public LovColorType(String color_type)
        {
            COLOR_TYPE = color_type;
        }
    }
}
