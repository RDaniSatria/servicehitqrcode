﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovItemObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
        
        //public static Func<WmsUnitDataContext, string, IQueryable<ItemLovRecord>>
        //   ItemQuery =
        //       CompiledQuery.Compile((WmsUnitDataContext db, string _dataArea)
        //           =>
        //                from color in db.ECORESCOLORs
        //                join productMaster in db.ECORESPRODUCTMASTERDIMENSIONVALUEs on new { COLOR = color.RECID, PARTITION = color.PARTITION }
        //                                                                        equals new { COLOR = (long)productMaster.COLOR, PARTITION = productMaster.PARTITION }
        //                join product in db.ECORESPRODUCTs on new { PRODUCT = (long)productMaster.COLORPRODUCTMASTER, PARTITION = productMaster.PARTITION }
        //                                            equals new { PRODUCT = product.RECID, PARTITION = product.PARTITION }
        //                where product.XTSCATEGORYCODE == "U"
        //                join invent in db.INVENTTABLEs on new { RECID = product.RECID, PARTITION = product.PARTITION }
        //                                            equals new { RECID = invent.PRODUCT, PARTITION = invent.PARTITION }
        //                where invent.DATAAREAID == _dataArea
        //                    && invent.ITEMID.Length == 3
        //                select new ItemLovRecord(invent.ITEMID, product.SEARCHNAME)
        //   );
        public static Func<WmsUnitDataContext,IQueryable<ItemLovRecord>>
           ItemQuery =
               CompiledQuery.Compile((WmsUnitDataContext db)
                   =>
                         from color in db.MPMSERVVIEWDAFTARUNITWARNAs
                         select new ItemLovRecord(color.ITEMID, color.SEARCHNAME)
           );
        //public List<ItemLovRecord> List(String _dataArea)
        //{
        //    var query = ItemQuery((WmsUnitDataContext)Context, _dataArea).GroupBy(z => new { z.ITEMID, z.NAME })
        //                .Select(group => new ItemLovRecord { ITEMID = group.Key.ITEMID, NAME = group.Key.NAME }).OrderBy(o => o.ITEMID).ToList();

        //    return query;
        //}

        public List<ItemLovRecord> List()
        {
            var query = ItemQuery((WmsUnitDataContext)Context).GroupBy(z => new { z.ITEMID, z.NAME })
                        .Select(group => new ItemLovRecord { ITEMID = group.Key.ITEMID, NAME = group.Key.NAME }).OrderBy(o => o.ITEMID).ToList();

            return query;
        }
    }
}
