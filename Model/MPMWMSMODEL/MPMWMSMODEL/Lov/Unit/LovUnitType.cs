﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovUnitType
    {
        public String UNIT_TYPE { get; set; }
        
        public LovUnitType(String unit_type)
        {
            UNIT_TYPE = unit_type;
        }
    }
}
