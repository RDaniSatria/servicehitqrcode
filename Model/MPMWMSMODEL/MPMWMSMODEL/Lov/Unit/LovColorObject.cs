﻿using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Unit
{
    public class LovColorObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }

        //public static Func<WmsUnitDataContext, string, IQueryable<ColorLovRecord>>
        //   ColorQuery =
        //       CompiledQuery.Compile((WmsUnitDataContext db, string _itemid)
        //           =>
        //                from color in db.ECORESCOLORs
        //                join productMaster in db.ECORESPRODUCTMASTERDIMENSIONVALUEs on new { COLOR = color.RECID, PARTITION = color.PARTITION }
        //                                                                        equals new { COLOR = (long)productMaster.COLOR, PARTITION = productMaster.PARTITION }
        //                join product in db.ECORESPRODUCTs on new { PRODUCT = (long)productMaster.COLORPRODUCTMASTER, PARTITION = productMaster.PARTITION }
        //                                            equals new { PRODUCT = product.RECID, PARTITION = product.PARTITION }
        //                join invent in db.INVENTTABLEs on new { RECID = product.RECID, PARTITION = product.PARTITION }
        //                                            equals new { RECID = invent.PRODUCT, PARTITION = invent.PARTITION }
        //                where invent.ITEMID == _itemid
        //                select new ColorLovRecord(color.NAME, productMaster.DESCRIPTION)
        //   );
        public static Func<WmsUnitDataContext, string, IQueryable<ColorLovRecord>>
           ColorQuery =
               CompiledQuery.Compile((WmsUnitDataContext db, string _itemid)
                   =>
                        from color in db.MPMSERVVIEWDAFTARUNITWARNAs
                        where color.ITEMID == _itemid
                        select new ColorLovRecord(color.ITEMCOLORCODE, color.ITEMCOLOR)
                        
           );

        public List<ColorLovRecord> List(String _itemid)
        {
            var query = ColorQuery((WmsUnitDataContext)Context, _itemid).OrderBy(x => x.COLOR).ToList();

            return query;
        }
    }
}
