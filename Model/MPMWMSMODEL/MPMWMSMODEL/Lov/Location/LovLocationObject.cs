﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Location
{
    public class LovLocationObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovLocation> List(String dataarea_id, String site_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovLocation> lov = from location in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                                              where (location.DATAAREA_ID == dataarea_id) &&
                                              (location.SITE_ID == site_id)
                                              select new LovLocation(location.LOCATION_ID, location.LOCATION_NAME, location.LOCATION_TYPE);
                return lov.ToList();
            }
        }

        public List<LovLocation> List(String dataarea_id, String site_id, String type)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovLocation> lov = from location in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                                              where (location.DATAAREA_ID == dataarea_id) &&
                                              (location.SITE_ID == site_id)
                                              && (location.LOCATION_TYPE == type)
                                              select new LovLocation(location.LOCATION_ID, location.LOCATION_NAME, location.LOCATION_TYPE);
                return lov.ToList();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
