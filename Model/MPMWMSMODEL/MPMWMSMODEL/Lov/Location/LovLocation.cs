﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Location
{
    public class LovLocation
    {
        public String LOCATION_ID
        {
            get;
            set;
        }

        public String LOCATION_NAME
        {
            get;
            set;
        }

        public String LOCATION_TYPE
        {
            get;
            set;
        }

        public LovLocation(String id, String name, String type)
        {
            LOCATION_ID = id;
            LOCATION_NAME = name;
            LOCATION_TYPE = type;
        }
    }
}
