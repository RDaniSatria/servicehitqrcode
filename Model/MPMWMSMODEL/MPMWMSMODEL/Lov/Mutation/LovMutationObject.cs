﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Mutation
{
    public class LovMutationObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovMutation> List(String dataarea_id, String site_id_delivered, String status)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovMutation> lov = from mutation in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_MUTATION_SITEs
                                              where (mutation.DATAAREA_ID == dataarea_id) &&
                                              (mutation.SITE_ID_NEW == site_id_delivered)
                                              select new LovMutation(
                                                  mutation.MUTATION_ID,
                                                  mutation.DATAAREA_ID,
                                                  mutation.SITE_ID,
                                                  mutation.SITE_ID_NEW);
                return lov.ToList();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
