﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Mutation
{
    public class LovMutation
    {
        public String MUTATION_ID
        {
            get;
            set;
        }

        public String DATAAREA_ID
        {
            get;
            set;
        }

        public String SITE_ID
        {
            get;
            set;
        }

        public String SITE_ID_NEW
        {
            get;
            set;
        }

        public LovMutation(String mutation_id, String dataarea_id, String site_id, String site_id_new)
        {
            MUTATION_ID = mutation_id;
            DATAAREA_ID = dataarea_id;
            SITE_ID = site_id;
            SITE_ID_NEW = site_id_new;
        }
    }
}
