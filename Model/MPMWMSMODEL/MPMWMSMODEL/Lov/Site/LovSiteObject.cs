﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.Site
{
    public class LovSiteObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovSite> List(String dataarea_id)
        {
            using (
                var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                IQueryable<LovSite> lov = from site in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).INVENTSITEs
                                          where site.DATAAREAID == dataarea_id
                                          select new LovSite(site.SITEID, site.NAME);

                return lov.ToList();
            }
        }

        //ListGudang Report NRFS Unit
        public List<LovSite> ListGudang()
        {
            using (
               var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
               {
                   IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
               }))
            {
                //var exceptSite = new List<string> { "S01", "S02", "S03", "S04", "S05", "S06", "S07", "S08", "S09", "SD1" };
                IQueryable<LovSite> lovg = from site in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).INVENTSITEs
                                           where !(site.SITEID == "S01" || site.SITEID == "S02" || site.SITEID == "S03" || site.SITEID == "S04"
                                           || site.SITEID == "S05" || site.SITEID == "S06" || site.SITEID == "S07" || site.SITEID == "S08"
                                           || site.SITEID == "S09" || site.SITEID == "SD1"
                                           )
                                           select new LovSite(site.SITEID, site.NAME);


                return lovg.ToList();
            }
        }

        //ListEkspedisi Report NRFS Unit
        public List<LovSite> ListEkspedisi()
        {
            using (
               var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
               {
                   IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
               }))
            {
                IQueryable<LovSite> lovEkp = from eks in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).SALESCARRIERs                                           
                                           select new LovSite(eks.CNPJCPFNUM_BR,eks.CARRIERNAME);


                return lovEkp.ToList();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
