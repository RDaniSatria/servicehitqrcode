﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Site
{
    public class LovSite
    {
        public String SITE_ID 
        {
            get;
            set;
        }

        public String SITE_NAME
        {
            get;
            set;
        }

        public LovSite(String id, String name)
        {
            SITE_ID = id;
            SITE_NAME = name;
        }
    }
}
