﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.ReceivingPart
{
    public class LovReceivingPartLine
    {
        public String JOURNAL_ID { get; set; }
        public String ITEM_ID { get; set; }
        public String FROM_INVENTDIM_ID { get; set; }
        public String TO_INVENTDIM_ID { get; set; }
        public String SITE_ID { get; set; }
        public String LOCATION_ID { get; set; }
        public int QUANTITY { get; set; }
        public int STORED { get; set; }
        public int POSTED { get; set; }
        public String HISTORY { get; set; }

        public LovReceivingPartLine(String journal_id, String item_id, String from_inventdim_id, String to_inventdim_id, 
            String site_id, String location_id, int quantity, int stored, int posted)
        {
            JOURNAL_ID = journal_id;
            FROM_INVENTDIM_ID = from_inventdim_id;
            TO_INVENTDIM_ID = to_inventdim_id;
            ITEM_ID = item_id;
            SITE_ID = site_id;
            LOCATION_ID = location_id;
            QUANTITY = quantity;
            STORED = stored;
            POSTED = posted;
        }
    }
}
