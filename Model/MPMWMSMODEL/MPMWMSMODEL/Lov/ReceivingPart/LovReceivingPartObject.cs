﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Model;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Setting;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.ReceivingPart
{
    public class LovReceivingPartObject : IDisposable
    {
       // System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();
        public WmsUnitDataContext Context = new WmsUnitDataContext();
        //public List<LovReceivingPart> List(String dataarea_id, String maindealer_id, String site_id)
        //{
        //    IQueryable<LovReceivingPart> lov =
        //        from trans in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).xvsMPM_GetSPGNbrs
        //        where (trans.DATAAREAID == dataarea_id)
        //        && (trans.XTSMAINDEALERCODE == maindealer_id)
        //        && (trans.INVENTSITEID == site_id)
        //        && (trans.POSTED_STORE == 0)
        //        && (trans.POSTED_RCV == 1)
        //        && (trans.XTSISSTORED == 0)
        //        && (trans.XTSCATEGORYCODE == "P")
        //        group trans by new { trans.JournalId } into g
        //        orderby g.Key.JournalId
        //        select new LovReceivingPart(g.Key.JournalId);
        //    return lov.ToList();
        //}

        public List<LovReceivingPart> ListNotPosted(String dataarea_id)
        {
            IQueryable<LovReceivingPart> lov =
                from trans in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).xvsMPM_GetSPGNbr_Posteds
                where (trans.DATAAREAID == dataarea_id)
                group trans by new { trans.JournalId } into g
                select new LovReceivingPart(g.Key.JournalId);
            return lov.ToList();
        }

        public List<xvsMPM_GetSPGNbrViewPart> ListRec(String dataarea_id, String maindealer_id, String site_id)
        {

            try
            {

                var result = MASTERJAMQUERYRec(Context, dataarea_id, maindealer_id, site_id);
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
        private static Func<WmsUnitDataContext, IQueryable<MPM_SETTING_DETAIL>>
            QuerySetting =
                CompiledQuery.Compile((WmsUnitDataContext db)
                    =>

           (from a in db.MPM_SETTING_DETAILs
            join h in db.MPM_SETTINGs
            on a.SETTING_ID equals h.SETTING_ID
            where a.SETTING_ID.Equals("STR_PART") && a.KEY_TAG_1.Equals("DATE") && a.KEY_TAG_2.Equals("LIVE") && h.IS_ACTIVE.Equals("Y")
            select a)
           );
        public List<MPM_SETTING_DETAIL> ListSetting()
        {
            ICollection<MPM_SETTING_DETAIL> lov = QuerySetting((WmsUnitDataContext)Context).ToList(); 


            return lov.ToList();
        }


        private static Func<WmsUnitDataContext, string, string, string, IQueryable<xvsMPM_GetSPGNbrViewPart>>
            MASTERJAMQUERYRec =
                CompiledQuery.Compile((WmsUnitDataContext db, string dataarea_id, string maindealer_id, string site_id)
                    =>

           (from a in db.xvsMPM_GetSPGNbrViewParts
            //join b in db.MPMRESERVEQTYSTORINGPARTs
            //on a.ITEMID equals b.PART
            where a.DATAAREAID.Equals(dataarea_id) && a.XTSMAINDEALERCODE.Equals(maindealer_id) && a.INVENTSITEID.Equals(site_id)
            select a)
           );
        private static Func<WmsUnitDataContext, string, string, string, IQueryable<LovReceivingPart>>
             QueryView =
                 CompiledQuery.Compile((WmsUnitDataContext db, string dataarea_id, string maindealer_id, string site_id)
                     =>

            (from a in db.xvsMPM_GetSPGNbrViewParts
             
             where a.DATAAREAID.Equals(dataarea_id) && a.XTSMAINDEALERCODE.Equals(maindealer_id) && a.INVENTSITEID.Equals(site_id)             
             orderby a.TERLAMA ascending
             select new LovReceivingPart
             {
                 ITEMID = a.ITEMID,
                 JournalID = a.JOURNALID,
                 TERLAMA = a.TERLAMA,
                 Qty = a.QTY * -1,
                 XTSPLATENO_ID = a.XTSPLATENO,
                 Sisa = a.QTY * -1,
                 QtyInDokumen = a.QTYINDOKUMEN,
                 SisaQtyInDokumen = a.SISA,
                 Claim = a.CLAIM,
                 QtyIn = a.QTYIN,
                 QtyPembanding = a.QTYPEMBANDING,
                 ToInventDimid = a.TOINVENTDIMID,
                 StatusPosting = a.STATUSPOSTING

             })
            );

        public List<LovReceivingPart> ListSPGRec(String dataarea_id, String maindealer_id, String site_id)
        {

            try
            {
                //SettingDetailObject _SettingDetailObject = new SettingDetailObject();
                var item = ListSetting();
                string a = item.FirstOrDefault().TAG_1.ToString();
                string date = "10/19/2017";
                var result = QueryView(Context, dataarea_id, maindealer_id, site_id).Where(c => c.TERLAMA >= Convert.ToDateTime(a)).ToList();

                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<MPM_POLICY_MENU> LevelUserParts(String user_id)
        {
            ICollection<MPM_POLICY_MENU> lov = ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPM_POLICY_MENUs
                .Where(c => c.USER_ID == user_id).Take(1).ToList();


            return lov.ToList();
        }

        public List<LovReceivingPartLine> List(String dataarea_id, String journal_id)
        {
            IQueryable<LovReceivingPartLine> lov =
                        from trans in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).xvsMPM_GetSPGNbrs
                        where (trans.DATAAREAID == dataarea_id) &&
                            (trans.JournalId == journal_id) &&
                            (trans.POSTED_STORE == 0) &&
                            (trans.POSTED_RCV == 1) &&
                            (trans.XTSISSTORED == 0) &&
                            (trans.XTSCATEGORYCODE == "P")
                        select new LovReceivingPartLine(
                            trans.JournalId,
                            trans.ItemId,
                            trans.inventdimid,
                            trans.TOINVENTDIMID,
                            trans.INVENTSITEID,
                            trans.INVENTLOCATIONID,
                            Convert.ToInt32(trans.Qty),
                            Convert.ToInt32(trans.XTSISSTORED),
                            Convert.ToInt32(trans.POSTED_STORE)
                        );
                    return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
