﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.ReceivingPart
{
    public class LovReceivingPart
    {
        public String USER_ID { get; set; }
        public int ROLE_ADMIN { get; set; }
        public int ROLE_STAFF { get; set; }
        public String XTSPLATENO_ID { get; set; }
        public decimal? Qty { get; set; }
        public String ITEMID { get; set; }
        public decimal? Sisa { get; set; }
        public String JOURNAL_ID { get; set; }
        public String JournalID { get; set; }
        public int? QtyInDokumen { get; set; }
        public int? SisaQtyInDokumen { get; set; }
        public int? Claim { get; set; }
        public int? QtyIn { get; set; }
        public int? QtyPembanding { get; set; }
        public string ToInventDimid { get; set; }
        public string XTSMAINDEALERCODE { get; set; }
        public string DATAAREAID { get; set; }
        public DateTime TERLAMA { get; set; }
        public int? StatusPosting { get; set; }
        public LovReceivingPart() { }
        public LovReceivingPart(String journal_id)
        {
            JOURNAL_ID = journal_id;
        }
    }
}
