﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Lov.DataArea
{
    public class LovDataAreaObject: IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsMPMITModuleDataContext();

        public List<LovDataArea> List()
        {
            IQueryable<LovDataArea> lov = from data in ((MPMWMSMODEL.Module.WmsMPMITModuleDataContext)Context).DATAAREAs
                                          where data.ID == "MML" || data.ID == "DAT"
                                            select new LovDataArea(data.ID.ToLower(), data.NAME);
            return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
