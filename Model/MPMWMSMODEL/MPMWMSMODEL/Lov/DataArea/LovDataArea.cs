﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.DataArea
{
    public class LovDataArea
    {
        public String DATAAREA_ID
        {
            get;
            set;
        }

        public String DATAAREA_NAME
        {
            get;
            set;
        }

        public LovDataArea(String id, String name)
        {
            DATAAREA_ID = id;
            DATAAREA_NAME = name;
        }
    }
}
