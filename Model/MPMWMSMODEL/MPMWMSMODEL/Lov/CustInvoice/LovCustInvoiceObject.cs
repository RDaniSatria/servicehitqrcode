﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.View.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace MPMWMSMODEL.Lov.CustInvoice
{    
    public class LovCustInvoiceObject: IDisposable
    {
        MPMWMSMODEL.Module.WmsUnitDataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();
        public List<string> ListCustInvoiceJour(string dealercode, string dataareaid, string maindealerid, string siteid)
        {
            try
            {
                using (
                    var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    var itemsObj =
                        from custinv in Context.CUSTINVOICEJOURs
                        join avpic in Context.MPMWMS_DO_AVPICKs on custinv.INVOICEID equals avpic.DO_ID
                        where custinv.INVOICEACCOUNT.Equals(dealercode)
                        && avpic.DATAAREA_ID.Equals(dataareaid)
                        && avpic.MAINDEALER_ID.Equals(maindealerid)
                        && avpic.SITE_ID.Equals(siteid)
                        && avpic.QUANTITY_DO != avpic.QUANTITY_SL
                        select avpic.DO_ID;
                    return itemsObj.ToList();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
