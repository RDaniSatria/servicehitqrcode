﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Warehouse
{
    public class LovWarehouse
    {
        public String WAREHOUSE_ID
        {
            get;
            set;
        }

        public String WAREHOUSE_NAME
        {
            get;
            set;
        }

        public LovWarehouse(String id, String name)
        {
            WAREHOUSE_ID = id;
            WAREHOUSE_NAME = name;
        }
    }
}
