﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Lov.Warehouse
{
    public class LovWarehouseObject : IDisposable
    {
        System.Data.Linq.DataContext Context = new MPMWMSMODEL.Module.WmsUnitDataContext();

        public List<LovWarehouse> List(String dataarea_id, String site_id)
        {
            IQueryable<LovWarehouse> lov = from location in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).INVENTLOCATIONs
                                          where (location.DATAAREAID == dataarea_id) &&
                                          (location.INVENTSITEID == site_id)
                                           select new LovWarehouse(location.INVENTLOCATIONID, location.NAME);
            return lov.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
