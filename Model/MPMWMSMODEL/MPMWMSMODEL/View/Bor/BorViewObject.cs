﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.View.Bom;

namespace MPMWMSMODEL.View.Bor
{
    public class BorViewObject : MPMDbObject<WmsUnitDataContext>
    {
        public BorViewObject()
            : base()
        {
        }

        public List<BOM_BOR_TYPE_COLOR_RECORD> List(String dataarea_id, String maindealer_id, String site_id, int new_type_status = 1)
        {
            try
            {
                IQueryable<BOM_BOR_TYPE_COLOR_RECORD> itemsBOMObj =
                    from bom in Context.XVS_BOR_VIEWs
                    where
                        bom.BOMLINE == Convert.ToString(new_type_status)
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                    group bom by new { bom.ITEMID, bom.INVENTCOLORID, bom.INVENTSITEID, bom.INVENTLOCATIONID } into bom_group
                    select new BOM_BOR_TYPE_COLOR_RECORD(
                        bom_group.Key.ITEMID,
                        bom_group.Key.INVENTCOLORID,
                        bom_group.Key.INVENTSITEID,
                        bom_group.Key.INVENTLOCATIONID,
                        "BOR",
                        Convert.ToInt32(bom_group.Sum(x => x.QTY)),
                        Convert.ToInt32(bom_group.Sum(x => x.XTSBOMQTYPDT))
                    );

                return itemsBOMObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public BOM_BOR_TYPE_COLOR_RECORD Item(String dataarea_id, String maindealer_id, String site_id,
            String typecolor, int new_type_status = 1)
        {
            try
            {
                List<BOM_BOR_TYPE_COLOR_RECORD> list = List(dataarea_id, maindealer_id, site_id, new_type_status);
                return list.Where(x => x._TYPE + x._COLOR == typecolor).Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOR_VIEW Item(String dataarea_id, String maindealer_id, String site_id,
            String type, String color, int new_type_status = 1)
        {
            try
            {
                IQueryable<XVS_BOR_VIEW> itemsObj =
                    from bom in Context.XVS_BOR_VIEWs
                    where
                        bom.BOMLINE == Convert.ToString(new_type_status)
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.ITEMID == type
                        && bom.INVENTCOLORID == color
                        && bom.XTSBOMQTYPDT != bom.QTY
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOR_VIEW ItemNewBundle(String dataarea_id, String maindealer_id, String site_id,
            String inventtransid)
        {
            try
            {
                IQueryable<XVS_BOR_VIEW> itemsObj =
                    from bom in Context.XVS_BOR_VIEWs
                    where
                        bom.BOMLINE == "0" && 
                        bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.INVENTTRANSIDFATHER == inventtransid
                        //&& bom.INVENTTRANSID == inventtransid
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOR_VIEW ItemOldBundle(String dataarea_id, String maindealer_id, String site_id,
            String journalid)
        {
            try
            {
                IQueryable<XVS_BOR_VIEW> itemsObj =
                    from bom in Context.XVS_BOR_VIEWs
                    where
                        bom.BOMLINE == "0"
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.JOURNALID == journalid
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
