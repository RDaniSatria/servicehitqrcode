﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.View.Customer
{
    public class CustomerViewObject : MPMDbObject<WmsUnitDataContext>
    {
        public CustomerViewObject()
            : base()
        {
        }

        public IQueryable<CustomerRecord> List()
        {
            try
            {
                var itemsObj = from custtable in Context.CUSTTABLEs
                               join dirpartytable in Context.DIRPARTYTABLEs on custtable.PARTY equals dirpartytable.RECID into dir_join
                               from result_dir in dir_join.DefaultIfEmpty()
                               join logisticpostaladdress in Context.LOGISTICSPOSTALADDRESSes on result_dir.PRIMARYADDRESSLOCATION equals logisticpostaladdress.LOCATION into cust_join
                               from result in cust_join.DefaultIfEmpty()
                               where
                               (result.VALIDFROM.Date <= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date) &&
                               (result.VALIDTO.Date >= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date)
                               select new CustomerRecord(
                                    custtable.DATAAREAID,
                                    custtable.ACCOUNTNUM,
                                    result_dir.NAME,
                                    result.ADDRESS,
                                    result.CITY,
                                    result.STATE
                                   );
                return itemsObj;
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public CustomerRecord Item(String dataarea_id, String account_num)
        {
            try
            {
                var itemsObj = from custtable in Context.CUSTTABLEs
                               join dirpartytable in Context.DIRPARTYTABLEs on custtable.PARTY equals dirpartytable.RECID into dir_join
                               from result_dir in dir_join.DefaultIfEmpty()
                               join logisticpostaladdress in Context.LOGISTICSPOSTALADDRESSes on result_dir.PRIMARYADDRESSLOCATION equals logisticpostaladdress.LOCATION into cust_join
                               from result in cust_join.DefaultIfEmpty()
                               where
                               (custtable.DATAAREAID == dataarea_id) &&
                               (custtable.ACCOUNTNUM == account_num) &&
                               (result.VALIDFROM.Date <= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date) &&
                               (result.VALIDTO.Date >= MPMDateUtil.Trunc(MPMDateUtil.DateTime).Date)
                               select new CustomerRecord(
                                    custtable.DATAAREAID,
                                    custtable.ACCOUNTNUM,
                                    result_dir.NAME,
                                    result.ADDRESS,
                                    result.CITY,
                                    result.STATE
                                   );
                var currentItem = itemsObj.Take(1).FirstOrDefault();;
                return currentItem;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
