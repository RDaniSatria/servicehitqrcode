﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.View.Customer
{
    public class CustomerRecord
    {
        public String DATAAREA_ID { get; set; }
        public String ACCOUNTNUM { get; set; }
        public String NAME { get; set; }
        public String ADDRESS { get; set; }
        public String CITY { get; set; }
        public String STATE { get; set; }

        public CustomerRecord(String dataarea_id, String accountnum, String name, String address, String city, String state)
        {
            DATAAREA_ID = dataarea_id;
            ACCOUNTNUM = accountnum;
            NAME = name;
            ADDRESS = address;
            CITY = city;
            STATE = state;
        }
    }
}
