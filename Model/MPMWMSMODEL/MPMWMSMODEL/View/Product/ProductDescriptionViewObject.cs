﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.View.Product
{
    public class ProductDescriptionViewObject : MPMDbObject<WmsUnitDataContext>
    {
        public ProductDescriptionViewObject()
            : base()
        {
        }

        /*
        public MPMVIEW_UNITTYPECOLOR_DESCRIPTION Item(String item_id, String color_id)
        {
            try
            {
                var itemsObj =
                    from product in Context.MPMVIEW_UNITTYPECOLOR_DESCRIPTIONs
                    where
                    (product.type == item_id)
                    && (product.color == color_id)
                    select product;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        */
        public MPMSERVVIEWDAFTARUNITWARNA ItemV365(String item_id, String color_id)
        {
            try
            {
                var itemsObj =
                    from product in Context.MPMSERVVIEWDAFTARUNITWARNAs
                    where
                    (product.ITEMID == item_id)
                    && (product.ITEMCOLORCODE == color_id)
                    select product;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
