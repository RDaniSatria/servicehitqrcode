﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.View.Product
{
    public class ProductRecord
    {
        public String PRODUCT_ID { get; set; }
        public String SITE_ID { get; set; }
        public String LOCATION_ID { get; set; }
        public int QUANTITY { get; set; }
        public DateTime POSTEDDATETIME { get; set; }
        public String INVENTDIMID { get; set; }

        public ProductRecord(String product_id, String site_id, String location_id, int quantity, DateTime posteddatetime, String inventdimid)
        {
            PRODUCT_ID = product_id;
            SITE_ID = site_id;
            LOCATION_ID = location_id;
            QUANTITY = quantity;
            POSTEDDATETIME = posteddatetime;
            INVENTDIMID = inventdimid;
        }
        public ProductRecord(String product_id, String site_id, String location_id, int quantity, String inventdimid)
        {
            PRODUCT_ID = product_id;
            SITE_ID = site_id;
            LOCATION_ID = location_id;
            QUANTITY = quantity;
            INVENTDIMID = inventdimid;
        }
        public ProductRecord(String product_id, String site_id, String location_id, String inventdimid)
        {
            PRODUCT_ID = product_id;
            SITE_ID = site_id;
            LOCATION_ID = location_id;
            INVENTDIMID = inventdimid;
        }
    }
}
