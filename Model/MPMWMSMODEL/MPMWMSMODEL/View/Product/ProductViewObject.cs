﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.View.Product
{
    public class ProductViewObject : MPMDbObject<WmsUnitDataContext>
    {
        public ProductViewObject()
            : base()
        {
        }

        public List<ProductRecord> List(String dataarea_id, String site_id = "", String category_id = "", String item_id = "")
        {
            try
            {
                var itemsObj =
                    from product in Context.vMPM_PRODUCTs
                    join jurnal in Context.INVENTJOURNALTRANs
                    on new { product.ITEMID, product.DATAAREAID } equals new { jurnal.ITEMID, jurnal.DATAAREAID }
                    join inventdim in Context.INVENTDIMs
                    on new { product.DATAAREAID, product.INVENTDIMID } equals new { inventdim.DATAAREAID,inventdim.INVENTDIMID}
                    where
                    (product.INVENTLOCATIONID != "")
                    && (product.DATAAREAID == dataarea_id)
                    && (product.INVENTSITEID == site_id)
                    && (product.XTSCATEGORYCODE == category_id)
                    && (product.ITEMID == item_id)
                    && !product.INVENTLOCATIONID.Contains("RCV")
                    && !product.INVENTLOCATIONID.Contains("ITV")
                    && !product.INVENTLOCATIONID.Contains("HTL01")
                    // orderby jurnal.POSTEDDATETIME descending 
                    select new ProductRecord(
                            product.ITEMID,
                            product.INVENTSITEID,
                            product.INVENTLOCATIONID,
                            Convert.ToInt32(product.AVAILPHYSICAL),
                            jurnal.TRANSDATE,
                            product.INVENTDIMID

                        );
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        public List<ProductRecord> ListIDimID( String Lokasiid = "")
        {
            try
            {
                var itemsObj =
                    from product in Context.INVENTDIMs
                    where (product.INVENTSITEID != "" )
                    && product.INVENTLOCATIONID == Lokasiid
                    select new ProductRecord(
                            product.DATAAREAID,
                            product.INVENTSITEID,
                            product.INVENTLOCATIONID,
                            product.INVENTDIMID
                        );
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<ProductRecord> ListHistory(String dataarea_id, String site_id = "", String category_id = "", String item_id = "")
        {
            try
            {
                var itemsObj =
                    from product in Context.vMPM_PRODUCTs
                    join jurnal in Context.INVENTJOURNALTABLEs
                    on product.INVENTLOCATIONID equals jurnal.INVENTLOCATIONID
                    where
                    (product.DATAAREAID == dataarea_id)
                    && (product.INVENTSITEID == site_id)
                    && (product.XTSCATEGORYCODE == category_id)
                    && (product.ITEMID == item_id)
                    && (product.XTSHOTLINEWAREHOUSE == 0)
                    && (product.XTSPICKLIST == 1)
                    select new ProductRecord(
                            product.DATAAREAID,
                            product.INVENTSITEID,
                            product.INVENTLOCATIONID,

                            Convert.ToInt32(product.AVAILPHYSICAL),
                            jurnal.POSTEDDATETIME,
                            product.INVENTDIMID
                        );
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public ProductRecord Item(String dataarea_id, String site_id = "", String category_id = "", String item_id = "")
        {
            try
            {
                var itemsObj =
                    from product in Context.vMPM_PRODUCTs
                    join jurnal in Context.INVENTJOURNALTABLEs
                     on product.INVENTLOCATIONID equals jurnal.INVENTLOCATIONID
                    where
                    (product.DATAAREAID == dataarea_id)
                    && (product.INVENTSITEID == site_id)
                    && (product.XTSCATEGORYCODE == category_id)
                    && (product.ITEMID == item_id)
                    select new ProductRecord(
                            product.DATAAREAID,
                            product.INVENTSITEID,
                            product.INVENTLOCATIONID,
                            Convert.ToInt32(product.AVAILPHYSICAL),
                            jurnal.POSTEDDATETIME,
                           product.INVENTDIMID
                        );
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public ProductRecord Item(String dataarea_id, String site_id = "", String location_id = "",
            String category_id = "", String item_id = "")
        {
            try
            {
                var itemsObj =
                    from product in Context.vMPM_PRODUCTs
                    join jurnal in Context.INVENTJOURNALTABLEs
                     on product.INVENTLOCATIONID equals jurnal.INVENTLOCATIONID
                    where
                    (product.DATAAREAID == dataarea_id)
                    && (product.INVENTSITEID == site_id)
                    && (product.INVENTLOCATIONID == location_id)
                    && (product.XTSCATEGORYCODE == category_id)
                    && (product.ITEMID == item_id)
                    select new ProductRecord(
                            product.DATAAREAID,
                            product.INVENTSITEID,
                            product.INVENTLOCATIONID,
                            Convert.ToInt32(product.AVAILPHYSICAL),
                            jurnal.POSTEDDATETIME,
                           product.INVENTDIMID
                        );
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
