﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.View.Bom
{
    public class BOM_BOR_TYPE_COLOR_RECORD
    {
        public String _TYPE { get; set; }
        public String _COLOR { get; set; }
        public String _SITE_ID { get; set; }
        public String _WAREHOUSE_ID { get; set; }
        public int _QTY { get; set; }
        public int _QTY_PDT { get; set; }
        public String _JOURNAL_ID { get; set; }
        public String _TRANSFER_ID_PARENT { get; set; }
        public String _TRANSFER_ID_CHILD { get; set; }
        public String _BOM_OR_BOR { get; set; }

        public BOM_BOR_TYPE_COLOR_RECORD()
        {
        }

        public BOM_BOR_TYPE_COLOR_RECORD(String type, String color, String bom_or_bor)
        {
            _TYPE = type;
            _COLOR = color;
            _QTY = 1;
            _QTY_PDT = 0;
            _BOM_OR_BOR = bom_or_bor;
        }

        public BOM_BOR_TYPE_COLOR_RECORD(String type, String color, String site_id, String warehouse_id, String bom_or_bor, int qty, int qty_pdt)
        {
            _TYPE = type;
            _COLOR = color;
            _SITE_ID = site_id;
            _WAREHOUSE_ID = warehouse_id;
            _QTY = qty;
            _QTY_PDT = qty_pdt;
            _BOM_OR_BOR = bom_or_bor;
        }

        public BOM_BOR_TYPE_COLOR_RECORD(String type, String color, String transfer_id_parent, String bom_or_bor, int qty, int qty_pdt)
        {
            _TYPE = type;
            _COLOR = color;
            _QTY = qty;
            _TRANSFER_ID_PARENT = transfer_id_parent;
            _QTY_PDT = qty_pdt;
            _BOM_OR_BOR = bom_or_bor;
        }
    }

    public class BomViewObject : MPMDbObject<WmsUnitDataContext>
    {
        public BomViewObject()
            : base()
        {
        }

        public List<BOM_BOR_TYPE_COLOR_RECORD> List(String dataarea_id, String maindealer_id, String site_id, int new_type_status = 1)
        {
            try
            {
                IQueryable<BOM_BOR_TYPE_COLOR_RECORD> itemsBOMObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == new_type_status
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                    group bom by new { bom.ITEMID, bom.INVENTCOLORID, bom.INVENTSITEID, bom.INVENTLOCATIONID } into bom_group
                    select new BOM_BOR_TYPE_COLOR_RECORD(
                        bom_group.Key.ITEMID, 
                        bom_group.Key.INVENTCOLORID,
                        bom_group.Key.INVENTSITEID,
                        bom_group.Key.INVENTLOCATIONID,
                        "BOM",
                        Convert.ToInt32(bom_group.Sum(x => x.QTY)),
                        Convert.ToInt32(bom_group.Sum(x => x.XTSBOMQTYPDT))
                    );

                return itemsBOMObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public BOM_BOR_TYPE_COLOR_RECORD Item(String dataarea_id, String maindealer_id, String site_id, 
            String typecolor, int new_type_status = 1)
        {
            try
            {
                List<BOM_BOR_TYPE_COLOR_RECORD> list = List(dataarea_id, maindealer_id, site_id, new_type_status);
                return list.Where(x => x._TYPE + x._COLOR == typecolor).Take(1).FirstOrDefault();

                /*IQueryable<BOM_TYPE_COLOR_RECORD> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == new_type_status
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.ITEMID + bom.INVENTCOLORID == typecolor
                    group bom by new { bom.ITEMID, bom.INVENTCOLORID, bom.INVENTSITEID, bom.INVENTLOCATIONID } into bom_group
                    select new BOM_TYPE_COLOR_RECORD(
                        bom_group.Key.ITEMID,
                        bom_group.Key.INVENTCOLORID,
                        bom_group.Key.INVENTSITEID,
                        bom_group.Key.INVENTLOCATIONID,
                        Convert.ToInt32(bom_group.Sum(x => x.QTY)),
                        Convert.ToInt32(bom_group.Sum(x => x.XTSBOMQTYPDT))
                    );
                return itemsObj.Take(1).FirstOrDefault();
                 */
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOM_VIEW Item(String dataarea_id, String maindealer_id, String site_id,
            String type, String color, int new_type_status = 1)
        {
            try
            {
                IQueryable<XVS_BOM_VIEW> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == new_type_status
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.ITEMID == type
                        && bom.INVENTCOLORID == color
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOM_VIEW ItemByJournalBomId(String bom_id, String dataarea_id, String maindealer_id, String site_id,
            String type, String color, int new_type_status = 1)
        {
            try
            {
                IQueryable<XVS_BOM_VIEW> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == new_type_status
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.ITEMID == type
                        && bom.INVENTCOLORID == color
                        && bom.JOURNALID == bom_id
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOM_VIEW ItemNewBundle(String dataarea_id, String maindealer_id, String site_id, 
            String inventtransid)
        {
            try
            {
                IQueryable<XVS_BOM_VIEW> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == 0
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.INVENTTRANSID == inventtransid
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOM_VIEW ItemNewBundleByJournalBomId(String bom_id, String dataarea_id, String maindealer_id, String site_id,
            String inventtransid)
        {
            try
            {
                IQueryable<XVS_BOM_VIEW> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == 0
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.INVENTTRANSID == inventtransid
                        && bom.JOURNALID == bom_id
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public XVS_BOM_VIEW ItemOldBundle(String dataarea_id, String maindealer_id, String site_id,
            String journalid)
        {
            try
            {
                IQueryable<XVS_BOM_VIEW> itemsObj =
                    from bom in Context.XVS_BOM_VIEWs
                    where
                        bom.BOMLINE == 0
                        && bom.XTSBOMFLAG == 0
                        && bom.DATAAREAID == dataarea_id
                        && bom.XTSMAINDEALERCODE == maindealer_id
                        && bom.INVENTSITEID == site_id
                        && bom.JOURNALID == journalid
                    select bom;
                return itemsObj.Take(1).FirstOrDefault();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
