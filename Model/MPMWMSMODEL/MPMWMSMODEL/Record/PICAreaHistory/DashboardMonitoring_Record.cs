﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PICAreaHistory
{
    public class DashboardMonitoring_Record
    {
        [Display(Name = "Kode Dealer")]
        public String DEALERID { get; set; }

        [Display(Name = "Nomor SO")]
        public String SOID { get; set; }

        [Display(Name = "NPK")]
        public String PIC { get; set; }

        [Display(Name = "Waktu")]
        public String PICTIME { get; set; }

        [Display(Name = "Status")]
        public String PICSTATUS { get; set; }

        [Display(Name = "NPK")]
        public String PICTRANSPORTER { get; set; }

        [Display(Name = "Waktu")]
        public String TRANSPORTERTIME { get; set; }

        [Display(Name = "Status")]
        public String TRANSPORTERSTATUS { get; set; }

        [Display(Name = "NPK")]
        public String PICCHECKER { get; set; }

        [Display(Name = "Waktu")]
        public String CHECKERTIME { get; set; }

        [Display(Name = "Status")]
        public String CHECKERSTATUS { get; set; }


        public DashboardMonitoring_Record()
        {
        }

        public DashboardMonitoring_Record
            (
                  String _dealerid
                , String _soid
                , String _pic
                , String _pictime
                , String _picstatus
                , String _pictransporter
                , String _transportertime
                , String _transporterstatus
                , String _picchecker
                , String _checkertime
                , String _checkerstatus
            )
        {
            DEALERID = _dealerid;
            SOID = _soid;
            PIC = _pic;
            PICTIME = _pictime;
            PICSTATUS = _picstatus;
            PICTRANSPORTER = _pictransporter;
            TRANSPORTERTIME = _transportertime;
            TRANSPORTERSTATUS = _transporterstatus;
            PICCHECKER = _picchecker;
            CHECKERTIME = _checkertime;
            CHECKERSTATUS = _checkerstatus;
        }
    }
}
