﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record
{
    public class MPMWMS_UNIT_Record
    {
        public String MACHINE_ID { get; set; }
        public String FRAME_ID { get; set; }
        public String UNIT_TYPE { get; set; }
        public String UNIT_DESC { get; set; }
        public String COLOR_TYPE { get; set; }
        public String UNIT_YEAR { get; set; }
        public String SITE_ID { get; set; }
        public String WAREHOUSE_ID { get; set; }
        public String LOCATION_ID { get; set; }
        public String SL_ID { get; set; }
        public String SL_DATE { get; set; }
        public String SPG_ID { get; set; }
        public String SPG_DATE { get; set; }
        public String SPG_POLICE_NUMBER { get; set; }
        public String SPG_DRIVER { get; set; }
        public String STORING_DATE { get; set; }
        public String DO_ID { get; set; }
        public String DO_DATE { get; set; }
        public String PICKING_ID { get; set; }
        public String PICKING_DATE { get; set; }
        public String SHIPPING_LIST_ID { get; set; }
        public String SHIPPING_LIST_DATE { get; set; }
        public String SHIPPING_POLICE_NUMBER { get; set; }
        public String SHIPPING_DRIVER { get; set; }
        public String DEALER_CODE { get; set; }
        public String DEALER_NAME { get; set; }
        public String MUTATION_ID { get; set; }
        public String EXPEDITION_ID { get; set; }
        public String POLICE_NUMBER { get; set; }
        public String DRIVER { get; set; }
    }
}
