﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PackingPart
{
    public class LIST_PART_Record
    {
        public String ITEMID { get; set; }
        public String ITEMNAME { get; set; }
        public String BOXID { get; set; }
        public String SOID { get; set; }
        public int QTY { get; set; }
        public int JOBID { get; set; }

        public LIST_PART_Record()
        {
        }

        public LIST_PART_Record
            (
                  String _itemId
                , String _name
                , String _boxId
                , int _qty
                , String _soId
                , int _jobId
            )
        {
            ITEMID = _itemId;
            ITEMNAME = _name;
            BOXID = _boxId;
            QTY = _qty;
            SOID = _soId;
            JOBID = _jobId;
        }
    }
}
