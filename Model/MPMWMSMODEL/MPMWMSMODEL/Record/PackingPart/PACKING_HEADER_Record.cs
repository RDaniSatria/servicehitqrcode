﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PackingPart
{
    public class PACKING_INFO_Record
    {
        public int JOBID { get; set; }
        public String DEALERID { get; set; }
        public String DEALERNAME { get; set; }
        public int TOTALSO { get; set; }
        public int TOTALPART { get; set; }
        public String ITEMID { get; set; }
        public String ITEMNAME { get; set; }
        public String SALESID { get; set; }
        public String PACKINGSLIPID { get; set; }
        public String RUTEID { get; set; }
        public String KOTA { get; set; }

        public PACKING_INFO_Record()
        {
        }

        public PACKING_INFO_Record
            (
                  int _jobId
                , String _dealerId
                , String _dealerName
                , int _totalSO
                , int _totalPart
                , String _salesId
                , String _itemId
                , String _itemName
                , String _packingId
                , String _rute
                , String _kota
            )
        {
            JOBID = _jobId;
            DEALERID = _dealerId;
            DEALERNAME = _dealerName;
            TOTALSO = _totalSO;
            TOTALPART = _totalPart;
            SALESID = _salesId;
            ITEMID = _itemId;
            ITEMNAME = _itemName;
            PACKINGSLIPID = _packingId;
            RUTEID = _rute;
            KOTA = _kota;
        }
    }
}
