﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PackingPart
{
    public class PICKING_LIST_Record
    {
        //JOBID SALESID ITEMID NAME    INVENTLOCATIONID QTY XTSTROLLEYNBR

        public String JOBID { get; set; }
        public String SALESID { get; set; }
        public String ITEMID { get; set; }
        public String NAME { get; set; }
        public String INVENTLOCATIONID { get; set; }
        public int QTY { get; set; }
        public String XTSTROLLEYNBR { get; set; }

        public PICKING_LIST_Record()
        {
        }

        public PICKING_LIST_Record
            (
                  String _jobId
                , String _salesId
                , String _itemId
                , String _name
                , String _location
                , int _qty
                , String _trolley
            )
        {
            JOBID = _jobId;
            SALESID = _salesId;
            ITEMID = _itemId;
            NAME = _name;
            INVENTLOCATIONID = _location;
            QTY = _qty;
            XTSTROLLEYNBR = _trolley;
        }
    }
}
