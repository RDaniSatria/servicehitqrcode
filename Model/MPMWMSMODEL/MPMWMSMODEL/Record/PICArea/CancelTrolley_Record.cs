﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PICArea
{
    public class CancelTrolley_Record
    {
        [Display(Name = "Trolley Number")]
        public String TROLLEYNBR { get; set; }

        [Display(Name = "Job ID")]
        public int JOBID { get; set; }

        [Display(Name = "Status Transporter")]
        public String STATUSTRANSPORTER { get; set; }

        public CancelTrolley_Record()
        {
        }

        public CancelTrolley_Record
            (
                  String _trolleyNbr
                , int _jobId
                , String _statusTransporter
            )
        {
            TROLLEYNBR = _trolleyNbr;
            JOBID = _jobId;
            STATUSTRANSPORTER = _statusTransporter;
        }
    }
}
