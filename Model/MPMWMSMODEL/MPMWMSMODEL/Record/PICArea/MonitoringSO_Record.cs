﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PICArea
{
    public class MonitoringSO_Record
    {
        [Display(Name = "Total Sales Order")]
        public int SO_TOTAL { get; set; }

        [Display(Name = "Sales Order Done")]
        public int SO_DONE { get; set; }

        [Display(Name = "Sales Order Not Done")]
        public int SO_NOTDONE { get; set; }

        public MonitoringSO_Record()
        {
        }

        public MonitoringSO_Record
            (
                  int _soNotDone
                , int _soDone
                , int _soTotal
            )
        {
            SO_TOTAL = _soTotal;
            SO_DONE = _soDone;
            SO_NOTDONE = _soNotDone;
        }
    }
}
