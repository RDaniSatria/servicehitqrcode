﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PICArea
{
    public class MonitoringTargetActual_Record
    {
        [Display(Name = "PIC")]
        public String NPK { get; set; }

        [Display(Name = "Name")]
        public String USER_NAME { get; set; }

        [Display(Name = "Position")]
        public String POSITION { get; set; }

        [Display(Name = "Target Total SO")]
        public int TARGETSO { get; set; }

        [Display(Name = "Actual Total SO")]
        public int ACTUALSO { get; set; }

        [Display(Name = "Target Total Line")]
        public int TARGETLINESO { get; set; }

        [Display(Name = "Actual Total Line")]
        public int ACTUALLINESO { get; set; }

        [Display(Name = "Target Total Qty")]
        public int TARGETQTYSO { get; set; }

        [Display(Name = "Actual Total Qty")]
        public int ACTUALQTYSO { get; set; }

        public MonitoringTargetActual_Record()
        {
        }

        public MonitoringTargetActual_Record
            (
                  String _npk
                , String _name
                , String _position
                , int _targetso
                , int _actualso
                , int _targetlineso
                , int _actuallineso
                , int _targetqtyso
                , int _actualqtyso
            )
        {
            NPK	= _npk;
            USER_NAME = _name;
            POSITION = _position;
            TARGETSO = _targetso;
            ACTUALSO =_actualso;
            TARGETLINESO = _targetlineso;
            ACTUALLINESO = _actuallineso;
            TARGETQTYSO = _targetqtyso;
            ACTUALQTYSO = _actualqtyso;
        }
    }
}
