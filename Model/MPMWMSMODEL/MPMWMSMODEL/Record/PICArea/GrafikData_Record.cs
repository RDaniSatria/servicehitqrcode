﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.PICArea
{
    public class GrafikData_Record
    {
        [Display(Name = "Kategori")]
        public String KATEGORI { get; set; }

        [Display(Name = "Value")]
        public int VALUE { get; set; } 

        [Display(Name = "Value MTD")]
        public int VALUEMTD { get; set; }

        public GrafikData_Record()
        {
        }

        public GrafikData_Record
            (
                  String  _kategori
                , int _value
                , int _valueMTD
            )
        {
            KATEGORI = _kategori;
            VALUE = _value;
            VALUEMTD = _valueMTD;
        }
    }
}
