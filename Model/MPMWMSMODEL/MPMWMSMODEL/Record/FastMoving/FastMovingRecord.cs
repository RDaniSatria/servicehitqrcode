﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.FastMoving
{
    public class FastMovingRecord
    {
        [Display(Name = "Unit Type")]
        public String TYPE { get; set; }

        [Display(Name = "Type Name")]
        public String TYPENAME { get; set; }

        [Display(Name = "Unit Color")]
        public String COLOR { get; set; }

        [Display(Name = "Color Name")]
        public String COLORNAME { get; set; }

        public FastMovingRecord()
        {
        }

        public FastMovingRecord(String _type, String _typeName, String _color, String _colorName)
        {
            TYPE = _type;
            TYPENAME = _typeName;
            COLOR = _color;
            COLORNAME = _colorName;
        }
    }
}
