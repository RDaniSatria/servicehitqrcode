﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.SO
{
    public class DASHBOARD_SO_MONITORING_REC
    {
        public int NUMBER { get; set; } 
        public string NAMA_PO { get; set; }
        public int? SO { get; set; }
        public int? LINES { get; set;}
        public int? PICK { get; set; }
        public int? PACK { get; set; }
        public int? INV { get; set; }
        public int? SJ { get; set; }
        public int? MUAT { get; set;}
        public int? SISA_SO { get; set; }
        public int? SISA_LINES { get; set; }
        public int? SISA_INV { get; set; }
        public int? SISA_SJ { get; set; }
        public int? SISA_MUAT { get; set; }
        public int? SISA_SJ_H_MIN { get; set; }

        public DASHBOARD_SO_MONITORING_REC() { }
        public DASHBOARD_SO_MONITORING_REC(
            string nama_po,
            int? so,
            int? lines,
            int? pick,
            int? pack,
            int? inv,
            int? sj,
            int? muat,
            int? sisa_so,
            int? sisa_lines,
            int? sisa_inv,
            int? sisa_sj,
            int? sisa_muat,
            int? sisa_sj_h_min
            ) 
        {
            switch (nama_po)
            { 
                 case "Hotline":
                    NAMA_PO = "PO Hotline (HTL)";
                    NUMBER = 1;
                    break;
                case "Urgent":
                    NAMA_PO = "PO Urgent";
                    NUMBER = 2;
                    break;
                case "Fix":
                    NAMA_PO = "PO Fix";
                    NUMBER = 3;
                    break;
                case "Regular":
                    NAMA_PO = "PO Regular";
                    NUMBER = 4;
                    break;
                case "Refill":
                    NAMA_PO = "PO Refill";
                    NUMBER = 5;
                    break;
                case "TOTAL":
                    NAMA_PO = "TOTAL";
                    NUMBER = 6;
                    break;
                case "OIL":
                    NAMA_PO = "OIL";
                    NUMBER = 7;
                    break;
                case "BAN":
                    NAMA_PO = "BAN";
                    NUMBER = 8;
                    break;
            }
            SO = so;
            LINES = lines;
            PICK = pick;
            PACK = pack;
            INV = inv;
            SJ = sj;
            MUAT = muat;
            SISA_SO = sisa_so;
            SISA_LINES = sisa_lines;
            SISA_INV = sisa_inv;
            SISA_SJ = sisa_sj;
            SISA_MUAT = sisa_muat;
            SISA_SJ_H_MIN = sisa_sj_h_min;
        }
    }
}
