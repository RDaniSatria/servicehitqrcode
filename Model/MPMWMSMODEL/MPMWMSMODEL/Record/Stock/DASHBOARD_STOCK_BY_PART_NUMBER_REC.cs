﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.Stock
{
    public class DASHBOARD_STOCK_BY_PART_NUMBER_REC
    {
        public string PARTNUMBER { get; set; }
        public string DESCRIPTION { get; set; }
        public string GROUP_PART_NUMBER { get; set; }
        public Decimal? AVERAGE_POD { get; set; }
        public int? POD_MTD { get; set; }
        public Decimal? AVERAGE_SO { get; set; }
        public int? SO_MTD { get; set; }
        public int? STOCK { get; set; }
        public int MIN { get; set; }
        public int MAX { get; set; }
        public int? INTRANSIT_YTD { get; set; }
        public int? PO_FIX_AHM { get; set; }
        public int? PO_REG_AHM { get; set; }
        public int? SUPPLY_AHM { get; set; }
        public Decimal? PERCENT_SUPPLY { get; set; }
        public Decimal? SL { get; set; }
        public Decimal? PERCENT_POD_MTD_VS_AVERAGE_POD { get; set; }
        public DASHBOARD_STOCK_BY_PART_NUMBER_REC() { }
        public DASHBOARD_STOCK_BY_PART_NUMBER_REC(
            string partnumber,
            string description,
            string grouppartnumber,
            decimal? averagepod, 
            int? podmtd,
            decimal? averageso,
            decimal? somtd,
            decimal? stock,
            decimal? min,
            decimal? max,
            decimal? intransitytd,
            decimal? pofixahm,
            decimal? poregahm,
            decimal? supplyahm
            ) 
        {
            PARTNUMBER = partnumber;
            DESCRIPTION = description;
            GROUP_PART_NUMBER = grouppartnumber;
            AVERAGE_POD = averagepod;
            POD_MTD = podmtd;
            AVERAGE_SO = averageso;
            SO_MTD = somtd!=null? (int)somtd : 0;
            STOCK = stock != null ? (int)stock : 0;
            MIN = min != null ? (int)min : 0;
            MAX = max != null ? (int)max : 0;
            INTRANSIT_YTD = intransitytd != null ? (int)intransitytd : 0;
            PO_FIX_AHM = pofixahm != null ? (int)pofixahm : 0;
            PO_REG_AHM = poregahm != null ? (int)poregahm : 0;
            SUPPLY_AHM = supplyahm != null ? (int)supplyahm : 0; 
            if ((pofixahm + poregahm) != 0)
            {
                PERCENT_SUPPLY = (supplyahm / (pofixahm + poregahm)) * 100;
            }
            if (averageso != 0)
            {
                SL = stock / averageso;
            }
            if (averagepod != 0)
            {
                PERCENT_POD_MTD_VS_AVERAGE_POD = (POD_MTD / averagepod) * 100;
            }
        }
    }        
}
