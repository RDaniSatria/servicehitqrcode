﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.EADUnit
{
    public class ConstantaTypeRecord
    {
        [Display(Name = "Unit Type")]
        public String TYPE { get; set; }

        [Display(Name = "Type Name")]
        public String TYPENAME { get; set; }

        [Display(Name = "Constanta Value")]
        public Decimal VALUE { get; set; }

        [Display(Name = "Description")]
        public String DESCRIPTION { get; set; }


        public ConstantaTypeRecord()
        {
        }
        
        public ConstantaTypeRecord(String type, String typeName, String description, Decimal constanta)
        {
            TYPE = type;
            TYPENAME = typeName;
            DESCRIPTION = description;
            VALUE = constanta;
        }
    }

    public class RouteMulti_REC
    {
        public int ID { get; set; }

        //[Display(Name = "Unit Type")]
        public String ROUTEID { get; set; }

        //[Display(Name = "Type Name")]
        public String ROUTENAME { get; set; }

        //[Display(Name = "Constanta Value")]
        public string MULTIROUTE { get; set; }

        //[Display(Name = "Description")]
        public int? NUMBER { get; set; }

        public int NUMBERURUT { get; set; }

    }
}
