﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.Box
{
    public class MonitoringBox_Record
    {
        [Display(Name = "Nomor")]
        public int NOMOR { get; set; }

        [Display(Name = "PIC")]
        public String NPK { get; set; }

        [Display(Name = "PIC Name")]
        public String NAMA { get; set; }

        [Display(Name = "Box ID")]
        public String BOXID { get; set; }

        [Display(Name = "Dimension")]
        public decimal DIMENSION { get; set; }

        [Display(Name = "Weight")]
        public decimal WEIGHT { get; set; }

        [Display(Name = "Dealer")]
        public String DEALER { get; set; }

        [Display(Name = "Sales ID")]
        public String SALESID { get; set; }

        [Display(Name = "Packing ID")]
        public String PACKINGID { get; set; }

        [Display(Name = "Part ID")]
        public String ITEMID { get; set; }

        [Display(Name = "Qty")]
        public int QTY { get; set; }


        public MonitoringBox_Record()
        {
        }

        public MonitoringBox_Record
            (
                int _nomor,
                String _npk,
                String _nama,
                String _boxid,
                decimal _dimension,
                decimal _weight,
                String _dealer,
                String _salesid,
                String _packingid,
                String _itemid,
                int _qty
            )
        {
            NOMOR = _nomor;
            NPK = _npk;
            NAMA = _nama;
            BOXID = _boxid;
            DIMENSION = _dimension;
            WEIGHT = _weight;
            DEALER = _dealer;
            SALESID = _salesid;
            PACKINGID = _packingid;
            ITEMID = _itemid;
            QTY = _qty;
        }

    }
}
