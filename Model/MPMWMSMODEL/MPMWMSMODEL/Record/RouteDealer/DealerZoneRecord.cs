﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.RouteDealer
{
    public class DealerZoneRecord
    {
        [Display(Name = "DEALER CODE")]
        public String DEALERCODE { get; set; }

        [Display(Name = "DEALER NAME")]
        public String DEALERNAME { get; set; }

        [Display(Name = "CITY")]
        public String CITY { get; set; }

        [Display(Name = "ZONE CODE")]
        public String ZONECODE { get; set; }

        [Display(Name = "ZONE NAME")]
        public String ZONENAME { get; set; }

        [Display(Name = "ONGKOS ANGKUT")]
        public Decimal ONGKOSANGKUT { get; set; }

        [Display(Name = "DISTANCE")]
        public Decimal DISTANCE { get; set; }

        public DealerZoneRecord() { }

        public DealerZoneRecord(String _dealerCode, String _dealerName, String _city, String _zoneCode, String _zoneName, Decimal _ongkosAngkut, Decimal _distance)
        {
            DEALERCODE = _dealerCode;
            DEALERNAME = _dealerName;
            CITY = _city;
            ZONECODE = _zoneCode;
            ZONENAME = _zoneName;
            ONGKOSANGKUT = _ongkosAngkut;
            DISTANCE = _distance;

        }
    }
}
