﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.RouteDealer
{
    public class RouteZoneRecord
    {
        [Display(Name = "ROUTE ID")]
        public String ROUTEID { get; set; }
        
        [Display(Name = "DESCRIPTION")]
        public String DESCRIPTION { get; set; }

        public RouteZoneRecord() { }

        public RouteZoneRecord
            (
                  String _routeId
                , String _description
            ) 
        {
            ROUTEID = _routeId;
            DESCRIPTION = _description;
        }
    }
}
