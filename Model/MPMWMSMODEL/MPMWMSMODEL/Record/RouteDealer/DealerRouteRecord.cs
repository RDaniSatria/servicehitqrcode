﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Record.RouteDealer
{
    public class DealerRouteRecord
    {
        [Display(Name = "DEALER CODE")]
        public String ACCOUNTNUM { get; set; }

        [Display(Name = "ROUTE ID")]
        public String ROUTEID { get; set; }

        [Display(Name = "DESCRIPTION")]
        public String DESCRIPTION { get; set; }

        public DealerRouteRecord() { }

        public DealerRouteRecord(String _accountNum, String _routeId, String _desc) 
        {
            ACCOUNTNUM = _accountNum;
            ROUTEID = _routeId;
            DESCRIPTION = _desc;
        }
    }
}
