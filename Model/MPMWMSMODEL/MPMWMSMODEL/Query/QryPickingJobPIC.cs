﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Module;
using MPMLibrary.NET.Lib.Exception;
using System.Data.Linq.SqlClient;

namespace MPMWMSMODEL.Query
{
    public class QryPickingJobPIC : IDisposable
    {
        WmsUnitDataContext Context = new WmsUnitDataContext();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }

        public class SummaryPickingDonePIC_Rec
        {
            public string Dealer { get; set; }
            public int Qty { get; set; }

            public SummaryPickingDonePIC_Rec()
            {
            }

            public SummaryPickingDonePIC_Rec(string _dealer, int _qty)
            {
                Dealer = _dealer;
                Qty = _qty;
            }
        }

        public class SummaryPickingJobPIC_Rec
        {
            public string PIC { get; set; }
            public int Qty { get; set; }

            public SummaryPickingJobPIC_Rec()
            {
            }

            public SummaryPickingJobPIC_Rec(string _pic, int _qty)
            {
                PIC = _pic;
                Qty = _qty;
            }
        }

        public class SummaryPicking_Rec
        {
            public string PickingId { get; set; }

            public SummaryPicking_Rec()
            {
                PickingId = "";
            }

            public SummaryPicking_Rec(string _pickingid)
            {
                PickingId = _pickingid;
            }
        }      

        public class PickingJobPIC_Rec
        {
            public int JobId { get; set; }
            public string Customer { get; set; }
            public string CustomerName { get; set; }
            public string PickingId { get; set; }
            public string SOId { get; set; }
            public string ItemId { get; set; }
            public decimal Qty { get; set; }
            public decimal UnPickQty { get; set; }
            public string InventDimId { get; set; }
            public string LocationId { get; set; }
            public string PIC { get; set; }
            public long RecId { get; set; }
            public string Trolley { get; set; }
            public string PickingUser { get; set; }
            public int StatusPicking { get; set; }
            public int StatusTransporter { get; set; }

            public PickingJobPIC_Rec()
            {
                JobId = 0;
                Customer = "";
                CustomerName = "";
                PickingId = "";
                SOId = "";
                ItemId = "";
                Qty = 0;
                UnPickQty = 0;
                InventDimId = "";
                LocationId = "";
                PIC = "";
                RecId = 0;
                StatusPicking = 0;
                StatusTransporter = 0;
            }

            public PickingJobPIC_Rec(
                int _jobid,
                string _customer,
                string _customername,
                string _pickingid,
                string _soid,
                string _itemid,
                decimal _qty,
                decimal _unpickqty,
                string _inventdimid,
                string _locationid,
                string _pic,
                long _recid,
                string _trolley,
                string _pickingUser,
                int _statusPicking,
                int _statusTransporter)
            {
                JobId = _jobid;
                Customer = _customer;
                CustomerName = _customername;
                PickingId = _pickingid;
                SOId = _soid;
                ItemId = _itemid;
                Qty = _qty;
                UnPickQty = _unpickqty;
                InventDimId = _inventdimid;
                LocationId = _locationid;
                PIC = _pic;
                RecId = _recid;
                Trolley = _trolley;
                PickingUser = _pickingUser;
                StatusPicking = _statusPicking;
                StatusTransporter = _statusTransporter;
            }
        }

        public class PickingJobPICGroup_Rec
        {
            public int JobId { get; set; }
            public string InitialLocation { get; set; }
            public string TrolleyIdCurrent { get; set; }
            public int StatusTransporter { get; set; }

            public PickingJobPICGroup_Rec(
                int _jobid,
                string _initialLocation,
                string _trolleyIdCurrent,
                int _statusTransporter)
            {
                JobId = _jobid;
                InitialLocation = _initialLocation;
                TrolleyIdCurrent = _trolleyIdCurrent;
                StatusTransporter = _statusTransporter;
            }
        }

        #region QUERY
        private static Func<WmsUnitDataContext, string, string, string, string, IQueryable<PickingJobPIC_Rec>>
            QueryListPickingJobPICFilterNPK =
                CompiledQuery.Compile((WmsUnitDataContext db, string _dataArea, string _mainDealer, string _siteId, string _NPK)
                    =>
                        from area in db.MPMWMS_MAP_SO_PICAREAs
                        orderby area.JOBID
                            where (area.DATAAREAID == _dataArea)
                            && (area.MAINDEALERID == _mainDealer)
                            && (area.SITEID == _siteId)
                            && (area.ISDONEPICKING == "N")
                        join orderTrans in db.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                            where (orderTrans.DATAAREAID == _dataArea)
                            && (orderTrans.MPMSTATUSPICKING == 0) //flag utk yang belum diproses picking PDT
                        orderby orderTrans.INVENTTRANSREFID, orderTrans.ITEMID ascending
                        join picking in db.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID } 
                                                            equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                            where (picking.DATAAREAID == _dataArea)
                        join invDim in db.INVENTDIMs on orderTrans.INVENTDIMID equals invDim.INVENTDIMID
                            where (invDim.INVENTSITEID == _siteId)
                        join pic in db.MPMWMS_MAP_PIC_WAREHOUSEs on invDim.INVENTSITEID equals pic.SITEID
                            where (SqlMethods.Like(invDim.INVENTLOCATIONID.Replace(_siteId, ""), pic.INITIALWAREHOUSEID + "%"))
                            && (pic.SITEID == _siteId)
                            && (pic.NPK == _NPK)
                        select new PickingJobPIC_Rec
                        (
                              area.JOBID
                            , area.DEALERID
                            , picking.DELIVERYNAME
                            , picking.PICKINGROUTEID
                            , picking.TRANSREFID
                            , orderTrans.ITEMID
                            , orderTrans.QTY
                            , orderTrans.XTSUNPICKQTY
                            , orderTrans.INVENTDIMID
                            , invDim.INVENTLOCATIONID//pic.INITIALWAREHOUSEID
                            , pic.PIC
                            , orderTrans.RECID
                            , picking.XTSTROLLEYNBR
                            , picking.XTSLOCKEDUSERID
                            , orderTrans.MPMSTATUSPICKING
                            , orderTrans.MPMSTATUSTRANSPORTER
                        )
                );

        private static Func<WmsUnitDataContext, string, string, string, int, IQueryable<PickingJobPIC_Rec>>
            QueryJobPICByJobId =
                CompiledQuery.Compile((WmsUnitDataContext db, string _dataArea, string _mainDealer, string _siteId, int _jobId)
                    =>
                        from area in db.MPMWMS_MAP_SO_PICAREAs
                        orderby area.JOBID
                            where (area.DATAAREAID == _dataArea)
                            && (area.MAINDEALERID == _mainDealer)
                            && (area.SITEID == _siteId)
                            && (area.ISDONEPICKING == "N")
                            && (area.JOBID == _jobId)
                        join orderTrans in db.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                            where (orderTrans.DATAAREAID == _dataArea)
                            && (orderTrans.MPMSTATUSPICKING == 0) //flag utk yang belum diproses picking PDT
                        join picking in db.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID } 
                                                            equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                            where (picking.DATAAREAID == _dataArea)
                        join invDim in db.INVENTDIMs on orderTrans.INVENTDIMID equals invDim.INVENTDIMID
                            where (invDim.INVENTSITEID == _siteId)
                        join pic in db.MPMWMS_MAP_PIC_WAREHOUSEs on invDim.INVENTSITEID equals pic.SITEID
                            where (SqlMethods.Like(invDim.INVENTLOCATIONID.Replace(_siteId, ""), pic.INITIALWAREHOUSEID + "%"))
                            && (pic.SITEID == _siteId)
                        select new PickingJobPIC_Rec
                        (
                              area.JOBID
                            , area.DEALERID
                            , picking.DELIVERYNAME
                            , picking.PICKINGROUTEID
                            , picking.TRANSREFID
                            , orderTrans.ITEMID
                            , orderTrans.QTY
                            , orderTrans.XTSUNPICKQTY
                            , orderTrans.INVENTDIMID
                            , pic.INITIALWAREHOUSEID
                            , pic.PIC
                            , orderTrans.RECID
                            , picking.XTSTROLLEYNBR
                            , picking.XTSLOCKEDUSERID
                            , orderTrans.MPMSTATUSPICKING
                            , orderTrans.MPMSTATUSTRANSPORTER
                        )
                );

        private static Func<WmsUnitDataContext, string, string, string, string, IQueryable<PickingJobPIC_Rec>>
            QueryListPickingDoneByPIC =
                CompiledQuery.Compile((WmsUnitDataContext db, string _dataArea, string _mainDealer, string _siteId, string _NPK)
                    =>
                        from area in db.MPMWMS_MAP_SO_PICAREAs
                        orderby area.JOBID
                            where (area.DATAAREAID == _dataArea)
                            && (area.MAINDEALERID == _mainDealer)
                            && (area.SITEID == _siteId)
                            && (area.ISDONEPICKING == "Y") // ambil job yang sudah selesai proses Picking
                        join orderTrans in db.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                            where (orderTrans.DATAAREAID == _dataArea)
                            && (orderTrans.MPMSTATUSPICKING == 1) //flag utk yang sudah diproses picking PDT
                        join picking in db.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID } 
                                                            equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                            where (picking.DATAAREAID == _dataArea)
                            //&& (picking.XTSTROLLEYNBR == "")
                            //&& (picking.XTSLOCKEDUSERID == "")
                        join invDim in db.INVENTDIMs on orderTrans.INVENTDIMID equals invDim.INVENTDIMID
                            where (invDim.INVENTSITEID == _siteId)
                        join pic in db.MPMWMS_MAP_PIC_WAREHOUSEs on invDim.INVENTSITEID equals pic.SITEID
                        where (SqlMethods.Like(invDim.INVENTLOCATIONID.Replace(_siteId, ""), pic.INITIALWAREHOUSEID + "%"))
                            && (pic.SITEID == _siteId)
                            //&& (pic.NPK == _NPK)
                        select new PickingJobPIC_Rec
                        (
                              area.JOBID
                            , area.DEALERID
                            , picking.DELIVERYNAME
                            , picking.PICKINGROUTEID
                            , picking.TRANSREFID
                            , orderTrans.ITEMID
                            , orderTrans.QTY
                            , orderTrans.XTSUNPICKQTY
                            , orderTrans.INVENTDIMID
                            , pic.INITIALWAREHOUSEID.ToUpper()
                            , picking.XTSLOCKEDUSERID
                            , orderTrans.RECID
                            , picking.XTSTROLLEYNBR
                            , picking.XTSLOCKEDUSERID
                            , orderTrans.MPMSTATUSPICKING
                            , orderTrans.MPMSTATUSTRANSPORTER
                        )
                );

        /* query untuk mendapatkan data daftar job lama berdasarkan user di & trolley number nya */
        private static Func<WmsUnitDataContext, string, string, string, string, string, IQueryable<PickingJobPIC_Rec>>
            QueryLoadTransporterByUserTrolley =
                CompiledQuery.Compile((WmsUnitDataContext db, string _dataArea, string _mainDealer, string _siteId, string _NPK, string _trolley)
                    =>
                        from area in db.MPMWMS_MAP_SO_PICAREAs
                        orderby area.JOBID
                            where (area.DATAAREAID == _dataArea)
                            && (area.MAINDEALERID == _mainDealer)
                            && (area.SITEID == _siteId)
                            && (area.ISDONEPICKING == "Y") // ambil job yang sudah selesai proses Picking
                            && (area.ISDONETRANSPORTER == "N") // ambil yang belum selesai dikerjakan Transporter
                        join orderTrans in db.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                            where (orderTrans.DATAAREAID == _dataArea)
                            && (orderTrans.MPMSTATUSPICKING == 1) //flag utk yang sudah diproses picking PDT
                            //&& (orderTrans.MPMSTATUSTRANSPORTER == 0) // flag untuk transporter yang belum selesai
                        join picking in db.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID } 
                                                            equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                            where (picking.DATAAREAID == _dataArea)
                            && (picking.XTSLOCKEDUSERID == _NPK)
                            && (picking.XTSTROLLEYNBR == _trolley)
                        join invDim in db.INVENTDIMs on orderTrans.INVENTDIMID equals invDim.INVENTDIMID
                            where (invDim.INVENTSITEID == _siteId)
                        join pic in db.MPMWMS_MAP_PIC_WAREHOUSEs on invDim.INVENTSITEID equals pic.SITEID
                            where (SqlMethods.Like(invDim.INVENTLOCATIONID.Replace(_siteId, ""), pic.INITIALWAREHOUSEID + "%"))
                            && (pic.SITEID == _siteId)
                            //&& (pic.NPK == _NPK)
                        select new PickingJobPIC_Rec
                        (
                              area.JOBID
                            , area.DEALERID
                            , picking.DELIVERYNAME
                            , picking.PICKINGROUTEID
                            , picking.TRANSREFID
                            , orderTrans.ITEMID
                            , orderTrans.QTY
                            , orderTrans.XTSUNPICKQTY
                            , orderTrans.INVENTDIMID
                            , pic.INITIALWAREHOUSEID.ToUpper()
                            , _NPK //pic.PIC
                            , orderTrans.RECID
                            , picking.XTSTROLLEYNBR
                            , picking.XTSLOCKEDUSERID
                            , orderTrans.MPMSTATUSPICKING
                            , orderTrans.MPMSTATUSTRANSPORTER
                        )
                );
        #endregion

        public PickingJobPIC_Rec cekExistPendingTrolley(string dataareaid, string maindealer, string siteid, string npk, string trolley)
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, maindealer, siteid, npk);
            query = query.Where(x => x.Trolley != "" && x.Trolley != trolley && x.StatusTransporter == 0 && x.PIC == npk)
                                    .Select(data => new PickingJobPIC_Rec
                                                    (
                                                          data.JobId
                                                        , data.Customer
                                                        , data.CustomerName
                                                        , data.PickingId
                                                        , data.SOId
                                                        , data.ItemId
                                                        , data.Qty
                                                        , data.UnPickQty
                                                        , data.InventDimId
                                                        , data.LocationId
                                                        , data.PIC
                                                        , data.RecId
                                                        , data.Trolley
                                                        , data.PickingUser
                                                        , data.StatusPicking
                                                        , data.StatusTransporter
                                                    ));
            return query.Take(1).FirstOrDefault();
        }


        public PickingJobPIC_Rec cekExistJobByNPK(string dataareaid, string maindealer, string siteid, string npk)
        {
            var query = QueryListPickingJobPICFilterNPK(Context, dataareaid, maindealer, siteid, npk);

            query = query.OrderBy(c => c.JobId);

            return query.Take(1).FirstOrDefault();
        }

        public PickingJobPIC_Rec cekExistJobByNPKJobId(string dataareaid, string maindealer, string siteid, string npk, int jobid)
        {
            var query = QueryJobPICByJobId(Context, dataareaid, maindealer, siteid, jobid);

            return query.Where(z => z.PickingUser == npk).OrderBy(x => x.SOId).Take(1).FirstOrDefault();
        }

        public PickingJobPIC_Rec cekExistJobByJobId(string dataareaid, string maindealer, string siteid, int jobid)
        {
            var query = QueryJobPICByJobId(Context, dataareaid, maindealer, siteid, jobid);

            return query.OrderBy(x => x.SOId).Take(1).FirstOrDefault();
        }

        public List<PickingJobPIC_Rec> ListPickingDoneByPIC(string dataareaid, string maindealer, string siteid, string npk)
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, maindealer, siteid, npk);
            return query.ToList();
        }

        public List<PickingJobPICGroup_Rec> ListPickingDoneByPICGrouped(string dataareaid, string maindealer, string siteid, string npk, string trolley = "")
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, maindealer, siteid, npk);

            query = from data in query where data.StatusTransporter == 1 select data;

            // jika trolley nya tidak kosong, akan di filter berdasarkan trolley nya
            if (trolley != "")
            {
                query = from data in query where data.Trolley == trolley select data;
            }

            var groupData = query.GroupBy(x => new { x.JobId, x.LocationId, x.StatusTransporter })
                                                    .Select(data => new PickingJobPICGroup_Rec(data.Key.JobId, data.Key.LocationId, trolley, data.Key.StatusTransporter)).ToList();
            return groupData.ToList();
        }

        public List<PickingJobPIC_Rec> ListPickingDoneByJobId(string dataareaid, string maindealer, string siteid, string npk, int jobId)
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, maindealer, siteid, npk);

            var dataJob = query.Where(x => x.JobId == jobId).ToList()
                                    .Select(data => new PickingJobPIC_Rec
                                                    (
                                                          data.JobId
                                                        , data.Customer
                                                        , data.CustomerName
                                                        , data.PickingId
                                                        , data.SOId
                                                        , data.ItemId
                                                        , data.Qty
                                                        , data.UnPickQty
                                                        , data.InventDimId
                                                        , data.LocationId
                                                        , data.PIC
                                                        , data.RecId
                                                        , data.Trolley
                                                        , data.PickingUser
                                                        , data.StatusPicking
                                                        , data.StatusTransporter
                                                    )).ToList();

            return dataJob.ToList();
        }

        public List<PickingJobPICGroup_Rec> ListGetPartTransporterGrouped(string dataareaid, string maindealer, string siteid, string npk, string trolley, Boolean getNextNewJob)
        {
            var dataLama = QueryLoadTransporterByUserTrolley(Context, dataareaid, maindealer, siteid, npk, trolley).ToList();

            if (dataLama.Count() == 0 || getNextNewJob)
            {
                return getNextJobId(dataareaid, maindealer, siteid, npk, trolley);
            }
            else
            {
                var groupDataLama = dataLama.Where(x => x.PIC == npk).GroupBy(x => new { x.JobId, x.LocationId, x.StatusTransporter })
                                                        .Select(data => new PickingJobPICGroup_Rec(data.Key.JobId, data.Key.LocationId, trolley, data.Key.StatusTransporter)).ToList();

                return groupDataLama.ToList();
            }
        }

        public List<PickingJobPICGroup_Rec> getNextJobId(string dataareaid, string maindealer, string siteid, string npk, string trolley)
        {
            var dataBaru = QueryListPickingDoneByPIC(Context, dataareaid, maindealer, siteid, npk);

            var groupDataBaru = dataBaru.Where(z => z.Trolley == "" && z.PickingUser == "").GroupBy(x => new { x.JobId, x.LocationId, x.StatusTransporter })
                                                    .Select(data => new PickingJobPICGroup_Rec(data.Key.JobId, data.Key.LocationId, trolley, data.Key.StatusTransporter)).ToList();

            return groupDataBaru.ToList();
        }


        public Boolean cekJobAllTranspoterDone(string _dataArea, string _mainDealer, string _siteId, string npk, string trolley, int jobId)
        {
            var cek =   from area in Context.MPMWMS_MAP_SO_PICAREAs
                            where (area.DATAAREAID == _dataArea)
                                && (area.MAINDEALERID == _mainDealer)
                                && (area.SITEID == _siteId)
                                && (area.JOBID == jobId)
                                && (area.ISDONEPICKING == "Y")
                        join orderTrans in Context.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                            where (orderTrans.DATAAREAID == _dataArea)
                                && (orderTrans.MPMSTATUSTRANSPORTER == 0)
                                && (orderTrans.QTY > 0)
                        join picking in Context.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID }
                                                            equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                            where (picking.XTSTROLLEYNBR == trolley)
                        select area;

            if (cek.FirstOrDefault() != null)
            {
                return false;
            }

            return true;
        }

        #region OLD CODE
        /**
        public List<PickingJobPIC_Rec> ListJobByNPK(String _dataArea, String _mainDealer, String _siteId, String _NPK)
        {
            try
            {
                var itemsObj =  from area in Context.MPMWMS_MAP_SO_PICAREAs
                                orderby area.JOBID
                                    where (area.DATAAREAID == _dataArea)
                                    && (area.MAINDEALERID == _mainDealer)
                                    && (area.SITEID == _siteId)
                                    && (area.ISDONEPICKING == "N")
                                join orderTrans in Context.WMSORDERTRANs on area.SOID equals orderTrans.INVENTTRANSREFID
                                    where (orderTrans.DATAAREAID == _dataArea)
                                join picking in Context.WMSPICKINGROUTEs on new { A = orderTrans.ROUTEID, B = orderTrans.PARTITION, C = orderTrans.DATAAREAID, D = orderTrans.INVENTTRANSREFID } 
                                                                    equals new { A = picking.PICKINGROUTEID, B = picking.PARTITION, C = picking.DATAAREAID, D = picking.TRANSREFID }
                                    where (picking.DATAAREAID == _dataArea)
                                join invDim in Context.INVENTDIMs on orderTrans.INVENTDIMID equals invDim.INVENTDIMID
                                    where (invDim.INVENTSITEID == _siteId)
                                join pic in Context.MPMWMS_MAP_PIC_WAREHOUSEs on invDim.INVENTLOCATIONID equals pic.INITIALWAREHOUSEID
                                    where (pic.SITEID == _siteId)
                                    && (pic.NPK == _NPK)
                               select new PickingJobPIC_Rec
                               (
                                     area.DEALERID
                                   , picking.DELIVERYNAME
                                   , picking.TRANSREFID
                                   , picking.TRANSREFID
                                   , orderTrans.ITEMID
                                   , orderTrans.QTY
                                   , orderTrans.XTSUNPICKQTY
                                   , orderTrans.INVENTDIMID
                                   , pic.INITIALWAREHOUSEID
                                   , pic.PIC
                                   , orderTrans.RECID
                               );
                return itemsObj.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        **/

        /**
         * comment by Ardy 3 Feb 2017 ~ karena table MPMWMS_MAP_PIC_WAREHOUSE diganti dengan yang baru
        private static Func<WmsUnitDataContext, string, string, IQueryable<PickingJobPIC_Rec>>
            QueryListPickingJobPIC =
                CompiledQuery.Compile((WmsUnitDataContext db, string dataareaid, string siteid)
                    =>
                        from o in db.WMSORDERTRANs
                        join r in db.WMSPICKINGROUTEs on
                            new { routeid = o.ROUTEID, dataareaid = o.DATAAREAID } equals
                            new { routeid = r.PICKINGROUTEID, dataareaid = r.DATAAREAID } into j_or
                        from or in j_or.DefaultIfEmpty()
                        join x in db.SALESTABLEs on
                            new { R = or.TRANSREFID } equals
                            new { R = x.SALESID } into j_x
                        from xx in j_x.DefaultIfEmpty()
                        join d in db.INVENTDIMs on
                            new { inventdimid = o.INVENTDIMID, dataareaid = o.DATAAREAID } equals
                            new { inventdimid = d.INVENTDIMID, dataareaid = d.DATAAREAID } into j_od
                        from od in j_od.DefaultIfEmpty()
                        join l in db.INVENTLOCATIONs on
                            new { inventlocationid = od.INVENTLOCATIONID, dataareaid = od.DATAAREAID } equals
                            new { inventlocationid = l.INVENTLOCATIONID, dataareaid = l.DATAAREAID } into j_odl
                        from odl in j_odl.DefaultIfEmpty()
                        join m in db.MPMWMS_MAP_PIC_WAREHOUSEs on
                            new { warehouseid = odl.INVENTLOCATIONID } equals
                            new { warehouseid = m.WAREHOUSEID } into j_odlm
                        from odlm in j_odlm.DefaultIfEmpty()
                        where
                            o.DATAAREAID == dataareaid
                            && od.INVENTSITEID == siteid
                            && o.EXPEDITIONSTATUS == 3
                            && o.MPMSTATUSPICKING == 0
                            && xx.SALESSTATUS < 2
                            && o.PARTITION == 5637144576
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "HOTLINE NON CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "HOTLINE CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "URGENT CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "URGENT NON CLAIM"
                        orderby (xx.PAYMENT == "CASH" ? "0" : xx.PAYMENT), or.ACTIVATIONDATETIME, or.CUSTOMER, o.RECID
                        select new PickingJobPIC_Rec(
                                or.CUSTOMER,
                                or.DELIVERYNAME,
                                or.PICKINGROUTEID,
                                or.TRANSREFID,
                                o.ITEMID,
                                o.QTY,
                                o.XTSUNPICKQTY,
                                o.INVENTDIMID,
                                odl.INVENTLOCATIONID,
                                odlm.NPK,
                                o.RECID
                            )
                );
        **/

        /**
         * comment by Ardy 3 Feb 2017 ~ karena table MPMWMS_MAP_PIC_WAREHOUSE diganti dengan yang baru
        private static Func<WmsUnitDataContext, string, string, string, IQueryable<PickingJobPIC_Rec>>
            QueryListPickingJobPICFilterNPK =
                CompiledQuery.Compile((WmsUnitDataContext db, string dataareaid, string siteid, string npk)
                    =>
                        from o in db.WMSORDERTRANs
                        join r in db.WMSPICKINGROUTEs on
                            new { routeid = o.ROUTEID, dataareaid = o.DATAAREAID, partition = o.PARTITION } equals
                            new { routeid = r.PICKINGROUTEID, dataareaid = r.DATAAREAID, partition = r.PARTITION } into j_or
                        from or in j_or.DefaultIfEmpty()
                        join x in db.SALESTABLEs on
                            new { R = or.TRANSREFID, D = or.DATAAREAID, P = or.PARTITION } equals
                            new { R = x.SALESID, D = x.DATAAREAID, P = x.PARTITION } into j_x
                        from xx in j_x.DefaultIfEmpty()
                        join d in db.INVENTDIMs on
                            new { inventdimid = o.INVENTDIMID, dataareaid = o.DATAAREAID, partition = o.PARTITION } equals
                            new { inventdimid = d.INVENTDIMID, dataareaid = d.DATAAREAID, partition = d.PARTITION } into j_od
                        from od in j_od.DefaultIfEmpty()
                        join l in db.INVENTLOCATIONs on
                            new { inventlocationid = od.INVENTLOCATIONID, dataareaid = od.DATAAREAID, partition = od.PARTITION } equals
                            new { inventlocationid = l.INVENTLOCATIONID, dataareaid = l.DATAAREAID, partition = l.PARTITION } into j_odl
                        from odl in j_odl.DefaultIfEmpty()
                        join m in db.MPMWMS_MAP_PIC_WAREHOUSEs on
                            new { warehouseid = odl.INVENTLOCATIONID } equals
                            new { warehouseid = m.WAREHOUSEID } into j_odlm
                        from odlm in j_odlm.DefaultIfEmpty()
                        join c in db.MPMWMS_MAP_PIC_COLORs on
                            new { npk = odlm.NPK } equals
                            new { npk = c.PIC } into j_oc
                        from oc in j_oc.DefaultIfEmpty()
                        where
                            o.DATAAREAID == dataareaid
                            && od.INVENTSITEID == siteid
                            && o.EXPEDITIONSTATUS == 3
                            && oc.NPK == npk
                            && o.MPMSTATUSPICKING == 0
                            && xx.SALESSTATUS < 2
                            && o.PARTITION == 5637144576
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "HOTLINE NON CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "HOTLINE CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "URGENT CLAIM"
                            && xx.XTSINVSLSTRANSTYPE.ToUpper() != "URGENT NON CLAIM"
                        orderby or.ACTIVATIONDATETIME, or.CUSTOMER, o.RECID
                        select new PickingJobPIC_Rec(
                                or.CUSTOMER,
                                or.DELIVERYNAME,
                                or.PICKINGROUTEID,
                                or.TRANSREFID,
                                o.ITEMID,
                                o.QTY,
                                o.XTSUNPICKQTY,
                                o.INVENTDIMID,
                                odl.INVENTLOCATIONID,
                                odlm.NPK,
                                o.RECID
                            )
                );
        **/
        
        //public List<PickingJobPIC_Rec> List(string dataareaid, string siteid)
        //{
        //    var query = QueryListPickingJobPIC(Context, dataareaid, siteid);
        //    return query.ToList();
        //}

        //public List<SummaryPickingJobPIC_Rec> ListSummarybyPIC(string dataareaid, string siteid)
        //{
        //    var query = QueryListPickingJobPIC(Context, dataareaid, siteid);
        //    var list =
        //        from a in query
        //        group a by a.PIC into g
        //        select new SummaryPickingJobPIC_Rec(
        //            g.Key, g.Count());
        //    return list.ToList();
        //}

        /**
         * * comment by Ardy 3 Feb 2017 ~ karena table MPMWMS_MAP_PIC_WAREHOUSE diganti dengan yang baru
        private static Func<WmsUnitDataContext, string, string, IQueryable<V_MPM_PICKING_DONE_BY_PIC>>
            QueryListPickingDone =
                CompiledQuery.Compile((WmsUnitDataContext db, string dataareaid, string siteid)
                    =>
                        from a in db.V_MPM_PICKING_DONE_BY_PICs
                        join b in db.MPMWMS_MAP_PICKINGs on
                            new { PICK = a.PICKINGROUTEID, SITE = a.INVENTSITEID }
                            equals new { PICK = b.JOURNALID, SITE = b.SITE_ID } into join_ab
                        from c in join_ab.DefaultIfEmpty()
                        where
                            a.DATAAREAID == dataareaid
                            && a.INVENTSITEID == siteid
                        select a
                );
        

        public List<V_MPM_PICKING_DONE_BY_PIC> ListPickingDoneByPIC(string dataareaid, string siteid, string pic)
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, siteid, pic);
            return query.ToList();
        }

        public List<SummaryPickingDonePIC_Rec> ListSummaryDonebyPIC(string dataareaid, string siteid, string pic)
        {
            var query = QueryListPickingDoneByPIC(Context, dataareaid, siteid, pic);
            var list =
                from a in query
                group a by a.CUSTOMER into g
                select new SummaryPickingDonePIC_Rec(
                    g.Key, g.Count());
            return list.ToList();
        }

        public List<SummaryPickingDonePIC_Rec> ListSummaryDone(string dataareaid, string siteid)
        {
            var query = QueryListPickingDone(Context, dataareaid, siteid);
            var list =
                from a in query
                group a by a.CUSTOMER into g
                select new SummaryPickingDonePIC_Rec(
                    g.Key, g.Count());
            return list.ToList();
        }
         
        **/
        #endregion
    }
}
