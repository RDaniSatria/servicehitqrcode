﻿using MPMWMSMODEL.Module;
using MPMWMSMODEL.View.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class CustomerModel : BaseModel
    {
        private CustomerViewObject _CustomerViewObject = new CustomerViewObject();

        public CustomerModel()
            : base()
        {
            _CustomerViewObject.Context = (WmsUnitDataContext)Context;
        }

        public CustomerViewObject CustomerViewObject
        {
            get
            {
                return _CustomerViewObject;
            }
        }
    }
}
