﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPM365SERVICE.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.Service.MPMMuliaService;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Bom;
using MPMWMSMODEL.Table.Bor;
using MPMWMSMODEL.Table.Faktur;
using MPMWMSMODEL.Table.InventJournalTrans;
using MPMWMSMODEL.View.Bom;
using MPMWMSMODEL.View.Bor;

namespace MPMWMSMODEL.Model
{
    public class BundleUnitAccModel : PDTUnitModel
    {
        private BomViewObject _BomViewObject = new BomViewObject();
        private BorViewObject _BorViewObject = new BorViewObject();
        private BomObject _BomObject = new BomObject();
        private BorObject _BorObject = new BorObject();
        private InventJournalTransObject _InventJournalTransObject = new InventJournalTransObject();
        private FakturObject _FakturObject = new FakturObject();

        public BundleUnitAccModel()
            : base()
        {
            _BomViewObject.Context = (WmsUnitDataContext)Context;
            _BorViewObject.Context = (WmsUnitDataContext)Context;

            _BomObject.Context = (WmsUnitDataContext)Context;
            _BorObject.Context = (WmsUnitDataContext)Context;

            _InventJournalTransObject.Context = (WmsUnitDataContext)Context;

            _FakturObject.Context = (WmsUnitDataContext)Context;
        }

        public BomViewObject BomViewObject
        {
            get { return _BomViewObject; }
        }

        public BorViewObject BorViewObject
        {
            get { return _BorViewObject; }
        }

        public FakturObject FakturObject
        {
            get { return _FakturObject; }
        }

        public InventJournalTransObject InventJournalTransObject
        {
            get { return _InventJournalTransObject; }
        }

        public BomObject BomObject
        {
            get { return _BomObject; }
        }

        public BorObject BorObject
        {
            get { return _BorObject; }
        }

        public BOM_BOR_TYPE_COLOR_RECORD BundleUnit(String machine_or_frame_id, String type, String color,
            String site_id, String warehouse_id,
            String status)
        {
            String unit_type_new = "";
            String unit_color_new = "";
            String transfer_id_parent = "";
            String transfer_id_child = "";

            try
            {
                BeginTransaction();
                //proses scan by qr code
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQRWithLocation(UserSession.DATAAREA_ID, qrcode, site_id, warehouse_id);
                //end of proses scan by qr code

                ////proses scan by nosin
                //MPMWMS_UNIT unit = UnitObject.ItemByLocation(UserSession.DATAAREA_ID, machine_or_frame_id, site_id, warehouse_id); //--> Modif By Anong 9 / 6 / 2019 Ket: Untuk QRCODE
                ////end of proses scan by nosin

                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }

                if (unit.UNIT_TYPE != type || unit.COLOR_TYPE != color)
                {
                    throw new MPMException("Unit type and color mismatch!!!");
                }

                if (unit.STATUS == "S")
                {
                    throw new MPMException("Unit already shipping!!!");
                }

                if (status == "BOM")
                {
                    XVS_BOM_VIEW bomOld = BomViewObject.Item(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        type, color);
                    if (bomOld == null)
                    {
                        throw new MPMException("Type Old not found !!!");
                    }

                    XVS_BOM_VIEW bomNew = BomViewObject.ItemNewBundle(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        bomOld.INVENTTRANSIDFATHER);
                    if (bomNew == null)
                    {
                        throw new MPMException("Type New not found !!!");
                    }

                    // update bomflag at AX
                    INVENTJOURNALTRAN inventTransOld = InventJournalTransObject.ItemByTransferIdFather(
                        bomOld.JOURNALID,
                        bomOld.DATAAREAID,
                        bomOld.ITEMID,
                        bomOld.INVENTCOLORID,
                        bomOld.INVENTTRANSIDFATHER);
                    if (inventTransOld == null)
                    {
                        throw new MPMException("InventJournalTrans Old found !!!");
                    }
                    /*
                    inventTransOld.XTSBOMQTYPDT = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    {
                        inventTransOld.XTSBOMFLAG = 1;
                    }
                     * */
                    InventoryService invent = new InventoryService();
                    var resultAX = new ResultCommon();
                    MPMInventoryParm[] param;
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    param[0].parmJournalId = bomOld.JOURNALID;
                    param[0].parmItemIdBOM = bomOld.ITEMID;
                    param[0].parmColor = bomOld.INVENTCOLORID;
                    param[0].parmInventTransIdFather = bomOld.INVENTTRANSIDFATHER;
                    //if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    //{
                    //    //untuk param flag
                    //}

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    INVENTJOURNALTRAN inventTransNew = InventJournalTransObject.ItemByTransferId(
                        bomNew.JOURNALID,
                        bomNew.DATAAREAID,
                        bomNew.ITEMID,
                        bomNew.INVENTCOLORID,
                        bomNew.INVENTTRANSID);
                    if (inventTransNew == null)
                    {
                        throw new MPMException("InventJournalTrans New found !!!");
                    }
                    /*
                    inventTransNew.XTSBOMQTYPDT = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    if (inventTransNew.XTSBOMQTYPDT >= inventTransNew.QTY)
                    {
                        inventTransNew.XTSBOMFLAG = 1;
                    }
                    */
                    resultAX = new ResultCommon();
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    param[0].parmJournalId = bomNew.JOURNALID;
                    param[0].parmItemIdBOM = bomNew.ITEMID;
                    param[0].parmColor = bomNew.INVENTCOLORID;
                    param[0].parmInventTransId = bomNew.INVENTTRANSID;

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    // update unit
                    unit.UNIT_TYPE = bomNew.ITEMID;
                    unit.COLOR_TYPE = bomNew.INVENTCOLORID;
                    UnitObject.Update(unit, "BOM");

                    // insert to bom table wms
                    MPMWMS_BOM itemBOMWMS = BomObject.Create();
                    itemBOMWMS.MACHINE_ID = unit.MACHINE_ID;
                    itemBOMWMS.FRAME_ID = unit.FRAME_ID;
                    itemBOMWMS.JOURNAL_ID = bomNew.JOURNALID;
                    itemBOMWMS.JOURNAL_DATE = bomNew.XTSTRANSDATE;
                    itemBOMWMS.UNIT_TYPE = bomOld.ITEMID;
                    itemBOMWMS.COLOR_TYPE = bomOld.INVENTCOLORID;
                    itemBOMWMS.UNIT_TYPE_NEW = bomNew.ITEMID;
                    itemBOMWMS.COLOR_TYPE_NEW = bomNew.INVENTCOLORID;
                    BomObject.Insert(itemBOMWMS);

                    unit_type_new = itemBOMWMS.UNIT_TYPE_NEW;
                    unit_color_new = itemBOMWMS.COLOR_TYPE_NEW;
                    transfer_id_parent = inventTransOld.INVENTTRANSIDFATHER;
                    transfer_id_child = inventTransNew.INVENTTRANSID;

                    ((WmsUnitDataContext)Context).MPM_SP_BOMBOR_ADDEDIT(
                        itemBOMWMS.DATAAREA_ID,
                        itemBOMWMS.SITE_ID,
                        unit.WAREHOUSE_ID,
                        itemBOMWMS.UNIT_TYPE,
                        itemBOMWMS.COLOR_TYPE,
                        itemBOMWMS.UNIT_TYPE_NEW,
                        itemBOMWMS.COLOR_TYPE_NEW,
                        unit.UNIT_YEAR,
                        1,
                        UserSession.NPK);
                }
                else if (status == "BOR")
                {
                    XVS_BOR_VIEW borOld = BorViewObject.Item(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        type, color);
                    if (borOld == null)
                    {
                        throw new MPMException("Type Old not found !!!");
                    }

                    XVS_BOR_VIEW borNew = BorViewObject.ItemNewBundle(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        borOld.INVENTTRANSID);
                    if (borNew == null)
                    {
                        throw new MPMException("Type New not found !!!");
                    }

                    // update borflag at AX
                    INVENTJOURNALTRAN inventTransOld = InventJournalTransObject.ItemByTransferIdFather(
                        borOld.JOURNALID,
                        borOld.DATAAREAID,
                        borOld.ITEMID,
                        borOld.INVENTCOLORID,
                        borOld.INVENTTRANSIDFATHER);
                    if (inventTransOld == null)
                    {
                        throw new MPMException("InventJournalTrans Old found !!!");
                    }
                    /*
                    inventTransOld.XTSBOMQTYPDT = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    {
                        inventTransOld.XTSBOMFLAG = 1;
                    } */
                    InventoryService invent = new InventoryService();
                    var resultAX = new ResultCommon();
                    MPMInventoryParm[] param;
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    param[0].parmJournalId = borOld.JOURNALID;
                    param[0].parmItemIdBOM = borOld.ITEMID;
                    param[0].parmColor = borOld.INVENTCOLORID;
                    param[0].parmInventTransIdFather = borOld.INVENTTRANSIDFATHER;
                    //if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    //{
                    //    //untuk param flag
                    //}

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    INVENTJOURNALTRAN inventTransNew = InventJournalTransObject.ItemByTransferId(
                        borNew.JOURNALID,
                        borNew.DATAAREAID,
                        borNew.ITEMID,
                        borNew.INVENTCOLORID,
                        borNew.INVENTTRANSID);
                    if (inventTransNew == null)
                    {
                        throw new MPMException("InventJournalTrans New found !!!");
                    }
                    /*
                    inventTransNew.XTSBOMQTYPDT = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    if (inventTransNew.XTSBOMQTYPDT >= inventTransNew.QTY)
                    {
                        inventTransNew.XTSBOMFLAG = 1;
                    } */
                    resultAX = new ResultCommon();
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    param[0].parmJournalId = borNew.JOURNALID;
                    param[0].parmItemIdBOM = borNew.ITEMID;
                    param[0].parmColor = borNew.INVENTCOLORID;
                    param[0].parmInventTransId = borNew.INVENTTRANSID;

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    // update unit
                    unit.UNIT_TYPE = borNew.ITEMID;
                    unit.COLOR_TYPE = borNew.INVENTCOLORID;
                    UnitObject.Update(unit, "BOR");

                    // insert to bom table wms
                    MPMWMS_BOR itemBORWMS = BorObject.Create();
                    itemBORWMS.MACHINE_ID = unit.MACHINE_ID;
                    itemBORWMS.FRAME_ID = unit.FRAME_ID;
                    itemBORWMS.JOURNAL_ID = borNew.JOURNALID;
                    itemBORWMS.JOURNAL_DATE = borNew.XTSTRANSDATE;
                    itemBORWMS.UNIT_TYPE = borOld.ITEMID;
                    itemBORWMS.COLOR_TYPE = borOld.INVENTCOLORID;
                    itemBORWMS.UNIT_TYPE_NEW = borNew.ITEMID;
                    itemBORWMS.COLOR_TYPE_NEW = borNew.INVENTCOLORID;
                    BorObject.Insert(itemBORWMS);

                    unit_type_new = itemBORWMS.UNIT_TYPE_NEW;
                    unit_color_new = itemBORWMS.COLOR_TYPE_NEW;
                    transfer_id_parent = inventTransOld.INVENTTRANSIDFATHER;
                    transfer_id_child = inventTransNew.INVENTTRANSID;

                    ((WmsUnitDataContext)Context).MPM_SP_BOMBOR_ADDEDIT(
                        itemBORWMS.DATAAREA_ID,
                        itemBORWMS.SITE_ID,
                        unit.WAREHOUSE_ID,
                        itemBORWMS.UNIT_TYPE,
                        itemBORWMS.COLOR_TYPE,
                        itemBORWMS.UNIT_TYPE_NEW,
                        itemBORWMS.COLOR_TYPE_NEW,
                        unit.UNIT_YEAR,
                        -1,
                        UserSession.NPK);
                }

                // update faktur
                //MPMINVFAKTUR faktur = FakturObject.Item(unit.DATAAREA_ID, unit.MACHINE_ID);
                //if (faktur != null)
                //{
                //    faktur.KODETYPE = unit_type_new;
                //    faktur.KODEWARNA = unit_color_new;
                //}
                SalesService salesService = new SalesService();
                var resultAXFaktur = new ResultCommon();
                MPMInvFakturParm[] invfakturparm;
                invfakturparm = new MPMInvFakturParm[1];
                invfakturparm[0] = new MPMInvFakturParm();
                invfakturparm[0].parmMachineId = unit.MACHINE_ID;
                invfakturparm[0].parmKodeTipe = unit_type_new;
                invfakturparm[0].parmKodeWarna = unit_color_new;
                resultAXFaktur = salesService.updateInvFakturTipeWarna(invfakturparm, UserSession.DATAAREA_ID);
                if (resultAXFaktur.Message != "" && resultAXFaktur.Success == 0)
                {
                    throw new Exception(resultAXFaktur.Message);
                }

                Commit();

                BOM_BOR_TYPE_COLOR_RECORD result = new BOM_BOR_TYPE_COLOR_RECORD();
                result._TYPE = unit_type_new;
                result._COLOR = unit_color_new;                
                result._TRANSFER_ID_PARENT = transfer_id_parent;
                result._TRANSFER_ID_CHILD = transfer_id_child;

                //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                //String nameType = unit.UNIT_TYPE;
                //if (viewProduct != null)
                //{
                //    nameType = viewProduct.nametype;
                //}

                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                if (viewProduct != null)
                {
                    //nameType = viewProduct.nametype;
                    nameType = viewProduct.ITEMNAME;
                }

                PrintStoring(
                    unit.MACHINE_ID, 
                    unit.SITE_ID, 
                    unit.LOCATION_ID, 
                    unit.COLOR_TYPE, nameType, 
                    unit.UNIT_YEAR,
                    unit.SPG_DATE.Value,
                    unit.UNIT_TYPE);

                return result;
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public BOM_BOR_TYPE_COLOR_RECORD BundleUnitWithoutPrint(String bom_id, String machine_or_frame_id, String type, String color,
            String site_id, String warehouse_id,
            String status)
        {
            String unit_type_new = "";
            String unit_color_new = "";
            String transfer_id_parent = "";
            String transfer_id_child = "";

            try
            {
                BeginTransaction();

                MPMWMS_UNIT unit = UnitObject.ItemByLocation(UserSession.DATAAREA_ID, machine_or_frame_id, site_id, warehouse_id);
                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }

                if (unit.UNIT_TYPE != type || unit.COLOR_TYPE != color)
                {
                    throw new MPMException("Unit type and color mismatch!!!");
                }

                if (unit.STATUS == "S")
                {
                    throw new MPMException("Unit already shipping!!!");
                }

                if (status == "BOM")
                {
                    XVS_BOM_VIEW bomOld = BomViewObject.ItemByJournalBomId(
                        bom_id,
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        type, color);
                    if (bomOld == null)
                    {
                        throw new MPMException("Type Old not found !!!");
                    }

                    XVS_BOM_VIEW bomNew = BomViewObject.ItemNewBundleByJournalBomId(
                        bom_id,
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        bomOld.INVENTTRANSIDFATHER);
                    if (bomNew == null)
                    {
                        throw new MPMException("Type New not found !!!");
                    }

                    // update bomflag at AX
                    INVENTJOURNALTRAN inventTransOld = InventJournalTransObject.ItemByTransferIdFather(
                        bomOld.JOURNALID,
                        bomOld.DATAAREAID,
                        bomOld.ITEMID,
                        bomOld.INVENTCOLORID,
                        bomOld.INVENTTRANSIDFATHER);
                    if (inventTransOld == null)
                    {
                        throw new MPMException("InventJournalTrans Old found !!!");
                    }
                    /*
                    inventTransOld.XTSBOMQTYPDT = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    {
                        inventTransOld.XTSBOMFLAG = 1;
                    }
                     * */
                    InventoryService invent = new InventoryService();
                    var resultAX = new ResultCommon();
                    MPMInventoryParm[] param;
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    param[0].parmJournalId = bomOld.JOURNALID;
                    param[0].parmItemIdBOM = bomOld.ITEMID;
                    param[0].parmColor = bomOld.INVENTCOLORID;
                    param[0].parmInventTransIdFather = bomOld.INVENTTRANSIDFATHER;
                    //if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    //{
                    //    //untuk param flag
                    //}

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    INVENTJOURNALTRAN inventTransNew = InventJournalTransObject.ItemByTransferId(
                        bomNew.JOURNALID,
                        bomNew.DATAAREAID,
                        bomNew.ITEMID,
                        bomNew.INVENTCOLORID,
                        bomNew.INVENTTRANSID);
                    if (inventTransNew == null)
                    {
                        throw new MPMException("InventJournalTrans New found !!!");
                    }
                    /*
                    inventTransNew.XTSBOMQTYPDT = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    if (inventTransNew.XTSBOMQTYPDT >= inventTransNew.QTY)
                    {
                        inventTransNew.XTSBOMFLAG = 1;
                    }*/
                    resultAX = new ResultCommon();
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    param[0].parmJournalId = bomNew.JOURNALID;
                    param[0].parmItemIdBOM = bomNew.ITEMID;
                    param[0].parmColor = bomNew.INVENTCOLORID;
                    param[0].parmInventTransId = bomNew.INVENTTRANSID;

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    // update unit
                    unit.UNIT_TYPE = bomNew.ITEMID;
                    unit.COLOR_TYPE = bomNew.INVENTCOLORID;
                    UnitObject.Update(unit, "BOM");

                    // insert to bom table wms
                    MPMWMS_BOM itemBOMWMS = BomObject.Create();
                    itemBOMWMS.MACHINE_ID = unit.MACHINE_ID;
                    itemBOMWMS.FRAME_ID = unit.FRAME_ID;
                    itemBOMWMS.JOURNAL_ID = bomNew.JOURNALID;
                    itemBOMWMS.JOURNAL_DATE = bomNew.XTSTRANSDATE;
                    itemBOMWMS.UNIT_TYPE = bomOld.ITEMID;
                    itemBOMWMS.COLOR_TYPE = bomOld.INVENTCOLORID;
                    itemBOMWMS.UNIT_TYPE_NEW = bomNew.ITEMID;
                    itemBOMWMS.COLOR_TYPE_NEW = bomNew.INVENTCOLORID;
                    BomObject.Insert(itemBOMWMS);

                    unit_type_new = itemBOMWMS.UNIT_TYPE_NEW;
                    unit_color_new = itemBOMWMS.COLOR_TYPE_NEW;
                    transfer_id_parent = inventTransOld.INVENTTRANSIDFATHER;
                    transfer_id_child = inventTransNew.INVENTTRANSID;

                    ((WmsUnitDataContext)Context).MPM_SP_BOMBOR_ADDEDIT(
                        itemBOMWMS.DATAAREA_ID,
                        itemBOMWMS.SITE_ID,
                        unit.WAREHOUSE_ID,
                        itemBOMWMS.UNIT_TYPE,
                        itemBOMWMS.COLOR_TYPE,
                        itemBOMWMS.UNIT_TYPE_NEW,
                        itemBOMWMS.COLOR_TYPE_NEW,
                        unit.UNIT_YEAR,
                        1,
                        UserSession.NPK);
                }
                else if (status == "BOR")
                {
                    XVS_BOR_VIEW borOld = BorViewObject.Item(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        type, color);
                    if (borOld == null)
                    {
                        throw new MPMException("Type Old not found !!!");
                    }

                    XVS_BOR_VIEW borNew = BorViewObject.ItemNewBundle(
                        UserSession.DATAAREA_ID,
                        UserSession.MAINDEALER_ID,
                        UserSession.SITE_ID_MAPPING,
                        borOld.INVENTTRANSID);
                    if (borNew == null)
                    {
                        throw new MPMException("Type New not found !!!");
                    }

                    // update borflag at AX
                    INVENTJOURNALTRAN inventTransOld = InventJournalTransObject.ItemByTransferIdFather(
                        borOld.JOURNALID,
                        borOld.DATAAREAID,
                        borOld.ITEMID,
                        borOld.INVENTCOLORID,
                        borOld.INVENTTRANSIDFATHER);
                    if (inventTransOld == null)
                    {
                        throw new MPMException("InventJournalTrans Old found !!!");
                    }
                    //inventTransOld.XTSBOMQTYPDT = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    //if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    //{
                    //    inventTransOld.XTSBOMFLAG = 1;
                    //}
                    InventoryService invent = new InventoryService();
                    var resultAX = new ResultCommon();
                    MPMInventoryParm[] param;
                    param = new MPMInventoryParm[1];
                    param[0] = new MPMInventoryParm();
                    param[0].parmQty = Convert.ToInt32(inventTransOld.XTSBOMQTYPDT) - 1;
                    param[0].parmJournalId = borOld.JOURNALID;
                    param[0].parmItemIdBOM = borOld.ITEMID;
                    param[0].parmColor = borOld.INVENTCOLORID;
                    param[0].parmInventTransIdFather = borOld.INVENTTRANSIDFATHER;
                    if (inventTransOld.XTSBOMQTYPDT <= inventTransOld.QTY)
                    {
                        //untuk param flag
                    }

                    //resultAX = invent.updateQtyBOMUnit(param, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    INVENTJOURNALTRAN inventTransNew = InventJournalTransObject.ItemByTransferId(
                        borNew.JOURNALID,
                        borNew.DATAAREAID,
                        borNew.ITEMID,
                        borNew.INVENTCOLORID,
                        borNew.INVENTTRANSID);
                    if (inventTransNew == null)
                    {
                        throw new MPMException("InventJournalTrans New found !!!");
                    }
                    //inventTransNew.XTSBOMQTYPDT = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    //if (inventTransNew.XTSBOMQTYPDT >= inventTransNew.QTY)
                    //{
                    //    inventTransNew.XTSBOMFLAG = 1;
                    //}
                    
                    MPMInventoryParm[] param1;
                    param1 = new MPMInventoryParm[1];
                    param1[0] = new MPMInventoryParm();
                    param1[0].parmQty = Convert.ToInt32(inventTransNew.XTSBOMQTYPDT) + 1;
                    param1[0].parmJournalId = borNew.JOURNALID;
                    param1[0].parmItemIdBOM = borNew.ITEMID;
                    param1[0].parmColor = borNew.INVENTCOLORID;
                    param1[0].parmInventTransId = borNew.INVENTTRANSID;
                    if (inventTransNew.XTSBOMQTYPDT >= inventTransNew.QTY)
                    {
                        //untuk param flag
                    }

                    //resultAX = invent.updateQtyBOMUnit(param1, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}

                    // update unit
                    unit.UNIT_TYPE = borNew.ITEMID;
                    unit.COLOR_TYPE = borNew.INVENTCOLORID;
                    UnitObject.Update(unit, "BOR");

                    // insert to bom table wms
                    MPMWMS_BOR itemBORWMS = BorObject.Create();
                    itemBORWMS.MACHINE_ID = unit.MACHINE_ID;
                    itemBORWMS.FRAME_ID = unit.FRAME_ID;
                    itemBORWMS.JOURNAL_ID = borNew.JOURNALID;
                    itemBORWMS.JOURNAL_DATE = borNew.XTSTRANSDATE;
                    itemBORWMS.UNIT_TYPE = borOld.ITEMID;
                    itemBORWMS.COLOR_TYPE = borOld.INVENTCOLORID;
                    itemBORWMS.UNIT_TYPE_NEW = borNew.ITEMID;
                    itemBORWMS.COLOR_TYPE_NEW = borNew.INVENTCOLORID;
                    ////BorObject.Insert(itemBORWMS);

                    ((WmsUnitDataContext)Context).ExecuteCommand("EXEC MPMWMS_INSERT_BOR " +
                                                                " '" + UserSession.DATAAREA_ID + "'" +
                                                                " ,'" + UserSession.MAINDEALER_ID + "'" +
                                                                " ,'" + UserSession.SITE_ID_MAPPING + "'" +
                                                                " , '" + unit.MACHINE_ID + "' " +
                                                                " , '" + unit.FRAME_ID + "' " +
                                                                " , '" + borOld.ITEMID + "' " +
                                                                " , '" + borOld.INVENTCOLORID + "' " +
                                                                " , '" + borNew.ITEMID + "' " +
                                                                " , '" + borNew.INVENTCOLORID + "' " +
                                                                " , '" + UserSession.USER_ID + "'");

                    unit_type_new = itemBORWMS.UNIT_TYPE_NEW;
                    unit_color_new = itemBORWMS.COLOR_TYPE_NEW;
                    transfer_id_parent = inventTransOld.INVENTTRANSIDFATHER;
                    transfer_id_child = inventTransNew.INVENTTRANSID;

                    ((WmsUnitDataContext)Context).MPM_SP_BOMBOR_ADDEDIT(
                        itemBORWMS.DATAAREA_ID,
                        itemBORWMS.SITE_ID,
                        unit.WAREHOUSE_ID,
                        itemBORWMS.UNIT_TYPE,
                        itemBORWMS.COLOR_TYPE,
                        itemBORWMS.UNIT_TYPE_NEW,
                        itemBORWMS.COLOR_TYPE_NEW,
                        unit.UNIT_YEAR,
                        -1,
                        UserSession.NPK);
                }

                // update faktur
                //MPMINVFAKTUR faktur = FakturObject.Item(unit.DATAAREA_ID, unit.MACHINE_ID);
                //if (faktur != null)
                //{
                //    faktur.KODETYPE = unit_type_new;
                //    faktur.KODEWARNA = unit_color_new;
                //}
                SalesService salesService = new SalesService();
                var resultAXFaktur = new ResultCommon();
                MPMInvFakturParm[] invfakturparm;
                invfakturparm = new MPMInvFakturParm[1];
                invfakturparm[0] = new MPMInvFakturParm();
                invfakturparm[0].parmMachineId = unit.MACHINE_ID;
                invfakturparm[0].parmKodeTipe = unit_type_new;
                invfakturparm[0].parmKodeWarna = unit_color_new;
                resultAXFaktur = salesService.updateInvFakturTipeWarna(invfakturparm, UserSession.DATAAREA_ID);
                if (resultAXFaktur.Message != "" && resultAXFaktur.Success == 0)
                {
                    throw new Exception(resultAXFaktur.Message);
                }


                Commit();

                BOM_BOR_TYPE_COLOR_RECORD result = new BOM_BOR_TYPE_COLOR_RECORD();
                result._TYPE = unit_type_new;
                result._COLOR = unit_color_new;
                result._TRANSFER_ID_PARENT = transfer_id_parent;
                result._TRANSFER_ID_CHILD = transfer_id_child;

                /*
                MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                if (viewProduct != null)
                {
                    nameType = viewProduct.nametype;
                }
                */
                 
                return result;
            }
            catch (Exception e)
            {
                Rollback();
                throw new Exception(e.Message);
            }
        }
    }
}
