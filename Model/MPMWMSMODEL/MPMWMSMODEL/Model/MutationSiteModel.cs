﻿using MPM365SERVICE.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.Service.MPMMuliaService;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.MutationSite;
using MPMWMSMODEL.Table.Setting;
using MPMWMSMODEL.xtsMuliaService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class MutationSiteModel : BaseModel
    {
      //  protected String UrlNetTcpXtsTransInventService = "net.tcp://10.10.101.3:8201/DynamicsAx/Services/xtsMuliaService";
        //protected String UrlNetTcpXtsTransInventService = "net.tcp://10.10.101.14:8201/DynamicsAx/Services/xtsMuliaService";
        protected String ServiceGroup = "xtsMuliaService";

        //protected CallContext ContextWS = new CallContext();

        private MutationSiteObject _MutationSiteObject = new MutationSiteObject();
        private SettingDetailObject _SettingDetailObject = new SettingDetailObject();
        
        public MutationSiteModel()
            : base()
        {
            _MutationSiteObject.Context = (WmsUnitDataContext)Context;
            _MutationSiteObject.MutationSiteLineObject.Context = _MutationSiteObject.Context;
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;
        }

        public MutationSiteObject MutationSiteObject
        {
            get { return _MutationSiteObject; }
        }

        /** WEB SERVICE **/
        //protected xtsMuliaService.xtsTransferInventServiceClient ConnectWebService()
        //{
        //    try
        //    {
        //        try
        //        {
        //            MPM_SETTING_DETAIL item = _SettingDetailObject.Item("AX_WS", "WMS_UNIT", "WMS_UNIT");
        //            if (item != null)
        //            {
        //                UrlNetTcpXtsMuliaService = item.TAG_1;

        //            }

        //            item = _SettingDetailObject.Item("AX_WS", "WMS_PART", "WMS_PART");
        //            if (item != null)
        //            {
        //                UrlNetTcpMPMWMSPartService = item.TAG_1;

        //            }

        //            item = _SettingDetailObject.Item("AX_WS", "USERNAME", "PASSWORD");
        //            if (item != null)
        //            {
        //                LogonASUser = item.TAG_3 + "\\" + item.TAG_1;
        //                DomainWS = item.TAG_3;
        //                UsernameWS = item.TAG_1;
        //                PasswordWS = item.TAG_2;
        //            }
        //        }
        //        catch (MPMException e)
        //        {
        //            throw new MPMException(e.Message);
        //        }

        //        System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
        //        bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
        //        bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

        //        ContextWS.Company = UserSession.DATAAREA_ID;
        //        ContextWS.LogonAsUser = LogonASUser;
        //        ContextWS.Language = "en-us";
        //        System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
        //            UrlNetTcpXtsMuliaService, LogonASUser);

        //        xtsMuliaService.xtsTransferInventServiceClient client = new xtsMuliaService.xtsTransferInventServiceClient(bind, xRemote);
        //        client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
        //        client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
        //        client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
        //        client.Open();

        //        return client;
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        //protected void DisconnectWebService(xtsMuliaService.xtsTransferInventServiceClient client)
        //{
        //    client.Close();
        //    client = null;
        //}
        /** END OF WEB SERVICE **/

        public void Insert(bool submit, MPMWMS_MUTATION_SITE mutation_site)
        {
            try
            {
                if (mutation_site.SITE_ID == mutation_site.SITE_ID_NEW)
                {
                    throw new MPMException("Destination site not different with beginning site !!");
                }
                else
                {
                    // before insert find location receiving in site 2
                    mutation_site.WAREHOUSE_ID_NEW = LocationObject.GetWarehouseByType(mutation_site.DATAAREA_ID, mutation_site.SITE_ID_NEW, "R");

                    MutationSiteObject.Insert(mutation_site);
                    if (submit) SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_MUTATION_SITE mutation_site)
        {
            try
            {
                if (mutation_site.SITE_ID == mutation_site.SITE_ID_NEW)
                {
                    throw new MPMException("Destination site not different with beginning site !!");
                }
                else
                {
                    mutation_site.WAREHOUSE_ID_NEW = LocationObject.GetWarehouse(mutation_site.DATAAREA_ID, mutation_site.SITE_ID_NEW, "R");
                    MutationSiteObject.Update(mutation_site);   
                    if (submit) SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        private void Validate(MPMWMS_UNIT unit)
        {
            MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(
                unit.DATAAREA_ID,
                unit.SITE_ID,
                unit.LOCATION_ID);

            if (location == null)
            {
                throw new MPMException("Can't find location");
            }

            if (location.LOCATION_TYPE == "P")
            {
                throw new MPMException("Can't mutation Picking unit");
            }
            else if (location.LOCATION_TYPE == "E")
            {
                throw new MPMException("Can't mutation Shipping unit");
            }
            if (location.LOCATION_TYPE == "B")
            {
                throw new MPMException("Can't mutation Blacklist unit");
            }
            if (location.LOCATION_TYPE == "N")
            {
                throw new MPMException("Can't mutation NRFS unit");
            }
        }

        //ADD BY VANIA
        public void InsertLine(bool submit, MPMWMS_MUTATION_SITE_LINE mutation_line)
        {
            try
            {
                // get data from header
                MPMWMS_MUTATION_SITE mutation = MutationSiteObject.Item(mutation_line.DATAAREA_ID, mutation_line.MUTATION_ID);
                if (mutation == null)
                {
                    throw new MPMException("Can't find mutation header !!!");
                }

                MPMWMS_UNIT unit = UnitObject.Item(mutation_line.DATAAREA_ID, mutation_line.MACHINE_ID);
                if (unit == null)
                {
                    throw new MPMException("Unit not found!!!");
                }

                Validate(unit);

                mutation_line.WAREHOUSE_ID = mutation.WAREHOUSE_ID;
                mutation_line.WAREHOUSE_ID_NEW = mutation.WAREHOUSE_ID_NEW;                

                //mutation_line.WAREHOUSE_ID = LocationObject.GetWarehouse(mutation_line.DATAAREA_ID, mutation_line.SITE_ID, mutation_line.LOCATION_ID);
                
                mutation_line.SITE_ID_INTR = mutation_line.SITE_ID;
                mutation_line.LOCATION_ID_INTR = GetLocation(mutation_line.DATAAREA_ID, mutation_line.SITE_ID_INTR, "T");
                mutation_line.WAREHOUSE_ID_INTR = LocationObject.GetWarehouse(mutation_line.DATAAREA_ID, mutation_line.SITE_ID_INTR, mutation_line.LOCATION_ID_INTR);

                mutation_line.LOCATION_ID_NEW = GetLocation(mutation_line.DATAAREA_ID, mutation_line.SITE_ID_NEW, "R");

                mutation_line.FRAME_ID = unit.FRAME_ID;

                //mutation_line.WAREHOUSE_ID_NEW = LocationObject.GetWarehouse(mutation_line.DATAAREA_ID, mutation_line.SITE_ID_NEW, mutation_line.LOCATION_ID_NEW);
                
                MutationSiteObject.MutationSiteLineObject.Insert(mutation_line);

                // pindahkan unit ke intransit
                unit.WAREHOUSE_ID = mutation_line.WAREHOUSE_ID_INTR;
                unit.LOCATION_ID = mutation_line.LOCATION_ID_INTR;
                unit.STATUS = "BM";
                UnitObject.Update(unit, "BM", mutation_line.SITE_ID, mutation_line.WAREHOUSE_ID, mutation_line.LOCATION_ID);

                // update warehouse_location actual stock in old location
                UpdateLocationStock(UserSession.DATAAREA_ID, mutation_line.SITE_ID, mutation_line.WAREHOUSE_ID, mutation_line.LOCATION_ID, -1);

                // update warehouse_location actual stock in new location
                UpdateLocationStock(UserSession.DATAAREA_ID, mutation_line.SITE_ID_INTR, mutation_line.WAREHOUSE_ID_INTR, mutation_line.LOCATION_ID_INTR, 1);
                Context.CommandTimeout = 500;
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateLine(bool submit, MPMWMS_MUTATION_SITE_LINE mutation_line)
        {
            try
            {
                mutation_line.WAREHOUSE_ID = LocationObject.GetWarehouse(mutation_line.DATAAREA_ID, mutation_line.SITE_ID, mutation_line.LOCATION_ID);
                MutationSiteObject.MutationSiteLineObject.Update(mutation_line);
                Context.CommandTimeout = 500;
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //add by vania
        public void Delete(String mutation_id, String dataarea_id)
        {
            try
            {
                BeginTransaction();

                // delete mutation site
                MPMWMS_MUTATION_SITE deletedItem = MutationSiteObject.Item(mutation_id, dataarea_id);

                // before delete header, delete all line first
                List<MPMWMS_MUTATION_SITE_LINE> list_line = MutationSiteObject.MutationSiteLineObject.List(dataarea_id, mutation_id);
                foreach (var a in list_line)
                {
                    DeleteLine(false, a.MUTATION_ID, a.MACHINE_ID, a.DATAAREA_ID, a.FRAME_ID);
                }

                MutationSiteObject.Delete(deletedItem);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        //add by vania
        public void DeleteLine(bool submit, String mutation_id, String machine_id, String dataarea_id, String frame_id)
        {
            try
            {
                // delete mutation list line
                MPMWMS_MUTATION_SITE_LINE deletedItem = MutationSiteObject.MutationSiteLineObject.Item(dataarea_id, mutation_id, machine_id, frame_id);
                if (deletedItem.STATUS.Equals("O"))
                {
                    String site_id_old = deletedItem.SITE_ID_INTR;
                    String warehouse_id_old = deletedItem.WAREHOUSE_ID_INTR;
                    String location_id_old = deletedItem.LOCATION_ID_INTR;

                    String site_id_new = deletedItem.SITE_ID;
                    String warehouse_id_new = deletedItem.WAREHOUSE_ID;
                    String location_id_new = deletedItem.LOCATION_ID;

                    MutationSiteObject.MutationSiteLineObject.Delete(deletedItem);

                    // pindahkan unit ke posisi awal
                    MPMWMS_UNIT unit = UnitObject.Item(deletedItem.DATAAREA_ID, deletedItem.MACHINE_ID);
                    if (unit == null)
                    {
                        throw new MPMException("Unit not found!!!");
                    }
                    unit.WAREHOUSE_ID = warehouse_id_new;
                    unit.LOCATION_ID = location_id_new;

                    MPMWMS_WAREHOUSE_LOCATION warehouseObj = LocationObject.Item(unit.DATAAREA_ID, site_id_new, location_id_new);
                    if (warehouseObj != null)
                    {
                        if (
                                warehouseObj.LOCATION_TYPE == "S" ||
                                warehouseObj.LOCATION_TYPE == "R"
                            )
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }
                        else if (
                                warehouseObj.LOCATION_TYPE == "N" ||
                                warehouseObj.LOCATION_TYPE == "RT" ||
                                warehouseObj.LOCATION_TYPE == "T" ||
                                warehouseObj.LOCATION_TYPE == "I"
                            )
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                    }
                    else
                        unit.STATUS = "U";

                    UnitObject.Update(unit, "U", site_id_old, warehouse_id_old, location_id_old);

                    // update warehouse_location actual stock in old location
                    UpdateLocationStock(UserSession.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

                    // update warehouse_location actual stock in new location
                    UpdateLocationStock(UserSession.DATAAREA_ID, site_id_new, warehouse_id_new, location_id_new, 1);
                    
                    if (submit) SubmitChanges();
                }
                else
                {
                    throw new MPMException("Status not Opened !!!");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DoPost(String mutation_id, String dataarea_id)
        {
            String transferId = "";
            InventoryService inventService = new InventoryService();
            //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebService();
            var resultAX = new ResultCommon();
            MPMInventoryParm[] inventParm;
            try
            {
                BeginTransaction();

                MPMWMS_MUTATION_SITE mutation = MutationSiteObject.Item(dataarea_id, mutation_id);
                if (mutation == null)
                {
                    throw new MPMException("Can't find mutation header !!!");
                }

                if (!mutation.STATUS.Equals("O"))
                {
                    throw new MPMException("Can't POST this mutation !!!");
                }

                // send to webservice
                //transferId = client.CreateTransferOrderHeader(ContextWS, "U", mutation.WAREHOUSE_ID, mutation.WAREHOUSE_ID_NEW, mutation_id);
                
                // get all line and set location new and location intransit
                List<MPMWMS_MUTATION_SITE_LINE> lines = MutationSiteObject.MutationSiteLineObject.List(dataarea_id, mutation_id);
                int line_number = 1;

                inventParm = new MPMInventoryParm[lines.Count];
                string guid = System.Guid.NewGuid().ToString();
                foreach (var line in lines)
                {
                    if (line.STATUS == "O")
                    {
                        line.TRANSFER_ID = transferId;

                        // get site, warehouse and location intransit
                        line.SITE_ID_INTR = line.SITE_ID;
                        line.LOCATION_ID_INTR = GetLocation(dataarea_id, line.SITE_ID_INTR, "T");
                        line.WAREHOUSE_ID_INTR = LocationObject.GetWarehouse(dataarea_id, line.SITE_ID_INTR, line.LOCATION_ID_INTR);

                        // get warehouse and location receiving new
                        string warehouseidnew = line.WAREHOUSE_ID_NEW;
                        line.LOCATION_ID_NEW = GetLocation(dataarea_id, line.SITE_ID_NEW, "R");
                        line.WAREHOUSE_ID_NEW = LocationObject.GetWarehouse(dataarea_id, line.SITE_ID_NEW, line.LOCATION_ID_NEW);

                        if(warehouseidnew != line.WAREHOUSE_ID_NEW)
                        {
                            throw new MPMException("Warehouse Id New Tidak Valid");
                        }

                        // update current unit intransit location
                        MPMWMS_UNIT unit = UnitObject.Item(line.MACHINE_ID, line.FRAME_ID, line.DATAAREA_ID);
                        if (unit == null)
                        {
                            throw new MPMException("Can't find unit !!!");
                        }
                        //unit.SITE_ID = line.SITE_ID_INTR;
                        //unit.WAREHOUSE_ID = line.WAREHOUSE_ID_INTR;
                        //unit.LOCATION_ID = line.LOCATION_ID_INTR;
                        //UnitObject.Update(unit, "MTS", line.SITE_ID, line.WAREHOUSE_ID, line.LOCATION_ID);

                        // update stock at location source (-1) and location intransit (+1)
                        //UpdateLocationStock(dataarea_id, line.SITE_ID, line.WAREHOUSE_ID, line.LOCATION_ID, -1);
                        //UpdateLocationStock(dataarea_id, line.SITE_ID_INTR, line.WAREHOUSE_ID_INTR, line.LOCATION_ID_INTR, 1);

                        line.STATUS = "P";
                        MutationSiteObject.MutationSiteLineObject.Update(line);

                        try
                        {
                            //header
                            inventParm[line_number - 1] = new MPMInventoryParm();
                            inventParm[line_number - 1].parmWmsNumber = guid;//mutation_id;
                            inventParm[line_number - 1].parmMainDealer = UserSession.MAINDEALER_ID;
                            inventParm[line_number - 1].parmAutoPosting = NoYes.No;
                            inventParm[line_number - 1].parmCategoryCode = "U";
                            inventParm[line_number - 1].parmWarehouseId = mutation.WAREHOUSE_ID;
                            inventParm[line_number - 1].parmToWarehouseId = mutation.WAREHOUSE_ID_NEW;
                            inventParm[line_number - 1].parmLocationFrom = mutation.WAREHOUSE_ID;
                            inventParm[line_number - 1].parmLocationTo = mutation.WAREHOUSE_ID_NEW;
                            inventParm[line_number - 1].parmTransferId = "";
                            //lines
                            inventParm[line_number - 1].parmLineNumber = line_number;
                            inventParm[line_number - 1].parmItemId = unit.UNIT_TYPE;
                            inventParm[line_number - 1].parmQty = 1;
                            inventParm[line_number - 1].parmColor = unit.COLOR_TYPE;
                            inventParm[line_number - 1].parmSiteId = line.SITE_ID;
                            //inventParm[line_number - 1].parmToSiteId = line.SITE_ID;
                            //inventParm[0].parmLocationFrom = line.WAREHOUSE_ID;
                            inventParm[line_number - 1].parmMachineNo = unit.MACHINE_ID;
                            inventParm[line_number - 1].parmChasisNo = unit.FRAME_ID;
                            inventParm[line_number - 1].parmPlateNo = mutation.POLICE_NUMBER;
                            inventParm[line_number - 1].parmCarrierName = mutation.EXPEDITION_ID;
                            inventParm[line_number - 1].parmDriverName = mutation.DRIVER;
                            inventParm[line_number - 1].parmTransDate = DateTime.Now;
                            //resultAX = inventService.createInventTransferOrder(inventParm, UserSession.DATAAREA_ID, true);
                            //if (resultAX.Message != "" && resultAX.Success == 0)
                            //{
                            //    throw new Exception(resultAX.Message);
                            //}
                            //transferId = resultAX.Data;
                            //client.CreateTransferOrderLine(ContextWS, transferId, line_number, unit.UNIT_TYPE,
                            //    "", unit.COLOR_TYPE, "", line.SITE_ID, 1, line.WAREHOUSE_ID);
                        }
                        catch (Exception e)
                        {
                            //Rollback();
                            //if (transferId != "")
                            //{
                            //    inventParm = new MPMInventoryParm[1];
                            //    inventParm[0] = new MPMInventoryParm();
                            //    inventParm[0].parmTransferId = transferId;
                            //    resultAX = inventService.cancelTransferOrder(inventParm, UserSession.DATAAREA_ID, true);
                            //    if (resultAX.Message != "" && resultAX.Success == 0)
                            //    {
                            //        throw new Exception(resultAX.Message);
                            //    }
                            //    //client.CancelTransferOrder(ContextWS, transferId);
                            //}
                            throw new MPMException(e.Message + " - [" + line_number + ": " + unit.UNIT_TYPE +
                                " - " + unit.COLOR_TYPE);
                        }
                        line_number++;
                    }
                }

                resultAX = inventService.createInventTransferOrder(inventParm, UserSession.DATAAREA_ID);
                if (resultAX.Message != "" && resultAX.Success == 0)
                {
                    throw new Exception(resultAX.Message);
                }
                transferId = resultAX.Data;

                //client.PostTransferOrder(ContextWS, transferId);
                MPMInventoryParm[] inventParmPosting;
                //foreach (var a in inventParm)
                //{
                    inventParmPosting = new MPMInventoryParm[1];
                    inventParmPosting[0] = new MPMInventoryParm();
                    inventParmPosting[0].parmTransferId = transferId;
                    //inventParmPosting[0].parmItemId = "*";//a.parmItemId;
                    //inventParmPosting[0].parmQty = 1;
                    //resultAX = inventService.postTransferOrderReceiveAll(inventParmPosting, UserSession.DATAAREA_ID);
                    //if (resultAX.Message != "" && resultAX.Success == 0)
                    //{
                    //    throw new Exception(resultAX.Message);
                    //}
                //}
                // update mutation
                mutation.TRANSFER_ID = transferId;
                mutation.STATUS = "P";
                MutationSiteObject.Update(mutation);

                foreach (var line in lines)
                {
                    line.TRANSFER_ID = transferId;
                    MutationSiteObject.MutationSiteLineObject.Update(line);
                }

                Commit();
            }
            catch (Exception e)
            {
                Rollback();
                //client.CancelTransferOrder(ContextWS, transferId);
                throw new MPMException(e.Message);
            }
            //DisconnectWebService(client);
        }

        public List<MPM_VIEW_MUTATION_SITE_SUMMARY> SummaryMutation(String mutation_id)
        {
            try
            {
                IQueryable<MPM_VIEW_MUTATION_SITE_SUMMARY> query =
                    from a in ((WmsUnitDataContext)Context).MPM_VIEW_MUTATION_SITE_SUMMARies
                    where
                        a.MUTATION_ID == mutation_id
                    select a;

                return query.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }
    }
}
