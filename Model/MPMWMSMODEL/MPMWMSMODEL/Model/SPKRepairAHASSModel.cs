﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.SPKAhass;

namespace MPMWMSMODEL.Model
{
    public class SPKRepairAHASSModel: BaseModel
    {
        private SPKRepairAhassObject _SPKRepairAhassObject = new SPKRepairAhassObject();

        public SPKRepairAHASSModel()
        {
            _SPKRepairAhassObject.Context = (WmsUnitDataContext)Context;
            _SPKRepairAhassObject.SPKRepairAhassLineObject.Context = (WmsUnitDataContext)Context;
        }

        public SPKRepairAhassObject SPKRepairAhassObject
        {
            get { return _SPKRepairAhassObject; }
        }

        public void Insert(bool submit, MPMWMS_SPK_REPAIR_AHASS repair_ahass)
        {
            try
            {
                SPKRepairAhassObject.Insert(repair_ahass);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_SPK_REPAIR_AHASS repair_ahass)
        {
            try
            {
                SPKRepairAhassObject.Update(repair_ahass);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, String spk_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                // delete expedition
                MPMWMS_SPK_REPAIR_AHASS deletedItem = SPKRepairAhassObject.Item(spk_id, dataarea_id, maindealer_id, site_id);
                if (deletedItem == null)
                {
                    throw new MPMException("Can't find the SPK Repair AHASS !!!");
                }

                if (deletedItem.MPMWMS_SPK_REPAIR_AHASS_LINEs.ToList().Count() >= 1)
                {
                    throw new MPMException("Can't delete this SPK Repair AHASS, because the line already exists !!!");
                }
                SPKRepairAhassObject.Delete(deletedItem);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertLine(bool submit, MPMWMS_SPK_REPAIR_AHASS_LINE repair_ahass_line)
        {
            try
            {
                SPKRepairAhassObject.SPKRepairAhassLineObject.Insert(repair_ahass_line);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateLine(bool submit, MPMWMS_SPK_REPAIR_AHASS_LINE repair_ahass_line)
        {
            try
            {
                SPKRepairAhassObject.SPKRepairAhassLineObject.Update(repair_ahass_line);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DeleteLine(bool submit, String spk_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id)
        {
            try
            {
                // delete expedition
                MPMWMS_SPK_REPAIR_AHASS_LINE deletedItem = SPKRepairAhassObject.SPKRepairAhassLineObject.Item(
                    spk_id, dataarea_id, maindealer_id, site_id, machine_id, frame_id);
                if (deletedItem == null)
                {
                    throw new MPMException("Can't find the SPK Repair AHASS Line !!!");
                }

                SPKRepairAhassObject.SPKRepairAhassLineObject.Delete(deletedItem);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public String Bast(String spk_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id, String bast_id)
        {
            try
            {
                MPMWMS_SPK_REPAIR_AHASS_LINE data = SPKRepairAhassObject.SPKRepairAhassLineObject.Item(
                spk_id, dataarea_id, maindealer_id, site_id, machine_id, frame_id);
                if (data == null)
                {
                    throw new MPMException("Can't find the SPK Repair AHASS Line !!!");
                }

                data.BAST_ID = bast_id;
                data.BAST_DATE = MPMDateUtil.DateTime;
                SubmitChanges();
                return data.BAST_ID;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DeleteBast(String spk_id, String dataarea_id, String maindealer_id, String site_id,
            String machine_id, String frame_id)
        {
            try
            {
                MPMWMS_SPK_REPAIR_AHASS_LINE data = SPKRepairAhassObject.SPKRepairAhassLineObject.Item(
                spk_id, dataarea_id, maindealer_id, site_id, machine_id, frame_id);
                if (data == null)
                {
                    throw new MPMException("Can't find the SPK Repair AHASS Line !!!");
                }

                if (data.MODIF_BY != UserSession.NPK)
                {
                    throw new MPMException("You can't delete this BAST data, because you're not the last user that modified this data !!!");
                }

                if (data.BAST_DATE.Value.Date != MPMDateUtil.DateTime.Date)
                {
                    throw new MPMException("Date already change, you can't delete this BAST !!!");
                }

                data.BAST_ID = "";  
                data.BAST_DATE = null;
                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
