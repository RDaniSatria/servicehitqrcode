﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.EADUnit;
using MPMWMSMODEL.Table.ConstantaUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class ConstantaUnitModel : BaseModel
    {
        private ConstantaUnitObject _ConstantaUnitObject = new ConstantaUnitObject();

        public ConstantaUnitModel()
            : base()
        {
            _ConstantaUnitObject.Context = (WmsUnitDataContext)Context;
        }

        #region MPMWMS_EAD_CONSTANTA_UNIT_TYPE

        public void Insert(MPMWMS_EAD_CONSTANTA_UNIT_TYPE _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_CONSTANTA_UNIT_TYPE data = _ConstantaUnitObject.Item(_item.TYPE);

                if (data != null)
                {
                    throw new MPMException("Data constanta unit is already exist !!!");
                }

                _ConstantaUnitObject.Insert(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_CONSTANTA_UNIT_TYPE _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_CONSTANTA_UNIT_TYPE data = _ConstantaUnitObject.Item(_item.TYPE);

                if (data == null)
                {
                    throw new MPMException("Can't find the route !!!");
                }

                _ConstantaUnitObject.Update(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_CONSTANTA_UNIT_TYPE _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_CONSTANTA_UNIT_TYPE data = _ConstantaUnitObject.Item(_item.TYPE);

                if (data == null)
                {
                    throw new MPMException("Can't find the route !!!");
                }

                _ConstantaUnitObject.Delete(data);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_CONSTANTA_UNIT_TYPE> List()
        {
            return _ConstantaUnitObject.ItemAll();
        }

        public List<ConstantaTypeRecord> ViewAll()
        {
            return _ConstantaUnitObject.ViewAll();
        }


        public MPMWMS_EAD_CONSTANTA_UNIT_TYPE Item(String _type)
        {
            return _ConstantaUnitObject.Item(_type);
        }

        #endregion
    }
}
