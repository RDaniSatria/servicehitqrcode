﻿using MPMLibrary.NET.Lib.Db.Objects;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.AutoPickingEAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class AutomaticPickingModel : BaseModel
    {
        AutomaticPickingObject _AutomaticPickingObject = new AutomaticPickingObject();

        public AutomaticPickingModel()
            : base()
        {
            _AutomaticPickingObject.Context = (WmsUnitDataContext)Context;
        }

        public List<AutomaticPickingRecord> TOResult(String _prioritas1, String _prioritas2, String _prioritas3)
        {
            try
            {
                return _AutomaticPickingObject.TOResult(_prioritas1, _prioritas2, _prioritas3);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
