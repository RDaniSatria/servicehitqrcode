﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Lov.ShippingList;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.CorrectionSL;
using MPMWMSMODEL.Table.ShippingListLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Util;
using MPM365SERVICE.MPMMuliaService;
using MPM365SERVICE.Service.MPMMuliaService;
using MPM365SERVICE.Respond;

namespace MPMWMSMODEL.Model
{
    public class CorrectionSLModel: ReturSLModel
    {
        private CorrectionSLObject _CorrectionSLObject = new CorrectionSLObject();
        
        public CorrectionSLModel()
            : base()
        {
            _CorrectionSLObject.Context = (WmsUnitDataContext)Context;
            _CorrectionSLObject.CorrectionSLLineObject.Context = _CorrectionSLObject.Context;
        }

        public CorrectionSLObject CorrectionSLObject
        {
            get { return _CorrectionSLObject; }
        }

        public List<LOV_MACHINE_SL> LovMachineSLForRetur1(String correction_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            MPMWMS_CORRECTION_SL item = CorrectionSLObject.Item(correction_sl_id, dataarea_id, maindealer_id, site_id);
            if (item == null)
            {
                return null;
            }
            return lovMachineRetur.List(item.SHIPPING_LIST_ID_1, item.DATAAREA_ID);
        }

        public List<LOV_MACHINE_SL> LovMachineSLForRetur2(String correction_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            MPMWMS_CORRECTION_SL item = CorrectionSLObject.Item(correction_sl_id, dataarea_id, maindealer_id, site_id);
            if (item == null)
            {
                return null;
            }
            return lovMachineRetur.List(item.SHIPPING_LIST_ID_2, item.DATAAREA_ID);
        }

        public void Insert(MPMWMS_CORRECTION_SL item)
        {
            try
            {
                // get data shipping list 1
                MPMWMS_SHIPPING_LIST sl_1 = ShippingListObject.Item(item.SHIPPING_LIST_ID_1, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                if (sl_1 == null)
                {
                    throw new MPMException("Can't get Shipping List ID (1) !!!");
                }

                // get data shipping list 2
                MPMWMS_SHIPPING_LIST sl_2 = ShippingListObject.Item(item.SHIPPING_LIST_ID_2, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                if (sl_2 == null)
                {
                    throw new MPMException("Can't get Shipping List ID (2) !!!");
                }

                String retur_sl_id_1 = RunningNumberReturShippingListUnit(UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                String retur_sl_id_2 = RunningNumberReturShippingListUnit(UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                String correction_id = RunningNumberCorrectionSL(UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);

                BeginTransaction();

                // create two retur SL from SL 1 and SL 2
                MPMWMS_RETUR_SL retur_1 = ReturSLObject.Create();
                retur_1.RETUR_SL_ID = retur_sl_id_1;
                retur_1.RETUR_SL_DATE = MPMDateUtil.DateTime;
                retur_1.SHIPPING_LIST_ID = sl_1.SHIPPING_LIST_ID;
                retur_1.SHIPPING_LIST_DATE = sl_1.SHIPPING_DATE;
                retur_1.PICKING_ID = sl_1.PICKING_ID;
                retur_1.PICKING_DATE = sl_1.PICKING_DATE;
                retur_1.DO_ID = sl_1.DO_ID;
                retur_1.DO_DATE = sl_1.DO_DATE;
                retur_1.DEALER_CODE = sl_1.DEALER_CODE;
                retur_1.DEALER_NAME = sl_1.DEALER_NAME;
                retur_1.DEALER_ADDRESS = sl_1.DEALER_ADDRESS;
                retur_1.DEALER_CITY = sl_1.DEALER_CITY;
                ReturSLObject.Insert(retur_1);

                MPMWMS_RETUR_SL retur_2 = ReturSLObject.Create();
                retur_2.RETUR_SL_ID = retur_sl_id_2;
                retur_2.RETUR_SL_DATE = MPMDateUtil.DateTime;
                retur_2.SHIPPING_LIST_ID = sl_2.SHIPPING_LIST_ID;
                retur_2.SHIPPING_LIST_DATE = sl_2.SHIPPING_DATE;
                retur_2.PICKING_ID = sl_2.PICKING_ID;
                retur_2.PICKING_DATE = sl_2.PICKING_DATE;
                retur_2.DO_ID = sl_2.DO_ID;
                retur_2.DO_DATE = sl_2.DO_DATE;
                retur_2.DEALER_CODE = sl_2.DEALER_CODE;
                retur_2.DEALER_NAME = sl_2.DEALER_NAME;
                retur_2.DEALER_ADDRESS = sl_2.DEALER_ADDRESS;
                retur_2.DEALER_CITY = sl_2.DEALER_CITY;
                ReturSLObject.Insert(retur_2);

                item.RETUR_SL_ID_1 = retur_1.RETUR_SL_ID;
                item.RETUR_SL_ID_2 = retur_2.RETUR_SL_ID;
                item.CORRECTION_SL_ID = correction_id;
                item.DATAAREA_ID = UserSession.DATAAREA_ID;
                item.MAINDEALER_ID = UserSession.MAINDEALER_ID;
                CorrectionSLObject.Insert(item);
                
                Commit();
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void InsertLine(MPMWMS_CORRECTION_SL_LINE item)
        {
            try
            {
                BeginTransaction();

                // set data to insert on correction line
                // machine data
                MPMWMS_UNIT unit_1 = UnitObject.Item(item.DATAAREA_ID, item.MACHINE_ID);
                item.FRAME_ID = unit_1.FRAME_ID;
                item.UNIT_TYPE = unit_1.UNIT_TYPE;
                item.COLOR_TYPE = unit_1.COLOR_TYPE;

                MPMWMS_UNIT unit_2 = UnitObject.Item(item.DATAAREA_ID, item.MACHINE_ID_NEW);
                item.FRAME_ID_NEW = unit_2.FRAME_ID;
                item.UNIT_TYPE_NEW = unit_2.UNIT_TYPE;
                item.COLOR_TYPE_NEW = unit_2.COLOR_TYPE;

                item.MAINDEALER_ID = UserSession.MAINDEALER_ID;

                // validation unit
                if (!item.UNIT_TYPE.Equals(item.UNIT_TYPE_NEW) || !item.COLOR_TYPE.Equals(item.COLOR_TYPE_NEW))
                {
                    throw new MPMException("Unit and Color type not mismatch !!!");
                }

                // create retur line 1
                MPMWMS_RETUR_SL retur_1 = ReturSLObject.Item(item.RETUR_SL_ID_1, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                MPMWMS_RETUR_SL_LINE retur_line_1 = ReturSLObject.ReturSLLineObject.Create(retur_1);                
                retur_line_1.MACHINE_ID = item.MACHINE_ID;
                retur_line_1.FRAME_ID = item.FRAME_ID;
                retur_line_1.COLOR_TYPE = item.COLOR_TYPE;
                retur_line_1.UNIT_TYPE = item.UNIT_TYPE;
                retur_line_1.MACHINE_ID_NEW = item.MACHINE_ID_NEW;
                retur_line_1.FRAME_ID_NEW = item.FRAME_ID_NEW;
                retur_line_1.COLOR_TYPE_NEW = item.COLOR_TYPE_NEW;
                retur_line_1.UNIT_TYPE_NEW = item.UNIT_TYPE_NEW;
                ReturSLObject.ReturSLLineObject.Insert(retur_line_1);

                // create retur line 2
                MPMWMS_RETUR_SL retur_2 = ReturSLObject.Item(item.RETUR_SL_ID_2, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                MPMWMS_RETUR_SL_LINE retur_line_2 = ReturSLObject.ReturSLLineObject.Create(retur_2);
                retur_line_2.MACHINE_ID = item.MACHINE_ID_NEW;
                retur_line_2.FRAME_ID = item.FRAME_ID_NEW;
                retur_line_2.COLOR_TYPE = item.COLOR_TYPE_NEW;
                retur_line_2.UNIT_TYPE = item.UNIT_TYPE_NEW;
                retur_line_2.MACHINE_ID_NEW = item.MACHINE_ID;
                retur_line_2.FRAME_ID_NEW = item.FRAME_ID;
                retur_line_2.COLOR_TYPE_NEW = item.COLOR_TYPE;
                retur_line_2.UNIT_TYPE_NEW = item.UNIT_TYPE;
                ReturSLObject.ReturSLLineObject.Insert(retur_line_2);
                
                // update shipping list line
                MPMWMS_SHIPPING_LIST_LINE line_1 = ShippingListObject.ShippingListLineObject.Item(item.SHIPPING_LIST_ID_1,
                    item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID, 
                    item.MACHINE_ID, item.FRAME_ID);
                line_1.MACHINE_ID_BEFORE_RETUR = line_1.MACHINE_ID;
                line_1.FRAME_ID_BEFORE_RETUR = line_1.FRAME_ID;
                line_1.MACHINE_ID = item.MACHINE_ID_NEW;
                line_1.FRAME_ID = item.FRAME_ID_NEW;
                line_1.STATUS = "C";
                retur_line_1.NO_URUT = line_1.NO_URUT;
                ShippingListObject.ShippingListLineObject.Update(line_1);

                MPMWMS_SHIPPING_LIST_LINE line_2 = ShippingListObject.ShippingListLineObject.Item(item.SHIPPING_LIST_ID_2,
                    item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID, 
                    item.MACHINE_ID_NEW, item.FRAME_ID_NEW); 
                line_2.MACHINE_ID_BEFORE_RETUR = line_2.MACHINE_ID;
                line_2.FRAME_ID_BEFORE_RETUR = line_2.FRAME_ID;
                line_2.MACHINE_ID = item.MACHINE_ID;
                line_2.FRAME_ID = item.FRAME_ID;
                line_2.STATUS = "C";
                retur_line_2.NO_URUT = line_2.NO_URUT;
                ShippingListObject.ShippingListLineObject.Update(line_2);

                // update Faktur 1
                /* COMMENT BF 365
                MPMINVFAKTUR faktur_1 = FakturObject.Item(
                    retur_line_1.DATAAREA_ID,
                    retur_line_1.MACHINE_ID);
                if (faktur_1 != null)
                {
                    faktur_1.NOSLCAB = retur_line_2.SHIPPING_LIST_ID;
                    faktur_1.TGLSLCAB = retur_line_2.SHIPPING_LIST_DATE;
                    faktur_1.NODOCAB = retur_line_2.DO_ID;
                    faktur_1.TGLDOCAB = retur_line_2.DO_DATE;
                    //faktur_1.KDDEALER = ((WmsUnitDataContext)Context).MPMFUNC_DEALER_MULIA(retur_2.DEALER_CODE);
                    faktur_1.KDDEALER = UnitObject.ItemByKdDealerH1(retur_2.DEALER_CODE).MDCODE;
                    faktur_1.KDDEALERAX = retur_2.DEALER_CODE;
                }
                */
                var dealer_retur_2 = UnitObject.ItemByKdDealerH1(retur_2.DEALER_CODE);
                var dealer_retur_1 = UnitObject.ItemByKdDealerH1(retur_1.DEALER_CODE);

                if(dealer_retur_2 == null)
                {
                    throw new Exception("Kode Dealer " + retur_2.DEALER_CODE + " bukan Dealer H1.");
                }
                if (dealer_retur_1 == null)
                {
                    throw new Exception("Kode Dealer " + retur_1.DEALER_CODE + " bukan Dealer H1.");
                }


                SalesService salesService = new SalesService();
                var resultAX = new ResultCommon();
                MPMInvFakturParm[] invfakturparm;
                invfakturparm = new MPMInvFakturParm[1];
                invfakturparm[0] = new MPMInvFakturParm();
                invfakturparm[0].parmKodeDealer = dealer_retur_2.MDCODE;
                invfakturparm[0].parmKodeDealerAx = retur_2.DEALER_CODE;
                invfakturparm[0].parmKodeTipe = retur_line_1.UNIT_TYPE;
                invfakturparm[0].parmKodeWarna = retur_line_1.COLOR_TYPE;
                invfakturparm[0].parmMachineId = retur_line_1.MACHINE_ID;
                invfakturparm[0].parmNoDoCab = retur_line_2.DO_ID;
                invfakturparm[0].parmNoSlCab = retur_line_2.SHIPPING_LIST_ID;
                invfakturparm[0].parmTglDoCab = retur_line_2.DO_DATE;
                invfakturparm[0].parmTglSlCab = retur_line_2.SHIPPING_LIST_DATE;
                resultAX = salesService.updateInvFakturCorrectionUnit(invfakturparm, UserSession.DATAAREA_ID);
                if (resultAX.Message != "" && resultAX.Success == 0)
                {
                    throw new Exception(resultAX.Message);
                }

                // update Faktur 2
                /* COMMENT BF 365
                MPMINVFAKTUR faktur_2 = FakturObject.Item(
                    retur_line_2.DATAAREA_ID,
                    retur_line_2.MACHINE_ID);
                if (faktur_2 != null)
                {
                    faktur_2.NOSLCAB = retur_line_1.SHIPPING_LIST_ID;
                    faktur_2.TGLSLCAB = retur_line_1.SHIPPING_LIST_DATE;
                    faktur_2.NODOCAB = retur_line_1.DO_ID;
                    faktur_2.TGLDOCAB = retur_line_1.DO_DATE;
                    //faktur_2.KDDEALER = ((WmsUnitDataContext)Context).MPMFUNC_DEALER_MULIA(retur_1.DEALER_CODE);
                    faktur_2.KDDEALER = UnitObject.ItemByKdDealerH1(retur_1.DEALER_CODE).MDCODE;
                    faktur_2.KDDEALERAX = retur_1.DEALER_CODE;
                }
                */
                resultAX = new ResultCommon();
                invfakturparm = new MPMInvFakturParm[1];
                invfakturparm[0] = new MPMInvFakturParm();
                invfakturparm[0].parmKodeDealer = dealer_retur_1.MDCODE;
                invfakturparm[0].parmKodeDealerAx = retur_1.DEALER_CODE;
                invfakturparm[0].parmKodeTipe = retur_line_2.UNIT_TYPE;
                invfakturparm[0].parmKodeWarna = retur_line_2.COLOR_TYPE;
                invfakturparm[0].parmMachineId = retur_line_2.MACHINE_ID;
                invfakturparm[0].parmNoDoCab = retur_line_1.DO_ID;
                invfakturparm[0].parmNoSlCab = retur_line_1.SHIPPING_LIST_ID;
                invfakturparm[0].parmTglDoCab = retur_line_1.DO_DATE;
                invfakturparm[0].parmTglSlCab = retur_line_1.SHIPPING_LIST_DATE;
                resultAX = salesService.updateInvFakturCorrectionUnit(invfakturparm, UserSession.DATAAREA_ID);
                if (resultAX.Message != "" && resultAX.Success == 0)
                {
                    throw new Exception(resultAX.Message);
                }

                CorrectionSLObject.CorrectionSLLineObject.Insert(item);
                
                // update unit
                MPMWMS_SHIPPING_LIST sl_line_2 = ShippingListObject.Item(item.SHIPPING_LIST_ID_2, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                unit_1.SHIPPING_ID = item.SHIPPING_LIST_ID_2;
                unit_1.SHIPPING_DATE = sl_line_2.SHIPPING_DATE;
                unit_1.DO_ID = sl_line_2.DO_ID;
                unit_1.SL_DATE = sl_line_2.DO_DATE;
                unit_1.PICKING_ID = sl_line_2.PICKING_ID;
                unit_1.PICKING_DATE = sl_line_2.PICKING_DATE;

                MPMWMS_SHIPPING_LIST sl_line_1 = ShippingListObject.Item(item.SHIPPING_LIST_ID_1, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                unit_2.SHIPPING_ID = item.SHIPPING_LIST_ID_1;
                unit_2.SHIPPING_DATE = sl_line_1.SHIPPING_DATE;
                unit_2.DO_ID = sl_line_1.DO_ID;
                unit_2.SL_DATE = sl_line_1.DO_DATE;
                unit_2.PICKING_ID = sl_line_1.PICKING_ID;
                unit_2.PICKING_DATE = sl_line_1.PICKING_DATE;
                
                UnitObject.Update(unit_1);
                UnitObject.Update(unit_2);

                Commit();
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void DeleteCorrection(String correction_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                BeginTransaction();
                MPMWMS_CORRECTION_SL item = CorrectionSLObject.Item(correction_sl_id, dataarea_id, maindealer_id, site_id);
                if (item == null)
                {
                    throw new MPMException("Can't find current item [" + correction_sl_id + "] !!!");
                }

                // find at line retur 1
                MPMWMS_RETUR_SL retur_1 = ReturSLObject.Item(item.RETUR_SL_ID_1, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                if (retur_1 == null)
                {
                    throw new MPMException("Can't find retur item 1 [" + item.RETUR_SL_ID_1 + "] !!!");
                }
                List<MPMWMS_RETUR_SL_LINE> retur_lines_1 = ReturSLObject.ReturSLLineObject.List(retur_1);
                if (retur_lines_1 != null && retur_lines_1.Count() > 0)
                {
                    throw new MPMException("Can't delete because already exist line at [" + item.RETUR_SL_ID_1 + "] !!!");
                }
                retur_1.IS_CANCEL = "Y";

                // find at line retur 2
                MPMWMS_RETUR_SL retur_2 = ReturSLObject.Item(item.RETUR_SL_ID_2, item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                if (retur_2 == null)
                {
                    throw new MPMException("Can't find retur item 1 [" + item.RETUR_SL_ID_2 + "] !!!");
                }
                List<MPMWMS_RETUR_SL_LINE> retur_lines_2 = ReturSLObject.ReturSLLineObject.List(retur_2);
                if (retur_lines_2 != null && retur_lines_2.Count() > 0)
                {
                    throw new MPMException("Can't delete because already exist line at [" + item.RETUR_SL_ID_2 + "] !!!");
                }
                retur_2.IS_CANCEL = "Y";

                CorrectionSLObject.Delete(item);
                Commit();
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
    }
}
