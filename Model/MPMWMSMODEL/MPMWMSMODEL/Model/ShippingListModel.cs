﻿using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.ShippingList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Table.PickingTakeMapping;
using MPMWMSMODEL.Table.ShippingListLine;
using MPMWMSMODEL.Table.Unit;
using MPMWMSMODEL.Table.Picking;
using MPMLibrary.NET.Lib.Util;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.SalesTable;
using MPMWMSMODEL.Table.Faktur;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.xtsMuliaService;
using MPMWMSMODEL.Table.InventJournalTable;
using MPMWMSMODEL.Table.SalesTableAVPick;
using System.Transactions;
using System.Data.Linq;
using System.Diagnostics;
using MPMWMSMODEL.Table.CustInvoiceJour;
using System.IO;
using MPM365SERVICE.Service.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.MPMMuliaService;
using MPMWMSMODEL.Table.Expedition;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace MPMWMSMODEL.Model
{
    public class ShippingListModel : BaseModel
    {
        protected String ServiceGroup = "xtsMuliaService";

        //protected CallContext ContextWS = new CallContext();

        private PickingTakeMappingObject _PickingTakeMappingObject = new PickingTakeMappingObject();
        private ShippingListObject _ShippingListObject = new ShippingListObject();
        private ShippingListLineObject _ShippingListLineObject = new ShippingListLineObject();
        private PickingObject _PickingObject = new PickingObject();
        private FakturObject _FakturObject = new FakturObject();
        private LocationObject _LocationObject = new LocationObject();
        private SalesTableObject _SalesTableObject = new SalesTableObject();
        private SalesTableAVPickObject _SalesTableAVPickObject = new SalesTableAVPickObject(); 
        private InventJournalTableObject _InventJournalTableObject = new InventJournalTableObject();

        private ExpeditionObject _ExpeditionObject = new ExpeditionObject();

        public ShippingListModel()
        {
            _ShippingListObject.Context = (WmsUnitDataContext)Context;
            _ShippingListObject.ShippingListLineObject.Context = (WmsUnitDataContext)Context;
            _PickingTakeMappingObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.Context = (WmsUnitDataContext)Context;
            _FakturObject.Context = (WmsUnitDataContext)Context;
            _LocationObject.Context = (WmsUnitDataContext)Context;
            _SalesTableObject.Context = (WmsUnitDataContext)Context;
            _SalesTableAVPickObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.InventJournalTransObject.Context = (WmsUnitDataContext)Context;
            _ExpeditionObject.Context = (WmsUnitDataContext)Context;
        }

        public ShippingListObject ShippingListObject
        {
            get { return _ShippingListObject; }
        }

        public SalesTableAVPickObject SalesTableAVPickObject
        {
            get { return _SalesTableAVPickObject; }
        }

        /** WEB SERVICE **/
        //protected xtsMuliaService.xtsCreateSalesServiceClient ConnectWebService()
        //{
        //    try
        //    {
        //        System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
        //        bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
        //        bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

        //        ContextWS.Company = UserSession.DATAAREA_ID;
        //        ContextWS.LogonAsUser = LogonASUser;
        //        ContextWS.Language = "en-us";
        //        System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
        //            UrlNetTcpXtsMuliaService, LogonASUser);

        //        xtsMuliaService.xtsCreateSalesServiceClient client = new xtsMuliaService.xtsCreateSalesServiceClient(bind, xRemote);
        //        client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
        //        client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
        //        client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
        //        client.Open();

        //        return client;
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        //protected void DisconnectWebService(xtsMuliaService.xtsCreateSalesServiceClient client)
        //{
        //    client.Close();
        //    client = null;
        //}
        /** END OF WEB SERVICE **/

        public void RegisterShippingListToAX(MPMWMS_SHIPPING_LIST header, String isInternal)
        {
            String invoice_id_ax = "";
            // input to web service
            try
            {
                SalesService salesService = new SalesService();
                var resultAX = new ResultCommon();
                MPMSalesOrderParm[] param;
                //xtsMuliaService.xtsCreateSalesServiceClient client = ConnectWebService();

                foreach (var row in ShippingListObject.ShippingListLineObject.List(
                    header.SHIPPING_LIST_ID,
                    header.DATAAREA_ID,
                    header.MAINDEALER_ID,
                    header.SITE_ID,
                    header.PICKING_ID,
                    header.DO_ID))
                {
                    if (row.STATUS == "O") {
                        MPMWMS_UNIT unit = UnitObject.Item(row.DATAAREA_ID, row.MACHINE_ID);
                        if (unit == null)
                        {
                            throw new MPMException("Unit not found !!!");
                        }

                        if (row.FRAME_ID != "")
                        {
                            resultAX = new ResultCommon();
                            param = new MPMSalesOrderParm[1];
                            param[0] = new MPMSalesOrderParm();
                            param[0].parmCategoryCode = "U";
                            param[0].parmCustAccount = header.DEALER_CODE;
                            param[0].parmCustAddress = header.DEALER_ADDRESS;
                            param[0].parmNamePIC = "";
                            //param[0].parmloaddate = header.SHIPPING_DATE;
                            param[0].parmPhoneNumber = "";
                            //param[0].transdate
                            param[0].parmPoliceNumber = header.POLICE_NUMBER;


                            //MPMWMS_EXPEDITION eXPEDITION = Model._exp.it
                            MPMWMS_EXPEDITION expedition = _ExpeditionObject.Item(header.EXPEDITION_ID, header.DATAAREA_ID,
                                header.MAINDEALER_ID, header.SITE_ID);

                            param[0].parmCarrierName = expedition.CARRIERNAMEAX;//"TRIT - PT. TRISTAR TRANSINDO";//header.EXPEDITION_ID;
                            param[0].parmMainDealer = UserSession.MAINDEALER_ID;
                            param[0].parmShippingListId = header.SHIPPING_LIST_ID;
                            param[0].parmConfigId = "";
                            param[0].parmInventColorId = row.COLOR_TYPE;
                            param[0].parmInventSizeId = "";
                            param[0].parmFromSiteId = row.SITE_ID;
                            param[0].parmFromLocationId = unit.WAREHOUSE_ID;
                            param[0].parmInvoiceId = header.DO_ID;
                            param[0].parmItemId = row.UNIT_TYPE;
                            param[0].parmChassisNumber = row.FRAME_ID;
                            param[0].parmDescription = "";
                            param[0].parmDriverName = header.DRIVER;
                            param[0].parmIsInternal = isInternal;
                            resultAX = salesService.createShippingList(param, UserSession.DATAAREA_ID);

                            if (resultAX.Success == 1)
                            {
                                //invoice_id_ax = resultAX.Data;
                                invoice_id_ax = header.DO_ID;
                            }

                            //invoice_id_ax = client.createShippingList(
                            //    ContextWS,
                            //    "U",
                            //    header.DEALER_CODE,
                            //    header.DEALER_ADDRESS,
                            //    "",
                            //    header.SHIPPING_DATE,
                            //    "",
                            //    header.SHIPPING_DATE,
                            //    header.POLICE_NUMBER,
                            //    header.EXPEDITION_ID,
                            //    UserSession.MAINDEALER_ID,
                            //    header.SHIPPING_LIST_ID,
                            //    "",
                            //    row.COLOR_TYPE,
                            //    "",
                            //    row.SITE_ID,
                            //    unit.WAREHOUSE_ID,
                            //    header.DO_ID,
                            //    row.UNIT_TYPE,
                            //    row.FRAME_ID,
                            //    "",
                            //    header.DRIVER,
                            //    isInternal);

                            if (invoice_id_ax == null || invoice_id_ax == "")
                            {
                                throw new MPMException("Invoice AX can't be retrieve !!!");
                            }
                        }
                    }
                }

                //DisconnectWebService(client);
                header.INVOICE_ID_AX = invoice_id_ax;
                SubmitChanges();
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void SendToAX(String dataarea_id, String maindealer_id, String site_id)
        {
            List<MPMWMS_SHIPPING_LIST> list = ShippingListObject.ListInvoiceAXEmpty(dataarea_id, maindealer_id, site_id);
            foreach (var header in list)
            {
                // find picking 
                MPMWMS_DO_AVPICK do_avpick = SalesTableAVPickObject.Item(header.DO_ID, header.DATAAREA_ID, header.MAINDEALER_ID, header.SITE_ID);
                //if (do_avpick.IS_INTERNAL == "N")
                //{
                    if (header.INVOICE_ID_AX == null || header.INVOICE_ID_AX == "")
                    {
                        try
                        {
                            //WS 365
                            RegisterShippingListToAX(header, do_avpick.IS_INTERNAL);
 
                            /*
                            String dealer_code = header.DEALER_CODE;
                            String dealer_code_mulia = "";

                            IQueryable<CUSTTABLE> custtable =
                                from c in ((WmsUnitDataContext)Context).CUSTTABLEs
                                where c.ACCOUNTNUM == dealer_code
                                select c;
                            if (custtable != null)
                            {
                                dealer_code_mulia = custtable.Take(1).FirstOrDefault().XTSKODECUSTMULIA;
                            }

                           
                            if (dealer_code_mulia != "")
                            {
                                List<vMPM_UMSL> umsls = ShippingListObject.UMSL(
                                    header.SHIPPING_LIST_ID,
                                    header.DATAAREA_ID,
                                    header.MAINDEALER_ID,
                                    header.SITE_ID,
                                    header.DO_ID);
                                
                                bool folderExists = Directory.Exists(@"c:\\UMSL\\" + dealer_code_mulia);
                                if (!folderExists)
                                    Directory.CreateDirectory(@"c:\\UMSL\\" + dealer_code_mulia);

                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\\UMSL\\" + dealer_code_mulia + "\\" + dealer_code_mulia + "_" + header.SHIPPING_LIST_ID.Replace("/", "") + ".umsl"))
                                {
                                    foreach (var umls in umsls)
                                    {
                                        String text = umls.SHIPPING_LIST_ID + ";" +
                                            umls.SHIPPING_DATE + ";" +
                                            umls.DEALER_CODE + ";" +
                                            umls.DEALER_SUB_CODE + ";" +
                                            umls.UNIT_TYPE + ";" +
                                            umls.COLOR_TYPE + ";" +
                                            umls.FRAME_ID + ";" +
                                            umls.MACHINE_ID + ";" +
                                            umls.DO_ID + ";" +
                                            umls.EXPEDITION_ID + ";" +
                                            umls.PDI;
                                        file.WriteLine(text);
                                    }
                                }
                                
                            }
                            */
                        }
                        catch (Exception)
                        {
                            //throw new MPMException(e.Message);
                        }
                    }
                    else
                    {
                        throw new MPMException("Already in AX !!!");
                    }
                //}
            }
        }

        public void ReSendToAX(String dataarea_id, String maindealer_id, String site_id, String shippinglist_id)
        {
            MPMWMS_SHIPPING_LIST header = ShippingListObject.Item(shippinglist_id, dataarea_id, maindealer_id, site_id);
            if (header != null)
            {
                // find picking 
                MPMWMS_DO_AVPICK do_avpick = SalesTableAVPickObject.Item(header.DO_ID, header.DATAAREA_ID, header.MAINDEALER_ID, header.SITE_ID);
                //if (do_avpick.is_internal == "N")
                //{
                    try
                    {
                        RegisterShippingListToAX(header, do_avpick.IS_INTERNAL);

                        /*
                        String dealer_code = header.DEALER_CODE;
                        String dealer_code_mulia = "";

                        IQueryable<CUSTTABLE> custtable =
                            from c in ((WmsUnitDataContext)Context).CUSTTABLEs
                            where c.ACCOUNTNUM == dealer_code
                            select c;
                        if (custtable != null)
                        {
                            dealer_code_mulia = custtable.Take(1).FirstOrDefault().XTSKODECUSTMULIA;
                        }

                        if (dealer_code_mulia != "")
                        {
                            List<vMPM_UMSL> umsls = ShippingListObject.UMSL(
                                header.SHIPPING_LIST_ID,
                                header.DATAAREA_ID,
                                header.MAINDEALER_ID,
                                header.SITE_ID,
                                header.DO_ID);
                            bool folderExists = Directory.Exists(@"c:\\UMSL\\" + dealer_code_mulia);
                            if (!folderExists)
                                Directory.CreateDirectory(@"c:\\UMSL\\" + dealer_code_mulia);

                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\\UMSL\\" + dealer_code_mulia + "\\" + dealer_code_mulia + "_" + header.SHIPPING_LIST_ID.Replace("/", "") + ".umsl"))
                            {
                                foreach (var umls in umsls)
                                {
                                    String text = umls.SHIPPING_LIST_ID + ";" +
                                        umls.SHIPPING_DATE + ";" +
                                        umls.DEALER_CODE + ";" +
                                        umls.DEALER_SUB_CODE + ";" +
                                        umls.UNIT_TYPE + ";" +
                                        umls.COLOR_TYPE + ";" +
                                        umls.FRAME_ID + ";" +
                                        umls.MACHINE_ID + ";" +
                                        umls.DO_ID + ";" +
                                        umls.EXPEDITION_ID + ";" +
                                        umls.PDI;
                                    file.WriteLine(text);
                                }
                            }
                        }
                        */
                    }
                    catch (Exception e)
                    {
                        throw new MPMException(e.Message);
                    }
                //}
                //else
                //{
                //    throw new MPMException("Shipping List not found !!!");
                //}
            }
        }

        private MPMWMS_SHIPPING_LIST InsertToHeaderSL(MPMWMS_PICKING picking, String NumberShippingList)
        {
            MPMWMS_SHIPPING_LIST _NewRowMPMWMS_SHIPPING_LIST = new MPMWMS_SHIPPING_LIST();
            _NewRowMPMWMS_SHIPPING_LIST.SHIPPING_LIST_ID = NumberShippingList;
            _NewRowMPMWMS_SHIPPING_LIST.SHIPPING_DATE = picking.PICKING_DATE; // sementara
            _NewRowMPMWMS_SHIPPING_LIST.DATAAREA_ID = picking.DATAAREA_ID;
            _NewRowMPMWMS_SHIPPING_LIST.MAINDEALER_ID = picking.MAINDEALER_ID;
            _NewRowMPMWMS_SHIPPING_LIST.PICKING_ID = picking.PICKING_ID;
            _NewRowMPMWMS_SHIPPING_LIST.PICKING_DATE = picking.PICKING_DATE;
            _NewRowMPMWMS_SHIPPING_LIST.DO_ID = picking.DO_ID;
            _NewRowMPMWMS_SHIPPING_LIST.DO_DATE = picking.DO_DATE;
            _NewRowMPMWMS_SHIPPING_LIST.DEALER_CODE = picking.DEALER_CODE;
            _NewRowMPMWMS_SHIPPING_LIST.DEALER_NAME = picking.DEALER_NAME;
            _NewRowMPMWMS_SHIPPING_LIST.DEALER_ADDRESS = picking.DEALER_ADDRESS;
            _NewRowMPMWMS_SHIPPING_LIST.DEALER_CITY = picking.DEALER_CITY;
            _NewRowMPMWMS_SHIPPING_LIST.EXPEDITION_ID = picking.EXPEDITION_ID;
            _NewRowMPMWMS_SHIPPING_LIST.POLICE_NUMBER = picking.POLICE_NUMBER;
            _NewRowMPMWMS_SHIPPING_LIST.DRIVER = picking.DRIVER;
            _NewRowMPMWMS_SHIPPING_LIST.DESCRIPTION = picking.DESCRIPTION;
            _NewRowMPMWMS_SHIPPING_LIST.STATUS = "O";
            _NewRowMPMWMS_SHIPPING_LIST.IS_CANCEL = "N";
            _NewRowMPMWMS_SHIPPING_LIST.IS_PRINT = "1";
            _ShippingListObject.Insert(_NewRowMPMWMS_SHIPPING_LIST);

            return _NewRowMPMWMS_SHIPPING_LIST;
        }

        private MPMWMS_SHIPPING_LIST_LINE InsertToDetailSL(MPMWMS_SHIPPING_LIST shipping, 
            MPMWMS_PICKING_TAKE_MAPPING picking_take,
            int nourut)
        {
            MPMWMS_SHIPPING_LIST_LINE _NewRowMPMWMS_SHIPPING_LIST_LINE = new MPMWMS_SHIPPING_LIST_LINE();

            _NewRowMPMWMS_SHIPPING_LIST_LINE.SHIPPING_LIST_ID = shipping.SHIPPING_LIST_ID;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.SHIPPING_DATE = shipping.PICKING_DATE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.DATAAREA_ID = shipping.DATAAREA_ID;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.MAINDEALER_ID = shipping.MAINDEALER_ID;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.PICKING_ID = shipping.PICKING_ID;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.PICKING_DATE = shipping.PICKING_DATE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.DO_ID = shipping.DO_ID;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.DO_DATE = shipping.DO_DATE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.MACHINE_ID = picking_take.MACHINE_ID_TAKE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.FRAME_ID = picking_take.FRAME_ID_TAKE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.COLOR_TYPE = picking_take.COLOR_TYPE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.UNIT_TYPE = picking_take.UNIT_TYPE;
            _NewRowMPMWMS_SHIPPING_LIST_LINE.STATUS = "O";
            _NewRowMPMWMS_SHIPPING_LIST_LINE.NO_URUT = nourut;

            _ShippingListObject.ShippingListLineObject.Insert(_NewRowMPMWMS_SHIPPING_LIST_LINE);

            return _NewRowMPMWMS_SHIPPING_LIST_LINE;
        }
        public List<ShippingListRec> ExcludeRekapSL(DateTime slDate, String dataarea, String site_id)
        {
            try
            {


                WmsUnitDataContext ctx = new WmsUnitDataContext();
                ctx.CommandTimeout = 300;
                String QueryitemsObj = "EXEC MPM_EKSPEDISI_NO_REKAP '" + site_id + "', '" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "' ";
                var list = Context.ExecuteQuery<ShippingListRec>(QueryitemsObj).ToList(); ;
                var result = from a in list
                             select new ShippingListRec
                             {
                                 EXPEDITION_ID = a.EXPEDITION_ID
                             };
                return result.ToList();
                

                //return Context.ExecuteQuery<EkspedisiBelumRekap_Record>(QueryitemsObj).ToList();


            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }
        public void GenerateRekapSL(DateTime slDate, String dataarea, String site_id)
        {
            try
            {
                
               
                WmsUnitDataContext ctx = new WmsUnitDataContext();
                ctx.CommandTimeout = 300;
                //ctx.MPM_GENERATE_SLU_REKAP(slDate.Date,slDate.Date, site_id, dataarea);
                String QueryitemsObj = "EXEC MPM_GENERATE_SLU_REKAP '"+ DateTime.Now.Date.ToString("yyyy-MM-dd") + "', '" + DateTime.Now.Date.ToString("yyyy-MM-dd") +"','"+site_id+"','"+ dataarea+"'";
                var list = Context.ExecuteQuery<ShippingListRec>(QueryitemsObj).ToList();

            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_FAKTUR_SL_MAPPING_Record> ListFaktur(String _dataArea, String _mainDealer, String _siteId, String _doId)
        {
            try
            {
                string query = @"SELECT a.*
                        FROM MPMWMS.DBO.MPMWMS_FAKTUR_SL_MAPPING a (NOLOCK)
                        JOIN MPMWMS.DBO.MPMWMS_SHIPPING_LIST sl (NOLOCK)
                        ON a.DO_ID = sl.DO_ID
                        AND a.SHIPPING_LIST_ID = sl.SHIPPING_LIST_ID
                        JOIN MPMAX.DBO.MPMINVFAKTUR f (NOLOCK)
                        ON f.ENGINENO = a.MACHINE_ID
                        WHERE a.DATAAREA_ID = '{0}' 
                        AND a.MAINDEALER_ID = '{1}'
                        AND a.SITE_ID =  '{2}' 
                        
                        AND f.NOSLCAB = ''
                        AND f.PARTITION = 5637144576
						and f.DATAAREAID = 'mml'
                        ";
                //AND CONVERT(DATE, sl.CREATE_DATE) = CONVERT(DATE, GETDATE()) 
                //AND DO_ID IN ({3})
                //AND a.SHIPPING_LIST_ID = 'SLU/00000003/10/2020/M2Z/GD1'
                string querytemp = String.Format(query, _dataArea, _mainDealer, _siteId, _doId);
                //var result = Context.ExecuteQuery<MPMWMS_FAKTUR_SL_MAPPING_Record>(query, _dataArea, _mainDealer, _siteId, _doId).ToList();
                var result = Context.ExecuteQuery<MPMWMS_FAKTUR_SL_MAPPING_Record>(querytemp).ToList();
                return result;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void SyncWithSalesAx(String dataarea_id, String maindealer_id, String site_id, String truck_id = "")
        {
            try
            {
                //_ShippingListObject.Context.CommandTimeout = 30;
                //_ShippingListObject.Context.MPM_SP_GENERATE_SHIPPING_LIST(dataarea_id, maindealer_id, site_id, truck_id, UserSession.NPK);
                //System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Server=mml-lsn-dbax;Database=MML_AX_LIVE;User ID=appreader;Password=Simpang4244;pooling=false;MultipleActiveResultSets=True;Connection Timeout=300");

                //System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Server=10.10.101.96;Database=MPMWMS;User ID=appreader;Password=Simpang4244;pooling=false;MultipleActiveResultSets=True;Connection Timeout=300");

                //System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Server=LIVE-365-DB3;Database=MPMWMS;User ID=appreader;Password=Simpang4244;pooling=false;MultipleActiveResultSets=True;Connection Timeout=300");

                var conString = System.Configuration.ConfigurationManager.ConnectionStrings["MPMWMS_SL"].ConnectionString;

                System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(conString);

                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("set arithabort on;", conn);
                command.Connection.Open();
                command.ExecuteNonQuery();
                WmsUnitDataContext ctx = new WmsUnitDataContext(conn);
                ctx.CommandTimeout = 300;
                ctx.MPM_SP_GENERATE_SHIPPING_LIST(dataarea_id, maindealer_id, site_id, truck_id, UserSession.NPK);


                var fakturs = this.ListFaktur(dataarea_id, maindealer_id, site_id, "");
                SalesService salesService = new SalesService();
                var resultAX = new ResultCommon();
                MPMInvFakturParm[] invfakturparm;
                foreach (var a in fakturs)
                {
                    invfakturparm = new MPMInvFakturParm[1];
                    invfakturparm[0] = new MPMInvFakturParm();
                    invfakturparm[0].parmKodeDealer = a.DEALER_CODE;
                    invfakturparm[0].parmKodeDealerAx = a.DEALER_CODE_AX;
                    invfakturparm[0].parmMachineId = a.MACHINE_ID;
                    invfakturparm[0].parmNoDoCab = a.DO_ID;
                    invfakturparm[0].parmNoSlCab = a.SHIPPING_LIST_ID;
                    invfakturparm[0].parmTglDoCab = a.DO_DATE;
                    invfakturparm[0].parmTglSlCab = a.SHIPPING_DATE;
                    resultAX = salesService.updateInvFakturCorrectionUnit(invfakturparm, dataarea_id);
                    if (resultAX.Message != "" && resultAX.Success == 0)
                    {
                        throw new MPMException(resultAX.Message);
                    }

                    var api_umsl = ConfigurationManager.AppSettings["API_UMSL"];
                    if (!string.IsNullOrEmpty(api_umsl))
                    {

                        HeaderUMSL requestUMSL = new HeaderUMSL();
                        List<DocumentUMSL> listObj = Get_UMSL_Document(a.SHIPPING_LIST_ID, dataarea_id, maindealer_id);
                        if (listObj != null && listObj.Count > 0)
                        {
                            requestUMSL.Driver = listObj[0].DRIVER;
                            requestUMSL.Ekspedisi = listObj[0].NAME;
                            requestUMSL.mainDealer = maindealer_id;
                            requestUMSL.principalCode = listObj[0].PRINCIPALCODE;
                            requestUMSL.mdCode = listObj[0].MDCODE;
                            requestUMSL.NoPolisi = listObj[0].POLICE_NUMBER;
                            requestUMSL.noSL = listObj[0].SHIPPING_LIST_ID;
                            requestUMSL.rekapSLID = listObj[0].REKAPSL_ID;
                            requestUMSL.tglSL = listObj[0].SLDATE.Year.ToString() + "-" + listObj[0].SLDATE.Month.ToString() + "-" + listObj[0].SLDATE.Day.ToString();
                            requestUMSL.detail = new List<DetailUMSL>();

                            foreach (var item in listObj)
                            {
                                DetailUMSL detumsl = new DetailUMSL();
                                detumsl.colorId = item.COLOR_TYPE;
                                detumsl.invoiceId = item.DO_ID;
                                detumsl.itemId = item.UNIT_TYPE;
                                detumsl.noMesin = item.MESIN;
                                detumsl.noRangka = item.FRAME_ID;
                                detumsl.tahunProduksi = Convert.ToInt32(item.UNIT_YEAR);
                                detumsl.unitDisc = 0;
                                requestUMSL.detail.Add(detumsl);
                            }

                            Call_API_UMSL(requestUMSL);
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }

            /*String LocationId = null;
            String WarehouseId = null;

            try
            {
                var vo_proses1 =
                        from a in ((WmsUnitDataContext)Context).MPMWMS_PICKINGs
                        where
                            a.DATAAREA_ID == dataarea_id
                            && a.MAINDEALER_ID == maindealer_id
                            && a.SITE_ID == site_id
                            && a.STATUS == "F"
                            && (a.POLICE_NUMBER == truck_id || truck_id == "")
                        group a by new { a.DATAAREA_ID, a.PICKING_ID, a.DO_ID } into group_a
                        select
                        new
                        {
                            group_a.Key.DATAAREA_ID,
                            group_a.Key.PICKING_ID,
                            group_a.Key.DO_ID
                        };

                List<String> listNoSL = new List<String>();

                Stopwatch stopWatch = new Stopwatch();
                long calc = 0;
                stopWatch.Start();

                foreach (var row_proses1 in vo_proses1.ToList())
                {
                    try
                    {
                        String NumberShippingList = RunningNumberShippingListUnit(
                            dataarea_id,
                            maindealer_id,
                            site_id);
                        listNoSL.Add(NumberShippingList);

                        BeginTransaction();

                        //cari keterangan dealer
                        MPMWMS_PICKING row_proses2 = _PickingObject.Item(
                            row_proses1.PICKING_ID,
                            dataarea_id,
                            maindealer_id,
                            site_id, row_proses1.DO_ID);
                        if (row_proses2 == null)
                        {
                            throw new MPMException("Can't find description dealer !!!");
                        }
                        //end cari keterangan dealer                                                       

                        // insert to header SL
                        MPMWMS_SHIPPING_LIST shipping = InsertToHeaderSL(row_proses2, NumberShippingList);

                        Debug.WriteLine("Insert SL :" + NumberShippingList + " " + (stopWatch.ElapsedMilliseconds - calc));
                        calc = stopWatch.ElapsedMilliseconds;

                        // mencari detail
                        int nourut = 1;
                        foreach (var row_proses3 in _PickingTakeMappingObject.List(
                            row_proses1.PICKING_ID,
                            dataarea_id,
                            maindealer_id,
                            site_id))
                        {
                            MPMWMS_SHIPPING_LIST_LINE shipping_line = InsertToDetailSL(shipping, row_proses3, nourut);

                            Debug.WriteLine("Insert Detail SL [" + nourut + "] :" + NumberShippingList + " " + (stopWatch.ElapsedMilliseconds - calc));
                            calc = stopWatch.ElapsedMilliseconds;
                            nourut++;

                            //update mpmwms_unit lama
                            MPMWMS_UNIT _RowMPMWMS_UNIT_LAMA = UnitObject.Item(
                                row_proses3.MACHINE_ID,
                                row_proses3.FRAME_ID,
                                row_proses3.DATAAREA_ID);
                            if (_RowMPMWMS_UNIT_LAMA == null)
                            {
                                throw new MPMException("Can't find OLD UNIT Data !!!");
                            }
                            _RowMPMWMS_UNIT_LAMA.PICKING_ID = null;
                            _RowMPMWMS_UNIT_LAMA.PICKING_DATE = null;
                            _RowMPMWMS_UNIT_LAMA.SHIPPING_ID = null;
                            _RowMPMWMS_UNIT_LAMA.SHIPPING_DATE = null;
                            _RowMPMWMS_UNIT_LAMA.DO_ID = null;
                            _RowMPMWMS_UNIT_LAMA.DO_DATE = null;
                            _RowMPMWMS_UNIT_LAMA.STATUS = "RFS";
                            _RowMPMWMS_UNIT_LAMA.IS_RFS = "Y";
                            _RowMPMWMS_UNIT_LAMA.IS_READYTOPICK = "N";
                            _RowMPMWMS_UNIT_LAMA.IS_READYTOPICK_AX = "N";
                            UnitObject.Update(_RowMPMWMS_UNIT_LAMA, "PIC");
                            //end update mpmwms_unit_lama

                            //update mpmwms_unit baru
                            MPMWMS_UNIT _RowMPMWMS_UNIT_BARU = UnitObject.Item(
                                row_proses3.MACHINE_ID_TAKE,
                                row_proses3.FRAME_ID_TAKE,
                                row_proses3.DATAAREA_ID);
                            if (_RowMPMWMS_UNIT_BARU == null)
                            {
                                throw new MPMException("Can't find NEW UNIT Data !!!");
                            }
                            _RowMPMWMS_UNIT_BARU.PICKING_ID = shipping.PICKING_ID;
                            _RowMPMWMS_UNIT_BARU.PICKING_DATE = shipping.PICKING_DATE;
                            _RowMPMWMS_UNIT_BARU.SHIPPING_ID = shipping.SHIPPING_LIST_ID;
                            _RowMPMWMS_UNIT_BARU.SHIPPING_DATE = shipping.SHIPPING_DATE;
                            _RowMPMWMS_UNIT_BARU.DO_ID = shipping.DO_ID;
                            _RowMPMWMS_UNIT_BARU.DO_DATE = shipping.DO_DATE;
                            _RowMPMWMS_UNIT_BARU.STATUS = "S";
                            _RowMPMWMS_UNIT_BARU.IS_RFS = "N";
                            _RowMPMWMS_UNIT_BARU.IS_READYTOPICK = "N";
                            _RowMPMWMS_UNIT_BARU.IS_READYTOPICK_AX = "N";

                            String warehouse_id_old = _RowMPMWMS_UNIT_BARU.WAREHOUSE_ID;
                            String location_id_old = _RowMPMWMS_UNIT_BARU.LOCATION_ID;
                            LocationId = GetLocation(_RowMPMWMS_UNIT_BARU.DATAAREA_ID, _RowMPMWMS_UNIT_BARU.SITE_ID, "E");
                            if (LocationId == null)
                            {
                                throw new MPMException("Can't get location id");
                            }
                            WarehouseId = LocationObject.GetWarehouse(_RowMPMWMS_UNIT_BARU.DATAAREA_ID, _RowMPMWMS_UNIT_BARU.SITE_ID, LocationId);
                            if (WarehouseId == null)
                            {
                                throw new MPMException("Can't get warehouse id");
                            }
                            _RowMPMWMS_UNIT_BARU.WAREHOUSE_ID = WarehouseId;
                            _RowMPMWMS_UNIT_BARU.LOCATION_ID = LocationId;
                            UpdateLocationStock(
                                _RowMPMWMS_UNIT_BARU.DATAAREA_ID,
                                _RowMPMWMS_UNIT_BARU.SITE_ID,
                                warehouse_id_old,
                                location_id_old, -1);

                            UpdateLocationStock(
                                _RowMPMWMS_UNIT_BARU.DATAAREA_ID,
                                _RowMPMWMS_UNIT_BARU.SITE_ID,
                                WarehouseId,
                                LocationId, 1);                           
                            UnitObject.Update(_RowMPMWMS_UNIT_BARU, "SLD", _RowMPMWMS_UNIT_BARU.SITE_ID, warehouse_id_old, location_id_old);
                            // end update mpmwms unit

                            SALESTABLE _RowSALESTABLE = _SalesTableObject.Item(
                                row_proses2.DO_ID,
                                row_proses2.DATAAREA_ID,
                                row_proses2.MAINDEALER_ID,
                                row_proses2.SITE_ID);
                            if (_RowSALESTABLE == null)
                            {
                                throw new MPMException("Can't get sales table");
                            }

                            MPMWMS_DO_AVPICK rowDOAVPick = _SalesTableAVPickObject.Item(
                                row_proses2.DO_ID,
                                row_proses2.DATAAREA_ID,
                                row_proses2.MAINDEALER_ID,
                                row_proses2.SITE_ID);
                            if (rowDOAVPick == null)
                            {
                                throw new MPMException("Can't get sales av pick");
                            }
                            rowDOAVPick.QUANTITY_SL++;

                            INVENTJOURNALTABLE _RowJOURNALTABLE = _InventJournalTableObject.Item(
                                row_proses3.DATAAREA_ID,
                                row_proses2.DO_ID);
                            
                            MPMINVFAKTUR _RowFaktur = _FakturObject.Item(
                                row_proses3.DATAAREA_ID,
                                row_proses3.MACHINE_ID_TAKE,
                                row_proses3.FRAME_ID_TAKE);

                            if (_RowFaktur != null)
                            {
                                _RowFaktur.NOSLCAB = NumberShippingList;
                                _RowFaktur.TGLSLCAB = row_proses2.PICKING_DATE; // sementara
                                _RowFaktur.NODOCAB = row_proses2.DO_ID;
                                if (_RowSALESTABLE != null)
                                {
                                    _RowFaktur.TGLDOCAB = _RowSALESTABLE.XTSTRANSDATE;
                                }
                                else if (_RowJOURNALTABLE != null)
                                {
                                    _RowFaktur.TGLDOCAB = _RowJOURNALTABLE.POSTEDDATETIME;
                                }
                            }

                            Debug.WriteLine("Update Faktur :" + row_proses2.PICKING_ID + " " + (stopWatch.ElapsedMilliseconds - calc));
                            calc = stopWatch.ElapsedMilliseconds;

                            //update picking take mapping
                            MPMWMS_PICKING_TAKE_MAPPING _RowMPMWMS_PICKING_TAKE_MAPPING = _PickingTakeMappingObject.Item(
                                row_proses2.PICKING_ID,
                                row_proses2.DATAAREA_ID,
                                row_proses2.MAINDEALER_ID,
                                row_proses2.SITE_ID,
                                row_proses2.DO_ID,
                                row_proses3.MACHINE_ID,
                                row_proses3.MACHINE_ID_TAKE);
                            if (_RowMPMWMS_PICKING_TAKE_MAPPING == null)
                            {
                                throw new MPMException("Can't get take mapping picking");
                            }

                            _RowMPMWMS_PICKING_TAKE_MAPPING.STATUS = "P";
                        }

                        // update picking
                        row_proses2.STATUS = "P";

                        Commit();

                        Debug.WriteLine("Commit :" + NumberShippingList + " " + (stopWatch.ElapsedMilliseconds - calc));
                        Debug.WriteLine("");
                        calc = stopWatch.ElapsedMilliseconds;
                    }
                    catch (MPMException e)
                    {
                        Rollback();
                        throw new MPMException(e.Message);
                    }
                }
                stopWatch.Stop();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
             * */
        }

        public void UpdateShippingPrint(String shipping_id, String dataarea_id, String maindealer_id, String site_id, String state)
        {
            try
            {
                MPMWMS_SHIPPING_LIST shipping = ShippingListObject.Item(shipping_id, dataarea_id, maindealer_id, site_id);
                if (shipping != null)
                {
                    if (state.ToLower().Equals("false"))
                    {
                        shipping.IS_PRINT = "0";
                        
                    }
                    else
                    {
                        if(shipping.REKAPSL_ID != null  )
                        {
                            if(shipping.REKAPSL_ID != "")
                            {
                                shipping.IS_PRINT = "1";
                                if (shipping.PRINT_DATE == null)
                                {
                                    shipping.PRINT_DATE = DateTime.Now;
                                }
                            }
                            
                        }else
                        {
                            shipping.IS_PRINT = "2";
                        }
                       
                    }
                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertLog(MPM_MDMS_LOGSENDSDM log)
        {
            BeginTransaction();

            try
            {
                _ShippingListObject.InsertLog(log);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Call_API_UMSL(HeaderUMSL requestUMSL)
        {
            MPM_MDMS_LOGSENDSDM log = new MPM_MDMS_LOGSENDSDM();
            try
            {
                var api_umsl = ConfigurationManager.AppSettings["API_UMSL"];
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(api_umsl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(requestUMSL);
                    log.JSONSEND = json;
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    log.JSONRETURN = result;
                    InsertLog(log);
                }
            }
            catch (Exception e)
            {
                log.JSONRETURN = "Error : " + e.Message;
                InsertLog(log);
            }
        }

        public List<DocumentUMSL> Get_UMSL_Document(string shiplistno, String dataarea, String kddlr)
        {
            try
            {
                WmsUnitDataContext ctx = new WmsUnitDataContext();
                ctx.CommandTimeout = 3600;
                String queryumsl = "EXEC MPM_SP_GET_UMSL_DOCUMENT '" + shiplistno + "', '" +
                    //+ DateTime.Now.Date.ToString("yyyy-MM-dd") + "','" + 
                    dataarea + "','" + kddlr + "'";
                var list = Context.ExecuteQuery<DocumentUMSL>(queryumsl).ToList();
                return list;
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
