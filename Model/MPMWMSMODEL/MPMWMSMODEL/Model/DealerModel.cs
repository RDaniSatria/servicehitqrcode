﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Mvc.Models;
using MPMWMSMODEL.Table.RouteDealer;

namespace MPMWMSMODEL.Model
{
    public class DealerModel : MPMModel
    {
        private DealerObject _dealerObject = new DealerObject();

        public DealerModel()
            : base()
        {
            Context = new WmsUnitDataContext();
            _dealerObject.Context = (WmsUnitDataContext)Context;
        }

        public Boolean Update(String accountnum, String zone, Decimal ongkos, Decimal lat, Decimal _long, Decimal distance)
        {
            return _dealerObject.Update(accountnum, zone, ongkos, lat, _long, distance);
        }

        public DealerObject.EDIT_DEALER getDealerInfo(String dealercode)
        {
            return _dealerObject.getDealerInfo(dealercode);
        }


    }
}
