﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.EADUnit;
using MPMWMSMODEL.Table.RouteEAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class RouteEADModel : BaseModel
    {
        public RouteEADObject _RouteEADObject = new RouteEADObject();
        private RouteListEADObject _RouteListEADObject = new RouteListEADObject();

        public RouteEADModel()
            : base()
        {
            _RouteEADObject.Context = (WmsUnitDataContext)Context;
            _RouteListEADObject.Context = (WmsUnitDataContext)Context;
        }

        #region MPMWMS_EAD_ROUTE_DEALER_UNIT

        public String Insert(MPMWMS_EAD_ROUTE_DEALER_UNIT _item)
        {
            BeginTransaction();
            try
            {
                var routeID = _RouteEADObject.Insert(_item);

                Commit();

                return routeID;
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_ROUTE_DEALER_UNIT _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_ROUTE_DEALER_UNIT data = _RouteEADObject.Item(_item.ROUTE_ID);

                if (data == null)
                {
                    throw new MPMException("Can't find the route !!!");
                }

                _RouteEADObject.Update(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_ROUTE_DEALER_UNIT _item)
        {
            BeginTransaction();
            try
            { 
                // CEK DATA DETAIL RUTE TERLEBIH DAHULU
                var detail = _RouteListEADObject.ViewRouteList(_item.ROUTE_ID);

                if(detail.Count > 0)
                {
                    throw new MPMException("Data rute " + _item.ROUTE_ID + " tidak bisa dihapus, karena memiliki detail rute !!!");
                }

                // KALAU TIDAK MEMILIK DATA DETAIL RUTE, BOLEH MELAKUKAN DELETE
                MPMWMS_EAD_ROUTE_DEALER_UNIT data = _RouteEADObject.Item(_item.ROUTE_ID);

                if (data == null)
                {
                    throw new MPMException("Can't find the route !!!");
                }

                _RouteEADObject.Delete(data);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_ROUTE_DEALER_UNIT> List()
        {
            return _RouteEADObject.ItemAll();
        }
        public List<RouteMulti_REC> ListAllMulti(string routeid)
        {
            return _RouteEADObject.ItemAllMulti(routeid);
        }

        public MPMWMS_EAD_ROUTE_DEALER_UNIT Item(String _routeId)
        {
            return _RouteEADObject.Item(_routeId);
        }

        #endregion


        #region MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST

        public List<ViewRouteListRecord> ViewRouteList(String _routeId)
        {
            return _RouteListEADObject.ViewRouteList(_routeId);
        }

        public void InsertUnitList(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST data = _RouteListEADObject.Item(_item.ROUTE_ID, _item.DEALER_CODE);

                if (data != null)
                {
                    throw new MPMException("Data already exist !!!");
                }

                _RouteListEADObject.Insert(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void UpdateUnitList(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST data = _RouteListEADObject.Item(_item.ROUTE_ID, _item.DEALER_CODE);

                if (data == null)
                {
                    throw new MPMException("Can't find the route list !!!");
                }

                _RouteListEADObject.Update(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void UpdateMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI data = _RouteEADObject.ItemMultibyId(_item.ID);

                if (data == null)
                {
                    throw new MPMException("Can't find the route list !!!");
                }

                _RouteEADObject.UpdateMulti(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void DeleteUnitList(MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST data = _RouteListEADObject.Item(_item.ROUTE_ID, _item.DEALER_CODE);

                if (data == null)
                {
                    throw new MPMException("Can't find the route list !!!");
                }

                _RouteListEADObject.Delete(data);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST> UnitList()
        {
            return _RouteListEADObject.ItemAll();
        }

        public MPMWMS_EAD_ROUTE_DEALER_UNIT_LIST UnitListItem(String _routeId, String _delaerCode)
        {
            return _RouteListEADObject.Item(_routeId, _delaerCode);
        }

        #endregion

        #region MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI
        public RouteMulti_REC ItemWithREC(String _routeId)
        {
            return _RouteEADObject.ItemWithREC(_routeId);
        }

        public String InsertMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI _item)
        {
            BeginTransaction();
            try
            {
                var routeID = _RouteEADObject.InsertMulti(_item);

                Commit();

                return routeID;
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }
        public MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI ItemMulti(String _routeId, string multiroute)
        {
            return _RouteEADObject.ItemDeleteMulti(_routeId, multiroute);
        }

       

        public void DeleteMulti(MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI _item)
        {
            BeginTransaction();
            try
            {
               
                MPMWMS_EAD_ROUTE_DEALER_UNIT_MULTI data = _RouteEADObject.ItemDeleteMulti(_item.ROUTE_ID, _item.MULTI_ROUTE_ID);

                if (data == null)
                {
                    throw new MPMException("Can't find the route !!!");
                }

                _RouteEADObject.DeleteMulti(data);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        #endregion

    }
}
