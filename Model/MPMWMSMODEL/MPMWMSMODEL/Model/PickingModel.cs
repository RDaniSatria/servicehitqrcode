﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.EcoResProduct;
using MPMWMSMODEL.Table.InventDim;
using MPMWMSMODEL.Table.InventJournalTable;
using MPMWMSMODEL.Table.Picking;
using MPMWMSMODEL.Table.PickingLine;
using MPMWMSMODEL.Table.PickingTakeMapping;
using MPMWMSMODEL.Table.SalesLine;
using MPMWMSMODEL.Table.SalesTable;
using MPMWMSMODEL.Table.SalesTableAVPick;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Table.ReturSL;
using System.Transactions;
using MPMWMSMODEL.Table.CustInvoiceJour;

namespace MPMWMSMODEL.Model
{
    public class SALESLINE_CUSTOM
    {
        public String SALES_ID { get; set; }
        public String DATAAREA_ID { get; set; }
        public String MAINDEALER_ID { get; set; }
        public String SITE_ID { get; set; }
        public String ITEM_ID { get; set; }
        public String UNIT_TYPE { get; set; }
        public String COLOR_ID { get; set; }
        public String UNIT_YEAR { get; set; }
        public int QTY { get; set; }

        public SALESLINE_CUSTOM()
        {
        }

        public SALESLINE_CUSTOM(
            String sales_id,
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String item_id,
            String unit_type,
            String color_id,
            String unit_year,
            int qty)
        {
            SALES_ID = sales_id;
            DATAAREA_ID = dataarea_id;
            MAINDEALER_ID = maindealer_id;
            SITE_ID = site_id;
            ITEM_ID = item_id;
            UNIT_TYPE = unit_type;
            COLOR_ID = color_id;
            UNIT_YEAR = unit_year;
            QTY = qty;
        }
    }

    public class PickingModel: BaseModel
    {
        private InventJournalTableObject _InventJournalTableObject = new InventJournalTableObject();
        private CustInvoiceJourObject _CustInvoiceJourObject = new CustInvoiceJourObject();
        private SalesLineObject _SalesLineObject = new SalesLineObject();
        private SalesTableAVPickObject _SalesTableAVPickObject = new SalesTableAVPickObject();
        private PickingObject _PickingObject = new PickingObject();
        private PickingTakeMappingObject _PickingTakeMappingObject = new PickingTakeMappingObject();
        private InventDimObject _InventDimObject = new InventDimObject();
        private ReturSLObject _ReturSLObject = new ReturSLObject();

        public PickingModel()
            : base()
        {
            _InventJournalTableObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.InventJournalTransObject.Context = _InventJournalTableObject.Context;
            _CustInvoiceJourObject.Context = (WmsUnitDataContext)Context; 
            _CustInvoiceJourObject.CustInvoiceTransObject.Context = (WmsUnitDataContext)Context;
            _SalesLineObject.Context = (WmsUnitDataContext)Context;
            _SalesTableAVPickObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.PickingLineObject.Context = _PickingObject.Context;
            _PickingTakeMappingObject.Context = (WmsUnitDataContext)Context;
            _InventDimObject.Context = (WmsUnitDataContext)Context;
            _ReturSLObject.Context = (WmsUnitDataContext)Context;
            _ReturSLObject.ReturSLLineObject.Context = _ReturSLObject.Context;
        }

        public ReturSLObject ReturSLObject
        {
            get { return _ReturSLObject; }
        }

        public SalesLineObject SalesLineObject
        {
            get { return _SalesLineObject; }
        }

        public InventJournalTableObject InventJournalTableObject
        {
            get { return _InventJournalTableObject; }
        }

        public CustInvoiceJourObject CustInvoiceJourObject
        {
            get { return _CustInvoiceJourObject; }
        }

        public SalesTableAVPickObject SalesTableAVPickObject
        {
            get { return _SalesTableAVPickObject; }
        }

        public PickingObject PickingObject
        {
            get { return _PickingObject; }
        }

        public PickingTakeMappingObject PickingTakeMappingObject
        {
            get { return _PickingTakeMappingObject; }
        }

        public InventDimObject InventDimObject
        {
            get { return _InventDimObject; }
        }

        public List<Object> QuantityCurrentTruck(String picking_id)
        {
            List<Object> a = new List<Object>();
            try
            {
                MPMWMS_PICKING data = PickingObject.Item(picking_id, 
                    UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                if (data == null)
                {
                    a.Add("");
                    a.Add("");
                    a.Add(0);
                    return a;
                }

                int count = QuantityCurrentTruck(data.DATAAREA_ID, data.EXPEDITION_ID, data.POLICE_NUMBER, data.PICKING_DATE);
                
                a.Add(data.EXPEDITION_ID);
                a.Add(data.POLICE_NUMBER);
                a.Add(count);

                return a;
            }
            catch (MPMException)
            {
                a.Add("");
                a.Add("");
                a.Add(0);
                return a;
            }
        }

        public int QuantityCurrentTruck(String dataarea_id, String expedition_id, String police_number, DateTime date)
        {
            try
            {
                var itemsObj =
                    from picking_line in PickingObject.PickingLineObject.Context.MPMWMS_PICKING_LINEs
                    join picking in PickingObject.Context.MPMWMS_PICKINGs on
                        new { picking_id = picking_line.PICKING_ID, dataarea_id = picking_line.DATAAREA_ID, do_id = picking_line.DO_ID } equals
                        new { picking_id = picking.PICKING_ID, dataarea_id = picking.DATAAREA_ID, do_id = picking.DO_ID } into picking_join
                    from result in picking_join.DefaultIfEmpty()
                    where
                        result.POLICE_NUMBER == police_number
                        && result.EXPEDITION_ID == expedition_id
                        && result.DATAAREA_ID == dataarea_id
                        && result.PICKING_DATE.Date == date.Date
                    select
                        new
                        {
                            police_number = result.POLICE_NUMBER,
                            driver = result.DRIVER,
                            picking_date = result.PICKING_DATE
                        };

                var itemsGroup =
                    from a in itemsObj
                    group a by new { a.police_number, a.driver } into grp
                    select new { key = grp.Key, cnt = grp.Count() };
                if (itemsGroup == null) return 0;
                else
                {
                    if (itemsGroup.Take(1).FirstOrDefault() != null)
                        return itemsGroup.Take(1).FirstOrDefault().cnt;
                    else return 0;
                }
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public void SyncWithSalesAx(SALESTABLE_AV_PICK row)
        {
            if (row.ISINTERNAL == "N")
            {
                row.QUANTITY_DO = CustInvoiceJourObject.CustInvoiceTransObject.SumQuantityDO(row.DOID, row.DATAAREAID, row.MAINDEALERID, row.SITEID);
            }
            else
            {
                //TO DO : get quantity from summarize journal table
                row.QUANTITY_DO = InventJournalTableObject.InventJournalTransObject.SumQuantityJournal(row.DOID, row.DATAAREAID);
            }

            // search already exists or not
            MPMWMS_DO_AVPICK itemAvPick = SalesTableAVPickObject.Item(row.DOID, 
                row.DATAAREAID, row.MAINDEALERID, row.SITEID);
            if (itemAvPick == null)
            {
                MPMWMS_DO_AVPICK rowAvPick = SalesTableAVPickObject.Create();
                rowAvPick.DO_ID = row.DOID;
                rowAvPick.DATAAREA_ID = row.DATAAREAID;
                rowAvPick.MAINDEALER_ID = row.MAINDEALERID;
                rowAvPick.SITE_ID = row.SITEID;
                rowAvPick.QUANTITY_DO = row.QUANTITY_DO;
                rowAvPick.QUANTITY_PICK = row.QUANTITY_PICK;
                rowAvPick.QUANTITY_SL = 0;
                rowAvPick.IS_INTERNAL = row.ISINTERNAL;
                SalesTableAVPickObject.Insert(rowAvPick);
            }
            else
            {
                itemAvPick.QUANTITY_DO = row.QUANTITY_DO;
            }
        }

        public void SyncWithSalesAx(String category_product, int year) 
        {
            try
            {
                /*List<SALESTABLE_AV_PICK> search = SalesTableObject.SalesAVPicksSync(category_product, year).ToList();
                for (int i = 0; i < search.Count(); i++)
                {
                    SALESTABLE_AV_PICK item = search[i];
                    SyncWithSalesAx(item);
                }*/

                ((WmsUnitDataContext)Context).CommandTimeout = 0;
                ((WmsUnitDataContext)Context).MPM_SP_SYNC_PICKING(UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);


                List<SALESTABLE_AV_PICK> searchJournal = CustInvoiceJourObject.JournalAVPicksSync(category_product, year).ToList();
                for (int i = 0; i < searchJournal.Count(); i++)
                {
                    SALESTABLE_AV_PICK item = searchJournal[i];
                    SyncWithSalesAx(item);
                }
                SubmitChanges();
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void SyncWithSalesAxCurrentItem(String category_product, String sales_id)
        {
            try
            {
                List<SALESTABLE_AV_PICK> search = CustInvoiceJourObject.SalesAVPicksSync(sales_id).ToList();
                for (int i = 0; i < search.Count(); i++)
                {
                    SALESTABLE_AV_PICK item = search[i];
                    SyncWithSalesAx(item);
                }

                List<SALESTABLE_AV_PICK> searchJournal = CustInvoiceJourObject.JournalAVPicksSync(category_product, sales_id).ToList();
                for (int i = 0; i < searchJournal.Count(); i++)
                {
                    SALESTABLE_AV_PICK item = searchJournal[i];
                    SyncWithSalesAx(item);
                }
                SubmitChanges();
                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DeletePicking(String picking_id, String do_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                BeginTransaction();
                // delete picking
                MPMWMS_PICKING deletedItem = PickingObject.Item(picking_id, dataarea_id, maindealer_id, site_id);

                if (deletedItem.MPMWMS_PICKING_LINEs.ToList().Count() > 0)
                {
                    throw new MPMException("Please delete all line first !!!");
                }
                
                PickingObject.Delete(deletedItem);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void DeletePickingLine(String picking_id, String do_id, String dataarea_id, String maindealer_id, String site_id, String machine_id, String frame_id)
        {
            try
            {
                BeginTransaction();

                MPMWMS_PICKING_LINE deletedItem = PickingObject.PickingLineObject.Item(
                    picking_id, dataarea_id, maindealer_id, site_id, do_id, machine_id, frame_id);
                if (deletedItem == null)
                {
                    throw new MPMException("Can't delete item line !!!");
                }

                // check if already take then can't be deleted
                MPMWMS_PICKING_TAKE_MAPPING take = PickingTakeMappingObject.ItemTake(picking_id, 
                    dataarea_id, maindealer_id, site_id, frame_id, machine_id);
                if (take != null)
                {
                    throw new MPMException("Can't delete because unit already scan !!!");
                }

                // update quantity do_avpick
                MPMWMS_DO_AVPICK do_avpick = SalesTableAVPickObject.Item(do_id, dataarea_id, maindealer_id, site_id);
                if (do_avpick == null)
                {
                    throw new MPMException("Can't find DO Available pick !!!");
                }
                do_avpick.QUANTITY_PICK--;
                SalesTableAVPickObject.Update(do_avpick);

                // delete picking
                // before delete update unit
                MPMWMS_UNIT unit = UnitObject.Item(deletedItem.MACHINE_ID, deletedItem.FRAME_ID, deletedItem.DATAAREA_ID);
                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }
                if (unit.STATUS != "S") {
                    unit.PICKING_ID = null;
                    unit.PICKING_DATE = null;
                    unit.DO_ID = null;
                    unit.DO_DATE = null;
                    unit.IS_READYTOPICK = "N";
                    unit.IS_READYTOPICK_AX = "N";
                    UnitObject.Update(unit, "PIC");
                }

                // update status picking to Open
                MPMWMS_PICKING picking = PickingObject.Item(picking_id, dataarea_id, maindealer_id, site_id);
                if (picking != null)
                {
                    picking.STATUS = "O";
                }

                PickingObject.PickingLineObject.Delete(deletedItem);
                Commit();    
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void InsertDetailPicking(
            String picking_id, 
            String dataarea_id, 
            String maindealer_id,
            String do_id, 
            String site_id, String color, String type, String year, int qty_pick)
        {
            try
            {
                // find the header
                MPMWMS_PICKING header = PickingObject.Item(picking_id, dataarea_id, maindealer_id, site_id);
                if (header == null)
                {
                    throw new MPMException("Header failed to access !!!");
                }

                for (int i = 0; i < qty_pick; i++)
                {
                    // find suggested unit
                    MPMWMS_UNIT unit = UnitObject.SuggestedPicking(dataarea_id, site_id, color, type, year);
                    if (unit == null)
                    {
                        throw new MPMException("Unit not found !!!");
                    }

                    MPMWMS_PICKING_LINE row = PickingObject.PickingLineObject.Create();
                    row.PICKING_ID = header.PICKING_ID;
                    row.DATAAREA_ID = header.DATAAREA_ID;
                    row.MAINDEALER_ID = header.MAINDEALER_ID;
                    row.DO_ID = header.DO_ID;
                    row.SITE_ID = unit.SITE_ID;
                    row.LOCATION_ID = unit.LOCATION_ID;
                    row.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                    row.MACHINE_ID = unit.MACHINE_ID;
                    row.FRAME_ID = unit.FRAME_ID;
                    row.COLOR_TYPE = unit.COLOR_TYPE;
                    row.UNIT_TYPE = unit.UNIT_TYPE;
                    PickingObject.PickingLineObject.Insert(row);

                    // update unit
                    /*unit.DO_ID = header.DO_ID;
                    unit.DO_DATE = header.DO_DATE;
                    unit.PICKING_ID = header.PICKING_ID;
                    unit.PICKING_DATE = header.PICKING_DATE;*/
                    unit.IS_READYTOPICK = "Y";
                    UnitObject.Update(unit, "PID");

                    // update sales table av pick
                    MPMWMS_DO_AVPICK sales_av = SalesTableAVPickObject.Item(do_id, dataarea_id, maindealer_id, site_id);
                    sales_av.QUANTITY_PICK += 1;
                    SalesTableAVPickObject.Update(sales_av);

                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<SALESLINE_CUSTOM> ListTypeNColorUnit(String do_id, String dataarea_id, String maindealer_id, String site_id)
        {
            List<CUSTINVOICETRAN> sales_lines = CustInvoiceJourObject.CustInvoiceTransObject.List(dataarea_id, do_id);

            // calculate quantity
            List<SALESLINE_CUSTOM> salesLines = new List<SALESLINE_CUSTOM>();
            foreach (var sales_line in sales_lines)
            {
                INVENTDIM d = InventDimObject.Item(sales_line.DATAAREAID, sales_line.INVENTDIMID);
                SALESLINE sl = SalesLineObject.Item(sales_line.SALESID, sales_line.DATAAREAID, sales_line.ITEMID, d.INVENTCOLORID);

                // search quantity in picking
                SALESLINE_CUSTOM sales = new SALESLINE_CUSTOM();
                sales.SALES_ID = sales_line.INVOICEID;
                sales.DATAAREA_ID = sales_line.DATAAREAID;
                sales.MAINDEALER_ID = maindealer_id;
                sales.SITE_ID = site_id;
                sales.UNIT_YEAR = sl.XTSUNITYEAR;
                sales.ITEM_ID = sales_line.ITEMID;
                sales.UNIT_TYPE = EcoResProductObject.Item(sales.ITEM_ID).XTSINVTYPEDESCR;
                sales.COLOR_ID = InventDimObject.Item(sales_line.DATAAREAID, sales_line.INVENTDIMID).INVENTCOLORID;
                int quantity_do = PickingObject.PickingLineObject.QuantityDO(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID);
                int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                sales.QTY = 
                    quantity_do - quantity_picking + quantity_retur;
                salesLines.Add(sales);
            }


            List<INVENTJOURNALTRAN> journal_lines = InventJournalTableObject.InventJournalTransObject.List(dataarea_id, do_id);
            salesLines =
                (
                    from a in journal_lines
                    group a by new
                    {
                        a.JOURNALID,
                        a.DATAAREAID,
                        a.XTSUNITYEAR,
                        a.ITEMID,
                        a.INVENTDIMID
                    } into g
                    select new SALESLINE_CUSTOM(
                            g.Key.JOURNALID,
                            g.Key.DATAAREAID,
                            maindealer_id,
                            site_id,
                            g.Key.ITEMID,
                            EcoResProductObject.Item(g.Key.ITEMID).XTSINVTYPEDESCR,
                            InventDimObject.Item(g.Key.DATAAREAID, g.Key.INVENTDIMID).INVENTCOLORID,
                            g.Key.XTSUNITYEAR,
                            Convert.ToInt32(g.Sum(x => x.QTY * -1)))
                ).ToList();

            foreach (var sales in salesLines)
            {
                int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                sales.QTY = sales.QTY - quantity_picking + quantity_retur;
            }

            /*
            foreach (var sales_line in journal_lines)
            {
                // search quantity in picking
                SALESLINE_CUSTOM sales = new SALESLINE_CUSTOM();
                sales.SALES_ID = sales_line.JOURNALID;
                sales.DATAAREA_ID = sales_line.DATAAREAID;
                sales.MAINDEALER_ID = maindealer_id;
                sales.SITE_ID = site_id;
                sales.UNIT_YEAR = sales_line.XTSUNITYEAR;
                sales.ITEM_ID = sales_line.ITEMID;
                sales.UNIT_TYPE = EcoResProductObject.Item(sales.ITEM_ID).XTSINVTYPEDESCR;
                sales.COLOR_ID = InventDimObject.Item(sales_line.DATAAREAID, sales_line.INVENTDIMID).INVENTCOLORID;
                int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                    sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                sales.QTY = Convert.ToInt32(sales_line.QTY * -1) - quantity_picking + quantity_retur;
                salesLines.Add(sales);
            }
             */

            return salesLines.Where(x => x.QTY > 0).ToList();
        }

        private CUSTINVOICETRAN FindSalesLineCurrentUnit(String dataarea_id, String do_id)
        {
            // search all sales id
            List<CUSTINVOICETRAN> sales_lines = CustInvoiceJourObject.CustInvoiceTransObject.List(dataarea_id, do_id);

            foreach (var sales_line in sales_lines)
            {
                // eliminate that already pick
                //List<MPMWMS_PICKING_LINE> picking_line = PickingObject.PickingLineObject.List(dataarea_id, do_id, );
            }
            return sales_lines.Take(1).FirstOrDefault();
        }

        // process picking
        public int ProcessPicking(
            String picking_id, 
            String do_id, 
            String dataarea_id, 
            String maindealer_id,
            String site_id,
            String color, 
            String item_id, 
            int qty_pick,
            string unit_year)
        {
            int qty_av_pick = 0;

            // get default site_id
            /*SALESTABLE sales_table = SalesTableObject.Item(do_id, dataarea_id, maindealer_id, site_id);
            if (sales_table == null)
            {
                // find at journal 
                INVENTJOURNALTABLE journal_table = InventJournalTableObject.Item(dataarea_id, sales_id);
                if (journal_table == null)
                {
                    throw new MPMException("Data sales table can't found!!!");
                }
            }*/

            CUSTINVOICETRAN sales_line = CustInvoiceJourObject.CustInvoiceTransObject.Item(do_id, dataarea_id, item_id, color);
            if (sales_line != null)
            {
                int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                    dataarea_id, maindealer_id, site_id, do_id, item_id, color, unit_year);
                int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                    dataarea_id, maindealer_id, site_id, do_id, item_id, color, unit_year);
                qty_av_pick = Convert.ToInt32(sales_line.QTY) - quantity_picking + quantity_retur;
                if (qty_pick > qty_av_pick)
                {
                    throw new MPMException("Can't pick more than [" + qty_av_pick + "] !!!");
                }

                INVENTDIM d = InventDimObject.Item(sales_line.DATAAREAID, sales_line.INVENTDIMID);
                SALESLINE sl = SalesLineObject.Item(sales_line.SALESID, sales_line.DATAAREAID, sales_line.ITEMID, d.INVENTCOLORID);

                InsertDetailPicking(picking_id, dataarea_id, maindealer_id, do_id, site_id, color, item_id, sl.XTSUNITYEAR, qty_pick);
            }
            else
            {
                List<INVENTJOURNALTRAN> journal_lines = InventJournalTableObject.InventJournalTransObject.List(dataarea_id, do_id);
                List<SALESLINE_CUSTOM> salesLines =
                    (
                        from a in journal_lines
                        group a by new
                        {
                            a.JOURNALID,
                            a.DATAAREAID,
                            a.XTSUNITYEAR,
                            a.ITEMID,
                            a.INVENTDIMID
                        } into g
                        select new SALESLINE_CUSTOM(
                                g.Key.JOURNALID,
                                g.Key.DATAAREAID,
                                maindealer_id,
                                site_id,
                                g.Key.ITEMID,
                                EcoResProductObject.Item(g.Key.ITEMID).XTSINVTYPEDESCR,
                                InventDimObject.Item(g.Key.DATAAREAID, g.Key.INVENTDIMID).INVENTCOLORID,
                                g.Key.XTSUNITYEAR,
                                Convert.ToInt32(g.Sum(x => x.QTY * -1)))
                    ).ToList();

                foreach (var sales in salesLines)
                {
                    int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                        sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);
                    int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                        sales.DATAAREA_ID, sales.MAINDEALER_ID, sales.SITE_ID, sales.SALES_ID, sales.ITEM_ID, sales.COLOR_ID, sales.UNIT_YEAR);                    
                    sales.QTY = sales.QTY - quantity_picking + quantity_retur;
                }

                SALESLINE_CUSTOM journalItem = salesLines.Where(x =>
                    x.ITEM_ID == item_id
                    && x.COLOR_ID == color
                    && x.UNIT_YEAR == unit_year
                    && x.SALES_ID == do_id).Take(1).FirstOrDefault();

                if (journalItem == null)
                {
                    throw new MPMException("Data invoice line can't found!!!");
                }
                else
                {
                    qty_av_pick = journalItem.QTY;
                    if (qty_pick > journalItem.QTY)
                    {
                        throw new MPMException("Can't pick more than [" + journalItem.QTY + "] !!!");
                    }
                    InsertDetailPicking(picking_id, dataarea_id, maindealer_id, do_id, site_id, color, item_id, journalItem.UNIT_YEAR, qty_pick);
                }

                // find at journal
                /*INVENTJOURNALTRAN journal_line = InventJournalTableObject.InventJournalTransObject.ItemByYear(do_id, dataarea_id, item_id, color, unit_year);
                if (journal_line != null)
                {
                    // get site_id
                    INVENTDIM dim = InventDimObject.Item(dataarea_id, journal_line.INVENTDIMID);
                    if (dim != null)
                    {
                        site_id = dim.INVENTSITEID;
                    }

                    int quantity_picking = PickingObject.PickingLineObject.SumQuantityPicking(
                        dataarea_id, maindealer_id, site_id, do_id, item_id, color, unit_year);
                    int quantity_retur = ReturSLObject.ReturSLLineObject.SumQuantityRetur(
                        dataarea_id, maindealer_id, site_id, do_id, item_id, color, unit_year);
                    qty_av_pick = Convert.ToInt32(journal_line.QTY * -1) - quantity_picking + quantity_retur;
                    if (qty_pick > qty_av_pick)
                    {
                        throw new MPMException("Can't pick more than [" + qty_av_pick + "] !!!");
                    }

                    InsertDetailPicking(picking_id, dataarea_id, maindealer_id, do_id, site_id, color, item_id, journal_line.XTSUNITYEAR, qty_pick);
                }
                else
                {
                    throw new MPMException("Data invoice line can't found!!!");

                }*/
            }

            return qty_av_pick - qty_pick;
        }

        // update pick for print
        public void UpdatePickPrint(String picking_id, String state)
        {
            try
            {
                MPMWMS_PICKING picking = PickingObject.Item(picking_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                if (picking != null)
                {
                    if (state.ToLower().Equals("false"))
                    {
                        picking.IS_PRINT = "0";
                    }
                    else
                    {
                        picking.IS_PRINT = "1";
                    }
                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        // data for picking slip report
        public List<PICKING_SLIP> PickingSlip(String sales_id, String dataarea_id)
        {
            IQueryable<PICKING_SLIP> search =
                from pl in PickingObject.PickingLineObject.Context.MPMWMS_PICKING_LINEs
                join p in PickingObject.Context.MPMWMS_PICKINGs on
                    new { dataarea = pl.DATAAREA_ID, picking = pl.PICKING_ID, sales = pl.DO_ID } equals
                    new { dataarea = p.DATAAREA_ID, picking = p.PICKING_ID, sales = p.DO_ID } into p_group
                from result in p_group.DefaultIfEmpty()
                where
                    result.DO_ID == sales_id
                    && result.DATAAREA_ID == dataarea_id
                select new PICKING_SLIP(
                    result.PICKING_ID,
                    result.PICKING_DATE,
                    result.DO_ID,
                    result.DATAAREA_ID,
                    result.DEALER_CODE,
                    result.DEALER_NAME,
                    result.DEALER_ADDRESS,
                    result.DEALER_CITY,
                    pl.MACHINE_ID,
                    pl.FRAME_ID,
                    pl.UNIT_TYPE,
                    pl.COLOR_TYPE,
                    pl.SITE_ID,
                    pl.LOCATION_ID);

            return search.ToList();
        }

        /**
         * Release take picking
         **/
        public void ReleasePickingTake(String picking_id, String do_id, String dataarea_id, String maindealer_id, String site_id, String machine_id, String machine_id_take)
        {
            try
            {
                BeginTransaction();

                MPMWMS_PICKING_TAKE_MAPPING takeItem = PickingTakeMappingObject.Item(picking_id, 
                    dataarea_id, maindealer_id, site_id, do_id, machine_id, machine_id_take);
                if (takeItem == null)
                {
                    throw new MPMException("Can't delete item take !!!");
                }

                /** PICKING **/
                // update status picking to 'O'
                MPMWMS_PICKING picking = PickingObject.Item(picking_id, dataarea_id, maindealer_id, site_id, do_id);
                if (picking == null)
                {
                    throw new MPMException("Can't update picking !!!");
                }
                picking.STATUS = "O";
                PickingObject.Update(picking);
                /** END OF PICKING **/

                /** UNIT TAKE **/
                // update unit take
                // put the unit @receiving area
                MPMWMS_UNIT unit = UnitObject.Item(takeItem.MACHINE_ID_TAKE, takeItem.FRAME_ID_TAKE, takeItem.DATAAREA_ID);
                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }

                if (unit.STATUS == "S")
                {
                    throw new MPMException("Can't release this unit, because unit already shipping !!!");
                }

                String site_id_old = unit.SITE_ID;
                String warehouse_id_old = unit.WAREHOUSE_ID;
                String location_id_old = unit.LOCATION_ID;

                unit.DO_ID = null;
                unit.DO_DATE = null;
                unit.PICKING_ID = null;
                unit.PICKING_DATE = null;

                // get new location @receiving area
                unit.LOCATION_ID = GetLocation(unit.DATAAREA_ID, unit.SITE_ID, "S");
                unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                UnitObject.Update(unit, "PISC", site_id_old, warehouse_id_old, location_id_old);
                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);
                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, warehouse_id_old, location_id_old, -1);
                /** END OF UNIT TAKE **/

                /** UNIT REAL @PICKING_LINE **/
                // find unit real
                MPMWMS_UNIT unitLine = UnitObject.Item(takeItem.MACHINE_ID, takeItem.FRAME_ID, takeItem.DATAAREA_ID);
                if (unitLine == null)
                {
                    throw new MPMException("Picking Unit not found !!!");
                }
                unitLine.IS_READYTOPICK = "Y";
                UnitObject.Update(unit, "PISC");
                /** END OF UNIT REAL @PICKING_LINE **/
                
                
                // delete picking take
                PickingTakeMappingObject.Delete(takeItem);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
    }
}
