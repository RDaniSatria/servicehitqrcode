﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.SpgIncludeBomDetail;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Mvc.Models;
using MPM365SERVICE.Service.MPMMuliaService;
using MPM365SERVICE.MPMMuliaService;
using MPM365SERVICE.Respond;

namespace MPMWMSMODEL.Model
{
    public class BomUnitModel : MPMModel
    {
        private SpgIncludeBomDetailObject _SpgIncludeBomDetailObject = new SpgIncludeBomDetailObject();
        private UnitObject _UnitObject = new UnitObject();
        private BundleUnitAccModel _BundleUnitAccModel = new BundleUnitAccModel();

        public BomUnitModel()
            : base()
        {
            Context = new WmsUnitDataContext();
            _SpgIncludeBomDetailObject.Context = (WmsUnitDataContext)Context;
            _UnitObject.Context = (WmsUnitDataContext)Context;
            //_BundleUnitAccModel.Context = (WmsUnitDataContext)Context;
        }

        public IQueryable<MPMWMS_SPG_INCLUDE_BOM_TRANS> List(String site_id)
        {
            return _SpgIncludeBomDetailObject.List(site_id);
        }

        public MPMWMS_SPG_INCLUDE_BOM_DETAIL item(long recid)
        {
            return _SpgIncludeBomDetailObject.item(recid);
        }

        public void update(long recid)
        { 
             MPMWMS_SPG_INCLUDE_BOM_DETAIL item = _SpgIncludeBomDetailObject.item(recid);
            List<MPMWMS_UNIT> list_unit = new List<MPMWMS_UNIT>();
            if (item.SPGID.Contains("ITO"))
            {
                //QUERY KE MUTATION
                list_unit = _UnitObject.ListByITO(item.DATAAREAID, item.SPGID);
            }
            else
            {
                list_unit = _UnitObject.ListBySPG(item.DATAAREAID, item.SITEID_UNIT, item.SPGID);
            }
            IEnumerable<MPMWMS_UNIT> list_unit_available = list_unit.Where(a => a.UNIT_TYPE == item.UNITTYPE1 && 
                                                                                 a.COLOR_TYPE == item.UNITCOLOR &&
                                                                                 a.SITE_ID == item.SITEID_UNIT &&
                                                                                 a.WAREHOUSE_ID == item.LOCATIONID_UNIT &&
                                                                                 (a.STATUS == "SPG" || a.STATUS == "RFS" || a.STATUS == "BM")
                                                                           ).ToList();
             if (list_unit.Count()<=0)
                 throw new MPMException("No SPG : " + item.SPGID + " Tidak Ditemukan Pada Gudang ");

             //in iblm ada data di 72 blm bisa diliat hasilnya.
             if (list_unit_available.Count() == item.QTY)
             {
                 
                 foreach(MPMWMS_UNIT item_MPMWMS_UNIT in list_unit_available)
                 {
                     _BundleUnitAccModel.BundleUnitWithoutPrint(item.BOMID, item_MPMWMS_UNIT.MACHINE_ID, item.UNITTYPE1, item.UNITCOLOR, item.SITEID_UNIT, item.LOCATIONID_UNIT,"BOM");
                 }

                //update status pada detail
                //diganti web service 365
                InventoryService inventService = new InventoryService();
                var resultAX = new ResultCommon();
                resultAX = inventService.updateSpgBomDetail(UserSession.NPK, recid.ToString(), UserSession.DATAAREA_ID);
                if (resultAX.Message != "" && resultAX.Success == 0)
                {
                    throw new Exception(resultAX.Message);
                }
                //item.STATUS = 2;
                //item.MODIFIEDBY_NONAX = UserSession.NPK;
                //item.MODIFIEDDATE_NONAX = MPMDateUtil.DateTime;
                //SubmitChanges();


            }
             else
             {
                 throw new MPMException("Pada No SPG " + item.SPGID +" Kode Type : " + item.UNITTYPE1 + " Kode Warna : "+ item.UNITCOLOR +" Qty : " + list_unit_available.Count() +" lebih kecil dari " +  (int)item.QTY );
             }
            
        }
        
    }
}
