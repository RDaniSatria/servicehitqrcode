﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Lov.ShippingList;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Faktur;
using MPMWMSMODEL.Table.ReturSL;
using MPMWMSMODEL.Table.ShippingList;
using MPMWMSMODEL.Table.ShippingListLine;
using MPMWMSMODEL.xtsMuliaService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Table.SalesTableAVPick;
using MPMWMSMODEL.Table.Picking;
using MPMWMSMODEL.Table.PickingTakeMapping;
using MPMLibrary.NET.Lib.Util;
using MPM365SERVICE.Service.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.MPMMuliaService;
using CallContext = MPMWMSMODEL.xtsMuliaService.CallContext;

namespace MPMWMSMODEL.Model
{
    public class ReturSLModel: BaseModel
    {
        protected String ServiceGroup = "xtsMuliaService";

        protected CallContext ContextWS = new CallContext();

        private ReturSLObject _ReturSLObject = new ReturSLObject();
        private PickingObject _PickingObject = new PickingObject();
        private ShippingListObject _ShippingListObject = new ShippingListObject();
        private FakturObject _FakturObject = new FakturObject();
        private SalesTableAVPickObject _SalesTableAVPickObject = new SalesTableAVPickObject();
        private PickingTakeMappingObject _PickingTakeMappingObject = new PickingTakeMappingObject();

        public LovMachineSLForReturObject lovMachineRetur = new LovMachineSLForReturObject();

        public ReturSLModel()
            : base()
        {
            _ReturSLObject.Context = (WmsUnitDataContext)Context;
            _ReturSLObject.ReturSLLineObject.Context = _ReturSLObject.Context;

            _PickingObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.PickingLineObject.Context = _PickingObject.Context;

            _PickingTakeMappingObject.Context = (WmsUnitDataContext)Context;

            _ShippingListObject.Context = (WmsUnitDataContext)Context;
            _ShippingListObject.ShippingListLineObject.Context = _ShippingListObject.Context;

            _SalesTableAVPickObject.Context = (WmsUnitDataContext)Context;

            _FakturObject.Context = (WmsUnitDataContext)Context;
        }

        /** WEB SERVICE **/
        protected xtsMuliaService.xtsCreateSalesServiceClient ConnectWebService()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpXtsMuliaService, LogonASUser);

                xtsMuliaService.xtsCreateSalesServiceClient client = new xtsMuliaService.xtsCreateSalesServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebService(xtsMuliaService.xtsCreateSalesServiceClient client)
        {
            client.Close();
            client = null;
        }
        /** END OF WEB SERVICE **/

        public List<LOV_MACHINE_SL> LovMachineSLForRetur(String retur_sl_id, String dataarea_id, String maindealer_id, String site_id)
        {
            MPMWMS_RETUR_SL item = ReturSLObject.Item(retur_sl_id, dataarea_id, maindealer_id, site_id);
            if (item == null)
            {
                return null;
            }
            return lovMachineRetur.List(item.SHIPPING_LIST_ID, item.DATAAREA_ID);
        }

        public ReturSLObject ReturSLObject
        {
            get { return _ReturSLObject; }
        }

        public PickingObject PickingObject
        {
            get { return _PickingObject; }
        }

        public ShippingListObject ShippingListObject
        {
            get { return _ShippingListObject; }
        }

        public FakturObject FakturObject
        {
            get { return _FakturObject; }
        }

        public SalesTableAVPickObject SalesTableAVPickObject
        {
            get { return _SalesTableAVPickObject; }
        }

        public PickingTakeMappingObject PickingTakeMappingObject
        {
            get { return _PickingTakeMappingObject; }
        }

        public void Insert(Boolean submit, MPMWMS_RETUR_SL retur)
        {
            try
            {
                // find data shipping list
                MPMWMS_SHIPPING_LIST sl = ShippingListObject.Item(retur.SHIPPING_LIST_ID, 
                    retur.DATAAREA_ID, retur.MAINDEALER_ID, retur.SITE_ID);
                if (sl == null)
                {
                    throw new MPMException("Can't find shipping list data !!!");
                }

                retur.RETUR_SL_DATE = MPMDateUtil.DateTime;
                retur.MAINDEALER_ID = sl.MAINDEALER_ID;
                retur.DATAAREA_ID = sl.DATAAREA_ID;
                retur.SHIPPING_LIST_ID = sl.SHIPPING_LIST_ID;
                retur.SHIPPING_LIST_DATE = sl.SHIPPING_DATE;
                retur.PICKING_ID = sl.PICKING_ID;
                retur.PICKING_DATE = sl.PICKING_DATE;
                retur.DO_ID = sl.DO_ID;
                retur.DO_DATE = sl.DO_DATE;
                retur.DEALER_CODE = sl.DEALER_CODE;
                retur.DEALER_NAME = sl.DEALER_NAME;
                retur.DEALER_ADDRESS = sl.DEALER_ADDRESS;
                retur.DEALER_CITY = sl.DEALER_CITY;
                retur.RETUR_SL_ID = RunningNumberReturShippingListUnit(retur.DATAAREA_ID, 
                    retur.MAINDEALER_ID, retur.SITE_ID);
                ReturSLObject.Insert(retur);

                if (submit)
                {
                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(Boolean submit, MPMWMS_RETUR_SL retur)
        {
            try
            {
                // find data shipping list
                MPMWMS_SHIPPING_LIST sl = ShippingListObject.Item(retur.SHIPPING_LIST_ID, retur.DATAAREA_ID, retur.MAINDEALER_ID, retur.SITE_ID);
                if (sl == null)
                {
                    throw new MPMException("Can't find shipping list data !!!");
                }

                retur.SHIPPING_LIST_DATE = sl.SHIPPING_DATE;
                retur.PICKING_ID = sl.PICKING_ID;
                retur.PICKING_DATE = sl.PICKING_DATE;
                retur.DO_ID = sl.DO_ID;
                retur.DO_DATE = sl.DO_DATE;
                retur.DEALER_CODE = sl.DEALER_CODE;
                retur.DEALER_NAME = sl.DEALER_NAME;
                retur.DEALER_ADDRESS = sl.DEALER_ADDRESS;
                retur.DEALER_CITY = sl.DEALER_CITY;
                retur.RETUR_SL_ID = RunningNumberReturShippingListUnit(retur.DATAAREA_ID, retur.MAINDEALER_ID, retur.SITE_ID);
                ReturSLObject.Update(retur);

                if (submit)
                {
                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(String retur_sl_id)
        {
            try
            {
                MPMWMS_RETUR_SL item = ReturSLObject.Item(retur_sl_id, 
                    UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                if (item == null)
                {
                    throw new MPMException("Can't find retur item [" + retur_sl_id + "] !!!");
                }

                List<MPMWMS_RETUR_SL_LINE> retur_lines = ReturSLObject.ReturSLLineObject.List(item);
                if (retur_lines != null && retur_lines.Count() > 0)
                {
                    throw new MPMException("Can't delete because already exist line at [" + item.RETUR_SL_ID + "] !!!");
                }
                item.IS_CANCEL = "Y";
                ReturSLObject.Update(item);

                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        private bool InsertToWebServiceRetur(
            String do_id, String unit_type, String color_type, String site_id, String warehouse_id, int quantity)
        {
            try
            {
            //    xtsMuliaService.xtsCreateSalesServiceClient client = ConnectWebService();
            //    bool result_ws = client.CreateSalesReturnSL(ContextWS,
            //        do_id,
            //        unit_type,
            //        quantity,
            //        color_type,
            //        site_id,
            //        warehouse_id);
            //    DisconnectWebService(client);
                SalesService sales = new SalesService();
                var resultAX = new ResultCommon();
                MPMSalesOrderParm[] param;
                param = new MPMSalesOrderParm[1];
                param[0] = new MPMSalesOrderParm();
                param[0].parmInvoiceId = do_id;
                param[0].parmItemId = unit_type;
                param[0].parmSalesQty = quantity;
                param[0].parmInventColorId = color_type;
                param[0].parmFromSiteId = site_id;
                param[0].parmFromLocationId = warehouse_id;
                resultAX =  sales.createSalesReturnSL(param, UserSession.DATAAREA_ID);

               if(resultAX.Success == 0)
                {
                    throw new Exception(resultAX.Message);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void InsertLine(MPMWMS_RETUR_SL_LINE item)
        {
            MPMWMS_SHIPPING_LIST_LINE line_sl = null;
            MPMWMS_UNIT unit = null;
            String warehouse_id_temp = "";
            try
            {
                // prepare data line unit
                unit = UnitObject.Item(item.DATAAREA_ID, item.MACHINE_ID);
                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }
                warehouse_id_temp = LocationObject.GetWarehouseByType(unit.DATAAREA_ID, unit.SITE_ID, "R");

                bool result = InsertToWebServiceRetur(item.DO_ID, unit.UNIT_TYPE, unit.COLOR_TYPE, item.SITE_ID, warehouse_id_temp, 1);
                if (!result)
                {
                    throw new MPMException("Web Service Retur Error !!!");
                }

                try
                {
                    BeginTransaction();

                    item.MACHINE_ID = unit.MACHINE_ID;
                    item.FRAME_ID = unit.FRAME_ID;
                    item.COLOR_TYPE = unit.COLOR_TYPE;
                    item.UNIT_TYPE = unit.UNIT_TYPE;

                    // update unit to receiving area
                    String warehouse_id_old = unit.WAREHOUSE_ID;
                    String location_id_old = unit.LOCATION_ID;
                    unit.WAREHOUSE_ID = warehouse_id_temp;
                    unit.LOCATION_ID = GetLocation(unit.DATAAREA_ID, unit.SITE_ID, "R");
                    unit.SHIPPING_ID = null;
                    unit.SHIPPING_DATE = null;
                    unit.STATUS = "RFS";
                    unit.IS_RFS = "Y";
                    UnitObject.Update(unit, "M", unit.SITE_ID, warehouse_id_old, location_id_old);

                    // update data line shipping list
                    line_sl = ShippingListObject.ShippingListLineObject.Item(item.SHIPPING_LIST_ID,
                        item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID,
                        item.MACHINE_ID, item.FRAME_ID);
                    if (line_sl == null)
                    {
                        throw new MPMException("Data Line of Shipping List can't be found !!!");
                    }
                    line_sl.RETUR_SL_ID = item.RETUR_SL_ID;

                    // update data picking take
                    if (line_sl.STATUS == "C")
                    {
                        MPMWMS_PICKING_TAKE_MAPPING taking = PickingTakeMappingObject.ItemTakeNoPicking(
                            line_sl.DATAAREA_ID,
                            line_sl.MAINDEALER_ID,
                            line_sl.SITE_ID,
                            line_sl.FRAME_ID,
                            line_sl.MACHINE_ID);
                        if (taking == null)
                        {
                            throw new MPMException("Data Line of Take Picking can't be found !!!");
                        }
                        taking.STATUS = "R";
                    }
                    else
                    {
                        MPMWMS_PICKING_TAKE_MAPPING taking = PickingTakeMappingObject.ItemTake(
                            line_sl.PICKING_ID,
                            line_sl.DATAAREA_ID,
                            line_sl.MAINDEALER_ID,
                            line_sl.SITE_ID,
                            line_sl.FRAME_ID,
                            line_sl.MACHINE_ID);
                        if (taking == null)
                        {
                            throw new MPMException("Data Line of Take Picking can't be found !!!");
                        }
                        taking.STATUS = "R";
                    }

                    // update DO to open
                    MPMWMS_DO_AVPICK doav = SalesTableAVPickObject.Item(line_sl.DO_ID,
                        line_sl.DATAAREA_ID, line_sl.MAINDEALER_ID, line_sl.SITE_ID);
                    if (doav == null)
                    {
                        throw new MPMException("Data available pick can't be found !!!");
                    }
                    doav.QUANTITY_PICK--;
                    doav.QUANTITY_SL--;

                    // insert to line
                    item.MAINDEALER_ID = line_sl.MAINDEALER_ID;
                    item.NO_URUT = line_sl.NO_URUT;
                    ReturSLObject.ReturSLLineObject.Insert(item);

                    line_sl.STATUS = "R";

                    ShippingListObject.ShippingListLineObject.Update(line_sl);

                    // update faktur
                    //MPMINVFAKTUR faktur = FakturObject.Item(line_sl.DATAAREA_ID, line_sl.MACHINE_ID);
                    //if (faktur != null)
                    //{
                    //}

                    Commit();
                }
                catch (MPMException ex)
                {
                    Rollback();
                    throw new MPMException(ex.Message);
                }
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }

                       
        }

        public void DeleteLine(String retur_sl_id, String machine_id, String frame_id)
        {
            try
            {
                // find retur line
                MPMWMS_RETUR_SL_LINE item = ReturSLObject.ReturSLLineObject.Item(retur_sl_id,
                    UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING,
                    machine_id, frame_id);
                if (item == null)
                {
                    throw new MPMException("Can't find item line from current retur [" + retur_sl_id + "]");
                }

                MPMWMS_UNIT unit = UnitObject.Item(item.DATAAREA_ID, item.MACHINE_ID);
                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!");
                }

                bool result = InsertToWebServiceRetur(item.DO_ID, item.UNIT_TYPE, item.COLOR_TYPE, item.SITE_ID, unit.WAREHOUSE_ID, -1);
                if (!result)
                {
                    throw new MPMException("Web Service Retur Error !!!");
                }

                try
                {
                    BeginTransaction();

                    // update shipping line
                    MPMWMS_SHIPPING_LIST_LINE line_sl = ShippingListObject.ShippingListLineObject.Item(item.SHIPPING_LIST_ID,
                        item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID,
                        item.MACHINE_ID, item.FRAME_ID);
                    if (line_sl == null)
                    {
                        throw new MPMException("Data Line of Shipping List can't be found !!!");
                    }
                    line_sl.RETUR_SL_ID = null;
                    line_sl.STATUS = "O";
                    ShippingListObject.ShippingListLineObject.Update(line_sl);

                    // update DO
                    MPMWMS_DO_AVPICK doav = SalesTableAVPickObject.Item(item.DO_ID,
                        item.DATAAREA_ID, item.MAINDEALER_ID, item.SITE_ID);
                    if (doav == null)
                    {
                        throw new MPMException("DO Available can't find !!!");
                    }
                    doav.QUANTITY_SL--;
                    doav.QUANTITY_PICK--;

                    // update data picking take
                    MPMWMS_PICKING_TAKE_MAPPING taking = PickingTakeMappingObject.ItemTake(
                        line_sl.PICKING_ID,
                        line_sl.DATAAREA_ID,
                        line_sl.MAINDEALER_ID,
                        line_sl.SITE_ID,
                        line_sl.FRAME_ID,
                        line_sl.MACHINE_ID);
                    if (taking == null)
                    {
                        throw new MPMException("Data Line of Take Picking can't be found !!!");
                    }
                    taking.STATUS = "P";

                    // update unit                
                    String warehouse_id_old = unit.WAREHOUSE_ID;
                    String location_id_old = unit.LOCATION_ID;
                    unit.WAREHOUSE_ID = LocationObject.GetWarehouseByType(unit.DATAAREA_ID, unit.SITE_ID, "S");
                    unit.LOCATION_ID = GetLocation(unit.DATAAREA_ID, unit.SITE_ID, "S");
                    unit.STATUS = "S";
                    UnitObject.Update(unit, "M", unit.SITE_ID, warehouse_id_old, location_id_old);

                    // delete line
                    ReturSLObject.ReturSLLineObject.Delete(item);

                    Commit();
                }
                catch (MPMException ex)
                {
                    Rollback();
                    throw new MPMException(ex.Message);
                }
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
