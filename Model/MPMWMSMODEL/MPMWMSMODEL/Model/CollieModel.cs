﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Collie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class CollieModel : BaseModel
    {
        public CollieObject _CollieObject = new CollieObject();

        public CollieModel()
            : base()
        {
            _CollieObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Insert(MPMWMS_COLLIE _item)
        {
            MPMWMS_COLLIE dataNew = new MPMWMS_COLLIE();

            BeginTransaction();
            try
            {
                var row = _CollieObject.Item(dataNew);

                if (row == null)
                {
                    dataNew.COLLIECODE = _item.COLLIECODE;
                    dataNew.DATAAREAID = _item.DATAAREAID;
                    dataNew.MAINDEALERID = _item.MAINDEALERID;
                    dataNew.SITEID = _item.SITEID;
                    dataNew.COLLIENAME = _item.COLLIENAME;
                    dataNew.COLIIETYPE = _item.COLIIETYPE;
                    dataNew.WIDTH = _item.WIDTH;
                    dataNew.HEIGHT = _item.HEIGHT;
                    dataNew.DEPTH = _item.DEPTH;
                    dataNew.COEFFICIENTCOST = _item.COEFFICIENTCOST;
                    dataNew.VALUE = _item.VALUE;
                    dataNew.FREIGHTVALUE = _item.FREIGHTVALUE;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _CollieObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_COLLIE _item)
        {
            BeginTransaction();
            try
            {
                var row = _CollieObject.Item(_item);

                if (row != null)
                {
                    row.COLLIENAME = _item.COLLIENAME;
                    row.COLIIETYPE = _item.COLIIETYPE;
                    row.WIDTH = _item.WIDTH;
                    row.HEIGHT = _item.HEIGHT;
                    row.DEPTH = _item.DEPTH;
                    row.COEFFICIENTCOST = _item.COEFFICIENTCOST;
                    row.VALUE = _item.VALUE;
                    row.FREIGHTVALUE = _item.FREIGHTVALUE;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                _CollieObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_COLLIE _item)
        {
            BeginTransaction();
            try
            {
                var row = _CollieObject.Item(_item);

                if (row != null)
                {
                    _CollieObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<CollieRecord> ViewAll()
        {
            return _CollieObject.ViewAll();
        }
    }
}
