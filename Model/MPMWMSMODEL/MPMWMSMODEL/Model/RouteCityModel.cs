﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.RouteCity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class RouteCityModel : BaseModel
    {
        public RouteCityObject _RouteCityObject = new RouteCityObject();

        public RouteCityModel()
            : base()
        {
            _RouteCityObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Insert(MPMWMS_ROUTE_CITY item)
        {
            MPMWMS_ROUTE_CITY dataNew = new MPMWMS_ROUTE_CITY();

            BeginTransaction();
            try
            {
                var row = _RouteCityObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row == null)
                {
                    dataNew.ROUTEID = item.ROUTEID;
                    dataNew.MAINDEALERID = item.MAINDEALERID;
                    dataNew.SITEID = item.SITEID;
                    dataNew.ROUTEID = item.ROUTEID;
                    dataNew.INDEX = item.INDEX;
                    dataNew.CITYRECID = item.CITYRECID;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _RouteCityObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_ROUTE_CITY item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteCityObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row != null)
                {
                    row.CITYRECID = item.CITYRECID;
                    row.INDEX = item.INDEX;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                _RouteCityObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_ROUTE_CITY item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteCityObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row != null)
                {
                    _RouteCityObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
    }
}
