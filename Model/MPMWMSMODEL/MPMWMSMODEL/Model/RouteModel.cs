﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.RouteDealer;
using MPMWMSMODEL.Table.Route;
using MPMWMSMODEL.Table.RouteCity;
using MPMWMSMODEL.Table.RouteDealer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class RouteModel : BaseModel
    {
        public RouteObject _RouteObject = new RouteObject();
        public RouteCityObject _RouteCityObject = new RouteCityObject();
        public RouteDealerObject _RouteDealerObject = new RouteDealerObject();

        public RouteModel()
            : base()
        {
            _RouteObject.Context = (WmsUnitDataContext)Context;
            _RouteCityObject.Context = (WmsUnitDataContext)Context;
            _RouteDealerObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Insert(MPMWMS_ROUTE item)
        {
            MPMWMS_ROUTE dataNew = new MPMWMS_ROUTE();

            BeginTransaction();
            try
            {
                var row = _RouteObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row == null)
                {
                    dataNew.ROUTEID = item.ROUTEID.Trim();
                    dataNew.DATAAREAID = item.DATAAREAID;
                    dataNew.MAINDEALERID = item.MAINDEALERID;
                    dataNew.SITEID = item.SITEID;
                    dataNew.ROUTEID = item.ROUTEID;
                    dataNew.DESCRIPTION = item.DESCRIPTION;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _RouteObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_ROUTE item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row != null)
                {
                    row.DESCRIPTION = item.DESCRIPTION;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                _RouteObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_ROUTE item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID);

                if (row != null)
                {
                    _RouteObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Insert_City(MPMWMS_ROUTE_CITY item)
        {
            MPMWMS_ROUTE_CITY dataNew = new MPMWMS_ROUTE_CITY();

            BeginTransaction();
            try
            {
                var row = _RouteCityObject.ItemByIndex(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID, item.INDEX);

                if (row == null)
                {
                    dataNew.ROUTEID = item.ROUTEID;
                    dataNew.DATAAREAID = item.DATAAREAID;
                    dataNew.MAINDEALERID = item.MAINDEALERID;
                    dataNew.SITEID = item.SITEID;
                    dataNew.ROUTEID = item.ROUTEID;
                    dataNew.CITYRECID = item.CITYRECID;
                    dataNew.INDEX = item.INDEX;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _RouteCityObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update_City(MPMWMS_ROUTE_CITY item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteCityObject.ItemByIndex(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID, item.INDEX);

                if (row != null)
                {
                    row.CITYRECID = item.CITYRECID;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                _RouteCityObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete_City(MPMWMS_ROUTE_CITY item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteCityObject.ItemByIndex(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ROUTEID, item.INDEX);

                if (row != null)
                {
                    _RouteCityObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Insert_Route_Dealer(MPMWMS_MAP_ROUTE_DEALER item)
        {
            MPMWMS_MAP_ROUTE_DEALER dataNew = new MPMWMS_MAP_ROUTE_DEALER();

            BeginTransaction();
            try
            {
                var row = _RouteDealerObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ACCOUNTNUM, item.ROUTEID);

                if (row == null)
                {
                    dataNew.ACCOUNTNUM = item.ACCOUNTNUM;
                    dataNew.ROUTEID = item.ROUTEID.Trim();
                    dataNew.DATAAREAID = item.DATAAREAID;
                    dataNew.MAINDEALERID = item.MAINDEALERID;
                    dataNew.SITEID = item.SITEID;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _RouteDealerObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update_Route_Dealer(MPMWMS_MAP_ROUTE_DEALER item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteDealerObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ACCOUNTNUM, item.ROUTEID);

                if (row != null)
                {
                    row.ROUTEID = item.ROUTEID;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                _RouteDealerObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete_Route_Dealer(MPMWMS_MAP_ROUTE_DEALER item)
        {
            BeginTransaction();
            try
            {
                var row = _RouteDealerObject.Item(item.DATAAREAID, item.MAINDEALERID, item.SITEID, item.ACCOUNTNUM, item.ROUTEID);

                if (row != null)
                {
                    _RouteDealerObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<RouteZoneRecord> getListRouteZone(String _dataArea, String _mainDealer, String _siteId)
        {
            return _RouteDealerObject.ListRouteZone(_dataArea, _mainDealer, _siteId);
        }

        public List<MPMWMS_TO_ZONE> getListZone()
        {
            return _RouteDealerObject.getListZone();
        }

        public List<DealerZoneRecord> getListDealerZone()
        {
            return _RouteDealerObject.ListDealerZone();
        }

        public List<DealerRouteRecord> getListDealerRoute(String _dataArea, String _mainDealer, String _siteId, String _accountNum)
        {
            return _RouteDealerObject.getListDealerRoute(_dataArea, _mainDealer, _siteId, _accountNum);
        }

        public List<MPMWMS_ROUTE> getListRoute(String _dataArea, String _mainDealer, String _siteId)
        {
            return _RouteDealerObject.getListRoute(_dataArea, _mainDealer, _siteId);
        }
    }
}
