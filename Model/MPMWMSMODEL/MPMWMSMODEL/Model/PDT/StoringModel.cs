﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Print;
using MPMLibrary.NET.Lib.Util;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.HistoryUnit;
using MPMWMSMODEL.Table.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MPMWMSMODEL.xtsMuliaService;
using System.Transactions;
using MPMWMSMODEL.Model.PDT;
using MPMWMSMODEL.Table.LokasiTitipan;
using MPMWMSMODEL.Table.Unit;
using MPMWMSMODEL.Table.VendorStoringUnit;
using MPMWMSMODEL.Table.ShippingList;
using MPMWMSMODEL.Table.QrUnit;
using System.Linq.Expressions;

namespace MPMWMSMODEL.Model
{
    public class SPGSTORING_DATA {
        public String _SPG_ID { get; set; }
        public DateTime _SPG_DATE { get; set; }
        public String _SL_ID { get; set; }
        public DateTime _SL_DATE { get; set; }
        public int _COUNT { get; set; }

        public SPGSTORING_DATA(String spg_id, DateTime spg_date, String sl_id, DateTime sl_date, int count)
        {
            _SPG_ID = spg_id;
            _SPG_DATE = spg_date;
            _SL_ID = sl_id;
            _SL_DATE = sl_date;
            _COUNT = count;
        }
    }

    public class SPGSTORING_DATA_DETAIL
    {
        public String _ID { get; set; }
        public DateTime _DATE { get; set; }
        public String _MACHINE_ID { get; set; }
        public String _FRAME_ID { get; set; }
        public String _QRCODE { get; set; }

        public SPGSTORING_DATA_DETAIL(String id, DateTime date, String machine_id, String frame_id,String qrcode)
        {
            _ID = id;
            _DATE = date;
            _MACHINE_ID = machine_id;
            _FRAME_ID = frame_id;
            _QRCODE = qrcode;
        }
    }
    public class MPMVUnitTypeColorRec
    {
        public string type { get; set; }
        public string color { get; set; }
    }

    public class StoringModel: PDTUnitModel
    {
        // ==============================================================================
        //xtsMuliaService.xtsTransferInventServiceClient client = null;

        public StoringVendorModel _StoringVendorModel = new StoringVendorModel();
        public LokasiTitipanObj _LokasiTitipan = new LokasiTitipanObj();
        public UnitObject _Unit = new UnitObject();
        //tambahan anong 7/18/2019
        public ShippingListObject _Shipping = new ShippingListObject();

        public StoringModel()
            : base()
        {
            _LokasiTitipan.Context = (WmsUnitDataContext)Context;
            _Shipping.Context = (WmsUnitDataContext)Context;
        }        

        /** VALIDATION **/
        public bool IsStoringLocation(String dataarea_id, String site_id, String head_location_id)
        {
            MPMWMS_WAREHOUSE_LOCATION location = LocationObject.ItemHead(dataarea_id, site_id, head_location_id);
            if (location == null) return false;

            if (location.LOCATION_TYPE.Equals("S"))
            {
                return true;
            }

            if (location.LOCATION_TYPE.Equals("R"))
            {
                return true;
            }

            return false;
        }

        private void Validation(MPMWMS_UNIT unit, String dataarea_id, String site_id)
        {
            try
            {
                // check the dataarea and site
                if (!((unit.SITE_ID == site_id) && (unit.DATAAREA_ID == dataarea_id)))
                {
                    throw new MPMException("Unit can't storing in this data area / site id !!!!");
                }

                // if not from receiving area than it can't use storing application
                MPMWMS_WAREHOUSE_LOCATION locationCheck = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                if (locationCheck == null)
                {
                    throw new MPMException("Can't find the current location for unit " + unit.MACHINE_ID);
                }
                else
                {
                    if (!locationCheck.LOCATION_TYPE.Equals("R"))
                    {
                        throw new MPMException("Location must in Storing Area (ver. Receiving), other than that use mutation location unit");
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        /** END OF VALIDATION **/

        public List<SPGSTORING_DATA> ListSPGForStoring()
        {
            try
            {
                var itemsObj =
                    from
                        unit in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                    join warehouse in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs on
                        new { unit.DATAAREA_ID, unit.WAREHOUSE_ID, unit.SITE_ID, unit.LOCATION_ID } equals
                        new { warehouse.DATAAREA_ID, warehouse.WAREHOUSE_ID, warehouse.SITE_ID, warehouse.LOCATION_ID } into warehouse_unit
                    from result in warehouse_unit
                    where
                        (unit.DATAAREA_ID == UserSession.DATAAREA_ID)
                        && (unit.SITE_ID == UserSession.SITE_ID_MAPPING)
                        && (unit.SPG_ID != null)
                        && (unit.SPG_ID != "")
                        && (result.LOCATION_TYPE == "R")
                        && (unit.STATUS == "SPG")
                    orderby
                        unit.SPG_DATE ascending
                    let k = new { unit.SL_ID, unit.SL_DATE, unit.SPG_ID, unit.SPG_DATE }
                    group unit by k into tgroup
                    select new SPGSTORING_DATA(tgroup.Key.SPG_ID, tgroup.Key.SPG_DATE.Value, tgroup.Key.SL_ID,
                        tgroup.Key.SL_DATE.Value, tgroup.Count());
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public List<SPGSTORING_DATA_DETAIL> ListSPGUnitForStoring(String spg_id)
        {
            try
            {
                var itemsObj =
                    from
                        unit in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                    join warehouse in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs on
                        new { unit.DATAAREA_ID, unit.WAREHOUSE_ID, unit.SITE_ID, unit.LOCATION_ID } equals
                        new { warehouse.DATAAREA_ID, warehouse.WAREHOUSE_ID, warehouse.SITE_ID, warehouse.LOCATION_ID } into warehouse_unit
                    from result in warehouse_unit
                    where
                        (unit.DATAAREA_ID == UserSession.DATAAREA_ID)
                        && (unit.SITE_ID == UserSession.SITE_ID_MAPPING)
                        && (unit.SPG_ID == spg_id)
                        && (result.LOCATION_TYPE == "R")
                        && (unit.STATUS == "SPG")
                    orderby
                        unit.SPG_DATE ascending
                    select new SPGSTORING_DATA_DETAIL(unit.SPG_ID, unit.SPG_DATE.Value, unit.MACHINE_ID, unit.FRAME_ID,unit.QR_CODE);
                return itemsObj.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        //public String ValidateStoring(String machine_or_frame_id,String Qrcode, String head_location_id, bool is_print)
        //{
        //    try
        //    {
        //        MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
        //        if (unit != null)
        //        {
        //            //Tambahan Dev.Magang 



        //            if (unit.STATUS == "SPG" || unit.STORING_DATE == null)
        //            {
        //                try
        //                {
        //                    BeginTransaction();
        //                    Validation(unit, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

        //                    String site_id_old = unit.SITE_ID;
        //                    String warehouse_id_old = unit.WAREHOUSE_ID;
        //                    String location_id_old = unit.LOCATION_ID;
        //                    String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, head_location_id);
        //                    String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);

        //                    // insert data to WMS
        //                    unit.LOCATION_ID = location_id_new;
        //                    unit.WAREHOUSE_ID = warehouse_id_new;

        //                    // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
        //                    MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
        //                    if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
        //                    {
        //                        unit.STATUS = "C";
        //                    }
        //                    else if (locationNew.LOCATION_TYPE.Equals("I"))
        //                    {
        //                        if (unit.IS_READYTOPICK.Equals("Y"))
        //                        {
        //                            throw new MPMException("Can't move to intransit location if unit in ready to pick");
        //                        }
        //                        unit.STATUS = "U";
        //                        unit.IS_RFS = "N";
        //                    }
        //                    else
        //                    {
        //                        unit.STATUS = "RFS";
        //                        unit.IS_RFS = "Y";
        //                    }
        //                    unit.STORING_DATE = MPMDateUtil.DateTime;
        //                    unit.IS_READYTOPICK = "N";
        //                    //unit.QR_CODE = Qrcode;

        //                    // update warehouse_location actual stock in old location
        //                    UpdateLocationStock(UserSession.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

        //                    // update warehouse_location actual stock in new location
        //                    UpdateLocationStock(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

        //                    UnitObject.Update(unit, "ST", site_id_old, warehouse_id_old, location_id_old);
        //                    Commit();
        //                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
        //                                 where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
        //                                 select a).Take(1).FirstOrDefault();

        //                    MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
        //                    _StoringVendorModel.createvehiclevendore(Qrcode, viewtypecolor.marketname, "Honda", viewtypecolor.namecolor, unit.MACHINE_ID, unit.FRAME_ID, unit.UNIT_YEAR, select.Latitude,select.Longitude,unit.SPG_ID);
        //                }
        //                catch (MPMException e)
        //                {
        //                    Rollback();
        //                    throw new MPMException(e.Message);
        //                }


        //                MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
        //                String nameType = unit.UNIT_TYPE;
        //                if (viewProduct != null)
        //                {
        //                    nameType = viewProduct.nametype;
        //                }

        //                if (is_print)
        //                    PrintStoring(
        //                        unit.MACHINE_ID, 
        //                        unit.SITE_ID, 
        //                        unit.LOCATION_ID, 
        //                        unit.COLOR_TYPE, nameType, 
        //                        unit.UNIT_YEAR,
        //                        unit.SPG_DATE.Value,
        //                        unit.UNIT_TYPE);

        //                return unit.LOCATION_ID;
        //            }
        //            else
        //            {
        //                throw new MPMException("Unit already validated !!!");
        //            }
        //        }
        //        else
        //        {
        //            throw new MPMException("Unit not found !!!");
        //        }
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}


        public String ProsesStoring(string machine_or_frame_id, String Qrcode, String head_location_id, bool is_print)
        {
            MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
            //if (unit.STATUS == "SPG" || unit.STORING_DATE == null)
            //{
                try
                {
                    BeginTransaction();
                    Validation(unit, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                    String site_id_old = unit.SITE_ID;
                    String warehouse_id_old = unit.WAREHOUSE_ID;
                    String location_id_old = unit.LOCATION_ID;
                    String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, head_location_id);
                    String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);

                    // insert data to WMS
                    unit.LOCATION_ID = location_id_new;
                    unit.WAREHOUSE_ID = warehouse_id_new;

                    // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                    MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                    if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                    {
                        unit.STATUS = "C";
                    }
                    else if (locationNew.LOCATION_TYPE.Equals("I"))
                    {
                        if (unit.IS_READYTOPICK.Equals("Y"))
                        {
                            throw new MPMException("Can't move to intransit location if unit in ready to pick");
                        }
                        unit.STATUS = "U";
                        unit.IS_RFS = "N";
                    }
                    else
                    {
                        unit.STATUS = "RFS";
                        unit.IS_RFS = "Y";
                    }
                    unit.STORING_DATE = MPMDateUtil.DateTime;
                    unit.IS_READYTOPICK = "N";
                    //unit.QR_CODE = Qrcode;

                    // update warehouse_location actual stock in old location
                    UpdateLocationStock(UserSession.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

                    // update warehouse_location actual stock in new location
                    UpdateLocationStock(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                    UnitObject.Update(unit, "ST", site_id_old, warehouse_id_old, location_id_old);
                    Commit();


                    //bool cekkodeqr = _StoringVendorModel.cekqrcodevendore(Qrcode);
                    //if (cekkodeqr)
                    //{
                    //    if (unit.STORING_DATE == null)
                    //    {
                    //        //updateStoringUnit(machine_or_frame_id);
                    //        ////storingdate
                    //    }

                    //}

                    //var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                    //                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                    //                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    //_StoringVendorModel.createvehiclevendore(Qrcode, viewtypecolor.marketname, "Honda", viewtypecolor.namecolor, unit.MACHINE_ID, unit.FRAME_ID, unit.UNIT_YEAR, select.Latitude, select.Longitude, unit.SPG_ID);
                }
                catch (MPMException e)
                {
                    Rollback();
                    throw new MPMException(e.Message);
                }


                //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                //String nameType = unit.UNIT_TYPE;
                //if (viewProduct != null)
                //{
                //    nameType = viewProduct.nametype;
                //}

                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                if (viewProduct != null)
                {
                    //nameType = viewProduct.nametype;
                    nameType = viewProduct.ITEMNAME;
                }

                if (is_print)
                    PrintStoring(
                        unit.MACHINE_ID,
                        unit.SITE_ID,
                        unit.LOCATION_ID,
                        unit.COLOR_TYPE, nameType,
                        unit.UNIT_YEAR,
                        unit.SPG_DATE.Value,
                        unit.UNIT_TYPE);

                return unit.LOCATION_ID;
            //}
            //else
            //{
            //    throw new MPMException("Unit already validated !!!");
            //}
        }
        public void Titipan (string machine_or_frame_id, string head_location_id)
        {
            MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
            // Insert Titipan 
            String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, head_location_id);
            String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);
            try
            {
                BeginTransaction();
                MPMWMS_LOKASI_TITIPAN insert = new MPMWMS_LOKASI_TITIPAN();
                insert.STATUS = 0;
                insert.SITE_ID = UserSession.SITE_ID_MAPPING;
                insert.WAREHOUSE_ID = warehouse_id_new;
                insert.LOCATION_ID = location_id_new;
                insert.MACHINE_ID = unit.MACHINE_ID;
                insert.SHIPPINGLIST_ID = unit.SHIPPING_ID;
                insert.TGLTITIPAN = DateTime.Now;
                insert.NOTE = "DO Virtual ";
                insert.ISLOCKED = false;
                _LokasiTitipan.InsertLokasiTitipan(insert);
                Commit();

            }
            catch (MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }

            // Tutup Insert Titipan
        }

        public void TitipanOpenGudang(string machine_or_frame_id, string head_location_id)
        {
            MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
            // Insert Titipan 
            String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, head_location_id);
            String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);
            try
            {
                BeginTransaction();
                MPMWMS_LOKASI_TITIPAN insert = new MPMWMS_LOKASI_TITIPAN();
                insert.STATUS = 0;
                insert.SITE_ID = UserSession.SITE_ID_MAPPING;
                insert.WAREHOUSE_ID = warehouse_id_new;
                insert.LOCATION_ID = location_id_new;
                insert.MACHINE_ID = unit.MACHINE_ID;
                insert.SHIPPINGLIST_ID = unit.SHIPPING_ID;
                insert.TGLTITIPAN = DateTime.Now;
                insert.NOTE = "DO Open Gudang ";
                insert.ISLOCKED = false;
                _LokasiTitipan.InsertLokasiTitipan(insert);
                Commit();

            }
            catch (MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }

            // Tutup Insert Titipan
        }
        
        public String ValidateStoring(String machine_or_frame_id, String Qrcode, String head_location_id, bool is_print)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.ItemByMachine(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                if (unit == null)
                {
                    unit = UnitObject.ItemByFrame(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                }
                string location_id = "";
                    if (unit != null)
                    {
                        //if(unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                        //{
                        //    throw new MPMException("Unit Not Found !!!");
                        //}
                        //MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
                        if (unit.STATUS == "SPG" || unit.STORING_DATE == null)
                        {
                            try
                            {
                                BeginTransaction();
                                Validation(unit, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                                String site_id_old = unit.SITE_ID;
                                String warehouse_id_old = unit.WAREHOUSE_ID;
                                String location_id_old = unit.LOCATION_ID;
                                String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, head_location_id);
                                String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);

                                // insert data to WMS
                                unit.LOCATION_ID = location_id_new;
                                unit.WAREHOUSE_ID = warehouse_id_new;

                                // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                                MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                                if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                                {
                                    unit.STATUS = "C";
                                }
                                else if (locationNew.LOCATION_TYPE.Equals("I"))
                                {
                                    if (unit.IS_READYTOPICK.Equals("Y"))
                                    {
                                        throw new MPMException("Can't move to intransit location if unit in ready to pick");
                                    }
                                    unit.STATUS = "U";
                                    unit.IS_RFS = "N";
                                }
                                else
                                {
                                    unit.STATUS = "RFS";
                                    unit.IS_RFS = "Y";
                                }
                                unit.STORING_DATE = MPMDateUtil.DateTime;
                                unit.IS_READYTOPICK = "N";
                                

                                // update warehouse_location actual stock in old location
                                UpdateLocationStock(UserSession.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

                                // update warehouse_location actual stock in new location
                                UpdateLocationStock(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                                UnitObject.Update(unit, "ST", site_id_old, warehouse_id_old, location_id_old);
                                Commit();

                            
                            }
                            catch (MPMException e)
                            {
                                Rollback();
                                throw new MPMException(e.Message);
                            }


                            //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                            //String nameType = unit.UNIT_TYPE;
                            //if (viewProduct != null)
                            //{
                            //    nameType = viewProduct.nametype;
                            //}
                            MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                            String nameType = unit.UNIT_TYPE;
                            if (viewProduct != null)
                            {
                                //nameType = viewProduct.nametype;
                                nameType = viewProduct.ITEMNAME;
                            }

                            if (is_print)
                                PrintStoring(
                                    unit.MACHINE_ID,
                                    unit.SITE_ID,
                                    unit.LOCATION_ID,
                                    unit.COLOR_TYPE, nameType,
                                    unit.UNIT_YEAR,
                                    unit.SPG_DATE.Value,
                                    unit.UNIT_TYPE);

                            location_id = unit.LOCATION_ID;
                        }
                        else
                        {
                            throw new MPMException("Unit already validated !!!");
                        }
            

                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!");
                    }

                return location_id;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DoStoringBySPG(String spg_id, String location_id)
        {
            List<SPGSTORING_DATA_DETAIL> list = ListSPGUnitForStoring(spg_id).ToList();
            try
            {
                foreach (var u in list)
                {
                    ValidateStoring(u._MACHINE_ID,u._QRCODE, location_id, false);
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DoStoringBySPGBatch(String spg_id, String location_id)
        {
            List<SPGSTORING_DATA_DETAIL> list = ListSPGUnitForStoring(spg_id).ToList();
            try
            {
                xtsMuliaService.xtsstoringServiceClient client = ConnectWebServiceStoring();
                foreach (var u in list)
                {
                    IQueryable<MPMWMS_UNIT> search = from unitQuery in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                                                     where (unitQuery.FRAME_ID == u._FRAME_ID || unitQuery.MACHINE_ID == u._MACHINE_ID)
                                                     select unitQuery;

                    MPMWMS_UNIT unit = search.FirstOrDefault();
                    if (unit != null)
                    {
                        // validation data unit
                        Validation(unit, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                        // preparation for update data
                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;
                        String location_id_new = GetLocationCell(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id);
                        String warehouse_id_new = LocationObject.GetWarehouse(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, location_id_new);
                        DateTime date_storing = MPMDateUtil.DateTime.Date;

                        // insert data to WMS
                        unit.LOCATION_ID = location_id_new;
                        unit.WAREHOUSE_ID = warehouse_id_new;
                        //unit.STATUS = "PREST";
                        //unit.STORING_DATE = DateTime.Now;

                        // ini dipindah ke store procedure
                        unit.IS_RFS = "N";
                        unit.IS_READYTOPICK = "N";
                        unit.STATUS = "ST";
                        unit.STORING_DATE = date_storing;

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(UserSession.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        UnitObject.Update(unit, "ST", site_id_old, warehouse_id_old, location_id_old);

                        // send to ax web service  
                        try
                        {
                            EntityKey[] keys;
                            keys = new EntityKey[1];

                            Axdxtsstoring heads = new Axdxtsstoring();
                            AxdEntity_xtsStoringTable[] head = new AxdEntity_xtsStoringTable[1];
                            head[0] = new AxdEntity_xtsStoringTable();

                            head[0].MainDealer = UserSession.MAINDEALER_ID;
                            head[0].CategoryCode = "U";

                            head[0].ItemId = unit.UNIT_TYPE;
                            head[0].ChassisNo = unit.FRAME_ID;
                            head[0].ColorId = unit.COLOR_TYPE;
                            head[0].Qty = 1;
                            head[0].QtySpecified = true;

                            head[0].FromSiteId = site_id_old;
                            head[0].ToSiteId = unit.SITE_ID;

                            head[0].FromWarehouseId = warehouse_id_old;
                            head[0].ToWarehouseId = warehouse_id_new;

                            head[0].Transdate = date_storing;
                            head[0].TransdateSpecified = true;

                            heads.xtsStoringTable = head;

                            keys = client.create(ContextWS, heads);
                            SubmitChanges();
                        }
                        catch (Exception e)
                        {
                            throw new MPMException(e.Message);
                        }
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!");
                    }
                }
                DisconnectWebServiceStoring(client);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public String DoStoring(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id)
        {            
            try
            {
                IQueryable<MPMWMS_UNIT> search = from unitQuery in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                                                 where (unitQuery.FRAME_ID == machine_or_frame_id || unitQuery.MACHINE_ID == machine_or_frame_id)
                                                 select unitQuery;

                MPMWMS_UNIT unit = search.FirstOrDefault();
                if (unit != null)
                {
                    // validation data unit
                    Validation(unit, dataarea_id, site_id);

                    // preparation for update data
                    String site_id_old = unit.SITE_ID;
                    String warehouse_id_old = unit.WAREHOUSE_ID;
                    String location_id_old = unit.LOCATION_ID;
                    String location_id_new = GetLocationCell(dataarea_id, site_id, head_location_id);
                    String warehouse_id_new = LocationObject.GetWarehouse(dataarea_id, site_id, location_id_new);
                    DateTime date_storing = MPMDateUtil.DateTime.Date;

                    // insert data to WMS
                    unit.LOCATION_ID = location_id_new;
                    unit.WAREHOUSE_ID = warehouse_id_new;
                    //unit.STATUS = "PREST";
                    //unit.STORING_DATE = DateTime.Now;

                    // ini dipindah ke store procedure
                    unit.IS_RFS = "Y";
                    unit.IS_READYTOPICK = "N";
                    unit.STATUS = "RFS";
                    unit.STORING_DATE = date_storing;

                    // update warehouse_location actual stock in old location
                    UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                    // update warehouse_location actual stock in new location
                    UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                    UnitObject.Update(unit, "ST", site_id_old, warehouse_id_old, location_id_old); 
                    SubmitChanges();

                    // send to ax web service  
                    try
                    {
                        xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebService();
                        client.AutoCreateTransfer(
                            ContextWS,
                            unit.UNIT_TYPE,
                            "",
                            unit.COLOR_TYPE,
                            "",
                            site_id_old,
                            warehouse_id_old,
                            unit.SITE_ID,
                            warehouse_id_new,
                            "",
                            -1,
                            unit.FRAME_ID,
                            "U",
                            UserSession.MAINDEALER_ID,
                            date_storing);
                        DisconnectWebService(client);
                    }
                    catch (Exception e)
                    {
                        throw new MPMException(e.Message);
                    }
                    
                    // print barcode
                    //Print(unit.MACHINE_ID, unit.SITE_ID, unit.LOCATION_ID, unit.COLOR_TYPE, unit.UNIT_TYPE, unit.SPG_DATE.Value);
                    return unit.LOCATION_ID;
                }
                else
                {
                    throw new MPMException("Unit not found !!!!");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Reprint(String machine_or_frame_id)
        {
            MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
            if (unit != null)
            {
                

                //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                //String nameType = unit.UNIT_TYPE;
                //String nameColor = unit.COLOR_TYPE;
                //if (viewProduct != null)
                //{
                //    nameType = viewProduct.nametype;
                //}
                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                String nameColor = unit.COLOR_TYPE;
                if (viewProduct != null)
                {
                    //nameType = viewProduct.nametype;
                    nameType = viewProduct.ITEMNAME;
                }
                if (unit.SHIPPING_ID != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }
                    else
                    {
                        if (unit.IS_VIRTUAL == 1)
                        {
                            //get shipping 
                            MPMWMS_SHIPPING_LIST spg = _Shipping.StoringSPG(unit.SHIPPING_ID, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                            String dealerCode = spg.DEALER_CODE;
                            String dealerName = spg.DEALER_NAME;
                            String sopir = spg.DRIVER;
                            String expedition = spg.EXPEDITION_ID;
                            String nopol = spg.POLICE_NUMBER;
                            PrintPicking(unit.MACHINE_ID, unit.SITE_ID, nameColor, nameType, dealerCode, dealerName, sopir, expedition, nopol);
                        }
                        else
                        {
                            PrintStoring(
                            unit.MACHINE_ID,
                            unit.SITE_ID,
                            unit.LOCATION_ID,
                            unit.COLOR_TYPE, nameType,
                            unit.UNIT_YEAR,
                            unit.SPG_DATE.Value,
                            unit.UNIT_TYPE
                            );
                        }
                    }
                }
                else
                {
                    PrintStoring(
                    unit.MACHINE_ID,
                    unit.SITE_ID,
                    unit.LOCATION_ID,
                    unit.COLOR_TYPE, nameType,
                    unit.UNIT_YEAR,
                    unit.SPG_DATE.Value,
                    unit.UNIT_TYPE
                    );
                }

                    
            }
            else
            {
                throw new MPMException("Unit not found !!!");
            }
        }
        public void updatemappingqrcode(MPMWMS_UNIT item,string qrcode, string machineid)
        {
            BeginTransaction();
            try
            {
               
                item.QR_CODE = qrcode;
                if(UserSession.USER_ID.Length > 10)
                {
                    item.MODIF_BY = UserSession.USER_ID.Substring(0, 10);
                }
                else
                {
                    item.MODIF_BY = UserSession.USER_ID;
                }
                
                item.MODIF_DATE = DateTime.Now;
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        public void updateqrcode(String machine_or_frame_id, String Qrcode)
        {
            BeginTransaction();
            try
            {
                MPMWMS_UNIT item = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
                               
                item.QR_CODE = Qrcode;
                item.MODIF_BY = UserSession.USER_ID;
                item.MODIF_DATE = DateTime.Now;
                Commit();
            }catch(MPMException ex)
            {
                Rollback();
            }
        }

        public void updateStoringUnit(String machine_or_frame_id)
        {
            BeginTransaction();
            try
            {
                MPMWMS_UNIT item = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);

                item.STORING_DATE = DateTime.Now;
                item.MODIF_BY = UserSession.NPK;
                item.MODIF_DATE = DateTime.Now;
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        public void updateQRStoringUnit(String machine_or_frame_id, string qrcode)
        {
            BeginTransaction();
            try
            {
                MPMWMS_UNIT item = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);

                item.QR_CODE = qrcode;
                item.MODIF_BY = UserSession.USER_ID;
                item.MODIF_DATE = DateTime.Now;
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        public void updateTitipan(String machine_or_frame_id,string siteid)
        {
            BeginTransaction();
            try
            {
                MPMWMS_LOKASI_TITIPAN item = _LokasiTitipan.Item(machine_or_frame_id,siteid);

                item.STATUS = 1;
                item.TGLKELUAR = DateTime.Now;
                item.MODIFIEDBY = UserSession.USER_ID;
                item.MODIFIEDDATETIME = DateTime.Now;
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        // update ardian
        public String GetMachineIDFromQR(String machine_or_frame_qr)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.ItemQR(UserSession.DATAAREA_ID, machine_or_frame_qr);
                if (unit != null)
                {
                    return unit.MACHINE_ID;
                }
                else
                {
                    return "0";
                }
            }
            catch (MPMException ex)
            {
                return "";
            }
        }

        public String ProsesStoringUnit(String machine_or_frame_id, String head_location_id, bool is_print)
        {
            try
            {
                MPMWMS_UNIT unit = this.UnitObject.Item(this.UserSession.DATAAREA_ID, machine_or_frame_id);
                if (unit == null)
                    throw new MPMException("Unit Tidak Ditemukan");
                MPMINVFAKTUR mpminvfaktur = this.UnitObject.Context.MPMINVFAKTURs.Where<MPMINVFAKTUR>((Expression<Func<MPMINVFAKTUR, bool>>)(x => x.ENGINENO.Equals(unit.MACHINE_ID) && x.NORANGKA.Equals(unit.FRAME_ID))).FirstOrDefault<MPMINVFAKTUR>();
                if (mpminvfaktur == null)
                    throw new MPMException("Data faktur dari unit tidak ditemukan");
                if (!mpminvfaktur.KODECABANG.Equals(this.UserSession.MAINDEALER_ID))
                    throw new MPMException(string.Format("Kode Main Dealer Unit {0} tidak sama dengan kode Main Dealer Session User {1}", (object)mpminvfaktur.KODECABANG, (object)this.UserSession.MAINDEALER_ID));
                string locationId1;
                if (unit.SHIPPING_ID != null)
                {
                    if (unit.SITE_ID == this.UserSession.SITE_ID_MAPPING)
                    {
                        int? isVirtual = unit.IS_VIRTUAL;
                        if ((isVirtual.GetValueOrDefault() != 1 ? 0 : (isVirtual.HasValue ? 1 : 0)) == 0 /* || unit.STORING_DATE.HasValue */)
                            throw new MPMException("Unit already validated");
                        if (!this._LokasiTitipan.DataTitipan(unit.MACHINE_ID).Count<LokasiTitipanRec>().Equals(0))
                            throw new MPMException("Unit tersebut sudah distoring");
                        MPMWMS_SHIPPING_LIST mpmwmsShippingList = this._Shipping.StoringSPG(unit.SHIPPING_ID, this.UserSession.DATAAREA_ID, this.UserSession.MAINDEALER_ID, this.UserSession.SITE_ID_MAPPING);
                        string dealerCode = mpmwmsShippingList.DEALER_CODE;
                        string dealerName = mpmwmsShippingList.DEALER_NAME;
                        string driver = mpmwmsShippingList.DRIVER;
                        string expeditionId = mpmwmsShippingList.EXPEDITION_ID;
                        string policeNumber = mpmwmsShippingList.POLICE_NUMBER;
                        this.updateStoringUnit(machine_or_frame_id);
                        MPMSERVVIEWDAFTARUNITWARNA mpmservviewdaftarunitwarna = this.ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        string type_name = unit.UNIT_TYPE;
                        string colorType = unit.COLOR_TYPE;
                        if (mpmservviewdaftarunitwarna != null)
                            type_name = mpmservviewdaftarunitwarna.ITEMNAME;
                        if (is_print)
                            this.PrintPicking(unit.MACHINE_ID, unit.SITE_ID, colorType, type_name, dealerCode, dealerName, driver, expeditionId, policeNumber);
                        this.Titipan(machine_or_frame_id, head_location_id);
                        locationId1 = unit.LOCATION_ID;
                    }
                    else
                    {
                        DateTime? nullable = unit.STORING_DATE;
                        if (!nullable.HasValue)
                            throw new MPMException("Unit tersebut belum distoring untuk pertama kali");
                        if (!this._LokasiTitipan.DataTitipanSiteSama(unit.MACHINE_ID, this.UserSession.SITE_ID_MAPPING).Count<LokasiTitipanRec>().Equals(0))
                            throw new MPMException("Unit tersebut sudah distoring");
                        List<LokasiTitipanRec> source = this._LokasiTitipan.Cekdatatitipan(unit.MACHINE_ID, this.UserSession.SITE_ID_MAPPING);
                        if (!source.Count<LokasiTitipanRec>().Equals(0))
                        {
                            foreach (LokasiTitipanRec lokasiTitipanRec in source)
                            {
                                nullable = lokasiTitipanRec.tglkeluar;
                                int num;
                                if (nullable.HasValue)
                                {
                                    int? status = lokasiTitipanRec.status;
                                    num = (status.GetValueOrDefault() != 0 ? 0 : (status.HasValue ? 1 : 0)) == 0 ? 1 : 0;
                                }
                                else
                                    num = 0;
                                if (num == 0)
                                    this.updateTitipan(lokasiTitipanRec.machineid, lokasiTitipanRec.siteid);
                            }
                        }
                        this.TitipanOpenGudang(machine_or_frame_id, head_location_id);
                        locationId1 = unit.LOCATION_ID;
                    }
                }
                else
                {
                    if (unit.SITE_ID != this.UserSession.SITE_ID_MAPPING)
                        throw new MPMException("Unit Tidak Ditemukan");
                    DateTime? nullable = unit.STORING_DATE;
                    if (nullable.HasValue)
                        throw new MPMException("Unit tersebut sudah distoring");
                    try
                    {
                        this.BeginTransaction();
                        this.Validation(unit, this.UserSession.DATAAREA_ID, this.UserSession.SITE_ID_MAPPING);
                        string siteId = unit.SITE_ID;
                        string warehouseId = unit.WAREHOUSE_ID;
                        string locationId2 = unit.LOCATION_ID;
                        string locationCell = this.GetLocationCell(this.UserSession.DATAAREA_ID, this.UserSession.SITE_ID_MAPPING, head_location_id);
                        string warehouse = this.LocationObject.GetWarehouse(this.UserSession.DATAAREA_ID, this.UserSession.SITE_ID_MAPPING, locationCell);
                        unit.LOCATION_ID = locationCell;
                        unit.WAREHOUSE_ID = warehouse;
                        MPMWMS_WAREHOUSE_LOCATION warehouseLocation = this.LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (warehouseLocation.LOCATION_TYPE.Equals("N") || warehouseLocation.LOCATION_TYPE.Equals("B"))
                            unit.STATUS = "C";
                        else if (warehouseLocation.LOCATION_TYPE.Equals("I"))
                        {
                            if (unit.IS_READYTOPICK.Equals("Y"))
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }
                        unit.STORING_DATE = new DateTime?(MPMDateUtil.DateTime);
                        unit.IS_READYTOPICK = "N";
                        this.UpdateLocationStock(this.UserSession.DATAAREA_ID, siteId, warehouseId, locationId2, -1);
                        this.UpdateLocationStock(this.UserSession.DATAAREA_ID, this.UserSession.SITE_ID_MAPPING, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);
                        this.UnitObject.Update(unit, "ST", siteId, warehouseId, locationId2);
                        this.Commit();
                        locationId1 = unit.LOCATION_ID;
                    }
                    catch (MPMException ex)
                    {
                        this.Rollback();
                        throw new MPMException(ex.Message);
                    }
                    MPMSERVVIEWDAFTARUNITWARNA mpmservviewdaftarunitwarna = this.ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    string str = unit.UNIT_TYPE;
                    if (mpmservviewdaftarunitwarna != null)
                        str = mpmservviewdaftarunitwarna.ITEMNAME;
                    if (is_print)
                    {
                        string machineId = unit.MACHINE_ID;
                        string siteId = unit.SITE_ID;
                        string locationId3 = unit.LOCATION_ID;
                        string colorType = unit.COLOR_TYPE;
                        string unit_name = str;
                        string unitYear = unit.UNIT_YEAR;
                        nullable = unit.SPG_DATE;
                        DateTime spg_date = nullable.Value;
                        string unitType = unit.UNIT_TYPE;
                        this.PrintStoring(machineId, siteId, locationId3, colorType, unit_name, unitYear, spg_date, unitType);
                    }
                }
                return locationId1;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        #region QR CODE UNIT

        //Update Baru Dev.Magang

        public String CekQrCode(String machine_or_frame_id)
        {
            try
            {


                MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
                String QRCODE = "";

                if (unit != null)
                {
                    if (unit.QR_CODE != null)
                    {
                        QRCODE = unit.QR_CODE;
                    }
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }


                return QRCODE;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        public String ProsesMapping(String machine_or_frame_id, String qrcode)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    if (unit.QR_CODE != null)
                    {
                        throw new MPMException("Unit Telah DiMapping");
                    }

                    bool cekkodeqr = _StoringVendorModel.cekqrcodevendore(qrcode);
                    if (!cekkodeqr)
                    {
                        throw new MPMException("QR CODE Tidak Valid");
                    }

                    // Kirim Data Vendor dan Update Qr-Code UNit
                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    _StoringVendorModel.createvehiclevendore(qrcode, viewtypecolor.ITEMMARKETNAME, "Honda", viewtypecolor.ITEMCOLORCODE, unit.MACHINE_ID, unit.FRAME_ID, unit.UNIT_YEAR, select.Latitude, select.Longitude, unit.SPG_ID);

                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
            return qrcode;
        }

        public String ProsesQrCode(String machine_or_frame_id, String qrcode)
        {
            try
            {


                MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);


                if (unit != null)
                {
                    bool cekkodeqr = _StoringVendorModel.cekqrcodevendore(qrcode);
                    if (!cekkodeqr)
                    {
                        throw new MPMException("QR CODE Tidak Valid");
                    }

                    // Kirim Data Vendor dan Update Qr-Code UNit
                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    _StoringVendorModel.createvehiclevendore(qrcode, viewtypecolor.ITEMMARKETNAME, "Honda", viewtypecolor.ITEMCOLORCODE, unit.MACHINE_ID, unit.FRAME_ID, unit.UNIT_YEAR, select.Latitude, select.Longitude, unit.SPG_ID);

                    // Tutup Kirim Data Vendor dan Update Qr-Code UNit
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }


                return qrcode;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        //End Update baru Dev.Magang

       //get location sebelumnya location 
       public string GetLocationOld(String machine_or_frame_id)
        {
            String QRCode = "";
            String qrcode = "";
            QRCode = machine_or_frame_id;
            char[] spearator = { '/' };
            String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            qrcode = strlist[strlist.Length - 1];

            //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

            if (QRCode == "")
            {
                throw new MPMException("Belum Scan QR Code");
            }

            MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
            if (unit != null)
            {
                return unit.LOCATION_ID;
            }
            else {
                //return null;
                throw new MPMException("Unit not found !!!!");
            }
        }

        //tambahan anas 
       //public XVS_BOM_VIEW getjurnalbundelbom
       public XVS_BOM_VIEW getjurnalbundelbom(String type, String color, String site_id, String warehouse_id)
       {
           try
           {
               XVS_BOM_VIEW bom1 = UnitObject.ItemByjurnalBundleUnitBom1(type, color, site_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID);
               var journalbom = bom1.JOURNALID;

               XVS_BOM_VIEW bom = UnitObject.ItemByjurnalBundleUnitBom2(journalbom);
               return bom;
           }
           catch (MPMException ex)
           {
               throw new MPMException(ex.Message);
           }
       }

       //tambahan anas 
       //public XVS_BOR_VIEW getjurnalbundelbor
       public XVS_BOR_VIEW getjurnalbundelbor(String type, String color, String site_id, String warehouse_id)
       {
           try
           {
               XVS_BOR_VIEW bor1 = UnitObject.ItemByjurnalBundleUnitBor1(type, color, site_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID);
               var journalbor = bor1.JOURNALID;

               XVS_BOR_VIEW bor = UnitObject.ItemByjurnalBundleUnitBor2(journalbor);
               return bor;
           }
           catch (MPMException ex)
           {
               throw new MPMException(ex.Message);
           }
       }

       //tambahan anas
        //public MPMWMS_MUTATION_LOCATION getjurnal(String machine_or_frame_id)
        public string getjurnal(String machine_or_frame_id)
        {
            String QRCode = "";
            String qrcode = "";
            QRCode = machine_or_frame_id;
            char[] spearator = { '/' };
            String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            qrcode = strlist[strlist.Length - 1];

            //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

            if (QRCode == "")
            {
                throw new MPMException("Belum Scan QR Code");
            }

            MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
            if (unit == null)
            {
                throw new MPMException("Unit not found !!!!");
            }

            //MPMWMS_MUTATION_LOCATION unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
            MPMWMS_MUTATION_LOCATION loc = UnitObject.ItemByjurnal(unit.MACHINE_ID);
            //var loc = UnitObject.ItemByjurnal("JFK1E1131870");
            //return loc.MUTATION_LOCATION_ID;
            if (loc == null)
            {
                throw new MPMException("Unit not found in MUTATION LOCATION !!!!");
            }
            else
            {
                //return loc.MUTATION_LOCATION_ID;
                return loc.MUTATION_LOCATION_ID;
            }
            
        }

        //tambahan anas  get data mutation_location new and old
        public MPMWMS_MUTATION_LOCATION DataMutationLocationNewOld(String Nosin, String Mutation_ID)
        {
            MPMWMS_MUTATION_LOCATION loc = UnitObject.ItemByjurnal1(Nosin, Mutation_ID);
           
            if (loc == null)
            {
                throw new MPMException("Unit not found in MUTATION LOCATION !!!!");
            }
            else
            {
                return loc;
            }
        }

        //tambahan anas 
        //MPMWMSPDTBUNDLEUNITACC
        public string GetjurnalBundleUnit(String machine_or_frame_id, String STATUS)
        {
            String QRCode = "";
            String qrcode = "";
            QRCode = machine_or_frame_id;
            char[] spearator = { '/' };
            String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            qrcode = strlist[strlist.Length - 1];

            //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

            if (QRCode == "")
            {
                throw new MPMException("Belum Scan QR Code");
            }

            MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
            // MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
            //MPMWMS_MUTATION_LOCATION loc = UnitObject.ItemByjurnal(unit.MACHINE_ID);
            //XVS_BOM_VIEW bom = UnitObject.ItemByjurnalBundleUnit(unit.UNIT_TYPE, unit.COLOR_TYPE); //tambahan anas
            var jurnalid = "";
            if (STATUS == "BOM")
            {
                MPMWMS_BOM bom = UnitObject.ItemByjurnalBundleUnitBom(unit.UNIT_TYPE, unit.COLOR_TYPE); //tambahan anas
                jurnalid = bom.JOURNAL_ID;
            }
            else if (STATUS == "BOR")
            {
                MPMWMS_BOR bor = UnitObject.ItemByjurnalBundleUnitBor(unit.UNIT_TYPE, unit.COLOR_TYPE); //tambahan anas
                jurnalid = bor.JOURNAL_ID;
            }

            return jurnalid;
        }

        //tambahan anas 
        public string getjurnalpicking(String picking_id)
        {
            String QRCode = "";
            String qrcode = "";
            QRCode = picking_id;
            char[] spearator = { '/' };
            String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            qrcode = strlist[strlist.Length - 1];

            //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

            if (QRCode == "")
            {
                throw new MPMException("Belum Scan QR Code");
            }

            MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
            MPMWMS_PICKING loc = UnitObject.ItemByjurnalPicking(unit.PICKING_ID);
            
            return loc.PICKING_ID;
        }

        // update mapping QR tanpa call WS vendor QR
        public void ProsesTrackingQRnoWS(String machine_or_frame_id, String type, String flag, String OldLocation)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
               
                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    _StoringVendorModel.insertQRUnit(unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag, OldLocation);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // update mapping QR tanpa call WS vendor QR
        //tambahan anas
        public void ProsesTrackingQRMutasi1(String newJurnal, String machine_or_frame_id)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    if(viewtypecolor == null)
                    {
                        throw new MPMException("Unit tidak ditemukan di AX.");
                    }


                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //MPMWMS_MUTATION_LOCATION getMutationLocationOld = DataMutationLocationNewOld(machine_or_frame_id, oldjurnal);//tambahan anas
                    MPMWMS_MUTATION_LOCATION getMutationLocationNew = DataMutationLocationNewOld(unit.MACHINE_ID, newJurnal);//tambahan anas

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnitMutasiOut(getMutationLocationNew, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "2", "OUT");
                    _StoringVendorModel.insertQRUnitMutasiIn(getMutationLocationNew, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "2", "IN");
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // update mapping QR tanpa call WS vendor QR
        //tambahan anas
        public void ProsesTrackingQRMutasi2(String newJurnal, String machine_or_frame_id)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //MPMWMS_MUTATION_LOCATION getMutationLocationOld = DataMutationLocationNewOld(machine_or_frame_id, oldjurnal);//tambahan anas
                    MPMWMS_MUTATION_LOCATION getMutationLocationNew = DataMutationLocationNewOld(unit.MACHINE_ID, newJurnal);//tambahan anas

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnitMutasiOut(getMutationLocationNew, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "3", "OUT");
                    _StoringVendorModel.insertQRUnitMutasiIn(getMutationLocationNew, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "3", "IN");
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // update mapping QR tanpa call WS vendor QR
        //tambahan anas
        public void ProsesTrackingQRnoWS1(String machine_or_frame_id, String type, String flag)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    _StoringVendorModel.insertQRUnit1(unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // update mapping QR tanpa call WS vendor QR
        //tambahan anas
        public void ProsesTrackingQRnoWS3(String JOURNALID, String machine_or_frame_id, String type, String flag)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnit3(JOURNALID, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC Bom
        public void ProsesTrackingQRnoWSBundelBom(XVS_BOM_VIEW JOURNALID, String machine_or_frame_id, String type, String flag, String typecolor, String color, String site_id, String warehouse_id)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];


                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                XVS_BOM_VIEW bom1 = UnitObject.ItemByjurnalBundleUnitBom1(typecolor, color, site_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID);

                MPMWMS_UNIT unit1 = UnitObject.ItemByQRWithLocation(UserSession.DATAAREA_ID, qrcode, site_id, warehouse_id);

                if (unit1.UNIT_TYPE != bom1.ITEMID || unit1.COLOR_TYPE != bom1.INVENTCOLORID)
                {
                    throw new MPMException("Unit type and color mismatch!!!");
                }

                if (unit1.STATUS == "S")
                {
                    throw new MPMException("Unit already shipping!!!");
                }

                //MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
                if (unit1 != null)
                {
                    if (unit1.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit1.SITE_ID && a.DATAAREAID == unit1.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit1.UNIT_TYPE, unit1.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit1.UNIT_TYPE, unit1.COLOR_TYPE);
                    unit1.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRBundleUnitBom(JOURNALID, unit1, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC Bor
        public void ProsesTrackingQRnoWSBundelBor(XVS_BOR_VIEW JOURNALID, String machine_or_frame_id, String type, String flag, String typecolor, String color, String site_id, String warehouse_id)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];


                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                XVS_BOR_VIEW bor1 = UnitObject.ItemByjurnalBundleUnitBor1(typecolor, color, site_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID);

                MPMWMS_UNIT unit1 = UnitObject.ItemByQRWithLocation(UserSession.DATAAREA_ID, qrcode, site_id, warehouse_id);

                if (unit1.UNIT_TYPE != bor1.ITEMID || unit1.COLOR_TYPE != bor1.INVENTCOLORID)
                {
                    throw new MPMException("Unit type and color mismatch!!!");
                }

                if (unit1.STATUS == "S")
                {
                    throw new MPMException("Unit already shipping!!!");
                }

                //MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
                if (unit1 != null)
                {
                    if (unit1.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit1.SITE_ID && a.DATAAREAID == unit1.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit1.UNIT_TYPE, unit1.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit1.UNIT_TYPE, unit1.COLOR_TYPE);
                    unit1.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRBundleUnitBor(JOURNALID, unit1, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC
        public void ProsesTrackingQRnoWS4(/*String JOURNALID,*/ String machine_or_frame_id, String type, String flag)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRBundleUnit(/*JOURNALID,*/ unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // update mapping QR tanpa call WS vendor QR
        //tambahan anas
        public void ProsesTrackingQRnoWSPicking(String machine_or_frame_id, String type, String flag, String picking)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);

                MPMWMS_PICKING mpmwmspicking = UnitObject.ItemByPicking(picking);
                //CUSTTABLE cusTable = UnitObject.ItemByKdDealer(mpmwmspicking.DEALER_CODE);
                MPMSERVVIEWCHANNELH1 cusTable = UnitObject.ItemByKdDealerH1(mpmwmspicking.DEALER_CODE);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnitPicking(cusTable, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag, picking);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }
       
        // update mapping QR tanpa call WS vendor QR
        public void ProsesMappingQRnoWS(String machine_or_frame_id, String qrcode)
        {
            try
            {

                MPMWMS_UNIT unit = UnitObject.ItemByMachine(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                if(unit == null)
                {
                    unit = UnitObject.ItemByFrame(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                }

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    //if (unit.QR_CODE != null)
                    //{
                    //    throw new MPMException("Unit Telah Di Mapping");
                    //}

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //update anas(perubahan flag 1 menjadi out)
                    _StoringVendorModel.insertQRUnit(unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "1", "1");
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {
               
                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas (MPMWMSPDTMAPPINGQRUNIT)
        // update mapping QR tanpa call WS vendor QR
        public void ProsesMappingQRnoWSMappingQR(String machine_or_frame_id, String qrcode, String Unmatch,String Unit, String qrFromNosin, String nosinFromQr)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.ItemByMachine(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                MPMWMS_UNIT unit_error = UnitObject.ItemByMachineUnitError(qrcode, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                //MPMWMS_UNIT DataUnmatch1 = UnitObject.ItemByNosinUnmatch1(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                //MPMWMS_UNIT DataUnmatch2 = UnitObject.ItemByQrUnmatch2(qrcode, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                //tambahan anas
                //MPMWMSQRUNIT qrunit = UnitObject.ItemByMachineQrUnit(machine_or_frame_id, UserSession.SITE_ID_MAPPING);
                if (unit == null)
                {
                    unit = UnitObject.ItemByFrame(machine_or_frame_id, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                }

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    //if (unit.QR_CODE != null)
                    //{
                    //    throw new MPMException("Unit Telah Di Mapping");
                    //}

                    //tambahan anas (cek qr code)
                    //if (unit.QR_CODE != null)
                    //{
                    //    //tambahan anas(cek jika unmatcah = '1')
                    //    if (Unmatch == "1")
                    //    {
                    //          //updatemappingstsmatching(qrunit, "1", machine_or_frame_id);
                    //    }
                    //    //tambahan anas
                    //    throw new MPMException("Unit Telah Di Mapping dengan qrcode" + qrcode);
                    //}

                    //tambahan anas (cek nosin)
                    //if (unit.MACHINE_ID != null)
                    //{
                    //    //tambahan anas(cek jika unmatcah = '1')
                    //    if (Unmatch == "1")
                    //    {
                    //        //updatemappingstsmatching(qrunit, "1", machine_or_frame_id);
                    //    }
                    //    //tambahan anas
                    //    throw new MPMException("Unit Telah Di Mapping dengan nosin" + machine_or_frame_id);
                    //}

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();


                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    unit.QR_CODE = qrcode;
                    //if (unit_error != null && machine_or_frame_id == unit_error.MACHINE_ID)
                    //{
                    //    updatemappingqrcode(unit, qrcode, machine_or_frame_id);
                    //}
                    //else
                    if (unit_error != null)
                    {
                        updatemappingqrcode(unit_error, null, machine_or_frame_id);
                    }
                   
                        updatemappingqrcode(unit, qrcode, machine_or_frame_id);
                    

                    //update anas(perubahan flag 0 menjadi IN, PENAMBAHAN hardcode "AHM")
                    _StoringVendorModel.insertQRUnitMappingQR(unit, select, qrFromNosin, nosinFromQr, Unit, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "1", "IN","AHM", Unmatch);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        // QC UNIT NRFS
        //tambahan anas
        public void ProsesTrackingQRnoWSQcUnit(String machine_or_frame_id, String type, String flag)
        {
            try
            {
                String nosin = "";
                nosin = machine_or_frame_id;

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (nosin == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByNosin(UserSession.DATAAREA_ID, nosin, UserSession.SITE_ID_MAPPING);

                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    //unit.QR_CODE = nosin;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);
                    //additional 2021-03-07
                    String qrcodenew = unit.QR_CODE != "" ? unit.QR_CODE : nosin;

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnitClockingUni(unit, select, qrcodenew, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas
        public void updatemappingstsmatching(MPMWMSQRUNIT item, string unmatch1, string machineid)
        {
            BeginTransaction();
            try
            {

                item.STSMATCHING = unmatch1;
                if (UserSession.USER_ID.Length > 10)
                {
                    item.MODIFBY = UserSession.USER_ID.Substring(0, 10);
                }
                else
                {
                    item.MODIFBY = UserSession.USER_ID;
                }

                item.MODIFDATE = DateTime.Now;
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        //tambahan anas (ceknosin mapping)
        public  string  CekNosinMapping(String nosin)
        {
            string SetQR = "";
            try {

                MPMWMS_UNIT nosin_unit = UnitObject.ItemByCekNosin(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                MPMWMS_UNIT nosin_error = UnitObject.ItemByCekNosin(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                if (nosin_unit == null)
                {
                    nosin_unit = UnitObject.ItemByCekNoka(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                    if (nosin_unit != null) {
                        SetQR = nosin_unit.QR_CODE.ToString();
                    }
                    else if (nosin_unit == null) {
                        throw new MPMException("Unit tidak ditemukan");
                    }
                    
                }

                if (nosin_unit != null)
                {
                    if (nosin_unit.QR_CODE != null)
                    {
                        SetQR = nosin_unit.QR_CODE.ToString();
                    }
                }

                return SetQR;
            }
            catch (MPMException e) {

                throw new MPMException(e.Message);

            }
           
        }

        //tambahan anas (cek qr mapping)
        public string CekQrMapping(String qr/*, String nosin*/)
        {
            string SetNosin = "";
            try
            {
                MPMWMS_UNIT qr_unit = UnitObject.ItemByCekQr(qr, /*nosin,*/ UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                //if (qr_unit == null)
                //{
                //    qr_unit = UnitObject.ItemByCekNoka(qr, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                //    SetNosin = qr_unit.QR_CODE.ToString();
                //}

                if (qr_unit != null)
                {
                    if (qr_unit.MACHINE_ID != null)
                    {
                        SetNosin = qr_unit.MACHINE_ID.ToString();
                    }
                }
                return SetNosin;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        

        //tambahan anas
        public void UpdateUnmatch1(string nosin)
        {
            //MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nsoin, UserSession.SITE_ID_MAPPING);
            MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nosin, UserSession.SITE_ID_MAPPING);
            UpdateStsMatching(qrunit, "1");
        }

        //tambahan anas
        public void UpdateUnmatch0(string nosin)
        {
            MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nosin, UserSession.SITE_ID_MAPPING);
            UpdateStsMatching(qrunit, "0");
        }

        //tambahan anas
        public void UpdateUnmatchUnit(string nosin, string Unmatch)
        {
            //MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nsoin, UserSession.SITE_ID_MAPPING);
            MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nosin, UserSession.SITE_ID_MAPPING);
            UpdateStatusMatchingUnit(qrunit, Unmatch);
        }

        //tambahan anas
        public void UpdateStatusMatchingUnit(MPMWMSQRUNIT unit, string unmatch)
        {
            BeginTransaction();
            try
            {
                WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();

                //db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '1' where  ENGINENO = '" + unit.ENGINENO + "'and SITEID = '" + unit.SITEID + "' and MODIFBY = '" + UserSession.USER_ID + "' and MODIFDATE = '" + DateTime.Now +"' ");
                db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '" + unmatch + "', MODIFBY = '" + UserSession.USER_ID + "', MODIFDATE = '" + DateTime.Now + "' where  QRCODE = '" + unit.QRCODE + "'and SITEID = '" + unit.SITEID + "' ");

            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        //tambahan anas
        public void UpdateUnmatch(/*string nosin,*/ string qr, string Unmatch)
        {
            //MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQrUnit(nsoin, UserSession.SITE_ID_MAPPING);
            MPMWMSQRUNIT qrunit = QrUnitObject.ItemByMachineQr1(/*nosin,*/ qr, UserSession.SITE_ID_MAPPING);
            UpdateStatusMatching(qrunit, Unmatch);
        }

        //tambahan anas
        public void UpdateStatusMatching(MPMWMSQRUNIT unit, string unmatch)
        {
            BeginTransaction();
            try
            {
                WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();

                //db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '1' where  ENGINENO = '" + unit.ENGINENO + "'and SITEID = '" + unit.SITEID + "' and MODIFBY = '" + UserSession.USER_ID + "' and MODIFDATE = '" + DateTime.Now +"' ");
                db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '" + unmatch + "', MODIFBY = '" + UserSession.USER_ID + "', MODIFDATE = '" + DateTime.Now + "' where  QRCODE = '" + unit.QRCODE + "'and SITEID = '" + unit.SITEID + "' ");
                
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        //tambahan anas
        public void UpdateStsMatching(MPMWMSQRUNIT unit, string unmatch)
        {
            BeginTransaction();
            try
            {
                WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();

                //db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '1' where  ENGINENO = '" + unit.ENGINENO + "'and SITEID = '" + unit.SITEID + "' and MODIFBY = '" + UserSession.USER_ID + "' and MODIFDATE = '" + DateTime.Now +"' ");
                db.ExecuteCommand("update MPMWMSQRUNIT set STSMATCHING = '"+unmatch+ "', MODIFBY = '"+UserSession.USER_ID+ "', MODIFDATE = '" +DateTime.Now+"' where  ENGINENO = '" + unit.ENGINENO + "'and SITEID = '" + unit.SITEID + "' ");

                //unit.STSMATCHING = unmatch;
                //if (UserSession.USER_ID.Length > 10)
                //{
                //    unit.MODIFBY = UserSession.USER_ID.Substring(0, 10);
                //}
                //else
                //{
                //    unit.MODIFBY = UserSession.USER_ID;
                //}
                //unit.MODIFDATE = DateTime.Now;
                //db.SubmitChanges();
                //Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        #endregion

        #region cek shipping by nosin
        public string CekNosi(String nosin)
        {
            string SetShippListID = "";
            try
            {
                if (nosin == null || nosin == "")
                {
                    throw new MPMException("Nosin Belum Diinputkan");
                }

                MPMWMS_UNIT nosin_unit = UnitObject.ItemByCekNosin(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                MPMWMS_SHIPPING_LIST_LINE nosin_shipplist = UnitObject.ItemCekShipListByNosin(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                MPMWMS_SHIPPING_LIST_LINE noka_shipplist = UnitObject.ItemCekShipListByNoka(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);
                
                if (nosin_unit == null)
                {
                    nosin_unit = UnitObject.ItemByCekNoka(nosin, UserSession.DATAAREA_ID, UserSession.SITE_ID_MAPPING);

                    if (nosin_unit != null)
                    {
                        if (noka_shipplist == null)
                        {
                            throw new MPMException("Unit tersebut Belum mempunyai QrCode");
                        }

                        SetShippListID = noka_shipplist.SHIPPING_LIST_ID.ToString();
                    }
                    else if (nosin_unit == null)
                    {
                        throw new MPMException("Nosin tidak ditemukan");
                    }
                }
                if (nosin_unit != null)
                {
                    if (nosin_shipplist == null)
                    {
                        throw new MPMException("Unit tersebut Belum mempunyai QrCode");
                    }

                    SetShippListID = nosin_shipplist.SHIPPING_LIST_ID.ToString();
                }
                return SetShippListID;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        #endregion

    }
}
