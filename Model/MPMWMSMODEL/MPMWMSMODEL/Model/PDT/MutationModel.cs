﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.MutationLocation;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.xtsMuliaService;
using MPMLibrary.NET.Lib.Util;
using MPM365SERVICE.Service.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.MPMMuliaService;

namespace MPMWMSMODEL.Model.PDT
{
    public class MutationModel : PDTUnitModel
    {
        private MutationLocationObject _MutationLocationObject = new MutationLocationObject();

        // ==============================================================================

        public MutationModel()
            : base()
        {
            _MutationLocationObject.Context = (WmsUnitDataContext)Context;
        }

        public MutationLocationObject MutationLocationObject
        {
            get { return _MutationLocationObject; }
        }

        public void Validation(MPMWMS_UNIT unit, String dataarea_id, String site_id)
        {
            // check the dataarea and site
            if (!((unit.SITE_ID == site_id) && (unit.DATAAREA_ID == dataarea_id)))
            {
                throw new MPMException("Unit can't mutation in this data area / site id !!!!");
            }

            // can't move if not storing yet
            if (unit.STORING_DATE == null)
            {
                throw new MPMException("Unit must storing first!!!");
            }

            if (unit.STATUS == "S")
            {
                throw new MPMException("Unit already shipping!!!");
            }

            // check the location can't in receiving area
            MPMWMS_WAREHOUSE_LOCATION locationCheck = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
            if (locationCheck == null)
            {
                throw new MPMException("Can't find the current location for unit " + unit.MACHINE_ID);
            }
            else
            {
                if (locationCheck.LOCATION_TYPE.Equals("I"))
                {
                    throw new MPMException("Location in Intransit Vendor, can't move (dead end)");
                }
                else if (locationCheck.LOCATION_TYPE.Equals("E"))
                {
                    throw new MPMException("Location in Shipping Area, can't move (dead end)");
                }
                else if (locationCheck.LOCATION_TYPE.Equals("P"))
                {
                    throw new MPMException("Location in Picking Area, can't move (dead end)");
                }
            }
        }

        public String DoMutation(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id, bool is_print = true)
        {
            try 
            {                
                String location_result;
                String mutation_location;//tambahan anas create jurnal baru 5-12-2019
                BeginTransaction();

                ////proses scan by qr code
                //String QRCode = "";
                //String qrcode = "";
                //QRCode = machine_or_frame_id;
                //char[] spearator = { '/' };
                //String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                //qrcode = strlist[strlist.Length - 1];

                ////qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                //if (QRCode == "")
                //{
                //    throw new MPMException("Belum Scan QR Code");
                //}


                //MPMWMS_UNIT unit = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);
                ////end of proses scan by qr code

                //proses scan by nosin
                MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                //--> Modif By Anong 9 / 6 / 2019 Ket: Untuk QRCODE
                //end of proses scan by nosin    

                if (unit != null)
                    {
                        Validation(unit, dataarea_id, site_id);

                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;

                        unit.LOCATION_ID = GetLocationCell(dataarea_id, site_id, head_location_id);
                        unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                        MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("RT"))
                        {
                            unit.STATUS = "RT";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("I"))
                        {
                            throw new MPMException("Can't move to intransit location vendor");
                            /*
                            if (unit.IS_READYTOPICK.Equals("Y"))
                            {
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            }
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                             */
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }

                        UnitObject.Update(unit, "MTL", site_id_old, warehouse_id_old, location_id_old);

                        MPMWMS_MUTATION_LOCATION mutation = MutationLocationObject.Create();
                        mutation.MUTATION_LOCATION_ID = RunningNumberMutationLocation(dataarea_id, site_id);
                        mutation.DATAAREA_ID = unit.DATAAREA_ID;
                        mutation.SITE_ID = unit.SITE_ID;
                        mutation.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                        mutation.LOCATION_ID = unit.LOCATION_ID;
                        mutation.FRAME_ID = unit.FRAME_ID;
                        mutation.MACHINE_ID = unit.MACHINE_ID;
                        mutation.COLOR_TYPE = unit.COLOR_TYPE;
                        mutation.UNIT_TYPE = unit.UNIT_TYPE;
                        mutation.SITE_ID_OLD = site_id_old;
                        mutation.WAREHOUSE_ID_OLD = warehouse_id_old;
                        mutation.LOCATION_ID_OLD = location_id_old;
                        MutationLocationObject.Insert(mutation);

                        mutation_location = mutation.MUTATION_LOCATION_ID; //tambahan anas create jurnal baru 5-12-2019

                        // sent WS to axapta
                        /*String warehouse_to_ws = "";
                        warehouse_to_ws = unit.WAREHOUSE_ID;
                        MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                        if (location != null)
                        {
                            if (location.LOCATION_TYPE == "N")
                            {
                                warehouse_to_ws = site_id + "STR";
                            }
                        }*/

                        if (warehouse_id_old != unit.WAREHOUSE_ID)
                        {
                            try
                            {
                            string guid = Guid.NewGuid().ToString();
                                InventoryService invent = new InventoryService();
                                var resultAX = new ResultCommon();
                                MPMInventoryParm[] param;
                                param = new MPMInventoryParm[1];
                                param[0] = new MPMInventoryParm();
                                param[0].parmJournalName = "ITW";
                                param[0].parmItemId = unit.UNIT_TYPE;
                                param[0].parmColor = unit.COLOR_TYPE;
                                param[0].parmSiteId = site_id_old;
                                param[0].parmToSiteId = unit.SITE_ID;
                                param[0].parmWarehouseId = warehouse_id_old;
                                param[0].parmToWarehouseId = unit.WAREHOUSE_ID;
                                //param[0].parmLocationFrom = location_id_old;
                                //param[0].parmLocationTo = unit.LOCATION_ID;
                                param[0].parmLocationFrom = warehouse_id_old;
                                param[0].parmLocationTo = unit.WAREHOUSE_ID;
                                param[0].parmQty = 1;
                                param[0].parmCategoryCode = "U";
                                param[0].parmMainDealer = UserSession.MAINDEALER_ID;
                                param[0].parmTransDate = DateTime.Now;
                            param[0].parmWmsNumber = guid;//mutation.MUTATION_LOCATION_ID;
                                param[0].parmMachineNo = "";
                                param[0].parmChasisNo = "";
                                param[0].parmReferenceNumber = guid;
                            param[0].parmAutoPosting = NoYes.Yes;
                            resultAX = invent.createInventJournal(param, UserSession.DATAAREA_ID);
                                if (resultAX.Message != "" && resultAX.Success == 0)
                                {
                                    if(!resultAX.Message.Contains("CANNOT BE PICKED BECAUSE ONLY 0.00 IS/ARE AVAILABLE FROM THE INVENTORY FOR ITEM"))
                                    throw new Exception(resultAX.Message);
                                }
                                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebServiceTransfer();
                                //client.CreateInventoryTransfer(
                                //    ContextWS,
                                //    unit.UNIT_TYPE,
                                //    unit.COLOR_TYPE,
                                //    site_id_old,
                                //    warehouse_id_old,
                                //    unit.SITE_ID,
                                //    unit.WAREHOUSE_ID,
                                //    1,
                                //    "U",
                                //    UserSession.MAINDEALER_ID,
                                //    DateTime.Now,
                                //    mutation.MUTATION_LOCATION_ID);
                                //DisconnectWebServiceTransfer(client);
                            }
                            catch (Exception e)
                            {
                                throw new MPMException(e.Message);
                            }
                        }


                        //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        //String nameType = unit.UNIT_TYPE;
                        //if (viewProduct != null)
                        //{
                        //    nameType = viewProduct.nametype;
                        //}

                        MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        String nameType = unit.UNIT_TYPE;
                        if (viewProduct != null)
                        {
                            //nameType = viewProduct.nametype;
                            nameType = viewProduct.ITEMNAME;
                        }

                        if (is_print)
                            PrintStoring(
                                unit.MACHINE_ID,
                                unit.SITE_ID,
                                unit.LOCATION_ID,
                                unit.COLOR_TYPE, nameType,
                                unit.UNIT_YEAR,
                                unit.SPG_DATE.Value,
                                unit.UNIT_TYPE);

                        location_result = unit.LOCATION_ID;
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!!");
                    }

                    Commit();
                    
                    return mutation_location;//tambahan anas create jurnal baru 5-12-2019
                    //return location_result; //comment  anas


            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
        public String DoMutationv2(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id, bool is_print = true)
        {
            
            try
            {
                String mutation_location;//tambahan anas create jurnal baru 5-12-2019
                BeginTransaction();
                String location_result;

                //proses by qrcode
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                var Validasi = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                                where a.SITE_ID == site_id && a.QR_CODE == qrcode && a.LOCATION_ID.Contains(head_location_id)
                                select a).FirstOrDefault();

                var query = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.SITE_ID == site_id && a.QR_CODE == qrcode
                             select a).FirstOrDefault();

                ////proses by nosin
                //var Validasi = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                //                where a.SITE_ID == site_id && a.MACHINE_ID == machine_or_frame_id && a.LOCATION_ID.Contains(head_location_id)
                //                select a).FirstOrDefault(); // modif by anong 9 / 6 / 2019

                //var query = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                //             where a.SITE_ID == site_id && a.MACHINE_ID == machine_or_frame_id
                //             select a).FirstOrDefault();

                if (Validasi != null)
                {
                    throw new MPMException("Nomor Mesin "+ Validasi.MACHINE_ID.ToString() +" Sudah berada di lokasi "+ Validasi.LOCATION_ID.ToString());
                }
                
                string nomesin = query.WAREHOUSE_ID.ToString();
                string hasil = nomesin.Substring((nomesin.Length - 4), 4);
                if (query == null)
                {

                    throw new MPMException("NOMOR MESIN TIDAK TERSEDIA");

                }
                if (query.STATUS == "C" || hasil == "NRFS")
                {

                    throw new MPMException("GAGAL!! NOMOR MESIN BERLOKASI NRFS HARUS DIMUTASI MELALUI MENU BAST LKU");

                }

                else
                {
                    ////proses by nosin
                    //MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);

                    //proses byqrcode
                    MPMWMS_UNIT unit = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);

                    if (unit != null)
                    {
                        Validation(unit, dataarea_id, site_id);

                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;

                        unit.LOCATION_ID = GetLocationCell(dataarea_id, site_id, head_location_id);
                        unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                        MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("RT"))
                        {
                            unit.STATUS = "RT";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("I"))
                        {
                            throw new MPMException("Can't move to intransit location vendor");
                            /*
                            if (unit.IS_READYTOPICK.Equals("Y"))
                            {
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            }
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                             */
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }

                        UnitObject.Update(unit, "MTL", site_id_old, warehouse_id_old, location_id_old);

                        MPMWMS_MUTATION_LOCATION mutation = MutationLocationObject.Create();
                        mutation.MUTATION_LOCATION_ID = RunningNumberMutationLocation(dataarea_id, site_id);
                        mutation.DATAAREA_ID = unit.DATAAREA_ID;
                        mutation.SITE_ID = unit.SITE_ID;
                        mutation.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                        mutation.LOCATION_ID = unit.LOCATION_ID;
                        mutation.FRAME_ID = unit.FRAME_ID;
                        mutation.MACHINE_ID = unit.MACHINE_ID;
                        mutation.COLOR_TYPE = unit.COLOR_TYPE;
                        mutation.UNIT_TYPE = unit.UNIT_TYPE;
                        mutation.SITE_ID_OLD = site_id_old;
                        mutation.WAREHOUSE_ID_OLD = warehouse_id_old;
                        mutation.LOCATION_ID_OLD = location_id_old;
                        MutationLocationObject.Insert(mutation);

                        mutation_location = mutation.MUTATION_LOCATION_ID; //tambahan anas create jurnal baru 5-12-2019

                        // sent WS to axapta
                        /*String warehouse_to_ws = "";
                        warehouse_to_ws = unit.WAREHOUSE_ID;
                        MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                        if (location != null)
                        {
                            if (location.LOCATION_TYPE == "N")
                            {
                                warehouse_to_ws = site_id + "STR";
                            }
                        }*/

                        if (warehouse_id_old != unit.WAREHOUSE_ID)
                        {
                            try
                            {
                                string guid = Guid.NewGuid().ToString();
                                InventoryService invent = new InventoryService();
                                var resultAX = new ResultCommon();
                                MPMInventoryParm[] param;
                                param = new MPMInventoryParm[1];
                                param[0] = new MPMInventoryParm();
                                param[0].parmJournalName = "ITW";
                                param[0].parmItemId = unit.UNIT_TYPE;
                                param[0].parmColor = unit.COLOR_TYPE;
                                param[0].parmSiteId = site_id_old;
                                param[0].parmToSiteId = unit.SITE_ID;
                                param[0].parmWarehouseId = warehouse_id_old;
                                param[0].parmToWarehouseId = unit.WAREHOUSE_ID;
                                //param[0].parmLocationFrom = location_id_old;
                                //param[0].parmLocationTo = unit.LOCATION_ID;
                                param[0].parmLocationFrom = warehouse_id_old;
                                param[0].parmLocationTo = unit.WAREHOUSE_ID;
                                param[0].parmQty = 1;
                                param[0].parmCategoryCode = "U";
                                param[0].parmMainDealer = UserSession.MAINDEALER_ID;
                                param[0].parmTransDate = DateTime.Now;
                                param[0].parmWmsNumber = guid;//mutation.MUTATION_LOCATION_ID;
                                param[0].parmMachineNo = "";
                                param[0].parmChasisNo = "";
                                param[0].parmReferenceNumber = guid;//Guid.NewGuid().ToString();
                                param[0].parmAutoPosting = NoYes.Yes;
                                resultAX = invent.createInventJournal(param, UserSession.DATAAREA_ID);
                                if (resultAX.Message != "" && resultAX.Success == 0)
                                {
                                    throw new Exception(resultAX.Message);
                                }
                                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebServiceTransfer();
                                //client.CreateInventoryTransfer(
                                //    ContextWS,
                                //    unit.UNIT_TYPE,
                                //    unit.COLOR_TYPE,
                                //    site_id_old,
                                //    warehouse_id_old,
                                //    unit.SITE_ID,
                                //    unit.WAREHOUSE_ID,
                                //    1,
                                //    "U",
                                //    UserSession.MAINDEALER_ID,
                                //    DateTime.Now,
                                //    mutation.MUTATION_LOCATION_ID);
                                //DisconnectWebServiceTransfer(client);
                            }
                            catch (Exception e)
                            {
                                throw new MPMException(e.Message);
                            }
                        }


                        //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        //String nameType = unit.UNIT_TYPE;
                        //if (viewProduct != null)
                        //{
                        //    nameType = viewProduct.nametype;
                        //}
                        MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        String nameType = unit.UNIT_TYPE;
                        if (viewProduct != null)
                        {
                            //nameType = viewProduct.nametype;
                            nameType = viewProduct.ITEMNAME;
                        }

                        if (is_print)
                            PrintStoring(
                                unit.MACHINE_ID,
                                unit.SITE_ID,
                                unit.LOCATION_ID,
                                unit.COLOR_TYPE, nameType,
                                unit.UNIT_YEAR,
                                unit.SPG_DATE.Value,
                                unit.UNIT_TYPE);

                        location_result = unit.LOCATION_ID;
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!!");
                    }

                    Commit();

                    return mutation_location;//tambahan anas create jurnal baru 5-12-2019
                    //return location_result; //comment  anas
                }
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
        public String DoMutationvalidation(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id, String nama_mekanik, bool is_print = true)
        {
            try
            {
                String location_result;

                BeginTransaction();

                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }


                //var query = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                //             where a.SITE_ID == site_id && a.MACHINE_ID == machine_or_frame_id
                //             select a).FirstOrDefault(); --> Modif By Anong 9/6/2019 Ket: Untuk QRCODE 

                MPMWMS_UNIT query = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);

                

                if (query == null)
                {

                    throw new MPMException("NOMOR MESIN TIDAK TERSEDIA");

                }

                var querynya = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKULINEs 
                                join h in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKUHEADERs
                                on a.MPMSALLKUHEADER equals h.RECID

                             where a.NOMERMESIN.Replace(" ","") == query.MACHINE_ID //machine_or_frame_id  --> modif by anong
                             select a).FirstOrDefault();
                if (querynya == null)
                {
                    throw new MPMException("Nomor mesin yang dimasukkan tidak memiliki dokumen LKU");
                }
                string nomesin = query.WAREHOUSE_ID.ToString();
                string check2 = querynya.NOMERMESIN.ToString();
             
                string hasil = nomesin.Substring((nomesin.Length - 4), 4);
                if (query.STATUS != "C" || hasil != "NRFS")
                {

                    throw new MPMException("GAGAL!! NOMOR MESIN BERLOKASI SELAIN NRFS HARUS DIMUTASI MELALUI MENU MOVEMENT UNIT");

                }
               
                else
                {
                    //MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                    MPMWMS_UNIT unit = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);
                    if (unit != null)
                    {
                        //MPMLKUServiceCreateMovementBastLKURequest requst = new MPMLKUServiceCreateMovementBastLKURequest();
                        //xtsMuliaService.MPMLKUService client1 = ConnectWebServiceLKU();

                        //requst.nomermesin = unit.MACHINE_ID;
                        //requst.namamekanik = nama_mekanik;
                        //client1.CreateMovementBastLKU(requst);
                        //client1 = ConnectWebServiceLKU();
                        //DisconnectWebServiceBastLKU((MPMLKUServiceClient)client1);

                        InventoryService invent = new InventoryService();
                        var resultAX = new ResultCommon();
                        MPMInventoryParm[] param;
                        param = new MPMInventoryParm[1];
                        param[0] = new MPMInventoryParm();
                        param[0].parmMachineNo = unit.MACHINE_ID;
                        param[0].parmMechanicName = nama_mekanik;
                        resultAX = invent.createMovementBastLKU(param, UserSession.DATAAREA_ID);
                        if (resultAX.Message != "" && resultAX.Success == 0)
                        {
                            throw new Exception(resultAX.Message);
                        }
                        Validation(unit, dataarea_id, site_id);

                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;


                        unit.LOCATION_ID = GetLocationCell(dataarea_id, site_id, head_location_id);
                        unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                        MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("RT"))
                        {
                            unit.STATUS = "RT";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("I"))
                        {
                            throw new MPMException("Can't move to intransit location vendor");
                            /*
                            if (unit.IS_READYTOPICK.Equals("Y"))
                            {
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            }
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                             */
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }

                        UnitObject.Update(unit, "MTL", site_id_old, warehouse_id_old, location_id_old);

                        MPMWMS_MUTATION_LOCATION mutation = MutationLocationObject.Create();
                        mutation.MUTATION_LOCATION_ID = RunningNumberMutationLocation(dataarea_id, site_id);
                        mutation.DATAAREA_ID = unit.DATAAREA_ID;
                        mutation.SITE_ID = unit.SITE_ID;
                        mutation.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                        mutation.LOCATION_ID = unit.LOCATION_ID;
                        mutation.FRAME_ID = unit.FRAME_ID;
                        mutation.MACHINE_ID = unit.MACHINE_ID;
                        mutation.COLOR_TYPE = unit.COLOR_TYPE;
                        mutation.UNIT_TYPE = unit.UNIT_TYPE;
                        mutation.SITE_ID_OLD = site_id_old;
                        mutation.WAREHOUSE_ID_OLD = warehouse_id_old;
                        mutation.LOCATION_ID_OLD = location_id_old;
                        MutationLocationObject.Insert(mutation);

                        // sent WS to axapta
                        /*String warehouse_to_ws = "";
                        warehouse_to_ws = unit.WAREHOUSE_ID;
                        MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                        if (location != null)
                        {
                            if (location.LOCATION_TYPE == "N")
                            {
                                warehouse_to_ws = site_id + "STR";
                            }
                        }*/

                        if (warehouse_id_old != unit.WAREHOUSE_ID)
                        {
                            try
                            {
                                string guid = Guid.NewGuid().ToString();
                                MPMInventoryParm[] param1;
                                param1 = new MPMInventoryParm[1];
                                param1[0].parmItemId = unit.UNIT_TYPE;
                                param1[0].parmColor = unit.COLOR_TYPE;
                                param1[0].parmSiteId = site_id_old;
                                param1[0].parmWarehouseId = warehouse_id_old;
                                param1[0].parmToSiteId = unit.SITE_ID;
                                param1[0].parmToWarehouseId = unit.WAREHOUSE_ID;
                                param1[0].parmQty = 1;
                                param1[0].parmCategoryCode = "U";
                                param1[0].parmMainDealer = UserSession.MAINDEALER_ID;
                                param1[0].parmTransDate = DateTime.Now;
                                param1[0].parmWmsNumber = guid;//mutation.MUTATION_LOCATION_ID;
                                param1[0].parmJournalName = "ITW";
                                param1[0].parmReferenceNumber = guid;
                                param1[0].parmAutoPosting = NoYes.Yes; //additional 2021-04-07
                                resultAX = invent.createInventJournal(param1, UserSession.DATAAREA_ID);
                                //resultAX = invent.createInventTransferOrder(param1, UserSession.DATAAREA_ID);
                                if (resultAX.Message != "" && resultAX.Success == 0)
                                {
                                    throw new Exception(resultAX.Message);
                                }
                                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebServiceTransfer();
                                //client.CreateInventoryTransfer(
                                //    ContextWS,
                                //    unit.UNIT_TYPE,
                                //    unit.COLOR_TYPE,
                                //    site_id_old,
                                //    warehouse_id_old,
                                //    unit.SITE_ID,
                                //    unit.WAREHOUSE_ID,
                                //    1,
                                //    "U",
                                //    UserSession.MAINDEALER_ID,
                                //    DateTime.Now,
                                //    mutation.MUTATION_LOCATION_ID);
                                //DisconnectWebServiceTransfer(client);
                            }
                            catch (Exception e)
                            {
                                throw new MPMException(e.Message);
                            }
                        }

                        //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        //String nameType = unit.UNIT_TYPE;
                        //if (viewProduct != null)
                        //{
                        //    nameType = viewProduct.nametype;
                        //}
                        MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        String nameType = unit.UNIT_TYPE;
                        if (viewProduct != null)
                        {
                            //nameType = viewProduct.nametype;
                            nameType = viewProduct.ITEMNAME;
                        }

                        if (is_print)
                            PrintStoring(
                                unit.MACHINE_ID,
                                unit.SITE_ID,
                                unit.LOCATION_ID,
                                unit.COLOR_TYPE, nameType,
                                unit.UNIT_YEAR,
                                unit.SPG_DATE.Value,
                                unit.UNIT_TYPE);

                        location_result = unit.LOCATION_ID;
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!!");
                    }
                    
                  

                    Commit();

                    return location_result;
                }
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException("Proses pembuatan BAST gagal karena problem webservice BAST");
            }
        
        }

        //Tambahan Unit QC 
        public String DoMutationvalidationlku(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id, String nama_mekanik, bool is_print = true)
        {
            try
            {
                String location_result;

                BeginTransaction();
                var query = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.SITE_ID == site_id && a.MACHINE_ID == machine_or_frame_id
                             select a).FirstOrDefault();
                if (query == null)
                {

                    throw new MPMException("NOMOR MESIN TIDAK TERSEDIA");

                }

                var querynya = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKULINEs
                                join h in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKUHEADERs
                                on a.MPMSALLKUHEADER equals h.RECID

                                where a.NOMERMESIN.Replace(" ", "") == machine_or_frame_id
                                select a).FirstOrDefault();
                if (querynya == null)
                {
                    throw new MPMException("Nomor mesin yang dimasukkan tidak memiliki dokumen LKU");
                }
                string nomesin = query.WAREHOUSE_ID.ToString();
                string check2 = querynya.NOMERMESIN.ToString();

                string hasil = nomesin.Substring((nomesin.Length - 4), 4);
                if (query.STATUS != "C" || hasil != "NRFS")
                {

                    throw new MPMException("GAGAL!! NOMOR MESIN BERLOKASI SELAIN NRFS HARUS DIMUTASI MELALUI MENU MOVEMENT UNIT");

                }

                else
                {
                    MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                    if (unit != null)
                    {
                        //MPMLKUServiceCreateMovementBastLKURequest requst = new MPMLKUServiceCreateMovementBastLKURequest();
                        //xtsMuliaService.MPMLKUService client1 = ConnectWebServiceLKU();

                        //requst.nomermesin = machine_or_frame_id;
                        //requst.namamekanik = nama_mekanik;
                        //client1.CreateMovementBastLKU(requst);
                        //client1 = ConnectWebServiceLKU();
                        //DisconnectWebServiceBastLKU((MPMLKUServiceClient)client1);
                        InventoryService invent = new InventoryService();
                        var resultAX = new ResultCommon();
                        MPMInventoryParm[] param;
                        param = new MPMInventoryParm[1];
                        param[0] = new MPMInventoryParm();
                        param[0].parmMachineNo = machine_or_frame_id;
                        param[0].parmMechanicName = nama_mekanik;
                        resultAX = invent.createMovementBastLKU(param, UserSession.DATAAREA_ID);
                        if (resultAX.Message != "" && resultAX.Success == 0)
                        {
                            throw new Exception(resultAX.Message);
                        }
                        Validation(unit, dataarea_id, site_id);

                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;


                        unit.LOCATION_ID = GetLocationCell(dataarea_id, site_id, head_location_id);
                        unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                        MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("RT"))
                        {
                            unit.STATUS = "RT";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("I"))
                        {
                            throw new MPMException("Can't move to intransit location vendor");
                            /*
                            if (unit.IS_READYTOPICK.Equals("Y"))
                            {
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            }
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                             */
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }

                        UnitObject.Update(unit, "MTL", site_id_old, warehouse_id_old, location_id_old);

                        MPMWMS_MUTATION_LOCATION mutation = MutationLocationObject.Create();
                        mutation.MUTATION_LOCATION_ID = RunningNumberMutationLocation(dataarea_id, site_id);
                        mutation.DATAAREA_ID = unit.DATAAREA_ID;
                        mutation.SITE_ID = unit.SITE_ID;
                        mutation.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                        mutation.LOCATION_ID = unit.LOCATION_ID;
                        mutation.FRAME_ID = unit.FRAME_ID;
                        mutation.MACHINE_ID = unit.MACHINE_ID;
                        mutation.COLOR_TYPE = unit.COLOR_TYPE;
                        mutation.UNIT_TYPE = unit.UNIT_TYPE;
                        mutation.SITE_ID_OLD = site_id_old;
                        mutation.WAREHOUSE_ID_OLD = warehouse_id_old;
                        mutation.LOCATION_ID_OLD = location_id_old;
                        MutationLocationObject.Insert(mutation);

                        // sent WS to axapta
                        /*String warehouse_to_ws = "";
                        warehouse_to_ws = unit.WAREHOUSE_ID;
                        MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                        if (location != null)
                        {
                            if (location.LOCATION_TYPE == "N")
                            {
                                warehouse_to_ws = site_id + "STR";
                            }
                        }*/

                        if (warehouse_id_old != unit.WAREHOUSE_ID)
                        {
                            try
                            {
                                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebServiceTransfer();
                                //client.CreateInventoryTransfer(
                                //    ContextWS,
                                //    unit.UNIT_TYPE,
                                //    unit.COLOR_TYPE,
                                //    site_id_old,
                                //    warehouse_id_old,
                                //    unit.SITE_ID,
                                //    unit.WAREHOUSE_ID,
                                //    1,
                                //    "U",
                                //    UserSession.MAINDEALER_ID,
                                //    DateTime.Now,
                                //    mutation.MUTATION_LOCATION_ID);
                                //DisconnectWebServiceTransfer(client);
                                string guid = Guid.NewGuid().ToString();
                                MPMInventoryParm[] param1;
                                param1 = new MPMInventoryParm[1];
                                param1[0] = new MPMInventoryParm();
                                param1[0].parmItemId = unit.UNIT_TYPE;
                                param1[0].parmColor = unit.COLOR_TYPE;
                                param1[0].parmSiteId = site_id_old;
                                param1[0].parmWarehouseId = warehouse_id_old;
                                param1[0].parmToSiteId = unit.SITE_ID;
                                param1[0].parmToWarehouseId = unit.WAREHOUSE_ID;
                                param1[0].parmLocationFrom = warehouse_id_old;
                                param1[0].parmLocationTo = unit.WAREHOUSE_ID;
                                param1[0].parmQty = 1;
                                param1[0].parmCategoryCode = "U";
                                param1[0].parmMainDealer = UserSession.MAINDEALER_ID;
                                param1[0].parmTransDate = DateTime.Now;
                                param1[0].parmWmsNumber = guid;//mutation.MUTATION_LOCATION_ID;
                                param1[0].parmMachineNo = "";
                                param1[0].parmChasisNo = "";
                                param1[0].parmJournalName = "ITW";
                                param1[0].parmReferenceNumber = guid;
                                param1[0].parmAutoPosting = NoYes.Yes; //additional 2021-04-07
                                //resultAX = invent.createInventTransferOrder(param1, UserSession.DATAAREA_ID);
                                resultAX = invent.createInventJournal(param1, UserSession.DATAAREA_ID);
                                if (resultAX.Message != "" && resultAX.Success == 0)
                                {
                                    throw new Exception(resultAX.Message);
                                }
                            }
                            catch (Exception e)
                            {
                                throw new MPMException(e.Message);
                            }
                        }

                        //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        //String nameType = unit.UNIT_TYPE;
                        //if (viewProduct != null)
                        //{
                        //    nameType = viewProduct.nametype;
                        //}
                        MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        String nameType = unit.UNIT_TYPE;
                        if (viewProduct != null)
                        {
                            //nameType = viewProduct.nametype;
                            nameType = viewProduct.ITEMNAME;
                        }

                        if (is_print)
                            PrintStoring(
                                unit.MACHINE_ID,
                                unit.SITE_ID,
                                unit.LOCATION_ID,
                                unit.COLOR_TYPE, nameType,
                                unit.UNIT_YEAR,
                                unit.SPG_DATE.Value,
                                unit.UNIT_TYPE);

                        location_result = unit.LOCATION_ID;
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!!");
                    }



                    Commit();

                    return location_result;
                }
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }

        }

        public String DoMutationvalidationNonLKU(String machine_or_frame_id, String dataarea_id, String site_id, String head_location_id, String nama_mekanik, bool is_print = true)
        {
            try
            {
                String location_result;

                BeginTransaction();
                var query = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMWMS_UNITs
                             where a.SITE_ID == site_id && a.MACHINE_ID == machine_or_frame_id
                             select a).FirstOrDefault();
                if (query == null)
                {

                    throw new MPMException("NOMOR MESIN TIDAK TERSEDIA");

                }

                //var querynya = (from a in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKULINEs
                //                join h in ((MPMWMSMODEL.Module.WmsUnitDataContext)Context).MPMSALLKUHEADERs
                //                on a.MPMSALLKUHEADER equals h.RECID

                //                where a.NOMERMESIN.Replace(" ", "") == machine_or_frame_id
                //                select a).FirstOrDefault();
                //if (querynya == null)
                //{
                //    throw new MPMException("Nomor mesin yang dimasukkan tidak memiliki dokumen LKU");
                //}
                string nomesin = query.WAREHOUSE_ID.ToString();
                string check2 = query.MACHINE_ID.ToString();

                string hasil = nomesin.Substring((nomesin.Length - 4), 4);
                if (query.STATUS != "C" || hasil != "NRFS")
                {

                    throw new MPMException("GAGAL!! NOMOR MESIN BERLOKASI SELAIN NRFS HARUS DIMUTASI MELALUI MENU MOVEMENT UNIT");

                }

                else
                {
                    MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                    if (unit != null)
                    {
                        //MPMLKUServiceCreateMovementBastLKURequest requst = new MPMLKUServiceCreateMovementBastLKURequest();
                        //xtsMuliaService.MPMLKUService client1 = ConnectWebServiceLKU();

                        //requst.nomermesin = machine_or_frame_id;
                        //requst.namamekanik = nama_mekanik;
                        //client1.CreateMovementBastLKU(requst);
                        //client1 = ConnectWebServiceLKU();
                        //DisconnectWebServiceBastLKU((MPMLKUServiceClient)client1);

                        InventoryService invent = new InventoryService();
                        var resultAX = new ResultCommon();
                        MPMInventoryParm[] param;
                        param = new MPMInventoryParm[1];
                        param[0] = new MPMInventoryParm();
                        param[0].parmMachineNo = machine_or_frame_id;
                        param[0].parmMechanicName = nama_mekanik;
                        resultAX = invent.createMovementBastLKU(param, UserSession.DATAAREA_ID);
                        if (resultAX.Message != "" && resultAX.Success == 0)
                        {
                            throw new Exception(resultAX.Message);
                        }
                        Validation(unit, dataarea_id, site_id);

                        String site_id_old = unit.SITE_ID;
                        String warehouse_id_old = unit.WAREHOUSE_ID;
                        String location_id_old = unit.LOCATION_ID;


                        unit.LOCATION_ID = GetLocationCell(dataarea_id, site_id, head_location_id);
                        unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                        // update warehouse_location actual stock in old location
                        UpdateLocationStock(dataarea_id, site_id_old, warehouse_id_old, location_id_old, -1);

                        // update warehouse_location actual stock in new location
                        UpdateLocationStock(dataarea_id, site_id, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                        // find new location status, if is NRFS or BLACKLIST then STATUS unit must set C (Claim)
                        MPMWMS_WAREHOUSE_LOCATION locationNew = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                        if (locationNew.LOCATION_TYPE.Equals("N") || locationNew.LOCATION_TYPE.Equals("B"))
                        {
                            unit.STATUS = "C";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("RT"))
                        {
                            unit.STATUS = "RT";
                            unit.IS_RFS = "N";
                        }
                        else if (locationNew.LOCATION_TYPE.Equals("I"))
                        {
                            throw new MPMException("Can't move to intransit location vendor");
                            /*
                            if (unit.IS_READYTOPICK.Equals("Y"))
                            {
                                throw new MPMException("Can't move to intransit location if unit in ready to pick");
                            }
                            unit.STATUS = "U";
                            unit.IS_RFS = "N";
                             */
                        }
                        else
                        {
                            unit.STATUS = "RFS";
                            unit.IS_RFS = "Y";
                        }

                        UnitObject.Update(unit, "MTL", site_id_old, warehouse_id_old, location_id_old);

                        MPMWMS_MUTATION_LOCATION mutation = MutationLocationObject.Create();
                        mutation.MUTATION_LOCATION_ID = RunningNumberMutationLocation(dataarea_id, site_id);
                        mutation.DATAAREA_ID = unit.DATAAREA_ID;
                        mutation.SITE_ID = unit.SITE_ID;
                        mutation.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                        mutation.LOCATION_ID = unit.LOCATION_ID;
                        mutation.FRAME_ID = unit.FRAME_ID;
                        mutation.MACHINE_ID = unit.MACHINE_ID;
                        mutation.COLOR_TYPE = unit.COLOR_TYPE;
                        mutation.UNIT_TYPE = unit.UNIT_TYPE;
                        mutation.SITE_ID_OLD = site_id_old;
                        mutation.WAREHOUSE_ID_OLD = warehouse_id_old;
                        mutation.LOCATION_ID_OLD = location_id_old;
                        MutationLocationObject.Insert(mutation);

                        // sent WS to axapta
                        /*String warehouse_to_ws = "";
                        warehouse_to_ws = unit.WAREHOUSE_ID;
                        MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                        if (location != null)
                        {
                            if (location.LOCATION_TYPE == "N")
                            {
                                warehouse_to_ws = site_id + "STR";
                            }
                        }*/

                        if (warehouse_id_old != unit.WAREHOUSE_ID)
                        {
                            try
                            {
                                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebServiceTransfer();
                                //client.CreateInventoryTransfer(
                                //    ContextWS,
                                //    unit.UNIT_TYPE,
                                //    unit.COLOR_TYPE,
                                //    site_id_old,
                                //    warehouse_id_old,
                                //    unit.SITE_ID,
                                //    unit.WAREHOUSE_ID,
                                //    1,
                                //    "U",
                                //    UserSession.MAINDEALER_ID,
                                //    DateTime.Now,
                                //    mutation.MUTATION_LOCATION_ID);
                                //DisconnectWebServiceTransfer(client);
                                string guid = Guid.NewGuid().ToString();
                                MPMInventoryParm[] param1;
                                param1 = new MPMInventoryParm[1];
                                param1[0] = new MPMInventoryParm();
                                param1[0].parmItemId = unit.UNIT_TYPE;
                                param1[0].parmColor = unit.COLOR_TYPE;
                                param1[0].parmSiteId = site_id_old;
                                param1[0].parmWarehouseId = warehouse_id_old;
                                param1[0].parmToSiteId = unit.SITE_ID;
                                param1[0].parmToWarehouseId = unit.WAREHOUSE_ID;
                                param1[0].parmLocationFrom = warehouse_id_old;
                                param1[0].parmLocationTo = unit.WAREHOUSE_ID;
                                param1[0].parmQty = 1;
                                param1[0].parmCategoryCode = "U";
                                param1[0].parmMainDealer = UserSession.MAINDEALER_ID;
                                param1[0].parmTransDate = DateTime.Now;
                                param1[0].parmWmsNumber = guid;//mutation.MUTATION_LOCATION_ID;
                                param1[0].parmMachineNo = "";
                                param1[0].parmChasisNo = "";
                                param1[0].parmJournalName = "ITW";
                                param1[0].parmReferenceNumber = guid;
                                param1[0].parmAutoPosting = NoYes.Yes; //additional 2021-04-07
                                //resultAX = invent.createInventTransferOrder(param1, UserSession.DATAAREA_ID);
                                resultAX = invent.createInventJournal(param1, UserSession.DATAAREA_ID);
                                if (resultAX.Message != "" && resultAX.Success == 0)
                                {
                                    throw new Exception(resultAX.Message);
                                }
                            }
                            catch (Exception e)
                            {
                                throw new MPMException(e.Message);
                            }
                        }

                        //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        //String nameType = unit.UNIT_TYPE;
                        //if (viewProduct != null)
                        //{
                        //    nameType = viewProduct.nametype;
                        //}
                        MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                        String nameType = unit.UNIT_TYPE;
                        if (viewProduct != null)
                        {
                            //nameType = viewProduct.nametype;
                            nameType = viewProduct.ITEMNAME;
                        }

                        if (is_print)
                            PrintStoring(
                                unit.MACHINE_ID,
                                unit.SITE_ID,
                                unit.LOCATION_ID,
                                unit.COLOR_TYPE, nameType,
                                unit.UNIT_YEAR,
                                unit.SPG_DATE.Value,
                                unit.UNIT_TYPE);

                        location_result = unit.LOCATION_ID;
                    }
                    else
                    {
                        throw new MPMException("Unit not found !!!!");
                    }



                    Commit();

                    return location_result;
                }
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }

        }
    }
}
