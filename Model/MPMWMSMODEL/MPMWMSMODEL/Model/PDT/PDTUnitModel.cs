﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Print;
using MPMLibrary.NET.Lib.Util;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.HistoryUnit;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.Unit;
using MPMWMSMODEL.xtsMuliaService;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MPMWMSMODEL.Model
{
    public class PDTUnitModel: BaseModel
    {
        protected String ServiceGroup = "xtsMuliaService";

        protected CallContext ContextWS = new CallContext();

        public PDTUnitModel()
            : base()
        {
        }

        public void PrintStoring(
            String machine_id, 
            String site_id, 
            String location_id, 
            String color_type, String unit_name, 
            String unit_year,
            DateTime spg_date,
            String unit_type
            
            )
        {
            try
            {
                if (UserSession.TAG_1 != null && UserSession.TAG_1 != "")
                {
                    DateTime print_date = MPMDateUtil.DateTime;

                    String new_location_id = "";
                    if (location_id == "RCVAREA" || location_id == "STORING")
                    {
                        new_location_id = location_id;
                    }
                    else if (location_id.Length >= 6)
                    {
                        String cell = location_id.Substring(location_id.Length - 2);
                        String baris =
                            location_id.Substring(location_id.Length - 4, 2);
                        String kolom =
                            location_id.Substring(location_id.Length - 5, 1);
                        new_location_id =
                                location_id.Substring(0, location_id.Length - (cell.Length +
                                                                               baris.Length +
                                                                               kolom.Length)) +
                                "." + kolom + "." + baris + "." + cell;
                    }
                    else
                    {
                        new_location_id = location_id;
                    }

                    String data =
                        "^XA^PR3^XZ\r\n" +
                        "^XA\r\n" +
                        "^FO450,0^A0N,60,60^FD" + new_location_id + "^FS\r\n" +
                        "^FO465,60^A0N,30,32^FD" + unit_type + "-" + site_id + "^FS\r\n" +
                        "^FO180,0^A0N,30,32^FD" + machine_id +  "^FS\r\n" +
                        "^FO180,30^A0N,30,32^FD" + color_type + "(" + unit_year + ")" + "^FS\r\n" +
                        "^FO180,60^A0N,30,32^FD" + unit_name + "^FS\r\n" +
                        "^FO180,90^A0N,30,32^FD" + spg_date.ToString("dd-MM-yyyy") + "^FS\r\n" +
                        "^FO405,90^A0N,20,22^FD" + print_date.ToString("dd-MM-yyyy") + "^FS\r\n" +
                        "^FO220,120^BY2,1.0^BCN,50,Y,N,N^FD" + machine_id +
                        "^FS\r\n" +
                        "^PQ1\r\n" +
                        "^XZ";

                    if (UserSession.TAG_2 == "2")
                    {
                        data =
                            "^XA^PR3^XZ\r\n" +
                            "^XA\r\n" +
                            "^FO450,1030^A0N,60,60^FD" + new_location_id + "^FS\r\n" +
                            "^FO465,1060^A0N,30,32^FD" + unit_type + "-" + site_id + "^FS\r\n" +
                            "^FO180,100^A0N,30,32^FD" + machine_id +  "^FS\r\n" +
                            "^FO180,1030^A0N,30,32^FD" + color_type + "(" + unit_year + ")" + "^FS\r\n" +
                            "^FO180,1060^A0N,30,32^FD" + unit_name + "^FS\r\n" +
                            "^FO180,1090^A0N,30,32^FD" + spg_date.ToString("dd-MM-yyyy") + "^FS\r\n" +
                            "^FO405,1090^A0N,20,22^FD" + print_date.ToString("dd-MM-yyyy") + "^FS\r\n" +
                            "^FO220,1120^BY2,1.0^BCN,50,Y,N,N^FD" + machine_id +
                            "^FS\r\n" +
                            "^PQ1\r\n" +
                            "^XZ";
                    }

                    MPMNetworkPrint print = new MPMNetworkPrint(UserSession.TAG_1, 6101, data);
                    print = null;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        public void PrintPicking(String machine_id, String site_id, String color_name, String type_name, 
            String dealer_code, String dealer_name, String sopir, String expedisi, String nopol)
        {
            try
            {
                if (UserSession.TAG_1 != null && UserSession.TAG_1 != "")
                {
                    DateTime print_date = MPMDateUtil.DateTime;

                    String data =
                        "^XA^PR3^XZ\r\n" +
                        "^XA\r\n" +
                        "^FO200,15^A0N,30,35^FD" + machine_id + " (" + dealer_code + ")^FS\r\n" +
                        "^FO200,50^A0N,30,35^FD" + dealer_name + "^FS\r\n" +
                        "^FO200,80^A0N,30,35^FD" + type_name + "^FS\r\n" +
                        "^FO200,110^A0N,30,35^FD" + color_name + "^FS\r\n" +
                        "^FO200,140^A0N,30,35^FD" + sopir + " - " + expedisi + "^FS\r\n" +
                        "^FO200,170^A0N,20,22^FD" + print_date.ToString("dd-MM-yyyy hh:mm:ss") + "^FS\r\n" +
                        "^FO550,170^A0N,30,35^FD" + site_id + "^FS\r\n" +
                        "^FO480,140^A0N,30,35^FD" + nopol + "^FS\r\n" +
                        "^PQ1\r\n" +
                        "^XZ";

                    if (UserSession.TAG_2 == "2")
                    {
                        data =
                            "^XA^PR3^XZ\r\n" +
                            "^XA\r\n" +
                            "^FO200,1015^A0N,30,35^FD" + machine_id + " (" + dealer_code + ")^FS\r\n" +
                            "^FO200,1050^A0N,30,35^FD" + dealer_name + "^FS\r\n" +
                            "^FO200,1080^A0N,30,35^FD" + type_name + "^FS\r\n" +
                            "^FO200,1110^A0N,30,35^FD" + color_name + "^FS\r\n" +
                            "^FO200,1140^A0N,30,35^FD" + sopir + " - " + expedisi + "^FS\r\n" +
                            "^FO200,1170^A0N,20,22^FD" + print_date.ToString("dd-MM-yyyy hh:mm:ss") + "^FS\r\n" +
                            "^FO550,1170^A0N,30,35^FD" + site_id + "^FS\r\n" +
                           
                            "^PQ1\r\n" +
                            "^XZ";
                    }

                    MPMNetworkPrint print = new MPMNetworkPrint(UserSession.TAG_1, 6101, data);
                    print = null;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        /** WEB SERVICE **/
        protected xtsMuliaService.xtsTransferInventServiceClient ConnectWebService()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpXtsMuliaService, LogonASUser);

                xtsMuliaService.xtsTransferInventServiceClient client = new xtsMuliaService.xtsTransferInventServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebService(xtsMuliaService.xtsTransferInventServiceClient client)
        {
            client.Close();
            client = null;
        }

        protected xtsMuliaService.xtsstoringServiceClient ConnectWebServiceStoring()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpXtsMuliaService, LogonASUser);

                xtsMuliaService.xtsstoringServiceClient client = new xtsMuliaService.xtsstoringServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebServiceStoring(xtsMuliaService.xtsstoringServiceClient client)
        {
            client.Close();
            client = null;
        }

        protected xtsMuliaService.MPMLKUServiceClient ConnectWebServiceLKU()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpXtsMuliaService, LogonASUser);

                xtsMuliaService.MPMLKUServiceClient client = new xtsMuliaService.MPMLKUServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }


        protected xtsMuliaService.xtsTransferInventServiceClient ConnectWebServiceTransfer()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpXtsMuliaService, LogonASUser);

                xtsMuliaService.xtsTransferInventServiceClient client = new xtsMuliaService.xtsTransferInventServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebServiceBastLKU (xtsMuliaService.MPMLKUServiceClient client)
        {
            client.Close();
            client = null;
        }

        protected void DisconnectWebServiceTransfer(xtsMuliaService.xtsTransferInventServiceClient client)
        {
            client.Close();
            client = null;
        }
        /** END OF WEB SERVICE **/

        protected String GetLocationCell(String dataarea_id, String site_id, String head_location_id)
        {
            IQueryable<String> search = from location in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                                        where (location.DATAAREA_ID == dataarea_id) &&
                                        (location.SITE_ID == site_id) &&
                                        (SqlMethods.Like(location.LOCATION_ID, head_location_id + "%")) &&
                                        (location.STOCK_MAX - (location.STOCK_ACTUAL + location.STOCK_BOOKING) > 0)
                                        orderby location.LOCATION_ID ascending
                                        select location.LOCATION_ID;

            if (search.FirstOrDefault() == null)
            {
                throw new MPMException("Location Id not found !!!!");
            }

            String location_id = search.FirstOrDefault().ToString();
            return location_id;
        }        
    }
}
