﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.HistoryUnit;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class ReceivingModel : PDTUnitModel
    {
        public ReceivingModel()
            : base()
        {
        }

        public void DoReceiving(String dataarea_id, String machine_or_frame_id)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                if (unit != null)
                {
                    if (unit.STATUS.Equals("C"))
                    {
                        throw new MPMException("Unit Claim");
                    }
                    else if (unit.STATUS.Equals("RB"))
                    {
                        throw new MPMException("Unit Robbing");
                    }
                    else if (unit.STATUS.Equals("PDI"))
                    {
                        throw new MPMException("Unit Pre Delivery");
                    }
                    else if (unit.STATUS.Equals("CO"))
                    {
                        throw new MPMException("Unit Claim Others");
                    }
                    else if (unit.STATUS.Equals("S"))
                    {
                        throw new MPMException("Unit Sold");
                    }
                    else if (unit.STATUS.Equals("CS"))
                    {
                        throw new MPMException("Unit Cancel SPG");
                    }
                    else if (unit.STATUS.Equals("U") || unit.STATUS.Equals("RFS"))
                    {
                        unit.IS_RFS = "Y";
                        unit.IS_READYTOPICK = "N";
                        unit.STATUS = "RFS";
                        unit.STORING_DATE = DateTime.Now;
                        UnitObject.Update(unit, "RCV");
                        SubmitChanges();
                    }
                    else
                    {
                        throw new MPMException("Failed to found unit !!!");
                    }
                }
                else
                {
                    throw new MPMException("Unit not found !!!!");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
