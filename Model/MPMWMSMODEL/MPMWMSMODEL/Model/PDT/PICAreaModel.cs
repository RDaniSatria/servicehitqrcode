﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.PICArea;
using MPMWMSMODEL.Table.PICArea;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class PICAreaModel : BaseModel
    {
        public PICAreaObject _PICAreaObject = new PICAreaObject();

        public PICAreaModel()
            : base()
        {
            _PICAreaObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Insert(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            MPMWMS_MAP_PIC_WAREHOUSE dataNew = new MPMWMS_MAP_PIC_WAREHOUSE();

            BeginTransaction();
            try
            {
                var row = _PICAreaObject.ItemByInitial(item.PIC, item.NPK, item.INITIALWAREHOUSEID, item.SITEID);

                if (row == null)
                {
                    dataNew.PIC = item.PIC;
                    dataNew.NPK = item.NPK;
                    dataNew.SITEID = item.SITEID;
                    dataNew.INITIALWAREHOUSEID = item.INITIALWAREHOUSEID;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                validasiData(dataNew);

                _PICAreaObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_MAP_PIC_WAREHOUSE item, String InitialOld)
        {
            BeginTransaction();
            try
            {
                var row = _PICAreaObject.Item(item.PIC, item.NPK, item.SITEID, InitialOld);

                if (row != null)
                {
                    row.INITIALWAREHOUSEID = item.INITIALWAREHOUSEID;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }

                validasiData(item);

                _PICAreaObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            BeginTransaction();
            try
            {
                var row = _PICAreaObject.Item(item.PIC, item.NPK, item.SITEID, item.INITIALWAREHOUSEID);

                if (row != null)
                {
                    _PICAreaObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        private void validasiData(MPMWMS_MAP_PIC_WAREHOUSE item)
        {
            try
            {
                if (!_PICAreaObject.validasiDataUser(item))
                {
                    throw new MPMException("NPK not registered on system");
                }

                if (!_PICAreaObject.validasiDataLocation(item))
                {
                    throw new MPMException("Warehouse area not registered at AX");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<PerformReportRecord> Report_PerfomancePIC(string _reportType, DateTime _beginDate, DateTime _endDate)
        {
            return _PICAreaObject.Report_PerfomancePIC(_reportType, _beginDate, _endDate).ToList();
        }

        public List<GrafikData_Record> GrafikMonitoringSO(DateTime _soDate, String _siteId)
        {
            return _PICAreaObject.dataGrafikMonitoringSO(_soDate, _siteId);
        }

        public List<MonitoringTargetActual_Record> GrafikMonitoringTargetActual(DateTime _soDate)
        {
            return _PICAreaObject.dataMonitoringTargetActualPIC(_soDate);
        }

        public List<CancelTrolley_Record> getListTransporterTrolley(String _siteId)
        {
            return _PICAreaObject.getTransporterTrolleyCanCanceled(_siteId);
        }
        
        public void prosesCancelTrolley(String _siteId)
        {
            _PICAreaObject.prosesCancelTrolley(_siteId);
        }
    }
}
