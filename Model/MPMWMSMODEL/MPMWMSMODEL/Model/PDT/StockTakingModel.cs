﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.StockTakingUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Util;

namespace MPMWMSMODEL.Model.PDT
{
    public class StockTakingModel : PDTUnitModel
    {
        public class STOCK_TAKING_RECORD
        {
            public String STOCK_TAKING_ID { get; set; }

            public STOCK_TAKING_RECORD(String id)
            {
                STOCK_TAKING_ID = id;
            }
        }

        private StockTakingUnitObject _StockTakingUnitObject = new StockTakingUnitObject();
        private StockTakingUnitReplicateObject _StockTakingUnitReplicateObject = new StockTakingUnitReplicateObject();
        private StockTakingUnitSamplingObject _StockTakingUnitSamplingObject = new StockTakingUnitSamplingObject();

        public StockTakingModel()
            : base()
        {
            _StockTakingUnitObject.Context = (WmsUnitDataContext)Context;
            _StockTakingUnitSamplingObject.Context = (WmsUnitDataContext)Context;
            _StockTakingUnitReplicateObject.Context = (WmsUnitDataContext)Context;
        }

        public StockTakingUnitObject StockTakingUnitObject
        {
            get { return _StockTakingUnitObject; }
        }

        public StockTakingUnitSamplingObject StockTakingUnitSamplingObject
        {
            get { return _StockTakingUnitSamplingObject; }
        }

        public StockTakingUnitReplicateObject StockTakingUnitReplicateObject
        {
            get { return _StockTakingUnitReplicateObject; }
        }

        public void AddSampling(String stock_taking_id, String warehouse_id, String unit_type, String color_type)
        {
            try
            {
                if (warehouse_id == "") warehouse_id = "ALL";
                if (unit_type == "") unit_type = "ALL";
                if (color_type == "") color_type = "ALL";

                MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA item = StockTakingUnitSamplingObject.Create();
                item.STOCK_TAKING_ID = stock_taking_id;
                item.STOCK_TAKING_DATE = MPMDateUtil.DateTime;
                item.DATAAREA_ID = UserSession.DATAAREA_ID;
                item.SITE_ID = UserSession.SITE_ID_MAPPING;
                item.WAREHOUSE_ID = warehouse_id;
                item.UNIT_TYPE = unit_type;
                item.COLOR_TYPE = color_type;
                item.POST = "0";
                StockTakingUnitSamplingObject.Insert(item);
                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void RemoveSampling(String stock_taking_id, String warehouse_id, String unit_type, String color_type)
        {
            try
            {
                MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA item = StockTakingUnitSamplingObject.Item(
                    stock_taking_id, warehouse_id, unit_type, color_type);

                if (item == null)
                {
                    throw new MPMException("Item not found !!!");
                }
                else
                {
                    StockTakingUnitSamplingObject.Delete(item);
                    SubmitChanges();
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void PostSampling(String stock_taking_id)
        {
            try
            {
                BeginTransaction();
                List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> list = StockTakingUnitSamplingObject.List(
                    stock_taking_id);

                foreach (var a in list)
                {
                    a.POST = "1";
                }

                // replicate all UNIT
                ((WmsUnitDataContext)Context).MPM_SP_STOCK_TAKING_UNIT_REPLICATE(stock_taking_id, UserSession.NPK);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Finish(String stock_taking_id)
        {
            try
            {
                bool value = IsPost(stock_taking_id);
                if (!value)
                {
                    throw new MPMException("Data must be posting first !!!");
                }

                BeginTransaction();
                List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> list = StockTakingUnitSamplingObject.List(
                    stock_taking_id);

                foreach (var a in list)
                {
                    a.POST = "2";
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, String stock_taking_id, String machine_id, String frame_id, String location_id)
        {
            try
            {
                // delete
                MPMWMS_STOCK_TAKING_UNIT deletedItem = StockTakingUnitObject.Item(stock_taking_id, machine_id, frame_id, location_id);
                StockTakingUnitObject.Delete(deletedItem);

                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<STOCK_TAKING_RECORD> ListStockTakingPDT()
        {
            try
            {
                IQueryable<STOCK_TAKING_RECORD> list =
                    from a in
                        (
                            from c in ((WmsUnitDataContext)Context).MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                            where c.POST != "2"
                            group c by new { c.STOCK_TAKING_ID, c.DATAAREA_ID, c.SITE_ID } into g_c
                            select new
                            {
                                STOCK_TAKING_ID = g_c.Key.STOCK_TAKING_ID,
                                DATAAREA_ID = g_c.Key.DATAAREA_ID,
                                SITE_ID = g_c.Key.SITE_ID,
                                Count = g_c.Count()
                            }
                            )
                    join b in
                        (
                            from d in ((WmsUnitDataContext)Context).MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIAs
                            where d.POST == "1"
                            group d by new { d.STOCK_TAKING_ID, d.DATAAREA_ID, d.SITE_ID } into g_d
                            select new
                            {
                                STOCK_TAKING_ID = g_d.Key.STOCK_TAKING_ID,
                                DATAAREA_ID = g_d.Key.DATAAREA_ID,
                                SITE_ID = g_d.Key.SITE_ID,
                                Count = g_d.Count()
                            }
                            ) on
                        new { a.STOCK_TAKING_ID, a.DATAAREA_ID, a.SITE_ID, a.Count } equals
                        new { b.STOCK_TAKING_ID, b.DATAAREA_ID, b.SITE_ID, b.Count } into a_b_join
                    from result_a_b_join in a_b_join
                    where
                        result_a_b_join.DATAAREA_ID == UserSession.DATAAREA_ID
                        && result_a_b_join.SITE_ID == UserSession.SITE_ID_MAPPING
                    select new STOCK_TAKING_RECORD(a.STOCK_TAKING_ID);

                return list.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        public bool IsPost(String stock_taking_id)
        {
            try
            {
                List<MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA> list = StockTakingUnitSamplingObject.List(
                    stock_taking_id);

                if (list.Count <= 0) return false;

                foreach (var a in list)
                {
                    if (a.POST == "0") return false;
                }

                return true;
            }
            catch (MPMException)
            {
                return false;
            }
        }

        public void DoStockTaking(String machine_or_frame_id, String stock_taking_id,
            String dataarea_id, String site_id, String head_location_id)
        {
            try
            {
                /*IQueryable<MPMWMS_UNIT> search =
                    from unitQuery in ((WmsUnitDataContext)Context).MPMWMS_UNITs
                    where (unitQuery.FRAME_ID == "MH1" + machine_or_frame_id || unitQuery.MACHINE_ID == machine_or_frame_id)
                    select unitQuery; 
                */

                //proses by scan qrcode
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }
                MPMWMS_UNIT unit = new MPMWMS_UNIT();


                if (qrcode.Length > 20)
                {
                    unit = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);
                }
                else
                {
                    unit = UnitObject.ItemByMachineOrFrameId(qrcode, dataarea_id);
                }
                //end of proses scan by qrcode

                ////proses scan by nosin
                //MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_or_frame_id);
                ////end of proses scan by nosin

                if (unit == null)
                {
                    throw new MPMException("Unit not found !!!!");
                }

                if (unit.SITE_ID != site_id)
                {
                    throw new MPMException("Can't find the unit from another site !!!");
                }

                if (unit != null)
                {
                    // get data in sampling criteria for get date value
                    MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA criteria =
                            StockTakingUnitSamplingObject.Item(stock_taking_id, dataarea_id, site_id);
                    if (criteria == null) 
                    {
                        throw new MPMException("Can't find ID - " + stock_taking_id + " !!!");
                    }
                    
                    MPMWMS_STOCK_TAKING_UNIT stocktaking = StockTakingUnitObject.Create();
                    stocktaking.STOCK_TAKING_ID = stock_taking_id;
                    stocktaking.STOCK_TAKING_DATE = criteria.STOCK_TAKING_DATE;
                    stocktaking.DATAAREA_ID = unit.DATAAREA_ID;
                    stocktaking.SITE_ID = unit.SITE_ID;
                    stocktaking.WAREHOUSE_ID = unit.WAREHOUSE_ID;
                    stocktaking.LOCATION_ID = unit.LOCATION_ID;
                    stocktaking.FRAME_ID = unit.FRAME_ID;
                    stocktaking.MACHINE_ID = unit.MACHINE_ID;
                    stocktaking.COLOR_TYPE = unit.COLOR_TYPE;
                    stocktaking.UNIT_TYPE = unit.UNIT_TYPE;
                    stocktaking.SITE_ID_MANUAL = site_id;
                    stocktaking.HEAD_LOCATION_ID_MANUAL = head_location_id;
                    StockTakingUnitObject.Insert(stocktaking);
                    SubmitChanges();

                    // filter by sampling criteria
                    MPMWMS_STOCK_TAKING_UNIT_SAMPLE_CRITERIA criteriaSampling =
                            StockTakingUnitSamplingObject.FilterByCriteria(
                                stock_taking_id, unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.UNIT_TYPE, unit.COLOR_TYPE);

                    if (criteriaSampling == null)
                    {
                        throw new MPMException("Invalid data at sampling criteria !!!");
                    }
                }
                else
                {
                    throw new MPMException("Unit not found !!!!");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
                //iniiii.. 2x input
            }
        }
    }
}
