﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.MPMWMSPartService;
using MPMWMSMODEL.Table.CustPackingSlipJour;
using MPMWMSMODEL.Table.DosPart;
using MPMWMSMODEL.Table.PackingPart;
using MPMWMSMODEL.Table.PackingPartLine;
using MPMWMSMODEL.Table.PickingPart;
using MPMWMSMODEL.Table.SalesTable;
using MPMWMSMODEL.Table.TroliPart;
using MPMWMSMODEL.Table.WmsPickingRoute;
using MPMWMSMODEL.Table.PackingBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Table.PackingPartMapping;
using MPMWMSMODEL.Table.MapPicking;
using MPMWMSMODEL.Record.PackingPart;
using MPMWMSMODEL.Table.SOPICArea;
using MPMWMSMODEL.Table.BoxProperty;
using MPMWMSMODEL.Table.SOPICAreaHistory;
using MPMWMSMODEL.Table.Setting;

namespace MPMWMSMODEL.Model.PDT
{
    public class TRANS_PACKINGBOX
    {
        public PDT_PACKING_DATA_LINE HEADER { get; set; }
        public List<MPMWMS_PACKINGBOX> DETAIL { get; set; }
        public int IS_DETAIL_AVAILABLE { get; set; }
    }

    public class TRANS_SCANPART
    {
        public List<PDT_PACKING_DATA_LINE> PACKING_SCAN { get; set; }
        public int PACKING_TOTAL_PACKED { get; set; }
        public int PACKING_TOTAL_ALL { get; set; }
        public int IS_POSTING { get; set; }
        public int PACKING_BOX_TOTAL { get; set; }

    }

    public class TransMonitoringPacking
    {
        public String XTSTROLLEYNBR { get; set; }
        public String PIC { get; set; }
        public String DEALERCODE { get; set; }
        public String PACKINGID { get; set; }
        public String PICKINGID { get; set; }
        public String SALESID { get; set; }
        public int XTSISTRANSFERED { get; set; }

        public TransMonitoringPacking()
        {
 
        }
    }

    public class PackingPartModel: PDTPartModel
    {
        private WmsPickingRouteObject _WmsPickingRouteObject = new WmsPickingRouteObject();
        private CustPackingSlipJourObject _CustPackingSlipJourObject = new CustPackingSlipJourObject();
        private TroliPartObject _TroliPartObject = new TroliPartObject();
        private DosPartObject _DosPartObject = new DosPartObject();
        private SalesTableObject _SalesTableObject = new SalesTableObject();
        private PackingPartMappingObject _PackingPartMappingObject = new PackingPartMappingObject();
        private PackingBoxObject _PackingBoxObject = new PackingBoxObject();
        private MapPickingObject _MapPickingObject = new MapPickingObject();
        private PackingPartObject _PackingPartObject = new PackingPartObject();
        private SOPICAreaObject _SOPICAreaObject = new SOPICAreaObject();
        private BoxPropertyObject _BoxPropertyObject = new BoxPropertyObject();
        private SOPICAreaHistoryObject _SOPICAreaHistoryObject = new SOPICAreaHistoryObject();
        private SettingDetailObject _SettingDetailObject = new SettingDetailObject();

        public PackingPartModel()
            : base()
        {
            _WmsPickingRouteObject.Context = (WmsUnitDataContext)Context;
            _WmsPickingRouteObject.WmsOrderTransObject.Context = _WmsPickingRouteObject.Context;
            _CustPackingSlipJourObject.Context = (WmsUnitDataContext)Context;
            _CustPackingSlipJourObject.CustPackingSlipTransObject.Context = _CustPackingSlipJourObject.Context;
            _TroliPartObject.Context = (WmsUnitDataContext)Context;
            _DosPartObject.Context = (WmsUnitDataContext)Context;
            _SalesTableObject.Context = (WmsUnitDataContext)Context;
            _SalesTableObject.SalesLineObject.Context = _SalesTableObject.Context;
            _PackingPartMappingObject.Context = (WmsUnitDataContext)Context;
            _PackingBoxObject.Context = (WmsUnitDataContext)Context;
            _MapPickingObject.Context = (WmsUnitDataContext)Context;
            _PackingPartObject.Context = (WmsUnitDataContext)Context;
            _SOPICAreaObject.Context = (WmsUnitDataContext)Context;
            _BoxPropertyObject.Context = (WmsUnitDataContext)Context;
            _SOPICAreaHistoryObject.Context = (WmsUnitDataContext)Context;
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;
        }

        public MapPickingObject MapPickingObject
        {
            get { return _MapPickingObject; }
        }

        public SalesTableObject SalesTableObject
        {
            get { return _SalesTableObject; }
        }

        public WmsPickingRouteObject WmsPickingRouteObject
        {
            get { return _WmsPickingRouteObject; }
        }

        public CustPackingSlipJourObject CustPackingSlipJourObject
        {
            get { return _CustPackingSlipJourObject; }
        }

        public TroliPartObject TroliPartObject
        {
            get { return _TroliPartObject; }
        }

        public DosPartObject DosPartObject
        {
            get { return _DosPartObject; }
        }

        public PackingPartMappingObject PackingPartMappingObject
        {
            get { return _PackingPartMappingObject; }
        }

        public PackingBoxObject PackingBoxObject
        {
            get { return _PackingBoxObject; }
        }

        /**
         * Create New DOS
         **/
        public String CreateDOS(String dataarea_id, String dimension)
        {
            MPMWMS_DOS_PART dos = DosPartObject.Item(dimension, dataarea_id);
            if (dos == null)
            {
                dos = DosPartObject.Create();
                dos.DOS_ID = dimension; // +"/" + RunningNumberDosPart(dataarea_id);
                dos.DATAAREA_ID = dataarea_id;
                DosPartObject.Insert(dos);
                SubmitChanges();

                return dos.DOS_ID;
            }
            else
            {
                return dimension;
            }
        }

        /**
         * Show DOS
         **/
        public List<String> ListDos(String dataarea_id, String troli_id, String pic)
        {
            IQueryable<String> query = from packing_line in CustPackingSlipJourObject.CustPackingSlipTransObject.Context.CUSTPACKINGSLIPTRANs
                                       join packing in CustPackingSlipJourObject.Context.CUSTPACKINGSLIPJOURs on
                                            new { packing_id = packing_line.PACKINGSLIPID, 
                                                    dataarea = packing_line.DATAAREAID,
                                                    do_id = packing_line.SALESID 
                                            } equals
                                            new
                                            {
                                                packing_id = packing.PACKINGSLIPID,
                                                dataarea = packing.DATAAREAID,
                                                do_id = packing.SALESID
                                            } into pack
                                       from result in pack.DefaultIfEmpty()
                                       where (result.DATAAREAID == dataarea_id) &&
                                       (result.XTSLOCKEDUSERID == pic) &&
                                       (result.XTSTROLLEYNBR == troli_id) &&
                                       (packing_line.XTSCARTONNUMBER != "")
                                       group packing_line by new { packing_line.XTSCARTONNUMBER } into packing_line_group
                                       select packing_line_group.Key.XTSCARTONNUMBER;
            return query.ToList();
        }

        public List<String> ListDealerPacking(String dataarea_id, String troli_id, String pic)
        {
            IQueryable<String> query = from packing_line in CustPackingSlipJourObject.CustPackingSlipTransObject.Context.CUSTPACKINGSLIPTRANs
                                       join packing in CustPackingSlipJourObject.Context.CUSTPACKINGSLIPJOURs on
                                            new
                                            {
                                                packing_id = packing_line.PACKINGSLIPID,
                                                dataarea = packing_line.DATAAREAID,
                                                do_id = packing_line.SALESID
                                            } equals
                                            new
                                            {
                                                packing_id = packing.PACKINGSLIPID,
                                                dataarea = packing.DATAAREAID,
                                                do_id = packing.SALESID
                                            } into pack
                                       from result in pack.DefaultIfEmpty()
                                       where (result.DATAAREAID == dataarea_id) &&
                                       (result.XTSLOCKEDUSERID == pic) &&
                                       (result.XTSTROLLEYNBR == troli_id) &&
                                       (result.XTSISTRANSFERED==1)
                                       //(packing_line.XTSCARTONNUMBER != "")
                                       group result by new { result.INVOICEACCOUNT } into packing_dealer_group
                                       select packing_dealer_group.Key.INVOICEACCOUNT;
            return query.ToList();
        }

        /** 
        ** Create by Sluvie
        ** List Job from Transporter 
        **/
        public class JOB_RECORD {
            public int NUMBER { get; set; }
            public string DEALER { get; set; }

            public JOB_RECORD(int number, string dealer)
            {
                NUMBER = number;
                DEALER = dealer;
            }
        }

        public List<JOB_RECORD> ListJobPacking(string dataarea_id, string trolley_id, string pic)
        {
            try
            {
                IQueryable<JOB_RECORD> query =
                    from a in MapPickingObject.Context.MPMWMS_MAP_PICKINGs
                    from b in CustPackingSlipJourObject.Context.CUSTPACKINGSLIPJOURs
                    where
                        a.JOURNALID == b.XTSPICKINGROUTEID
                        && b.DATAAREAID == dataarea_id
                        && b.XTSTROLLEYNBR == trolley_id
                        && b.XTSLOCKEDUSERID == pic
                        && b.PARTITION == 5637144576
                    select new JOB_RECORD(a.NUMBER, b.INVOICEACCOUNT);
                return query.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }

        /** 
        ** Create by Sluvie
        ** Dealer Code by job 
        **/
        public string DealerCodeByJob(int job_code)
        {
            try
            {
                IQueryable<string> query =
                    from a in MapPickingObject.Context.MPMWMS_MAP_PICKINGs
                    from b in CustPackingSlipJourObject.Context.CUSTPACKINGSLIPJOURs
                    where
                        a.JOURNALID == b.XTSPICKINGROUTEID
                        && a.NUMBER == job_code
                        && b.PARTITION == 5637144576
                    select b.INVOICEACCOUNT;
                return (query.Take(1).FirstOrDefault() == null ? "" : query.Take(1).FirstOrDefault());
            }
            catch (MPMException)
            {
                return "";
            }
        }

        /** AX **/
        public List<PDT_PACKING_DATA> ListPackingAX(String dataarea_id, String trolley_id, String pic)
        {
            IQueryable<PDT_PACKING_DATA> search =
                from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                    new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                    new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                from result in pack_join.DefaultIfEmpty()
                where
                    line.DATAAREAID == dataarea_id
                    && result.XTSTROLLEYNBR == trolley_id
                    && result.XTSLOCKEDUSERID == pic
                    && line.XTSISPACKED != 2
                group line by new  { line.PACKINGSLIPID, result.XTSPICKINGROUTEID, line.DATAAREAID, 
                    result.ORDERACCOUNT, result.DELIVERYNAME } into pack_group
                select new PDT_PACKING_DATA(
                    pack_group.Key.PACKINGSLIPID, 
                    pack_group.Key.XTSPICKINGROUTEID, 
                    pack_group.Key.DATAAREAID,
                    pack_group.Key.ORDERACCOUNT,
                    pack_group.Key.DELIVERYNAME
                );
            
            return search.ToList();
        }

        public List<PDT_PACKING_DATA> ListPackingAXPicked(String dataarea_id, String trolley_id, String pic)
        {
            IQueryable<PDT_PACKING_DATA> search =
                from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                    new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                    new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                from result in pack_join.DefaultIfEmpty()
                where
                    line.DATAAREAID == dataarea_id
                    && result.XTSTROLLEYNBR == trolley_id
                    && result.XTSLOCKEDUSERID == pic
                    && line.XTSISPACKED != 2
                    && result.XTSISTRANSFERED == 1
                group line by new
                {
                    line.PACKINGSLIPID,
                    result.XTSPICKINGROUTEID,
                    line.DATAAREAID,
                    result.ORDERACCOUNT,
                    result.DELIVERYNAME
                } into pack_group
                select new PDT_PACKING_DATA(
                    pack_group.Key.PACKINGSLIPID,
                    pack_group.Key.XTSPICKINGROUTEID,
                    pack_group.Key.DATAAREAID,
                    pack_group.Key.ORDERACCOUNT,
                    pack_group.Key.DELIVERYNAME
                );

            return search.ToList();
        }

        public List<PDT_PACKING_DATA_LINE> ListPackingAXLine(String dataarea_id, String trolley_id, String pic)
        {
            IQueryable<PDT_PACKING_DATA_LINE> search =
                from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                    new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                    new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                from result in pack_join.DefaultIfEmpty()
                join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                    new { result.DATAAREAID, result.SALESID } equals
                    new { sales.DATAAREAID, sales.SALESID } into sales_join
                from result_sales in sales_join
                where
                    line.DATAAREAID == dataarea_id
                    && result.XTSTROLLEYNBR == trolley_id
                    && result.XTSLOCKEDUSERID == pic
                    && line.XTSISPACKED != 2
                    && result.XTSISTRANSFERED == 1
                    //&& result_sales.SALESSTATUS == 1 // open order
                select new PDT_PACKING_DATA_LINE(
                        result.PACKINGSLIPID, 
                        result.XTSPICKINGROUTEID, 
                        result.DATAAREAID, 
                        line.ITEMID,
                        result.ORDERACCOUNT,
                        result.DELIVERYNAME,
                        line.XTSISPACKED
                    );

            return search.ToList();
        }

        private int TOTALITEMALL_PACKING(string dataareaid, string trolleyid, string pic, string dealercode)
        {
            try
            {
                var result =
                    from a in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    from b in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs
                    where
                        a.DATAAREAID == dataareaid
                        && a.PACKINGSLIPID == b.PACKINGSLIPID
                        && a.DATAAREAID == b.DATAAREAID
                        && a.PARTITION == b.PARTITION
                        && b.XTSTROLLEYNBR == trolleyid
                        && b.XTSLOCKEDUSERID == pic
                        && b.INVOICEACCOUNT == dealercode
                        && a.PARTITION == 5637144576
                        && b.XTSISTRANSFERED == 0
                    select new { a.LINENUM };
                return result.Count();
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        private int TOTALITEMPACKED_PACKING(string dataareaid, string trolleyid, string pic, string dealercode)
        {
            try
            {
                var result =
                    from a in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    from b in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs
                    where
                        a.DATAAREAID == dataareaid
                        && a.PACKINGSLIPID == b.PACKINGSLIPID
                        && a.DATAAREAID == b.DATAAREAID
                        && a.PARTITION == b.PARTITION
                        && b.XTSTROLLEYNBR == trolleyid
                        && b.XTSLOCKEDUSERID == pic
                        && b.INVOICEACCOUNT == dealercode
                        && a.XTSISPACKED == 1
                        && a.PARTITION == 5637144576
                        && b.XTSISTRANSFERED == 0
                    select new { a.LINENUM };
                return result.Count();
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        private List<string> ListPacking(string dataareaid, string trolleyid, string pic)
        {
            try
            {
                IQueryable<string> result =
                    from b in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs
                    where
                        b.DATAAREAID == dataareaid
                        && b.XTSTROLLEYNBR == trolleyid
                        && b.XTSLOCKEDUSERID == pic
                        && b.PARTITION == 5637144576
                    select b.PACKINGSLIPID;
                return result.ToList();
            }
            catch (MPMException)
            {
                return null;
            }
        }


        public TRANS_SCANPART ListPackingAXLineFilterDealerCodeForScanPart(String dataareaid, String trolleyid, String pic, String dealercode)
        {
            TRANS_SCANPART temp = new TRANS_SCANPART();
            
            List<PDT_PACKING_DATA_LINE> packing_line_by_dealer_code = new List<PDT_PACKING_DATA_LINE>();
            packing_line_by_dealer_code = ListPackingAXLineFilterDealerCode( dataareaid,  trolleyid,  pic,  dealercode);
            /*
            List<PDT_PACKING_DATA_LINE> packing_line_by_dealer_code_all = new List<PDT_PACKING_DATA_LINE>();
            packing_line_by_dealer_code_all = ListPackingAXLineFilterDealerCodeAll(dataarea_id, trolley_id, pic, dealer_code);

            int packed = 0;
            int total = 0;
            int totalbox = 0;
            String packingid = "";

            List<string> list_packing = new List<string>();

            foreach (PDT_PACKING_DATA_LINE item in packing_line_by_dealer_code_all.OrderBy(a=>a.PACKING_ID))
            {
                if (item.IS_PACKED == 1)
                {
                    packed++;
                }
                
                total++;
                
                if (packingid != item.PACKING_ID)
                {
                    list_packing.Add(item.PACKING_ID);
                    packingid = item.PACKING_ID;
                    
                    //totalbox = totalbox + _PackingBoxObject.Number_distinctbox(packingid);
                }
            }

            if (list_packing.Count()>0)
                totalbox = _PackingBoxObject.Number_distinctbox(list_packing);
            */

            /*
            foreach( PDT_PACKING_DATA_LINE item in ListPackingAXLineFilterPerProcessAll(dataarea_id,  trolley_id,  pic, String dealer_code))
            {
                if (packingid != item.PACKING_ID)
                {
                    packingid = item.PACKING_ID;
                    totalbox = totalbox + _PackingBoxObject.Number_distinctbox(packingid);
                }
            }
            */

            List<string> list_packing = ListPacking(dataareaid, trolleyid, pic);
            temp.PACKING_SCAN = packing_line_by_dealer_code;
            temp.PACKING_TOTAL_PACKED = TOTALITEMPACKED_PACKING(dataareaid, trolleyid, pic, dealercode);
            temp.PACKING_TOTAL_ALL = TOTALITEMALL_PACKING(dataareaid, trolleyid, pic, dealercode);
            temp.IS_POSTING = IsPostingAvailablePackingAXLine( dataareaid, trolleyid, pic);
            temp.PACKING_BOX_TOTAL = _PackingBoxObject.Number_distinctbox(list_packing);

            return temp;

        }

        public int IsPostingAvailablePackingAXLine(String dataarea_id, String trolley_id, String pic)
        {
            IEnumerable<PDT_PACKING_DATA_LINE> search =
                    from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                        new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID, line.PARTITION } equals
                        new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID, pack.PARTITION } into pack_join
                    from result in pack_join.DefaultIfEmpty()
                    join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                        new { result.DATAAREAID, result.SALESID, result.PARTITION } equals
                        new { sales.DATAAREAID, sales.SALESID, sales.PARTITION } into sales_join
                    from result_sales in sales_join
                    //group result_sales by new {line.DATAAREAID, result.INVOICEACCOUNT} into group_result_sales
                    //from group_result_sales in 
                    where
                        line.DATAAREAID == dataarea_id
                        && result.XTSTROLLEYNBR == trolley_id
                        && result.XTSLOCKEDUSERID == pic
                        //&& line.XTSISPACKED != 2
                        //&& line.XTSISPACKED == 0
                        && result.XTSISTRANSFERED == 1
                        && line.PARTITION == 5637144576
                    select new PDT_PACKING_DATA_LINE(
                        result.PACKINGSLIPID,
                        result.XTSPICKINGROUTEID,
                        result.DATAAREAID,
                        line.ITEMID,
                        result.ORDERACCOUNT,
                        result.DELIVERYNAME,
                        line.XTSISPACKED
                        );

            bool is_posting = true;
            foreach (PDT_PACKING_DATA_LINE item in search)
            {
                if (item.IS_PACKED == 0)
                    is_posting = false;
            }

            if (is_posting == true)
                return 1;
            else
                return 0;
        }

        public List<PDT_PACKING_DATA_LINE> ListPackingAXLineFilterDealerCode(String dataarea_id, String trolley_id, String pic, String dealer_code)
        {
            IEnumerable<PDT_PACKING_DATA_LINE> search =
                    from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                        new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID, line.PARTITION } equals
                        new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID, pack.PARTITION } into pack_join
                    from result in pack_join.DefaultIfEmpty()
                    join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                        new { result.DATAAREAID, result.SALESID, result.PARTITION } equals
                        new { sales.DATAAREAID, sales.SALESID, sales.PARTITION } into sales_join
                    from result_sales in sales_join
                    //group result_sales by new {line.DATAAREAID, result.INVOICEACCOUNT} into group_result_sales
                    //from group_result_sales in 
                    where
                        line.DATAAREAID == dataarea_id
                        && result.XTSTROLLEYNBR == trolley_id
                        && result.XTSLOCKEDUSERID == pic
                        //&& line.XTSISPACKED != 2
                        && line.XTSISPACKED == 0
                        && result.XTSISTRANSFERED == 1
                        && result.INVOICEACCOUNT == dealer_code
                        && line.PARTITION == 5637144576
                    select new PDT_PACKING_DATA_LINE(
                        result.PACKINGSLIPID,
                        result.XTSPICKINGROUTEID,
                        result.DATAAREAID,
                        line.ITEMID,
                        result.ORDERACCOUNT,
                        result.DELIVERYNAME,
                        line.XTSISPACKED
                        );

            IEnumerable<PDT_PACKING_DATA_LINE> search2 =
                from hasil in search
                group hasil by new { hasil.DATAAREA_ID, hasil.DEALER_CODE, hasil.DEALER_NAME, hasil.PART_ID } into group_result_sales
                select new PDT_PACKING_DATA_LINE(
                        "-",
                        "-",
                        group_result_sales.Key.DATAAREA_ID,
                        group_result_sales.Key.PART_ID,
                        group_result_sales.Key.DEALER_CODE,
                        group_result_sales.Key.DEALER_NAME,
                        0
                    );
                
            return search2.ToList();
        }

        public List<PDT_PACKING_DATA_LINE> ListPackingAXLineFilterDealerCodeAll(String dataarea_id, String trolley_id, String pic, String dealer_code)
        {
            IEnumerable<PDT_PACKING_DATA_LINE> search =
                    from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                        new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                        new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                    from result in pack_join.DefaultIfEmpty()
                    join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                        new { result.DATAAREAID, result.SALESID } equals
                        new { sales.DATAAREAID, sales.SALESID } into sales_join
                    from result_sales in sales_join
                    //group result_sales by new {line.DATAAREAID, result.INVOICEACCOUNT} into group_result_sales
                    //from group_result_sales in 
                    where
                        line.DATAAREAID == dataarea_id
                        && result.XTSTROLLEYNBR == trolley_id
                        && result.XTSLOCKEDUSERID == pic
                        && line.XTSISPACKED != 2
                        //&& line.XTSISPACKED == 0
                        && result.XTSISTRANSFERED == 1
                        && result.INVOICEACCOUNT == dealer_code
                    select new PDT_PACKING_DATA_LINE(
                        result.PACKINGSLIPID,
                        result.XTSPICKINGROUTEID,
                        result.DATAAREAID,
                        line.ITEMID,
                        result.ORDERACCOUNT,
                        result.DELIVERYNAME,
                        line.XTSISPACKED
                        );

            IEnumerable<PDT_PACKING_DATA_LINE> search2 =
                from hasil in search
                group hasil by new {hasil.PACKING_ID,hasil.PICKING_ID,  hasil.DATAAREA_ID, hasil.PART_ID, hasil.DEALER_CODE, hasil.DEALER_NAME, hasil.IS_PACKED } into group_result_sales
                select new PDT_PACKING_DATA_LINE(
                        group_result_sales.Key.PACKING_ID,
                        group_result_sales.Key.PICKING_ID,
                        group_result_sales.Key.DATAAREA_ID,
                        group_result_sales.Key.PART_ID,
                        group_result_sales.Key.DEALER_CODE,
                        group_result_sales.Key.DEALER_NAME,
                        group_result_sales.Key.IS_PACKED
                    );

            return search2.ToList();
        }

        public List<string> ListPackingAXLineFilterPerProcessAll(String dataarea_id, String trolley_id, String pic, String dealer_code)
        {
            //IEnumerable<PDT_PACKING_DATA_LINE> search =
            var search =
                    from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                    join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                        new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                        new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                    from result in pack_join.DefaultIfEmpty()
                    join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                        new { result.DATAAREAID, result.SALESID } equals
                        new { sales.DATAAREAID, sales.SALESID } into sales_join
                    from result_sales in sales_join
                    //group result_sales by new {line.DATAAREAID, result.INVOICEACCOUNT} into group_result_sales
                    //from group_result_sales in 
                    where
                        line.DATAAREAID == dataarea_id
                        && result.XTSTROLLEYNBR == trolley_id
                        && result.XTSLOCKEDUSERID == pic
                        && line.XTSISPACKED != 2
                        //&& line.XTSISPACKED == 0
                        && result.XTSISTRANSFERED == 1
                        && result.INVOICEACCOUNT == dealer_code
                    select new
                    {
                        result.PACKINGSLIPID,
                        result.XTSPICKINGROUTEID,
                        result.DATAAREAID,
                        line.ITEMID,
                        result.ORDERACCOUNT,
                        result.DELIVERYNAME,
                        line.XTSISPACKED
                   };

            var search2 =
                from hasil in search
                group hasil by new { hasil.PACKINGSLIPID } into group_result_sales
                select new
                {
                    PACKINGSLIPID = group_result_sales.Key.PACKINGSLIPID
                };


            List<string> temp = new List<string>();

            foreach (var item in search2.ToList())
            {
                temp.Add(item.PACKINGSLIPID);
            }

            return temp;
        }

        public List<WMSORDERTRAN> ListPickingLine(String dataarea_id, String trolley_id)
        {
            IQueryable<WMSORDERTRAN> search =
                from line in WmsPickingRouteObject.WmsOrderTransObject.Context.WMSORDERTRANs
                join pick in WmsPickingRouteObject.WmsOrderTransObject.Context.WMSPICKINGROUTEs on
                    new { dataarea = line.DATAAREAID, pick = line.ROUTEID } equals
                    new { dataarea = pick.DATAAREAID, pick = pick.PICKINGROUTEID } into pick_join
                from result in pick_join.DefaultIfEmpty()
                where
                    line.DATAAREAID == dataarea_id
                    && result.XTSTROLLEYNBR == trolley_id
                    && line.EXPEDITIONSTATUS == 10
                orderby result.PICKINGROUTEID ascending
                select line;

            return search.ToList();
        }


        public IQueryable<TransMonitoringPacking> ListMonitoringPacking()
        {
            IQueryable<TransMonitoringPacking> search =
                    from pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs
                    join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                        new { pack.DATAAREAID, pack.SALESID } equals
                        new { sales.DATAAREAID, sales.SALESID }
                    join troli in ((WmsUnitDataContext)Context).MPMWMS_TROLI_PARTs on
                        new { pack.XTSTROLLEYNBR } equals
                        new { XTSTROLLEYNBR = troli.TROLI_ID }
                    where
                        pack.DATAAREAID == UserSession.DATAAREA_ID
                        && pack.XTSTROLLEYNBR != ""
                        //&& result.XTSTROLLEYNBR == trolley_id
                        && pack.XTSLOCKEDUSERID != ""
                        //&& line.XTSISPACKED != 2
                        //&& line.XTSISPACKED == 0
                        //&& pack.XTSISTRANSFERED == 1
                        && sales.INVENTSITEID == UserSession.SITE_ID_MAPPING
                        && sales.SALESSTATUS == 2
                    orderby troli.ORDER
                    select new TransMonitoringPacking
                    {
                        XTSTROLLEYNBR = pack.XTSTROLLEYNBR,
                        PIC = pack.XTSLOCKEDUSERID,
                        DEALERCODE = pack.INVOICEACCOUNT,
                        PACKINGID = pack.PACKINGSLIPID,
                        PICKINGID = pack.XTSPICKINGROUTEID,
                        SALESID = sales.SALESID,
                        XTSISTRANSFERED = pack.XTSISTRANSFERED
                    };

            return search;
        }

        public void SetPickingStatusPacking(String dataarea_id, String packingslip_id, String route_id, String item_id, String inventdim_id)
        {
            WMSORDERTRAN item = WmsPickingRouteObject.WmsOrderTransObject.Item(dataarea_id, route_id, item_id, inventdim_id);
            item.XTSPACKINGSLIPID = packingslip_id;
        }

        public void UpdatePacking(String dataarea_id, String packing_id, String item_id, String dos_id, int quantity)
        {
            try
            {
                // skenario get sum quantity ordered on current packing_id and part_id
                List<CUSTPACKINGSLIPTRAN> lines = CustPackingSlipJourObject.CustPackingSlipTransObject.List(
                    dataarea_id,
                    packing_id,
                    item_id);

                int sumQty_linescust = 0;

                foreach (var line in lines)
                {
                    {
                        sumQty_linescust += Convert.ToInt32(line.QTY);
                    }
                }

                List<MPMWMS_PACKINGBOX> lines_box = _PackingBoxObject.List(packing_id, item_id);
                int sumQty = 0;

                foreach (var line in lines_box)
                {
                    sumQty += Convert.ToInt32(line.QUANTITY);
                }

                if (sumQty_linescust == sumQty)
                {
                    //jika nominal sama maka update menjadi 1

                    foreach (var line in lines)
                    {
                        //line.XTSCARTONNUMBER = dos_id;
                        //line.QTY = quantity;
                        line.XTSISPACKED = 1;
                    }
                    SubmitChanges();

                }

                /*
                int sumQty = 0;
                foreach (var line in lines)
                {
                    if (line.XTSISPACKED == 0)
                    {
                        sumQty += Convert.ToInt32(line.QTY);
                    }
                }

                if (quantity != sumQty)
                {
                    throw new MPMException("Quantity not valid !!!");
                }
                
                
 
                foreach (var line in lines)
                {
                    line.XTSCARTONNUMBER = dos_id;
                    //line.QTY = quantity;
                    line.XTSISPACKED = 1;                    
                }
                SubmitChanges();
                */ 
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UnloadPackingLine(String dataarea_id, String packing_id, String item_id)
        {
            CUSTPACKINGSLIPTRAN line = CustPackingSlipJourObject.CustPackingSlipTransObject.Item(
                dataarea_id,
                packing_id,
                item_id);
            
            if (line == null)
            {
                throw new MPMException("Part Id can't be found !!!");
            }

            line.XTSCARTONNUMBER = "";
            //line.QTY = 0;
            line.XTSISPACKED = 0;
            SubmitChanges();
        }

        public void Finish(String dataarea_id, String trolley_id, String pic)
        {
            try
            {
                
                MPMWMSPartService.SalesPackingSlipServiceClient client = ConnectWebServiceSales();
                List<PDT_PACKING_DATA_LINE> packings = ListPackingAXLine(dataarea_id, trolley_id, pic);
                foreach (var packing in packings)
                {   
                    CUSTPACKINGSLIPTRAN line = CustPackingSlipJourObject.CustPackingSlipTransObject.Item(
                        packing.DATAAREA_ID, packing.PACKING_ID, packing.PART_ID);
                    if (line.XTSISPACKED == 1)
                    {
                        line.XTSISPACKED = 2;
                        WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataarea_id, packing.PICKING_ID);
                        if (route != null)
                        {
                            route.XTSTROLLEYNBR = "";
                            route.XTSLOCKEDUSERID = "";
                        }
                        
                        client.regSOToDelivered(ContextWS, line.SALESID);
                        CUSTPACKINGSLIPJOUR item_custpackingslipjour = CustPackingSlipJourObject.Item(line.PACKINGSLIPID, dataarea_id);
                        item_custpackingslipjour.XTSTROLLEYNBR = "";
                        item_custpackingslipjour.XTSLOCKEDUSERID = "";
                        item_custpackingslipjour.XTSISTRANSFERED = 2;

                        MPMWMS_MAP_PICKING picking = _MapPickingObject.Item(packing.PICKING_ID);
                        if (picking != null)
                        {
                            picking.JOURNALID = null;
                        }
                    }
                }

                /*List<PDT_PACKING_DATA> packs = ListPackingAX(dataarea_id, trolley_id, pic);
                foreach (var a in packs)
                {
                }*/

                SubmitChanges();

                DisconnectWebServiceSales(client);
            }
            //catch (Exception e)
            catch (System.ServiceModel.FaultException<MPMWMSPartService.AifFault> exception)//(Exception exception) 
            {
                //throw new MPMException(e.Message);

                String msg = "";
                foreach (var exceptionLog in exception.Detail.InfologMessageList)
                {
                    msg = msg + exceptionLog.Message;
                }
                throw new Exception(msg);
            }
        }

        public void DeletePackingBox(String dealer_code, String packing_id, String box_id, String part_id)
        {
            //update custpacking slip trans
            List<CUSTPACKINGSLIPTRAN> linescust = CustPackingSlipJourObject.CustPackingSlipTransObject.List(
                    UserSession.DATAAREA_ID,
                    packing_id,
                    part_id);

            foreach (CUSTPACKINGSLIPTRAN line in linescust)
            {
                line.XTSISPACKED = 0;
                SubmitChanges();
            }

            //delete packing box
            MPMWMS_PACKINGBOX itemperbox = _PackingBoxObject.item(packing_id, box_id, part_id);
            _PackingBoxObject.Delete(itemperbox);
            SubmitChanges();
        }

        public void RemovePacking(String packing_id)
        {
            List<CUSTPACKINGSLIPTRAN> linescust = CustPackingSlipJourObject.CustPackingSlipTransObject.ListByPackingId(
                    UserSession.DATAAREA_ID,
                    packing_id);

            foreach (CUSTPACKINGSLIPTRAN line in linescust)
            {
                //delete packing box
                List<MPMWMS_PACKINGBOX> listpackingbox = _PackingBoxObject.List(line.PACKINGSLIPID, line.ITEMID);

                foreach (MPMWMS_PACKINGBOX itempackingbox in listpackingbox)
                {
                    _PackingBoxObject.Delete(itempackingbox);
                }

                line.XTSISPACKED = 0;
                SubmitChanges();
            }

            CUSTPACKINGSLIPJOUR tran = _CustPackingSlipJourObject.Item(
                packing_id, UserSession.DATAAREA_ID);
            if (tran != null)
            {
                tran.XTSISTRANSFERED = 0;
                SubmitChanges();
            }

        }

        public void InsertOrUpdatePackingBox(String dataarea_id, String trolley_id, String dealer_code, String pic, String box_id, String part_no, int qty )
        {
            IEnumerable<PDT_PACKING_DATA_LINE> searchwithqty_sub = 
                from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                        join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                            new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                            new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                        from result in pack_join.DefaultIfEmpty()
                        join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                            new { result.DATAAREAID, result.SALESID } equals
                            new { sales.DATAAREAID, sales.SALESID } into sales_join
                        from result_sales in sales_join
                        where
                            line.DATAAREAID == dataarea_id
                            && result.XTSTROLLEYNBR == trolley_id
                            && result.XTSLOCKEDUSERID == pic
                            //&& line.XTSISPACKED != 2
                            && line.XTSISPACKED == 0
                            && result.XTSISTRANSFERED == 1
                            && result.INVOICEACCOUNT == dealer_code
                            && line.ITEMID == part_no
                        select new PDT_PACKING_DATA_LINE(
                            result.PACKINGSLIPID,
                            result.XTSPICKINGROUTEID,
                            result.DATAAREAID,
                            line.ITEMID,
                            result.ORDERACCOUNT,
                            result.DELIVERYNAME,
                            line.XTSISPACKED,
                            (int)line.QTY
                            );

            IEnumerable<PDT_PACKING_DATA_LINE> searchwithqty =
                from hasil in searchwithqty_sub
                group hasil by new { hasil.DATAAREA_ID, hasil.DEALER_CODE, hasil.DEALER_NAME, hasil.PART_ID } into group_result_sales
                select new PDT_PACKING_DATA_LINE(
                        "-",
                        "-",
                        group_result_sales.Key.DATAAREA_ID,
                        group_result_sales.Key.PART_ID,
                        group_result_sales.Key.DEALER_CODE,
                        group_result_sales.Key.DEALER_NAME,
                        0,
                        group_result_sales.Sum(a=>a.QTY)
                    );

            IEnumerable<PDT_PACKING_DATA_LINE> search_sub =
                        from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                        join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                            new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                            new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                        from result in pack_join.DefaultIfEmpty()
                        join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                            new { result.DATAAREAID, result.SALESID } equals
                            new { sales.DATAAREAID, sales.SALESID } into sales_join
                        from result_sales in sales_join
                        where
                            line.DATAAREAID == dataarea_id
                            && result.XTSTROLLEYNBR == trolley_id
                            && result.XTSLOCKEDUSERID == pic
                            //&& line.XTSISPACKED != 2
                            && line.XTSISPACKED == 0
                            && result.XTSISTRANSFERED == 1
                            && result.INVOICEACCOUNT == dealer_code
                            && line.ITEMID == part_no
                        select new PDT_PACKING_DATA_LINE(
                            result.PACKINGSLIPID,
                            result.XTSPICKINGROUTEID,
                            result.DATAAREAID,
                            line.ITEMID,
                            result.ORDERACCOUNT,
                            result.DELIVERYNAME,
                            line.XTSISPACKED
                            );

            IEnumerable<PDT_PACKING_DATA_LINE> search =
                        from hasil in search_sub
                        group hasil by new { hasil.PACKING_ID, hasil.PICKING_ID, hasil.DATAAREA_ID, hasil.PART_ID,hasil.DEALER_CODE, hasil.DEALER_NAME, hasil.IS_PACKED} into group_result_sales
                        select new PDT_PACKING_DATA_LINE(
                            group_result_sales.Key.PACKING_ID,
                            group_result_sales.Key.PICKING_ID,
                            group_result_sales.Key.DATAAREA_ID,
                            group_result_sales.Key.PART_ID,
                            group_result_sales.Key.DEALER_CODE,
                            group_result_sales.Key.DEALER_NAME,
                            group_result_sales.Key.IS_PACKED
                        );


            List<PDT_PACKING_DATA_LINE> searchlist = search.ToList();

            int qtylinebox = 0;
            foreach (var itemsearch in searchlist)
            {
                List<MPMWMS_PACKINGBOX> linesbox = _PackingBoxObject.List(itemsearch.PACKING_ID, itemsearch.PART_ID);

                foreach (var itemlinebox in linesbox)
                {
                    qtylinebox = qtylinebox + itemlinebox.QUANTITY;
                }

            }

            int qty_total = searchwithqty.ToList().First().QTY;

            //pengecekan apakah masih available
            if (qty > (qty_total - qtylinebox))
            {
                throw new MPMException("Quantity not valid !!!");
            }

            //---------------------------------------------------------------------

            int qty_available = qty;
            //mencari packing yang bisa dipenuhi
            foreach (var itemsearch in searchlist.OrderBy(x=>x.PACKING_ID))
            {
                List<CUSTPACKINGSLIPTRAN> linescust = CustPackingSlipJourObject.CustPackingSlipTransObject.List(
                    UserSession.DATAAREA_ID,
                    itemsearch.PACKING_ID,
                    itemsearch.PART_ID);
                int sumQty_linescust = 0;
                foreach (var line in linescust)
                {
                   sumQty_linescust = sumQty_linescust + Convert.ToInt32(line.QTY);   
                }

                List<MPMWMS_PACKINGBOX> linesbox = _PackingBoxObject.List(itemsearch.PACKING_ID, itemsearch.PART_ID);
                int sumQty = 0;
                foreach (var itemlinebox in linesbox)
                {
                    sumQty = sumQty + itemlinebox.QUANTITY;
                }

                if ( qty_available>0  && sumQty_linescust > sumQty)
                {
                    //dimasukkan ke box
                    MPMWMS_PACKINGBOX itemperbox = _PackingBoxObject.item(itemsearch.PACKING_ID, box_id, itemsearch.PART_ID);

                    if (itemperbox != null)
                    {
                        if (qty_available >= (sumQty_linescust - sumQty)) // penuhi semua
                        {
                            itemperbox.QUANTITY = itemperbox.QUANTITY + (sumQty_linescust - sumQty);
                            qty_available = qty_available - (sumQty_linescust - sumQty);

                            //update custinvoicetrans
                            foreach (var temp_update in linescust)
                            {
                                temp_update.XTSISPACKED = 1;
                            }

                            
                        }
                        else //penuhi sebagian
                        {
                            itemperbox.QUANTITY = itemperbox.QUANTITY + qty_available;
                            qty_available = qty_available - qty_available;
                        }

                        //update
                        _PackingBoxObject.Update(itemperbox);
                        SubmitChanges();
                    }
                    else
                    {
                        //insert
                        MPMWMS_PACKINGBOX newRowMPMWMS_PACKINGBOX = new MPMWMS_PACKINGBOX();
                        newRowMPMWMS_PACKINGBOX.DEALERCODE = itemsearch.DEALER_CODE;
                        newRowMPMWMS_PACKINGBOX.PACKINGID = itemsearch.PACKING_ID;
                        newRowMPMWMS_PACKINGBOX.BOXID = box_id;
                        newRowMPMWMS_PACKINGBOX.PARTNO = itemsearch.PART_ID;

                        if (qty_available >= (sumQty_linescust - sumQty)) // penuhi semua
                        {
                            newRowMPMWMS_PACKINGBOX.QUANTITY = (sumQty_linescust - sumQty);
                            qty_available = qty_available - (sumQty_linescust - sumQty);

                            //update custinvoicetrans
                            foreach (var temp_update in linescust)
                            {
                                temp_update.XTSISPACKED = 1;
                            }

                            SubmitChanges();
                        }
                        else
                        {
                            newRowMPMWMS_PACKINGBOX.QUANTITY = qty_available;
                            qty_available = qty_available - qty_available;

                        }

                        _PackingBoxObject.Insert(newRowMPMWMS_PACKINGBOX);

                        SubmitChanges();
                    }
                }
                
            }

        }

        public List<MPMWMS_PACKINGBOX> ListPackingBox(String packingId)
        {
            return _PackingBoxObject.List(packingId);
        }

        public List<TRANS_PACKINGBOX> ListTransPackingBox(String dataarea_id, String trolley_id, String dealer_code, String pic)
        {
            IEnumerable<PDT_PACKING_DATA_LINE> search_sub =
                        from line in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPTRANs
                        join pack in ((WmsUnitDataContext)Context).CUSTPACKINGSLIPJOURs on
                            new { dataarea = line.DATAAREAID, pack = line.PACKINGSLIPID } equals
                            new { dataarea = pack.DATAAREAID, pack = pack.PACKINGSLIPID } into pack_join
                        from result in pack_join.DefaultIfEmpty()
                        join sales in ((WmsUnitDataContext)Context).SALESTABLEs on
                            new { result.DATAAREAID, result.SALESID } equals
                            new { sales.DATAAREAID, sales.SALESID } into sales_join
                        from result_sales in sales_join
                        where
                            line.DATAAREAID == dataarea_id
                            && result.XTSTROLLEYNBR == trolley_id
                            && result.XTSLOCKEDUSERID == pic
                            && line.XTSISPACKED != 2
                            //&& line.XTSISPACKED == 0
                            && result.XTSISTRANSFERED == 1
                            && result.INVOICEACCOUNT == dealer_code
                        select new PDT_PACKING_DATA_LINE(
                            result.PACKINGSLIPID,
                            result.XTSPICKINGROUTEID,
                            result.DATAAREAID,
                            line.ITEMID,
                            result.ORDERACCOUNT,
                            result.DELIVERYNAME,
                            line.XTSISPACKED
                            );

            IEnumerable<PDT_PACKING_DATA_LINE> search =
                        from hasil in search_sub
                        group hasil by new { hasil.PACKING_ID, hasil.PICKING_ID, hasil.DATAAREA_ID, hasil.PART_ID, hasil.DEALER_CODE, hasil.DEALER_NAME, hasil.IS_PACKED } into group_result_sales
                        select new PDT_PACKING_DATA_LINE(
                            group_result_sales.Key.PACKING_ID,
                            group_result_sales.Key.PICKING_ID,
                            group_result_sales.Key.DATAAREA_ID,
                            group_result_sales.Key.PART_ID,
                            group_result_sales.Key.DEALER_CODE,
                            group_result_sales.Key.DEALER_NAME,
                            group_result_sales.Key.IS_PACKED
                        );

            List<PDT_PACKING_DATA_LINE> searchlist = search.ToList();
            List<TRANS_PACKINGBOX> trans_packingboxline = new List<TRANS_PACKINGBOX> ();


            foreach (PDT_PACKING_DATA_LINE itemsearch in searchlist)
            {
                TRANS_PACKINGBOX temp_trans_packingboxline = new TRANS_PACKINGBOX();

                //add header
                temp_trans_packingboxline.HEADER = itemsearch;

                //add line
                List<MPMWMS_PACKINGBOX> linesbox = _PackingBoxObject.List(itemsearch.PACKING_ID, itemsearch.PART_ID);
                temp_trans_packingboxline.DETAIL = linesbox;

                if (linesbox.Any())
                {
                    temp_trans_packingboxline.IS_DETAIL_AVAILABLE = 1;
                }
                else
                    temp_trans_packingboxline.IS_DETAIL_AVAILABLE = 0;


                //add list
                trans_packingboxline.Add(temp_trans_packingboxline);

                
            }

            return trans_packingboxline;
        }



        public void GeneratePacking(String dataarea_id, String trolley_id, String pic)
        {
            try
            {
                MPMWMSPartService.SalesPackingSlipServiceClient client = ConnectWebServiceSales();

                // list all header picking that must be to create into packing slip
                List<WMSPICKINGROUTE> pickings = WmsPickingRouteObject.ListForPacking(dataarea_id, 10, trolley_id);
                foreach (var pick in pickings)
                {
                    // detail
                    List<WMSORDERTRAN> lines = WmsPickingRouteObject.WmsOrderTransObject.List(pick);
                    if (lines.Count > 0)
                    {
                        bool is_create = false;
                        if (lines[0].XTSPACKINGSLIPID.Equals(""))
                        {
                            is_create = true;
                        }

                        if (is_create)
                        {
                            //Saya pindahkan pengisian sales parm table ke sebelum looping karena seharusnya hanya terbentuk 1 line header saja
                            SALESTABLE sales = SalesTableObject.Item(pick.TRANSREFID, dataarea_id, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                            if (sales == null)
                            {
                                throw new MPMException("Sales Id not found !!!!");
                            }

                            AxdEntity_SalesParmTable ParmTables = new AxdEntity_SalesParmTable();
                            AxdEntity_SalesParmLine[] ParmLine = new AxdEntity_SalesParmLine[lines.Count];

                            EntityKey[] keys;
                            AxdSalesPackingSlip runPackingSlip = new AxdSalesPackingSlip();
                            keys = new EntityKey[1];

                            ParmTables.SalesId = sales.SALESID;
                            ParmTables.CustAccount = sales.CUSTACCOUNT;
                            ParmTables.InvoiceAccount = sales.INVOICEACCOUNT;
                            ParmTables.SalesName = sales.SALESNAME;
                            ParmTables.Log = trolley_id + ",0," + UserSession.NPK + "," + pick.PICKINGROUTEID;

                            int i = 0;
                            foreach (var line in lines)
                            {
                                if (line.QTY > 0)
                                {
                                    try
                                    {
                                        //Deklarasikan arraynya untuk mengisikan linenya
                                        ParmLine[i] = new AxdEntity_SalesParmLine();
                                        ParmLine[i].ItemId = line.ITEMID;
                                        ParmLine[i].InventDimId = line.INVENTDIMID;
                                        ParmLine[i].DeliverNow = line.QTY;
                                        ParmLine[i].InventNow = 1;
                                        ParmLine[i].InventTransId = line.INVENTTRANSID;
                                        ParmLine[i].CustAccount = sales.CUSTACCOUNT;
                                        ParmLine[i].InvoiceAccount = sales.INVOICEACCOUNT;
                                        ParmLine[i].OrigSalesId = sales.SALESID;
                                        ParmLine[i].LineNum = 1;
                                        ParmLine[i].DeliverNowSpecified = true;
                                        i++;
                                    }
                                    catch (Exception e)
                                    {
                                        throw new MPMException(e.Message);
                                    }
                                }
                            }

                            //Saya pindahkan syntax untuk menjoinkan line terhadap parm table dan untuk posting packing slipnya 
                            ParmTables.SalesParmLine = ParmLine;
                            runPackingSlip.SalesParmTable = new AxdEntity_SalesParmTable[] { ParmTables };
                            EntityKey[] PackingSlipId = client.create(ContextWS, runPackingSlip);
                            EntityKey key_value = PackingSlipId[0];
                            String paramId = key_value.KeyData[0].Value;

                            // find packing slip id
                            CUSTPACKINGSLIPJOUR pack = CustPackingSlipJourObject.ItemBySID(paramId, dataarea_id);
                            String packingId = "";
                            if (pack != null)
                            {
                                packingId = pack.PACKINGSLIPID;
                                pack.XTSTROLLEYNBR = trolley_id;
                                pack.XTSLOCKEDUSERID = pic;
                                pack.XTSPICKINGROUTEID = pick.PICKINGROUTEID;
                            }

                            foreach (var line in lines)
                            {
                                line.XTSPACKINGSLIPID = packingId;
                            }
                            SubmitChanges();
                        }
                    }
                }

                DisconnectWebServiceSales(client);

                // try to update trolley
                /*pickings = WmsPickingRouteObject.List(dataarea_id, 10, pic, trolley_id);
                foreach (var pick in pickings)
                {
                    List<WMSORDERTRAN> lines = WmsPickingRouteObject.WmsOrderTransObject.List(pick);
                    if (lines.Count > 0)
                    {
                        if (!lines[0].XTSPACKINGSLIPID.Equals(""))
                        {
                            CUSTPACKINGSLIPJOUR pack = CustPackingSlipJourObject.Item(lines[0].XTSPACKINGSLIPID, dataarea_id);
                            if (pack != null)
                            {
                                pack.XTSTROLLEYNBR = trolley_id;
                                pack.XTSLOCKEDUSERID = pic;
                                SubmitChanges();
                            }
                        }
                    }
                }*/
            }
            //catch (Exception e)
            catch (System.ServiceModel.FaultException<MPMWMSPartService.AifFault> exception)//(Exception exception) 
            {
                //throw new MPMException(e.Message);

                String msg = "";
                foreach (var exceptionLog in exception.Detail.InfologMessageList)
                {
                    msg = msg + exceptionLog.Message;
                }
                throw new Exception(msg);

            }
        }
        /** END OF AX **/


        #region UNTUK APP PACKING DI WINDOWS FORM

        public List<MPMWMS_MAP_SO_PICAREA> listDataPacking(string _trollyNbr, string _npk, string _dataArea)
        {
            return _PackingPartObject.dataPacking(_trollyNbr, _npk, _dataArea);
        }

        public List<int> listJobIDReadyPack(string _trollyNbr, string _npk, string _dataArea)
        {
            return _PackingPartObject.jobIDReadyPack(_trollyNbr, _npk, _dataArea);
        }

        public List<PACKING_INFO_Record> listDataPackingInfoByJob(string _npk, string _dataArea, int _jobId)
        {
            return _PackingPartObject.dataPackingInfoByJob(_dataArea, _npk, _jobId);
        }

        public MPMWMS_PACKINGBOX getPackingBox(MPMWMS_PACKINGBOX _item)
        {
            return _PackingBoxObject.item(_item.PACKINGID, _item.BOXID, _item.PARTNO);
        }

        public void insertUpdatePackingBox(MPMWMS_PACKINGBOX _item)
        {
            try
            {
                MPMWMS_PACKINGBOX dataPackingBox = _PackingBoxObject.item(_item.PACKINGID, _item.BOXID, _item.PARTNO);

                BeginTransaction();

                if (dataPackingBox == null)
                {
                    _PackingBoxObject.Insert(_item);
                }
                else
                {
                    dataPackingBox.QUANTITY = _item.QUANTITY;
                    _PackingBoxObject.Update(dataPackingBox);
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new Exception(e.Message);
            }
        }

        public Boolean checkAlreadyPacked(String _packingId, String _itemId, String _boxId)
        {
            return _PackingPartObject.checkAlreadyPacked(_packingId, _itemId, _boxId);
        }

        public int getSisaQtyPacking(String _itemId, int _jobId, String _dataArea, String _siteId)
        {
            return Convert.ToInt32(_PackingPartObject.qtySisaPacking(_itemId, _jobId, _dataArea, _siteId));
        }

        public void updateFlagIsPacked(String _itemId, int _jobId, String _dataArea, String _siteId, int _isPacked)
        {
            try
            {
                _PackingPartObject.updateFlagIsPacked(_itemId, _jobId, _dataArea, _siteId, _isPacked);
            }
            catch (MPMException e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<LIST_PART_Record> getDataListPart(string _trolley, String _dataArea, String _siteId)
        {
            return _PackingPartObject.dataListPart(_trolley, _dataArea, _siteId);
        }

        public void deletePackingBox(MPMWMS_PACKINGBOX _item)
        {
            try
            {
                MPMWMS_PACKINGBOX dataPackingBox = _PackingBoxObject.item(_item.PACKINGID, _item.BOXID, _item.PARTNO);

                BeginTransaction();

                if (dataPackingBox != null)
                {
                    _PackingBoxObject.Delete(dataPackingBox);
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new Exception(e.Message);
            }
        }

        public String prosesPostingPacking(String _dataArea, String _siteId, String _mainDealer, String _trolleyId)
        {
            String returnStr = string.Empty;
            MPMWMS_MAP_SO_PICAREA picArea = new MPMWMS_MAP_SO_PICAREA();
            Boolean isPackedFinish = false;

            isPackedFinish = _PackingPartObject.cekIsPackedFinish(_trolleyId, _dataArea, _siteId);

            if (isPackedFinish)
            {
                var data = this.getDataListPart(_trolleyId, _dataArea, _siteId);

                try
                {
                    prosesInvoice(_dataArea, data, _mainDealer, _siteId);
                }
                catch (MPMException e)
                {
                    throw new Exception(e.Message);
                }

                BeginTransaction();
                try
                {
                    foreach (var item in data)
                    {
                        this.updateFlagIsPacked(item.ITEMID, item.JOBID, _dataArea, _siteId, 2);
                        _PackingPartObject.resetFlagCustPackingSlipJour(item.ITEMID, item.JOBID, _dataArea, _siteId);
                    }

                    foreach (var item in data.GroupBy(x => new { x.JOBID, x.SOID }))
                    {
                        returnStr += "Job " + item.Key.JOBID + " Sales ID " + item.Key.SOID + " berhasil posting." + System.Environment.NewLine;
                    }

                    foreach (var job in data.GroupBy(x => new { x.JOBID }))
                    {
                        picArea = _SOPICAreaObject.ItemByJob(job.Key.JOBID, _dataArea, _mainDealer, _siteId);

                        if (picArea != null)
                        {
                            _SOPICAreaObject.Delete(picArea);
                        }
                    }

                    Commit();

                    return returnStr;
                }
                catch (MPMException e)
                {
                    Rollback();

                    throw new Exception(e.Message);
                }
            }
            else
            {
                throw new Exception("Proses packing untuk trolley " + _trolleyId + " masih belum selesai !!!");
            }
        }

        public void insertOrUpdateBoxProperties(MPMWMS_BOXPROPERTy  _item)
        {
            try
            {
                MPMWMS_BOXPROPERTy boxProperties = _BoxPropertyObject.item(_item.BOXID);

                BeginTransaction();

                if (boxProperties == null)
                {
                    _BoxPropertyObject.Insert(_item);
                }
                else
                {
                    boxProperties.DIMENSION = _item.DIMENSION;
                    boxProperties.WEIGHT = _item.WEIGHT;

                    _BoxPropertyObject.Update(boxProperties);
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new Exception(e.Message);
            }
        }

        public String numSeqStikerBox(String _site, int _hari)
        {
            int numberSeq = 0;
            String leadingZero = "000";

            numberSeq = _PackingPartObject.getWeightSticker("REG-BOX-PACKING", _site + "-" + _hari, DateTime.Today.Year, DateTime.Today.Month);

            return _hari + leadingZero.Substring(1, (leadingZero.Length - numberSeq.ToString().Length)) + numberSeq.ToString();
        }

        public int getDimensionBox(String _collieCode, String _maindealer, String _siteid)
        {
            try
            {
                if (_collieCode != "")
                {
                    var strLen = (_collieCode.Length > 4) ? 4 : _collieCode.Length;
                    _collieCode = _collieCode.Substring(0, strLen);
                }

                return _PackingPartObject.getDimensionBox(_collieCode, _maindealer, _siteid);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public MPMWMS_COLLIE getCollieData(String _collieCode, String _maindealer, String _siteid)
        {
            if(_collieCode != "")
            {
                var strLen = (_collieCode.Length > 4) ? 4 : _collieCode.Length;
                _collieCode = _collieCode.Substring(0, strLen);
            }

            return _PackingPartObject.getCollieData(_collieCode, _maindealer, _siteid);
        }

        public void prosesInvoice(String dataarea, List<LIST_PART_Record> data, String maindealer, String siteid)
        {
            try
            {
                MPMWMS_MAP_PIC_HISTORY picHistory = new MPMWMS_MAP_PIC_HISTORY();
                //String invoiceId = "";
                MPMWMSPartService.SalesPackingSlipServiceClient client = ConnectWebServiceSales();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 5, 0);
                client.Endpoint.Binding.CloseTimeout = new TimeSpan(0, 5, 0);
                client.Endpoint.Binding.OpenTimeout = new TimeSpan(0, 5, 0);
                client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 5, 0);
                client.Endpoint.Binding.ReceiveTimeout = new TimeSpan(0, 5, 0);
                

                var salesData = data.GroupBy(x => new { x.SOID }).ToList();

                foreach (var pick in salesData)
                {
                    client.generateInvoice(ContextWS, pick.Key.SOID);

                    // update waktu endpacking ke table MPMWMS_MAP_PIC_HISTORY
                    picHistory = _SOPICAreaHistoryObject.ItemBySO(pick.Key.SOID, dataarea, maindealer, siteid);

                    if (picHistory != null)
                    {
                        picHistory.ENDPACKING = MPMDateUtil.DateTime;

                        _SOPICAreaHistoryObject.Update(picHistory);
                    }
                }

                SubmitChanges();
                DisconnectWebServiceSales(client);
            }
            catch (System.ServiceModel.FaultException<MPMWMSPartService.AifFault> exception)//(Exception exception) 
            {
                Rollback();

                String msg = "";
                foreach (var exceptionLog in exception.Detail.InfologMessageList)
                {
                    msg = msg + exceptionLog.Message;
                }
                throw new Exception(msg);
            }
        }

        public void prosesPacking(String dataarea_id, String trolley_id, String pic, String maindealer, String siteid)
        {
            try
            {
                MPMWMS_MAP_PIC_HISTORY picHistory = new MPMWMS_MAP_PIC_HISTORY();

                #region RD ~ COMMENT 8 DES 2017 

                /* RD ~ COMMENT 8 DES 2017 
                String packingSlipId = "";
                MPMWMSPartService.SalesPackingSlipServiceClient client = ConnectWebServiceSales();

                var salesData = (WmsPickingRouteObject.ListReadyToPack(dataarea_id, 10, trolley_id))
                                .GroupBy(x => new { x.TRANSREFID, x.PICKINGROUTEID }).ToList();

                foreach (var pick in salesData)
                {
                    packingSlipId = client.generatePacking(ContextWS, pick.Key.TRANSREFID);
                    //packingSlipId = client.generatePacking(ContextWS, pick.Key.TRANSREFID, pic);

                    // find packing slip id
                    CUSTPACKINGSLIPJOUR pack = CustPackingSlipJourObject.Item(packingSlipId, dataarea_id);
                    
                    if (pack != null)
                    {
                        CustPackingSlipJourObject.updateRuteId(pack.PACKINGSLIPID);

                        pack.XTSTROLLEYNBR = trolley_id;
                        pack.XTSLOCKEDUSERID = pic;
                        pack.XTSPICKINGROUTEID = pick.Key.PICKINGROUTEID;

                        foreach (var line in _WmsPickingRouteObject.WmsOrderTransObject.ItemsPacking(pick.Key.PICKINGROUTEID, dataarea_id))
                        {
                            line.XTSPACKINGSLIPID = packingSlipId;
                        }
                    }
                    

                    // update pic checker = pic packer & waktu startscan ke table MPMWMS_MAP_PIC_HISTORY
                    picHistory = _SOPICAreaHistoryObject.ItemBySO(pick.Key.TRANSREFID, dataarea_id, maindealer, siteid);

                    if (picHistory != null)
                    {
                        MPM_SETTING_DETAIL setting_detail = _SettingDetailObject.Item("WORK_HOUR", UserSession.DATAAREA_ID, _SettingDetailObject.getEnumDay(DateTime.Today.DayOfWeek));

                        picHistory.PICCHECKER = pic;
                        picHistory.STARTPACKING = MPMDateUtil.DateTime;
                        picHistory.WORKHOURPACKING = Convert.ToInt16(setting_detail.TAG_1);
                        picHistory.RESTHOURPACKING = Convert.ToInt16(setting_detail.TAG_2);

                        _SOPICAreaHistoryObject.Update(picHistory);
                    }

                    SubmitChanges();
                }

                DisconnectWebServiceSales(client);
                */

                #endregion

                #region RD ~ COMMENT 25 MEI 2018

                /* RD ~ ADDED 8 DES 2017
                foreach (var data in _CustPackingSlipJourObject.ListByTrolley(trolley_id, dataarea_id))
                {
                    _CustPackingSlipJourObject.updateLockedUser(data.SALESID, pic, dataarea_id);

                    // update pic checker = pic packer & waktu startscan ke table MPMWMS_MAP_PIC_HISTORY
                    picHistory = _SOPICAreaHistoryObject.ItemBySO(data.SALESID, dataarea_id, maindealer, siteid);

                    if (picHistory != null)
                    {
                        MPM_SETTING_DETAIL setting_detail = _SettingDetailObject.Item("WORK_HOUR", UserSession.DATAAREA_ID, _SettingDetailObject.getEnumDay(DateTime.Today.DayOfWeek));

                        picHistory.PICCHECKER = pic;
                        picHistory.STARTPACKING = MPMDateUtil.DateTime;
                        picHistory.WORKHOURPACKING = Convert.ToInt16(setting_detail.TAG_1);
                        picHistory.RESTHOURPACKING = Convert.ToInt16(setting_detail.TAG_2);

                        _SOPICAreaHistoryObject.Update(picHistory);
                    }

                    SubmitChanges();
                }
                */

                #endregion

                MPM_SETTING_DETAIL setting_detail = _SettingDetailObject.Item("WORK_HOUR", UserSession.DATAAREA_ID, _SettingDetailObject.getEnumDay(DateTime.Today.DayOfWeek));

                // RD ADDED 25 MEI 2018
                _CustPackingSlipJourObject.updateLockedUserChecker(pic, trolley_id, siteid, dataarea_id, setting_detail.TAG_1, setting_detail.TAG_2);
            }
            catch (MPMException e)
            {
                throw new Exception(e.Message);
            }
            catch (System.ServiceModel.FaultException<MPMWMSPartService.AifFault> exception)//(Exception exception) 
            {
                String msg = "";
                foreach (var exceptionLog in exception.Detail.InfologMessageList)
                {
                    msg = msg + exceptionLog.Message;
                }
                throw new Exception(msg);
            }
        }

        public Boolean cekFinishIsPacked(String _dataArea, String _siteId, String _trolleyId)
        {
            try
            {
                return _PackingPartObject.cekIsPackedFinish(_trolleyId, _dataArea, _siteId);
            }
            catch (MPMException e)
            {
                throw new Exception(e.Message);
            }
        }

        public void updateRegCollie(String _dataArea, String _siteId, string _trolley)
        {
            try
            {
                String regCollie = "";
                int jobIdCurrent = 0;
                String soCurrent = "";

                var data = this.getDataListPart(_trolley, _dataArea, _siteId);

                var salesData = data.GroupBy(x => new { x.SOID, x.JOBID }).ToList();

                foreach (var z in salesData)
                {
                    if(jobIdCurrent == 0)
                    {
                        jobIdCurrent = z.Key.JOBID;
                        soCurrent = z.Key.SOID;
                    }
                    else
                    {
                        if(jobIdCurrent != z.Key.JOBID)
                        {
                            jobIdCurrent = z.Key.JOBID;
                            soCurrent = z.Key.SOID;
                        }
                    }

                    regCollie = "R" + DateTime.Today.Year.ToString().Substring(2) + soCurrent.Substring(19);

                    _PackingPartObject.updateRegCollie(_dataArea, _siteId, regCollie, z.Key.SOID);
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public String getIPPrinter(String _settingId, String _ipComputer)
        {
            MPM_SETTING_DETAIL item = _SettingDetailObject.Item(_settingId, _ipComputer, _ipComputer);

            if (item != null)
            {
                return item.TAG_1;
            }

            return "";
        }

        public void updateInvoiceProses(String _dataArea, String _siteId, string _trolley)
        {
            try
            {
                _PackingPartObject.updateInvoiceProses(_dataArea, _siteId, _trolley);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<PICKING_LIST_Record> getPickingListByJob(String _dataArea, String _siteId, String _jobId, String _trolley)
        {
            try
            {
                return _PackingPartObject.getPickingListByJob(_dataArea, _siteId, _jobId, _trolley);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void updateBoxId(String _dataArea, String _siteId, String _mainDealer, String _boxIdOld, String _boxIdNew)
        {
            try
            {
                _PackingPartObject.updateBoxId(_dataArea, _siteId, _mainDealer, _boxIdOld, _boxIdNew);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        #endregion
    }
}
