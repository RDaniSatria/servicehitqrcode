﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.ClockingUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class ClockingUnitModel : PDTUnitModel
    {
        public StoringVendorModel _StoringVendorModel = new StoringVendorModel();
        public List<ClockingUnitListPending> ListClocingPending(string userid, string siteid, string statuskerja)
        {
            try
            {
                return ClockingUnit.ListPending(userid, siteid, statuskerja);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public int ScenUnit(string Nomesin)
        {
            try
            {
                BeginTransaction();
                var item = UnitObject.ListClockingUnit(Nomesin);
                /*Hendy coment 2019-11-04
                if (item == null || item.Count <= 0)
                {
                    throw new MPMException("Status Nomor Mesin RFS tidak bisa scan, silahkan rubah status terlebih dulu");
                }
                */
                var item2 = HistClockingObject.Item(Nomesin);
                if (item2 != null)
                {
                    int?[] status = { 0, 2 };
                    var item3 = HistClockingObject.Item(Nomesin, UserSession.USER_ID, status);
                    if (item3 != null)
                    {
                        //alert 
                        return 1;
                    }
                    var item6 = HistClockingObject.Item(Nomesin, 3);
                    if (item6 != null)
                    {
                        throw new MPMException("Status Nomor mesin sudah selesai dikerjakan oleh teknisi " + item6.USERID);
                    }
                    var item4 = HistClockingObject.Item(Nomesin, 1);
                    if (item4 == null)
                    {
                        var item7 = HistClockingObject.Item(Nomesin, 0);
                        var item8 = HistClockingObject.Item(Nomesin, 2);
                        var item9 = HistClockingObject.ItemNotePassed(Nomesin, 3);
                        if(item7 == null && item8 == null && item9 == null)
                        throw new MPMException("Gagal!. Unit " + Nomesin + " masih dalam proses pengerjaan teknisi '" + item2.USERID + "' Silahkan request ke teknisi yang bersangkutan untuk Pending pekerjaan terlebih dulu");
                    }
                    
                    var item5 = ClockingUnit.ItemSalLKU(Nomesin);
                    if (item5 == null)
                    {
                        //Non Lku 
                        return 2;
                    }
                    else
                    {
                        //Lku
                        return 3;
                    }

                }
                else
                {
                    MPMWMSHISTCLOCKING insert = new MPMWMSHISTCLOCKING();
                    insert.USERID = UserSession.USER_ID;
                    insert.NOMESIN = Nomesin;
                    insert.STATUSKERJA = 0;
                    insert.TGLJAMMULAI = null;
                    insert.TGLJAMSELESAI = null;
                    insert.STATUSQC = 0;
                    
                    var item5 = ClockingUnit.ItemSalLKU(Nomesin);
                    if (item5 == null)
                    {
                        //Non Lku
                        insert.TYPEPERBAIKAN = "NON LKU";
                        HistClockingObject.Insert(insert);
                        Commit();
                        return 2;
                    }
                    else
                    {
                        //Lku
                        insert.TYPEPERBAIKAN = "LKU";
                        HistClockingObject.Insert(insert);
                        Commit();
                        return 3;
                    }

                    
                }
            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public MPMWMSHISTCLOCKING itemHist(string Nomesin)
        {
            try
            {
                return HistClockingObject.Item(Nomesin);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public int ScenConfirm(string Nomesin)
        {
            var item5 = ClockingUnit.ItemSalLKU(Nomesin);
            if (item5 == null)
            {
                //Non Lku 
                return 2;
            }
            else
            {
                //Lku
                return 3;
            }
        }
        public List<ClockingUnitNonLkujasa> ListNonLku(string Nomesin)
        {
            try
            {
                return ClockingUnit.ListNonLku(Nomesin);
            }
            catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void DeleteDetailNonLku(string Nomesin,string item)
        {
            BeginTransaction();
            try
            {
                ClockingUnit.DeleteDetailNonLku(Nomesin,item);
                Commit();
            }
            catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLkujasarepairRec> ListLkuRepair(string nosin, string userid)
        {
            try
            {
                var list = ClockingUnit.ListJasaRepair( nosin, userid);
                return list;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<Time> ListLkuTimer(int statuskerja, string unit, string userid)
        {
            try
            {
                var list = ClockingUnit.ListTime(statuskerja, unit, userid);
                return list.ToList();
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLkujasabongkarRec> ListLkuBongkat(string nosin, string userid)
        {
            try
            {
                var list = ClockingUnit.ListJasaBongkar( nosin, userid);
                return list;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void InsertDetailNonLku(string Nomesin,string item, int type)
        {
            BeginTransaction();
            try
            {
                MPMDETAILNONLKU insert = new MPMDETAILNONLKU();
                insert.USERID = UserSession.USER_ID;
                insert.NOMESIN = Nomesin;
                insert.TYPEPERBAIKAN = "NON LKU";
                if(type == 1)
                {
                    insert.TYPEREPAIR = "Bongkar Part";
                }
                else
                {
                    insert.TYPEREPAIR = "Repair";
                }
                insert.ITEM = item;
                ClockingUnit.InsertDetailNonLku(insert);
                Commit();
            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLisKerusakan> ListClockingNonLku(String Nomesin)
        {
            try
            {
                return ClockingUnit.ListKerusakan(Nomesin);
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitLisKerusakan> ListClockingLku(String Nomesin)
        {
            try
            {
                return ClockingUnit.ListKerusakanLku(Nomesin);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnit> ListUnitClocking(String Nomesin)
        {
            try
            {
                return ClockingUnit.ListUnit(Nomesin);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnit> ListHistClocking(String Nomesin)
        {
            try
            {
                return ClockingUnit.ListHistoryUnit(Nomesin);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingAsalUnit> ListAsalUnit(String Nomesin, string type)
        {
            try
            {
                return ClockingUnit.ListAsalUnitClocking(Nomesin, type);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingTujuanUnit> ListUnitTujuan(String Nomesin)
        {
            try
            {
                return ClockingUnit.ListTujuanUnit(Nomesin);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingTujuanUnit> ValidasiDetailNonLKU(String Nomesin, String KodePart)
        {
            try
            {
                return ClockingUnit.ValidasiDetailNonLKU(Nomesin, KodePart);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingTujuanUnit> ValidasiSalLKU(String Nomesin, String KodePart)
        {
            try
            {
                return ClockingUnit.ValidasiSalLKU(Nomesin, KodePart);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingTujuanUnit> ListCekDataMoving(String Nomesin, String KodePart)
        {
            try
            {
                return ClockingUnit.ListCekDataMoving(Nomesin, KodePart);
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public List<ClockingUnitNonLku> ItemNonLku(string Nomesin)
        {
            try
            {
                List<ClockingUnitNonLku> list = new List<ClockingUnitNonLku>();
                var itemtgl = (from a in ((WmsUnitDataContext)Context).MPMWMS_HISTORY_UNITs
                                    where a.MACHINE_ID == Nomesin
                                    && !a.WAREHOUSE_ID_OLD.EndsWith("nrfs")
                                    && a.WAREHOUSE_ID.EndsWith("nrfs")
                                    orderby a.CREATE_DATE descending
                                    select a).Take(1).FirstOrDefault();
                var item = HistClockingObject.Item(Nomesin);
                ClockingUnitNonLku NonLku = new ClockingUnitNonLku();
                NonLku.nomesin = Nomesin;
                NonLku.statuskerja = item.STATUSKERJA;
                NonLku.statusqc = item.STATUSQC;
                NonLku.tglqc = item.TGLSTATUSQC==null?"":item.TGLSTATUSQC.Value.ToString("dd/MM/yyyy");
                //NonLku.tgllku = itemtgl.CHANGES_DATE;
                NonLku.tgl = itemtgl == null ? "": itemtgl.CHANGES_DATE.ToString("dd/MM/yyyy") ;
                NonLku.userid = item.USERID;
                list.Add(NonLku);
                return list;
            }catch(MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }

        
        public List<ClockingUnitNonLku> ItemLku(string Nomesin)
        {
            try
            {
                List<ClockingUnitNonLku> list = new List<ClockingUnitNonLku>();
                //var itemtgl = ClockingUnit.ItemSalLKU(Nomesin);
                var itemtgl = ClockingUnit.ItemSalLKUH(Nomesin);
                var item = HistClockingObject.Item(Nomesin);
                ClockingUnitNonLku NonLku = new ClockingUnitNonLku();
                NonLku.nomesin = Nomesin;
                NonLku.statuskerja = item.STATUSKERJA;
                NonLku.statusqc = item.STATUSQC;
                NonLku.tgllku = itemtgl.TGLLKU;//itemtgl == null ? DateTime.Now : itemtgl.TGLLKU; //DateTime.Now;
                NonLku.tglqc = item.TGLSTATUSQC == null ? "" : item.TGLSTATUSQC.Value.ToString("dd/MM/yyyy");
                NonLku.tgl = Convert.ToDateTime(Convert.ToDateTime(NonLku.tgllku).ToShortDateString()).ToString("dd/MM/yyyy");//Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy");
                NonLku.userid = item.USERID;
                list.Add(NonLku);
                return list;
            }
            catch (MPMException ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        public void updateUserClocking (string Nosin)
        {
            BeginTransaction();
            try
            {
                var items = HistClockingObject.Item(Nosin);
                items.USERID = UserSession.USER_ID;
                items.TGLJAMMULAI = null;
                items.STATUSKERJA = 0;
                HistClockingObject.Update(items);
                Commit();
            }
            catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public void Start(int statuskerja ,string nomesin)
        {
            BeginTransaction();
            try
            {
                var cekClocking = ClockingUnit.CekItemCloking(nomesin);
                if(cekClocking.USERID == UserSession.USER_ID)
                {
                    if (statuskerja == 0)
                    {   
                        var items = HistClockingObject.Item(nomesin);
                        items.STATUSKERJA = 1;
                        items.TGLJAMMULAI = DateTime.Now;
                        items.USERID = UserSession.USER_ID;
                        HistClockingObject.Update(items);
                        var data = ClockingUnit.ListPendingClocking(nomesin);
                        data.ForEach(x => x.STATUSKERJA = 1);
                        data.ForEach(x => x.TGLJAMSELESAI = DateTime.Now);
                    }
                    if (statuskerja == 3)
                    {
                        var items = HistClockingObject.Item(nomesin);
                        items.STATUSKERJA = 1;
                        items.TGLJAMSELESAI = null;
                        items.TGLSTATUSQC = null;
                        items.PICQC = null;
                        items.USERID = UserSession.USER_ID;
                        HistClockingObject.Update(items);
                    }
                    else if (statuskerja == 2)
                    {
                        var items = HistClockingObject.Item(nomesin);
                        items.STATUSKERJA = 1;
                        //items.TGLJAMMULAI = DateTime.Now;
                        items.USERID = UserSession.USER_ID;
                        HistClockingObject.Update(items);   

                        var itemp = ClockingUnit.Itempending(nomesin);
                        itemp.STATUSKERJA = 1;
                        itemp.TGLJAMSELESAI = DateTime.Now;
                        ClockingUnit.UpdatePending(itemp);
                    }
                    Commit();
                }else
                {
                    throw new MPMException("TeknisiLain");
                }
                
            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }

        public void DeletDetailTujanUnit(string NosinTujuan,string NosinAwal, string item)
        {
            BeginTransaction();
            try
            {
                ClockingUnit.DeleteDetailTujuan(NosinAwal, NosinTujuan, item);
                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public void MovingPart(string kodepart, string nosinawal, string nosintujuan)
        {
            BeginTransaction();
            try
            {
                MPMLKUROBBINGPART insert = new MPMLKUROBBINGPART();
                insert.NOMESINAWAL = nosinawal;
                insert.NOMESINTUJUAN = nosintujuan;
                insert.USERID = UserSession.USER_ID;
                insert.KODEPART = kodepart;
                insert.TGLAMBIL = DateTime.Now;

                ClockingUnit.InsertMovingPart(insert);
                Commit();
            }
            catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
                

        }
        public void Pending(int statuskerja, string nomesin,string alasan)
        {
            BeginTransaction();
            try
            {
                var cekClocking = ClockingUnit.CekItemCloking(nomesin);
                if (cekClocking.USERID == UserSession.USER_ID)
                {
                    if (statuskerja == 1)
                    {
                        var items = HistClockingObject.Item(nomesin);
                        items.STATUSKERJA = 2;
                        HistClockingObject.Update(items);

                        MPMWMSHISTPENDINGMEKANIK insert = new MPMWMSHISTPENDINGMEKANIK();
                        insert.USERID = UserSession.USER_ID;
                        insert.STATUSKERJA = 2;
                        insert.NOMESIN = nomesin;
                        insert.TGLJAMMULAI = DateTime.Now;
                        insert.ALASANPENDING = alasan;
                        ClockingUnit.InsertPending(insert);
                    }
                    Commit();
                }
                else
                {
                    throw new MPMException("TeknisiLain");
                }
                
            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }
        public void Done(int statuskerja, string nomesin,int type)
        {
            BeginTransaction();
            try
            {
                var cekClocking = ClockingUnit.CekItemCloking(nomesin);
                if (cekClocking.USERID == UserSession.USER_ID)
                {
                    if (statuskerja == 1)
                    {
                        var items = HistClockingObject.Item(nomesin);
                        items.STATUSKERJA = 3;
                        items.TGLJAMSELESAI = DateTime.Now;
                        HistClockingObject.Update(items);
                        //Commit();
                        //type == 2 LKU
                        if (type == 2)
                        {
                            var data = ClockingUnit.ListSalLkuLine(nomesin);
                            data.ForEach(x => x.ISKELUAR = 1);
                            data.ForEach(x => x.TGLKELUAR = DateTime.Now);
                            //foreach (var itemslku in data )
                            //{
                            //    BeginTransaction();
                            //    try
                            //    {
                            //        itemslku.ISKELUAR = 1;
                            //        itemslku.TGLKELUAR = DateTime.Now;
                            //        ClockingUnit.UpdateSalLKU(itemslku);
                            //        Commit();
                            //    }catch(MPMException exx)
                            //    {
                            //        Rollback();
                            //        throw new MPMException(exx.Message);
                            //    }
                                
                            //}
                            
                        }
                    }
                    Commit();

                }
                else
                {
                    throw new MPMException("TeknisiLain");
                }
                
            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }

        // CLOCKING UNIT NRFS
        //tambahan anas
        public void ProsesTrackingQRnoWSClockingUnit(String machine_or_frame_id, String type, String flag)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.ItemByQR(UserSession.DATAAREA_ID, qrcode, UserSession.SITE_ID_MAPPING);
                
                if (unit != null)
                {
                    if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    {
                        throw new MPMException("Unit Tidak Ditemukan");
                    }

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas 
                    _StoringVendorModel.insertQRUnitClockingUni(unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, type, flag);
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }
    }
}
