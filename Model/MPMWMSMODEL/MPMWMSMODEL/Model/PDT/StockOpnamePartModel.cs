﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.MPMWMSPartService;
using MPMWMSMODEL.Table.InventDim;
using MPMWMSMODEL.Table.InventJournalTable;
using MPMWMSMODEL.Table.InventLocation;
using MPMWMSMODEL.Table.StockTakingPart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class StockOpnamePartModel: PDTPartModel
    {
        private InventJournalTableObject _InventJournalTableObject = new InventJournalTableObject();
        private StockTakingPartObject _StockTakingPartObject = new StockTakingPartObject();
        private InventDimObject _InventDimObject = new InventDimObject();

        public StockOpnamePartModel()
            : base()
        {
            _InventJournalTableObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.InventJournalTransObject.Context = _InventJournalTableObject.Context;

            _StockTakingPartObject.Context = (WmsUnitDataContext)Context;

            _InventDimObject.Context = (WmsUnitDataContext)Context;
        }

        public InventJournalTableObject InventJournalTableObject
        {
            get { return _InventJournalTableObject; }
        }

        public StockTakingPartObject StockTakingPartObject
        {
            get { return _StockTakingPartObject; }
        }

        public InventDimObject InventDimObject
        {
            get { return _InventDimObject; }
        }

        public void ValidationLocation(String dataarea_id, String site_id, String location_id)
        {
            INVENTLOCATION item = InventLocationObject.Item(dataarea_id, site_id, location_id);
            if (item == null)
            {
                throw new MPMException("Location can't found in this site " + site_id);
            }
        }

        public String GetFirstSiteFromStockOpname(String journal_id, String dataarea_id)
        {
            INVENTJOURNALTRAN tran =
                InventJournalTableObject.InventJournalTransObject.ItemByTag(journal_id, dataarea_id, "113");

            if (tran != null)
            {
                INVENTDIM dim = InventDimObject.Item(dataarea_id, tran.INVENTDIMID);
                return dim.INVENTSITEID;
            }

            return "";
        }

        public String StockOpname()
        {
            try
            {
                String journalId = "";
                InventJournalTableServiceClient client = ConnectWebService();
                journalId = client.StockOpname(ContextWS, UserSession.MAINDEALER_ID);
                DisconnectWebService(client);
                return journalId;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void StockOpnameLine(String journal_id, String dataarea_id, String current_part, String site_id,
            String current_rack, int quantity)
        {
            try
            {
                bool result = false;
                InventJournalTableServiceClient client = ConnectWebService();

                // check already exist or not
                // if exist then update
                INVENTJOURNALTRAN line = InventJournalTableObject.InventJournalTransObject.Item(journal_id, dataarea_id, current_part, site_id, current_rack);
                if (line != null)
                {
                    result = client.UpdateJournalTag(ContextWS, journal_id, line.INVENTTRANSID, quantity);                    
                    
                    /*line.COUNTED = quantity;
                    line.QTY = line.COUNTED - line.INVENTONHAND;*/
                }
                // else insert
                else
                {                    
                    client.StockOpnameLine(ContextWS, journal_id, current_part, site_id, current_rack, quantity);
                    line = InventJournalTableObject.InventJournalTransObject.Item(journal_id, dataarea_id, current_part, site_id, current_rack);
                    result = true;
                }
                DisconnectWebService(client);

                if (result)
                {
                    try
                    {
                        BeginTransaction();
                        // insert to history tag
                        InsertHistoryTag(line, quantity);
                        Commit();
                    }
                    catch (Exception e)
                    {
                        Rollback();
                        throw new MPMException(e.Message);
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        private void InsertHistoryTag(INVENTJOURNALTRAN line, int quantity)
        {
            try
            {
                // search site and rack from inventdim
                INVENTDIM dim = InventDimObject.Item(line.DATAAREAID, line.INVENTDIMID);

                MPMWMS_STOCK_TAKING_PART st = StockTakingPartObject.Create();
                st.CHANGES_DATE = MPMDateUtil.DateTime;
                st.STOCK_TAKING_ID = line.JOURNALID;
                st.COUNTING_ID = Convert.ToInt32(line.LINENUM);
                st.DATAAREA_ID = line.DATAAREAID;
                st.ITEM_ID = line.ITEMID;
                st.SITE_ID = dim.INVENTSITEID;
                st.WAREHOUSE_ID = dim.INVENTLOCATIONID;
                st.QUANTITY = quantity;
                st.COUNTING_TAG = (int) RunningNumberStockTakingPartCounting(st.DATAAREA_ID, st.SITE_ID, st.STOCK_TAKING_ID);
                StockTakingPartObject.Insert(st);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void StockOpnameLine(String journal_id, String dataarea_id, String tag, int quantity)
        {
            try
            {
                // check already exist or not
                // if exist then update
                INVENTJOURNALTRAN line = InventJournalTableObject.InventJournalTransObject.ItemByTag(journal_id, dataarea_id, tag);
                if (line != null)
                {
                    /*line.COUNTED = quantity;
                    line.QTY = line.COUNTED - line.INVENTONHAND;*/

                    InventJournalTableServiceClient client = ConnectWebService();
                    bool result = client.UpdateJournalTag(ContextWS, journal_id, line.INVENTTRANSID, quantity);
                    DisconnectWebService(client);

                    if (result)
                    {
                        try
                        {
                            BeginTransaction();
                            // insert to history tag
                            InsertHistoryTag(line, quantity);
                            Commit();
                        }
                        catch (MPMException e)
                        {
                            Rollback();
                            throw new MPMException(e.Message);
                        }
                    }
                    else
                    {
                        throw new MPMException("Web Service failed !!!");
                    }
                }
                else
                {
                    throw new MPMException("Tag Not found !!!");
                }
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void StockOpnameIntransitITV(String journal_id, String dataarea_id)
        {
            try
            {
                List<INVENTJOURNALTRAN> list = InventJournalTableObject.InventJournalTransObject.ListAll(dataarea_id, journal_id);

                foreach (var a in list)
                {
                    INVENTDIM dim = InventDimObject.Item(dataarea_id, a.INVENTDIMID);
                    if (dim.INVENTLOCATIONID == UserSession.SITE_ID_MAPPING + "ITV")
                    {
                        if (a.INVENTONHAND > 0)
                        {                            
                            InventJournalTableServiceClient client = ConnectWebService();
                            bool result = client.UpdateJournalTag(ContextWS, journal_id, a.INVENTTRANSID, Convert.ToInt32(a.INVENTONHAND));
                            DisconnectWebService(client);

                            if (result)
                            {
                                try
                                {
                                    BeginTransaction();
                                    // insert to history tag
                                    InsertHistoryTag(a, Convert.ToInt32(a.INVENTONHAND));
                                    Commit();
                                }
                                catch (MPMException e)
                                {
                                    Rollback();
                                    throw new MPMException(e.Message);
                                }
                            }
                            else
                            {
                                throw new MPMException("Web Service failed !!!");
                            }
                             
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void PostStoring(String journal_id)
        {
            InventJournalTableServiceClient client = ConnectWebService();
            //client.PostTransferJournal(ContextWS, journal_id);
            DisconnectWebService(client);
        }
    }
}
