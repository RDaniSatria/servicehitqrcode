﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.MPMWMSPartService;
using MPMWMSMODEL.View.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class MutationRackModel: PDTPartModel
    {
        public MutationRackModel()
            : base()
        {
        }

        public String AddHeader()
        {
            try
            {
                InventJournalTableServiceClient client = ConnectWebService();
                String journalId = client.MovementPart(ContextWS, UserSession.MAINDEALER_ID);
                DisconnectWebService(client);
                return journalId;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void AddDetail(String journal_id, String part_id, String site_id, String rack_begin, String rack_end, int qty)
        {
            try
            {
                // validate part
                ProductRecord part = ProductViewObject.Item(UserSession.DATAAREA_ID, site_id, site_id + rack_begin, "P", part_id);
                if (part == null)
                {
                    throw new MPMException("Part begin not found !!!");
                }
                if (part.QUANTITY < qty)
                {
                    throw new MPMException("Quantity overloaded [max: " + part.QUANTITY + "] !!!");
                }

                qty = qty * -1;

                InventJournalTableServiceClient client = ConnectWebService();
                client.MovementLinePart(ContextWS, journal_id, part_id, site_id, site_id + rack_begin, site_id, site_id + rack_end, qty);
                DisconnectWebService(client);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void ValidationProduct(String dataarea_id, String site_id, String item_id)
        {
            ProductRecord item = ProductViewObject.Item(dataarea_id, site_id, "P", item_id);
            if (item == null)
            {
                throw new MPMException("Can't find the Item ID, please choose another ID !!!");
            }
        }

        public void ValidationProduct(String dataarea_id, String site_id, String location_id, String item_id)
        {
            ProductRecord item = ProductViewObject.Item(dataarea_id, site_id, site_id + location_id, "P", item_id);
            if (item == null)
            {
                throw new MPMException("Can't find the Item ID, please choose another ID in the current location !!!");
            }
        }

        public void ValidationProduct(String dataarea_id, String site_id, String location_id, String item_id, int qty)
        {
            ProductRecord item = ProductViewObject.Item(dataarea_id, site_id, site_id + location_id, "P", item_id);
            if (item == null)
            {
                throw new MPMException("Can't find the Item ID, please choose another ID in the current location !!!");
            }

            if (item.QUANTITY < qty)
            {
                throw new MPMException("Quantity overloaded [" + item.QUANTITY + "]");
            }
        }

        public void PostStoring(String journal_id)
        {
            MPMWMSPartService.InventJournalTableServiceClient client = ConnectWebService();
            client.PostTransferJournal(ContextWS, journal_id);
            DisconnectWebService(client);
        }
    }
}
