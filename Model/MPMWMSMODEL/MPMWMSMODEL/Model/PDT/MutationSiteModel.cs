﻿using MPM365SERVICE.MPMMuliaService;
using MPM365SERVICE.Respond;
using MPM365SERVICE.Service.MPMMuliaService;
//using MPM365SERVICE.MPMMuliaService;
//using MPM365SERVICE.Respond;
//using MPM365SERVICE.Service.MPMMuliaService;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InventTransferTable;
using MPMWMSMODEL.Table.MutationSite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class MutationSiteModel : PDTUnitModel
    {
        private MutationSiteObject _MutationSiteObject = new MutationSiteObject();
        private InventTransferTableObject _InventTransferTableObject = new InventTransferTableObject();
        public StoringVendorModel _StoringVendorModel = new StoringVendorModel();
        public MutationSiteModel()
            : base()
        {
            _MutationSiteObject.Context = (WmsUnitDataContext)Context;
            _MutationSiteObject.MutationSiteLineObject.Context = _MutationSiteObject.Context;

            _InventTransferTableObject.Context = (WmsUnitDataContext)Context;
        }

        public MutationSiteObject MutationSiteObject
        {
            get { return _MutationSiteObject; }
        }

        public InventTransferTableObject InventTransferTableObject
        {
            get { return _InventTransferTableObject; }
        }

        public List<MPMWMS_MUTATION_SITE> Document(String dataarea_id, String site_id)
        {
            return MutationSiteObject.ListPosted(dataarea_id, site_id);
        }

        // update anas
        public String GetNosin(String dataarea_id, String qrcode)
        {
            try
            {
                MPMWMS_UNIT unit = UnitObject.GetNosinFromQr(dataarea_id, qrcode);
                if (unit != null)
                {
                    return unit.MACHINE_ID;
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DoReceivePart(String mutation_id, String dataarea_id, String machine_id)
        {
            try
            {
                BeginTransaction();

                ////proses scan by qr code
                ////tambahana anas merubah scan menggunakan QR code
                //String QRCode = "";
                //String qrcode = "";
                //QRCode = machine_id;
                //char[] spearator = { '/' };
                //String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                //qrcode = strlist[strlist.Length - 1];


                //MPMWMS_UNIT unit = UnitObject.GetNosinFromQr(dataarea_id, qrcode);
                //if (unit == null)
                //{
                //    throw new MPMException("Can't find unit !!!");
                //}
                ////end of scan by qr code

                //scan by nosin
                //komen anas
                MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_id);
                //tambahan anas

                MPMWMS_MUTATION_SITE mutation = MutationSiteObject.Item(dataarea_id, mutation_id);
                if (mutation == null)
                {
                    throw new MPMException("Mutation not found !!!");
                }

                MPMWMS_MUTATION_SITE_LINE mutation_line = MutationSiteObject.MutationSiteLineObject.Item(dataarea_id, mutation_id, unit.MACHINE_ID, unit.FRAME_ID);
                if (mutation_line == null)
                {
                    throw new MPMException("Mutation Line not found !!!");
                }

                // update stock at location source (-1) and location intransit (+1)
                UpdateLocationStock(dataarea_id, mutation_line.SITE_ID_INTR, mutation_line.WAREHOUSE_ID_INTR, mutation_line.LOCATION_ID_INTR, -1);
                UpdateLocationStock(dataarea_id, mutation_line.SITE_ID_NEW, mutation_line.WAREHOUSE_ID_NEW, mutation_line.LOCATION_ID_NEW, 1);

                // update current unit intransit location to receive another site      
                unit.IS_READYTOPICK_AX = "N";
                unit.SITE_ID = mutation_line.SITE_ID_NEW;
                unit.WAREHOUSE_ID = mutation_line.WAREHOUSE_ID_NEW;
                unit.LOCATION_ID = mutation_line.LOCATION_ID_NEW;
                UnitObject.Update(unit, "MTS", mutation_line.SITE_ID_INTR, mutation_line.WAREHOUSE_ID_INTR, mutation_line.LOCATION_ID_INTR);
                mutation_line.STATUS = "R";

                // send to web service
                //xtsMuliaService.xtsTransferInventServiceClient client = ConnectWebService();
                //client.PostTransferOrderReceive(ContextWS, mutation.TRANSFER_ID, unit.UNIT_TYPE);
                //DisconnectWebService(client);
                InventoryService invent = new InventoryService();
                var resultAX = new ResultCommon();
                MPMInventoryParm[] param1;
                param1 = new MPMInventoryParm[1];
                param1[0] = new MPMInventoryParm();
                param1[0].parmTransferId = mutation.TRANSFER_ID;
                param1[0].parmItemId = unit.UNIT_TYPE;
                param1[0].parmQty = 1;
                param1[0].parmTransDate = DateTime.Now;
                param1[0].parmMachineNo = unit.MACHINE_ID;
                resultAX = invent.postTransferOrderReceive(param1,UserSession.DATAAREA_ID);
                if (resultAX.Message != "" && resultAX.Success == 0)
                {
                    int qtyreceived = MutationSiteObject.MutationSiteLineObject.ValidasiQtyReceived(mutation_line.MACHINE_ID, mutation_line.TRANSFER_ID);
                    if (qtyreceived > 0) {

                    }
                    else {
                        throw new Exception(resultAX.Message);
                    }
                }
                //invent.createInventTransferOrder;

                MutationSiteObject.MutationSiteLineObject.Update(mutation_line);
                Commit();
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void UpdateStatusLine(String mutation_id, String dataarea_id, String status)
        {
            try
            {
                MPMWMS_MUTATION_SITE mutation = MutationSiteObject.Item(dataarea_id, mutation_id);
                if (mutation == null)
                {
                    throw new MPMException("Mutation not found !!!");
                }

                List<MPMWMS_MUTATION_SITE_LINE> lines = MutationSiteObject.MutationSiteLineObject.List(dataarea_id, mutation_id);
                foreach (var line in lines)
                {
                    line.STATUS = status;
                }
                mutation.STATUS = "FR";
                MutationSiteObject.Update(mutation);
                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void PostReceive(String mutation_id, String dataarea_id)
        {
            try
            {
                MPMWMS_MUTATION_SITE mutation = MutationSiteObject.Item(dataarea_id, mutation_id);
                if (mutation == null)
                {
                    throw new MPMException("Mutation not found !!!");
                }

                List<MPMWMS_MUTATION_SITE_LINE> lines = MutationSiteObject.MutationSiteLineObject.List(dataarea_id, mutation_id);
                int count = 0;
                foreach (var line in lines)
                {
                    if (line.STATUS.Equals("R")) count++;
                }
                if (count == lines.Count())
                {
                    mutation.STATUS = "FR";
                }
                else
                {
                    mutation.STATUS = "HR";
                }
                MutationSiteObject.Update(mutation);
                SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

       
        // PDT Movement Site Receive
        //tambahan anas
        public void ProsesTrackingQRnoWSMutationReceive(String mutation_id, String dataarea_id, String machine_or_frame_id)
        {
            try
            {
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT unit = UnitObject.GetNosinFromQr(dataarea_id, qrcode);

                if (unit != null)
                {
                    //if (unit.SITE_ID != UserSession.SITE_ID_MAPPING)
                    //{
                    //    throw new MPMException("Unit Tidak Ditemukan");
                    //}

                    var select = (from a in ((WmsUnitDataContext)Context).MPMViewINVENTSITEs
                                  where a.SITEID == unit.SITE_ID && a.DATAAREAID == unit.DATAAREA_ID
                                  select a).Take(1).FirstOrDefault();

                    //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewtypecolor = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    MPMSERVVIEWDAFTARUNITWARNA viewtypecolor = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    
                    unit.QR_CODE = qrcode;
                    //updatemappingqrcode(unit, qrcode, machine_or_frame_id);

                    //tambahan anas
                    MPMWMS_UNIT unit1 = UnitObject.GetNosinFromQr(dataarea_id, qrcode);
                    if (unit1 == null)
                    {
                        throw new MPMException("Can't find unit !!!");
                    }
                    MPMWMS_MUTATION_SITE_LINE mutation_line = MutationSiteObject.MutationSiteLineObject.Item(dataarea_id, mutation_id, unit1.MACHINE_ID, unit1.FRAME_ID);
                    if (mutation_line == null)
                    {
                        throw new MPMException("Mutation Line not found !!!");
                    }
                    _StoringVendorModel.insertQRUnitMutationReceiveOut(mutation_line, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "9", "OUT");
                    _StoringVendorModel.insertQRUnitMutationReceiveIn(mutation_line, unit, select, qrcode, viewtypecolor.ITEMMARKETNAME, viewtypecolor.ITEMCOLORCODE, "9", "IN");
                }
                else
                {
                    throw new MPMException("Unit Tidak Ditemukan");
                }
            }
            catch (MPMException ex)
            {

                throw new MPMException(ex.Message);
            }
        }

        //tambahan anas 
        //validasi scan unit (receiving) 
        public void ValidasiReceive(String mutation_id, String dataarea_id, String machine_id)
        {
            try {
                ////proses scan by qr code
                //String QRCode = "";
                //String qrcode = "";
                //QRCode = machine_id;
                //char[] spearator = { '/' };
                //String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                //qrcode = strlist[strlist.Length - 1];

                ////qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                //MPMWMS_UNIT unit = UnitObject.GetNosinFromQr(dataarea_id, qrcode);
                //if (unit == null)
                //{
                //    throw new MPMException("Can't find unit !!!");
                //}
                ////end of proses scan qr code

                //proses scan by nosin
                MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, machine_id);
                if (unit == null)
                {
                    throw new MPMException("Can't find unit !!!");
                }
                //end of proses scan by nosin

                MPMWMS_MUTATION_SITE_LINE mutation_line = MutationSiteObject.MutationSiteLineObject.Item1(dataarea_id, mutation_id, unit.MACHINE_ID, unit.FRAME_ID);
                if (mutation_line != null)
                {
                    throw new MPMException("Unit sudah di scan / 'STATUS' unit sudah 'R' ");
                }
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }

        }
    }
}
