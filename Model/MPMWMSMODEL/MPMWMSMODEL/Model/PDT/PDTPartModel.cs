﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Services;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.MPMWMSPartService;
using MPMWMSMODEL.Table.InventLocation;
using MPMWMSMODEL.Table.Setting;
using MPMWMSMODEL.View.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class PDTPartModel: BaseModel
    {
        protected String ServiceGroup = "MPMWMSPartService";

        protected CallContext ContextWS = new CallContext();

        private InventLocationObject _InventLocationObject = new InventLocationObject();
        private ProductViewObject _ProductViewObject = new ProductViewObject();
        private SettingDetailObject _SettingDetailObject = new SettingDetailObject();

        public PDTPartModel()
            : base()
        {
            _InventLocationObject.Context = (WmsUnitDataContext)Context;
            _ProductViewObject.Context = (WmsUnitDataContext)Context;
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;
        }

        public InventLocationObject InventLocationObject
        {
            get { return _InventLocationObject; }
        }

        public ProductViewObject ProductViewObject
        {
            get { return _ProductViewObject; }
        }

        /** WEB SERVICE **/
        protected MPMWMSPartService.InventJournalTableServiceClient ConnectWebService()
        {
            try
            {
                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpMPMWMSPartService, LogonASUser);

                MPMWMSPartService.InventJournalTableServiceClient client = new MPMWMSPartService.InventJournalTableServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebService(MPMWMSPartService.InventJournalTableServiceClient client)
        {
            client.Close();
            client = null;
        }

        protected MPMWMSPartService.SalesPackingSlipServiceClient ConnectWebServiceSales()
        {
            try
            {
                try
                {
                    MPM_SETTING_DETAIL item = _SettingDetailObject.Item("AX_WS", "WMS_UNIT", "WMS_UNIT");
                    if (item != null)
                    {
                        UrlNetTcpXtsMuliaService = item.TAG_1;

                    }

                    item = _SettingDetailObject.Item("AX_WS", "WMS_PART", "WMS_PART");
                    if (item != null)
                    {
                        UrlNetTcpMPMWMSPartService = item.TAG_1;

                    }

                    item = _SettingDetailObject.Item("AX_WS", "USERNAME", "PASSWORD");
                    if (item != null)
                    {
                        LogonASUser = item.TAG_3 + "\\" + item.TAG_1;
                        DomainWS = item.TAG_3;
                        UsernameWS = item.TAG_1;
                        PasswordWS = item.TAG_2;
                    }
                }
                catch (MPMException e)
                {
                    throw new MPMException(e.Message);
                }

                ContextWS.Company = UserSession.DATAAREA_ID;
                ContextWS.LogonAsUser = LogonASUser;
                ContextWS.Language = "en-us";
                System.ServiceModel.EndpointAddress xRemote = MPMAIFConnection.getRemoteAddress(ServiceGroup,
                    UrlNetTcpMPMWMSPartService, LogonASUser);

                System.ServiceModel.NetTcpBinding bind = new System.ServiceModel.NetTcpBinding();
                bind.Security.Mode = System.ServiceModel.SecurityMode.Transport;
                bind.Security.Message.ClientCredentialType = System.ServiceModel.MessageCredentialType.Windows;

                MPMWMSPartService.SalesPackingSlipServiceClient client = new MPMWMSPartService.SalesPackingSlipServiceClient(bind, xRemote);
                client.ClientCredentials.Windows.ClientCredential.Domain = DomainWS;
                client.ClientCredentials.Windows.ClientCredential.UserName = UsernameWS;
                client.ClientCredentials.Windows.ClientCredential.Password = PasswordWS;
                client.Open();

                return client;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        protected void DisconnectWebServiceSales(MPMWMSPartService.SalesPackingSlipServiceClient client)
        {
            client.Close();
            client = null;
        }
        /** END OF WEB SERVICE **/
    }
}
