﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.QCUnitNRFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class QCUnitNRFS_Model : PDTUnitModel
    {
        public QCUnitNRFS_Obj _objQCUnit = new QCUnitNRFS_Obj();

        public QCUnitNRFS_Model()
            : base()
        {
            _objQCUnit.Context = (WmsUnitDataContext)Context;
        }

        public void updateUnit (MPMWMS_UNIT item)
        {
            BeginTransaction();
            try
            {
                var row = _objQCUnit.ItemUnit(item.MACHINE_ID);

                if(row !=null)
                {
                    row.MACHINE_ID = item.MACHINE_ID;
                    row.STORING_DATE = row.SPG_DATE;
                }
                else
                {
                    throw new MPMException("Data Tidak Ada");
                }
                _objQCUnit.updateUnit(row);
                Commit();

            }catch(MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }

        public void updateHeader(MPMWMSHISTCLOCKING a)
        {
            BeginTransaction();
            try
            {
                var row = _objQCUnit.ItemGridHeader(a.NOMESIN);
                if (row != null)
                {
                    row.NOMESIN = a.NOMESIN;
                    row.STATUSQC = a.STATUSQC;
                    row.MODIFBY = a.MODIFBY;
                    row.MODIFDATE = a.MODIFDATE;
                    row.PICQC = a.PICQC;
                    row.TGLSTATUSQC = a.TGLSTATUSQC;

                }
                else
                {
                    throw new MPMException("tidak ada");
                }
                _objQCUnit.UpdateHeader(row);
                Commit();

            }
            catch (MPMException E)
            {
                Rollback();
                throw new MPMException(E.Message);
            }
        }
    }
}
