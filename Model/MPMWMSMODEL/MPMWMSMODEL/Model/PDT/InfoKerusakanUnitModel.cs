﻿using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InfoKerusakanUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
   public class InfoKerusakanUnitModel : PDTUnitModel
    {
       
            public InfoKerusakanUnit_Obj _objInfoKerusakan = new InfoKerusakanUnit_Obj();

            public InfoKerusakanUnitModel()
                : base()
            {
                _objInfoKerusakan.Context = (WmsUnitDataContext)Context;
            }
    }
    
}
