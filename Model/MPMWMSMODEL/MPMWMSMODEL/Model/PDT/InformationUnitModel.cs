﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record;
using MPMWMSMODEL.Table.Picking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Table.InventJournalTable;
using MPMWMSMODEL.Table.MutationSite;

namespace MPMWMSMODEL.Model.PDT
{
    public class InformationUnitModel : PDTUnitModel
    {
        private PickingObject _PickingObject = new PickingObject();
        private InventJournalTableObject _InventJournalTableObject = new InventJournalTableObject();
        private MutationSiteObject _MutationSiteObject = new MutationSiteObject();

        public InformationUnitModel()
            : base()
        {
            _PickingObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.PickingLineObject.Context = (WmsUnitDataContext)Context;
            _MutationSiteObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.Context = (WmsUnitDataContext)Context;
        }

        public MutationSiteObject MutationSiteObject
        {
            get { return _MutationSiteObject; }
        }

        public PickingObject PickingObject
        {
            get { return _PickingObject; }
        }

        public InventJournalTableObject InventJournalTableObject
        {
            get { return _InventJournalTableObject; }
        }

        public MPMWMS_UNIT_Record ItemUnit(String machine_or_frame_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                MPMWMS_UNIT_Record unit = new MPMWMS_UNIT_Record();
                
                //proses information unit by qrcode
                String QRCode = "";
                String qrcode = "";
                QRCode = machine_or_frame_id;
                char[] spearator = { '/' };
                String[] strlist = QRCode.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                qrcode = strlist[strlist.Length - 1];

                //qrcode = QRCode.Substring(QRCode.Length - 32, 32);

                if (QRCode == "")
                {
                    throw new MPMException("Belum Scan QR Code");
                }

                MPMWMS_UNIT row = UnitObject.ItemByQR(dataarea_id, qrcode, site_id);
                

                //information unit by nosin
                //MPMWMS_UNIT row = UnitObject.Item(dataarea_id, machine_or_frame_id); //--> Modif By Anong 9 / 6 / 2019 Ket: Untuk QRCODE

                if (row != null)
                {
                    unit.MACHINE_ID = row.MACHINE_ID;
                    unit.FRAME_ID = row.FRAME_ID;
                    unit.UNIT_TYPE = row.UNIT_TYPE;
                    
                    unit.UNIT_YEAR = row.UNIT_YEAR;
                    unit.COLOR_TYPE = row.COLOR_TYPE;
                    unit.SITE_ID = row.SITE_ID;
                    unit.WAREHOUSE_ID = row.WAREHOUSE_ID;
                    unit.LOCATION_ID = row.LOCATION_ID;
                    unit.SL_ID = row.SL_ID;
                    unit.SL_DATE = (row.SL_DATE != null ? row.SL_DATE.Value.ToString("dd-MM-yyyy") : "");
                    unit.SPG_ID = row.SPG_ID;

                    //unit.UNIT_DESC = EcoResProductObject.Item(unit.UNIT_TYPE).XTSINVTYPEDESCR;
                    MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                    if (viewProduct == null)
                    {
                        unit.UNIT_DESC = "";
                    }
                    else
                    {
                        unit.UNIT_DESC = viewProduct.ITEMNAME;
                    }
                    INVENTJOURNALTABLE invent = InventJournalTableObject.Item(row.DATAAREA_ID, row.SPG_ID);
                    if (invent != null)
                    {
                        unit.SPG_POLICE_NUMBER = invent.XTSPLATENO;
                        unit.SPG_DRIVER = invent.XTSDRIVERNAME;
                    }
                    unit.SPG_DATE = (row.SPG_DATE != null ? row.SPG_DATE.Value.ToString("dd-MM-yyyy") : "");
                    unit.STORING_DATE = (row.STORING_DATE != null ? row.STORING_DATE.Value.ToString("dd-MM-yyyy") : "");
                    unit.DO_ID = row.DO_ID;
                    unit.DO_DATE = (row.DO_DATE != null ? row.DO_DATE.Value.ToString("dd-MM-yyyy") : "");
                    unit.PICKING_ID = row.PICKING_ID;
                    unit.PICKING_DATE = (row.PICKING_DATE != null ? row.PICKING_DATE.Value.ToString("dd-MM-yyyy") : "");
                    unit.SHIPPING_LIST_ID = row.SHIPPING_ID;
                    unit.SHIPPING_LIST_DATE = (row.SHIPPING_DATE != null ? row.SHIPPING_DATE.Value.ToString("dd-MM-yyyy") : "");
                    MPMWMS_PICKING picking = PickingObject.Item(row.PICKING_ID, dataarea_id, maindealer_id, site_id);
                    if (picking != null)
                    {
                        unit.DEALER_CODE = picking.DEALER_CODE;
                        unit.DEALER_NAME = picking.DEALER_NAME;
                        unit.SHIPPING_POLICE_NUMBER = picking.POLICE_NUMBER;
                        unit.SHIPPING_DRIVER = picking.DRIVER;
                    }
                    picking = null;

                    MPMWMS_MUTATION_SITE mutation = MutationSiteObject.LastItem(unit.MACHINE_ID);
                    if (mutation != null) {
                        unit.MUTATION_ID = mutation.MUTATION_ID;
                        unit.EXPEDITION_ID = mutation.EXPEDITION_ID;
                        unit.POLICE_NUMBER = mutation.POLICE_NUMBER;
                        unit.DRIVER = mutation.DRIVER;
                    }
                }
                //Add Row by Anong 9/6/2019 ket : Error 
                else
                {
                    throw new MPMException("Data Belum Terdaftar");
                }
                return unit;
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
