﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;

namespace MPMWMSMODEL.Model.PDT
{
    public class UnloadingUnitModel: PDTUnitModel
    {
        public class UNLOADING_UNIT_RECORD
        {
            public String POLICE_NUMBER { get; set; }
            public String DRIVER { get; set; }
            public String JOURNAL_ID { get; set; }
            public DateTime JOURNAL_DATE { get; set; }

            public UNLOADING_UNIT_RECORD(
                String police_number, 
                String driver,
                String journal_id,
                DateTime journal_date)
            {
                POLICE_NUMBER = police_number;
                DRIVER = driver;
                JOURNAL_ID = journal_id;
                JOURNAL_DATE = journal_date;
            }
        }

        public class UNLOADING_UNIT_POLICE_NUMBER_RECORD
        {
            public String POLICE_NUMBER { get; set; }
            public String DRIVER { get; set; }
            public DateTime JOURNAL_DATE { get; set; }

            public UNLOADING_UNIT_POLICE_NUMBER_RECORD(
                String police_number, 
                String driver,
                DateTime journal_date)
            {
                POLICE_NUMBER = police_number;
                DRIVER = driver;
                JOURNAL_DATE = journal_date;
            }
        }

        public UnloadingUnitModel()
            : base()
        {
        }

        private IQueryable<UNLOADING_UNIT_RECORD> RunQuery(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            return 
                from u in ((WmsUnitDataContext)Context).MPM_VIEW_UNLOAD_UNITs
                where
                    u.DATAAREA_ID == dataarea_id
                    && u.MAINDEALER_ID == maindealer_id
                    && u.SITE_ID == site_id
                select new UNLOADING_UNIT_RECORD(
                    u.POLICE_NUMBER,
                    u.DRIVER,
                    u.SPG_ID,
                    u.SPG_DATE
                );
        }

        private IQueryable<UNLOADING_UNIT_RECORD> RunQueryByIdStart(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            return
                from u in ((WmsUnitDataContext)Context).MPM_VIEW_UNLOAD_UNITs
                where
                    u.DATAAREA_ID == dataarea_id
                    && u.MAINDEALER_ID == maindealer_id
                    && u.SITE_ID == site_id
                    && u.UNLOAD_START_TIME == null
                    && u.POLICE_NUMBER + u.DRIVER + u.SPG_DATE.Day.ToString() + u.SPG_DATE.Month.ToString() + u.SPG_DATE.Year.ToString() == id
                select new UNLOADING_UNIT_RECORD(
                    u.POLICE_NUMBER,
                    u.DRIVER,
                    u.SPG_ID,
                    u.SPG_DATE
                );
        }

        private IQueryable<UNLOADING_UNIT_RECORD> RunQueryByIdStop(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            return
                from u in ((WmsUnitDataContext)Context).MPM_VIEW_UNLOAD_UNITs
                where
                    u.DATAAREA_ID == dataarea_id
                    && u.MAINDEALER_ID == maindealer_id
                    && u.SITE_ID == site_id
                    && u.UNLOAD_START_TIME != null
                    && u.UNLOAD_STOP_TIME == null
                    && u.POLICE_NUMBER + u.DRIVER + u.SPG_DATE.Day.ToString() + u.SPG_DATE.Month.ToString() + u.SPG_DATE.Year.ToString() == id
                select new UNLOADING_UNIT_RECORD(
                    u.POLICE_NUMBER,
                    u.DRIVER,
                    u.SPG_ID,
                    u.SPG_DATE
                );
        }

        private IQueryable<UNLOADING_UNIT_POLICE_NUMBER_RECORD> RunQueryGroupByPoliceNumberStart(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            return
                from u in ((WmsUnitDataContext)Context).MPM_VIEW_UNLOAD_UNITs
                where
                    u.DATAAREA_ID == dataarea_id
                    && u.MAINDEALER_ID == maindealer_id
                    && u.SITE_ID == site_id
                    && u.UNLOAD_START_TIME == null
                group u by new { u.POLICE_NUMBER, u.DRIVER, u.SPG_DATE } into g
                orderby g.Key.SPG_DATE ascending, g.Key.POLICE_NUMBER ascending
                select new UNLOADING_UNIT_POLICE_NUMBER_RECORD(
                    g.Key.POLICE_NUMBER,
                    g.Key.DRIVER,
                    g.Key.SPG_DATE
                );
        }

        private IQueryable<UNLOADING_UNIT_POLICE_NUMBER_RECORD> RunQueryGroupByPoliceNumberStop(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            return
                from u in ((WmsUnitDataContext)Context).MPM_VIEW_UNLOAD_UNITs
                where
                    u.DATAAREA_ID == dataarea_id
                    && u.MAINDEALER_ID == maindealer_id
                    && u.SITE_ID == site_id
                    && u.UNLOAD_START_TIME != null
                    && u.UNLOAD_STOP_TIME == null
                group u by new { u.POLICE_NUMBER, u.DRIVER, u.SPG_DATE } into g
                orderby g.Key.SPG_DATE ascending, g.Key.POLICE_NUMBER ascending
                select new UNLOADING_UNIT_POLICE_NUMBER_RECORD(
                    g.Key.POLICE_NUMBER,
                    g.Key.DRIVER,
                    g.Key.SPG_DATE
                );
        }

        public List<UNLOADING_UNIT_RECORD> List(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            IQueryable<UNLOADING_UNIT_RECORD> itemQry = RunQuery(dataarea_id, maindealer_id, site_id);
            return itemQry.ToList();
        }

        public List<UNLOADING_UNIT_POLICE_NUMBER_RECORD> ListPoliceNumberStart(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            List<UNLOADING_UNIT_POLICE_NUMBER_RECORD> list = RunQueryGroupByPoliceNumberStart(dataarea_id, maindealer_id, site_id).ToList();
            return list;
        }

        public List<UNLOADING_UNIT_POLICE_NUMBER_RECORD> ListPoliceNumberStop(
            String dataarea_id,
            String maindealer_id,
            String site_id)
        {
            List<UNLOADING_UNIT_POLICE_NUMBER_RECORD> list = RunQueryGroupByPoliceNumberStop(dataarea_id, maindealer_id, site_id).ToList();
            return list;
        }

        public List<UNLOADING_UNIT_RECORD> ListSPGStart(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            List<UNLOADING_UNIT_RECORD> list = 
                RunQueryByIdStart(dataarea_id, maindealer_id, site_id, id).ToList();
            return list;
        }

        public List<UNLOADING_UNIT_RECORD> ListSPGStop(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            List<UNLOADING_UNIT_RECORD> list =
                RunQueryByIdStop(dataarea_id, maindealer_id, site_id, id).ToList();
            return list;
        }

        public void SetUnloadStart(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            List<UNLOADING_UNIT_RECORD> list =
                RunQueryByIdStart(dataarea_id, maindealer_id, site_id, id).ToList();

            foreach (var a in list)
            {
                ((WmsUnitDataContext)Context).MPM_SP_UPDATE_UNLOAD_UNIT(dataarea_id, site_id, a.JOURNAL_ID, "START");
            }
        }

        public void SetUnloadStop(
            String dataarea_id,
            String maindealer_id,
            String site_id,
            String id)
        {
            List<UNLOADING_UNIT_RECORD> list =
                RunQueryByIdStop(dataarea_id, maindealer_id, site_id, id).ToList();

            foreach (var a in list)
            {
                ((WmsUnitDataContext)Context).MPM_SP_UPDATE_UNLOAD_UNIT(dataarea_id, site_id, a.JOURNAL_ID, "STOP");
            }
        }
    }
}
