﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InventJournalTable;
using MPMWMSMODEL.Table.StoringPart;
using MPMWMSMODEL.Table.StroringPart;
using MPMWMSMODEL.View.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model.PDT
{
    public class StoringPartModel: PDTPartModel
    {
        InventJournalTableObject _InventJournalTableObject = new InventJournalTableObject();
        public StoringCheckInOutObj _StroingCheckInOutObj = new StoringCheckInOutObj();
        public ReserveQtyStoringObj _ReserveQtyStoringObj = new ReserveQtyStoringObj();
        public TemporaryStoringPartObj _TemporaryStoringPartObj = new TemporaryStoringPartObj();
        public StoringPartModel()
            : base()
        {
            _ReserveQtyStoringObj.Context = (WmsUnitDataContext)Context;
            _StroingCheckInOutObj.Context = (WmsUnitDataContext)Context;
            _TemporaryStoringPartObj.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.Context = (WmsUnitDataContext)Context;
            _InventJournalTableObject.InventJournalTransObject.Context = _InventJournalTableObject.Context;
        }

        public InventJournalTableObject InventJournalTableObject
        {
            get { return _InventJournalTableObject; }
        }

        public String DoStoring(String no_spg)
        {
            try
            {
                String result = "";
                MPMWMSPartService.InventJournalTableServiceClient client = ConnectWebService();
                result = client.PartStoring(ContextWS, UserSession.MAINDEALER_ID, "P", no_spg, MPMDateUtil.DateTime);
                DisconnectWebService(client);

                return result;
            }
            catch (MPMException)
            {
                return null;
            }
        }

       



        public String DoStoringLine(String receiving_id, String item_id,
            String fromInventDimIdRcv, String toInventDimIdRcv,
            String site_id_to, String location_id_to, int quantity, String journal_id_storing)
        {
            try
            {
                // validation
                if (receiving_id.Equals(""))
                {
                    throw new MPMException("Receiving Id not found !!!");
                }

                if (item_id.Equals(""))
                {
                    throw new MPMException("Part Id not found !!!");
                }

                if (fromInventDimIdRcv.Equals(""))
                {
                    throw new MPMException("Rack Id Source not found !!!");
                }

                if (toInventDimIdRcv.Equals(""))
                {
                    throw new MPMException("Rack Id Destination not found !!!");
                }

                if (site_id_to.Equals(""))
                {
                    throw new MPMException("Site Id Destination not found !!!");
                }

                if (location_id_to.Equals(""))
                {
                    throw new MPMException("Rack Id Destination not found !!!");
                }

                if (quantity <= 0)
                {
                    throw new MPMException("Quantity must greater than 0 !!!");
                }

                // find location to is already exist or not
                //if (InventLocationObject.Item(UserSession.DATAAREA_ID, site_id_to, site_id_to + location_id_to) == null)
                //{
                //    throw new MPMException("Rack Not Found !!!");
                //} dirubah karena tidak perlu ada site_id_to+location_id_to
                if (InventLocationObject.Item(UserSession.DATAAREA_ID, site_id_to, location_id_to) == null)
                {
                    throw new MPMException("Rack Not Found !!!");
                }
                MPMWMSPartService.InventJournalTableServiceClient client = ConnectWebService();
                /*bool result = client.PartStoringLine(
                    ContextWS,
                    journal_id,
                    receiving_id,
                    item_id,
                    fromInventDimIdRcv,
                    toInventDimIdRcv,
                    site_id_to,
                    site_id_to + location_id_to,
                    quantity);*/
                String result = client.PostStroingPart(
                    ContextWS,
                    UserSession.MAINDEALER_ID,
                    "P",
                    receiving_id,
                    MPMDateUtil.DateTime,
                    item_id,
                    fromInventDimIdRcv,
                    toInventDimIdRcv,
                    site_id_to,
                    //site_id_to + location_id_to, saya komen karena di table hanya membutuh location id saja 
                    location_id_to,
                    quantity,
                    journal_id_storing);
                DisconnectWebService(client);

                return result;
            }
            catch (Exception e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void PostStoring(String storing_id)
        {
            try
            {
                MPMWMSPartService.InventJournalTableServiceClient client = ConnectWebService();
                client.PostTransferJournal(ContextWS, storing_id);
                DisconnectWebService(client);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
