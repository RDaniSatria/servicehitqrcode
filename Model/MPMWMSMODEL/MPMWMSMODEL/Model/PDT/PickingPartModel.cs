﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.PickingPart;
using MPMWMSMODEL.Table.SalesTableAVPick;
using MPMWMSMODEL.Table.TroliPart;
using MPMWMSMODEL.Table.WmsPickingRoute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.Query;
using MPMWMSMODEL.Table.MapPicking;
using MPMWMSMODEL.Table.InventDim;
using MPMWMSMODEL.Table.SOPICAreaHistory;
using MPMWMSMODEL.Table.SOPICArea;
using MPMWMSMODEL.Record.PICAreaHistory;
using MPMWMSMODEL.Table.Setting;

namespace MPMWMSMODEL.Model.PDT
{
    public class PickingPartModel: PDTPartModel
    {
        public static int EXPEDITION_STATUS_PICKING = 10;

        private TroliPartObject _TroliPartObject = new TroliPartObject();
        private SalesTableAVPickObject _SalesTableAVPickObject = new SalesTableAVPickObject();
        public QryPickingJobPIC qryPickingJobPIC = new QryPickingJobPIC();
        public MapPickingObject _mapPickingObject = new MapPickingObject();
        public MapPicHistoryObject _mapPicHistoryObject = new MapPicHistoryObject();


        private WmsPickingRouteObject _WmsPickingRouteObject = new WmsPickingRouteObject();
        private InventDimObject _InventDimOBject = new InventDimObject();
        private SOPICAreaHistoryObject _SOPICAreaHistoryObject = new SOPICAreaHistoryObject();
        private SOPICAreaObject _SOPICAreaObject = new SOPICAreaObject();
        private QryPickingJobPIC _QryPickingJobPIC = new QryPickingJobPIC();
        private SettingDetailObject _SettingDetailObject = new SettingDetailObject();

        private String pickingIdOld = "";

        public PickingPartModel()
            : base()
        {
            _TroliPartObject.Context = (WmsUnitDataContext)Context;
            _SalesTableAVPickObject.Context = (WmsUnitDataContext)Context;
            _mapPickingObject.Context = (WmsUnitDataContext)Context;
            _mapPicHistoryObject.Context = (WmsUnitDataContext)Context;
            
            _WmsPickingRouteObject.Context = (WmsUnitDataContext)Context;
            _WmsPickingRouteObject.WmsOrderTransObject.Context = _WmsPickingRouteObject.Context;
            _WmsPickingRouteObject.WmsOrderTransObject.InventDimObject.Context = _WmsPickingRouteObject.Context;

            _InventDimOBject.Context = (WmsUnitDataContext)Context;
            _SOPICAreaHistoryObject.Context = (WmsUnitDataContext)Context;
            _SOPICAreaObject.Context = (WmsUnitDataContext)Context;
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;

        }

        public SalesTableAVPickObject SalesTableAVPickObject
        {
            get { return _SalesTableAVPickObject; }
        }
        
        public TroliPartObject TroliPartObject
        {
            get { return _TroliPartObject; }
        }

        public WmsPickingRouteObject WmsPickingRouteObject
        {
            get { return _WmsPickingRouteObject; }
        }

        /**
         * Update DO AV Pick
         * mode = 1 (ADD)
         * mode = 0 (SUB)
         **/
        private void UpdateDOAVPick(String dataarea_id, String maindealer_id, String site_id, 
            String sales_id, int quantity, int quantity_available, int mode)
        {
            MPMWMS_DO_AVPICK do_avpick = SalesTableAVPickObject.Item(sales_id, dataarea_id, maindealer_id, site_id);
            if (do_avpick != null)
            {
                if (mode == 1)
                {
                    do_avpick.QUANTITY_PICK = (do_avpick.QUANTITY_PICK - quantity_available) + quantity;
                }
                else if (mode == 0)
                {
                    do_avpick.QUANTITY_PICK = (do_avpick.QUANTITY_PICK - quantity) + quantity_available;
                }
                SalesTableAVPickObject.Update(do_avpick);
            }
            else
            {
                throw new MPMException("Can't update data DO Quantity Pick");
            }
        }
           

        // =======================================================================================


        /** AX **/
        public void InsertPickingRoute(String dataarea_id, String route_id, String pic)
        {
            WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataarea_id, route_id);
            if (route == null)
            {
                throw new MPMException("Route Picking not found !!!");
            }

            route.XTSLOCKEDUSERID = pic;
            SubmitChanges();
        }

        public void UpdatePickingRoute(String dataarea_id, String route_id, String pic, String troli_id)
        {
            WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataarea_id, route_id, troli_id);
            if (route == null)
            {
                route = WmsPickingRouteObject.Item(dataarea_id, route_id);
                if (route == null)
                {
                    throw new MPMException("Route Picking not found !!!");

                }
            }

            route.XTSLOCKEDUSERID = pic;
            route.XTSTROLLEYNBR = troli_id;
            SubmitChanges();
        }

        public void DeletePickingPartAX(String dataarea_id, String route_id)
        {
            WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataarea_id, route_id);
            if (route == null)
            {
                throw new MPMException("Route Picking not found !!!");
            }

            route.XTSLOCKEDUSERID = "";
            SubmitChanges();
        }

        /**
         * comment by Ardy 3 Feb 2017 ~ karena table MPMWMS_MAP_PIC_WAREHOUSE diganti dengan yang baru
        public void TransporterPickJob(string dataareaid, string journalid, string pic, string color)
        {
            WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataareaid, journalid);
            if (route == null)
            {
                throw new MPMException("Route picking not found !!!");
            }
            try
            {
                BeginTransaction();
                
                List<WMSORDERTRAN> details = WmsPickingRouteObject.WmsOrderTransObject.List(route);
                foreach (var row in details)
                {
                    // CEK BERDASARKAN WARNA
                    var colors =
                        from m in ((WmsUnitDataContext)Context).MPMWMS_MAP_PIC_COLORs
                        join p in ((WmsUnitDataContext)Context).MPMWMS_MAP_PIC_WAREHOUSEs on
                            new { N = m.PIC } equals new { N = p.NPK } into join_mp
                        from result_p in join_mp
                        join d in ((WmsUnitDataContext)Context).INVENTDIMs on
                            new { LOC = result_p.WAREHOUSEID } equals new { LOC = d.INVENTLOCATIONID } into join_pd
                        from result_d in join_pd
                        where
                            m.COLOR == color && result_d.INVENTDIMID == row.INVENTDIMID
                        select new { m.COLOR, result_p.WAREHOUSEID };

                    if (colors.Count() > 0)
                    {
                        // ganti jangan menggunakan variable MPMSTATUSPICKING
                        if (row.MPMSTATUSPICKING == 1) row.MPMSTATUSTRANSPORTER = 1;
                    }
                }
                Commit();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
        **/

        /**
         * create by juko
         **/
        public void DeletePickingPartAX(String dataarea_id, String route_id, String troli_id)
        {
            WMSPICKINGROUTE route = WmsPickingRouteObject.Item(dataarea_id, route_id, troli_id);
            if (route == null)
            {
                throw new MPMException("Route Picking not found !!!");
            }

            route.XTSLOCKEDUSERID = "";
            route.XTSTROLLEYNBR = "";
            SubmitChanges();
        }

        public void SetTrolleyPicking(String dataarea_id, String pic, String trolley_id)
        {
            List<WMSPICKINGROUTE> routes = WmsPickingRouteObject.List(dataarea_id, pic);
            foreach (var route in routes.ToList())
            {
                route.XTSTROLLEYNBR = trolley_id;
            }
            SubmitChanges();
        }

        public bool HasAnotherPartIdAX(String dataarea_id, String part_id, String pic, String trolley_id)
        {
            List<WMSPICKINGROUTE> routes = WmsPickingRouteObject.List(dataarea_id, pic, trolley_id);
            foreach (var route in routes.ToList()) 
            {
                foreach (var route_line in WmsPickingRouteObject.WmsOrderTransObject.List(route))
                {
                    if ((route_line.EXPEDITIONSTATUS != EXPEDITION_STATUS_PICKING) && (route_line.ITEMID.Equals(part_id)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void InsertPartAX(String dataarea_id, String route_id, String part_id, String invent_dim_id, int quantity, String rec_id)
        {
            try
            {
                BeginTransaction();

                WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(dataarea_id, route_id, part_id, invent_dim_id, 
                    Convert.ToInt64(rec_id));
                if (order == null)
                {
                    throw new MPMException("Part not found !!!");
                }
                else
                {     
                    if (order.EXPEDITIONSTATUS == 10 && order.INVENTDIMID.Equals(invent_dim_id))
                    {
                        throw new MPMException("Part already picked !!!");
                    }

                    // if quantity overquota
                    if (order.QTY + order.XTSUNPICKQTY < quantity)
                    {
                        throw new MPMException("Quantity overloaded !!!");
                    }

                    // update order trans
                    decimal qty_awal = order.XTSUNPICKQTY + order.QTY;
                    order.XTSUNPICKQTY = qty_awal - quantity;

                    //order.XTSUNPICKQTY = order.QTY - quantity;
                    order.QTY = quantity;
                    order.MPMSTATUSPICKING = 1;
                    //order.EXPEDITIONSTATUS = EXPEDITION_STATUS_PICKING;

                    INVENTDIM dim = _InventDimOBject.Item(order.DATAAREAID, order.INVENTDIMID);
                    WMSPICKINGROUTE route = _WmsPickingRouteObject.Item(order.DATAAREAID, order.ROUTEID);
                    MPM_SETTING_DETAIL setting_detail = _SettingDetailObject.Item("WORK_HOUR", UserSession.DATAAREA_ID, _SettingDetailObject.getEnumDay(DateTime.Today.DayOfWeek));

                    // insert to history pic
                    MPMWMS_MAP_PIC_HISTORY historyPic = _mapPicHistoryObject.Create();
                    historyPic.DATAAREAID = UserSession.DATAAREA_ID;
                    historyPic.SITEID = UserSession.SITE_ID_MAPPING;
                    //historyPic.WAREHOUSEID = (dim == null ? "" : dim.INVENTLOCATIONID);
                    historyPic.SOID = route.TRANSREFID;
                    historyPic.PICKINGID = order.ROUTEID;
                    historyPic.PIC = UserSession.NPK;
                    historyPic.WORKHOURPICKING = Convert.ToInt16(setting_detail.TAG_1);
                    historyPic.RESTHOURPICKING = Convert.ToInt16(setting_detail.TAG_2);

                    Commit();
                }
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void RemovePartAX(String dataarea_id, String route_id, String part_id, String invent_dim_id, String rec_id)
        {
            try
            {
                BeginTransaction();

                WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(dataarea_id, route_id, part_id,
                    invent_dim_id, Convert.ToInt64(rec_id));
                if (order == null)
                {
                    throw new MPMException("Part not found !!!");
                }
                else
                {
                    // update order trans
                    order.QTY = order.QTY + order.XTSUNPICKQTY;
                    order.XTSUNPICKQTY = 0;
                    order.MPMSTATUSPICKING = 0;
                    order.EXPEDITIONSTATUS = 3;

                    Commit();
                }
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Finish(String dataarea_id, String trolley_id, String pic)
        {
            try
            {
                MPMWMSPartService.SalesPackingSlipServiceClient client = ConnectWebServiceSales();

                List<WMSPICKINGROUTE> lists = WmsPickingRouteObject.List(dataarea_id, pic, trolley_id);
                foreach (var list in lists)
                {
                    if (list != null)
                    {
                        if (list.EXPEDITIONSTATUS == 3)
                        {
                            bool isRegSoDelivered = true;
                            bool submit = false;
                            List<WMSORDERTRAN> listTrans = WmsPickingRouteObject.WmsOrderTransObject.List(list);
                            foreach (var item in listTrans)
                            {
                                if (item != null)
                                {
                                    //pengecekan apakah akan memanggil RegSoDelivered
                                    if (!(item.XTSUNPICKQTY > 0 && item.QTY == 0))
                                    {
                                        isRegSoDelivered = false;
                                    }


                                    /* fungsi ini buat apa ya?
                                    if (item.MPMSTATUSPICKING < 2)
                                    {
                                        item.XTSUNPICKQTY = item.QTY;
                                        item.QTY = 0;
                                        submit = true;
                                    }
                                    */
                                }
                            }

                            /*if (list.PICKINGROUTEID != null)
                            {
                                MPMWMS_MAP_PICKING picking = _mapPickingObject.Item(list.PICKINGROUTEID);
                                if (picking != null)
                                {
                                    picking.JOURNALID = null;
                                    submit = true;
                                }
                            }*/

                            if (submit) SubmitChanges();

                            client.registrationAllPicking(ContextWS, list.PICKINGROUTEID);

                            if (isRegSoDelivered == true)
                                client.regSOToDelivered(ContextWS, list.TRANSREFID);
                        }
                    }
                }
                client = null;
            }
            catch (System.ServiceModel.FaultException<MPMWMSPartService.AifFault> exception)
            {
                String msg = "";

                foreach (var exceptionLog in exception.Detail.InfologMessageList)
                {
                    msg = msg + exceptionLog.Message;
                }
                throw new MPMException(msg);
            }
            catch (Exception ex)
            {
                throw new MPMException(ex.Message);
            }
        }
        /** END OF AX **/

        #region JOB PICKING TRANSACTION

        public void FinishJobPicking(String dataarea_id, String route_id, String part_id, String invent_dim_id, int quantity, String rec_id, int jobId)
        {
            try
            {
                BeginTransaction();

                WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(dataarea_id, route_id, part_id, invent_dim_id, Convert.ToInt64(rec_id));

                if (order == null)
                {
                    throw new MPMException("Part not found !!!");
                }
                else
                {
                    /* 9 maret 2017 ~ comment karena waktu SO confirm langsung terbentuk picking registration-nya jadi Completed !! */
                    //if (order.EXPEDITIONSTATUS == 10 && order.INVENTDIMID.Equals(invent_dim_id))
                    //{
                    //    throw new MPMException("Part already picked !!!");
                    //}

                    // if quantity overquota
                    if (order.QTY + order.XTSUNPICKQTY < quantity)
                    {
                        throw new MPMException("Quantity overloaded !!!");
                    }

                    // update order trans
                    decimal qty_awal = order.XTSUNPICKQTY + order.QTY;
                    order.XTSUNPICKQTY = qty_awal - quantity;

                    //order.XTSUNPICKQTY = order.QTY - quantity;
                    order.QTY = quantity;
                    order.MPMSTATUSPICKING = 1;
                    //order.EXPEDITIONSTATUS = EXPEDITION_STATUS_PICKING;

                    INVENTDIM dim = _InventDimOBject.Item(order.DATAAREAID, order.INVENTDIMID);
                    WMSPICKINGROUTE route = _WmsPickingRouteObject.Item(order.DATAAREAID, order.ROUTEID);

                    Commit();

                    InsertUpdateHistoryPIC(route.TRANSREFID, order.CUSTOMER, route.PICKINGROUTEID, jobId);

                }
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        void InsertUpdateHistoryPIC(string soId, string dealerId, string pickingId, int jobId)
        {
            try
            {
                BeginTransaction();

                MPMWMSMODEL.Query.QryPickingJobPIC query = new MPMWMSMODEL.Query.QryPickingJobPIC();

                MPMWMS_MAP_PIC_HISTORY historyPicData = _SOPICAreaHistoryObject.Item
                        (
                              soId
                            , dealerId
                            , pickingId
                            , UserSession.NPK
                            , UserSession.DATAAREA_ID
                            , UserSession.MAINDEALER_ID
                            , UserSession.SITE_ID_MAPPING
                        );

                if (historyPicData == null)
                {
                    MPM_SETTING_DETAIL setting_detail = _SettingDetailObject.Item("WORK_HOUR", UserSession.DATAAREA_ID, _SettingDetailObject.getEnumDay(DateTime.Today.DayOfWeek));

                    // insert to history pic
                    MPMWMS_MAP_PIC_HISTORY historyPic = new MPMWMS_MAP_PIC_HISTORY();

                    historyPic.SOID = soId;
                    historyPic.DEALERID = dealerId;
                    historyPic.PICKINGID = pickingId;
                    historyPic.PIC = UserSession.NPK;
                    historyPic.DATAAREAID = UserSession.DATAAREA_ID;
                    historyPic.MAINDEALERID = UserSession.MAINDEALER_ID;
                    historyPic.SITEID = UserSession.SITE_ID_MAPPING;
                    historyPic.STARTSCAN = MPMDateUtil.DateTime;
                    historyPic.JOBID = jobId;
                    historyPic.WORKHOURPICKING = Convert.ToInt16(setting_detail.TAG_1);
                    historyPic.RESTHOURPICKING = Convert.ToInt16(setting_detail.TAG_2);

                    _SOPICAreaHistoryObject.Insert(historyPic);

                    historyPicData = historyPic;
                }

                var itemObj = query.cekExistJobByJobId(UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING, jobId);

                // ini terjadi jika dalam 1 job ada lebih dari 1 SO, maka proses update ISDONEPICKING dilakukan per SO yang sudah selesai di PICKING
                // kondisinya adalah jika hasil query itemObj masih ada datanya tetapi no SO nya sudah berbeda dengan SO di MPMWMS_MAP_SO_PICAREA
                // maka dilakukan update set itemObj = null, agar bisa menjalankan code untuk update ENDSCAN

                if (itemObj != null && itemObj.SOId != soId)
                {
                    itemObj = null;
                }

                if (itemObj == null)
                {
                    // update end scan time pic
                    historyPicData.ENDSCAN = MPMDateUtil.DateTime;

                    _SOPICAreaHistoryObject.Update(historyPicData);

                    UpdatePickingDone(jobId, dealerId, soId);
                }
                

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        void UpdatePickingDone(int jobId, string dealerId, string soId)
        {
            MPMWMS_MAP_SO_PICAREA picAreaData = _SOPICAreaObject.Item
                    (
                          jobId
                        , UserSession.DATAAREA_ID
                        , UserSession.MAINDEALER_ID
                        , UserSession.SITE_ID_MAPPING
                        , dealerId
                        , soId
                    );

            if (picAreaData == null)
            {
                throw new MPMException("Data MAP SO PIC AREA is not valid !!!");
            }
            else
            {
                try
                {
                    //BeginTransaction();

                    _SOPICAreaObject.UpdateDonePicking(jobId);

                    //Commit();
                }
                catch (MPMException e)
                {
                    //Rollback();
                    throw new MPMException(e.Message);
                }
            }
        }

        #endregion

        #region TRANSPOTER TRANSACTION

        public List<MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPICGroup_Rec> loadDataGetPartTransporter
            (
                string dataareaid, 
                string maindealer, 
                string siteid, 
                string npk, 
                string trolley,
                string jobIdCurrent,
                Boolean getNextNewJob
            )
        {
            int xJobId = 0;

            try
            {
                var dataExist = _QryPickingJobPIC.cekExistPendingTrolley(dataareaid, maindealer, siteid, npk, trolley);

                if (dataExist != null && dataExist.Trolley != "" && dataExist.Trolley != trolley)
                {
                    throw new MPMException("Transporter " + dataExist.PickingUser + " terdapat Job pending untuk trolley " + dataExist.Trolley + " !!!");
                }

                var dataTransporter = _QryPickingJobPIC.ListGetPartTransporterGrouped(dataareaid, maindealer, siteid, npk, trolley, getNextNewJob);

                var dataJob = dataTransporter.OrderBy(x => x.StatusTransporter).ThenBy(x => x.JobId).ToList()
                                            .Select(data => new MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPICGroup_Rec
                                                (
                                                    data.JobId,
                                                    data.InitialLocation,
                                                    data.TrolleyIdCurrent,
                                                    data.StatusTransporter
                                                )).Take(1).ToList();

                foreach (MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPICGroup_Rec x in dataJob)
                {
                    xJobId = x.JobId;

                    if (jobIdCurrent == "" || Convert.ToInt32(jobIdCurrent) == x.JobId)
                    {
                        var dataPickingDone = _QryPickingJobPIC.ListPickingDoneByJobId(dataareaid, maindealer, siteid, npk, xJobId);

                        var dataPickingGrouping = dataPickingDone.GroupBy(t => new { t.PickingId })
                                                            .Select(data => new MPMWMSMODEL.Query.QryPickingJobPIC.SummaryPicking_Rec(data.Key.PickingId)).ToList();

                        foreach (MPMWMSMODEL.Query.QryPickingJobPIC.SummaryPicking_Rec z in dataPickingGrouping)
                        {
                            updateTrolleyUserInfo(dataareaid, z.PickingId, trolley, npk, maindealer, siteid);

                            // untuk update isi data history Transporter
                            MPMWMS_MAP_PIC_HISTORY historyPicData = _SOPICAreaHistoryObject.ItemByPicking
                            (
                                  z.PickingId
                                , UserSession.DATAAREA_ID
                                , UserSession.MAINDEALER_ID
                                , UserSession.SITE_ID_MAPPING
                            );

                            if ( historyPicData != null && 
                                (historyPicData.PICTRANSPORTER == null || historyPicData.STARTTAKE == null))
                            {
                                BeginTransaction();

                                historyPicData.PICTRANSPORTER = UserSession.NPK;
                                historyPicData.STARTTAKE = MPMDateUtil.DateTime;

                                _SOPICAreaHistoryObject.Update(historyPicData);

                                Commit();
                            }
                        }
                    }
                }

                var dataReturn = (  from x in dataTransporter
                                    where x.JobId == xJobId
                                    select x );

                return dataReturn.ToList();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void postingTransporter(String _dataarea, String _mainDealer, String _siteId, String _npk, string _trolleyId)
        {
            try
            {
                Boolean isNotComplete = false;
                String msgError = String.Empty;

                List<MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPIC_Rec> dataJob = _QryPickingJobPIC.ListPickingDoneByPIC(_dataarea, _mainDealer, _siteId, _npk);

                BeginTransaction();

                // cek apakah masih ada item part yang masih belum dilakukan proses TAKE, kalau belum maka tidak dapat melakukan proses POSTING
                if (dataJob.Where(z => z.Trolley == _trolleyId && z.StatusTransporter == 0 && z.Qty > 0).Count() > 0)
                {
                    throw new MPMException("Tidak dapat Posting!!! Masih ada item part yang belum dilakukan proses Take.");
                }

                var dataLoop = dataJob.Where(z => z.Trolley == _trolleyId && z.StatusTransporter == 1);

                // untuk cek apakah dalam 1 job tersebut masih ada yang belum dilakukan picking, karena maping PIC picking belum dilakukan !
                foreach (var x in dataLoop.Where(z => z.Qty > 0).OrderBy(x => x.JobId).GroupBy(z => z.JobId))
                {
                    // hitung apakah ada status transpoter = 0 dalam 1 job
                    if (!_QryPickingJobPIC.cekJobAllTranspoterDone(_dataarea, _mainDealer, _siteId, _npk, _trolleyId, x.Key))  
                    {
                        isNotComplete = true;
                        msgError += "Job " + x.Key + " tidak dapat diposting, karena masih ada item yang belum dilakukan picking!!!" + Environment.NewLine;
                    }
                }

                if (isNotComplete)
                {
                    throw new MPMException(msgError);
                }

                foreach (var x in dataLoop.OrderBy(x => x.JobId))
                {
                    WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(_dataarea, x.PickingId, x.ItemId, x.InventDimId, Convert.ToInt64(x.RecId));

                    if (order == null)
                    {
                        Rollback();

                        throw new MPMException("Part not found !!!");
                    }
                    else
                    {
                        /* 9 maret 2017 ~ comment karena waktu SO confirm langsung terbentuk picking registration-nya jadi Completed !! */
                        //if (order.EXPEDITIONSTATUS == 10 && order.INVENTDIMID.Equals(x.InventDimId))
                        //{
                        //    throw new MPMException("Part already picked !!!");
                        //}

                        order.MPMSTATUSPICKING = 2; // flag untuk transporter selesai

                        updateTransporterDone(x.JobId, order.CUSTOMER, order.INVENTTRANSREFID, order.ROUTEID, _npk, _trolleyId);
                    }
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void postTakePartJob(String _dataarea, List<MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPIC_Rec> _listPartCurrent)
        {
            try
            {
                BeginTransaction();

                foreach (var x in _listPartCurrent)
                {
                    WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(_dataarea, x.PickingId, x.ItemId, x.InventDimId, Convert.ToInt64(x.RecId));

                    if (order == null)
                    {
                        throw new MPMException("Part not found !!!");
                    }
                    else
                    {
                        /* 10 maret 2017 ~ validasi ini sudah tidak dipakai 
                         * karena saat confirm SO, AX akan sekaligus membentuk dokumen picking, maka dari itu EXPEDITIONSTATUS saat confirm SO sudah menjadi 10
                        if (order.EXPEDITIONSTATUS == 10 && order.INVENTDIMID.Equals(x.InventDimId))
                        {
                            throw new MPMException("Part already picked !!!");
                        }
                        */

                        order.MPMSTATUSTRANSPORTER = 1; // flag untuk transporter sudah mengambil (TAKE)
                    }
                } 

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void postReturnPartJob(String _dataarea, List<MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPIC_Rec> _listPartCurrent)
        {
            try
            {
                BeginTransaction();

                foreach (var x in _listPartCurrent)
                {
                    WMSORDERTRAN order = WmsPickingRouteObject.WmsOrderTransObject.ItemByRecId(_dataarea, x.PickingId, x.ItemId, x.InventDimId, Convert.ToInt64(x.RecId));

                    if (order == null)
                    {
                        throw new MPMException("Part not found !!!");
                    }
                    else
                    {
                        /* 10 maret 2017 ~ validasi ini sudah tidak dipakai 
                         * karena saat confirm SO, AX akan sekaligus membentuk dokumen picking, maka dari itu EXPEDITIONSTATUS saat confirm SO sudah menjadi 10
                        if (order.EXPEDITIONSTATUS == 10 && order.INVENTDIMID.Equals(x.InventDimId))
                        {
                            throw new MPMException("Part already picked !!!");
                        }
                        */

                        order.MPMSTATUSTRANSPORTER = 0; // flag untuk transporter sudah mengambil (TAKE)
                    }
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        void updateTransporterDone(int jobId, string dealerId, string soId, string pickingId, string npkPicker, string trolley)
        {
            try
            {
                MPMWMS_MAP_SO_PICAREA picAreaData = _SOPICAreaObject.Item
                                                (
                                                      jobId
                                                    , UserSession.DATAAREA_ID
                                                    , UserSession.MAINDEALER_ID
                                                    , UserSession.SITE_ID_MAPPING
                                                    , dealerId
                                                    , soId
                                                );

                if (picAreaData != null && picAreaData.ISDONETRANSPORTER == "N")
                {
                    picAreaData.ISDONETRANSPORTER = "Y";

                    _SOPICAreaObject.Update(picAreaData);

                    // untuk update data history PIC jika sudah proses transpoter sudah DONE
                    MPMWMS_MAP_PIC_HISTORY historyPicData = _SOPICAreaHistoryObject.ItemByPicking
                            (
                                  pickingId
                                , UserSession.DATAAREA_ID
                                , UserSession.MAINDEALER_ID
                                , UserSession.SITE_ID_MAPPING
                            );

                    if (historyPicData != null)
                    {
                        historyPicData.ENDTAKE = MPMDateUtil.DateTime;
                        historyPicData.TROLLEYNO = trolley;

                        _SOPICAreaHistoryObject.Update(historyPicData);
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }

        }

        void updateTrolleyUserInfo(String _dataarea, String _pickingId, String _trolleyNbr, String _npk, string _maindealer, string _siteid)
        {
            try
            {
                if (pickingIdOld != _pickingId)
                {
                    WMSPICKINGROUTE route = _WmsPickingRouteObject.Item(_dataarea, _pickingId);

                    if (route.XTSTROLLEYNBR == "" && route.XTSLOCKEDUSERID == "")
                    {
                        //var dataExist = _QryPickingJobPIC.cekExistPendingTrolley(_dataarea, _maindealer, _siteid, _npk, _trolleyNbr);

                        BeginTransaction();

                        //if (dataExist != null && dataExist.Trolley !="" && dataExist.Trolley != _trolleyNbr)
                        //{
                        //    throw new MPMException("Transporter " + dataExist.PickingUser + " terdapat Job pending untuk trolley " + dataExist.Trolley + " !!!");
                        //}

                        route.XTSTROLLEYNBR = _trolleyNbr;
                        route.XTSLOCKEDUSERID = _npk;

                        Commit();

                        pickingIdOld = _pickingId;
                    }
                }
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMSMODEL.Query.QryPickingJobPIC.PickingJobPICGroup_Rec> getNextJobTransporter(string dataareaid, string maindealer, string siteid, string npk, string trolley)
        {
            return _QryPickingJobPIC.getNextJobId(dataareaid, maindealer, siteid, npk, trolley);
        }

        #endregion

        #region MONITORING SO - PIC TRANSPORTER & CHECKER

        public List<DashboardMonitoring_Record> getDataDashboardMonitoring()
        {
            return _SOPICAreaHistoryObject.dataDashboardMonitoring();
        }
        
        #endregion
    }
}
