﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Picking;
using MPMWMSMODEL.Table.PickingTakeMapping;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Table.SalesTable;
using MPMWMSMODEL.Table.CustInvoiceJour;
using MPMWMSMODEL.Table.CustInvoiceTrans;
using MPMWMSMODEL.Query;

namespace MPMWMSMODEL.Model.PDT
{
    public class MPMWMS_PICKING_LIST_TAKE_CUSTOM
    {
        public String _PICKING_ID { get; set; }
        public String _MACHINE_ID { get; set; }
        public String _FRAME_ID { get; set; }
        public String _LOCATION_ID { get; set; }
        public String _UNIT_TYPE { get; set; }
        public String _COLOR_TYPE { get; set; }
        public String _UNIT_NAME { get; set; }
        public String _MACHINE_ID_TAKE { get; set; }
        public String _FRAME_ID_TAKE { get; set; }

        public MPMWMS_PICKING_LIST_TAKE_CUSTOM()
        {
        }

        public MPMWMS_PICKING_LIST_TAKE_CUSTOM(
                String picking_id, String machine_id, String frame_id, String location_id, String unit_type, String color_type,
                String machine_id_take, String frame_id_take
            )
        {

        }
    }

    public class PickingModel: PDTUnitModel
    {
        private PickingObject _PickingObject = new PickingObject();
        private PickingTakeMappingObject _PickingTakeMappingObject = new PickingTakeMappingObject();
        private SalesTableObject _SalesTableObject = new SalesTableObject();
        private CustInvoiceJourObject _CustInvoiceJourObject = new CustInvoiceJourObject();

        public PickingModel()
            : base()
        {
            _PickingObject.Context = (WmsUnitDataContext)Context;
            _PickingObject.PickingLineObject.Context = (WmsUnitDataContext)Context;
            _PickingTakeMappingObject.Context = (WmsUnitDataContext)Context;
            _SalesTableObject.Context = (WmsUnitDataContext)Context;
            _SalesTableObject.SalesLineObject.Context = (WmsUnitDataContext)Context;
            _CustInvoiceJourObject.Context = (WmsUnitDataContext)Context;
            _CustInvoiceJourObject.CustInvoiceTransObject.Context = (WmsUnitDataContext)Context;
        }

        public PickingObject PickingObject
        {
            get { return _PickingObject; }
        }

        public PickingTakeMappingObject PickingTakeMappingObject
        {
            get { return _PickingTakeMappingObject; }
        }

        public SalesTableObject SalesTableObject
        {
            get { return _SalesTableObject; }
        }

        public CustInvoiceJourObject CustInvoiceJourObject
        {
            get { return _CustInvoiceJourObject; }
        }

        public List<MPMWMS_PICKING_LIST_TAKE_CUSTOM> PickingListTake(String picking_id, String dataarea_id, String maindealer_id, String site_id)
        {
            List<MPMWMS_PICKING_LINE> query = PickingObject.PickingLineObject.List(picking_id, dataarea_id, maindealer_id, site_id);
            
            List<MPMWMS_PICKING_LIST_TAKE_CUSTOM> list = new List<MPMWMS_PICKING_LIST_TAKE_CUSTOM>();
            foreach (var a in query.ToList())
            {
                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(a.UNIT_TYPE, a.COLOR_TYPE);
                MPMWMS_PICKING_LIST_TAKE_CUSTOM b = new MPMWMS_PICKING_LIST_TAKE_CUSTOM();
                b._MACHINE_ID = a.MACHINE_ID;
                b._FRAME_ID = a.FRAME_ID;
                b._UNIT_TYPE = a.UNIT_TYPE;
                if (viewProduct != null)
                {
                    b._UNIT_NAME = viewProduct.ITEMNAME;
                }
                b._COLOR_TYPE = a.COLOR_TYPE;
                b._LOCATION_ID = a.LOCATION_ID;
                MPMWMS_PICKING_TAKE_MAPPING c = PickingTakeMappingObject.Item(a.PICKING_ID, a.DATAAREA_ID, a.MAINDEALER_ID, a.SITE_ID, a.DO_ID, a.MACHINE_ID);
                if (c != null)
                {
                    b._MACHINE_ID_TAKE = c.MACHINE_ID_TAKE;
                    b._FRAME_ID_TAKE = c.FRAME_ID_TAKE;
                }
                list.Add(b);
            }

            return list.OrderBy(x => x._PICKING_ID).ToList();
        }

        public bool InsertToTakeMapping(MPMWMS_UNIT unit, MPMWMS_PICKING picking, MPMWMS_PICKING_LINE line)
        {
            // before insert check on take mapping already exist or not
            MPMWMS_PICKING_TAKE_MAPPING itemOld = PickingTakeMappingObject.Item(line.PICKING_ID, 
                line.DATAAREA_ID, line.MAINDEALER_ID, line.SITE_ID,
                line.DO_ID, line.MACHINE_ID);
            if (itemOld == null)
            {
                MPMWMS_PICKING_TAKE_MAPPING item = PickingTakeMappingObject.ItemTakeNoPicking(
                    line.DATAAREA_ID, line.MAINDEALER_ID, line.SITE_ID,
                    unit.FRAME_ID, unit.MACHINE_ID);

                MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(
                    unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                if (location == null)
                {
                    return false;
                }

                if (!(location.LOCATION_TYPE == "S" || location.LOCATION_TYPE == "R" || location.LOCATION_TYPE == "T"
                     || location.LOCATION_TYPE == "I"))
                {
                    return false;
                }

                if (item != null)
                {
                    // jika open maka tidak bisa dimasukkan, namun jika statusnya return maka boleh dimasukkan
                    if (item.STATUS == "O")
                    {
                        return false;
                    }
                }

                // cari di take mapping apakah machine_id sudah ada yang sama
                List<MPMWMS_PICKING_TAKE_MAPPING> listCheck = PickingTakeMappingObject.ListOpen(line.DATAAREA_ID,
                    line.MAINDEALER_ID, line.SITE_ID);
                var takeCheck = listCheck.Where(x => x.MACHINE_ID == line.MACHINE_ID
                    && x.FRAME_ID == line.FRAME_ID && x.PICKING_ID == line.PICKING_ID).Take(1).FirstOrDefault();
                if (takeCheck != null)
                {
                    return false;
                }

                MPMWMS_PICKING_TAKE_MAPPING take = PickingTakeMappingObject.Create();
                take.COLOR_TYPE = unit.COLOR_TYPE;
                take.DATAAREA_ID = unit.DATAAREA_ID;
                take.MAINDEALER_ID = UserSession.MAINDEALER_ID;
                take.DO_ID = line.DO_ID;
                take.FRAME_ID = line.FRAME_ID;
                take.FRAME_ID_TAKE = unit.FRAME_ID;
                take.WAREHOUSE_ID = line.WAREHOUSE_ID;
                take.LOCATION_ID = line.LOCATION_ID;
                take.WAREHOUSE_ID_TAKE = unit.WAREHOUSE_ID;
                take.LOCATION_ID_TAKE = unit.LOCATION_ID;
                take.MACHINE_ID = line.MACHINE_ID;
                take.MACHINE_ID_TAKE = unit.MACHINE_ID;
                take.PICKING_ID = line.PICKING_ID;
                take.SITE_ID = line.SITE_ID;
                take.SITE_ID_TAKE = unit.SITE_ID;
                take.UNIT_TYPE = unit.UNIT_TYPE;
                PickingTakeMappingObject.Insert(take);

                // update unit
                MPMWMS_UNIT realUnit = UnitObject.Item(take.DATAAREA_ID, take.MACHINE_ID);
                if (realUnit == null)
                {
                    throw new MPMException("Already take picking");
                }
                if (realUnit.STATUS != "S")
                {
                    realUnit.DO_ID = picking.DO_ID;
                    realUnit.DO_DATE = picking.DO_DATE;
                    realUnit.PICKING_ID = picking.PICKING_ID;
                    realUnit.PICKING_DATE = picking.PICKING_DATE;
                    realUnit.IS_READYTOPICK = "N";
                    realUnit.IS_READYTOPICK_AX = "N";
                    UnitObject.Update(realUnit);
                }

                // count take and line
                int countTake = PickingTakeMappingObject.List(line.PICKING_ID,
                    line.DATAAREA_ID, line.MAINDEALER_ID, line.SITE_ID).Count() + 1;
                int countLine = PickingObject.PickingLineObject.List(line.PICKING_ID, line.DATAAREA_ID, line.MAINDEALER_ID, line.SITE_ID).Count();
                // update picking status to 'F'
                if (countTake >= countLine)
                {
                    //MPMWMS_PICKING picking = PickingObject.Item(line.PICKING_ID, line.DATAAREA_ID, line.MAINDEALER_ID, line.SITE_ID);
                    picking.STATUS = "F";
                }
                else
                {
                    picking.STATUS = "O";
                }

                return true;
            }

            return false;
        }

        private bool ValidatePickingLine(MPMWMS_UNIT unit, String picking_id, int FIFO_DAY)
        {
            MPMWMS_PICKING picking = PickingObject.Item(picking_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
            if (picking == null)
            {
                return false;
            }

            /*CUSTINVOICEJOUR invoicejour = CustInvoiceJourObject.Item(picking.DO_ID, picking.DATAAREA_ID);
            if (invoicejour == null)
            {
                return false;
            }

            SALESLINE salesline = SalesTableObject.SalesLineObject.Item(
                invoicejour.SALESID, picking.DATAAREA_ID, unit.UNIT_TYPE, unit.COLOR_TYPE);

            if (salesline == null)
            {
                return false;
            }

            if (unit.UNIT_YEAR != salesline.XTSUNITYEAR)
            {
                return false;
            }*/

            List<MPMWMS_PICKING_LINE> lines = PickingObject.Item(picking_id, UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING).
                MPMWMS_PICKING_LINEs.ToList();

            // found match
            var objectLine = lines.Where(x =>
                x.FRAME_ID == unit.FRAME_ID
                && x.MACHINE_ID == unit.MACHINE_ID).Take(1).FirstOrDefault();
            if (objectLine != null)
            {
                // ditemukan, namun pastikan tahun sesuai dengan permintaan
                MPMWMS_UNIT unit_line = UnitObject.ItemBySite(objectLine.DATAAREA_ID, objectLine.SITE_ID, objectLine.MACHINE_ID);
                if (unit_line != null && unit_line.UNIT_YEAR == unit.UNIT_YEAR)
                {
                    bool result = InsertToTakeMapping(unit, picking, objectLine);
                    if (result) return true;
                }
            }
            /*
            foreach (MPMWMS_PICKING_LINE line in lines)
            {
                MPMWMS_UNIT unit_line = UnitObject.ItemBySite(line.DATAAREA_ID, line.SITE_ID, line.MACHINE_ID);
                if (unit_line != null)
                {
                    if (line.FRAME_ID.Equals(unit.FRAME_ID) && line.MACHINE_ID.Equals(unit.MACHINE_ID) && unit_line.UNIT_YEAR == unit.UNIT_YEAR)
                    {
                        bool result = InsertToTakeMapping(unit, picking, line);
                        if (result) return true;
                    }
                }
            }
            */

            // found type and color
            var objectLines2 = lines.Where(x =>
                x.UNIT_TYPE == unit.UNIT_TYPE
                && x.COLOR_TYPE == unit.COLOR_TYPE).ToList();
            foreach (var a in objectLines2)
            {
                MPMWMS_UNIT unitLine = UnitObject.ItemBySite(a.DATAAREA_ID, a.SITE_ID, a.MACHINE_ID);
                if (unitLine != null)
                {
                    if (unitLine.SPG_DATE != null && unitLine.UNIT_YEAR == unit.UNIT_YEAR)
                    {
                        if (unitLine.SPG_DATE.Value.Date.AddDays(FIFO_DAY).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                        {
                            bool result = InsertToTakeMapping(unit, picking, a);
                            if (result) return true;
                        }
                        else if (unitLine.SPG_DATE.Value.Date.AddDays(FIFO_DAY * -1).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                        {
                            bool result = InsertToTakeMapping(unit, picking, a);
                            if (result) return true;
                        }
                    }
                }
            }
            /*
            foreach (MPMWMS_PICKING_LINE line in lines)
            {
                if (line.UNIT_TYPE.Equals(unit.UNIT_TYPE) && line.COLOR_TYPE.Equals(unit.COLOR_TYPE))
                {
                    MPMWMS_UNIT unitLine = UnitObject.ItemBySite(line.DATAAREA_ID, line.SITE_ID, line.MACHINE_ID);
                    if (unitLine != null)
                    {
                        if (unitLine.SPG_DATE != null && unitLine.UNIT_YEAR == unit.UNIT_YEAR)
                        {
                            //bool result = InsertToTakeMapping(unit, picking, line);
                            //if (result) return true;

                            if (unitLine.SPG_DATE.Value.Date.AddDays(FIFO_DAY).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                            {
                                bool result = InsertToTakeMapping(unit, picking, line);
                                if (result) return true;
                            }
                            else if (unitLine.SPG_DATE.Value.Date.AddDays(FIFO_DAY * -1).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                            {
                                bool result = InsertToTakeMapping(unit, picking, line);
                                if (result) return true;
                            }

                            // pengecualian buat caruban
                            /*if (unitLine.SITE_ID == "MD1")
                            {
                                InsertToTakeMapping(unit, line);
                                return true;
                            }

                            if (unitLine.SPG_DATE.Value.Date.AddDays(7).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                            {
                                InsertToTakeMapping(unit, line);
                                return true;
                            }
                            else if (unitLine.SPG_DATE.Value.Date.AddDays(-7).CompareTo(unit.SPG_DATE.Value.Date) >= 1)
                            {
                                InsertToTakeMapping(unit, line);
                                return true;
                            }*/
                        /*}
                    }
                }
            }*/
            return false;
        }

        public String DoPicking(String dataarea_id, String maindealer_id, String site_id, String picking_id, DateTime picking_date, String QRCODE , bool is_print, int FIFO_DAY)//modif by anong String machine_or_frame_id
        {
            BeginTransaction();
            // validate the unit
            //proses by scan qrcode
            //MPMWMS_UNIT unit = UnitObject.ItemByQR(dataarea_id, QRCODE, site_id);
            
            //proses scan by nosin
            MPMWMS_UNIT unit = UnitObject.Item(dataarea_id, QRCODE);
            if (unit != null)
            {
                if (!unit.SITE_ID.Equals(UserSession.SITE_ID_MAPPING))
                {
                    throw new MPMException("Unit already mutation to another site !!!");
                }

                if (unit.STATUS.Equals("U"))
                {
                    throw new MPMException("Unit is Unknown !!!");
                }
                else if (unit.STATUS.Equals("C"))
                {
                    throw new MPMException("Unit is Claim !!!");
                }
                else if (unit.STATUS.Equals("RB"))
                {
                    throw new MPMException("Unit is Robbing !!!");
                }
                else if (unit.STATUS.Equals("PDI"))
                {
                    throw new MPMException("Unit is Pre Delivery !!!");
                }
                else if (unit.STATUS.Equals("CO"))
                {
                    throw new MPMException("Unit is Claim Others !!!");
                }
                else if (unit.STATUS.Equals("S"))
                {
                    throw new MPMException("Unit is Sold !!!");
                }
                else if (unit.STATUS.Equals("CS"))
                {
                    throw new MPMException("Unit SPG already canceled !!!");
                }
                else if (unit.STATUS.Equals("RFS"))
                {
                    // check all unit on list detail line
                    bool find = ValidatePickingLine(unit, picking_id, FIFO_DAY);
                    if (find == false)
                    {
                        throw new MPMException("Failed to pick !!!");
                    }
                }
                else
                {
                    throw new MPMException("Unit can't be picking !!!");
                }
            }
            else
            {
                throw new MPMException("Unit not found");
            }

            // update unit
            String site_id_old = unit.SITE_ID;
            String warehouse_id_old = unit.WAREHOUSE_ID;
            String location_id_old = unit.LOCATION_ID;

            try
            {
                unit.PICKING_DATE = picking_date;
                unit.PICKING_ID = picking_id;
                unit.PICKING_SCAN_TIME = MPMDateUtil.DateTime;
                unit.IS_READYTOPICK = "Y";
                unit.LOCATION_ID = GetLocation(unit.DATAAREA_ID, unit.SITE_ID, "P");

                if (unit.LOCATION_ID.Equals(""))
                {
                    throw new MPMException("Location picking (Type) is not configured !!!");
                }

                unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);
                UnitObject.Update(unit, "PIS", site_id_old, warehouse_id_old, location_id_old);

                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);
                UpdateLocationStock(unit.DATAAREA_ID, site_id_old, warehouse_id_old, location_id_old, -1);

                Commit();
                //SubmitChanges();

                // find description product
                //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                //String nameType = unit.UNIT_TYPE;
                //String nameColor = unit.COLOR_TYPE;
                //if (viewProduct != null)
                //{
                //    nameType = viewProduct.nametype;
                //    nameColor = viewProduct.namecolor;
                //}
                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                String nameColor = unit.COLOR_TYPE;
                if (viewProduct != null)
                {
                    //nameType = viewProduct.nametype;
                    nameType = viewProduct.ITEMNAME;
                    nameColor = viewProduct.ITEMCOLORCODE;
                }

                // find dealer data
                MPMWMS_PICKING picking = PickingObject.Item(picking_id, dataarea_id, maindealer_id, site_id);
                String dealerCode = "";
                String dealerName = "";
                String sopir = "";
                String expedition = "";
                String nopol = "";//hendynambah
                if (picking != null)
                {
                    dealerCode = picking.DEALER_CODE;
                    dealerName = picking.DEALER_NAME;
                    expedition = picking.EXPEDITION_ID;
                    sopir = picking.DRIVER;
                    nopol = picking.POLICE_NUMBER;//hendynambah
                }

                if (is_print)
                    PrintPicking(unit.MACHINE_ID, unit.SITE_ID, nameColor, nameType, dealerCode, dealerName, sopir, expedition,nopol);
                return unit.MACHINE_ID;
            }
            catch (Exception e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Reprint(String machine_or_frame_id)
        {
            MPMWMS_UNIT unit = UnitObject.Item(UserSession.DATAAREA_ID, machine_or_frame_id);
            if (unit != null)
            {
                // find description product
                //MPMVIEW_UNITTYPECOLOR_DESCRIPTION viewProduct = ProductDescriptionViewObject.Item(unit.UNIT_TYPE, unit.COLOR_TYPE);
                //String nameType = unit.UNIT_TYPE;
                //String nameColor = unit.COLOR_TYPE;
                //if (viewProduct != null)
                //{
                //    nameType = viewProduct.nametype;
                //    nameColor = viewProduct.namecolor;
                //}
                MPMSERVVIEWDAFTARUNITWARNA viewProduct = ProductDescriptionViewObject.ItemV365(unit.UNIT_TYPE, unit.COLOR_TYPE);
                String nameType = unit.UNIT_TYPE;
                String nameColor = unit.COLOR_TYPE;
                if (viewProduct != null)
                {
                    //nameType = viewProduct.nametype;
                    nameType = viewProduct.ITEMNAME;
                    nameColor = viewProduct.ITEMCOLORCODE;
                }

                // find dealer data
                MPMWMS_PICKING picking = PickingObject.Item(unit.PICKING_ID, unit.DATAAREA_ID, UserSession.MAINDEALER_ID, unit.SITE_ID);
                String dealerCode = "";
                String dealerName = "";
                String sopir = "";
                String expedition = "";
                String nopol = "";
                if (picking != null)
                {
                    dealerCode = picking.DEALER_CODE;
                    dealerName = picking.DEALER_NAME;
                    expedition = picking.EXPEDITION_ID;
                    sopir = picking.DRIVER;
                    nopol = picking.POLICE_NUMBER;
                }
                PrintPicking(unit.MACHINE_ID, unit.SITE_ID, nameColor, nameType, dealerCode, dealerName, sopir, expedition,nopol);
            }
            else
            {
                throw new MPMException("Unit not found !!!");
            }
        }

        public int QuantityCurrentTruck(String dataarea_id, String expedition_id, String police_number, DateTime date)
        {
            try
            {
                var itemsObj =
                    from a in PickingTakeMappingObject.Context.MPMWMS_PICKING_TAKE_MAPPINGs
                    join picking in PickingObject.Context.MPMWMS_PICKINGs on
                        new { picking_id = a.PICKING_ID, dataarea_id = a.DATAAREA_ID, do_id = a.DO_ID } equals
                        new { picking_id = picking.PICKING_ID, dataarea_id = picking.DATAAREA_ID, do_id = picking.DO_ID } into picking_join
                    from result in picking_join.DefaultIfEmpty()
                    where
                        result.POLICE_NUMBER == police_number
                        && result.EXPEDITION_ID == expedition_id
                        && result.DATAAREA_ID == dataarea_id
                        && result.PICKING_DATE.Date == date.Date
                    select
                        new
                        {
                            police_number = result.POLICE_NUMBER,
                            driver = result.DRIVER,
                            picking_date = result.PICKING_DATE
                        };

                var itemsGroup =
                    from a in itemsObj
                    group a by new { a.police_number, a.driver } into grp
                    select new { key = grp.Key, cnt = grp.Count() };
                if (itemsGroup == null) return 0;
                else
                {
                    if (itemsGroup.Take(1).FirstOrDefault() != null)
                        return itemsGroup.Take(1).FirstOrDefault().cnt;
                    else return 0;
                }
            }
            catch (MPMException)
            {
                return 0;
            }
        }

        public int QuantityCurrentTruck(String picking_id)
        {
            List<Object> a = new List<Object>();
            try
            {
                MPMWMS_PICKING data = PickingObject.Item(picking_id,
                    UserSession.DATAAREA_ID, UserSession.MAINDEALER_ID, UserSession.SITE_ID_MAPPING);
                if (data == null)
                {
                    return 0;
                }

                int count = QuantityCurrentTruck(data.DATAAREA_ID, data.EXPEDITION_ID, data.POLICE_NUMBER, data.PICKING_DATE);

                return count;
            }
            catch (MPMException)
            {
                return 0;
            }
        }
    }
}
