﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.HistoryUnit;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.Setting;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class SettingModel : BaseModel
    {
        public SettingDetailObject _SettingDetailObject = new SettingDetailObject();
        public SettingObject _SettingObject = new SettingObject();

        public SettingModel()
            : base()
        {
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;
            _SettingObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Update(MPM_SETTING_DETAIL item)
        {
            BeginTransaction();
            try
            {
                var row = _SettingDetailObject.Item(item.SETTING_ID, item.KEY_TAG_1, item.KEY_TAG_2);

                if (row != null)
                {
                    row.TAG_1 = item.TAG_1;
                    row.TAG_2 = item.TAG_2;
                }
                else
                {
                    throw new MPMException("Item update can't found");
                }
                _SettingDetailObject.Update(row);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Insert(MPM_SETTING itemHeader, MPM_SETTING_DETAIL itemDetail)
        {
            MPM_SETTING dataNewSetting = new MPM_SETTING();
            MPM_SETTING_DETAIL dataNewDetail = new MPM_SETTING_DETAIL();

            BeginTransaction();

            try
            {
                var rowHeader = _SettingObject.Item(itemHeader.SETTING_ID);
                var rowDetail = _SettingDetailObject.Item(itemDetail.SETTING_ID, itemDetail.KEY_TAG_1, itemDetail.KEY_TAG_2);

                if (rowHeader == null)
                {
                    dataNewSetting.SETTING_ID = itemHeader.SETTING_ID;
                    dataNewSetting.SETTING_NAME = itemHeader.SETTING_NAME;
                    dataNewSetting.IS_ACTIVE = "Y";

                    _SettingObject.Insert(dataNewSetting);
                }

                if (rowDetail == null)
                {
                    dataNewDetail.SETTING_ID = itemDetail.SETTING_ID;
                    dataNewDetail.SETTING_NAME = itemDetail.SETTING_NAME;
                    dataNewDetail.KEY_TAG_1 = itemDetail.KEY_TAG_1;
                    dataNewDetail.KEY_TAG_2 = itemDetail.KEY_TAG_2;
                    dataNewDetail.TAG_1 = itemDetail.TAG_1;
                    dataNewDetail.TAG_2 = itemDetail.TAG_2;

                    _SettingDetailObject.Insert(dataNewDetail);
                }

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }
    }
}