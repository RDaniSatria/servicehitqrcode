﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MPMWMSMODEL.Table.VendorStoringUnit;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Table.QRCode;
using System.Diagnostics;
using System.Data.Linq;
using System.Data;
using System.IO;
using MPMWMSMODEL.Table.Customer;

namespace MPMWMSMODEL.Model.PDT
{
    public class RetrievedClass
    {
        public string datetime { get; set; }
        public string ip { get; set; }
        public string endpoint { get; set; }
        public string qrcode { get; set; }
        public string activity_description { get; set; }
        public string user_id { get; set; }
        public string user_type { get; set; }
        public string status { get; set; }
        public string idlog { get; set; }
        public string num_engine { get; set; }
        public string nik { get; set; }
        public string name { get; set; }
        public string message { get; set; }

    }

    public class Param
    {
        public string tipe { get; set; }
        public int jam { get; set; }
        public string date { get; set; }

    }

    public class StoringVendorModel : BaseModelMpmLive
    {
        public QRUnitObject _QRUnitObject = new QRUnitObject();

        public StoringVendorModel()
            : base()
        {
            _QRUnitObject.Context = (WmsMpmLiveDataContext)Context;
        }

        public string GetTokenVendore()
        {
            try
            {
                var Idvendor = ConfigurationManager.AppSettings["IDVendor"];
                var Vendor = ConfigurationManager.AppSettings["Vendor"];

                var item = VendorStoringUnitObject.ItemUnitVendor(Idvendor);
                if (item == null)
                {
                    return getkeyvendore(item);//anas menambahkan variabel item
                    //return getkeyvendore();
                }
                else
                {
                    if (item.DATE.Value.Date == DateTime.Now.Date)
                    {
                        return item.ACCESS_TOKEN;
                    }
                    else
                    {
                        return getkeyvendore(item);//anas menambahkan variabel item
                        //return refreshtokenvendore(item.RefreshToken);
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public string GetTokenVendoreLog()
        {
            try
            {
                var Idvendor = ConfigurationManager.AppSettings["IDVendor"];
                var Vendor = ConfigurationManager.AppSettings["Vendor"];

                var item = VendorStoringUnitObject.ItemUnitVendor(Idvendor);
                if (item == null)
                {
                    return getkeyvendore(item);//anas menambahkan variabel item
                    //return getkeyvendore();
                }
                else
                {
                    if (item.DATE.Value.Date == DateTime.Now.Date)
                    {
                        return item.ACCESS_TOKEN;
                    }
                    else
                    {
                        return getkeyvendore(item);//anas menambahkan variabel item
                        //return refreshtokenvendore(item.RefreshToken);
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public bool mappingqrcodevendorelog(MPMWMSQRUNIT _data, string qrcode, string numengine, string numframe, string createby, string usertype)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["MappingUrl"];
                //var url = "https://staging.qtrust.id/api/agent/vehicle/mapping";

                using (WebClient wc = new WebClient())
                {

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "MappingUrl", year, monthv2, ref runningNumber);

                    var IDAPILOG = "MappingUrl" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["Code"] = qrcode;
                    param["NumEngine"] = numengine;
                    param["NumFrame"] = numframe;
                    param["UserId"] = createby;
                    param["UserType"] = usertype;
                    param["RqUuid"] = IDAPILOG;//tambahan anas

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    //tambahan anas
                    InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO/*, result["code"].ToString()*/);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    //InsertLogVendor(url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());
                    //InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());
                    UpdateLogVendor(IDAPILOG, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        public bool mappingqrcodevendore(string qrcode, string numengine, string numframe, string createby, string usertype)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["MappingUrl"];
                //var url = "https://staging.qtrust.id/api/agent/vehicle/mapping";

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["Code"] = qrcode;
                    param["NumEngine"] = numengine;
                    param["NumFrame"] = numframe;
                    param["UserId"] = createby;
                    param["UserType"] = usertype;

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);


                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        public bool unmatch(string qrcode, string numengine, string siteid, string desc)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["Unmatch"];
                //var url = "https://staging.qtrust.id/api/agent/vehicle/mapping";

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["Code"] = qrcode;
                    param["num_engine"] = numengine;
                    param["description"] = desc;
                    param["BranchCode "] = siteid;

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        //tambahan anas
        public bool VehicleUnmatch(MPMWMSQRUNIT _data, string qrcode, string numengine, string siteid)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["VehicleUnmatch"];
                //var url = "https://staging.qtrust.id/api/mpm/agent/unmatch";

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "VehicleUnmatch", year, monthv2, ref runningNumber);

                    var IDAPILOG = "VehicleUnmatch" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["Code"] = qrcode;
                    param["NumEngine"] = numengine;
                    param["BranchCode"] = siteid;
                    param["RqUuid"] = IDAPILOG;


                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO/*, result["code"].ToString()*/);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    UpdateLogVendor(IDAPILOG, result["code"].ToString());
                    ////InsertLogVendor(url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());
                    //InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        //tambahan anas _item, _item.QRCODEUNCLAIM, _item.IDENTITYNUMBER, _item.IDENTITYNUMBERUNCLAIM, _item.KODEDEALER
        public bool VehicleUnclaim(MPM_VENDOR_CUSTOMER _item, string qrcodeUnclaim, string noktp, string noktpUnclaim, string kodeDealer)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["VehicleUnclaim"];
                //var url = "https://mpm-motor-staging.qtrust.id/api/vehicle/unclaim";

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "VehicleUnclaim", year, monthv2, ref runningNumber);

                    var IDAPILOG = "VehicleUnclaim" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["IdentityNumber"] = noktpUnclaim;
                    param["NumEngine"] = _item.CUSTOMER_AHM_NO;
                    param["BranchCode"] = kodeDealer;
                    param["UserId"] = "SYSTEM";
                    param["UserType"] = "ADMIN";
                    param["Code"] = qrcodeUnclaim;
                    param["RqUuid"] = IDAPILOG;


                    //error saat  di "var data" langsung ke catch fungsi sebelumnya 26-8-2020
                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    //InsertLogUncliamCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, _item.IDENTITYNUMBERUNCLAIM, _item.QRCODEUNCLAIM);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    UpdateLogCustomer(IDAPILOG, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine, qrcode);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        //tambahan anas (string _flag, string _agt, string _agf,  DateTime _datetime)
        public void updatetrackqrcodevendore(MPMWMSQRUNIT _data, string qrcode, string numengine, string numframe,
       string lat, string lng, string SPG, string _siteid, string _location, string _userid,
       string _activity, string _flag, string _agt, string _agf, DateTime _datetime, DateTime _documentDate)
        //public void updatetrackqrcodevendore(string qrcode, string numengine, string numframe, string lat, string lng, string SPG, string _siteid, string _location, string _userid, string _activity)
        {
            var tes = _datetime.ToString("yyyy-MM-dd HH:mm:ss");
            var DocumentDate = _documentDate.ToString("yyyy-MM-dd HH:mm:ss");
            var k = "";
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["UpdateTrack"];
                //var url = "https://staging.qtrust.id/api/agent/qrcode";

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "UpdateTrack", year, monthv2, ref runningNumber);

                    var IDAPILOG = "UpdateTrack" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["Code"] = qrcode;
                    param["FunLocCode"] = _location;
                    param["BranchCode"] = _siteid;
                    param["DocumentNo"] = SPG;
                    param["Lat"] = lat;
                    param["Lng"] = lng;
                    param["ActivityDescription"] = _activity;
                    param["UserId"] = _userid;
                    param["UserType"] = ConfigurationManager.AppSettings["VehicleUserType"];
                    param["DocumentDate"] = _documentDate.ToString("yyyy-MM-dd HH:mm:ss");

                    //tambahan untuk cek kirim data ke vendor (_flag,_agf, _agt)
                    param["Flag"] = _flag;
                    param["AgentFrom"] = _agf;
                    param["AgentTo"] = _agt;
                    param["Datetime"] = _datetime.ToString("yyyy-MM-dd HH:mm:ss");
                    param["RqUuid"] = IDAPILOG;//tambahan anas

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO/*, result["code"].ToString()*/);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    ////InsertLogVendor(url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());
                    //InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO, result["code"].ToString());

                    UpdateLogVendor(IDAPILOG, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {

                        }
                        else
                        {
                            Debug.WriteLine("masuk insert MPM_VENDOR_TRACK count >0 dengan status gagal " + json);
                            //inserttrack(new MPM_VENDOR_TRACK
                            //{
                            //    CODE = qrcode,
                            //    FUNLOCCODE = UserSession.SITE_ID_MAPPING,
                            //    BRANCHCODE = UserSession.MAINDEALER_ID,
                            //    DOCUMENTNO = SPG,
                            //    LATITUDE = lat,
                            //    LONGITUDE = lng,
                            //    USERID = UserSession.USER_ID,
                            //    CREATEBY = UserSession.USER_ID,
                            //    CREATEDATE = DateTime.Now
                            //});
                        }
                    }
                    else
                    {

                        Debug.WriteLine("masuk insert MPM_VENDOR_TRACK count = 0 dengan status gagal " + json);
                        //inserttrack(new MPM_VENDOR_TRACK
                        //{
                        //    CODE = qrcode,
                        //    FUNLOCCODE = UserSession.SITE_ID_MAPPING,
                        //    BRANCHCODE = UserSession.MAINDEALER_ID,
                        //    DOCUMENTNO = SPG,
                        //    LATITUDE = lat,
                        //    LONGITUDE = lng,
                        //    USERID = UserSession.USER_ID,
                        //    CREATEBY = UserSession.USER_ID,
                        //    CREATEDATE = DateTime.Now
                        //});
                    }
                }
            }
            catch (MPMException e)
            {
                Debug.WriteLine("masuk insert MPM_VENDOR_TRACK catch dengan status gagal ");
                //inserttrack(new MPM_VENDOR_TRACK
                //{
                //    CODE = qrcode,
                //    FUNLOCCODE = UserSession.SITE_ID_MAPPING,
                //    BRANCHCODE = UserSession.MAINDEALER_ID,
                //    DOCUMENTNO = SPG,
                //    LATITUDE = lat,
                //    LONGITUDE = lng,
                //    USERID = UserSession.USER_ID,
                //    CREATEBY = UserSession.USER_ID,
                //    CREATEDATE = DateTime.Now
                //});
            }
        }

        public void createvehiclevendore(string qrcode, string UnitTypeCode, string Brand, string UnitColorCode, string NumEngine, string NumFrame, string ManufactureYear, string lat, string lng, string SPG)//, string UserId, string UserType)
        {
            try
            {
                Brand = ConfigurationManager.AppSettings["VehicleBrand"];
                string key = GetTokenVendore();

                var url = ConfigurationManager.AppSettings["VehicleUrl"];

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["UnitTypeCode"] = UnitTypeCode;
                    param["Brand"] = Brand;
                    param["UnitColorCode"] = UnitColorCode;
                    param["NumEngine"] = NumEngine;
                    param["NumFrame"] = NumFrame;
                    param["ManufactureYear"] = ManufactureYear;
                    param["UserId"] = UserSession.USER_ID;
                    param["UserType"] = ConfigurationManager.AppSettings["VehicleUserType"];

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            bool b = mappingqrcodevendore(qrcode, NumEngine, NumFrame, UserSession.USER_ID, ConfigurationManager.AppSettings["VehicleUserType"]);
                            //updatetrackqrcodevendore(qrcode, NumEngine, NumFrame, lat, lng, SPG);
                            if (b == false)
                            {
                                insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = NumEngine, FRAME_ID = NumFrame, QRCODE = qrcode, CREATEBY = UserSession.USER_ID, CREATEDATE = DateTime.Now });
                            }
                        }
                        else
                        {
                            insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = NumEngine, FRAME_ID = NumFrame, QRCODE = qrcode, CREATEBY = UserSession.USER_ID, CREATEDATE = DateTime.Now });
                        }
                    }
                    else
                    {
                        insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = NumEngine, FRAME_ID = NumFrame, QRCODE = qrcode, CREATEBY = UserSession.USER_ID, CREATEDATE = DateTime.Now });
                    }
                }
            }
            catch (MPMException e)
            {
                insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = NumEngine, FRAME_ID = NumFrame, QRCODE = qrcode, CREATEBY = UserSession.USER_ID, CREATEDATE = DateTime.Now });
            }
        }

        public string getkeyvendore(MPM_VENDOR _item)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["TokenUrl"];

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "TokenUrl", year, monthv2, ref runningNumber);

                    var IDAPILOG = "TokenUrl" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["GrantType"] = ConfigurationManager.AppSettings["TokenGrantType"];
                    param["ClientId"] = ConfigurationManager.AppSettings["TokenClientId"];
                    param["ClientSecret"] = ConfigurationManager.AppSettings["TokenClientSecret"];
                    param["Email"] = ConfigurationManager.AppSettings["TokenEmail"];
                    param["Password"] = ConfigurationManager.AppSettings["TokenPassword"];
                    param["RqUuid"] = IDAPILOG;//tambahan anas

                    wc.Encoding = Encoding.UTF8;
                    //wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    InsertLogGetToken(IDAPILOG, url, _item.IDVENDOR, _item.VENDOR, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {

                            var data_result = JObject.Parse(result["data"].ToString());
                            //var token = MPMEncrypt.EncryptString(data_result["token"]["access_token"].ToString());
                            //var refreshtoken = MPMEncrypt.EncryptString(data_result["token"]["refresh_token"].ToString());

                            MPM_VENDOR item = new MPM_VENDOR();
                            item.IDVENDOR = ConfigurationManager.AppSettings["IDVendor"];
                            item.VENDOR = ConfigurationManager.AppSettings["Vendor"];
                            item.ACCESS_TOKEN = data_result["token"]["access_token"].ToString();
                            item.REFRESH_TOKEN = data_result["token"]["refresh_token"].ToString();
                            item.EXPIRES_IN = Convert.ToInt32(data_result["token"]["expires_in"].ToString());
                            item.DATE = DateTime.Now.Date;

                            insertmpmsetting(item);

                            return data_result["token"]["access_token"].ToString();
                        }
                        else
                        {
                            throw new MPMException("Gagal Mapping Qr Code Vendore");
                        }
                    }
                    else
                    {
                        throw new MPMException("Gagal Mapping Qr Code Vendore");
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public string refreshtokenvendore(string RToken)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["RefreshUrl"];

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["GrantType"] = ConfigurationManager.AppSettings["RefreshGrantType"];
                    param["ClientId"] = ConfigurationManager.AppSettings["RefreshClientId"];
                    param["ClientSecret"] = ConfigurationManager.AppSettings["RefreshClientSecret"];
                    param["RefreshToken"] = RToken;

                    wc.Encoding = Encoding.UTF8;
                    //wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {

                            var data_result = JObject.Parse(result["data"].ToString());
                            //var token = MPMEncrypt.EncryptString(data_result["token"]["access_token"].ToString());
                            //var refreshtoken = MPMEncrypt.EncryptString(data_result["token"]["refresh_token"].ToString());

                            MPM_VENDOR item = new MPM_VENDOR();
                            item.IDVENDOR = ConfigurationManager.AppSettings["IDVendor"];
                            item.VENDOR = ConfigurationManager.AppSettings["Vendor"];
                            item.ACCESS_TOKEN = data_result["token"]["access_token"].ToString();
                            item.REFRESH_TOKEN = data_result["token"]["refresh_token"].ToString();
                            item.EXPIRES_IN = Convert.ToInt32(data_result["token"]["expires_in"].ToString());
                            item.DATE = DateTime.Now.Date;

                            insertmpmsetting(item);

                            return data_result["token"]["access_token"].ToString();
                        }
                        else
                        {
                            throw new Exception("Gagal Mapping Qr Code Vendore");
                        }
                    }
                    else
                    {
                        throw new Exception("Gagal Mapping Qr Code Vendore");
                    }
                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void insertmpmsetting(MPM_VENDOR item)
        {
            BeginTransaction();
            try
            {
                var row = VendorStoringUnitObject.ItemUnitVendor(item.IDVENDOR);
                if (row == null)
                {
                    item.CREATEBY = "APISTORING";
                    item.CREATEDATE = DateTime.Now;
                    VendorStoringUnitObject.Insert(item);
                }
                else
                {
                    row.VENDOR = item.VENDOR;
                    row.ACCESS_TOKEN = item.ACCESS_TOKEN;
                    row.REFRESH_TOKEN = item.REFRESH_TOKEN;
                    row.EXPIRES_IN = item.EXPIRES_IN;
                    row.DATE = item.DATE;
                    item.MODIFBY = "APISTORING";
                    item.MODIFDATE = DateTime.Now;
                }
                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void insertmpmqrcode(MPMQRCODE_VENDOR item)
        {
            BeginTransaction();
            try
            {
                VendorStoringUnitObject.Insert(item);
                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void inserttrack(MPM_VENDOR_TRACK item)
        {
            BeginTransaction();
            try
            {
                VendorStoringUnitObject.Insert(item);
                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void InsertLogVendor(String idapilog, String url, int IDQR, String QrCode, String nosin/*, String ResponseCode*/)
        {
            BeginTransaction();
            try
            {

                MPMHITAPIQTRUSTLOG item = new MPMHITAPIQTRUSTLOG();
                item.IDAPILOG = idapilog;
                item.ALAMATAPI = url;
                item.IDQR = IDQR;
                item.QRCODE = QrCode;
                item.ENGINENO = nosin;
                item.WAKTU = MPMDateUtil.DateTime;
                item.STATUS = "0";
                //item.STATUS = ResponseCode;

                VendorStoringUnitObject.Insertlog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void InsertLogCustomer(String idapilog, String url, String nosin, String NoKTP, String Name)
        {
            BeginTransaction();
            try
            {

                MPMHITAPIQTRUSTLOG item = new MPMHITAPIQTRUSTLOG();

                item.IDAPILOG = idapilog;
                item.ALAMATAPI = url;
                item.ENGINENO = nosin;
                item.NOKTP = NoKTP;
                item.NAMA = Name;
                item.WAKTU = MPMDateUtil.DateTime;
                item.STATUS = "0";

                VendorStoringUnitObject.Insertlog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void InsertLogUncliamCustomer(String idapilog, String url, String nosin, String NoKTP, String Name, String NoKtpUnclaim, String qrcodeUnclaim)
        {
            BeginTransaction();
            try
            {

                MPMHITAPIQTRUSTLOG item = new MPMHITAPIQTRUSTLOG();

                item.IDAPILOG = idapilog;
                item.ALAMATAPI = url;
                item.ENGINENO = nosin;
                item.NOKTP = NoKtpUnclaim;
                item.NAMA = Name;
                item.WAKTU = MPMDateUtil.DateTime;
                item.STATUS = "0";
                item.QRCODE = qrcodeUnclaim; //qrcode unclaim
                //item.STATUS = ResponseCode;

                VendorStoringUnitObject.Insertlog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void InsertLogGetToken(String idapilog, String url, String IdVendor, String Vendor, String ResponseCode)
        {
            BeginTransaction();
            try
            {

                MPMHITAPIQTRUSTLOG item = new MPMHITAPIQTRUSTLOG();

                item.IDAPILOG = idapilog;
                item.ALAMATAPI = url;
                item.QRCODE = IdVendor;
                item.WAKTU = MPMDateUtil.DateTime;
                item.STATUS = ResponseCode;

                VendorStoringUnitObject.Insertlog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void UpdateLogVendor(String IdApiLog, String ResponseCode)
        {
            BeginTransaction();
            try
            {
                var item = _QRUnitObject.getDataLog(IdApiLog);

                item.STATUS = ResponseCode;

                _QRUnitObject.UpdateLog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public void UpdateLogCustomer(String IdApiLog, String ResponseCode)
        {
            BeginTransaction();
            try
            {
                var item = _QRUnitObject.getDataLog(IdApiLog);

                item.STATUS = ResponseCode;

                _QRUnitObject.UpdateLog(item);

                Commit();
            }
            catch (MPMException)
            {
                Rollback();
            }
        }

        public bool cekqrcodevendore(string qrcode)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["cekqrcodeUrl"];

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["Code"] = qrcode;

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var data = wc.UploadValues(url, "GET", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        public void insertQRUnit(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag, String OldLoc)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _unit.SPG_ID;
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.LOCATIONID = OldLoc;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        public void insertQRUnit2(String MUTATION_LOCATION_ID, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag, String OldLoc)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = MUTATION_LOCATION_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.LOCATIONID = OldLoc;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas movement unit 1 out
        public void insertQRUnitMutasiOut(MPMWMS_MUTATION_LOCATION _mutation_lokation_id, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _mutation_lokation_id.MUTATION_LOCATION_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _mutation_lokation_id.LOCATION_ID;//tambahan anas 
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.LOCATIONID = _mutation_lokation_id.LOCATION_ID_OLD;//tambahan anas
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            item.CREATEBY = _mutation_lokation_id.CREATE_BY;//tambahan anas 
            item.CREATEDATE = _mutation_lokation_id.CREATE_DATE;//tambahan anas 
            _QRUnitObject.Insert1(item);

            Commit();
        }

        //tambahan anas movement unit 1 in
        public void insertQRUnitMutasiIn(MPMWMS_MUTATION_LOCATION _mutation_lokation_id, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _mutation_lokation_id.MUTATION_LOCATION_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _mutation_lokation_id.LOCATION_ID;//tambahan anas 
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.LOCATIONID = _mutation_lokation_id.LOCATION_ID_OLD;//tambahan anas 
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        public void insertQRUnit1(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _unit.SPG_ID;
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        public void insertQRUnit3(String JOURNALID, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC
        public void insertQRBundleUnitBom(XVS_BOM_VIEW JOURNALID, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = JOURNALID.ITEMID;
            item.BRAND = "Honda";
            item.COLOR = JOURNALID.INVENTCOLORID;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = JOURNALID.JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = JOURNALID.INVENTSITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONID = _unit.LOCATION_ID;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = JOURNALID.INVENTSITEID;
            item.AGENTFROM = JOURNALID.INVENTSITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC
        public void insertQRBundleUnitBor(XVS_BOR_VIEW JOURNALID, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = JOURNALID.ITEMID;
            item.BRAND = "Honda";
            item.COLOR = JOURNALID.INVENTCOLORID;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = JOURNALID.JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = JOURNALID.INVENTSITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONID = _unit.LOCATION_ID;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = JOURNALID.INVENTSITEID;
            item.AGENTFROM = JOURNALID.INVENTSITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTBUNDLEUNITACC
        public void insertQRBundleUnit(/*String JOURNALID,*/ MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            //item.SPGID = JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTPICKING
        //2020-01-13 ganti custable dg view MPMSERVVIEWCHANNELH1
        public void insertQRUnitPicking(MPMSERVVIEWCHANNELH1 _cusTable, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag, String picking)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = picking; //PICKING_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = "DLRRECEIVING";//_unit.LOCATION_ID; // tambahan anas
            item.LOCATIONID = _unit.LOCATION_ID; // tambahan anas
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _cusTable.MDCODE;//tambahan anas
            item.AGENTFROM = _site.SITEID; // tambahan anas
            _QRUnitObject.Insert(item);

            Commit();
        }

        public void insertQRUnit(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _unit.SPG_ID;
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas (MPMWMSPDTMAPPINGQRUNIT)
        public void insertQRUnitMappingQR(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String qrFromNosin, String nosinFromQr, String Unit, String _qrCode, String _marketName, String _colorName, String kodetab, String flag, String ahm, String Unmatch)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _unit.SPG_ID; //
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONID = _unit.LOCATION_ID;//tambahan anas
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis; //NAMA TAB = PENERIMAAN GUDANG
            item.FLAG = flag; //0 = IN
            item.AGENTTO = _site.SITEID; //tambahan anas
            item.AGENTFROM = ahm; //tambahan anas agentform = "AHM"
            item.STSMATCHING = Unmatch; //tambahan anas (0 = match / 1 = Unmatch)
            item.NosinUnmatch1 = Unit; //tambahan anas
            item.QrUnmatch1 = qrFromNosin; //tambahan anas
            item.NosinUnmatch2 = nosinFromQr; //tambahan anas
            item.QrUnmatch2 = _qrCode; //tambahan anas
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTCLOCKINGUNIT
        public void insertQRUnitClockingUni(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            //item.SPGID = JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTCLOCKINGUNIT
        public void insertQRUnitClockingUnitNosin(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _unit.QR_CODE;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            //item.SPGID = JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTQCUNITNRFS
        public void insertQRUnitQcUni(MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            //item.SPGID = JOURNALID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONIDTO = _unit.LOCATION_ID;
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;
            item.AGENTTO = _site.SITEID;
            item.AGENTFROM = _site.SITEID;
            _QRUnitObject.Insert(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTMUTATIONRECEIVE
        public void insertQRUnitMutationReceiveOut(MPMWMS_MUTATION_SITE_LINE _mutation_line, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _mutation_line.MUTATION_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONID = _mutation_line.LOCATION_ID_INTR;//tambahan anas 
            item.LOCATIONIDTO = _mutation_line.LOCATION_ID_NEW;//tambahan anas 
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;//tambahan anas 
            item.AGENTTO = _mutation_line.SITE_ID_NEW;//tambahan anas 
            item.AGENTFROM = _mutation_line.SITE_ID_INTR;//tambahan anas 
            item.CREATEBY = _mutation_line.CREATE_BY;//tambahan anas 
            item.CREATEDATE = _mutation_line.CREATE_DATE;//tambahan anas 
            _QRUnitObject.Insert1(item);

            Commit();
        }

        //tambahan anas
        //MPMWMSPDTMUTATIONRECEIVE
        public void insertQRUnitMutationReceiveIn(MPMWMS_MUTATION_SITE_LINE _mutation_line, MPMWMS_UNIT _unit, MPMViewINVENTSITE _site, String _qrCode, String _marketName, String _colorName, String kodetab, String flag)
        {
            WmsUnitDataContext context1 = new WmsUnitDataContext();
            string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                               where a.JENISTAB == "QRCODE" && a.KODETAB == kodetab
                               select a.NAMATAB).Take(1).FirstOrDefault();
            MPMWMSQRUNIT item = new MPMWMSQRUNIT();

            BeginTransaction();

            item.QRCODE = _qrCode;
            item.UNITTYPE = _unit.UNIT_TYPE;
            item.BRAND = "Honda";
            item.COLOR = _unit.COLOR_TYPE;
            item.ENGINENO = _unit.MACHINE_ID;
            item.FRAMENO = _unit.FRAME_ID;
            item.UNITYEAR = _unit.UNIT_YEAR;
            item.LATITUDE = _site.Latitude;
            item.LONGITUDE = _site.Longitude;
            item.SPGID = _mutation_line.MUTATION_ID;//tambahan anas 
            item.MARKETNAME = _marketName;
            item.COLORNAME = _colorName;
            item.ISSEND = 0;
            item.SITEID = _site.SITEID;
            item.MAINDEALER = _site.XTSMAINDEALERCODE;
            item.LOCATIONID = _mutation_line.LOCATION_ID_INTR;//tambahan anas 
            item.LOCATIONIDTO = _mutation_line.LOCATION_ID_NEW;//tambahan anas 
            item.ACTIVITYDESCRIPTION = tabJenis;
            item.FLAG = flag;//tambahan anas 
            item.AGENTTO = _mutation_line.SITE_ID_NEW;//tambahan anas 
            item.AGENTFROM = _mutation_line.SITE_ID_INTR;//tambahan anas 
            _QRUnitObject.Insert(item);

            Commit();
        }

        //public string getLocation(string MACHINE_ID,string SITE_ID)
        //{
        //    WmsUnitDataContext Context1 = new WmsUnitDataContext();

        //    var data = (from a in Context1.MPMWMS_HISTORY_UNITs
        //                where a.MACHINE_ID.Equals(MACHINE_ID)
        //                && a.SITE_ID.Equals(SITE_ID)
        //                join b in Context1.MPMWMS_WAREHOUSE_LOCATIONs
        //                on new { a.LOCATION_ID, a.SITE_ID } equals new { b.LOCATION_ID, b.SITE_ID }
        //                where b.LOCATION_TYPE.Equals("S")                        
        //                select a.LOCATION_ID).Take(1).FirstOrDefault();


        //    return data.ToString();
        //}

        public List<MPMWMSQRUNIT> getDataByStatusSend(int _sendStatus)
        {
            BeginTransaction();
            try
            {
                //tambahan untuk cek kirim data ke vendor (menambahkan where (parameter) ke AGENTTO, AGENTFROM, ACTIVITYDESCRIPTION)
                var data = (from a in ((WmsMpmLiveDataContext)Context).MPMWMSQRUNITs
                            where a.ISSEND == _sendStatus
                            //&& a.AGENTTO == "Z0180"
                            ////&& a.ENGINENO == "JM71E1048580"
                            //&& a.AGENTFROM == "GD1"
                            //&& a.ACTIVITYDESCRIPTION == "MD TO DEALER"
                            //where a.IDQR == 1177737
                            orderby a.IDQR ascending
                            select a).Take(100).ToList();

                foreach (var item in data)
                {
                    if (item.SITEID == null || item.MAINDEALER == null || item.LOCATIONID == null || item.ACTIVITYDESCRIPTION == null)
                    {
                        var row = _QRUnitObject.ItemUpdate(item.ENGINENO, item.QRCODE);
                        WmsUnitDataContext context1 = new WmsUnitDataContext();
                        var unit = (from a in context1.MPMWMS_UNITs
                                    where a.MACHINE_ID == item.ENGINENO
                                    && a.QR_CODE == item.QRCODE
                                    select a).Take(1).FirstOrDefault();

                        var maindealer = (from a in context1.MPM_USERs
                                          where a.NPK == item.CREATEBY
                                          select a.MAIN_DEALER_ID).Take(1).FirstOrDefault();

                        string tabJenis = (from a in context1.MPMSALMASTERTABLEs
                                           where a.JENISTAB == "QRCODE" && a.KODETAB == "1"
                                           select a.NAMATAB).Take(1).FirstOrDefault();

                        row.SITEID = unit.SITE_ID;
                        row.LOCATIONID = unit.LOCATION_ID;
                        row.ACTIVITYDESCRIPTION = tabJenis;
                        row.MAINDEALER = maindealer;
                    }

                }

                Commit();
                return _QRUnitObject.getDataByStatusSend(_sendStatus);
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void updateQRUnit(String _engineno, int _IDQR, String _responseCode, String _responseMsg, String _responseStatus)
        {
            BeginTransaction();
            try
            {
                var item = _QRUnitObject.getDataByEngine(_engineno, _IDQR);

                if (item != null)
                {
                    switch (_responseStatus)
                    {
                        case "success":
                            item.ISSEND = 1;
                            break;

                        case "error":
                            item.ISSEND = 0;
                            break;

                        default:
                            item.ISSEND = 1;
                            break;
                    }

                    item.SENDDATE = MPMDateUtil.DateTime;
                    item.RESPONSECODE = _responseCode;
                    item.RESPONSEMSG = _responseMsg;
                    item.RESPONSESTATUS = _responseStatus;
                    item.ENGINENO = _engineno;
                    item.IDQR = _IDQR;
                    _QRUnitObject.Update(item);
                }

                Commit();
                Debug.WriteLine(item.IDQR + " Berhasil comit");

            }
            catch (MPMException ex)
            {
                Rollback();
                Debug.WriteLine("gagal update" + _IDQR + ex);
                throw new MPMException(ex.Message);
            }
        }

        public void GetLogsVendorFiltertgl(string tglfilter, string key)
        {
            var itemsParam = new List<Param>();
            try
            {

                var type = ConfigurationManager.AppSettings["Type"];


                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    DateTime wk = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1);
                    //var getDate = wk.ToString("yyyy-MM-dd");
                    var getDate = tglfilter;
                    var time = MPMDateUtil.DateTime.Hour;
                    var url = "";

                    string tmpkey = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImIzNzM2YmI5ODg1NzBhMGFlNGM2ODc3NzRiNDdhYzk3NTAzNmQ5NjVlNDQzNjRkYTBhNWE5NTA1ZmI3YmVkNTkyZTdiMDY5MGUwMWRlZjJhIn0.eyJhdWQiOiI0IiwianRpIjoiYjM3MzZiYjk4ODU3MGEwYWU0YzY4Nzc3NGI0N2FjOTc1MDM2ZDk2NWU0NDM2NGRhMGE1YTk1MDVmYjdiZWQ1OTJlN2IwNjkwZTAxZGVmMmEiLCJpYXQiOjE1Nzg0MTY0MDUsIm5iZiI6MTU3ODQxNjQwNSwiZXhwIjoxNTc4NTAyODA1LCJzdWIiOiIxMzQ3ZTc3MC02ZjhiLTExZTktOTJmZS00OTljNjUxZTVhNWEiLCJzY29wZXMiOltdfQ.Xf_nyxVLQOqltj6AQiImothhZCgo-pGNxovD3rMs-VSuaUw_ZUDXCGgkENHZqcMIDi5bEvJq5C8L69xeh0uoHgYseg4tiqDWmKhB3aNuK9dMLcvw6nzrtmtd4ob0vE04FHRvIkNaJ7ksXVudxLDe91caqUIw1L06nB_m1NOPw7fDTzO0K6dDSuZSaTIS9guDWqFsjgBjgVsV7iWGWqRWyoErtQf_nSy0ZHEXrDTU4FbbgDn7kPiwP8NlVxpxlWil5ONMBcsTSOeRAJwWYuZoVVyqrkPZYK_ZpESsmaZ7aIZ1xW7QuMoYKI6juUgZu-REmhiqnDn80qzAKL4fE120NbmdVzp-R6nSFglC9uNJLxHyJIsFNjN3I3HDz1Dq04XoqwVqqpso-Zw78yeQyyYxzILzSse0vOEzyKS8nnfs8Kw5mxBVrq8l_H5DbYbH-jIHehZ0Z-Hb2C4-uGfwtno6nqjb8hQ9cRkeglkaZlRFkDaumh1D_M7M0vRMEICl4qGAD56hXe9cOL-diZtGavd-UFLrX8B-PKJekhSEvQOoCiwdtgnO6IlTWeI9PpG30uQCRgY70uk_2Xo2EQfKSKhx4PfGCA1pEnaxRSca6AT0xBbbhoABmwcKmqAuyCoy3GfJbwa4BWkTILblOpN2AcqG5FZFgQDwygYBJnhcIImA6MY";
                    //string tmpkey = "Bearer " + key;
                    wc.Encoding = Encoding.UTF8;
                    //wc.Headers.Add("Authorization", "Bearer " + _key);
                    wc.Headers.Add("Authorization", tmpkey);
                    wc.Headers.Add("Accept", "application/json");

                    //param["type"] = type;
                    //param["date"] = Convert.ToString(Date);
                    //param["hour"] = Convert.ToString(time);

                    string[] DataType = { "claim", "customer ", "qrcode", "vehicle", "mapping", "unmatch" };

                    for (int i = 1; i <= 24; i++)
                    {
                        itemsParam.Add(new Param { tipe = DataType[0], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[1], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[2], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[3], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[4], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[5], date = getDate, jam = i });


                    }

                    var tmp = itemsParam.OrderBy(x => x.tipe);

                    foreach (var itemurl in tmp)
                    {
                        //var itemurl1 = itemurl.tipe;
                        //var itemurl2 = itemurl.date;
                        //var itemurl3 = itemurl.jam;
                        url = ConfigurationManager.AppSettings["LogsApi"] + "?type=" + itemurl.tipe + "&date=" + itemurl.date + "&hour=" + itemurl.jam;



                        var geturl = url;
                        //var url = ConfigurationManager.AppSettings["LogsApi"] + "?type=qrcode&date=2019-12-19&hour=07";

                        //token diambil dari ACCESS_TOKEN.MPM_VENDOR server 10.10.101.2

                        var data = wc.DownloadString(url);

                        //string json = Encoding.UTF8.GetString(data);
                        Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(data);

                        if (result["status"].ToString() == "success")
                        {

                            List<RetrievedClass> items = JsonConvert.DeserializeObject<List<RetrievedClass>>(result["data"].ToString());

                            //var getData =  new List<MPMQTRUSTSynchLogAPI>();
                            //var setData = getData.Select(x => x.nologapi).Max();
                            /* comment steven table belum ada
                            foreach (var row in items)
                            {
                                BeginTransaction();
                                try
                                {
                                     
                                    MPMQTRUSTSynchLogAPI item = new MPMQTRUSTSynchLogAPI();
                                    //item.nologapi = setData+1;
                                    item.waktu = Convert.ToDateTime(row.datetime);
                                    item.ip = row.ip;
                                    item.endpoint = row.endpoint;
                                    item.qrcode = row.qrcode;
                                    item.activitydescription = row.activity_description;
                                    item.userid = row.user_id;
                                    item.usertype = row.user_type;
                                    item.status = row.status;
                                    item.idlog = row.idlog;
                                    item.numengine = row.num_engine;
                                    item.nik = row.nik;
                                    item.name = row.name;
                                    item.message = row.message;

                                    //if (row.message.Length >= 1000)
                                    //{
                                    //    item.message = row.message.Substring((row.message.Length - 1000), 1000);
                                    //}
                                    //else
                                    //{
                                    //    item.message = row.message;
                                    //}

                                    VendorStoringUnitObject.InsertlogsApi(item);

                                    Commit();
                                     
                                }
                                catch (MPMException e)
                                {
                                    throw new MPMException(e.Message);
                                }
                            } */
                        }
                    }

                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void GetLogsVendor(string key)
        {
            var itemsParam = new List<Param>();
            try
            {

                var type = ConfigurationManager.AppSettings["Type"];


                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    DateTime wk = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1);
                    var getDate = wk.ToString("yyyy-MM-dd");
                    var time = MPMDateUtil.DateTime.Hour;
                    var url = "";

                    //string tmpkey = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk1MWM2MjBhMmNhYWFjMDZjOGEyNTU0NjRjMmI1NDNmZGNkYjEzYzQ2MmI2ZjU4NGU4MDQwM2NkMWVjN2EzOWU0MDgxNTRmYmZmNjRiMTZiIn0.eyJhdWQiOiI0IiwianRpIjoiOTUxYzYyMGEyY2FhYWMwNmM4YTI1NTQ2NGMyYjU0M2ZkY2RiMTNjNDYyYjZmNTg0ZTgwNDAzY2QxZWM3YTM5ZTQwODE1NGZiZmY2NGIxNmIiLCJpYXQiOjE1NzczNzk2MDUsIm5iZiI6MTU3NzM3OTYwNSwiZXhwIjoxNTc3NDY2MDA1LCJzdWIiOiIxMzQ3ZTc3MC02ZjhiLTExZTktOTJmZS00OTljNjUxZTVhNWEiLCJzY29wZXMiOltdfQ.cE8OjowKja-OhIxYO_Wyh49iQekqtg2FlQ2Nqjr3CFZG3vhDskYe1nQgLmRzF-lm3WUBbSIbjgtlavAu47KYEqsWllwP1nwFpVxAd1ZJt5UewswVvSB6nWMYW8SsJZeaJ4KjZq9sjDbeTfGPfYhvk2jKISKw0iJQbrQ4MzuLmr6ms-shvvOTpKOnZ5Qxb--UdmpoU-FTItF6hdEr8mpPgW2QTQm9TqglODXvDN0lBdSnNzUcTUHumxz1IyPwjeMgLBbz1Em_5AfB3-d0poC8kAlu3J1c5SfttpCFBgMOa39pp3T1J1qrbCuSD7YiU6wHwu4bJ2h3-7mn7vpShTnPhDBPcyhUQBDx3Hy1byeIigw2gNnpl4wtvnUBOu2zm_g2yqNojnBy9YKozE4g8UHHCgjzXT_SaBD9KJYTso7yQ1L_TEEcRqadpPTGYbWm6vCOPivxcVFjCA-evJt_BvC15e1vA0MCrhS-hvflMqqigc3HltZssp3vieQzwkjwjRp6dc4pjf5VXLSTYaQ8xIxC1LlEN-tMzP3O-4q8GUVg3ddeqhAn9thq66GZYNytp7MuJOZ_rofOfAKLeKd_-WUgUivyZioFg_42JRyePRYuf8gLcrmVwdRT8BehOr24HPHCIuOAnCfPoPeqskDvbG0IWS8uNHmV7iiDPiWNzXe_t7Q";
                    string tmpkey = "Bearer " + key;
                    wc.Encoding = Encoding.UTF8;
                    //wc.Headers.Add("Authorization", "Bearer " + _key);
                    wc.Headers.Add("Authorization", tmpkey);
                    wc.Headers.Add("Accept", "application/json");

                    //param["type"] = type;
                    //param["date"] = Convert.ToString(Date);
                    //param["hour"] = Convert.ToString(time);

                    string[] DataType = { "claim", "customer ", "qrcode", "vehicle", "mapping", "unmatch" };

                    for (int i = 1; i <= 24; i++)
                    {
                        itemsParam.Add(new Param { tipe = DataType[0], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[1], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[2], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[3], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[4], date = getDate, jam = i });
                        itemsParam.Add(new Param { tipe = DataType[5], date = getDate, jam = i });


                    }

                    var tmp = itemsParam.OrderBy(x => x.tipe);

                    foreach (var itemurl in tmp)
                    {
                        //var itemurl1 = itemurl.tipe;
                        //var itemurl2 = itemurl.date;
                        //var itemurl3 = itemurl.jam;
                        url = ConfigurationManager.AppSettings["LogsApi"] + "?type=" + itemurl.tipe + "&date=" + itemurl.date + "&hour=" + itemurl.jam;



                        var geturl = url;
                        //var url = ConfigurationManager.AppSettings["LogsApi"] + "?type=qrcode&date=2019-12-19&hour=07";

                        //token diambil dari ACCESS_TOKEN.MPM_VENDOR server 10.10.101.2

                        var data = wc.DownloadString(url);

                        //string json = Encoding.UTF8.GetString(data);
                        Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(data);

                        if (result["status"].ToString() == "success")
                        {

                            List<RetrievedClass> items = JsonConvert.DeserializeObject<List<RetrievedClass>>(result["data"].ToString());
                            /* comment steven table belum ada
                            foreach (var row in items)
                            {
                                BeginTransaction();
                                try
                                {
                                    MPMQTRUSTSynchLogAPI item = new MPMQTRUSTSynchLogAPI();
                                    item.waktu = Convert.ToDateTime(row.datetime);
                                    item.ip = row.ip;
                                    item.endpoint = row.endpoint;
                                    item.qrcode = row.qrcode;
                                    item.activitydescription = row.activity_description;
                                    item.userid = row.user_id;
                                    item.usertype = row.user_type;
                                    item.status = row.status;
                                    item.idlog = row.idlog;
                                    item.numengine = row.num_engine;
                                    item.nik = row.nik;
                                    item.name = row.name;
                                    item.message = row.message;

                                    VendorStoringUnitObject.InsertlogsApi(item);

                                    Commit();
                                }
                                catch (MPMException e)
                                {
                                    throw new MPMException(e.Message);
                                }
                            } */
                        }
                    }

                }
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void sendQRCodeVendor(MPMWMSQRUNIT _data, String _key)
        {
            try
            {
                //bool un = unmatch("db78d1427b69641396bfa13368290987", "JBK1E1658072", "GD1","Update Data");
                var brand = ConfigurationManager.AppSettings["VehicleBrand"];
                var url = ConfigurationManager.AppSettings["VehicleUrl"];
                //var url = "https://staging.qtrust.id/api/agent/vehicle/";
                var userType = ConfigurationManager.AppSettings["VehicleUserType"];

                //bool cekkodeqr = cekqrcodevendore(_data.QRCODE);
                //if (!cekkodeqr)
                //{
                //    throw new MPMException("QR CODE Tidak Valid");
                //}

                using (WebClient wc = new WebClient())
                {
                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "vehicle", year, monthv2, ref runningNumber);

                    var IDAPILOG = "vehicle" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["UnitTypeCode"] = _data.UNITTYPE;//ini tak ubah anong
                    param["Brand"] = brand;
                    param["UnitColorCode"] = _data.COLORNAME;
                    param["NumEngine"] = _data.ENGINENO;
                    param["NumFrame"] = _data.FRAMENO;
                    param["ManufactureYear"] = _data.UNITYEAR;
                    param["UserId"] = _data.CREATEBY;
                    param["UserType"] = userType;
                    param["BranchCode"] = _data.SITEID;//ini tak ubah anong
                    param["RqUuid"] = IDAPILOG;//tambahan anas

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + _key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    ////write vehicleLogList
                    //var path1 = Server.MapPath(@"~/Content/vehicleLogList.txt");
                    //using (StreamWriter writetext = new StreamWriter(path1))
                    //{
                    //    foreach (var writeRow in getNewData)
                    //    {

                    //        writetext.WriteLine(writeRow.FullName);
                    //    }

                    //}

                    //tambahan anas
                    InsertLogVendor(IDAPILOG, url, _data.IDQR, _data.QRCODE, _data.ENGINENO/*, result["code"].ToString()*/);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    UpdateLogVendor(IDAPILOG, result["code"].ToString());


                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //tambahan anas penambahan methode untuk (VehicleUnmatch)
                            if (_data.STSMATCHING == "1" && _data.NosinUnmatch1 != "" && _data.NosinUnmatch1 != null && _data.QrUnmatch1 != "" && _data.QrUnmatch1 != null)
                            {
                                try
                                {
                                    VehicleUnmatch(_data, _data.QrUnmatch1, _data.NosinUnmatch1, _data.SITEID);
                                }
                                catch
                                {

                                }
                            }

                            if (_data.STSMATCHING == "1" && _data.QrUnmatch2 != "" && _data.QrUnmatch2 != null && _data.NosinUnmatch2 != "" && _data.NosinUnmatch2 != null)
                            {
                                try
                                {
                                    VehicleUnmatch(_data, _data.QrUnmatch2, _data.NosinUnmatch2, _data.SITEID);
                                }
                                catch
                                {
                                }
                            }


                            bool b = mappingqrcodevendorelog(_data, _data.QRCODE, _data.ENGINENO, _data.FRAMENO, _data.CREATEBY, userType);

                            //tambahan untuk cek kirim data ke vendor (_data.FLAG, _data.AGENTTO, _data.AGENTFROM)
                            updatetrackqrcodevendore(_data, _data.QRCODE, _data.ENGINENO, _data.FRAMENO, _data.LATITUDE, _data.LONGITUDE, _data.SPGID, _data.SITEID, _data.LOCATIONID, _data.CREATEBY, _data.ACTIVITYDESCRIPTION, _data.FLAG, _data.AGENTTO, _data.AGENTFROM, Convert.ToDateTime(_data.CREATEDATE), Convert.ToDateTime(_data.MODIFDATE));
                            //updatetrackqrcodevendore(_data.QRCODE, _data.ENGINENO, _data.FRAMENO, _data.LATITUDE, _data.LONGITUDE, _data.SPGID, _data.SITEID, _data.LOCATIONID, _data.CREATEBY, _data.ACTIVITYDESCRIPTION);
                            var code = result["code"].ToString();

                            Object checkOut = "";
                            string message = String.Empty;
                            if (result.TryGetValue("message", out checkOut))
                            {
                                message = result["message"].ToString();
                            }

                            var status = result["status"].ToString();

                            updateQRUnit(_data.ENGINENO, _data.IDQR, code, message, status);
                            //updateQRUnit(_data.ENGINENO, code, message, status);

                            if (b == false)
                            {
                                insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = _data.ENGINENO, FRAME_ID = _data.FRAMENO, QRCODE = _data.QRCODE, CREATEBY = _data.CREATEBY, CREATEDATE = DateTime.Now });
                            }
                        }
                        else
                        {
                            insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = _data.ENGINENO, FRAME_ID = _data.FRAMENO, QRCODE = _data.QRCODE, CREATEBY = _data.CREATEBY, CREATEDATE = DateTime.Now });

                            var code = result["code"].ToString();

                            Object checkOut = "";
                            string message = String.Empty;
                            if (result.TryGetValue("message", out checkOut))
                            {
                                message = result["message"].ToString();
                            }

                            var status = result["status"].ToString();

                            updateQRUnit(_data.ENGINENO, _data.IDQR, code, message, status);
                        }
                    }
                    else
                    {
                        insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = _data.ENGINENO, FRAME_ID = _data.FRAMENO, QRCODE = _data.QRCODE, CREATEBY = _data.CREATEBY, CREATEDATE = DateTime.Now });

                        updateQRUnit(_data.ENGINENO, _data.IDQR, "400", "nothing result", "error");
                    }
                }
            }
            catch (MPMException e)
            {
                insertmpmqrcode(new MPMQRCODE_VENDOR { MACHINE_ID = _data.ENGINENO, FRAME_ID = _data.FRAMENO, QRCODE = _data.QRCODE, CREATEBY = _data.CREATEBY, CREATEDATE = DateTime.Now });

                throw new MPMException(e.Message);
            }
        }


        #region DATA CUSTOMER & TRACKING

        //public List<MPM_VENDOR_CUSTOMER> getCustomerQR(int _sendStatus)
        //{
        //    try
        //    {
        //        Context.ExecuteQuery<String>("EXEC [MPM_QRCODE_CUSTOMER]");

        //        return _QRUnitObject.getCustomerQR(_sendStatus);
        //    }
        //    catch (MPMException e)
        //    {
        //        throw new MPMException(e.Message);
        //    }
        //}

        public List<customerPenjualan> getCustomerQR(int _sendStatus)
        {
            try
            {
                Context.ExecuteQuery<String>("EXEC [MPM_QRCODE_CUSTOMER]");

                return _QRUnitObject.getCustomerQR(_sendStatus);
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        //public void sendCustomer(MPM_VENDOR_CUSTOMER _item, String _key)
        public void sendCustomer(customerPenjualan _item, String _key)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["Customer"];
                //var url = "https://staging.qtrust.id/api/agent/customer/";
                string code, message, status;

                var gender = "";
                if (_item.GENDER.ToString() == "N" || _item.GENDER.ToString() == "")
                {
                    gender = "3";
                }
                else
                {
                    gender = _item.GENDER.ToString();
                }

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "Customer", year, monthv2, ref runningNumber);

                    var IDAPILOG = "Customer" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var param = new NameValueCollection();
                    param["CustomerAHMNo"] = _item.CUSTOMER_AHM_NO;
                    param["Fullname"] = _item.NAME;
                    param["Email"] = _item.EMAIL;
                    param["PhoneNumber"] = _item.PHONENUMBER;
                    param["IdentityNumber"] = _item.IDENTITYNUMBER;
                    param["Gender"] = gender;
                    param["Birthdate"] = _item.BIRTHDATE.ToString();
                    param["Postcode"] = _item.POSTCODE;
                    param["Address"] = _item.ADDRESS;
                    param["Province"] = _item.PROVINCE;
                    param["City"] = _item.CITY;
                    param["District"] = _item.DISTRICT;
                    param["UserId"] = "SYSTEM";
                    param["UserType"] = "ADMIN";
                    param["BranchCode"] = _item.KODEDEALER;
                    param["RqUuid"] = IDAPILOG;//tambahan anas

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + _key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    InsertLogCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME/*, result["code"].ToString()*/);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    UpdateLogCustomer(IDAPILOG, result["code"].ToString());

                    //InsertLogCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, result["code"].ToString());
                    ////InsertLogCustomer(url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, result["code"].ToString());

                    //bool claim = mappingpenjualanclaim(_item, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NOFAKTUR, _item.CREATEBY, _item.KODEDEALER, Convert.ToDateTime(_item.CREATEDATE));
                    bool claim = mappingpenjualanprocces(_item, Convert.ToDateTime(_item.BIRTHDATE), Convert.ToDateTime(_item.CREATEDATE));

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        code = result["code"].ToString();

                        status = result["status"].ToString();
                        if (status == "success")
                        {
                            message = "";
                        }
                        else
                        {
                            message = result["message"].ToString();
                        }

                        if (claim)
                        {
                            updateCustomer(_item.CUSTOMER_AHM_NO, code, message, status);
                        }
                    }
                    else
                    {
                        updateCustomer(_item.CUSTOMER_AHM_NO, "400", "nothing result", "error");
                    }
                }
            }
            catch (Exception e)
            {
                updateCustomer(_item.CUSTOMER_AHM_NO, "400", e.Message, "error");
            }
        }

        //tambahan anas (DateTime invoicedate)
        //public bool mappingpenjualanclaim(MPM_VENDOR_CUSTOMER _item, string machine_id, string identitynumber, string nofaktur, string userid, string kddealer, DateTime invoicedate)
        public bool mappingpenjualanclaim(customerPenjualan _item, string machine_id, string identitynumber, string nofaktur, string userid, string kddealer, DateTime invoicedate)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["VehicleClaimUrl"];
                //var url = "https://staging.qtrust.id/api/agent/claim/";

                using (WebClient wc = new WebClient())
                {

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    //WmsMpmLiveDataContext db = new WmsMpmLiveDataContext();
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "VehicleClaimUrl", year, monthv2, ref runningNumber);

                    var IDAPILOG = "VehicleClaimUrl" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var paramclaim = new NameValueCollection();
                    paramclaim["NumEngine"] = machine_id;
                    paramclaim["IdentityNumber"] = identitynumber;
                    paramclaim["Invoice"] = nofaktur;
                    paramclaim["UserId"] = "SYSTEM";
                    paramclaim["UserType"] = "ADMIN";
                    paramclaim["BranchCode"] = kddealer; //tambahan anas
                    paramclaim["InvoiceDate"] = invoicedate.ToString("yyyy-MM-dd HH:mm:ss"); ; //tambahan anas
                    paramclaim["RqUuid"] = IDAPILOG;//tambahan anas
                    paramclaim["Flag"] = _item.NUMBERFLAG;//tambahan anas- flag = '2'

                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var dataclaim = wc.UploadValues(url, "POST", paramclaim);
                    string jsonclaim = Encoding.UTF8.GetString(dataclaim);

                    InsertLogCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(jsonclaim);

                    //InsertLogCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, result["code"].ToString());
                    //InsertLogCustomer(url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, result["code"].ToString());
                    UpdateLogCustomer(IDAPILOG, result["code"].ToString());


                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            updateCustomer(machine_id, result["code"].ToString(), result["message"].ToString(), result["status"].ToString());
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (MPMException e)
            {
                return false;
            }
        }

        //tambahan anas OneProcces
        //public bool mappingpenjualanprocces(MPM_VENDOR_CUSTOMER _item)
        public bool mappingpenjualanprocces(customerPenjualan _item, DateTime BirtDate, DateTime InvoiceDate)
        {
            try
            {
                string key = GetTokenVendore();
                var url = ConfigurationManager.AppSettings["OneProcces"];
                //var url = "https://mpm-motor-staging.qtrust.id/api/agent/vehicle/process"

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                            | SecurityProtocolType.Tls11
                            | SecurityProtocolType.Tls12
                            | SecurityProtocolType.Ssl3;

                    //tambahan anas running numb
                    WmsMPMITModuleDataContext db = new WmsMPMITModuleDataContext();
                    int? runningNumber = 0;
                    int year = MPMDateUtil.DateTime.Year;
                    int month = MPMDateUtil.DateTime.Month;
                    int monthv2 = 13;
                    db.MPMGETRUNNINGNUMBER("WMS", "VehicleClaimUrl", year, monthv2, ref runningNumber);

                    var IDAPILOG = "VehicleClaimUrl" + year + MPMStringUtil.LPAD(month + "", 2, '0') +
                        MPMStringUtil.LPAD(runningNumber.ToString(), 7, '0');

                    var paramprocces = new NameValueCollection();
                    paramprocces["NumEngine"] = _item.CUSTOMER_AHM_NO;
                    paramprocces["Code"] = _item.QR_CODE;
                    paramprocces["ActivityDescription"] = _item.ActivityDescription;
                    paramprocces["BranchCode"] = _item.KODEDEALER;
                    paramprocces["NumFrame"] = _item.FRAME_ID;
                    paramprocces["Brand"] = "Honda";
                    paramprocces["UnitTypeCode"] = _item.UNIT_TYPE;
                    paramprocces["UnitColorCode"] = _item.COLOR_TYPE;
                    paramprocces["ManufactureYear"] = _item.UNIT_YEAR;
                    paramprocces["UserId"] = _item.CREATEBY;
                    paramprocces["UserType"] = "ADMWMS";
                    paramprocces["Claim[IdentityNumber]"] = _item.IDENTITYNUMBER;
                    paramprocces["Claim[Flag]"] = _item.NUMBERFLAG;
                    paramprocces["Claim[Invoice]"] = _item.NOFAKTUR;
                    paramprocces["Claim[InvoiceDate]"] = InvoiceDate.ToString("yyyy-MM-dd HH:mm:ss");
                    paramprocces["Claim[CustomerAHMNo]"] = _item.CUSTOMER_AHM_NO;
                    paramprocces["Claim[Fullname]"] = _item.NAME;
                    paramprocces["Claim[PhoneNumber]"] = _item.PHONENUMBER;
                    paramprocces["Claim[Email]"] = _item.EMAIL;
                    paramprocces["Claim[Gender]"] = _item.GENDER;
                    paramprocces["Claim[Birthdate]"] = BirtDate.ToString("yyyy-MM-dd HH:mm:ss");
                    paramprocces["Claim[Address]"] = _item.ADDRESS;
                    paramprocces["Claim[Postcode]"] = _item.POSTCODE;
                    paramprocces["Claim[Province]"] = _item.PROVINCE;
                    paramprocces["Claim[City]"] = _item.CITY;
                    //paramprocces["Datetime"] = 
                    //paramprocces["RegistrationYear"] = 
                    //paramprocces["PoliceNo"] = 
                    //paramprocces["FunLocCode"] = 
                    //paramprocces["RqUuid"] = 
                    //paramprocces["Claim[village]"] = 




                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + key);
                    var dataclaim = wc.UploadValues(url, "POST", paramprocces);
                    string jsonclaim = Encoding.UTF8.GetString(dataclaim);

                    InsertLogCustomer(IDAPILOG, url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(jsonclaim);

                    UpdateLogCustomer(IDAPILOG, result["code"].ToString());

                    //InsertLogCustomer(url, _item.CUSTOMER_AHM_NO, _item.IDENTITYNUMBER, _item.NAME, result["code"].ToString());

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //StoringModel pdt = new StoringModel();
                            //pdt.updateqrcode(numengine,qrcode);

                            return true;
                        }
                        else
                        {
                            updateCustomer(_item.CUSTOMER_AHM_NO, result["code"].ToString(), result["message"].ToString(), result["status"].ToString());
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (MPMException e)
            {
                return false;
            }
        }
        public void processTrack(VendorTrackObject item, String _key)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["UpdateTrack"];

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["Code"] = item.Code;
                    param["UserId"] = item.UserId;
                    param["UserType"] = item.UserType;
                    param["FunLocCode"] = item.FunLocCode;


                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + _key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //return data_result["token"]["access_token"].ToString();
                        }
                        else
                        {
                            insertTrack(item);
                        }
                    }
                    else
                    {
                        insertTrack(item);
                    }
                }
            }
            catch (Exception e)
            {
                insertTrack(item);
                //throw new Exception(e.Message);
            }
        }

        public void processMapping(CustomerObject item, String _key)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["MappingUrl"];

                using (WebClient wc = new WebClient())
                {
                    var param = new NameValueCollection();
                    param["IdentityNumber"] = item.IdentityNumber;
                    param["Invoice"] = item.Invoice;
                    param["NumEngine"] = item.NumEngine;
                    param["PoliceNo"] = item.PoliceNo;


                    wc.Encoding = Encoding.UTF8;
                    wc.Headers.Add("Authorization", "Bearer " + _key);
                    var data = wc.UploadValues(url, "POST", param);
                    string json = Encoding.UTF8.GetString(data);

                    Dictionary<String, Object> result = JsonConvert.DeserializeObject<Dictionary<String, Object>>(json);

                    // tampilkan message return
                    if (result.Count > 0)
                    {
                        if (result["status"].ToString() == "success")
                        {
                            //return data_result["token"]["access_token"].ToString();
                        }
                        else
                        {
                            insertMapping(item);
                        }
                    }
                    else
                    {
                        insertMapping(item);
                    }
                }
            }
            catch (Exception e)
            {
                insertMapping(item);
                //throw new Exception(e.Message);
            }
        }

        public void updateCustomer(String _engineno, String _responseCode, String _responseMsg, String _responseStatus)
        {
            BeginTransaction();
            try
            {
                var item = _QRUnitObject.getCustomerByEngine(_engineno);

                if (item != null)
                {
                    switch (_responseStatus)
                    {
                        case "success":
                            item.ISSEND = 1;
                            break;

                        case "error":
                            item.ISSEND = 0;
                            break;

                        default:
                            item.ISSEND = 1;
                            break;
                    }

                    //var NilaiRESPONSEMSG = null;


                    item.SENDDATE = DateTime.Now; // MPMDateUtil.DateTime;
                    item.RESPONSECODE = _responseCode;
                    if (_responseMsg.Length >= 50)
                    {
                        item.RESPONSEMSG = _responseMsg.Substring((_responseMsg.Length - 50), 50);
                    }
                    else
                    {
                        item.RESPONSEMSG = _responseMsg;
                    }
                    item.RESPONSESTATUS = _responseStatus;

                    _QRUnitObject.UpdateCustomer(item);
                }

                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
                throw new MPMException(ex.Message);
            }
        }

        private void insertTrack(VendorTrackObject item)
        {
            BeginTransaction();

            try
            {
                MPM_VENDOR_TRACK row = new MPM_VENDOR_TRACK();

                row.CODE = item.Code;
                row.FUNLOCCODE = item.FunLocCode;
                row.USERID = item.UserId;
                row.USERTYPE = item.UserType;

                _QRUnitObject.Context.MPM_VENDOR_TRACKs.InsertOnSubmit(row);

                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        private void insertMapping(CustomerObject item)
        {
            BeginTransaction();

            try
            {
                MPM_VENDOR_MAP_ENGINECUSTOMER insert = new MPM_VENDOR_MAP_ENGINECUSTOMER();

                insert.IDENTITYNUMBER = item.IdentityNumber;
                insert.INVOICE = item.Invoice;
                insert.NUMENGINE = item.NumEngine;
                insert.POLICENUM = item.PoliceNo;
                insert.CREATEBY = "Admin";
                insert.CREATEDATE = DateTime.Now;

                _QRUnitObject.Context.MPM_VENDOR_MAP_ENGINECUSTOMERs.InsertOnSubmit(insert);

                Commit();
            }
            catch (MPMException ex)
            {
                Rollback();
            }
        }

        #endregion

    }
}
