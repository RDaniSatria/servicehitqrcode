﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Expedition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class ExpeditionModel : BaseModel
    {
        private ExpeditionObject _ExpeditionObject = new ExpeditionObject();

        public ExpeditionModel()
            : base()
        {
            _ExpeditionObject.Context = (WmsUnitDataContext)Context;
            _ExpeditionObject.ExpeditionLineObject.Context = (WmsUnitDataContext)Context;
        }

        public ExpeditionObject ExpeditionObject
        {
            get
            {
                return _ExpeditionObject;
            }
        }

        public void Insert(bool submit, MPMWMS_EXPEDITION expedition)
        {
            try
            {
                ExpeditionObject.Insert(expedition);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_EXPEDITION expedition)
        {
            try
            {
                ExpeditionObject.Update(expedition);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, String expedition_id, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                // delete expedition
                MPMWMS_EXPEDITION deletedItem = ExpeditionObject.Item(expedition_id, dataarea_id, maindealer_id, site_id);
                if (deletedItem == null)
                {
                    throw new MPMException("Can't find the expedition !!!");
                }

                if (deletedItem.MPMWMS_EXPEDITION_LINEs.ToList().Count() >= 1)
                {
                    throw new MPMException("Can't delete this expedition, because the line already exists !!!");
                }
                ExpeditionObject.Delete(deletedItem);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void InsertLine(bool submit, MPMWMS_EXPEDITION_LINE expeditionLine)
        {
            try
            {
                MPMWMS_EXPEDITION expedition = ExpeditionObject.Item(expeditionLine.EXPEDITION_ID, expeditionLine.DATAAREA_ID,
                    expeditionLine.MAINDEALER_ID, expeditionLine.SITE_ID);
                ExpeditionObject.ExpeditionLineObject.Insert(expeditionLine);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void UpdateLine(bool submit, MPMWMS_EXPEDITION_LINE expeditionLine)
        {
            try
            {
                MPMWMS_EXPEDITION expedition = ExpeditionObject.Item(expeditionLine.EXPEDITION_ID, expeditionLine.DATAAREA_ID,
                    expeditionLine.MAINDEALER_ID, expeditionLine.SITE_ID);
                ExpeditionObject.ExpeditionLineObject.Update(expeditionLine);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void DeleteLine(bool submit, String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                // delete expedition
                MPMWMS_EXPEDITION_LINE deletedItem = ExpeditionObject.ExpeditionLineObject.Item(expedition_id, police_number, 
                    dataarea_id, maindealer_id, site_id);
                ExpeditionObject.ExpeditionLineObject.Delete(deletedItem);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
