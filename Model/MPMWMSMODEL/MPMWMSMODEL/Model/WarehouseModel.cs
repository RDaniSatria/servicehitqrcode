﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.InventLocation;
using MPMWMSMODEL.Table.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class WarehouseModel: BaseModel
    {
        private InventLocationObject _InventLocationObject = new InventLocationObject();

        public WarehouseModel()
            : base()
        {
            _InventLocationObject.Context = (WmsUnitDataContext)Context;
        }

        public InventLocationObject InventLocationObject
        {
            get { return _InventLocationObject; }
        }

        public void Insert(bool submit, MPMWMS_WAREHOUSE_LOCATION warehouse)
        {
            try
            {
                LocationObject.Insert(warehouse);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_WAREHOUSE_LOCATION warehouse)
        {
            try
            {
                MPMWMS_WAREHOUSE_LOCATION item = LocationObject.Item(warehouse.DATAAREA_ID,warehouse.SITE_ID, warehouse.LOCATION_ID, warehouse.WAREHOUSE_ID);
                item = warehouse;
                LocationObject.Update(item);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, String location_id, String site_id, String warehouse_id, String dataarea_id)
        {
            try
            {
                // delete unit
                MPMWMS_WAREHOUSE_LOCATION deletedItem = LocationObject.Item(dataarea_id, site_id, location_id, warehouse_id);
                if (deletedItem.STOCK_ACTUAL + deletedItem.STOCK_BOOKING > 0)
                {
                    throw new MPMException("Can't delete this location, you must release the stock first !!!");
                }

                LocationObject.Delete(deletedItem);

                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
