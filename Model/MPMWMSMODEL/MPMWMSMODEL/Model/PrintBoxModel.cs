﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Util;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.PackingBox;
using MPMWMSMODEL.Table.BoxProperty;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Mvc.Models;

namespace MPMWMSMODEL.Model
{
    

    public class PrintBoxModel : MPMModel
    {
        private PackingBoxObject _PackingBoxObject = new PackingBoxObject();
        private BoxPropertyObject _BoxPropertyObject = new BoxPropertyObject();

        public PrintBoxModel()
            : base()
        {
            Context = new WmsUnitDataContext();
            _PackingBoxObject.Context = (WmsUnitDataContext)Context;
            _BoxPropertyObject.Context = (WmsUnitDataContext)Context;
        }

        public bool isAvailableIsiBox(String box_id)
        {
            return _PackingBoxObject.IsAvailableIsiBoxPDT(box_id);
        }

        public void InsertOrUpdateBoxProperty(String box_id, decimal weight)
        {
            if (_PackingBoxObject.IsAvailableIsiBoxPDT(box_id) == true)
            {
                //cek apakah available
                MPMWMS_BOXPROPERTy item = _BoxPropertyObject.item(box_id);

                if (item == null)
                {
                    //create
                    MPMWMS_BOXPROPERTy _newMPMWMS_BOXPROPERTy = new MPMWMS_BOXPROPERTy();
                    _newMPMWMS_BOXPROPERTy.BOXID = box_id;
                    _newMPMWMS_BOXPROPERTy.WEIGHT = weight;
                    _newMPMWMS_BOXPROPERTy.CREATEBY = UserSession.NPK;
                    _BoxPropertyObject.Insert(_newMPMWMS_BOXPROPERTy);
                    SubmitChanges();
                }
                else
                {
                    //update
                    item.WEIGHT = weight;
                    item.MODIFBY = UserSession.NPK;
                    _BoxPropertyObject.Update(item);
                    SubmitChanges();
                }
            }
            else
            {
                throw new MPMException("Box Id : "+ box_id +" Tidak Ditemuukan !!!");
            }
        }

        public List<TRANS_PRINTLABELBOX> Label(String BoxId)
        {
            List<TRANS_PRINTLABELBOX> temp = _PackingBoxObject.Label(BoxId);
            MPMWMS_BOXPROPERTy item_boxproperty = _BoxPropertyObject.item(BoxId);
            String berat = "0";

            if (item_boxproperty != null)
            {
                berat = item_boxproperty.WEIGHT.ToString();
            }

            temp.ForEach(s=>s.WEIGHT = berat);

            
            /*
            //set berat
            temp.WEIGHT = item_boxproperty.WEIGHT.ToString();

            //set kota
            if (temp.KOTA != "")
            {
                if (temp.KOTA.Length > 13)
                {
                    temp.KOTA = temp.KOTA.Substring(0, 13);
                }
            }
            */

            return temp;
            
 

        }

        
    }
}
