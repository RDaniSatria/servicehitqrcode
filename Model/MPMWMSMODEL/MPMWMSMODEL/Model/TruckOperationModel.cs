﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.TruckOperation;
using MPMWMSMODEL.Table.ExpeditionLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class TruckOperationModel : BaseModel
    {
         private TruckOperationObject _TruckOperationObject = new TruckOperationObject();
         private ExpeditionLineObject _ExpeditionLineObject = new ExpeditionLineObject();

         public TruckOperationModel()
            : base()
        {
            _ExpeditionLineObject.Context = (WmsUnitDataContext)Context;
            _TruckOperationObject.Context = (WmsUnitDataContext)Context;
        }

         public TruckOperationObject TruckOperationObject
        {
            get
            {
                return _TruckOperationObject;
            }
        }

        public void Insert(bool submit, MPMWMS_EAD_TRUCK_OPERATION truckoperation)
        {
            try
            {
                TruckOperationObject.Insert(truckoperation);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_EAD_TRUCK_OPERATION truckoperation)
        {
            try
            {
                TruckOperationObject.Update(truckoperation);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, DateTime date, int number, String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            try
            {
                // delete expedition
                MPMWMS_EAD_TRUCK_OPERATION deletedItem = TruckOperationObject.Item(date, number, expedition_id, police_number, dataarea_id, maindealer_id, site_id);
                if (deletedItem == null)
                {
                    throw new MPMException("Can't find the truck operation !!!");
                }

                TruckOperationObject.Delete(deletedItem);
                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public List<TRUCK_OPERATION> List(String _status)
        {
            return TruckOperationObject.List(_status);
        }

        public MPMWMS_EXPEDITION_LINE ItemExpeditionLine(String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            return _ExpeditionLineObject.Item(expedition_id, police_number, dataarea_id, maindealer_id, site_id);
        }

        public MPMWMS_EAD_TRUCK_OPERATION Item(DateTime date, int number, String expedition_id, String police_number, String dataarea_id, String maindealer_id, String site_id)
        {
            return TruckOperationObject.Item(date, number, expedition_id, police_number, dataarea_id, maindealer_id, site_id);
        }
        //public MPMWMS_EAD_TRUCK_OPERATION ItemNumber(String no )
        //{
        //    return TruckOperationObject.ItemNumber(no);
        //}
    }
}
