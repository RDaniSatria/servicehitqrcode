﻿using MPM365SERVICE.Respond;
using MPM365SERVICE.Service.MPMMuliaService;
using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Mvc.Models;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.Bor;
using MPMWMSMODEL.Table.SpgIncludeBomDetail;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class BorUnitModel : BaseModel//MPMModel
    {

        private BorObject _BorObject = new BorObject();
        private UnitObject _UnitObject = new UnitObject();
        private BundleUnitAccModel _BundleUnitAccModel = new BundleUnitAccModel();
        private StoringModel _StoringModel = new StoringModel();
        public bool checkJournal = false;

        public BorUnitModel()
            : base()
        {
            Context = new WmsUnitDataContext();
            _StoringModel.Context = (WmsUnitDataContext)Context;
            _UnitObject.Context = (WmsUnitDataContext)Context;
            _BorObject.Context = (WmsUnitDataContext)Context;
            _BundleUnitAccModel.Context = (WmsUnitDataContext)Context;
        }

        public List<MPMWMS_SP_AUTOBOR_LISTResult> ListAutoBor(String SITEID)
        {
            return _BorObject.ListAutoBor(SITEID);
        }

        public List<MPMWMS_SP_AUTOBOR_DETAILUNITResult> ListAutoBorDetailUnit(String JOURNALCOLLECTIVEID)
        {
            return _BorObject.ListAutoBorDetailUnit(JOURNALCOLLECTIVEID);
        }

        public List<MPMWMS_SP_AUTOBOR_DETAILPARTResult> ListAutoBorDetailPart(String JOURNALCOLLECTIVEID)
        {
            return _BorObject.ListAutoBorDetailPart(JOURNALCOLLECTIVEID);
        }

        public Response Process(String JOURNALCOLLECTIVEID)
        {
            Response response = new Response();
            try {
                //var checkJournal = false;

                var detailbomunit = this.ListAutoBorDetailUnit(JOURNALCOLLECTIVEID)
                    .Where(x => x.QTYPROCESSED != x.QTYBOR)
                    .Select(x=>new AUTOJOURNALBOR { 
                        JOURNALBOMID = x.JOURNALBOMID,
                        QTYBOR = x.QTYBOR,
                        UNITTYPE = x.UNITTYPE,
                        UNITCOLOR = x.UNITCOLOR,
                        QTYPROCESSED = x.QTYPROCESSED
                    }).Distinct().ToList();

                foreach (var a in detailbomunit)
                {
                    var units = _BorObject.ListProcessBOM(a.JOURNALBOMID,a.UNITTYPE,a.UNITCOLOR).Take((a.QTYBOR - a.QTYPROCESSED)).ToList();
                    
                    if (units.Count() != (a.QTYBOR - a.QTYPROCESSED)) // unit count = (qtybor - qtyprocessed)
                    {
                        throw new MPMException("Pada Journal BOM " + a.JOURNALBOMID + " Kode Type : " + a.UNITTYPE + " Kode Warna : " + a.UNITCOLOR + " Qty : " + units.Count() + " lebih kecil dari " + ((int)a.QTYBOR - (int)a.QTYPROCESSED));
                    }

                    try
                    {
                        var bomdetail = _BorObject.FindJournalBomId(a.JOURNALBOMID);
                        //bomdetail.STATUS = 1;
                        if (bomdetail.STATUS != 2)
                        {
                            throw new Exception("data error");
                        }
                        else
                        {
                            checkJournal = true;
                        }
                    }
                    catch (Exception exc)
                    {
                        response.Success = false;
                        response.Message = "Proses BOR WMS tidak bisa dilakukan, karena detail jurnal BOM "+ a.JOURNALBOMID +" belum melalui proses BOM WMS";
                        checkJournal = false;
                    }

                    if (checkJournal == true)
                    {
                        try
                        {
                            foreach (var processed in units)
                            {
                                var item = _BundleUnitAccModel.BundleUnitWithoutPrint("",processed.MACHINE_ID,
                                processed.UNIT_TYPE, processed.COLOR_TYPE,
                                processed.SITE_ID, processed.WAREHOUSE_ID,
                                "BOR");

                                if (item == null)
                                {
                                    throw new Exception("Can't find another item");
                                }

                                //proses scan by qr code
                                //string qrcode = processed.QR_CODE != null ? processed.QR_CODE : "";
                                //if (qrcode != "")
                                //{
                                //    _StoringModel.ProsesTrackingQRnoWS4(processed.QR_CODE, "4", "IN");

                                //    var getjournalid = _StoringModel.getjurnalbundelbor(processed.UNIT_TYPE, processed.COLOR_TYPE, processed.SITE_ID, processed.WAREHOUSE_ID);//tambahan anas
                                //    _StoringModel.ProsesTrackingQRnoWSBundelBor(getjournalid, processed.QR_CODE, "4", "IN",item._TYPE, item._COLOR, processed.SITE_ID, processed.WAREHOUSE_ID);//tambahan anas

                                //}
                            }
                        } catch (Exception excp)
                        {
                            checkJournal = false;
                            throw new Exception(excp.Message);
                        }
                        
                    }
                }

                if (checkJournal == true)
                {
                    try
                    {
                        var newdetailbomunit = this.ListAutoBorDetailUnit(JOURNALCOLLECTIVEID)
                                            .Where(x => x.QTYPROCESSED != x.QTYBOR)
                                            .Select(x => new AUTOJOURNALBOR
                                            {
                                                JOURNALBOMID = x.JOURNALBOMID,
                                                QTYBOR = x.QTYBOR,
                                                UNITTYPE = x.UNITTYPE,
                                                UNITCOLOR = x.UNITCOLOR,
                                                QTYPROCESSED = x.QTYPROCESSED
                                            }).Distinct().ToList();

                        //perbandingan sum qtybor dan qtyprocessed
                        var detailbomunit_count = newdetailbomunit
                            .GroupBy(item=>item.UNITTYPE)
                            .Select(x => new AUTOJOURNALBOR
                        {
                            QTYBOR = x.Sum((item => item.QTYBOR)),
                            QTYPROCESSED = x.Sum((item => item.QTYPROCESSED)),
                        }).ToList();



                        //BeginTransaction();

                        response.Success = true;
                        response.Message = "Success";
                        response.Data = JOURNALCOLLECTIVEID;

                        foreach (var check_qtybor in detailbomunit_count)
                        {
                            if (check_qtybor.QTYBOR != check_qtybor.QTYPROCESSED)
                            {
                                response.Success = false;
                                response.Message = "Jurnal proses BOR belum terproses sepenuhnya";
                                response.Data = JOURNALCOLLECTIVEID;
                            } 
                        }


                        //Commit();

                        ////MPMWMS_AUTOBORHISTORY history = new MPMWMS_AUTOBORHISTORY();
                        ////history.JOURNALCOLLECTIVEID = JOURNALCOLLECTIVEID;
                        ////history.TANGGALPROSES = DateTime.Now;
                        ////_BorObject.InsertHistory(history);

                        ////Commit();
                        ////response.Success = true;
                        ////response.Message = "Success";
                        ////response.Data = JOURNALCOLLECTIVEID;

                        // jika tidak sama message : Jurnal proses bor belum terproses sepenuhnya


                    }
                    catch (Exception excp2)
                    {
                        //Rollback();
                        throw new Exception(excp2.Message);
                    }
                    
                }
                                
            }
            catch(Exception e)
            {
                //Rollback();
                response.Success = false;
                response.Message = e.Message;
            }
            return response;
        }

    }

    
    public class Response
    {
        public bool Success { get; set; }
        public String Message { get; set; }
        public String Data { get; set; }
    }

    public class AUTOJOURNALBOR
    {
        public String JOURNALBOMID { get; set; }
        public int QTYBOR { get; set; }
        public String UNITTYPE { get; set; }
        public String UNITCOLOR { get; set; }
        public int QTYPROCESSED { get; set; }
    }
}
