﻿using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.DashboardSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class DashboardSOModel: BaseModel
    {
        public DashboardSOObject _DashboardSOObject = new DashboardSOObject();
    
        public DashboardSOModel()
            : base()
        {
            _DashboardSOObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
