﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.Box;
using MPMWMSMODEL.Table.BoxBarcode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class BoxBarcodeModel : BaseModel
    {
        public BoxBarcodeObject _BoxBarcodeObject = new BoxBarcodeObject();

        public BoxBarcodeModel()
            : base()
        {
            _BoxBarcodeObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public void Insert(MPMWMS_BOX_BARCODE _item)
        {
            MPMWMS_BOX_BARCODE dataNew = new MPMWMS_BOX_BARCODE();

            BeginTransaction();

            try
            {
                var row = _BoxBarcodeObject.Item(_item);

                if (row == null)
                {
                    dataNew.YEARLY = _item.YEARLY;
                    dataNew.MONTHLY = _item.MONTHLY;
                    dataNew.COLLIECODE = _item.COLLIECODE;
                    dataNew.DATAAREAID = _item.DATAAREAID;
                    dataNew.MAINDEALERID = _item.MAINDEALERID;
                    dataNew.SITEID = _item.SITEID;
                    dataNew.VALUE = _item.VALUE;
                }
                else
                {
                    throw new MPMException("Item already exist");
                }

                _BoxBarcodeObject.Insert(dataNew);
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_BOX_BARCODE item)
        {
            BeginTransaction();
            try
            {
                var row = _BoxBarcodeObject.Item(item);

                if (row == null)
                {
                    throw new MPMException("Item update can't found");
                }

                row.VALUE = row.VALUE + item.VALUE;

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_BOX_BARCODE _item)
        {
            BeginTransaction();
            try
            {
                var row = _BoxBarcodeObject.Item(_item);

                if (row != null)
                {
                    _BoxBarcodeObject.Delete(row);
                }
                else
                {
                    throw new MPMException("Item delete can't found");
                }
                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MonitoringBox_Record> getMonitoringBoxReport()
        {
            String QueryitemsObj =
                        "SELECT NOMOR = CONVERT(INT, ROW_NUMBER() OVER(ORDER BY P.BOXID, J.SALESID, T.ITEMID ASC)) " +
                        ", NPK = J.XTSLOCKEDUSERID " +
                        ", NAMA = U.USER_NAME " +
                        ", P.BOXID  " +
                        ", DIMENSION = CONVERT(DECIMAL(18,2), B.DIMENSION) " +
                        ", WEIGHT = CONVERT(DECIMAL(18,2), B.WEIGHT) " +
                        ", DEALER = P.DEALERCODE " +
                        ", J.SALESID " +
                        ", PACKINGID = J.PACKINGSLIPID " +
                        ", T.ITEMID " +
                        ", QTY = CONVERT(INT, T.QTY) " +
                        "FROM MPMWMS_PACKINGBOX (NOLOCK) P " +
                        "JOIN MPMWMS_BOXPROPERTIES (NOLOCK) B ON P.BOXID = B.BOXID " +
                        "JOIN CUSTPACKINGSLIPJOUR (NOLOCK) J ON J.PACKINGSLIPID = P.PACKINGID AND P.DEALERCODE = J.ORDERACCOUNT AND J.DATAAREAID = B.DATAAREAID AND J.PRINTMGMTSITEID = B.SITEID " +
                        "JOIN CUSTPACKINGSLIPTRANS (NOLOCK) T ON T.PACKINGSLIPID = J.PACKINGSLIPID AND T.ITEMID = P.PARTNO AND T.DATAAREAID = J.DATAAREAID " +
                        "JOIN MPM_USER (NOLOCK) U ON U.NPK = J.XTSLOCKEDUSERID " +
                        "ORDER BY P.BOXID, J.SALESID, T.ITEMID";

            List<MonitoringBox_Record> List = Context.ExecuteQuery<MonitoringBox_Record>(QueryitemsObj).ToList();

            return List;
        }

        public List<String> getBarcodeNumber
            (
                  int _numberBarcode
                , int _year
                , int _month
                , String _collieCode
                , String _site
                , String _mainDealer
                , String _dataArea
            )
        {
            return _BoxBarcodeObject.listBarcodeNumber(_numberBarcode, _year, _month, _collieCode, _site, _mainDealer, _dataArea);
        }
    }
}
