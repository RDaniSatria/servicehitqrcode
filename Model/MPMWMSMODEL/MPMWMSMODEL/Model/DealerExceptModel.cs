﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.DealerExcept;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class DealerExceptModel : BaseModel
    {
         private DealerExceptObject _DealerExceptObject = new DealerExceptObject();

         public DealerExceptModel()
            : base()
        {
            _DealerExceptObject.Context = (WmsUnitDataContext)Context;
        }

         public DealerExceptObject DealerExceptObject
        {
            get
            {
                return _DealerExceptObject;
            }
        }

        public void InsertDealerExcept(bool submit, MPMWMS_EAD_DEALER_EXCEPT dealerexcept)
        {
             try
             {
                 MPMWMS_EAD_DEALER_EXCEPT data = DealerExceptObject.ItemDealerExcept(dealerexcept.DATE, dealerexcept.DEALER_CODE);

                 if (data != null)
                 {
                     throw new MPMException("Dealer Except data is already exist !!!");
                 }

                 DealerExceptObject.InsertDealerExcept(dealerexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void UpdateDealerExcept(bool submit, MPMWMS_EAD_DEALER_EXCEPT dealerexcept)
         {
             try
             {
                 DealerExceptObject.UpdateDealerExcept(dealerexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void DeleteDealerExcept(bool submit, DateTime date, string dealercode)
         {
             try
             {
                 // delete expedition
                 MPMWMS_EAD_DEALER_EXCEPT deletedItem = DealerExceptObject.ItemDealerExcept(date, dealercode);
                 if (deletedItem == null)
                 {
                     throw new MPMException("Can't find the dealer except !!!");
                 }

                 DealerExceptObject.DeleteDealerExcept(deletedItem);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public List<MPMWMS_EAD_DEALER_EXCEPT> ListDealerExcept()
         {
             return DealerExceptObject.ListDealerExcept();
         }

         public List<DEALER_EXCEPT> ListDataDealerExcept()
         {
             return DealerExceptObject.ListDataDealerExcept();
         }

         public MPMWMS_EAD_DEALER_EXCEPT ItemDealerExcept(DateTime date, string dealercode)
         {
             return DealerExceptObject.ItemDealerExcept(date, dealercode);
         }

         public void InsertDealerDOExcept(bool submit, MPMWMS_EAD_DEALER_DO_EXCEPT dealerdoexcept)
         {
             try
             {
                 MPMWMS_EAD_DEALER_DO_EXCEPT data = DealerExceptObject.ItemDealerDOExcept(dealerdoexcept.DATE, dealerdoexcept.DEALER_CODE, dealerdoexcept.DO_ID);

                 if (data != null)
                 {
                     throw new MPMException("Dealer DO Except data is already exist !!!");
                 }
                 DealerExceptObject.InsertDealerDOExcept(dealerdoexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void UpdateDealerDOExcept(bool submit, MPMWMS_EAD_DEALER_DO_EXCEPT dealerdoexcept)
         {
             try
             {
                 DealerExceptObject.UpdateDealerDOExcept(dealerdoexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void DeleteDealerDOExcept(bool submit, DateTime date, string dealercode, string doid)
         {
             try
             {
                 // delete expedition
                 MPMWMS_EAD_DEALER_DO_EXCEPT deletedItem = DealerExceptObject.ItemDealerDOExcept(date, dealercode, doid);
                 if (deletedItem == null)
                 {
                     throw new MPMException("Can't find the dealer DO except !!!");
                 }

                 DealerExceptObject.DeleteDealerDOExcept(deletedItem);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public List<MPMWMS_EAD_DEALER_DO_EXCEPT> ListDealerDOExcept()
         {
             return DealerExceptObject.ListDealerDOExcept();
         }

         public List<DEALER_EXCEPT> ListDataDealerDOExcept()
         {
             return DealerExceptObject.ListDataDealerDOExcept();
         }

         public MPMWMS_EAD_DEALER_DO_EXCEPT ItemDealerDOExcept(DateTime date, string dealercode, string doid)
         {
             return DealerExceptObject.ItemDealerDOExcept(date, dealercode, doid);
         }

         public void InsertDealerTypeColorExcept(bool submit, MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT dealertypedoexcept)
         {
             try
             {
                 MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT data = DealerExceptObject.ItemDealerTypeColorExcept(dealertypedoexcept.DATE, dealertypedoexcept.DEALER_CODE, dealertypedoexcept.TYPE, dealertypedoexcept.COLOR);

                 if (data != null)
                 {
                     throw new MPMException("Dealer Type Color data is already exist !!!");
                 }
                 DealerExceptObject.InsertDealerTypeColorExcept(dealertypedoexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void UpdateDealerTypeColorExcept(bool submit, MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT dealertypedoexcept)
         {
             try
             {
                 DealerExceptObject.UpdateDealerTypeColorExcept(dealertypedoexcept);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public void DeleteDealerTypeColorExcept(bool submit, DateTime date, string dealercode, string tipe, string warna)
         {
             try
             {
                 // delete expedition
                 MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT deletedItem = DealerExceptObject.ItemDealerTypeColorExcept(date, dealercode, tipe, warna);
                 if (deletedItem == null)
                 {
                     throw new MPMException("Can't find the dealer type color except !!!");
                 }

                 DealerExceptObject.DeleteDealerTypeColorExcept(deletedItem);
                 if (submit) SubmitChanges();
             }
             catch (MPMException e)
             {
                 throw new MPMException(e.Message);
             }
         }

         public List<MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT> ListDealerTypeColorExcept()
         {
             return DealerExceptObject.ListDealerTypeColorExcept();
         }
         public List<DEALER_EXCEPT> ListDataDealerTypeColorExcept()
         {
             return DealerExceptObject.ListDataDealerTypeColorExcept();
         }
         public MPMWMS_EAD_DEALER_TYPE_COLOR_EXCEPT ItemDealerTypeColorExcept(DateTime date, string dealercode, string tipe, string warna)
         {
             return DealerExceptObject.ItemDealerTypeColorExcept(date, dealercode, tipe, warna);
         }
    }
}
