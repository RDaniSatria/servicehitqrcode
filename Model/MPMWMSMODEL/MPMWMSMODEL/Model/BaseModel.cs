﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Lib.Print;
using MPMLibrary.NET.Lib.Util;
using MPMLibrary.NET.Mvc.Models;
using MPMWMSMODEL.Lov;
using MPMWMSMODEL.Lov.DataArea;
using MPMWMSMODEL.Lov.Dealer;
using MPMWMSMODEL.Lov.Expedition;
using MPMWMSMODEL.Lov.Location;
using MPMWMSMODEL.Lov.Mutation;
using MPMWMSMODEL.Lov.ReceivingPart;
using MPMWMSMODEL.Lov.ShippingList;
using MPMWMSMODEL.Lov.Site;
using MPMWMSMODEL.Lov.Unit;
using MPMWMSMODEL.Lov.Warehouse;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.EcoResProduct;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.RunningNumber;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMWMSMODEL.View.Product;
using MPMWMSMODEL.Table.Setting;
using MPMWMSMODEL.Table.VendorStoringUnit;
using MPMWMSMODEL.Table.ClockingUnit;
using MPMWMSMODEL.Table.HistClocking;
using MPMWMSMODEL.Table.QrUnit;

namespace MPMWMSMODEL.Model
{
    public class BaseModel: MPMModel
    {
        protected String UrlNetTcpXtsMuliaService = "net.tcp://10.10.101.14:8201/DynamicsAx/Services/xtsMuliaService";
        protected String UrlNetTcpMPMWMSPartService = "net.tcp://10.10.101.14:8201/DynamicsAx/Services/MPMWMSPartService";

        protected String LogonASUser = "MPMNET\\adminax";
        protected String DomainWS = "MPMNET";
        protected String UsernameWS = "adminax";
        protected String PasswordWS = "P@ssw0rd";

        private SettingDetailObject _SettingDetailObject = new SettingDetailObject();
        private UnitObject _UnitObject = new UnitObject();

        public QrUnitObject _QrUnitObject = new QrUnitObject();


        private LocationObject _LocationObject = new LocationObject();
        private EcoResProductObject _EcoResProductObject = new EcoResProductObject();
        private ProductDescriptionViewObject _ProductDescriptionViewObject = new ProductDescriptionViewObject();

        private ClockingUnitObject _ClockingUnitObject = new ClockingUnitObject();
        private HistClockingObject _HistClockingObject = new HistClockingObject();

        public System.Data.Linq.DataContext ContextIT = new WmsMPMITModuleDataContext();

        public BaseModel()
            : base()
        {
            Context = new WmsUnitDataContext();


            _RunningNumberObject.Context = (WmsUnitDataContext)Context;
            _UnitObject.Context = (WmsUnitDataContext)Context;
            _UnitObject.HistoryUnitObject.Context = _UnitObject.Context;
            _LocationObject.Context = (WmsUnitDataContext)Context;
            _EcoResProductObject.Context = (WmsUnitDataContext)Context;
            _ProductDescriptionViewObject.Context = (WmsUnitDataContext)Context;
            _ClockingUnitObject.Context = (WmsUnitDataContext)Context;
            _HistClockingObject.Context = (WmsUnitDataContext)Context;


            // preparation for web service connection to AX
            _SettingDetailObject.Context = (WmsUnitDataContext)Context;
            try {
                //MPM_SETTING_DETAIL item = _SettingDetailObject.Item("AX_WS", "WMS_UNIT", "WMS_UNIT");
                //if (item != null)
                //{
                //    UrlNetTcpXtsMuliaService = item.TAG_1;

                //}

                //item = _SettingDetailObject.Item("AX_WS", "WMS_PART", "WMS_PART");
                //if (item != null)
                //{
                //    UrlNetTcpMPMWMSPartService = item.TAG_1;

                //}

                //item = _SettingDetailObject.Item("AX_WS", "USERNAME", "PASSWORD");
                //if (item != null)
                //{
                //    LogonASUser = item.TAG_3 + "\\" + item.TAG_1;
                //    DomainWS = item.TAG_3;
                //    UsernameWS = item.TAG_1;
                //    PasswordWS = item.TAG_2;
                //}
            } catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public UnitObject UnitObject
        {
            get
            {
                return _UnitObject;
            }
        }


        public QrUnitObject QrUnitObject
        {
            get
            {
                return _QrUnitObject;
            }
        }



        public HistClockingObject HistClockingObject
        {
            get
            {
                return _HistClockingObject;
            }
        }
        public ClockingUnitObject ClockingUnit
        {
            get
            {
                return _ClockingUnitObject;
            }
        }

        public LocationObject LocationObject
        {
            get
            {
                return _LocationObject;
            }
        }

        public EcoResProductObject EcoResProductObject
        {
            get
            {
                return _EcoResProductObject;
            }
        }

        public ProductDescriptionViewObject ProductDescriptionViewObject
        {
            get
            {
                return _ProductDescriptionViewObject;
            }
        }

        
        /********** DATA AREA *********/
        public LovDataAreaObject LovDataArea = new LovDataAreaObject();
        /********** END OF DATA AREA *********/

        public LovUnitTypeObject LovUnitType = new LovUnitTypeObject();
        public LovColorTypeObject LovColorType = new LovColorTypeObject();

        /********** SITE *********/
        private LovSiteObject lovSite = new LovSiteObject();
        public LovSiteObject LovSite
        {
            get { return lovSite; }
        }
        /********** END OF SITE *********/

        /********** LOCATION *********/
        private LovLocationObject lovLocation = new LovLocationObject();
        public LovLocationObject LovLocation
        {
            get { return lovLocation; }
        }
        /********** END OF LOCATION *********/

        /********** WAREHOUSE *********/
        private LovWarehouseObject lovWarehouse = new LovWarehouseObject();
        public LovWarehouseObject LovWarehouse
        {
            get { return lovWarehouse; }
        }
        /********** END OF LOCATION *********/

        /********** DEALER *********/
        private LovDealerObject lovDealer = new LovDealerObject();
        public LovDealerObject LovDealerObject
        {
            get { return lovDealer; }
        }
        /********** END OF DEALER *********/

        /********** EXPEDITION *********/
        private LovExpeditionObject lovExpedition = new LovExpeditionObject();
        public LovExpeditionObject LovExpedition
        {
            get { return lovExpedition; }
        }
        /********** END OF EXPEDITION *********/

        /********** RECEIVING *********/
        private LovReceivingPartObject lovReceivingPart= new LovReceivingPartObject();
        public LovReceivingPartObject LovReceivingPart
        {
            get { return lovReceivingPart; }
        }
        /********** END OF RECEIVING *********/

        /********** EXPEDITION POLICE NUMBER *********/
        private LovExpeditionPoliceNumberObject lovExpeditionPoliceNumber = new LovExpeditionPoliceNumberObject();
        public LovExpeditionPoliceNumberObject LovExpeditionPoliceNumberObject
        {
            get { return lovExpeditionPoliceNumber; }
        }
        /********** END OF EXPEDITION POLICE NUMBER *********/

        /********** EXPEDITION POLICE NUMBER *********/
        private LovMachineObject lovMachine = new LovMachineObject();
        public LovMachineObject LovMachine
        {
            get { return lovMachine; }
        }
        /********** END OF EXPEDITION POLICE NUMBER *********/

        /********** MUTATION SITE *********/
        private LovMutationObject lovMutation = new LovMutationObject();
        public LovMutationObject LovMutation
        {
            get { return lovMutation; }
        }
        /********** END OF MUTATION SITE *********/

        /********** SHIPPING LIST *********/
        private LovShippingListObject lovShippingList = new LovShippingListObject();
        public LovShippingListObject LovShippingList
        {
            get { return lovShippingList; }
        }
        /********** END OF SHIPPING LIST *********/

        /********** LOV TRUCK SHIPPING LIST *********/
        private LovTruckShippingListObject lovTruckShippingList = new LovTruckShippingListObject();
        public LovTruckShippingListObject LovTruckShippingList
        {
            get { return lovTruckShippingList; }
        }
        /********** END OF LOV TRUCK SHIPPING LIST *********/

        /********** RUNNING NUMBER *********/
        private RunningNumberObject _RunningNumberObject = new RunningNumberObject();
        public RunningNumberObject RunningNumberObject
        {
            get { return _RunningNumberObject; }
        }

        public String RunningNumberPickingUnit(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "PSL" + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, year);
            

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = "A";
                running.YEAR = year;
                running.MONTH = 0;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" + running.CODE_TAG_2 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 4, '0') + "/" + year + "/" + dataarea_id;

            running.NUMBER += 1;
            String tag_2 = running.CODE_TAG_2;
            // update running number
            if (running.NUMBER > 9999)
            {
                running.NUMBER = 1;
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = Convert.ToChar(Convert.ToByte(tag_2[0]) + 1) + "";
                running.YEAR = year;
                running.MONTH = 0;
                RunningNumberObject.Insert(running);
            }
            SubmitChanges();

            return value;
        }

        public String RunningNumberPickingPart(String dataarea_id)
        {
            String code_tag_1 = "PK";
            int year = MPMDateUtil.GetCurrentYearValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, year);

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = "A";
                running.YEAR = year;
                running.MONTH = 0;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" + dataarea_id + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 6, '0') + "/" + year;

            running.NUMBER += 1;

            // update running number
            if (running.NUMBER > 999999)
            {
                running.NUMBER = 1;
                running.CODE_TAG_2 = "";
            }
            SubmitChanges();

            return value;
        }

        public String RunningNumberPackingPart(String dataarea_id)
        {
            String code_tag_1 = "PA";
            int year = MPMDateUtil.GetCurrentYearValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, year);

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = "A";
                running.YEAR = year;
                running.MONTH = 0;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" + dataarea_id + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 6, '0') + "/" + year;

            running.NUMBER += 1;

            // update running number
            if (running.NUMBER > 999999)
            {
                running.NUMBER = 1;
                running.CODE_TAG_2 = "";
            }
            SubmitChanges();

            return value;
        }

        public String RunningNumberDosPart(String dataarea_id)
        {
            String code_tag_1 = "BOX";
            String code_tag_2 = dataarea_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 5, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberReceivingPart(String dataarea_id)
        {
            String code_tag_1 = "RCP";
            String code_tag_2 = dataarea_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberStoringPart(String dataarea_id)
        {
            String code_tag_1 = "STP";
            String code_tag_2 = dataarea_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberShippingListUnit(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "SLU";
            String code_tag_2 = dataarea_id + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 8, '0') + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" +
                year + "/" + maindealer_id + "/" + site_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberReturShippingListUnit(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "RET-SLU";
            String code_tag_2 = dataarea_id + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
                //SubmitChanges();
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 8, '0') + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" +
                year + "/" + dataarea_id + "/" + maindealer_id + "/" + site_id;

            running.NUMBER += 1;
            SubmitChanges();
            
            return value;
        }

        public String RunningNumberShippingListPart(String dataarea_id)
        {
            String code_tag_1 = "SLP";
            String code_tag_2 = dataarea_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" +
                year + "/" + dataarea_id;
            //StringUtil.LPAD(month + "", 2, '0') + "/" 

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberMutationLocation(String dataarea_id, String site_id)
        {
            String code_tag_1 = "MTL";
            String code_tag_2 = site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" + 
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;

            // update running number
            if (running.NUMBER > 999)
            {
                running.NUMBER = 1;
            }
            SubmitChanges();

            return value;
        }

        public String RunningNumberMutationSite(String dataarea_id, String site_id)
        {
            String code_tag_1 = "MTS";
            String code_tag_2 = site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" + site_id + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 6, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;

            // update running number
            if (running.NUMBER > 999)
            {
                running.NUMBER = 1;
            }
            SubmitChanges();

            return value;
        }

        public String RunningNumberMutationRack(String dataarea_id, String site_id)
        {
            String code_tag_1 = "MTR";
            String code_tag_2 = site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);

            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
                SubmitChanges();
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id;

            running.NUMBER += 1;

            // update running number
            if (running.NUMBER > 999)
            {
                running.NUMBER = 1;
            }
            SubmitChanges();

            return value;
        }

        public String NewDefaultIDStockTaking(String dataarea_id, String site_id)
        {
            String code_tag_1 = "STU";
            String code_tag_2 = dataarea_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 3, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id + "/" + site_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String GetDefaultIDStockTaking(String dataarea_id, String site_id)
        {
            String code_tag_1 = "STU";
            String code_tag_2 = dataarea_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                throw new MPMException("Stock Taking Unit ID not configured yet !!!");
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER - 1 + "", 3, '0') + "/" +
                year + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + dataarea_id + "/" + site_id;

            return value;
        }

        public String RunningNumberCorrectionSL(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "COR-SL";
            String code_tag_2 = dataarea_id + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
                //SubmitChanges();
            }

            String value = code_tag_1 + "/" +
                MPMStringUtil.LPAD(running.NUMBER + "", 8, '0') + "/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" +
                year + "/" + dataarea_id + "/" + maindealer_id + "/" + site_id;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberSPKAHASS(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "SPK-AH";
            String code_tag_2 = dataarea_id + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value = 
                MPMStringUtil.LPAD(running.NUMBER + "", 4, '0') + "/SPK-PSMH/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + year;

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public String RunningNumberBASTAHASS(String dataarea_id, String maindealer_id, String site_id)
        {
            String code_tag_1 = "BAST-AH";
            String code_tag_2 = dataarea_id + "-" + maindealer_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            String value =
                MPMStringUtil.LPAD(running.NUMBER + "", 6, '0') + "/MUT-G/" +
                MPMStringUtil.LPAD(month + "", 2, '0') + "/" + year + "/" + maindealer_id.ToUpper();

            running.NUMBER += 1;
            SubmitChanges();

            return value;
        }

        public long RunningNumberStockTakingPartCounting(String dataarea_id, String site_id, String journal_id)
        {
            String code_tag_1 = journal_id;
            String code_tag_2 = dataarea_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);

                SubmitChanges();
            }

            return running.NUMBER;
        }

        public long NewDefaultIDStockTakingPartCounting(String dataarea_id, String site_id, String journal_id)
        {
            String code_tag_1 = journal_id;
            String code_tag_2 = dataarea_id + "-" + site_id;
            int year = MPMDateUtil.GetCurrentYearValue();
            int month = MPMDateUtil.GetCurrentMonthValue();
            MPM_RUNNING_NUMBER running = RunningNumberObject.Item(code_tag_1, code_tag_2, year, month);
            // there is no running number
            if (running == null)
            {
                running = RunningNumberObject.Create();
                running.NUMBER = 1;
                running.CODE_TAG_1 = code_tag_1;
                running.CODE_TAG_2 = code_tag_2;
                running.YEAR = year;
                running.MONTH = month;
                RunningNumberObject.Insert(running);
            }

            running.NUMBER += 1;
            SubmitChanges();

            return running.NUMBER;
        }

        /** LOCATION **/
        public String GetLocation(String dataarea_id, String site_id, String type)
        {
            String result = "";
            LocationObject location = new LocationObject();
            location.Context = (WmsUnitDataContext)Context;
            MPMWMS_WAREHOUSE_LOCATION item = location.ItemByType(dataarea_id, site_id, type);
            if (item != null)
            {
                result = item.LOCATION_ID;
            }
            location = null;

            return result;
        }
        /********** END OF LOCATION *********/


        /** UNIT **/
        public void UpdateLocationStock(String dataarea_id, String site_id, String warehouse_id, String location_id, int quantity)
        {
            IQueryable<MPMWMS_WAREHOUSE_LOCATION> search = from location in ((WmsUnitDataContext)Context).MPMWMS_WAREHOUSE_LOCATIONs
                                                           where (location.DATAAREA_ID == dataarea_id) &&
                                                           (location.SITE_ID == site_id) &&
                                                           (location.LOCATION_ID == location_id) &&
                                                           (location.WAREHOUSE_ID == warehouse_id)
                                                           select location;
            MPMWMS_WAREHOUSE_LOCATION loc = search.FirstOrDefault();
            if (loc != null)
            {
                loc.STOCK_ACTUAL = loc.STOCK_ACTUAL + quantity;
                if (loc.STOCK_MAX < loc.STOCK_ACTUAL + loc.STOCK_BOOKING)
                {
                    throw new MPMException("Stock overlimited [ Stock Max: " + loc.STOCK_MAX + ", Stock Actual: " + loc.STOCK_ACTUAL +
                        ", Stock Booking: " + loc.STOCK_BOOKING + "]");
                }
            }
            else
            {
                throw new MPMException("Can't update stock actual in location [" + location_id + "]");
            }
        }
        /** END OF UNIT **/

        /** UTILITY FOR IMPORT DATA PURPOSED **/
        public void CalculateRealStock()
        {
            List<MPMWMS_WAREHOUSE_LOCATION> listLocation = LocationObject.List();
            foreach (var location in listLocation.ToList())
            {
                location.STOCK_ACTUAL = 0;
                location.STOCK_BOOKING = 0;
                SubmitChanges();
            }


            List<MPMWMS_UNIT> listUnit = UnitObject.List();
            foreach (var unit in listUnit.ToList())
            {
                if (unit.STATUS == "A")
                {
                    MPMWMS_WAREHOUSE_LOCATION location = LocationObject.Item(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID, unit.WAREHOUSE_ID);
                    if (location != null)
                    {
                        Console.WriteLine(unit.MACHINE_ID + " " + unit.LOCATION_ID);
                        location.STOCK_ACTUAL += 1;
                        SubmitChanges();
                    }
                }
            }
        }
        /** UTILITY FOR IMPORT DATA PURPOSED **/

        /** PRINT BARCODE **/
        protected void PrintZPL(String content)
        {
            MPMNetworkPrint print = new MPMNetworkPrint();
            print.printerIP = "";
            print.printerPort = 0;
            print.myZPL = content;
            print.PrintToIP();
            print = null;
        }
        /** END OF PRINT BARCODE **/
    }
}
