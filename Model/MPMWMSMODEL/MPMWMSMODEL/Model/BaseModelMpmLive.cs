﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPMLibrary.NET.Mvc.Models;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.VendorStoringUnit;

namespace MPMWMSMODEL.Model
{
    public class BaseModelMpmLive : MPMModel
    {
        private VendorStoringUnitObject _VendorStoringUnitObject = new VendorStoringUnitObject();
        public BaseModelMpmLive() : base()
        {
            Context = new WmsMpmLiveDataContext();
            _VendorStoringUnitObject.Context = (WmsMpmLiveDataContext)Context;
        }

        public VendorStoringUnitObject VendorStoringUnitObject
        {
            get
            {
                return _VendorStoringUnitObject;
            }
        }

    }
}
