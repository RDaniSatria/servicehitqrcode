﻿using MPMLibrary.NET.Lib.Exception;
using MPMLibrary.NET.Mvc.Models;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.HistoryUnit;
using MPMWMSMODEL.Table.Location;
using MPMWMSMODEL.Table.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class UnitModel : BaseModel
    {
        public UnitModel()
            : base()
        {
        }

        public void Insert(bool submit, MPMWMS_UNIT unit)
        {
            try
            {
                // get warehouse type
                unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                // changes type
                String changes_type = "U";
                if (unit.SL_DATE != null) changes_type = "SL";
                if (unit.SPG_DATE != null)
                {
                    changes_type = "SPG";
                }

                UnitObject.Insert(unit, changes_type);

                // update warehouse_location actual stock in new location
                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Update(bool submit, MPMWMS_UNIT unit)
        {
            try
            {
                // get warehouse type
                unit.WAREHOUSE_ID = LocationObject.GetWarehouse(unit.DATAAREA_ID, unit.SITE_ID, unit.LOCATION_ID);

                // changes type
                String changes_type = "U";
                if (unit.SL_DATE != null) changes_type = "SL";

                UnitObject.Update(unit, changes_type);

                // update warehouse_location actual stock in old location
                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID, -1);

                // update warehouse_location actual stock in new location
                UpdateLocationStock(unit.DATAAREA_ID, unit.SITE_ID, unit.WAREHOUSE_ID, unit.LOCATION_ID, 1);

                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }

        public void Delete(bool submit, String machine_id, String frame_id, String dataarea_id)
        {
            try
            {
                // delete unit
                MPMWMS_UNIT deletedItem = UnitObject.Item(machine_id, frame_id, dataarea_id);
                UnitObject.Delete(deletedItem);

                // update warehouse_location actual stock in old location
                UpdateLocationStock(deletedItem.DATAAREA_ID, deletedItem.SITE_ID, deletedItem.WAREHOUSE_ID, deletedItem.LOCATION_ID, -1);

                if (submit) SubmitChanges();
            }
            catch (MPMException e)
            {
                throw new MPMException(e.Message);
            }
        }
    }
}
