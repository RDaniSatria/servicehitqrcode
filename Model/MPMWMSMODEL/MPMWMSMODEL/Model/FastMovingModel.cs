﻿using MPMLibrary.NET.Lib.Exception;
using MPMWMSMODEL.Module;
using MPMWMSMODEL.Record.FastMoving;
using MPMWMSMODEL.Table.FastMoving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class FastMovingModel : BaseModel
    {
        private FastMovingObject _FastMovingObject = new FastMovingObject();

        public FastMovingModel()
            : base()
        {
            _FastMovingObject.Context = (WmsUnitDataContext)Context;
        }

        #region MPMWMS_EAD_FASTMOVING

        public void Insert(MPMWMS_EAD_FASTMOVING _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_FASTMOVING data = _FastMovingObject.Item(_item.TYPE, _item.COLOR);

                if (data != null)
                {
                    throw new MPMException("Fast moving data is already exist !!!");
                }
                
                _FastMovingObject.Insert(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Update(MPMWMS_EAD_FASTMOVING _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_FASTMOVING data = _FastMovingObject.Item(_item.TYPE, _item.COLOR);

                if (data == null)
                {
                    throw new MPMException("Can't find the fast moving data !!!");
                }

                _FastMovingObject.Update(_item);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();

                throw new MPMException(e.Message);
            }
        }

        public void Delete(MPMWMS_EAD_FASTMOVING _item)
        {
            BeginTransaction();
            try
            {
                MPMWMS_EAD_FASTMOVING data = _FastMovingObject.Item(_item.TYPE, _item.COLOR);

                if (data == null)
                {
                    throw new MPMException("Can't find the fast moving data !!!");
                }

                _FastMovingObject.Delete(data);

                Commit();
            }
            catch (MPMException e)
            {
                Rollback();
                throw new MPMException(e.Message);
            }
        }

        public List<MPMWMS_EAD_FASTMOVING> List()
        {
            return _FastMovingObject.ItemAll();
        }

        public List<FastMovingRecord> ViewAll()
        {
            return _FastMovingObject.ViewAll();
        }
        
        public MPMWMS_EAD_FASTMOVING Item(String _type, String _color)
        {
            return _FastMovingObject.Item(_type, _color);
        }

        #endregion
    }
}
