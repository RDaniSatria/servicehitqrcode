﻿using MPMWMSMODEL.Module;
using MPMWMSMODEL.Table.DashboardStock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPMWMSMODEL.Model
{
    public class DashboardStockModel : BaseModel
    {
        public DashboardStockByPartNumberObject _DashboardStockByPartNumberObject = new DashboardStockByPartNumberObject();
        public DashboardStockByKelBarangObject _DashboardStockByKelBarangObject = new DashboardStockByKelBarangObject();

        public DashboardStockModel()
            : base()
        {
            _DashboardStockByPartNumberObject.Context = (WmsUnitDataContext)Context;
            _DashboardStockByKelBarangObject.Context = (WmsUnitDataContext)Context;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
